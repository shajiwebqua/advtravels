<?php
 include('application/views/common/header.php');?>
<style>
.nav-tabs {
    margin: 0;
    padding: 0;
    border: 0;    
}
.nav-tabs > li > a {
    background: #f2f2f2;
    border-radius: 0;
    //box-shadow: inset 0 -8px 7px -9px rgba(0,0,0,.4),-2px -2px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li.active > a:hover {
    background: #fff;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li > a:hover {
    background: #e4e4e4;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}
/* Tab Content */
.tab-pane {
    background: #fff;
    //box-shadow: 0 0 4px rgba(0,0,0,.4);
    border-radius: 0;
    border:1px solid #e4e4e4;
    text-align: left;
    padding: 10px;
}

.cli
{
padding:5px 0px 5px 0px;
margin-top:3px;

}

.selclass > div:hover
{
background-color:#f4f4f4;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
 <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <b>Vehicle Documents</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px;'>
		<!-- <li><a href="<?php echo base_url('General/country');?>" style='color:#4b88ed;'><i class="fa fa-globe" aria-hidden="true"></i>Countries</a></li>
		<li><a href="<?php echo base_url('General/category');?>" style='color:#4b88ed;'><i class="fa fa-reorder" aria-hidden="true"></i>Categories</a></li>
		<li><a href="<?php echo base_url('General/agents');?>" style='color:#4b88ed;'><i class="fa fa-user-circle-o" aria-hidden="true"></i>Agents</a></li>
		<li><a href="<?php echo base_url('General/role');?>" style='color:#4b88ed;'><i class="fa fa-gears" aria-hidden="true"></i>Role </a></li>
		<li><a href="<?php echo base_url('General/branch');?>" style='color:#4b88ed;'><i class="fa fa-snowflake-o" aria-hidden="true"></i>Branches</a></li>
		<li><a href="<?php echo base_url('General/profession');?>" style='color:#4b88ed;'><i class="fa fa-user-circle-o" aria-hidden="true"></i>Profession</a></li>
		 -->
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->
              
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
   <div class="page-content">

         <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
<?php
		$dtab=$this->session->userdata('doctab');
?>
	 <div style='padding-left:3px;'> 
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item <?php if($dtab=='1' or $dtab=="") echo 'active'; ?>" id='tb1' >
						<a class="nav-link tp1" data-toggle="tab" href="#docadd"  role="tab"><i class="fa fa-user"></i> Document Add</a>
					</li>
					<li class="nav-item <?php if($dtab=='2') echo 'active'; ?>" id='tb2'>
						<a class="nav-link tp1 " data-toggle="tab" href="#docdt"  role="tab"> <i class="fa fa-cab"></i> Document Details</a>
					</li>
					
                 </ul>
                <!-- Tab panes -->
        <div class="tab-content">
		<div class="tab-pane <?php if($dtab=='1') echo 'active'; ?>" id="docadd" role="tabpanel"> 
	    <div style="background-color:#fff;padding:10px; ">
                        <!-- <div class="row"> -->
        <div class="col-md-12">
			<div class='row'>
				<div class='col-md-6'><h4>Add Documents</h4>  </div>
			   <div class='col-md-2' style='padding-top:6px;text-align:right;padding-right:5px;'>
		         <button type="submit" class="btn_new btn btn-success"  style='padding-left:5px;'><span class='glyphicon glyphicon-plus-sign'></span> New</button>
		      </div>
			</div>
		<hr style='margin:0px 0px 15px 0px;'>
        </div>

				
          <!--<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/add_area')?>" enctype="multipart/form-data">-->
                  <form class="form-horizontal">
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-3 control-label left1">Registration No</label>
                           <div class="col-md-5">
						   <select name='regno' class='regno form-control' id='regno' required>
						   <option value=''>----------</option>
						   <?php
						   foreach($result as $r1)
						   {
							   echo "<option value='". $r1->vehicle_id."'>".$r1->vehicle_regno."</option>";
						   }
						   ?>
						   </select>
						   </div>
                        </div>
                     </div>
					 

                     <div class="form-group">
                     <div class="row">
                      <label class="col-md-3 control-label left1">Vehicle</label>
                           <div class="col-md-5">
                            <input type="text" class="vehicle form-control"  name="vehiclena" id='vehiclena' required>
							 <input type="hidden" class="typeid form-control"  name="typeid" id='typeid' required>
                           </div>
                        </div>
                     </div>
					 
					<hr style='margin:15px 0px 15px 0px;'>
										 
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-3 control-label left1">Document</label>
                           <div class="col-md-5">
                            <input type="text" class="form-control"  name="docna" id='docna' required>
                           </div>
                        </div>
                    </div>
					
					 
					 <div class="form-group">
                     <div class="row">
                      <label class="col-md-3 control-label left1">Document No</label>
                           <div class="col-md-5">
                            <input type="text" class="form-control"  name="docno" id="docno" required>
                           </div>
                        </div>
                     </div>
					 					 
					 <div class="form-group">
                     <div class="row">
                      <label class="col-md-3 control-label left1">Validity From</label>
                           <div class="col-md-3">
                            <input type='text' class="form-control" id='datepicker1' name='vfrom' required />
                           </div>
                        </div>
                     </div>
					 
					<div class="form-group">
                    <div class="row">
                      <label class="col-md-3 control-label left1">Validity To</label>
                           <div class="col-md-3">
                            <input type='text' class="form-control" id='datepicker2' name='vto' required />
                           </div>
                    </div>
                    </div>
					 
					<div class="form-group">
                    <div class="row">
                     <label class="col-md-3 control-label left1">Remainder Period</label>
                           <div class="col-md-2">
                            <input type="text" class="form-control"  name="remind" id="remind" value="15" required>
                           </div>
						   <label class="col-md-1 control-label" style='text-align:left;padding:5px 0px;'>/Days</label>
                    </div>
                    </div>
					 
                    <hr style='margin:10px 0px 10px 0px;'>
 
                    <div class="form-group">
                    <div class="row">
                        <div class="col-md-8" align='right'>
                          <button type="button" class="btn_add btn btn-primary"  style='padding-left:5px;'><span class='glyphicon glyphicon-plus-sign'></span> Add Details</button>
                        </div>
                    </div>
                    </div>

                    </form>
                    </div>
					</div>  <!---- first tab panel ---------->

					
	<div class="tab-pane <?php if($dtab=='2') echo 'active'; ?>" id="docdt" role="tabpanel"> 
		<div style="background-color:#fff;padding:3px 10px 3px 10px;">
			<div class='row'>
			  <div class='col-md-6'><h4>Documents Details</h4> </div>
			  <div class='col-md-6' >
		         
				 <label class='col-md-3' style='padding-top:5px;'>Select Vehicle : </label>
				 <div class='col-md-7' >
				   <select name='sregno' class='form-control' id='sregno' required>
				   <option value="0">----------</option>
				   <?php
				   foreach($result as $r1)
				   {
					   echo "<option value='". $r1->vehicle_id."'>".$r1->vehicle_regno."</option>";
				   }
				   ?>
				   </select>
				 </div>
			 
				  <!--<button type="submit" class="btn_new btn btn-success"  style='padding-left:5px;'><span class='	glyphicon glyphicon-th-list'></span> Details View</button> -->
				  
		      </div>
			
            </div>
			
			 <div class='row'>
			<hr style='margin:0px 0px 15px 0px;'>
			</div>
			
				<div class='row' style='padding-left:10px; padding-right:10px;' >
				<table class="table table-striped table-hover table-bordered" id="example" width='100%'>
                <thead>
                <tr>
                 <th>Action</th>
                 <th>Vehicle</th>
				 <th>Type</th>
                 <th>Document</th>
				 <th>Doc-No</th>
                 <th>Validity from</th>
				 <th>validity To</th>
				 <th>Reminder</th>
                                  
                </tr>
                </thead>
                </table>
				</div>
                            <!-- END BORDERED TABLE PORTLET-->
		</div>						

       </div><!-- tab pane end --->
	   
		</div><!-- tab content -->
		
		</div>
</div>
  </div>
</div>
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<!-- modal windows --->

 <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >

              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
		 
<!--- End -------------------------->


<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
  {
    swal($("#msg").html(),"","success")
    $("#msg").html("");
  }
  
  $('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    autoclose:true,
	
});
   $('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});
 
  get_dataTable("0");
  disable();

$(".regno").prop('disabled',true);
$(".vehicle").prop('disabled',true);
$(".btn_add").prop('disabled',true);

function disable()
{
	$("#docna").prop('disabled',true);
	$("#docno").prop('disabled',true);
	$("#datepicker1").prop('disabled',true);
	$("#datepicker2").prop('disabled',true);
	$("#remind").prop('disabled',true);
}

function enable()
{
	$("#docna").prop('disabled',false);
	$("#docno").prop('disabled',false);
	$("#datepicker1").prop('disabled',false);
	$("#datepicker2").prop('disabled',false);
	$("#remind").prop('disabled',false);
}


$(".btn_new").click(function()
{   
    $(".regno").prop('disabled',false);
	$(".vehicle").prop('disabled',false);
	disable();
});


function set_details()
{
	$(".regno").prop('disabled',true);
	$(".vehicle").prop('disabled',true);
	$(".btn_add").prop('disabled',false);
	enable();
	$("#docna").focus();
}


$(".regno").change(function()
{
	var regn=$(".regno").val();
	 jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/get_vehicle_type",
        dataType: 'html',
        data: {regno: regn},
        success: function(res) 
		{
        var vdt=res.split(",");
		$(".vehicle").val(vdt[1]);
		$(".typeid").val(vdt[0]);
		
		set_details();
		
        }
	 });
		
});


$("#sregno").change(function()
{
	var regn=$("#sregno").val();
	get_dataTable(regn);
});


function get_dataTable(vid)
{ 
      $('#example').dataTable( {
         destroy: true,
        "processing": true,
        
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/doc_ajax/"+vid,// json datasource
               },
        "columnDefs":[
	{"width":"10%","targets":0},
	],
        "columns": [
            { "data": "edit" },
            { "data": "vehicle"},
			{ "data": "vtype"},
            { "data": "docna"},
			{ "data": "docno"},
			{ "data": "vfrom"},
			{ "data": "vto"},
            { "data": "reminder" },
       ]
  } );
}
  
  
$(".btn_add").click(function()
  {
	var rgno=$('#regno').val();
	
	var tid=$('#typeid').val();
	var dna=$('#docna').val();
	var dno=$('#docno').val();
	var vf=$('#datepicker1').val();
	var vt=$('#datepicker2').val();

	var re=$('#remind').val();
	
       $.ajax({
            url: "<?php echo base_url();?>General/add_documents",
            type: 'POST',
            //data: new FormData(this),
			dataType: 'html',
			data:{regno:rgno,typeid:tid,docna:dna,docno:dno,vfrom:vf,vto:vt,remind:re},
            success: function (data, status)
			{
		
              if(data="success")
              {
                swal("Success", "Document details successfully added.", "success");  
                //$('.btn_add')[0].reset();
				get_dataTable();
				$('#docna').val("");
				$('#docno').val("");
				$('#datepicker1').val("");
				$('#datepicker2').val("");
				$('#remind').val();
				$('#docna').focus();
              }
              else
              {
                swal("Error", "Something went wrong", "error");    
              }  
            }
          }); 
  });
  
  
      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
         var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/edit_document",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 


  // $('#example tbody').on('click', '.change', function () {
  //       var Result=$("#myModal2 .modal-body");
  //    //   $(this).parent().parent().toggleClass('selected');
  //        var id =  $(this).attr('id');
  //        //alert(id);
  //        //alert(id);
  //       jQuery.ajax({
  //       type: "POST",
  //       url: "<?php echo base_url(); ?>" + "userprofile/change_password",
  //       dataType: 'html',
  //       data: {uid: id},
  //       success: function(res) {
  //       Result.html(res);
  //                   }
  //           });
    
  //       });     
    
    
    
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

	

 //});
  


</script>



