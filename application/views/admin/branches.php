<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Branch Entries</b></h1>
	 <ol class="breadcrumb" style='font-size:15px'>
	 	<!-- <li><a href="<?php echo base_url('General/country');?>" style='color:#4b88ed;'><i class="fa fa-globe" aria-hidden="true"></i>Countries</a></li>
		<li><a href="<?php echo base_url('General/area');?>" style='color:#4b88ed;'><i class="fa fa-area-chart" aria-hidden="true"></i>Areas</a></li>
		<li><a href="<?php echo base_url('General/agents');?>" style='color:#4b88ed;'><i class="fa fa-user-circle-o" aria-hidden="true"></i>Agents</a></li>
		<li><a href="<?php echo base_url('General/category');?>" style='color:#4b88ed;'><i class="fa fa-reorder" aria-hidden="true"></i>Categories</a></li>
		<li><a href="<?php echo base_url('General/role');?>" style='color:#4b88ed;'><i class="fa fa-gears" aria-hidden="true"></i>Role </a></li>
		<li><a href="<?php echo base_url('General/profession');?>" style='color:#4b88ed;'><i class="fa fa-user-circle-o" aria-hidden="true"></i>Profession</a></li> -->
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->
              
<div class="page-content-wrapper">
       <!-- BEGIN CONTENT BODY -->
   <div class="page-content">
   
			 <div style="background-color:#fff;height:30px;margin-bottom:5px;">
			  <div class='row'>
				 <div class='col-md-4' style='padding:5px 0px 0px 30px;'>
				 <a href="<?php echo base_url('General/add_new_branch');?>"  data-target='#myModal2' data-toggle='modal' style='color:#4b88ed;'><i class="fa fa-plus" aria-hidden="true"></i>Add New Branch</a>
				 </div>
					<div id='idmsg' class='col-md-8 ' style='padding-top:5px;'><?php echo $this->session->flashdata('message'); ?></div>
  			 </div>
		    </div>
	
                  <!-- <div class="row"> -->
        <div style="background-color:#fff;padding:10px; ">
       
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                <div class="col-md-12">
                </div>
                </div>
                </div>
            <table class="table table-striped table-hover table-bordered" id="example">
               <thead>
                <tr>
                 <th>Edit</th>
                 <th>Del</th>
                 <th>Code</th>
				 <th>Name</th>
				 <th>Address</th>
				 <th>Mobile</th>
				 <th>Phone</th>
				 <th>Email</th>
                 <th>Status</th>
                </tr>
                </thead>
			</table>
			</div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                      </div>
                        </div>
		</div> <!-- end Row --->
                <!-- END CONTENT BODY -->
            <!-- </div> -->
            </div>
            <!-- END QUICK SIDEBAR -->
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content" style="height:420px;">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Add</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div> 


            <div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content" style="height:460px;">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>                
        
 
  <!-- End user details -->
  </div>
  
   <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 

</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 
 
 //sweet alert box ----------------------
$("#idmsg").hide();
if($("#idmsg").html()!="")
	{
		swal($("#idmsg").html(),"","success")
		$("#idmsg").html("");
	}
//-------------------------------------
 
 
      $('#example').dataTable( {
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/branch_ajax",// json datasource
               },
        "columnDefs": [
		{"width":"6%","targets":0},
		{"width":"6%","targets":1},
		{"width":"8%","targets":8},
		],
        "columns": [
            { "data": "edit" },
            { "data": "delete" },
            { "data": "code"},
			{ "data": "name"},
			{ "data": "address"},
			{ "data": "mobile"},
			{ "data": "phone"},
			{ "data": "email"},
            { "data": "status" },
      
       ]
  } );

  
      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal3 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/edit_branch",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 


    
    
    
    
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#msg').html(''); }, 3000);
		

 //});
  


</script>



