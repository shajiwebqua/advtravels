<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');
?>

<style type="text/css">
	.toggleLink{
	  display:block;
	  width:100%
	}
	.myn_fl{
    float: right;
    padding-top: 0 !important;
}
</style>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1 class='heading' style='color:#00adee'>Menus
		</h1>
		  <ol class="breadcrumb">
         <li> <a href="<?php echo base_url("Staff/new_staff");?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'> <i class="fa fa-list" aria-hidden="true"></i> New Staff</button></a></li>
		 <li> <a href="<?php echo base_url("Staff/view_staff");?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'> <i class="fa fa-plus" aria-hidden="true"></i> Staff List</button></a></li>
         </ol> 
	</section>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
	<!-- Content Header end -->
	<section class="content"> 
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12">

				<!--Add new user details -->
				<div style="background-color:#fff;border-radius:10px;padding:15px; ">

					<div class="portlet-title">
	 <!--   <div class="caption">
	        <div style="color:blue;" >
	          Details                   
	      </div> -->
	      <center><h5 style="color:green;" id="h5msg" ><?php echo $this->session->flashdata("message"); ?></h5></center>
	      <!--</div> -->
	  </div>
	  <div class="portlet-body form">
	  	<form  onsubmit="return checkdata();" class="form-horizontal" role="form" method="post" action="<?php echo base_url('StaffSalary/addSalary'); ?>" enctype="multipart/form-data">
	  		<!-- text input -->
	  		<div class="box-body">

	  			<div class="row">
	  			
	  				<?php
	  				$query = $this->db->query("

	  					SELECT menu_cat_id, menu_cat_name, menu_category, menu_id, menu_name, menu_link
	  					FROM menus
	  					LEFT JOIN menu_category ON menu_category.menu_cat_id = menus.menu_category
	  					WHERE menu_status = 1
	  					ORDER BY menus.menu_category ASC

	  					");

	  				$i = 1;
	  				//var_dump($query->result());
	  				$pannel_heading = null;
	  				
	  				foreach($query->result() as $value){
	  					$heading = $value->menu_cat_name;
	  					$body = $value->menu_name;
	  					
	  			if($pannel_heading == null || !strcmp($pannel_heading, $heading)){
	  				
	  				if($pannel_heading != null){
	  							echo '</div>
	  						</div>
	  					</div>';
	  				}

	  				$pannel_heading = $value->menu_cat_name;
	  				$pannel_body = $value->menu_name;
	  				echo '
	  				<div class="col-md-5 panel-group">
	  					<div class="panel panel-primary">
	  						<div class="panel-heading">'.$pannel_heading.' 
	  							<div class="myn_fl checkbox">
	  								<label class="checkbox-inline" style="padding-top: 0 !important;">
	  									<input type="checkbox" class=\'checkbox\' id="dashboard" value=\'1\' style="float:right;"/>Active &nbsp </label>
	  								</div>
	  							</div>
	  							<div class="panel-body" id="dashboard_bdy" style="">

	  								<div class="checkbox" style="display:block">
	  									<label class="checkbox-inline">
	  									<input type="checkbox" class=\'checkbox\' style="float:right;" id="dashboard" value=\'\'/> '.$pannel_body.' &nbsp 
	  									</label>
	  								</div>
	  								';

	  							}
	  							else{


	  								echo '
	  								<div class="checkbox" style="display:block">
	  									<label class="checkbox-inline">
	  										<input type="checkbox" class=\'checkbox\' style="float:right;" id="dashboard" value=\'\'/> '.$body.' &nbsp 
	  									</label>
	  								</div>

	  								';
	  							}

	  						}
	  						?>

	  			</div>

	  	</form>
	  </div>	
	</div>   
</div>
</div>
</section>

</div>

<?php include('application/views/common/footer.php');?>
<script type="text/javascript">

$("#h5msg").hide();
if($("#h5msg").html()!="")
{
	swal("Saved",$("#h5msg").val(),"success");
	$("#h5msg").html("");
}	
	
	
$('.checkbox').change(function() {
        if($(this).is(":checked") && $(this).val() == 1) {
            
        }
        else if($(this).is(":checked") == false && $(this).val() == 1){
        	
        }
    });

</script>