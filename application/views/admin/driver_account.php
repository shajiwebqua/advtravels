<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;  
}

</style>

 <!-- Content Wrapper. Contains page content -->
 
<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->   
  <section class="content-header">
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Driver Account</b></h1>

 </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header  end -->    
    
  <section class="content"> 
    <div id="msgbox" style="padding:2px 0px 2px 0px;">
         <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
         <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
         </div>
      </div>  
  <!-- Small boxes (Stat box) -->
    <div class="row">
  <div class="col-md-12">

  <!-- Add new user details-->
  
   <div style="background-color:#fff;border-radius:10px;padding:15px; ">
          
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" >
<!--- search ---------------------------------------- -->
        
         <div class="row">
         <div class="col-md-6">
                  <label class="col-md-4  control-label" style="padding-top:3px;"><b>Select Driver Name:</b> </label>
          <div class="col-md-7">
          <select class="form-control" id="drivers" name="drivers">
            <option value="default">--Select Driver--</option>
            <?php
              foreach($drivers as $driver)
			  {
				if($driver->driver_id!=1)
				{
                echo "<option value='".$driver->driver_id."'>".$driver->driver_name."</option>";
				}
              }
            ?>
          </select>
          </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="col-md-5 control-label" style="padding-top:3px;" >Select From Date : </label>
              <div class="col-md-5">
                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                  <input type="text" class="form-control date-pick" readonly id="datepicker1" value="<?php echo date('d-m-Y'); ?>">
                </div> 
              </div>
			  <div class="col-md-2">
            <button class="btn btn-success" onclick="load_driver_acc()">Go</button>
          </div> 
			  
            </div>
          </div>
       </div>
              <label style="background-color:#cecece;width:100%;height:1px;margin:20px 0px 30px 0px;"></label>
        </div>
      
        </form>
<!-- ***********************************************  search end  ******************************************** -->
              
        
        
        <!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
        
                    <div class="row">
					<div class='col-md-4' style='border-right:1px solid #e4e4e4;'> 
					
					<form method="post" action="<?php echo base_url('Driver/pay_amount') ?>">
					<div style='border:1px solid #e4e4e4;border-radius:10px;width:100%;padding:10px 10px 10px 20px; background-color:#e6e6e6;'>
					<h4 style='margin-top:0px;'><b>Payment</b></h4>
					<hr style='margin:0px;padding:0px 0px 5px 0px;'>
					
			
                      <div class="form-group" >
					  <div class='row'>
                      <label class="col-md-6 control-label" style='margin-top:5px;'>Payable Amount: </label>
					  </div>
					  <div class='row'>
                      <div class="col-md-10">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0" style="border:1px solid #cccccc;">
                          <span class="input-group-addon" style="border:none;">Rs</span>
                          <input type="text" class="form-control" onkeydown="return false" placeholder='Balance Amount' name='balance' style="text-align:right;" id='balance' value="">
                        </div>
                      </div>
					  </div>
                    </div>
				
                     <div class="form-group" style='margin-top:10px;'>
					  <div class='row'>
                      <label class="col-md-6 control-label" style='margin-top:5px;'>Enter Amount : </label>
					  </div>
					  <div class='row'>
                      <div class="col-md-10">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0" style="border:1px solid #cccccc;">
                          <span class="input-group-addon" style="border:none;">Rs</span>
                          <input type="text" class="form-control" onkeydown="return isNumber(event);" onkeyup="check(this)" placeholder='Payed Amount' name='amount' style="text-align:right;" id='amount' required>
                        </div>
                      </div>
					  </div>
                    </div>
					
					<!-- <div class="form-group" style='margin-top:10px;'>
					  <div class='row'>
                      <label class="col-md-4 control-label" style='text-align:right;margin-top:5px;'>Balance : </label>
                      <div class="col-md-6">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0" style="border:1px solid #cccccc;">
                          <span class="input-group-addon" style="border:none;">Rs</span>
                          <input type="text" id='balamount' class="form-control"  placeholder='Payed Amount' name='balamount' style="text-align:right;"  required>
                        </div>
                      </div>
					  </div>
                    </div> -->
					
                    <div class="form-group">
					<div class='row' >
                      <div class="col-md-8 " style="margin-top:10px">
                      <input type="hidden" name="driverid" id="driverid" value="" required>
                      <button type="submit" id="pay_submit" class="btn btn-success btn-lg">Submit Payment</button>
						
						</div>
                      </div>
                    </div>
				</div>
                    </form>
				</div>
					
                    <div class="col-md-8">
					<div class='row'>
					<h4 style='margin-top:0px;padding-left:10px;'>Payment List</h4>
					<hr style='margin:0px;padding:0px 0px 5px 0px;'>
					</div>
					
					
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
									</div>
								</div>
								</div>
                <table class="table table-striped table-hover table-bordered" id="example">
                  <thead>
                    <tr style='color:#4b88ed'>
                     <th>Slno</th>
                     <th>Description</th>
                     <th>Date</th>
                     <th>Debit</th>
                     <th>Credit</th>
                     <th>Balance</th>
                   </tr>
                 </thead>
               </table>
                        </div>
            <label style="background-color:#cecece;width:100%;height:1px;"></label>

                           </div>       
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  </div>
          
 
</section>


<!-- /.content-wrapper --> 
<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

$("#msgbox").hide();
if($("#msg").html()!="")
{
swal($("#msg").html(),"Payed, Thank You.","success");
$("#msg").html("");
}

 $('#datepicker1').datepicker({
  format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
    //endDate: 'now'
   });
   
 $('#datepicker2').datepicker({
  format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
   });
    
  $("#drivers").change(function(){
      load_driver_acc()
  });
  
    function load_driver_acc(){
      var driver_id = $('#drivers').val();
      var date = $("#datepicker1").val();
      $("#balance").val("")
      $("#driverid").val("")
       $("#pay_submit").prop("disabled" , true);
      if(driver_id == "default"){
        return;
      }
    
      $('#example').dataTable( {
       "ordering": false,
       destroy: true,
       "processing": true,
       "initComplete": function () {
        var api = this.api();
        length = api.rows().data().length;
           //var balance = parseInt(api.row(length-1).data().balance);
		   var balance = parseInt(api.row(0).data().balance);
            $("#balance").val(balance);
            $("#driverid").val(driver_id);
            $("button").prop("disabled" , false);
      },

      "ajax": {
        url :"<?php echo base_url(); ?>" + "Driver/driver_account_ajax/" + driver_id + "/" + date
      },
      "columnDefs":[
      {"width":"3%","targets":0},
      {"width":"12%","targets":2},
	  {"width":"8%","targets":3},
	  {"width":"8%","targets":4},
	  {"width":"8%","targets":5},
      ],

      "columns": [
      { "data": "slno" },
      { "data": "description"},
      { "data": "date"},
      { "data": "debit"},
      { "data": "credit"},
      { "data": "balance" }
      ]
    });
    }    
   
 /*  $("#amount").keypress(function(event) {
			 if ( event.which == 13 ) {
				 
				var amt=$("#amount").val();
				var pbal=$("#balance").val();
				var balamt=parseInt(pbal)-parseInt(amt);
				$("#balamount").val(balamt);	
				 
			}
	});
   */
   
   
   
   
    $(document).ready(function(){
        load_driver_acc();
        $("#pay_submit").prop("disabled" , true);
    }); 
</script>



