<?php  

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Investment extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		if (!isset($user))
		{ 
			//redirect('/');
		} 
		else
		{ 
			return true;
		}
		$this->load->helper(array('form', 'url','html'));
	}
	
	public function Add()
	{
		$this->load->model('Model_accounts');
		$cap['cresult']=$this->Model_accounts->view_capitals();
		$this->load->view("account/acc_investments",$cap); 
	}

	public function View()
	{
		$this->load->view("account/View_investments"); 
	}
	
  public function save_investment()
  {
	$this->load->model("Model_accounts");
	
	$dt=$this->input->post('idate');
	$iname=$this->input->post('iname');
	$iamt=$this->input->post('iamount');
	$dt1=explode("-",$dt);
	
	$dt2=$dt1[2]."-".$dt1[1]."-".$dt1[0];
		
	//HEAD ENTRY

	 $this->load->model('Model_accounts');
	 
	 $accdata=array(
	 "accu_date"=>date('Y-m-d'),
	 "accu_description"=>$iname,
	 "accu_voucherno"=>0,
	 "accu_narration"=>"Investment amount",
	 );
	  
	 $insid=$this->Model_accounts->insert_data("ac_accounts",$accdata);

	  //ENTRY 1
	 $accdata2=array(
	 "entry_accuid"=>$insid,
	 "entry_ledgerid"=>3, 	
	 "entry_debit"=>$iamt, 
	 "entry_credit"=>0,
	 );
	 
	 $this->Model_accounts->insert("ac_account_entry",$accdata2);
	 
	 //ENTRY 2
	 $accdata3=array(
	 "entry_accuid"=>$insid,
	 "entry_ledgerid"=>4,  			
	 "entry_debit"=>0, 
	 "entry_credit"=>$iamt,
	 );
	 
	 $this->Model_accounts->insert("ac_account_entry",$accdata3);
	 
	 // add data to cash book   
	 $acccash=array(
	 "cashbook_headid"=>$insid,
	 "cashbook_debit"=>$iamt,
	 "cashbook_credit"=>0, 
	 "cashbook_narration"=>"Investment amount -".$iamt,
	 );
	 $this->Model_accounts->insert("ac_cashbook",$acccash);

	//-----------------------------------
	 $this->session->set_flashdata('message', '1#Invetment amount added.');
	 redirect('Investment/Add');
	}

	public function delete_investment()
	{
		$id=$this->uri->segment(3);
		$this->load->model("Model_accounts");
			$this->Model_accounts->del_investment1($id);
			$this->Model_accounts->del_investment2($id);
			$this->session->set_flashdata('message', '3#Investment details sccessfully removed.'); 
		redirect('Investment/Add');
	}
	
}

?>
