<?php
class Model_vehicle extends CI_Model{
	
    public function form_insert($userdt)
    {
       $this->db->insert('vehicle_registration', $userdt);  
	   return $this->db->insert_id();
    }

 public function expense_insert($userdt)
    {
       $this->db->insert('vehicle_expense', $userdt);  
	   return $this->db->insert_id();
    }
	
  public function accessory_insert($userdt)
    {
       $this->db->insert('vehicle_accessory', $userdt);  
	}
	
  public function view_vehicle(){

		$this->db->select('*');
		//$this->db->join('vehicle_ownerreg', 'vehicle_ownerreg.owner_id=vehicle_registration.vehicle_owner','inner');
		$this->db->from('vehicle_registration');
		$this->db->join('vehicle_types', 'vehicle_types.veh_type_id=vehicle_registration.vehicle_type','inner');
		$query = $this->db->get();
		return $query->result();
	}
		
  public function view_expense()
  {
		$this->db->select('*');
		$this->db->from('vehicle_expense');
		$this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=vehicle_expense.expense_vehicleno','inner');
		$this->db->join('expense_type', 'expense_type.etype_id=vehicle_expense.expense_type','inner');
		$this->db->where('MONTH(vehicle_expense.expense_billdate)',date('m'));
		$this->db->where('YEAR(vehicle_expense.expense_billdate)',date('Y'));
		$this->db->order_by('vehicle_expense.expense_id','desc');
		$query = $this->db->get();
		return $query->result();
  }
	
	
	public function view_expense1($date1,$date2,$vid)
	{
		$this->db->select('*');
		$this->db->from('vehicle_expense');
		$this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=vehicle_expense.expense_vehicleno','inner');
		$this->db->join('expense_type', 'expense_type.etype_id=vehicle_expense.expense_type','inner');
		$where="(vehicle_expense.expense_billdate>='".$date1."' and  vehicle_expense.expense_billdate<='".$date2."')";	
		$this->db->where($where);
		$this->db->where('vehicle_expense.expense_vehicleno',$vid);
		$this->db->order_by('vehicle_expense.expense_id','desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function view_expense2($mon,$yr)
	{
		$this->db->select('*');
		$this->db->from('vehicle_expense');
		$this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=vehicle_expense.expense_vehicleno','inner');
		$this->db->join('expense_type', 'expense_type.etype_id=vehicle_expense.expense_type','inner');
		$this->db->where('MONTH(vehicle_expense.expense_billdate)',$mon);
		$this->db->where('YEAR(vehicle_expense.expense_billdate)',$yr);
		$this->db->order_by('vehicle_expense.expense_id','desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	
	public function view_accessory($lid){
		$this->db->select('*');
		//$this->db->join('vehicle_accessory','vehicle_ownerreg.owner_id=vehicle_registration.vehicle_owner','inner');
		$this->db->from('vehicle_accessory');
		$this->db->where('vehicle_accessory.accessory_vid',$lid);
		$this->db->order_by('vehicle_accessory.accessory_id desc');
		$query = $this->db->get();
		return $query->result();
	}

  public function view_PSvehicles()
	{
		$this->db->select('*');
		$this->db->from('services');
		$this->db->join('vehicle_registration','vehicle_registration.vehicle_id=services.ser_vehicle_id','inner');
		$this->db->join('vehicle_types','vehicle_registration.vehicle_type=vehicle_types.veh_type_id','inner');
		//$this->db->where("((services.ser_kilometer2-services.ser_kilometer1)>=10000)");
		$this->db->where('services.ser_status=2');
		$this->db->order_by('services.ser_id asc');
		$query = $this->db->get();
		return $query->result();
	}

  public function view_Dash_PSvehicles()
  {
		$this->db->select('*');
		$this->db->from('services');
		$this->db->join('vehicle_registration','vehicle_registration.vehicle_id=services.ser_vehicle_id','inner');
		$this->db->join('vehicle_types','vehicle_registration.vehicle_type=vehicle_types.veh_type_id','inner');
		$this->db->where("((services.ser_kilometer2-services.ser_kilometer1)>=10000)");
		$this->db->where('services.ser_status=1');
		$this->db->order_by('services.ser_id asc');
		$query = $this->db->get();
		return $query->result();
  }

  public function view_Dash_PSvehicles_nos()
  {
		$this->db->select('*');
		$this->db->from('services');
		$this->db->join('vehicle_registration','vehicle_registration.vehicle_id=services.ser_vehicle_id','inner');
		$this->db->join('vehicle_types','vehicle_registration.vehicle_type=vehicle_types.veh_type_id','inner');
		$this->db->where("((services.ser_kilometer2-services.ser_kilometer1)>=10000)");
		$this->db->where('services.ser_status=1');
		$this->db->order_by('services.ser_id asc');
		$query = $this->db->get();
		return $query->num_rows();
  }
		
  public function view_PServicing(){
		$this->db->select('*');
		$this->db->from('services');
		$this->db->join('vehicle_registration','vehicle_registration.vehicle_id=services.ser_vehicle_id','inner');
		$this->db->join('vehicle_types','vehicle_registration.vehicle_type=vehicle_types.veh_type_id','inner');
		$this->db->where('services.ser_status=2');
		$this->db->order_by('services.ser_id asc');
		$query = $this->db->get();
		return $query->result();
	}
	
  public function update_vehicle($id,$userdt)
  {
    $this->db->where('vehicle_id', $id);
    $this->db->update('vehicle_registration',$userdt); 
  } 

public function delete_vehicle($id)
{
    $this->db->where('vehicle_id', $id);
    $this->db->delete('vehicle_registration');
    return $this->db->affected_rows();
}

public function delete_accessory($id)
{
    $this->db->where('accessory_id', $id);
    $this->db->delete('vehicle_accessory');
    return $this->db->affected_rows();
}

public function delete_expense($id)
{
    $this->db->where('expense_id', $id);
    $this->db->delete('vehicle_expense');
    return $this->db->affected_rows();
}

//VEHICLE REPORTS --------------------------------------------------------------------------------------------->

public function tripcharge($vid){
  $this->db->select("SUM(trip_charge) as tripcharge");
  $this->db->from("trip_management");
  //$this->db->where("trip_vehicle_id IN (SELECT vehicle_id  FROM vehicle_registration where vehicle_regno = '". $vid ."')");
  $this->db->where("trip_vehicle_id",$vid);
  $this->db->where("trip_status", "2");
  $this->db->group_by('trip_vehicle_id');
  $query = $this->db->get();
  if($query->num_rows() > 0){
	  $result=$query->result();
      return  $result[0]->tripcharge;
  }
  else{ 
  return "0";
  }
} 



public function get_expenses($vid)
{
  $this->db->select("expense_type,etype_name,SUM(expense_amount) as expense_amount");
  $this->db->from("vehicle_expense");
  $this->db->join("expense_type","expense_type.etype_id=vehicle_expense.expense_type","inner");
  $this->db->where("expense_vehicleno",$vid);
  $this->db->group_by('expense_type,etype_name');
  $query = $this->db->get();
  if($query->num_rows() > 0){
     return  $query->result();
  }
  else{ 
  return "0";
  }
}


public function get_service_expense($vid)
{
  $this->db->select("SUM(service_invoiceamt) as service_amount");
  $this->db->from("vehicle_service");
  $this->db->where("service_vehicleid",$vid);
  $this->db->group_by('service_vehicleid');
  $query = $this->db->get();
  if($query->num_rows() > 0){
     $result=$query->result();
      return  $result[0]->service_amount;
  }
  else{ 
  return "0";
  }
}


public function get_vehicle_loan($vid)
{
  //$regno=$this->db->select('vehicle_regno')->from('vehicle_registration')->where("vehicle_id",$vid)->get()->row();

  $this->db->select("*");
  $this->db->from("loan_vehicle_master");
  $this->db->where("loan_vehicleno",$vid);
  $query = $this->db->get();
   return  $query->result();
 }

// VEHICLE REPORTS  date wise functions --------------------------------------------------------------------------------------------->

public function tripcharge_date($vid,$tedate1,$tedate2){
  $this->db->select("SUM(trip_charge) as tripcharge");
  $this->db->from("trip_management");
  //$this->db->where("trip_vehicle_id IN (SELECT vehicle_id  FROM vehicle_registration where vehicle_regno = '". $vid ."')");
  $this->db->where("trip_vehicle_id",$vid);
  $w="trip_management.trip_enddate>='".$tedate1 . "' and trip_management.trip_enddate<='".$tedate2."'";
  $this->db->where($w);
  $this->db->where("trip_status", "2");
  //$this->db->group_by('trip_vehicle_id');
  $query = $this->db->get();
  if($query->num_rows() > 0){
	  $result=$query->result();
      return  $result[0]->tripcharge;
  }
  else{ 
  return "0";
  }
}


public function get_expenses_date($vid,$tedate1,$tedate2)
{
  $this->db->select("expense_type,etype_name,SUM(expense_amount) as expense_amount");
  $this->db->from("vehicle_expense");
  $this->db->join("expense_type","expense_type.etype_id=vehicle_expense.expense_type","inner");
  $this->db->where("expense_vehicleno",$vid);
  
  $w="vehicle_expense.expense_date>='".$tedate1 . "' and vehicle_expense.expense_date<='".$tedate2."'";
  $this->db->where($w);
  $this->db->group_by('expense_type,etype_name');
  $query = $this->db->get();
  if($query->num_rows() > 0){
     return  $query->result();
  }
  else{ 
  return "0";
  }
}


public function get_service_expense_date($vid,$tedate1,$tedate2)
{
  $this->db->select("SUM(service_invoiceamt) as service_amount");
  $this->db->from("vehicle_service");
  $this->db->where("service_vehicleid",$vid);
  
  $w="vehicle_service.service_date>='".$tedate1 . "' and vehicle_service.service_date<='".$tedate2."'";
  $this->db->where($w);
  
  //$this->db->group_by('service_vehicleid');
  $query = $this->db->get();
  if($query->num_rows() > 0){
     $result=$query->result();
      return  $result[0]->service_amount;
  }
  else{ 
  return "0";
  }
}

public function getAll_vehicle_No()
{
  $this->db->select("vehicle_id,vehicle_regno");
  $this->db->from("vehicle_registration");
  $qry=$this->db->get();
  return $qry->result();
}


  public function update_expense($id,$expdt)
  {
    $this->db->where('expense_id', $id);
    $this->db->update('vehicle_expense',$expdt); 
  } 

// VEHICLE REPORTS  END --------------------------------------------------------------------------------------------->

//index page vehicle details -------------------------------------------------------------->

public function trip_charge($vid){
  $this->db->select("SUM(trip_charge) as tripcharge");
  $this->db->from("trip_management");
  $this->db->where("trip_vehicle_id",$vid);
  $this->db->where("MONTH(trip_enddate)",date('m'));
  $this->db->where("trip_status", "2");
  $query = $this->db->get();
  if($query->num_rows() > 0){
	  $result=$query->result();
      if($result[0]->tripcharge>0)
	 {
		 return $result[0]->tripcharge;
	 }
	 else{
		 return "0";
	 }
  }
  else{ 
  return "0";
  }
}

public function get_fuel_amount($vid)
{
  $this->db->select("SUM(expense_amount) as expense_amount");
  $this->db->from("vehicle_expense");
  $this->db->where("expense_vehicleno",$vid);
  $this->db->where("MONTH(expense_date)",date('m'));
  $w="(expense_type=1 or expense_type=2)";
  $this->db->where($w);
  $query = $this->db->get();
  if($query->num_rows() > 0){
     $result=$query->result();
	 if($result[0]->expense_amount>0)
	 {
		 return $result[0]->expense_amount;
	 }
	 else{
		 return "0";
	 }
  }
  else{ 
  return "0";
  }
}


public function get_other_amount($vid)
{
  $this->db->select("SUM(expense_amount) as other_amount");
  $this->db->from("vehicle_expense");
  $this->db->where("expense_vehicleno",$vid);
  $this->db->where("MONTH(expense_date)",date('m'));
  $w="(expense_type!=1 and expense_type!=2)";
  $this->db->where($w);
  $query = $this->db->get();
  if($query->num_rows() > 0){
     $result=$query->result();
	 if($result[0]->other_amount>0)
	 {
		 return $result[0]->other_amount;
	 }
	 else{
		 return "0";
	 }
  }
  else{ 
  return "0";
  }
}

public function get_service_charges($vid)
{
  $this->db->select("SUM(service_invoiceamt) as service_amount");
  $this->db->from("vehicle_service");
  $this->db->where("service_vehicleid",$vid);
  $this->db->where("MONTH(service_date)",date('m'));
  $query = $this->db->get();
  if($query->num_rows() > 0){
     $result=$query->result();
     if($result[0]->service_amount>0)
	 {
		 return $result[0]->service_amount;
	 }
	 else
	 {
		 return "0";
	 }
  }
  else{ 
  return "0";
  }
}

//index page vehicle details -------------------------------------------------------------->



}
?>