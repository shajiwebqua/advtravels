<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');

?>

<style type="text/css">
	 ul.list-group li { background-color: #f0f0f0; }
	 ul.list-group li:hover { background-color: #fff; }

</style>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1 class='heading' style='color:#00adee'>Menus
		</h1>
		 <ol class="breadcrumb">
         <li> <a href="<?php echo base_url("Staff/new_staff");?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'> <i class="fa fa-plus" aria-hidden="true"></i> New Staff</button></a></li>
		 <li> <a href="<?php echo base_url("Staff/view_staff");?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'> <i class="fa fa-list" aria-hidden="true"></i> Staff List</button></a></li>
         </ol> 
	</section>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
	<!-- Content Header end -->
	<section class="content"> 
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12">

				<!--Add new user details -->
				<div style="background-color:#fff;border-radius:10px;padding:15px; ">

					<div class="portlet-title">
	 <!--   <div class="caption">
	        <div style="color:blue;" >
	          Details                   
	      </div> -->
	      <center><h5 style="color:green;" id="h5msg" ><?php echo $this->session->flashdata("message"); ?></h5></center>
	      <!--</div> -->
	  </div>
	  <div class="portlet-body form">
	  <?php 
	  		$val = null;
	  		$user = 0;
	  		if($this->uri->segment(3) != null ){
	  			$val = $this->Model_staff->view_code($this->uri->segment(3));
	  		}

	  		if($val != null){
	  			echo "<h3>Staff :" . $val[0]->staff_name . "</h3>";
	  			$user = $this->uri->segment(3);
	  		}

	   ?>
	  	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('Menus/addSubmenus'); ?>" enctype="multipart/form-data">
	  		<!-- text input -->
	  		<div class="box-body">

	  			<div class="row">
	  			
	  				<?php
	  				echo "<input type='hidden' name='user' id='user' value='{$user}'>";
	  				echo '<ul class="col-md-4 list-group">';
	  				echo "<h4>Select Menus</h4>";

	  				foreach($result as $value){
	  					echo '<li class="list-group-item menu_head" data-catid='.$value->menu_cat_id.'>'.$value->menu_cat_name.'</li>';
	  				}
	  					echo '</ul>';
	  						?>

	  				<ul class='col-md-4 list-group' id="submenus">
	  					
	  				</ul>
	  			</div>

	  	</form>
	  </div>	
	</div>   
</div>
</div>
</section>

</div>

<?php include('application/views/common/footer.php');?>
<script type="text/javascript">
	
$('.menu_head').on("click",function() {
        $('.menu_head').removeClass('active');
        $(this).addClass('active');
        catid = $(this).attr("data-catid")
        user = $("#user").val()
        if(user == 0){
        	alert('Get Proper user code');
        	return;
        }
        
        $.ajax({
        	type : "post",
        	url  : "<?php echo base_url() ?>" + "Menus/listOfSubmenus",
        	dataType : "html",
        	data : {catid : catid, user : user},
        	success:function(result){
        		$("#submenus").empty();
        		//console.log(result)
        		result = JSON.parse(result)
        		user_menu = result['user_menu']
        		menu = result['menu']

        		if(menu[0] == null){
        			return;
        		}

        		menu_user_flag = false;
        		button_txt = "Add";

        		$("#submenus").append("<h4>Select SubMenus</h4>");
        		i = 0;
        		for(var value in menu){
        			j = 0;

        			if(user_menu != null){
        				for(var n in user_menu ){
        					if(user_menu[j++].menu_options == menu[i].menu_id){
        						menu_user_flag = true
        					}
        				}
        			}
        			
        			if(menu_user_flag){
        				$("#submenus").append(
        					"<li class='list-group-item submenu tick' onclick='addtick(this)' data-catid='"+menu[i].menu_id+"'>"+ menu[i].menu_name +" <input type='hidden' name='submunu_val[]' value='"+menu[i++].menu_id+"'><span class='badge'>&#10004;</span></li>"
        					);
        				menu_user_flag = false
        				button_txt = "Update";
        			}else{
        				$("#submenus").append(
        					"<li class='list-group-item submenu' onclick='addtick(this)' data-catid='"+menu[i].menu_id+"'>"+ menu[i++].menu_name +" <input type='hidden' name='submunu_val[]' value='0'></li>"
        					);
        			}

        			
        		}
        		$("#submenus").append("<input type='hidden' name='menuhead' value='"+catid+"'></li>");

        		$("#submenus").append("<button type='submit' class='btn btn-success btn-block' onclick='submit(this)'>"+button_txt+"</button>");
        	}
        });
});
function addtick(obj)
{
	if($(obj).hasClass( "tick" )){
		$(obj).removeClass( "tick" );
		$(obj).find("span").remove();
		$(obj).find("input").val("0");
	}
	else{
		$(obj).addClass("tick");
		$(obj).append("<span class='badge'>&#10004;</span>");
		$(obj).find("input").val($(obj).attr('data-catid'));
	}

}
</script>