<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver extends CI_Controller
{

function __construct(){
parent::__construct();
$user = $this->session->userdata('userid');
      if (!isset($user))
       { 
          redirect('/');
       } 
       else
       { 
			return true;
       }
	   
$this->load->helper(array('form', 'url'));
$this->load->model('Model_driver');
}

public function index()
{
   $this->load->view('admin/driver');
}

public function add_driver() 
{
   $this->load->view("admin/new_driver");
}
  
public function performance() 
{
	$this->session->unset_userdata('tkm');
	$this->session->unset_userdata('drna');
	$this->session->unset_userdata('tkm1');
	$dy=date('Y');
	$dy1=date('Y');
	
	if(date('m')==1)
	{
	$d1=12;
	$d2=date('m');
	$dy1=(date('Y')-1);
	}
	else
	{
	$d1=date('m')-1;
	$d2=date('m');
	$dy1=date('Y');
	}
	//$d1=6;
	//$d2=7;
	
	$this->performer_driver1($d1,$dy1); //previous month
	$this->performer_driver2($d2,$dy);  //current month
		
	$this->load->model("Model_general");
	$data['pr']=$this->yearly_performance();
	$data['drvnames']=$this->Model_general->get_perform_driver_names();
	$data['totalkm']=$this->Model_general->get_driver_yearly_totalkm();
	$data['monthlykm1']=$this->Model_general->get_driver_monthly_totalkm($d2,$dy);
	$data['monthlykm2']=$this->Model_general->get_driver_monthly_totalkm($d1,$dy1);
	
	//$data['monthlykm1']=$this->Model_general->get_driver_monthly_totalkm(7);
	//$data['monthlykm2']=$this->Model_general->get_driver_monthly_totalkm(6);
	
	$data['dmon']=date('m');
	$this->load->view("admin/driver_performance",$data);
}

// to change the month 
public function performs() 
{
		$this->session->set_userdata('tkm',"");
		$this->session->set_userdata('drna',"");
		$this->session->set_userdata('tkm1',"");
		
		$mn=$this->input->post('dmonth');
		$dy=$this->input->post('dyear');
		$dy1=$dy;
		
		if($mn!="")
		{
		$d1=$mn-1;
		$d2=$mn;
				
			if($mn==1)
			{
			$d1=12;	
			$dy1=$dy-1;
			}
				
		$this->performer_driver1($d1,$dy1); //previous month
		$this->performer_driver2($d2,$dy);  //current month
				
		$this->load->model("Model_general");
		$data['pr']=$this->yearly_performance();
		$data['drvnames']=$this->Model_general->get_perform_driver_names();
		$data['result1']=$this->Model_general->get_driver_perform($d2,$dy);
		$data['totalkm']=$this->Model_general->get_driver_yearly_totalkm();
		$data['monthlykm1']=$this->Model_general->get_driver_monthly_totalkm($d2,$dy);
		$data['monthlykm2']=$this->Model_general->get_driver_monthly_totalkm($d1,$dy1);
				
		$data['dmon']=$mn;
		$this->load->view("admin/driver_performance",$data);
		}
}

public function add_new_driver()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('name', 'name', 'required');
$this->form_validation->set_rules('email', 'email', 'required');


 if($this->form_validation->run())
        {
		$fname= base_url('assets/dist/img/user.jpg');
	
       /*if(isset($_FILES['image']))
	   {
				$uploadPath = './upload/images/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|jpe';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
										
				
			if(!$this->upload->do_upload('image'))
	            {
                	$this->session->set_flashdata('message1', 'Image missing, Try again.');
					redirect('Driver/add_driver');  
				}
				else
				{
					$upload_data = $this->upload->data();
					$file_name = $upload_data['file_name'];
					$fname=base_url('/upload/images')."/".$file_name;
				}
		}
		else
		{
			$fname=$this->input->post('drimage');
		}*/
			
              
    $name=$this->input->post('name');
    $email=$this->input->post('email');
    $mobile=$this->input->post('mobile');
    $address=$this->input->post('address');
    $landline=$this->input->post('landline');
    $licence=$this->input->post('licence');
    $badge=$this->input->post('badge');
    $status=$this->input->post('status');
    $exdate=$this->input->post('expdate');
	$drsal=$this->input->post('drsalary');
		
	$staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
    $date=date("Y/m/d");
		
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 

	$dt= date('Y-m-d', strtotime('-1 day'));
	
       $userdt = array(
                'driver_name' =>strtoupper($name),
                'driver_image'=>$fname,
                'driver_email' => $email,
				'driver_mobile' =>$mobile,
                'driver_landline' =>$landline,
                'driver_address' =>$address,
                'driver_licenceno' =>$licence,
				'driver_startdate' =>date('Y-m-d'),
				'driver_enddate' =>$dt,
				'driver_expdate' =>$exdate,
				'driver_badgeno' => $badge,
				'driver_salary' => $drsal,
				'driver_status'=>"1",
				'driver_available'=>"0",
               );
			   
         $this->load->Model('Model_driver');
         $this->Model_driver->form_insert($userdt);
		 
		/* // To create ledger Account-----------------
		  
		  $ledger=array('acc_ledg_lname'=>strtoupper($name),
						  'acc_ledg_under' => "",
						  'acc_ledg_name'=>strtoupper($name),
						  'acc_ledg_address'=>$address."- Mob:".$mobile,
						  'acc_ledg_opbalance'=>"0",
						  );
		  
		  $this->load->model('Model_account');
          $this->Model_account->insert("acc_ledgers",$ledger);
		  
		  // To create ledger Account-----------------
		  
*/

          $uid = $this->db->insert_id();
			$userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Driver Details Added'
            );
			
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
		    
		  
		  $this->session->set_flashdata('message', 'Driver details added..!');
		  redirect('Driver/add_driver');  
  }
 else
 {
    $this->session->set_flashdata('message1', 'Data missing, Try again.');
    //$data['message']="Data Inserted Successfully";
	redirect('Driver/add_driver');  
 }  
}

public function driver_ajax()
{

        $this->load->model('Model_driver');
        $results=$this->Model_driver->view_driver();
		
        $data1 = array();
        foreach($results  as $r) 
               {
            $edit="<a href='#myModal2' id='$r->driver_id' data-toggle='modal' class='edit open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='Edit' data-toggle='modal' style='padding-left:11px;padding-right:11px;'>Edit</button></center></a>";
			$mm= "<center><button class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' >Delete</button></center>";
            $del=anchor('Driver/del_entry/'.$r->driver_id, $mm, array('id' =>'del_conf'));
            //$del=anchor('Driver/del_entry/'.$r->driver_id, 'Delete', array('id' =>'del_conf'));
            $img="<img src='$r->driver_image' class='img-thumbnail' class='img-responsive' alt='Responsive image' style='width:80px; height:82px;' />";
    
			
		if($r->driver_status==1)
		{
			$st="<font color='green'>Active</font>";
			$drna=$r->driver_name;
		}
		else
		{
			$st="<font color='red'>Resigned</font>";
			$drna="<font color='red'>".$r->driver_name."</font>";
		}
                      array_push($data1, array(
                            "edit"=>$edit.$del,
                            "id"=>"$r->driver_id",
                            "name"=>$drna,
                            "address"=>$r->driver_address."<br><b>Mobile:</b>".$r->driver_mobile."<br><b>Email:</b>".$r->driver_email,
                            "licence"=>"<b>Licence:</b>".$r->driver_licenceno."<br><b>Exp.Date:</b>".date_format(date_create($r->driver_expdate),"d-m-Y")."<br><b>Badge:</b>".$r->driver_badgeno."<br><b>Salary: </b>".$r->driver_salary,
							"drsal"=>"$r->driver_salary",
							"image"=>"$img",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}


 public function edit_driver() 
	{
	$m="";
    $f="";
    
        $data=$this->session->all_userdata();
        $id=$this->input->post('uid');
        $query = $this->db->select('*')->from('driverregistration')->where('driver_id',$id)->get();
        $row = $query->row();
     
    
    echo "  
     <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>            
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Driver Details</font></h4>
    </div>
    <br>

 <form  class='form-horizontal' role='form' method='post' action='". site_url('Driver/update_driver'). "' enctype='multipart/form-data' onsubmit='return checkdata();'>
                    <!-- text input -->
 <div class='row'>
 <div class='col-md-8'>
                <div class='form-group'>
                    <label class='col-md-4 control-label' style='padding-top:10px;'>Name</label>
                        <div class='col-md-8'>  
						<input type='text' name='name' class='form-control'  id='mask_date2' value='$row->driver_name' required/>
					</div>                   
				</div>
				
				<div class='form-group'>
                    <label class='col-md-4 control-label' style='padding-top:10px;'>Address </label>
                    <div class='col-md-8'>  
					<textarea name='address' row='2' class='form-control'>$row->driver_address</textarea>
				    </div>                   
			    </div>
				
			<div class='form-group'>
				<label class='col-md-4 control-label' style='padding-top:10px;'>Email </label>
				<div class='col-md-8'>  
				    <input type='email' name='email' class='form-control'  id='mask_date2' value='$row->driver_email' required/>
				</div>                   
            </div>	
				
	</div>
	<div class='col-md-4'>
				
                 <div class='form-group' >
                         <div class='col-md-12'>
                             <div class='fileinput fileinput-new' data-provides='fileinput'>
                             <div class='fileinput-new thumbnail' style='max-width: 145px; max-height: 152px;''>
                              <img src='$row->driver_image' alt='' /> </div>
                               <div class='fileinput-preview fileinput-exists thumbnail' style='max-width: 145px; max-height: 152px;'> </div>
                             <div>
                              <span class='btn btn-default btn-file'>
                              <span class='fileinput-new'> Select </span>
                              <span class='fileinput-exists'> Change </span>
                              <input type='file' name='image'> </span>
                              <a href='javascript:;' class='btn btn-default fileinput-exists' data-dismiss='fileinput'> Remove </a>
                         </div>
                        </div>
                      </div>
                      <input type='hidden' name='tmpimage' value='$row->driver_image'>
                    
                 </div> 
    </div>
</div>

<div class='row'>
<div class='col-md-12'>  

        <div class='form-group'>
          <label class='col-md-3  control-label' style='margin-left :-17px;'>Mobile </label>
          <div class='col-md-8'>
          <input type='number' class='form-control' placeholder='Enter mobile no' name='mobile' value='$row->driver_mobile' required id='mobile'>
          <label id='mobmessage' style='color:red;'></label>
            </div>
          </div>
          
          <div class='form-group'>
          <label class='col-md-3  control-label'  style='margin-left :-17px;'>Landline </label>
          <div class='col-md-8'>
          <input type='number' class='form-control' placeholder='Enter landline' name='landline' value='$row->driver_landline' id='landline' required/>
          <label id='landmessage' style='color:red;'></label>
          </div>
          </div>
         <div class='form-group'>
          <label class='col-md-3  control-label'  style='margin-left :-17px;'>Licence No </label>
          <div class='col-md-8'>
          <input type='text' class='form-control' placeholder='Enter licence' name='licence' value='$row->driver_licenceno' required/>
          </div>
          </div>

          <div class='form-group'>
          <label class='col-md-3  control-label'  style='margin-left :-17px;'>Badge No</label>
          <div class='col-md-8'>
          <input type='text' class='form-control' placeholder='Enter Bagde No' name='badge' value='$row->driver_badgeno' required/>
          </div>
          </div>
                      <div class='form-group'>
                      <label class='col-md-3  control-label'   style='margin-left :-17px;'>Status </label>
					<div class='col-md-8'>
                      <select type='text' name='status' class='form-control'>";
 
					   echo '<option value="0" ';if($row->driver_status=='0')echo "selected"; echo'>Inactive</option>
							<option value="1"';if($row->driver_status=='1')echo "selected"; echo ">Active</option>
							</select>
                               </div>                     
                       </div>
	   
						</div>
						</div>
	   
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-8'>
                <input type='hidden' name='uid'  value='$row->driver_id'/>
                <input type='submit' class='btn btn-primary' value='Update Details'/>
				 <button type='button' class='btn btn-default'  data-dismiss='modal' style='margin-left:20px;'>Close</button>
                </div>
                </div>                                
           </form>
		   
		<!--  <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div> -->

       <script type='text/javascript'>
      
    $('#mobmessage').hide();
    $('#landmessage').hide();
  function checkdata()
  {
    
    var mob=$('#mobile').val();
    var landline=$('#landline').val();
      
  
    if(mob.length<10 || mob.length>10)
    {
	  $('#mobmessage').show();
      $('#mobmessage').html('Invalid mobile no,10 digits only.');
      return false;
    }
    else if(landline.length<11 || landline.length>11)
    {
	 $('#mobmessage').hide();
     $('#landmessage').show();     
 	 $('#landmessage').html('Invalid mobile no,11 digits only.');
      return false;
    }
    else
    {
	  $('#mobmessage').hide();
      $('#landmessage').hide();
      $('#mobmessage').html('');
      $('#landmessage').html('');
      return true;
    }
  }

</script>  ";
 
 }
  
 
public function update_driver()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('name', 'usrname', 'required');
$this->form_validation->set_rules('email', 'usremail', 'required');
$this->form_validation->set_rules('mobile', 'usrmobile', 'required');

 
       if($this->form_validation->run())
        {
        $upload1 = $_FILES['image']['name'];
        $fname=$this->input->post('tmpimage');
         
    if(isset($_FILES['image']) && $_FILES['image']['size']!=0)
    {

        $config['upload_path'] = 'upload/images';                        
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $this->load->library('upload', $config);
        $this->upload->do_upload('image');
        $upload_data = $this->upload->data();
        $file_name = $upload_data['file_name'];
        $fname=base_url('/upload/images')."/".$file_name;
    }
        
    $this->load->model('Model_driver');
	
    $name=$this->input->post('name');
    $email=$this->input->post('email');
    $mobile=$this->input->post('mobile');
    $address=$this->input->post('address');
    $landline=$this->input->post('landline');
    $licence=$this->input->post('licence');
    $badge=$this->input->post('badge');
	$status=$this->input->post('status');
    $uid=$this->input->post('uid');
    $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
       $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
    
     $userdt = array(
                'driver_name' => strtoupper($name),
                'driver_image'=>$fname,
                'driver_email' => $email,
        'driver_mobile' =>$mobile,
                'driver_landline' =>$landline,
                'driver_address' =>$address,
                'driver_licenceno' =>$licence,
        'driver_badgeno' => $badge,
        'driver_status'=>$status
               );
  
        $this->Model_driver->update_driver($uid,$userdt);
        $this->session->set_flashdata('message',  'Details updated..!');
         $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Driver Details Updated'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
      redirect('Driver/index');

       }
        else 
        {
		   redirect('Driver/index');
        }                   
}

  public function del_entry()
  {
    $id=$this->uri->segment(3);
	//$id=$this->input->post('id');
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
            $this->load->model('Model_driver');
            $result=$this->Model_driver->delete_driver($id);
            if($result)
            {
                    $this->session->set_flashdata('message2', 'Details Removed.!');
                    $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$id,
            'log_desc'=>'Driver Details Deleted'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
                   redirect('Driver/index');
            }
           else
           {
            $this->session->set_flashdata('message', 'Please try again');
            redirect('Driver/index');
           }
  }
  
  
  
  public function driver_trip_list(){
    $this->load->model("Model_driver");
    $data["drivers"] = $this->Model_driver->select_cl("driverregistration", "driver_id, driver_name");
    $this->load->view("admin/driver_trip_list", $data);
  }
  
  
 /* public function driver_account(){
    $this->load->model("Model_driver");
    $data["drivers"] = $this->Model_driver->select_cl("driverregistration", "driver_id, driver_name");
    $this->load->view("admin/driver_account", $data);
  }
   */
   
  
  public function driver_triplist_ajax()
  {
    $driver_id = $this->uri->segment(3);
	$dt = $this->uri->segment(4);
	$mon= $this->uri->segment(5);
	$this->load->model("Model_driver");

	if($dt=='0' and $mon=="0")
	{
  	$results = $this->Model_driver->select("trip_management", array("trip_driver_id" => $driver_id));
	}
	else if($dt!=="0" and $mon=="0")
	{
	$results = $this->Model_driver->select("trip_management", array("trip_driver_id" => $driver_id,"trip_enddate>="=>$dt));
	}
	else if($dt=="0" and $mon!="0")
	{
	$results = $this->Model_driver->select("trip_management", array("trip_driver_id" => $driver_id,"MONTH(trip_enddate)="=>$mon));
	}
  
   $data1 = array();
    foreach($results as $r)
	{
		$total=0;
		
      //$startdate    = date_create($r->start_date);
      //$enddate      = date_create($r->end_date);
      //$no_of_days   = date_diff($startdate,$enddate);
	  
	  $no_of_days   = 10;
	  
	  $sd1=explode("-",$r->trip_startdate);
	  $sd2=$sd1[2]."-".$sd1[1]."-".$sd1[0];
	  
	  $ed1=explode("-",$r->trip_enddate);
	  $ed2=$ed1[2]."-".$ed1[1]."-".$ed1[0];
	  
	  $total=number_format(($r->trip_charge + $r->trip_drbatha),"2",".","");
	  

		array_push($data1, array(
            "tripid"=> $r->trip_id,
            "sedate"=> $sd2." <font color=red>=></font> ".$ed2,
           // "days"=> $no_of_days->format("%a days"),
			"days"=> $no_of_days,
			"tfromto"=>$r->trip_from." <font color=red>=></font> ".$r->trip_to,
            "tcharge"=> $r->trip_charge,
            "dbata"=> $r->trip_drbatha,
            "total"=> $total,
      ));
      }
       echo json_encode(array('data' => $data1));
    }

	
	

  /*  public function driver_account_ajax(){
      $driver_id = $this->uri->segment(3);
      
      $date = $this->uri->segment(4);
      $time = strtotime($date);
      $date = date('Y-m-d',$time);
      
      $this->load->model("Model_driver");
      $results = $this->Model_driver->display_driver_account("driver_account", array("driver_id" => $driver_id, "DATE(date) <=" => $date));
      $data1 = array();
      $i = count($results);
      $balance = 0;
      foreach($results as $r){
        array_push($data1, array(
          "slno"=> $i--,
          "description"=> $r->description,
          "date"=> $r->date,
          "debit"=> $r->debit,
          "credit"=> $r->credit,
          "balance"=> $r->balance
          ));
      }
      echo json_encode(array('data' => $data1));
    }
*/
	
 /*   public function pay_amount(){
      if($this->input->post() != null){
        $this->load->model("Model_driver");
          $driver = $this->input->post("driverid");
          $amount = $this->input->post("amount");
          //echo $amount;
          $bls = $this->Model_driver->select("driver_account",array("driver_id"  => $driver , "bl_status" => '1'), "balance");

          $balance = 0;
          if($bls != null){
            $this->Model_driver->update("driver_account",array("bl_status" => '0'), array("driver_id" => $driver));  
            $balance += $bls[0]->balance;
          }
          $balance -= $amount;
          $driver_acc = array(
              "driver_id" => $driver,
              "debit" => $amount,
              "description" => "Payed",
              "date" => date("Y-m-d"),
              "balance" => $balance,
              "bl_status" => '1'
          ); 
          $this->Model_driver->insert("driver_account", $driver_acc);
        $this->session->set_flashdata('message',  'Amount : Rs. ' .$this->input->post("amount"));
      }
      redirect("Driver/driver_account");
    }

	
	*/
	
	
/*  public function driver_payment()
  {
    $this->load->model("Model_driver");
    $driver = $this->input->post("driverid");
    $data["driver_name"] = $this->Model_driver->select("driverregistration",array("driver_id" => $driver),"driver_name");
    $data = $this->Model_driver->count_charges(array("driver_id" => $driver));
    
    $charges = array(
      "total" => $data[0]->credit,
      "payed" => $data[0]->debit,
      "payable" => $data[0]->credit - $data[0]->debit
    );

    echo json_encode($charges);
  }
  */
  

  
public function upload_image()
  {
	  if(isset($_FILES['myfile']))
	   {
		$uploadPath ='/public_html/upload/images';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|jpe';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
										
				
				if(!$this->upload->do_upload('myfile'))
	            {

                $this->session->set_flashdata('message1', $config['upload_path']);
        		redirect('Driver/newdrv');  
				}
				else
				{
					$upload_data = $this->upload->data();
					$file_name = $upload_data['file_name'];
					$fname=base_url('/upload/images')."/".$file_name;
					echo $fname;
				}
            }
			else
			{
				 $this->session->set_flashdata('message1', 'Image missing, Try again.');
        		redirect('Driver/newdrv');  
			}
  }
    
  public function newdrv()
  {
	  $this->load->view('admin/new_driver1');
  }
   
   
 // yearly performance details------------------------------------------------  
   
  public function yearly_performance()
  {
	$this->load->model("Model_general");
	$drres=array();
	$i=0;
	$j=0;
	
	$drresult=$this->Model_general->get_perform_driver_names();

	foreach($drresult as $drr)
	{
	$drid=$drr->driver_id;
	$i=0;
		for($x=1;$x<=12;$x++)
		{
			$row1=$this->Model_general->get_driver_perform_yearly($x,$drid);
			if(is_object($row1))
			{
			$dkm=($row1->endkm - $row1->startkm);
			$dkm=round(($dkm/100),0,1);
			$drres[$j][$i++]=$dkm;
			}
			else
			{
				$drres[$j][$i++]=1;
			}
		}
		$j++;
	}
	return $drres;
	}
	
	
	
//--------------the following functions written into the Login page for starter dashboard --------->
	
public function performer_driver1($dt1,$y1)
{
$this->load->Model('Model_general');
$result=$this->Model_general->get_driver_perform($dt1,$y1);
$dresult=$this->Model_general->get_perform_driver_names();

$nrows=$this->Model_general->get_driver_perform_numrows($dt1,$y1);

$data1=array();
$i=0;
$tkm="";
$dna="";
$dtop=array();
$drid=0;
$dkmid="1,0";
if($nrows>0)
{

$m=array();	
	$x1=0;
	foreach($dresult as $r11)
	{
		if($x1<count($result))
		{
			if($result[$x1]->trip_driver_id==$r11->driver_id)
			{
			
				$rkm=($result[$x1]->endkm - $result[$x1]->startkm);
				$v1=round(($rkm/100),0,1);
				
				$d=array("0"=>$v1,"1"=> $result[$x1]->trip_driver_id);
				array_push($m,$d);	
				$x1++;
			}	
			else
			{
				$d=array("0"=>"1","1"=> $r11->driver_id);
				array_push($m,$d);
			}	
		}
		else
		{
		$d=array("0"=>"1","1"=> $r11->driver_id);
		array_push($m,$d);	
		}
	}	

	//var_dump($m1);
		
	for($x2=0;$x2<count($m);$x2++)
	{
		if($m[$x2][0]==0)
		{
			$m[$x2][0]=5;
			$tkm.=$m[$x2][0].",";
		}
		else
		{
		$tkm.=$m[$x2][0].",";
		}
	}

	array_multisort(array_column($m, 0), SORT_DESC, $m);
	$dkmid=$m[0][0].",".$m[0][1];

	//var_dump($m);
	
	foreach($dresult as $dr)
	{
		$dna.=$dr->driver_name. ',';	
	}
	
	
}
else
{
	//foreach($dresult as $d22)
	//{
	//$dkmid="1,".$d22->driver_id;
	//break;
	//}
	
		foreach($dresult as $dr)
		{
		$dna.=$dr->driver_name . ',';	
		$tkm.='1' .',';
		}	
}	

$tkm=substr($tkm,0,strlen($tkm)-1);
$dna=substr($dna,0,strlen($dna)-1);

$dd1=explode(",",$dkmid);

$this->session->set_userdata('tkm',$tkm);
$this->session->set_userdata('drna',$dna);
$this->session->set_userdata('drvid',$dd1[1]);
}
 
 
 
public function performer_driver2($dt2,$y2)
{
$this->load->Model('Model_general');
$result=$this->Model_general->get_driver_perform($dt2,$y2);
$dresult=$this->Model_general->get_perform_driver_names();

$nrows1=$this->Model_general->get_driver_perform_numrows($dt2,$y2);

$data1=array();
$i=0;
$tkm1="";
$dna1="";
$dtop1=array();
$drid1=0;
$dkmid1="1,0";

if($nrows1>0)
{

		
	$m1=array();	
	$x1=0;
	foreach($dresult as $r11)
	{
		if($x1<count($result))
		{
			if($result[$x1]->trip_driver_id==$r11->driver_id)
			{
			
				$rkm=($result[$x1]->endkm - $result[$x1]->startkm);
				$v1=round(($rkm/100),0,1);
							
				$d=array("0"=>$v1,"1"=> $result[$x1]->trip_driver_id);
				array_push($m1,$d);	
				$x1++;
			}	
			else
			{
				$d=array("0"=>"1","1"=> $r11->driver_id);
				array_push($m1,$d);
			}	
		}
		else
		{
		$d=array("0"=>"1","1"=> $r11->driver_id);
		array_push($m1,$d);	
		}
	}	

	
	for($x2=0;$x2<count($m1);$x2++)
	{
		if($m1[$x2][0]==0)
		{
			$m1[$x2][0]=5;
			$tkm1.=$m1[$x2][0].",";
		}
		else
		{
		$tkm1.=$m1[$x2][0].",";
		}
	}

	array_multisort(array_column($m1, 0), SORT_DESC, $m1);
	$dkmid1=$m1[0][0].",".$m1[0][1];
//-----------------------
		
	foreach($dresult as $dr)
	{
		$dna1.=$dr->driver_name. ',';	
	}
}
else
{
//	foreach($dresult as $d22)
//	{
//	$dkmid1="1,".$d22->driver_id;
//	break;
//	}	
$dkmid1="1,0";

	foreach($dresult as $dr)
		{
		$dna1.=$dr->driver_name. ',';	
		$tkm1.='1' .',';
		}
		
}	

$dd2=explode(",",$dkmid1);

$tkm1=substr($tkm1,0,strlen($tkm1)-1);
$this->session->set_userdata('tkm1',$tkm1);
$this->session->set_userdata('drvid1',$dd2[1]);
}
 
//----------------------------------------------------------------------------------------------->	
	
	
	
  }
 ?>