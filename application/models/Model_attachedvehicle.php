<?php
class Model_attachedvehicle extends CI_Model{
	
    public function form_insert($log)
    {
       $this->db->insert('attached_vehicledetails', $log);  
    }



    public function get_ownerid()
    {
    $this->db->select('*');
    $this->db->from('vehicle_ownerreg');
    $query = $this->db->get();
    return $query->result();
    }

     public function view_vehicle()
    {
    $this->db->select('*');
    $this->db->from('attached_vehicledetails');
     $this->db->join('vehicle_ownerreg', 'vehicle_ownerreg.owner_id=attached_vehicledetails.attached_ownerid','inner');
    $query = $this->db->get();
    return $query->result();
    }
   
    public function update_vehicle($id,$usr_newdt)
    {
    	$this->db->where('attached_id', $id);
	$this->db->update('attached_vehicledetails', $usr_newdt); 
    }
    public function delete_vehicle($id)
    {
    	$this->db->where('attached_id', $id);
    $this->db->delete('attached_vehicledetails');
    return $this->db->affected_rows();
    }


    

}