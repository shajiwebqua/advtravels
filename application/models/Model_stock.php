<?php
class Model_stock extends CI_Model{
	
  
	public function insert_stock($values){
        $this->db->insert('item_stock', $values);  
    }
	
	
	
	 public function update_stock($values, $where){
        $this->db->where("item_sta_particulars",$where);
        $this->db->update('item_stock', $values); 
    }
	
	public function view_stock()
	{
		$this->db->select('*')->from("item_stock");
		
		//$where="(YEAR(salary_date)='".date('Y')."') ";
		//$this->db->where($where);
		$this->db->order_by("item_sta_id","asc");
		$query=$this->db->get();
		return $query->result();
	}
	
	public function view_stock1($cat)
	{
		$this->db->select('*')->from("item_stock");
		$this->db->where("item_sta_category",$cat);
		$this->db->order_by("item_sta_id","asc");
		$query=$this->db->get();
		return $query->result();
	}
	
	public function del_stock($id)
	{
    $this->db->where('item_sta_id', $id);
    $this->db->delete('item_stock');
    return $this->db->affected_rows();
    }
	
	
	public function update_stock_dt($stdata,$id)
	{
    $this->db->where('item_sta_id', $id);
    $this->db->update('item_stock',$stdata);
    }
	

	 public function insert_advance($values){
        $this->db->insert('salary_advance', $values); 
    }
		
	public function view_salary_details()
	{
		$this->db->select('*')->from("staff_salary");
		$this->db->join("staffregistration","staffregistration.staff_id=staff_salary.salary_staffid","inner");
		
		$where="(YEAR(salary_date)='".date('Y')."') ";
		$this->db->where($where);
		
		$this->db->order_by("salary_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	
	public function view_salary_details1($dt1,$dt2,$op)
	{
		$this->db->select('*')->from("staff_salary");
		$this->db->join("staffregistration","staffregistration.staff_id=staff_salary.salary_staffid","inner");
		if($op==1)
		{
			$where="(salary_date>='".$dt1."' and salary_date<='".$dt2."') ";
		}
		else if($op==1)
		{
			$where="(MONTH(salary_date)='".$dt1."' and YEAR(salary_date)='".$dt2."') ";
		}
		else
		{
			$where="(YEAR(salary_date)='".date('Y')."') ";
		}
		
		$this->db->where($where);
		$this->db->order_by("salary_id","desc");
		
		$query=$this->db->get();
		return $query->result();
	}
	
	
	
	public function view_advance_salary()
	{
		$this->db->select('*')->from("salary_advance");
		$this->db->join("staffregistration","staffregistration.staff_id=salary_advance.sal_adv_staffid","inner");
		
		$where="(YEAR(sal_adv_date)='".date('Y')."') ";
		$this->db->where($where);
		
		$this->db->order_by("sal_adv_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	
	public function view_advance_salary1($dt1,$dt2,$op)
	{
		$this->db->select('*')->from("salary_advance");
		$this->db->join("staffregistration","staffregistration.staff_id=salary_advance.sal_adv_staffid","inner");
		if($op==1)
		{
			$where="(sal_adv_date>='".$dt1."' and sal_adv_date<='".$dt2."') ";
		}
		else if($op==1)
		{
			$where="(MONTH(sal_adv_date)='".$dt1."' and YEAR(sal_adv_date)='".$dt2."') ";
		}
		else
		{
			$where="(YEAR(sal_adv_date)='".date('Y')."') ";
		}
		
		$this->db->where($where);
		
		$this->db->order_by("sal_adv_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
		
	public function delete_advance($id)
	{
    $this->db->where('sal_adv_id', $id);
    $this->db->delete('salary_advance');
    return $this->db->affected_rows();
    }
		
	public function get_advance_total()
	{
	    $this->db->select('staff_name,SUM(sal_adv_amount) as total')->from("salary_advance");
		$this->db->join("staffregistration","staffregistration.staff_id=salary_advance.sal_adv_staffid","inner");
		$where="(MONTH(sal_adv_date)='".date('m')."') ";
		$this->db->where($where);
		$this->db->GROUP_by("staff_name");
		$query=$this->db->get();
		return $query->result();
		
	}
		
	// DELIVERY -------------------------------------
	
	public function insert_delivery($values){
        $this->db->insert('delivery_stock', $values);  
    }
		
	public function view_delivery()
	{
		$this->db->select('*')->from("delivery_stock");
		$this->db->where("MONTH(delivery_date)=".date('m'));
		$this->db->order_by("delivery_id","asc");
		$query=$this->db->get();
		return $query->result();
	}
	
	public function view_delivery1($dt1,$dt2)
	{
		$this->db->select('*')->from("delivery_stock");
		$where="delivery_date>='".$dt1."' and  delivery_date<='".$dt2."'"; 
		$this->db->where($where);
		$this->db->order_by("delivery_id","asc");
		$query=$this->db->get();
		return $query->result();
	}
	
	
	public function del_delivery($id)
	{
	
    $this->db->where('delivery_id', $id);
    $this->db->delete('delivery_stock');
    return $this->db->affected_rows();
    }
	
	
	
	
}