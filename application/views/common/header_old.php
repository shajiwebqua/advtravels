<!DOCTYPE html>
<html>
<style  type="text/css">
 .fset
 {
   height:30px;
   width:100%;
   background-color:#d2d3d4; 
   border-radius:10px;
   padding-top:5px;
   margin-bottom:10px;
   text-align:center;
 }
 
/* pre-loading-----------------------------*/
#loading {
    width:100px;
    height:100px;
    border:2px solid #ccc;
    border-top-color:#f50;
    border-radius:100%;

/* here we center it*/
    position:fixed;
    top:0;
    right:0;
    left:0;
    bottom:0;
    margin:auto;
    
/* Tha animation*/
    animation: round 1s linear infinite;
}

@keyframes round {
    from{transform: rotate(0deg)}
    to{transform: rotate(360deg)}
}

#overlay {
    height:100%;
    width:100%;
    background:#000;
    opacity:1;
    left:0;
    top:0;
    z-index:9999;
}
/* pre-loading-----------------------------*/

/*multilevel menu ----------------*/

.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu{
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #777575;
    margin-top: 5px;
    margin-right: -10px;
	
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}

.ddown-menu >li > a {
    color: #666 !important
}
/* ------end menu ----------------*/

</style>
 

<head>
  <meta charset="UTF-8">
  <title>Advance Travels</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"  type="text/css" />
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" >
	
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>"  type="text/css" /> 
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css');?>"  type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css');?>"  type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/jasny-bootstrap.min.css');?>" media="screen">
	   
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css')?>" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css')?>" type="text/css">
  
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/css/jquery-modal.css');?>"  type="text/css" />
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css');?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.css');?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css')?>"   type="text/css" /> 
  
	<link rel="stylesheet" href="<?php echo base_url('assets/common/font-awesome/css/font-awesome.min.css');?>" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url('assets/common/search_tabs/selectize.css');?>" type="text/css">
  
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/css/sweetalert.css');?>" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.css');?>" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dist/css/tab.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/dist/css/select2.min.css');?>" type="text/css" />
	
      </head>
      <body onload="myFunction()" class="skin-green sidebar-mini">
	  
        <div class="wrapper">
          <header class="main-header">
            <a href="#" class="logo">
              <span class="logo-mini" style='color:#5ab1e2;'><b>A<font color='red'>W</font>H</b></span>
              <span class="logo-lg">
			  <h4 style='color:#5ab1e2;'>Advance<font color='red'> World</font> Holidays</h4>
            </a>

            <nav class="navbar navbar-static-top" role="navigation">
			<label id='nts' style='margin:10px 0px 0px 20px;font-size:18px;font-weight:bold;color:#000;'></label>
			<input type='hidden' id='accmes' name='accmes' value='<?php echo $this->session->flashdata('mesacc');?>'>
				
              <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
            <img src="<?php echo base_url('upload/images/logo_dark.png');?>"  height='45px' alt="logo">
			<div class="navbar-custom-menu">
            <ul class="nav navbar-nav ">

	<!--- renewal notification ------------------------------------>
	
		  <?php
		    //$this->load->model('Model_general');
			//$results=$this->Model_general->renewal_documents();
			
			$this->db->select('*');
			$this->db->from('vehicle_documents');
			$this->db->join('vehicle_registration',"vehicle_registration.vehicle_id=vehicle_documents.doc_vehicle_id","inner");
			$this->db->join('vehicle_types',"vehicle_types.veh_type_id=vehicle_documents.doc_vehicle_type","inner");
			$this->db->order_by("vehicle_documents.doc_vehicle_id","asc");
			$query = $this->db->get();
			$results=$query->result();		
			
			$slno=1;
			$cnt=0;
					foreach($results  as $re)   //for renewal count
					{
					$d1=strtotime($re->doc_valid_to);
					$d2=strtotime(date('Y-m-d'));
					$days=floor(($d1-$d2) / (60*60*24));
					if($days<=$re->doc_reminder)
					{
						$cnt++;
					}}  //---end-----

		  ?>
				<li class="dropdown messages-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					  Renewals <!--<i class="fa fa-envelope-o"></i>-->
					  <span class="label label-success" id='recount' ><?=$cnt;?></span>
					</a>
					<ul class="dropdown-menu">
					  <li class="header"><b>You have 4 renewals</b></li>
					  <li style='padding-left:10px;'><table width='100%'><tr style='color:green'><td width='110px'>Vehicle</td><td>Expiry_Date</td><td>Days</td></tr></table></li>
					  <li>
						<!-- inner menu: contains the actual data -->
						<ul class="menu">
						
						<?php
						$slno=0;
						foreach($results  as $re) 
						{
						$dat1=date_format(date_create($re->doc_valid_to),"d-m-Y");	
							
						$d1=strtotime($re->doc_valid_to);
						$d2=strtotime(date('Y-m-d'));
						$days=floor(($d1-$d2) / (60*60*24));
						if($days<=$re->doc_reminder)
						{
							
							if($days<=0)
							{
							$stat="Expired";
							}
							else
							{
								$stat=$days." days";
							}

						?>
						
						  <li><!-- start message -->
							<a href="<?php echo site_url('General/Renewals'); ?>">
							<table width='100%'><tr ><td width='110px'><?=$re->vehicle_regno;?></td><td><?=$dat1;?></td><td style='color:red' align='right'><small><i class="fa fa-clock-o"></i> <?=$stat;?></small></td></tr></table>
							</a>
						  </li>
						  <?php
						  $slno++;
						  }
						  if($slno>3)
							  break;
						  }
						  ?>
						  <!-- end message -->
					   
						</ul>
					  </li>
					  <li class="footer"><a class='renewal' href="<?php echo site_url('General/Renewals'); ?>">See All Renewals</a></li>
					</ul>
				  </li>
				  
		<!--- renewal notification ------------------------------------>
 
                  <li class="dropdown user user-menu"> 
					  
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                         <img src="<?php echo $this->session->userdata('userimage');?>" class="user-image" alt="User Image"> 
						 <span class="hidden-xs"><?php echo $this->session->userdata('name');?></span> </a>
					  
                      <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header" >
                         <img src="<?php echo $this->session->userdata('userimage');?>" class="img-circle" alt="User Image">
                         <p style="color:#000;"><?php echo $this->session->userdata('name');?> </p>
                       </li>
                       
                       <li class="user-footer">
                        <div class="pull-left"> <!--<a href="<?php echo site_url('admin/profile') ?>" class="btn btn-default btn-flat">Profile</a> --> </div>
                        <div class="pull-right"> <a href=" <?php echo site_url('admin/logout') ?>" class="btn btn-default btn-flat">Sign out</a> </div>
                      </li>
                    </ul>
                  </li>
                  
                  <!-- Control Sidebar Toggle Button -->
                  <li style="margin-right:20px;"> </li>
                </ul>
              </div>
  
            </nav>
          </header>
          <!-- Left side column. contains the logo and sidebar -->
          <div class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
              
              <?php
              $lvl="";
              $ul=$this->session->userdata('userlevel');
              if($ul=='1')
              {
               $lvl="Super Admin";
             }
             if($ul=='2')
             {
               $lvl="Administrator";
             }
             if($ul=='3')
             {
               $lvl="User";
             }
             ?>

             <ul class="sidebar-menu" style='margin-top:10px;'>
             <!-- <li class="header">MAIN NAVIGATION</li> -->

              <li class="treeview">
                <a href="<?php echo site_url('Login/home') ?>">
                 <i class="fa fa-group"></i> <span>Dashboard</span>
               </a>
             </li>
				
				<li class="treeview">
                    <a href="">
                        <i class="fa fa-arrow-circle-o-right" style="color:#31b3ff"></i>
                        <span>Accounts</span>
                        <i class="fa fa-angle-down pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
						<li><a href="<?php echo site_url('Investment/Add'); ?>"><i class="fa fa-caret-right"></i>Investments</a></li>
						<li><a href="<?php echo base_url('Ledger/Add');?>"><i class="fa fa-caret-right"></i>Create Ledger</a></li>
						<li><a href="<?php echo base_url('Ledger/View');?>"><i class="fa fa-caret-right"></i>View Ledgers</a></li>
						<li><hr style='margin:2px;border-color:#565656;'></li>
						
						<li><a href="<?php echo site_url('Voucher/receipts'); ?>"><i class="fa fa-caret-right"></i>Receipts</a></li>
						<li><a href="<?php echo base_url('Voucher/payments');?>"><i class="fa fa-caret-right"></i>Payments</a></li>
						<li><hr style='margin:2px;border-color:#565656;'></li>
						
						<li><a href="<?php echo site_url('Account/profit_loss'); ?>"><i class="fa fa-caret-right"></i>Profit & Loss Account</a></li>
						<li><a href="<?php echo site_url('Account/balance_sheet'); ?>"><i class="fa fa-caret-right"></i>Balance sheet</a></li>
						<li><a href="<?php echo site_url('Account/trial_balance'); ?>"><i class="fa fa-caret-right"></i>Trial Balance</a></li>
                    </ul>
                </li>

				<li class="treeview">
                    <a href="">
                        <i class="fa fa-arrow-circle-o-right" style="color:#31b3ff"></i>
                        <span>Master Options </span>
                        <i class="fa fa-angle-down pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
										
					<li><a href="<?php echo site_url('admin/contacts') ?>"><i class="fa fa-caret-right"></i> Corporate Contacts</a></li>
					<li><a href="<?php echo site_url('Asset/index') ?>"><i class="fa fa-caret-right"></i> Asset Details</a></li>

						<li style='color:#9fc8f3;padding-left:10px;'>Documentations</li>
						<li><a href="<?php echo site_url('General/Document'); ?>"><i class="fa fa-caret-right"></i> Add Vehicle Documents</a></li>
						<li><a href="<?php echo site_url('General/Documents'); ?>"><i class="fa fa-caret-right"></i>List Vehicle Documents</a></li>
						<li><a href="<?php echo site_url('General/Renewals'); ?>"><i class="fa fa-caret-right"></i>Renewal Documents</a></li>
						<li style='color:#9fc8f3;padding-left:10px;'>Monthly Targets</li>
                       	<li><a href="<?php echo site_url('General/Targets'); ?>"><i class="fa fa-caret-right"></i>Set Mothly Targets</a></li>
						<li><a href="<?php echo site_url('General/Target_Report'); ?>"><i class="fa fa-caret-right"></i>Mothly Target Report</a></li>
						
						<li style='color:#9fc8f3;padding-left:10px;'>Marketting Reports</li>
                       	<li><a href="<?php echo site_url('General/Marketing'); ?>"><i class="fa fa-caret-right"></i>Add Marketting Reports</a></li>
						<li><a href="<?php echo site_url('General/view_marketing'); ?>"><i class="fa fa-caret-right"></i>View Maketting Business</a></li>
						
						<li style='color:#9fc8f3;padding-left:10px;'>Service Evaluations</li>
                       	<li><a href="<?php echo site_url('Services/Evaluation'); ?>"><i class="fa fa-caret-right"></i>Service Evaluations</a></li>
						<li><a href="<?php echo site_url('Services/Evaluation_list'); ?>"><i class="fa fa-caret-right"></i>View Service Evalauations</a></li>
												
                    </ul>
                </li>
								
				<li class="treeview">
                    <a href="">
                        <i class="fa fa-arrow-circle-o-right" style="color:#31b3ff"></i>
                        <span>Incomes/Expenses</span>
                        <i class="fa fa-angle-down pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
						<li><a href="<?php echo site_url('General/Incomes'); ?>"><i class="fa fa-caret-right"></i>Add Additional Incomes</a></li>
						<li><a href="<?php echo site_url('General/View_Income'); ?>"><i class="fa fa-caret-right"></i>View Additional Incomes</a></li>
						<li><hr style='margin:2px;border-color:#565656;'></li>
						<li><a href="<?php echo site_url('Vehicle/vehicle_expense'); ?>"><i class="fa fa-caret-right"></i>Add Vehicle Expenses</a></li>
						<li><a href="<?php echo site_url('Vehicle/Expenses'); ?>"><i class="fa fa-caret-right"></i>Vehicle Expense List</a></li>
						<li><a href="<?php echo site_url('General/Expenses'); ?>"><i class="fa fa-caret-right"></i>Add General Expenses</a></li>
						<li><a href="<?php echo site_url('General/View_expense'); ?>"><i class="fa fa-caret-right"></i>General Expense List</a></li>
						<li><a href="<?php echo site_url('StaffSalary/Advance'); ?>"><i class="fa fa-caret-right"></i>Advance Salary</a></li>	
						<li><a href="<?php echo site_url('StaffSalary/Salary'); ?>"><i class="fa fa-caret-right"></i>Salary Payments</a></li>	
						<!--<li><a href="<?php echo site_url('Driver/driver_account'); ?>"><i class="fa fa-caret-right"></i>Driver Payments</a></li>-->
                    </ul>
                </li>
				
				<li class="treeview">
                    <a href="">
                        <i class="fa fa-arrow-circle-o-right" style="color:#31b3ff"></i>
                        <span>Purchase/Stock</span>
                        <i class="fa fa-angle-down pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
						<li><a href="<?php echo site_url('Stationary/Purchase_request'); ?>"><i class="fa fa-caret-right"></i>Purchase Request</a></li>
						<li><a href="<?php echo site_url('Vehicle/Expenses'); ?>"><i class="fa fa-caret-right"></i>Vehicle Expense List</a></li>
						<li><a href="<?php echo site_url('Stock/Add'); ?>"><i class="fa fa-caret-right"></i>Add New Stock</a></li>
						<li><a href="<?php echo site_url('Stock/Delivery'); ?>"><i class="fa fa-caret-right"></i>Delivery Stock</a></li>
						<li><a href="<?php echo site_url('StaffSalary/Advance'); ?>"><i class="fa fa-caret-right"></i>Advance Salary</a></li>	
						<li><a href="<?php echo site_url('StaffSalary/Salary'); ?>"><i class="fa fa-caret-right"></i>Salary Payments</a></li>	
						<!--<li><a href="<?php echo site_url('Driver/driver_account'); ?>"><i class="fa fa-caret-right"></i>Driver Payments</a></li>-->
                    </ul>
                </li>
				
			<?php
			$m=1;
			$s=1;
              $staff_id = $this->session->userdata('usercode');
              if($staff_id != "admin"){
                $menus = $this->Model_staff->view_menucategory();
                $cat_ids = $this->Model_menu->get_distinct_menu_cat_id(array("staff_id" => $staff_id));
                foreach($cat_ids as $cat_id){
                  foreach($menus as $menu){
                    if($cat_id->menu_cat_id == $menu->menu_cat_id){

                      $sub_menus = $this->Model_menu->get_menu_link(array('menu_category' => $cat_id->menu_cat_id, "menu_type" => "0"));
                      $user_sub_menus = $this->Model_menu->get_user_menu_options(array('menu_cat_id' => $cat_id->menu_cat_id , "staff_id" => $staff_id));

                      if(count($sub_menus) > 0 ){
                        echo '

                        <li class="treeview" id="mainop'.$m.'">
                          <a href="#">
                            <span>'.$menu->menu_cat_name.'</span>
                            <i class="fa fa-angle-left pull-right"></i>
                          </a>
                          ';
                          echo '<ul class="treeview-menu">';
                          foreach($sub_menus as $sub_menu){
                            foreach($user_sub_menus as $user_sub_menu){
                              if($sub_menu->menu_id == $user_sub_menu->menu_options){
                                echo '
                                <li id="sub'.$s.'" >a href="'.site_url($sub_menu->menu_link).'"><i class="fa fa-caret-right" aria-hidden="true"></i>'.$sub_menu->menu_name.'</a></li>
                                ';
								$s++;
                              }
                            }
                          }     
                        }
                        echo '</ul>';
                        echo '</li>';
						$m++;
                      }

                    }
                  }
                }
                else{
                  $menu_categories = $this->Model_staff->view_menucategory();
                  foreach($menu_categories as $menu_category){
                    $menu_options = $this->Model_menu->get_menu_link(array("menu_category" => $menu_category->menu_cat_id, "menu_type" => "0"));
                    echo '

                    <li class="treeview" id="mainop'.$m.'">
                      <a href="#">
                        <i class="fa fa-arrow-circle-o-right" aria-hidden="true" style="color:#31b3ff"></i>
                        <span>'.$menu_category->menu_cat_name.'</span>
                        <i class="fa fa-angle-down pull-right"></i>
                      </a>
                      <ul class="treeview-menu">
                        ';
                        foreach($menu_options as $menu_option){
                          echo '
                          <li id="sub'.$s.'"><a href="'.site_url($menu_option->menu_link).'"><i class="fa fa-caret-right" aria-hidden="true"></i>'.$menu_option->menu_name.'</a></li>
                          ';
						  $s++;
                        }
                        echo '</ul>';
                        echo '</li>';
						$m++;
                      }
                    }
                    ?>
							
          <li>&nbsp;</li>
 
          </ul>
        </section>
		
		
		
        <!-- /.sidebar -->
      </div>
 