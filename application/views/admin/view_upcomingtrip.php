<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}
</style>
 
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Upcoming Trip Details</b></h1>
   <ol class="breadcrumb" style='font-size:15px;'>
    <li> <a href="<?php echo base_url('Trip/index');?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'>  <i class="fa fa-cab" aria-hidden="true"></i>New Trip Sheet</button></a></li>
		<li><a id='view-pdf' href="<?php echo site_url('Pdf/pdf_UPCtrip') ?>" style='color:#4b88ed;'><button class='btn btn-primary'> <i class="fa fa-print" aria-hidden="true"></i>Print </button></a></li>
		<!-- data-target="#myModal1" --> <li><a id='idsave' href="" data-target="" data-toggle='modal'style='color:#4b88ed;'><button class='btn btn-primary'> <i class="fa fa-save" aria-hidden="true"></i>Save</button></a></li>
		<!-- data-target="#myModal1" --><li><a id='idshare' href="" data-target="" data-toggle='modal' style='color:#4b88ed;'><button class='btn btn-primary'> <i class="fa fa-share-alt" aria-hidden="true"></i>Share</button></a></li>
     </ol> 
 </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header  end -->	  
	  
  <section class="content"> 
        <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
		</div>  
  <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details-->
	
	 <div style="background-color:#fff;border-radius:10px;padding:15px; ">
					
		<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">

				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
								<tr style='color:#4b88ed'>
								 <th>Trip Id</th>
								 <th>Customer</th>
								 <th>Mobile/Phone</th>
								 <th>Driver</th>
								 <th>Date</th>
								 <th>Days</th>
								 <th>Purpose</th>
								 <th>Status</th>
								 </tr>
								</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-body" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Edit</h4>
								</div>
							</div>
							</div>
						<!-- /.modal-dialog -->
						</div>
                   </div>			

           <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>

		
            <!-- /.modal-dialog -->
            </div>
                   </div>  

           <div class="modal fade draggable-modal" id="myModals" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Share</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
          </div>     
        </div>       

				
	</div>
	<!-- End user details -->

    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------

 setTimeout(function(){ $("#idh5").html(""); }, 3000);
 
      $('#example').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
		'scrollX':true,
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/trip_UPCajax",// json datasource
               },
			   "columnDefs":[
			   {"width":"8%","targets":0},
			   {"width":"10%","targets":2},
			   {"width":"10%","targets":4},
			   {"width":"5%","targets":5},
			   {"width":"7%","targets":7},
			   ],
			   
        "columns": [
			{ "data": "tripid" },
            { "data": "customer"},
			{ "data": "custphone"},
			{ "data": "driver"},
            { "data": "date" },
			{ "data": "days" },
			{ "data": "purpose" },
			{ "data": "status" },
       ]
  } );

var table = $('#example').DataTable();
  //    $('#example tbody').on( 'click', 'tr', function () {
  //       if ( $(this).hasClass('selected') ) {
  //           $(this).removeClass('selected');
  //       }
  //       else {
	 //        table.$('tr.selected').removeClass('selected');
  //           $(this).addClass('selected');
  //       }
  // });			   

  
  
  $('#view-pdf').on('click',function(){
	  
        var pdf_link = $(this).attr('href');
	    //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
        return false; 

    });    

  
      $('#idedit').click(function () {
        var id=$('#example').find('tr.selected').find('td').eq(1).text();
		alert(id);
		 if(id=="")
		 {
			 alert ("please Select staff details..");
		 }
		 else
		 {
	
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/edit_trip/"+id,
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
			
                   }
            });
		 }
        }); 

		
  $('#idview').click(function ()
  {
        var Result=$("#myModal1 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(id=="")
		 {
			 alert ("please Select staff details..");
		 }
		 else
		 {
			$("#idview").attr('data-target','#myModal1') 
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/trip_details",
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
		 }
        }); 		


       $('#idshare').click(function () {
        var Result=$("#myModals .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
   //      var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 // if(id=="")
		 // {
			//  alert ("please Select staff details..");
		 // }
		
			$("#idshare").attr('data-target','#myModals') 
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/share_utrip_details",
        dataType: 'html',
        //data: {id: id},
        success: function(res) {
			 Result.html(res);
                    }
            });
		 //}
    
        });  

  $('#idsave').click(function ()
    {   
         // var id=$('#example').find('tr.selected').find('td').eq(1).text();
         // if(id=="")
         // {
         //     alert ("please Select trip details..");
         // }
         // else
         // {
            jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "Trip/save_utrip",
            dataType: 'html',
            //data:{tid:id},
            success: function(res) {
				alert("Trip sheet saved...!");
             }
            });
         //}
        }); 
		
		
		
		
		
		
	$('#iddelete').click(function () 
    {
    var res=confirm("Delete this coustomer details?");
    if(res)
    {
    var id=$('#example').find('tr.selected').find('td').eq(1).text();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/del_entry",
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    }
        });     
	
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });
 //});
  


</script>



