<?php
class Model_userprofile extends CI_Model{
	
    public function form_insert($userdt)
    {
       $this->db->insert('users', $userdt);  
    }

    public function view_userprofiles(){

    $this->db->select('*');
    $this->db->from('users');
    $query = $this->db->get();
    return $query->result();
}

  public function update_user_profile($id,$usr_newdt)
  {
	$this->db->where('user_id', $id);
	$this->db->update('users', $usr_newdt); 
  } 
 
 public function update_user_pass($uid,$usr_dt)
  {
	$this->db->where('user_id', $uid);
	$this->db->update('users', $usr_dt); 
  } 
 
 
 
public function delete_userprofile($id)
{
    $this->db->where('user_id', $id);
    $this->db->delete('users');
    return $this->db->affected_rows();
}

/*public function can_login($username,$password)
{
$userna=$username;
$pass=$password;
 
	    $this->db->select('count(*) as cnt1');
		$this->db->from('users');
		$this->db->where('username',$userna);
		$this->db->where('password',$pass);
		$this->db->where('status',"1");
		$query=$this->db->get();
		$res1=$query->row();
	    
		if($res1->cnt1==0)
		{
			return false;
		}
		else
		{
		
   		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('username',$userna);
		$this->db->where('password',$pass);
		$this->db->where('status',"1");
		$query=$this->db->get();
		return $query;
		}
 
}*/
}