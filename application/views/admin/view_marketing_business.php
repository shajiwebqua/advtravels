<?php
 include('application/views/common/header.php');
 ?>
<style>

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
 <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <b>Business Marketing Details</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px;'>
		 <li><a href="<?php echo base_url('General/Marketing');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-plus" aria-hidden="true"></i> Add Marketing Reports</button></a></li>
		 <li><a id='view-pdf' href="<?php echo base_url('Reports/marketing');?>" style='color:#4b88ed;'><button style='padding:2px 20px 2px 20px;'><i class="fa fa-print" aria-hidden="true"></i> Print</button></a></li>
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
              
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
   <div class="page-content">
  <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
<?php
		$dtab=$this->session->userdata('doctab');
?>

	<div style="background-color:#fff;padding:3px 10px 3px 10px;">
		
		
	<div class='row' style=' background-color:#e4e4e4;margin:0px;padding:5px;margin-top:10px;'>
	<form onsubmit='return checkdata1();' method="POST" action="<?php echo base_url('General/View_marketing/1')?>" enctype="multipart/form-data">
	<label class='col-md-2 la-right'>Select Date:</label>
	<div class='col-md-3' style='padding:0px;'>
			<div class='row'>
				<div class='col-md-6' >
				<input class='form-control' data-provide="datepicker" id="datepicker1" name='fdate'>
				</div><div class='col-md-6' >
				<input class='form-control' data-provide="datepicker" id="datepicker2" name='sdate'>
				</div>
			</div>
	</div>
	
	<div class='col-md-1' style='padding:2px;margin-left:10px;'>
	<button type='submit' name='btnget1' >Get</button>
	</div>
	</form>
	<form onsubmit='return checkdata2();' method="POST" action="<?php echo base_url('General/View_marketing/2')?>" enctype="multipart/form-data">
	<label class='col-md-1 la-right' >Month:</label>
	<div class='col-md-2'>
		<select name='mon' id='mon' class='form-control'>
		  <option value=''> ------------</option>
		  <option value='1'> January</option>
		  <option value='2'> February</option>
		  <option value='3'> March</option>
		  <option value='4'> April</option>
		  <option value='5'> May</option>
		  <option value='6'> June</option>
		  <option value='7'> July</option>
		  <option value='8'> August</option>
		  <option value='9'> September</option>
		  <option value='10'> October</option>
		  <option value='11'> Novenber</option>
		  <option value='12'> December</option>
		  </select>
	</div>
	<div class='col-md-1' style='padding:0px 2px;'>
	<select name='syear' id='syear' class='form-control'>
	 <option value='' >-year-</option>
		<?php
		$y=date('Y')+1;
		for($x=$y;$x>=2017;$x--)
		{
		?>
		<option value='<?=$x;?>' <?php if($x==date('Y')) echo "selected";?> ><?=$x;?></option>
		<?php
		}
		?>
	</select>
	</div>
	
	<div class='col-md-1' style='padding:2px;'>
	<button type='submit' name='btnget2' >Get</button>
	</div>
	</form>
	</div>
		
			<div class='row'>
			  <div class='col-md-12'> <h5>Busines Details of <?php echo $sop;?></h5> </div>
            </div>
			
			 <div class='row'>
			<hr style='margin:0px 0px 15px 0px;'>
			</div>
			
				<div class='row' style='padding-left:15px; padding-right:15px;' >
				
				<table class="table table-striped table-hover table-bordered" id="example" width='100%'>
                <thead>
                <tr>
					 <th>Action</th>
					 <th>Date</th>
					 <th>Staff_Name</th>
					 <th>Customer</th>
					 <th>Business</th>
					 <th>Follow_up</th>
                </tr>
                </thead>
				
				 <tbody>
				 <?php
				 if(isset($marres))
				 {
				 foreach($marres as $mr)
				 {
				 if($mr->mar_cust_group=='1')
				 {
					 $cgroup="INDIVIDUAL";
				 }					 
				 ELSE
				 {
					 $cgroup="CORPORATE";
				 }
				
				$mm='<button class="btn btn-warning btn-xs">Delete</button>';
				$del=anchor('General/del_marketing/'.$mr->mar_id, $mm, array('id' =>'del_conf'));
            
					 
				 ?>
                <tr>
					 <td>
					 <?=$del;?>
					 </td>
					 <td width='80px;'><?=$mr->mar_edate;?></td>
					 <td width='120px;'><b>Staff: </b><?=$mr->staff_name;?><br><?=$mr->profession_name;?></td>
					 
					 <td><b>Type:&nbsp; </b><?=$cgroup;?>
					 <br><b>Name:&nbsp; </b><?=$mr->mar_cust_name;?>
					 <br><b>Address:&nbsp; </b><?=$mr->mar_cust_address;?>
					 <br><label style='color:blue;font-size:14px;margin-top:2px;'>---------Contact Person---------</label>
					 <br><b>Name:&nbsp; </b><?=$mr->mar_cont_person;?>
					 <br><b>Desig:&nbsp; </b><?=$mr->mar_designation;?>
					 <br><b>Mob:&nbsp; </b><?=$mr->mar_cont_number;?></td>
					 
					 <td><p><b>Business:&nbsp; </b><?=$mr->mar_business;?></p>
					 <p><b>Discussion:&nbsp; </b><?=$mr->mar_discussion;?></p>
					 <p><b>Time Start:&nbsp; </b><?=$mr->mar_start_time;?> &nbsp;&nbsp;&nbsp; <b>End: </b><?=$mr->mar_end_time;?></p>
					 <p><b>Reponse:&nbsp;</b> <?=$mr->mar_res_status;?></p>
					 <p><b>Steps-Taken:&nbsp;</b> <?=$mr->mar_business_steps;?></p></td>
					 
					 <td><?=$mr->mar_followup_date;?></td>
                </tr>
				<?php
				 }}
				 ?>
                </tbody>

                </table>
				</div>
				
				
				
				
			</div>
   
   <!-- END BORDERED TABLE PORTLET-->

	</div>
</div>

</section>
</div>
<!-- modal windows --->

        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >

              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
		 
<!--- End -------------------------->

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
  
  $('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    autoclose:true,
	
});
   $('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});
 
 $('#example').dataTable( {});
  
  // get_dataTable("0");
 // disable();

 $('#timepicker1').timepicker();
 $('#timepicker2').timepicker();

$("#staffna").change(function()
{
	var sna=$(".staffna").val();
	 jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/get_staff_designation",
        dataType: 'html',
        data: {sna:sna},
        success: function(res) 
		{
        $pr=res.split(',');
		$("#stprofid").val($pr[0]);
		$("#stdesig").val($pr[1]);
				
        }
	 });
		
});


$('#view-pdf').on('click',function(){
     
		var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        $.createModal({
        title:'Monthly Targets',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
	   return false; 

    });  
 

    
    
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });
</script>
