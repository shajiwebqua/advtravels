<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Enquery List</b> </h1>
    <ol class="breadcrumb" style='font-size:15px;'>
	<li><a  href="<?php echo site_url('Enquiry/index') ?>" style='color:#4b88ed;'><i class="fa fa-cab" aria-hidden="true"></i>Add Enquiry</a></li>
        <li><a id='view-pdf' href="<?php echo site_url('Pdf/enquery') ?>" style='color:#4b88ed;'><i class="fa fa-print" aria-hidden="true"></i>Print</a></li>
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 	  
  <section class="content"> 
<!--   <div style="color:blue;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Print,Share,Save,View,Edit,Delate)  </div> -->
			<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
  
    <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!------------------------------------ Add new user details ----------------------------------->

<div style="background-color:#fff;padding:15px; ">
							
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
 
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                      <div class="row" >
                      <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
					  <table class="table table-striped table-hover table-bordered" id="example" width='100%'>
                      <thead>
								<tr >
								 <th>Delete</th>
								 <th>SL/NO</th>
								 <th>Party Name</th>
								 <th>Phone No</th>
								 <th>details of journy</th>
								 <th>Nature of Journy</th>
								 <th>Prefered Vehicle</th>
								 <th>Starting Date & Time</th>
								 <th>Returning Date & Time</th>
								 <th>Start From</th>
								 <th>Amount</th>
								 <th>upto Km</th>
								 <th>Per.Km</th>
								 <th>Permit</th>
								 </tr>
								</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
         <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
			  </div>
                   </div> 

                    <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>           


					<div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div> 

                   <div class="modal fade draggable-modal" id="myModalsh" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Share</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>                   

				   
        
  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 
 

//sweet alert ----------------------------
	$("#idmsg").hide();
	if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
// sweet alert box -------------------------- 
 
 
 
      $('#example').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
		"scrollX":true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Enquiry/order_ajax",
               
               },
		"columnDefs":[
		{"width":"5%","targets":0},
		{"width":"8%","targets":1},
		{"width":"8%","targets":2},
		],
			   
        "columns": [
            { "data": "delete" },
            { "data": "sl_no"},
            { "data": "name"},
            { "data": "phno"},
			{ "data": "journey"},
      		{ "data": "nature"},
            { "data": "vehicle"},
            { "data": "start_date" },
            { "data": "return_date" },
			{ "data": "start_from" },
            { "data": "amount" },
			{ "data": "upto_km" },
			{ "data": "per_km" },
			{ "data": "permit" },
        ]
    });
			   
 var table = $('#example').DataTable();
      $('#example tbody').on( 'click', 'tr', function () {
         if ( $(this).hasClass('selected') ) {
             $(this).removeClass('selected');
         }
         else {
 	        table.$('tr.selected').removeClass('selected');
             $(this).addClass('selected');
         }
   });			    

  	
     $('#idview').click(function () 
     {
      var Result=$("#myModal2 .modal-body");
      var slno=$('#example').find('tr.selected').find('span.sln').text();
      if(slno=="")
      {
       alert ("please Select Order details..");
     }
     else
     {
      $('#idview').attr('data-target','#myModal2'); 

      jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Order/show_order",
        dataType: 'html',
        data: {slno: slno},
        success: function(res) {
          Result.html(res);
        }
      });
    }
  });

     $('#view-pdf').on('click',function(){
       var slno=$('#example').find('tr.selected').find('span.sln').text();
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
    var pdf_link = $(this).attr('href')+'/'+slno;
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Given Enquiry',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
        return false
    });

    $(document).on("click", "#del_enq", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

 //});
  


</script>



