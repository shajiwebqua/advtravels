<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Vehicles List</b></h1>
    <ol class="breadcrumb" style='font-size:15px;'>
	<li><a  href="<?php echo base_url('Vehicle/addvehicle');?>"  style='color:#4b88ed;'><i class="fa fa-print" aria-hidden="true"></i>New Vehicle</a></li>
  	<li><a  id="view-pdf" href="<?php echo base_url('Pdf/pdf_vehicle');?>"  style='color:#4b88ed;'><i class="fa fa-print" aria-hidden="true"></i>Print/Save</a></li>
	
	</ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 	  
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
	<div style="padding:2px 0px 2px 0px;">
	   <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
		  <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div></center>
	   </div>
	</div>
	
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details -->
	
	 <div style="background-color:#fff;padding:15px; ">
		
							
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                 <CENTER>
                    <h5 style="color:green;">
                       <?php echo $this->session->flashdata('usermsg'); ?>
                    </h5>
                </CENTER>
				
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
								
					<table class="table table-striped table-hover table-bordered" id="example" width='100%'>
                        <thead>
						 	<tr>
							  <th>Action</th>
							  <th>ID</th>
							  <th>Date</th>
							  <th>Vehicle Reg. No</th>
							  <th>Modal</th>
							  <th>Seats</th>
							  <th>Price</th>
							  <th>Status</th>
							  <th>Availability</th>
							  <th>Report</th>
							</tr>
						</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Edit</h4>
								</div>
							</div>
							</div>
						<!-- /.modal-dialog -->
						</div>
                   </div>	

                    <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>       						
				
	</div>
	<!-- End user details -->
	</div>
	
	</div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>



<script type="text/javascript">


//sweet alert ----------------------------
	$("#idmsg").hide();
	if($("#idh5").html()!="")
	{
		swal( $("#idh5").html(),"","success")
		$("#idh5").html("");
	}
// sweet alert box -------------------------- 

 
      $('#example').DataTable({
		"ordering":false,
        "destroy": true,
        "processing": true,
        
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Vehicle/vehicle_ajax",// json datasource
                },
			   "columnDefs":[
					{"width":"8%","targets":0},
					{"width":"20%","targets":3},
					{"width":"20%","targets":4},
				],
			   
        "columns": [
            { "data": "action1"},
            { "data": "vid" },
			{ "data": "date" },
			{ "data": "regno" },
			{ "data": "vtype" },
			{ "data": "seats" },
			{ "data": "vamt" },
			{ "data": "status" },
			{ "data": "available" },
			{ "data": "report" },
      ]	
});
            
 /*var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });	*/		

$('#example tbody').on('click', '.add', function () {
        var Result=$("#myModal1 .modal-body");
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "vehicle/add_vehicle",
        dataType: 'html',
        // data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 

      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "vehicle/edit_vehicle/1",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 


  $('#example tbody').on('click', '.change', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "userprofile/change_password",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 		

		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

 //});
  
 $('#view-pdf').on('click',function(){
     //   var id=$('#example').find('tr.selected').find('td').eq(1).text();
     // if(id=="")
     // {
     //   alert ("please Select customer details..");
     // }
     // else
     // {
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
    var pdf_link = $(this).attr('href');
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Vehicle Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
     return false; 

    });   


 $(document).on("click", "#change_conf", function () {
            return confirm('Are you sure Change Vehicle Available Status?');
        });	

</script>



