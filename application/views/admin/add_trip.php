<?php
 include('application/views/common/header.php');
 ?>
<!-- <link rel="stylesheet" href="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.css');?>" rel="stylesheet" type="text/css" />
 <script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
 <script src="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.js');?>"></script> -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/bootstrap/bootstrap.min.css');?>">-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/font-awesome/css/font-awesome.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/search_tabs/selectize.css');?>">
  
 
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.ttext
{
	background-color:#fff;
	font-weight:bold;
	color:Green;
}

.search-btn
{
	padding-top:3px;
	z-index:1000;
	right:17px;
	position:absolute;
}

.select_btn1 ,.select_btn2{
  
    position: absolute;
    top: 3px;
    z-index: 999;
   
}
.select_btn1{
	 right: 19px;
}
.select_btn2 {
    right: 19px;
}

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
    
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>New Trip Sheet</b></h1>
    <ol class="breadcrumb">
   <li> <a href="<?php echo base_url('Trip/view_trip');?>" style="color:#4b88ed;font-size:15px;"> <i class="fa fa-backward" aria-hidden="true"></i>Back to Trip List</a></li>
    </ol> 
    </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
  <!-- Content Header end -->
  <section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg'style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id="msg"><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
  
    <!-- Small boxes (Stat box) -->
    <div class="row">
  <div class="col-md-12">

  <!--Add new user details -->
<div style="background-color:#fff;border-radius:10px;padding:15px; ">
    
   <!--   <div class="portlet-title">
      <div class="caption">
        <div style="color:blue;" >
            Trip Details                   
        </div> 
        </div>
       </div> -->
              
			  
 <div class="portlet-body form">
    <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Trip/add_new_trip')?>" enctype="multipart/form-data"  onsubmit='return checkdata();'>

        <div class="row">
        <div class="col-md-6" style="border-right:1px solid #e4e4e4;">             
        
<!----place control here -->


<!--	<div class="form-group">
      <label for="basic" class="col-lg-2 control-label">Search Here</label>
      <div class="col-lg-10">
        <select id="basic" class="selectpicker show-tick form-control" data-live-search="true">
		<option value=""></option>
          <?php
		$cquery=$this->db->select("customer_name")->from('customers')->get()->result();
		foreach( $cquery as $r)
		{
		   echo "<option value='".$r->customer_id."'>".$r->customer_name."</option>";
		}
		?>
		 <option>cow</option>
          <option data-subtext="option subtext">bull</option>
          <option class="get-class" disabled>ox</option>
          <optgroup label="test" data-subtext="optgroup subtext">
            <option>ASD</option>
            <option selected>Bla</option>
            <option>Ble</option>
          </optgroup> 
		  </select>
      </div>
    </div>-->
<!---------------------------------->
          <div class='form-group'>
                    <!-- <div class='row'> -->
          <label class='col-md-3 control-label' style='padding-top:2px;'>Customer Name :</label>
           <div class='col-md-8'>
					  <section class="form-control " style='padding:0px;'>
						<select id="id2" class="form-controll_prop" placeholder="Choose Your Name" name="custid">
						  <option value=""></option>
						  <?php
							$cquery=$this->db->select("customer_name,customer_id")->from('customers')->get()->result();
							foreach( $cquery as $r)
							{
								echo "<option value='".$r->customer_id."'>".$r->customer_name."</option>";
							}
						  ?>
											  
						</select>
						
						</section>
					    <div class="select_btn1">                               <!-- </div> -->
					    <a href="<?php echo base_url('Customer/add_tripcustomer/2');?>" data-target="#myModal2" data-toggle='modal'><button id='custadd' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span></button></a>
					    </div>	
	 				<!-- <div class="select_btn2">                              
						<a href="" data-target="#myModalc" data-toggle='modal'><button id='custsearch' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button></a>
					 </div>  -->
 				  </div>
         </div>
		 
		  <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Customer Mobile : </label>
          <div class='col-md-8'>
          <input type='text' class='form-control' name='cmobile' id='idcmobile' required >
          </div>
          </div>
		 
          <div class='form-group'>
          <label class='col-md-3  control-label' style='padding-top:5px;'>Purpose : </label>
          <div class='col-md-8'>
          <input type='text' class='form-control' name='purpose' required>
          </div>
          </div>
		<div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Date Start-End : </label>
          <div class='col-md-4'>
          <input type='date' class='form-control' name='startdate' id='startdate' required>
          </div>
          <div class='col-md-4'>
          <input type='date' class='form-control' name='enddate' id='enddate' required>
          </div>
       </div>
	   
	    <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Time Start-End : </label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='starttime' >
          </div>
			<div class='col-md-4'>
          <input type='text' class='form-control' name='endtime' >
          </div>
        </div>
		  
     <!--     <div class='form-group'>
      
          <label class='col-md-3 control-label ' >Vehicle : </label>
          <div class='col-md-6' id="regs1">
		  <input type='text' class='form-control' name='ehicle' id='vregno' required>
                                                
          </div>
			<div style="padding-top:3px;"> 
			<a href="" data-target="#myModals" data-toggle='modal' ><button id='vehiclesearch' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button></a>
			</div>
          </div>

      <div class='form-group'>
                  
		<label class='col-md-3 control-label ' >Driver Name : </label>
        <div class='col-md-6'>
		<input type='text' class='form-control' name='driver' id='driver' required>
		<input type='hidden' class='form-control' name='driver' id='driverid' value=''>
        </div>
		   <div style="padding-top:3px;"> 
		   <a href="" data-target="#myModald" data-toggle='modal' ><button  id='driversearch' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button></a>
		   </div>
                                     
        </div> -->
		
    </div>
            
       <div class="col-md-6">       
	   
        <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Trip From :</label>
          <div class='col-md-8'>
          <input type='text' class='form-control' name='from' >
          </div>
        </div>
      
        <div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:5px;'>Trip To : </label>
          <div class='col-md-8'>
          <input type='text' class='form-control' name='to' id="to" >
          </div>
        </div>
		
	  <div class='form-group'>
                    <!-- <div class='row'> -->
          <label class='col-md-3 control-label ' >Vehicle : </label>
          <div class='col-md-8' id="regs1">
		   <div class='search-btn'> 
			<a href="" data-target="#myModals" data-toggle='modal' ><button id='vehiclesearch' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button></a>
		  </div>
		  <input type='text' class='form-control' name='vehicle' id='vregno' required>
		  <input type='hidden' class='form-control' name='vehicleid' id='vehicleid' >
		 
          </div>
			
          </div>
		
		<div class='form-group'>
                    <!-- <div class='row'> -->
		<label class='col-md-3 control-label ' >Driver Name : </label>
        <div class='col-md-8'>
		 <div class='search-btn'> 
		   <a id='driversearch' data-target="" data-toggle='modal' ><button  class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button></a>
		   </div>
		<input type='text' class='form-control' name='driver' id='driver' required>
	 	<input type='hidden' class='form-control' name='driverid' id='driverid' value=''> 
        </div>
       <!-- </div> -->
        </div>
		
     
    <!--  <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Date Start-End : </label>
          <div class='col-md-4'>
          <input type='date' class='form-control' name='startdate' id='startdate' required>
          </div>
          <div class='col-md-4'>
          <input type='date' class='form-control' name='enddate' id='enddate' required>
          </div>
       </div>
      
      <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Time Start-End : </label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='starttime' required>
          </div>
      <div class='col-md-4'>
          <input type='text' class='form-control' name='endtime' required>
          </div>
          </div>  -->
      
      <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>KM Start-End : </label>
          <div class='col-md-5'>
         
		  <div class="search-btn" style="margin-top:-1px;"> 
		   <a href="" id="btncheck" data-target="" data-toggle="modal"><button>Check</button></a>
		   </div>
		    <input type='number' class='form-control' name='startkm' id='startkm'>
          </div>
      <div class='col-md-3'>
          <input type='number' class='form-control' name='endkm' id='endkm' >
          </div>
          </div>
          </div>
</div>

<label style="background-color:#cecece;width:100%;height:1px;"></label>
<div class='row'>
 <div class="col-md-6" style="border-right:1px solid #e4e4e4;">
 <label class='col-xs-4 control-label' style="text-align:left;">Customer Behavior : </label>
 <div class='col-md-8' style="text-align:left;">
 <textarea rows=2 cols=30 class='form-control' name='cbehavior' id='idbehavior'style="padding:0px;" >Nil</textarea>
 </div>
 </div>
 
 <div class="col-md-6">
 <label class='col-md-3 control-label'>Min.Charge KM : </label>
 <div class='col-md-2'>
 <input type='number' class='form-control' name='minckm' id='idminckm' >
 </div>
 <div class="col-md-1" >
 <input type='button' class='btn btn-info' style="height:30px;width:50px;"  name='btncal' id='idbtnget' value='Get' >
 </div>
 
 <label class='col-md-3 control-label' >Running KM : </label>
 <div class='col-md-3'>
 <input type='number' class='form-control ttext'  style="background-color:#fff;" name='runningkm' id='runningkm' value="0" readonly>
 </div>
 </div>
 
</div>


<label style="background-color:#cecece;width:100%;height:1px;"></label>

<div class="row" style="background-color:#ededed;">
<div class='col-md-6' style="border-right:2px solid #fff; margin:10px 0px 10px 0px;">
<div class='row'>
<div class="col-md-11" style="color:blue;">Minimum Charges :</div>
<div class="col-md-4">Min. Days:</div>
<div class="col-md-4">Min.Charge :</div>
<div class="col-md-4" ><b>Total :</b></div>
</div>
<div class='row'>
<div class="col-md-4"><input type='number' class='form-control ' name='mindays' id='mindays'></div>
<div class="col-md-4"><input type='number' class='form-control ' name='mincharge' id='mincharge'></div>
<div class="col-md-4"><input type='number' class='form-control ttext' style='background-color:#fff;'   name='mintotal' id='mintotal' readonly></div>
</div>

<div class='row' >
    <div class="col-md-11" style="color:blue;">Additional Charges :</div>
</div>
<div class='row'>
<div class="col-md-4">Addi. Km:</div>
<div class="col-md-4">KM.Charge :</div>
<div class="col-md-4"><b>Total :</b></div>
</div>
<div class='row'>
<div class="col-md-4"><input type='number' class='form-control ' name='addikm' id='addikm'></div>
<div class="col-md-4"><input type='number' class='form-control ' name='addikmcharge' id='addikmcharge'></div>
<div class="col-md-4" ><input type='number' class='form-control ttext' style="background-color:#fff;"  name='additotal' id='additotal' readonly></div>
</div>
<div class='row'  style="padding-top:10px;">
<div class="col-md-8"  style='text-align:right;'><label class='control-label'><b>Total Trip Charge : </b></div>
<div class="col-md-4"><input type='number' class='form-control ttext' style="background-color:#fff;"  name='triptotal' id='triptotal' readonly></div>
</div>

</div>

<div class='col-md-6' style="margin:10px 0px 10px 0px;">
<div class='row'>
    <div class="col-md-11" style="color:blue;">Other Charges :</div>
</div>

<div class='row'>
<div class="col-md-4">Toll/Parking :</div>
<div class="col-md-4">Interstate Permit :</div>
<div class="col-md-4">Other Charges :</div>
</div>
<div class='row'>
<div class="col-md-4"><input type='number' class='form-control tx' name='tollparking' id='tollparking' value="0" ></div>
<div class="col-md-4"> <input type='number' class='form-control tx' name='ispermit' id='ispermit' value="0"></div>
<div class="col-md-4"><input type='number' class='form-control tx' name='ocharge' id='ocharge' value="0"></div>
</div>

<div class='row' style="padding-top:5px;">
<div class="col-md-4" style="text-align:right;">Other Charges Description</div>
<div class="col-md-8"><textarea rows=2 class='form-control ' name='otherdesc' id='otherdesc'>Nil</textarea></div>
</div>

<div class='row' style="padding-top:20px;">

<label class="col-md-4 control-label" style="font-size:15px;color:blue;text-align:right;">Grand Total: </label> 
<div class="col-md-4"><input type='number' class='form-control ttext' style="background-color:#fff;" name='gtotal' id='gtotal' value="0" readonly></div>
<div class="col-md-4" ><input type='button' class='btn btn-info'   style="height:30px;width:150px;"  name='btncal' id='idbtncalc'value='Calculate' ></div>
</div>

</div>
</div>
<!-- ---------------------- -->
</div>

<label style="background-color:#cecece;width:100%;height:1px;"></label>

 <!-- display grand total -------------------------------------------------------------->
    <div class="row">
	<div class='col-md-6'>
		 <div class="row" style="padding-top:10px;">
			<label class="col-md-4 control-label" >Addition Information</label>
		 <div class="col-md-7"><textarea rows='3' cols='70' name='addiinfo'>Nil</textarea></div>
			
		</div>
	</div>
	
    <div class='col-md-6'>
		  <div class="row" style="padding-top:20px;">
		  <label class="col-md-6 control-label" style="font-size:30px; text-align:right;color:blue;">RS : </label> 
		  <label id="lblgtotal" class="col-md-5 control-label" style="font-size:30px; text-align:left;color:blue;">0.00</label>
		  </div>  
	</div>
   </div>
<!--  ------------------------------------------------------------ -->
	
<label style="background-color:#cecece;width:100%;height:1px;"></label>
<div class='form-group'>
<div class='row'>
	<center><button type="submit" class="btn btn-success" style="padding:10px 50px 10px 50px;" id="idsavebtn" > <b>Save Trip Details</b></button></center>
</div>
</div>
 </div>

 <label style="background-color:#cecece;width:100%;height:1px;"></label>
 
 </form>
 </div>
   
 
   <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                       </div>
   </div>  
   
 
	<div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                       </div>
    </div>   

 <!--- customer List -----> 
  
<!--<div class="modal fade" id="myModalc" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Customer List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class="max-width">
                <table id="TblCustomer" class="table table-striped table-hover table-bordered" cellspacing="0">
				<thead>
            <tr>
                <th>Select</th>
                <th>Customer Name</th>
                <th>Customer Mobile</th>
              </tr>
        </thead>
    </table>
            </div>
        </div>


    </div>
</div>
</div>
</div> -->

 <!--- Vehicles List -->  
  
<div class="modal fade" id="myModals" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Vehicles List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class=" max-width">
                <table id="TblVehicle" class="table table-striped table-hover table-bordered" cellspacing="0">
				<thead>
            <tr>
                <th>Select</th>
                <th>Registration No</th>
                <th>Vehicle Type</th>
                <th>No Of Seats</th>
				<th>Status</th>
            </tr>
        </thead>
    </table>
            </div>
        </div>


    </div>
</div>
</div>
</div>

<!--- Drivers List --> 


<div class="modal fade" id="myModald" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Drivers List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class="col-md-12">
            <table id="TblDriver" class="table table-striped table-hover table-bordered" cellspacing="0">
            <thead>
            <tr>
                <th>Select</th>
				<th>ID</th>
                <th>Driver Name</th>
                <th>Mobile</th>
				<th>Status</th>
            </tr>
        </thead>
      
		<!--	  <tbody>
        <?php  foreach($results2  as $r) 
               {   ?>

              <?php $edit="<a href='#myModald' id='$r->driver_id'  class='select open-AddBookDialog2'>Select</a>";?>
            <tr>
                <td><?=$edit;?></td>
                <td><?=$r->driver_name;?></td>
                <td><?=$r->driver_mobile;?></td>
                
                <?php if($r->driver_status==1)
                {
                 $st="<font color='green'>Available</font>";
                }
                else
                {
                 $st="<font color='red'>Not Available</font>";
                } ?>
                <td><?=$st;?></td>
                
            </tr>
            <?php } ?>
           
        </tbody> -->
		
    </table>
        </div>
      </div>
 	 </div>
 	</div>
   </div>
     
  </div>
  <!------------------------------------->
  
  <div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria- hidden="true">
	    <div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Check Kilometer</h4>
				</div>
			</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
   </div>  
   
  
 </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 

</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//-- driver list
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------


$('#btncheck').click(function () {
	var Result=$("#myModal3 .modal-body");
	var km=$("#startkm").val();
	var vid=$("#vehicleid").val();
	
	if (vid=='')
	{
		swal("Please select vehicle...");
		//alert("Please select vehicle...");	
	}
	else if(km=='')
	{
		//alert("Please type stating kilometer...");	
		swal("Please type starting kilometer.");
	}
	else
	{
		$("#btncheck").attr("data-target","#myModal3");
	    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/check_kilometer",
        dataType: 'html',
        data: {skm:km,vid:vid},
        success: function(res) {
        Result.html(res);
        }
		});
	}
});
//-----------------------------------------


$("#id2").change(function()
{
	    var id =  $("#id2").val();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_cust_mobile",
        dataType: 'html',
        data: {cid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#idcmobile").val(obj.mobile);
		       }
            });
});

//drop down script .................
//..................................

$('#TblDriver').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/driver_ajax/0",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"7%","targets":1},
		],
			   
        "columns": [
            { "data": "select"},
			{ "data": "drid"},
		    { "data": "name"},
            { "data": "mobile"},
			{ "data": "status"},
       ]
  } );


$("#driversearch").click(function()
{
	var sdate=$("#startdate").val();
	var edate=$("#enddate").val();
  //alert(sdate);
	
	if(sdate=="" || edate=="")
	{
		alert("please select trp starting date and Ending date.")
	}
	else
	{
		$("#driversearch").attr("data-target","#myModald");
		$('#TblDriver').dataTable({
			 destroy: true,
			"processing": true,
			"ajax": {
					url :"<?php echo base_url(); ?>" + "Trip/driver_ajax/"+sdate+"/"+edate,// json datasource
				   },
			"columnDefs":[
			{"width":"6%","targets":0},
			{"width":"7%","targets":1},
			],
				   
			"columns": [
				{ "data": "select"},
				{ "data": "drid"},
				{ "data": "name"},
				{ "data": "mobile" },
				{ "data": "status"},
		   ]
		} );
	}
});
  //-- vehicle List

  $('#TblVehicle').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/vehicle_ajax",
				
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		],
			   
        "columns": [
            { "data": "select"},
		    { "data": "regno"},
            { "data": "vtype" },
			{ "data": "noofseats" },
			{ "data": "status" },
       ]
  } );

  //Customer list

 /* $('#TblCustomer').dataTable({
         destroy: true,
        "processing": true,
        "ajax":{
                url :"<?php echo base_url(); ?>" + "Trip/customer_ajax",// json datasource
				
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		],
			   
        "columns": [
            { "data": "select"},
		    { "data": "cname"},
            { "data": "cmobile"},
       ]
  } ); */
   
  
  // selection ---- vechicle and driver -------------------

  $('#TblDriver tbody').on('click', '.drselect', function () {
  
	    var id =  $(this).attr('id');
         jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_driverDT",
        dataType: 'html',
        data: {did: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#driver").val(obj.name);
				$("#driverid").val(obj.driverid);
		       }
            });
        }); 
  
  
 
 $('#TblVehicle tbody').on('click', '.vselect', function () {
         var id =  $(this).attr('id');
         jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_vehicleDT",
        dataType: 'html',
        data: {vid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#vregno").val(obj.name);
				$("#vehicleid").val(obj.vehicleid);
		       }
            });
    
        }); 
 
 /*$('#TblCustomer tbody').on('click', '.cselect', function () {
     //   $(this).parent().parent().toggleClass('selected');
        var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_customerDT",
        dataType: 'html',
        data: {cid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#custname").val(obj.name);
				$("#idcmobile").val(obj.mobile);
        $("#idcid").val(obj.id);
		       }
            });
    
        }); */
 
    /* $('#TblCustomer tbody').on('click', '#custadd', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         // var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Customer/add_customer",
        dataType: 'html',
        // data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
        }); */
 //---------------------------------------------------
  
 
$(document).ready(function () {
	$("#idsavebtn").prop('disabled', true);
	$("#idbtnget").prop('disabled', true);
	$("#idbtncalc").prop('disabled', true);
        setTimeout(function(){ $("#msg").html(""); }, 3000);
    }); 
	
</script>

<script>
$('#example1 tbody').on('click', '.select', function () {
 var Result=$("#myModals .modal-body");
 var id =  $(this).attr('id');
 $('#regno').val( id );
 $('#myModals').modal('hide')
});
</script>


<script>
$('#example2 tbody').on('click', '.select', function () {
 var Result=$("#myModald .modal-body");
 var id =  $(this).attr('id');
 $('#driver').val(id);
 $('#myModald').modal('hide')
});

/* -------- button desable/enanble ------------------*/

$('#idminckm').focus(function () {
	$("#idbtnget").prop('disabled', false);
});

/* -------- button desable/enanble ------------------*/


/* get Button  ----------------------*/

$('#idbtnget').click(function () {
	
	var skm=$.trim(parseInt($('#startkm').val()));
	var ekm=$.trim(parseInt($('#endkm').val()));
	
	var sdate=$('#startdate').val();
	var edate=$('#enddate').val();
	
	
	var mk1=$.trim($('#idminckm').val());
	
	if(sdate=="" || edate=="")
	{
		alert("Please choose Trip Start and End Date.");
	}
	
	else if(isNaN(skm) || isNaN(ekm) || skm=="0" || ekm=="0" || (parseInt(skm) > parseInt(ekm)))
	{
		alert("Please enter Starting KM and Ending KM.");
	}
	else if(mk1.length==0)
	{
		alert("please type minimum charge KM..!");
	}
	
	else
	{
	var rkm=(parseInt(ekm)-parseInt(skm));	
	
	var edate1=new Date(edate);
	var sdate1=new Date(sdate);
	
	var days = (parseInt(edate1.getTime() - sdate1.getTime())) / (1000 * 60 * 60 * 24);
		
	var adkm=parseInt(rkm)-(parseInt(days)*parseInt(mk1))
	
	//var days = (edate1.getTime() - sdate1.getTime())
	$("#runningkm").val(rkm);
	$("#mindays").val(days);
	$("#addikm").val(adkm);
	
	$("#idbtncalc").prop('disabled', false);
	$("#idsavebtn").prop('disabled', false);
	}

});
/*----------------------------------------*/




/* claulate totals ----------------------*/
$('#idbtncalc').click(function () {

var mdays=$('#mindays').val();	
var mcharge=$('#mincharge').val();	

var akm=$('#addikm').val();	
var akmc=$('#addikmcharge').val();
	
var tpc=$('#tollparking').val();	
var oc=$('#ocharge').val();	
var isp=$('#ispermit').val();	

var mtot=(parseFloat(mdays)* parseFloat(mcharge)).toFixed(2);
var adtot=(parseFloat(akm)* parseFloat(akmc)).toFixed(2);
var ttot=(parseFloat(mtot)+parseFloat(adtot)).toFixed(2);
var gtot=(parseFloat(ttot)+parseFloat(tpc)+parseFloat(oc)+parseFloat(isp)).toFixed(2);

$("#mintotal").val(mtot);
$("#additotal").val(adtot);
$("#triptotal").val(ttot);
$("#gtotal").val(gtot);
$("#lblgtotal").html(gtot);

//$("#idsavebtn").prop('disabled', false);
});
/*----------------------------------------*/


///dropdown list  customer name
   $('#id2').selectize({
     create: false
     , sortField: {
     //field: 'text',
     direction: 'asc'
   }
   , dropdownParent: 'body'
   });


</script>



 