<?php include('application/views/common/header.php'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1 class='heading' style='color:#00adee;padding-bottom:15px; font-weight:bold;'>Ledgers
           </h1>
	  <ol class="breadcrumb" style="margin-right:20px;font-size:13px;">
        <li><a href="#"></a></li>
      </ol>
	  
    </section>

    <!-- Main content -->
    <section class="content">
   
      <div class="row">
        <div class="col-md-5">
         <div class="box" style="border:1px solid #f5f3f3;">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Create Ledger</h3> 
				<center><input type='hidden' id="msg" value='<?php echo $this->session->flashdata('message'); ?>'></CENTER>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
	            <div class="col-md-12">
				 <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Ledger/save_ledger')?>" enctype="multipart/form-data">
						
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Ledger Name :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="lname" required>
								</div>
							</div>
						</div>
				
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Under :</label>
								<div class="col-md-7">
									<select  class="form-control" name='lunder'  id='lunder' required>
									  <option value="">---under----</option>
									  <?php
										$gquery=$this->db->select("*")->from('ac_groups')->get()->result();
										foreach( $gquery as $g)
										{
											echo "<option value='".$g->grp_id."'>".$g->grp_name."</option>";
										}
									  ?>
														  
									</select>
								</div>
							</div>
						</div>
						
			
						<div class="form-group" style='margin-top:20px;margin-bottom:20px;'>
						   <div class="row">
							  <label class="col-md-4 control-label"></label>
								<div class="col-md-7">
									<input type="submit" class="btn btn-primary "  name="btnsubmit" value="Save Ledger">
								</div>
							</div>
						</div>
				</form>
				
			    </div>
        </div>

       </div>
    </div>
   </div>
   <!-- end create group---->
   <!-- group list ---->
    <div class="col-md-7">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Ledger Lists</h3> 
				
            </div>
            <!-- /.box-header -->
            <div class="box-body" style='min-height:450px;'>
              <div class="row">
	            <div class="col-md-12">
				<table class="table table-hover table-bordered" id="example"  width='100%' border=0 style="font-size:14px">
             <thead>
                <tr>
				<th width='10%' style='text-align:center;'>Edit</th>
				<th width='8%'>ID</th> 
				<th >Ledger</th>
                <th>Group</th> 
                </tr>
                </thead>
				<tbody>
					<?php
					foreach($ledgers as $r)
					{
						$del=anchor('Account/delete_ledger/'.$r->ledg_id,'<span class="glyphicon glyphicon-trash" aria-hidden="true" style="font-size:14px;color:red;"></span>', array('id' =>'del_conf'));
					?>
						<tr>
						<td class='dtable' align='center'> 
							<a href='#' id='<?=$r->ledg_id;?>' class='abtn abtn-edit edit' data-toggle="modal" data-target="#ModalEdit" style='padding-right:7px;'>
							<span class="glyphicon glyphicon-edit" aria-hidden="true" style='font-size:16px;'></span></a>
						</td>
						 <td ><?=$r->ledg_id;?></td>
						 <td ><?=$r->ledg_name;?></td>
						 <td ><?=$r->grp_name;?></td>
						 </tr>
					<?php
					}
					?>	
				</tbody> 
				</table>
			  </div>
        </div>

       </div>
    </div>
   </div>
 
</div>
</section>
</div>
          


<!-- /.content-wrapper -->
  
<!---EDIT  modal dialog start -->

<!-- Modal -->
<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style='top:50px;border:3px solid #b7b7b7;'>
	 <div class="modal-body" style='padding:0px;'>
            
     </div>
    </div>
  </div>
</div>

<style> tr{ height:30px; } </style>

  
<?php include('application/views/common/footer.php'); ?>
   
<script type="text/javascript">

$("#example").dataTable({
	"ordering":false,
	});

var msg=$("#msg").val().split('#');
if(msg[0]=='1')
{
	swal('',msg[1],'success'); //Saved
}
else if(msg[0]=='2')
{
	swal('',msg[1],'success'); //Updated.
}
else if(msg[0]=='3')
{
	swal('',msg[1],'success'); //Removed.
}
else if(msg[0]=='4')
{
	swal('',msg[1],'error'); //Try Again.
}

 $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });


  $('#example tbody').on('click', '.edit', function (){
        var Result=$("#ModalEdit .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Ledger/edit_ledger",
        dataType: 'html',
        data: {lid: id,op:0},
        success: function(res) 
		{
				Result.html(res);
        }
        });
   }); 
 
</script>  
