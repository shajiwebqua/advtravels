<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1 class='heading'>New Loan Entry (Vehicle)
		</h1>
		<ol class="breadcrumb">
		<li> <a href="<?php echo base_url('Loan/index');?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Loan Details</button></a></li>
		<li> <a href="<?php echo base_url('Loan1/repayment');?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'> <i class="fa fa-list" aria-hidden="true"></i> Loan Schedule</button></a></li>
			<!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
		</ol>
	</section>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
	<!-- Content Header end -->
	<section class="content"> 
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12">
				<!--Add new user details -->
			<div style="background-color:#fff;border-radius:2px;padding:15px; ">
			<div class="portlet-title">
	      <center><h5 style="color:red;" id="h5msg" >&nbsp;<?php echo isset($validation)?$validation:"" ?></h5></center>
	      <!--</div> -->
	  </div>
	  <hr style='margin:0px;'>
	  <div class="portlet-body form">
	  	<form class="form-horizontal" role="loan_form" id="loan_form" method="post" action=" <?php echo base_url('Loan/add_loan_master');?>">
	  		<!-- text input -->
	  		<div class="box-body">

	  			<div class="row ">
	  				<div class="container-fluid">
	  					<div class="row">
	  							<div class="col-md-8 container">
	  									<div class="form-group">
	  										<label class="control-label col-sm-3" for="ltl_ref">LTL Reference No</label>
	  										<div class="col-sm-5"> 
	  											<input type="text" name="ltl_ref" class="form-control" id="ltl_ref" value="" required>
	  										</div>
	  										<div class="col-sm-4" style="color:red" id="ltl_error"><?php echo isset($ltlerror)?$ltlerror:"" ?></div>
	  									</div>
	  									<div class="form-group">
	  										<label class="control-label col-sm-3" for="vehicle_name">Vehicle Name</label>
	  										<div class="col-sm-8"> 
	  											<input type="text" name="vehicle_name" class="form-control" id="vehicle_name" value="" required>
	  										</div>
	  									</div>
	  									<div class="form-group">
	  										<label class="control-label col-sm-3" for="vehicle_amount">Vehicle Amount</label>
	  										<div class="col-sm-8"> 
	  											<input type="number" name="vehicle_amount" class="form-control" id="vehicle_amount" value="" required>
	  										</div>
	  									</div>
	  									<div class="form-group">
	  										<label class="control-label col-sm-3" for="loan_amount">loan Amount</label>
	  										<div class="col-sm-8"> 
	  											<input type="number" name="loan_amount" class="form-control" id="loan_amount" value="" required>
	  										</div>
	  									</div>
	  									<div class="form-group">
	  										<label class="control-label col-sm-3" for="intrest_rate">Intrest Rate</label>
	  										<div class="col-sm-8"> 
	  											<input type="number" name="intrest_rate" id="intrest_rate" class="form-control" value="" id="phone_no" required>
	  										</div>
	  									</div>
	  									<div class="form-group">
  										<label class="control-label col-sm-3" for="period">Period</label>
  										<div class="col-sm-4"> 
  											<input type="number" class="form-control" name="period" id="period" value="" required>
  										</div><div class="col-sm-2" style="padding-top:3px;padding-left:0px;">/ Months</div>
  									</div>
  									<div class="form-group">
  										<label class="control-label col-sm-3" for="down_paymet">Down Payment</label>
  										<div class="col-sm-8"> 
  											<input type="number" class="form-control" name="down_paymet" id="down_paymet" value="" required>
  										</div>
  									</div>
  									<div class="form-group">
  										<label class="control-label col-sm-3" for="vehicle_regno">Vehicle Regno</label>
  										<div class="col-sm-7"> 
  											<input type="text" class="form-control" name="vehicle_regno" id="vehicle_regno" value="" required>
  										</div>
  									</div>
  									<div class="form-group">
  										<label class="control-label col-sm-3" for="other_charges">Other Charges</label>
  										<div class="col-sm-7"> 
  											<input type="number" class="form-control" name="other_charges" id="other_charges" value="" required>
  										</div>
  									</div>
  									<div class="form-group">
  										<label class="control-label col-sm-3" for="starting_date">Starting Date</label>
  										<div class="col-sm-7"> 
  											<input type="date" class="form-control" name="starting_date" id="starting_date" value="" required>
  										</div>
  									</div>
	  							</div>
	  					</div>
	  				</div>
	  			</div>
				<hr style='margin-top:5px;'>
				<div class="form-group">
						<div class="col-sm-7 col-sm-offset-2" > 
						<button class="btn btn-success" type="submit" >Add Details</button>
					</div>
				</div>
	  				
	  			</form>
	  		</div>
	  </div>	
	</div>   
</div>
</div>
</section>
</div>
<?php include('application/views/common/footer.php');
/*
if(isset($account_master)){
		echo '
		<script>
			$("document").ready(function() {
			    $("#payment_mode option[value=\''.$account_master[0]->payment_mode.'\']").prop(\'selected\', true);
			});
		</script>
		';	  													
}
*/
?>
<script type="text/javascript">
	$("#ltl_ref").blur(function(){
        ltlref = $(this).val();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Loan/checkltlref",
        dataType: 'html',
        data: {ltlrefno: ltlref},
        success: function(res) {
            $("#addbtn").prop("disabled", true)
        }
        });
       });
</script>