<?php
 include('application/views/common/header.php');?>
 
<style>
/* radio button ----------------*/

#m-radio {
	margin: 0 0px;
}

#m-radio label {
  border-radius: 3px;
  border: 1px solid #D1D3D4
  	color:#888 ;
}

/* hide input */
#m-radio input.radio:empty {
	margin-left: -999px;
}

/* style label */
#m-radio input.radio:empty ~ label {
	position: relative;
	float: left;
	line-height: 2.5em;
	text-indent: 3.25em;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

#m-radio input.radio:empty ~ label:before {
	position: absolute;
	display: block;
	top: 0;
	bottom: 0;
	left: 0;
	content: '';
	width: 2.5em;
	background: #D1D3D4;
	border-radius: 3px 0 0 3px;

}

/* toggle hover */
#m-radio input.radio:hover:not(:checked) ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #C2C2C2;

}

#m-radio input.radio:hover:not(:checked) ~ label {
	color:#777 ;
}

/* toggle on */
#m-radio input.radio:checked ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #9CE2AE;
	background-color: #4DCB6D;
}

#m-radio input.radio:checked ~ label {
	/*color: #888;*/
}

/* radio focus */
.form-horizontal .checkbox, .form-horizontal .radio {
    min-height: 0px;
}
.no-pad {
	padding-left: 0px;
	padding-right: 0px;
}
.right1
{
	text-align:right;
	padding-right:10px;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Edit Fare Details</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px'>
    <li><a href="<?php echo site_url('General/farelist') ?>" ><input type='button' class='btn btn-primary add' value='Back To List'></a></li>
	 
   </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">
  <!-- Add new user details -->
              
<div class="page-content-wrapper">
   <!-- BEGIN CONTENT BODY -->
   <div class="page-content">
   
   <div style="padding:0px 15px 0px 15px;">
		 <div id="idmsg"  style="background-color:#fff;height:25px;margin-bottom:5px;">
		 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div>
		 <div id='msgerr'><?php echo $this->session->flashdata('message1'); ?></div>
		 <div id='msgdel'><?php echo $this->session->flashdata('message2'); ?></div>
		 </center></div>
	</div>

	<div style="background-color:#fff;padding:10px; ">
	
	<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/update_fare')?>" enctype="multipart/form-data">
	
	<input type="hidden" class="form-control"  name="fareid" id='fareid' value='<?=$result->fare_id;?>' required>
	<div class='row' style='padding-left:20px;'>
	<div class='col-md-3'>
	
		<div class='row' >
		 <label> Fare Category: </label>
		 <hr style='margin-top:3px;'>
		</div>
			
		<div class='row col-md-offset-1'>
		 <div id="m-radio" class="col-md-11 no-pad">
		  <input type="radio" name="fcat" id="radio1" class="radio" value='1' <?php if($result->fare_category=='1') echo 'checked';?>/>
		  <label for="radio1" style='float:left;'><b><FONT color='green'>NORMAL</font> TRIP FARE</b></label>
		  </div>
		</div>
		  <br>
		<div class='row col-md-offset-1' >
		  <div id="m-radio" class="col-md-11 no-pad">
		  <input type="radio" name="fcat" id="radio2" class="radio" value='2'  <?php if($result->fare_category=='2') echo 'checked';?> />
		  <label for="radio2" ><b><FONT color='green'>FIXED</font> TRIP FARE</b></label>
		  </div>
		  </div>
		  <br>
		<div class='row col-md-offset-1' >
		  <div id="m-radio" class="col-md-11 no-pad">
		  <input type="radio" name="fcat" id="radio3" class="radio" value='3'  <?php if($result->fare_category=='3') echo 'checked';?> />
		  <label for="radio3" ><b><FONT color='green'>CLUBED</font> TRIP FARE</b></label>
		  </div>
		</div>				  
	</div>
	
	<div class='col-md-8'>
	
	<div class='row' style='padding-left:10px;' >
		 <label> Fare Details: </label>
		 <hr style='margin-top:3px;'>
		</div>
	
						<div class="form-group" style='margin-top:10px;'>
						   <div class="row">
							  <label class="col-md-3 control-label right1">Corporate Name :</label>
								<div class="col-md-7">
								<select name='coname' class='form-control' required>
								<option value="">-----------</option>
								<?php
								$res=$this->db->select('*')->from('customers')->where('customer_type','2')->get()->result();
								foreach($res as $r)
								{
									?>
									<option value='<?=$r->customer_id;?>' <?php if($r->customer_id==$result->fare_corporate) echo "selected";?> ><?=$r->customer_name;?></option>
								<?php
								} ?>
								</select>
								</div>
							</div>
						</div>
						
						
						<div  class="form-group" id ='triptype' style='margin-top:10px;'>
						   <div class="row">
							  <label class="col-md-3 control-label right1">Trip Type:</label>
								<div class="col-md-7">
								<select name='tripop'  id='tripop' class='form-control'>
								<option value="0">-----------</option>
								<?php
								$res=$this->db->select('*')->from('category')->get()->result();
								foreach($res as $r)
								{
									?>
									<option value='<?=$r->category_id;?>' <?php if($r->category_id==$result->fare_trip) echo "selected";?> ><?=$r->category_name;?></option>
								<?php
								} ?>
								</select>
								</div>
							</div>
						</div>

						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label right1">Vehicle :</label>
								<div class="col-md-7">
								
								<select name='vehicle' class='form-control' required>
								<option value="">-----------</option>
								<?php
								$res=$this->db->select('*')->from('vehicle_types')->get()->result();
								foreach($res as $r)
								{
									?>
									<option value='<?=$r->veh_type_id;?>' <?php if($r->veh_type_id==$result->fare_vehicle) echo "selected";?> ><?=$r->veh_type_name;?></option>
								<?php
								} ?>
								</select>

								</div>
							</div>
						</div>	

						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label right1">Fare Hrs :</label>
								<div class="col-md-7">
								<input type="text" class="form-control"  name="farehrs" id='farehrs' value='<?=$result->fare_description;?>' required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label right1">Fare KM :</label>
								<div class="col-md-7">
								<input type="text" class="form-control"  name="farekm" id='farekm' value='<?=$result->fare_km;?>' required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label right1">Amount :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="amount" id='amount' value='<?=$result->fare_amount;?>' required>
								</div>
							</div>
						</div>
						
                       <hr>
					                        
 
                    <div class="form-group">
						<div class="row">
						<div class="col-md-2 left1">
						</div>
                               <div class="col-md-9" style="padding-left:25px;">
                                   <button type="submit" id='idsubmit' class="btn btn-success">Update Fare Details</button>
                               </div>
                        </div>
                       </div>
	
	</div>
	</div>
	</form>
		
	</div>

		</div> <!-- end Row --->
                <!-- END CONTENT BODY -->
            <!-- </div> -->
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>

  <!-- End user details -->
  </div>
  
   <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 

</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

//sweet alert box ----------------------
$("#idmsg").hide();

var cop=$("#radio1").attr('checked');

if(cop=='checked')
{
 $("#triptype").hide();
}


$("#radio1").hide();
$("#radio2").hide();
$("#radio3").hide();


$("#radio1").change(function()
{
	$("#triptype").hide();
	$("#tripop").removeAttr("required");
});

$("#radio2").change(function()
{
	$("#triptype").show();
	$("#tripop").attr("required","required");
	$("#farehrs").val('0');
	$("#farekm").val('0');
	
});

$("#radio3").change(function()
{
	$("#triptype").show();
	$("#tripop").attr("required","required");
	$("#farehrs").val('0');
	$("#farekm").val('0');
});


if($.trim($("#msg").html())!="")
	{
		swal("Saved",$("#msg").html(),"success")
		$("#msg").html("");
		$("#msgerr").html("");
	}
	
$("#msgerr").hide();
if($.trim($("#msgerr").html())!="")
	{
		swal("Try Again",$("#msgerr").html(),"error")
		$("#msgerr").html("");
		$("#msg").html("");
	}

	$("#msgdel").hide();
if($.trim($("#msgdel").html())!="")
	{
		swal("Deleted",$("#msgdel").html(),"success")
		$("#msgdel").html("");
	}
//-------------------------------------
 
 $("#drshift").change(function()
 {
	 var st=$("#drshift").val();
	 if(st=='1')
	 {
		 $("#drtime").val("6 AM - 8 PM");
	 }
	 else if(st=='2')
	 {
		 $("#drtime").val("8 PM - 6 AM");
	 }
	 $("idsubmit").focus();
 });
 
 
 
  $('#example').dataTable( {
         destroy: true,
        "processing": true,
		"ordering":false,
		"scrollX" : true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/fare_list_ajax",// json datasource
               },
	
        
        "columns": [
           /* { "data": "edit" },*/
            { "data": "delete" },
			{ "data": "cfid" },
            { "data": "cname"},
            { "data": "cvehicle" },
			{ "data": "fcat" },
			{ "data": "ftrip" },
			{ "data": "hrs" },
			{ "data": "ckm" },
			{ "data": "camount" },
       ]
  });

   $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/edit_fare_list",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 
		
 	
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#msg').html(''); }, 3000);
 //});
 


</script>



