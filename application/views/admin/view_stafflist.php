<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>View Staffs Documents</b></h1>
    <!-- <ol class="breadcrumb" style="font-size:15px;">
<li><a href="<?php echo site_url('Pdf/staff') ?>" style='color:#4b88ed;' ><i class="fa fa-print" aria-hidden="true"></i>Print</a></li>
<li><a id='idsave' href="" style='color:#4b88ed;' ><i class="fa fa-save" aria-hidden="true"></i>Save</a></li>
<li><a id='idshare' href="" data-target="#myModalsh" data-toggle='modal' style='color:#4b88ed;'> <i class="fa fa-share-alt" aria-hidden="true"></i>Share</a></li>
<li><a  href="<?php echo site_url('Pdf/staff') ?>" style='color:#4b88ed;' ><i class="fa fa-eye" aria-hidden="true"></i>View</a></li>
<li><a id='idpassword' href="" data-target="" data-toggle='modal' style='color:#4b88ed;'><i class="fa fa-user-o" aria-hidden="true"></i>Password</a></li>
<li><a id='iddelete' href="" style='color:#4b88ed;'><i class="fa fa-trash" aria-hidden="true"></i>delete</a></li>

       </ol>  -->
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 
	  
  <section class="content"> 
  <!-- <div style="color:blue;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Print,Share,Save,View,Edit,Delate)  </div> -->
			<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
  
    <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!------------------------------------ Add new user details ----------------------------------->

<div style="background-color:#fff;padding:15px; ">
							
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
 
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                      <div class="row" >
                      <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
					  <table class="table table-striped table-hover table-bordered" id="example">
                      <thead>
								<tr style="color:#5068f8;">
                <!--  <th>Edit</th> -->
							 <th>ID</th>
								<th>Photo</th>
								<th>Name</th>
								<th>Mobile</th>
								<th>Certificate</th>
								<th>Licence</th>
								<th>Id Card</th>
								<th>Others</th>
								</tr>
								</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
         <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
			  </div>
                   </div> 

                    <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>           


					<div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div> 

                   <div class="modal fade draggable-modal" id="myModalsh" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Share</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>                   

  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 
 //sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------

      $('#example').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Staff/stafflist_ajax",// json datasource
               
               },
		"columnDefs":[
		{"width":"10%","targets":0},
		{"width":"10%","targets":1},
		{"width":"20%","targets":2},
		{"width":"13%","targets":3},
		],
			   
        "columns": [
      // { "data": "edit"},
      { "data": "id"},
	  { "data": "image"},
	  { "data": "name"},
	  { "data": "mobile"},
      { "data": "certificate" },
      { "data": "licence" },
      { "data": "idcard" },
        { "data": "others" },
           
           
       ]
    });
			

			
// var table = $('#example').DataTable();
//      $('#example tbody').on( 'click', 'tr', function () {
//         if ( $(this).hasClass('selected') ) {
//             $(this).removeClass('selected');
//         }
//         else {
// 	        table.$('tr.selected').removeClass('selected');
//             $(this).addClass('selected');
//         }
//   });			   
	

	/* $('#idview').click(function () 
	  {
        var Result=$("#myModal1 .modal-body");
        //var id =  $(this).attr('id');
		var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(id=="")
		 {
			 alert ("please Select staff details..");
		 }
		 else
		 {
			$("#idview").attr('data-target','#myModal1') 
			
			jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "Staff/view_staff_details",
			dataType: 'html',
			data: {staff_id: id},
			success: function(res) {
			Result.html(res);
						}
				});
		 }
		 
  }); 	*/	   
 
 

//   $('#idpassword').click(function ()
//   {
//         var Result=$("#myModal3 .modal-body");
//         // $(this).parent().parent().toggleClass('selected');
//          //var id =  $(this).attr('id');
//      	 var id=$('#example').find('tr.selected').find('td').eq(1).text();
// 		 if(id=="")
// 		 {
// 			 alert ("please Select staff details..");
// 		 }
// 		 else
// 		 {
// 			 $("#idpassword").attr('data-target','#myModal3') 
// 			jQuery.ajax({
// 			type: "POST",
// 			url: "<?php echo base_url(); ?>" + "Staff/change_password",
// 			dataType: 'html',
// 			data: {staff_id: id},
// 			success: function(res) {
// 			Result.html(res);
// 						}
// 			});
// 		 }
//   }); 


//    $('#view-pdf').on('click',function(){
//        var id=$('#example').find('tr.selected').find('td').eq(1).text();
//      if(id=="")
//      {
//        alert ("please Select customer details..");
//      }
//      else
//      {
//         var pdf_link = $(this).parent('a').attr('href')+'/'+id;
//         //alert(pdf_link);
//         var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
//         //alert(iframe);
//         $.createModal({
//         title:'Staff Details',
//         message: iframe,
//         closeButton:true,
//         scrollable: true,
//     });
       
//        } return false; 

//     }); 


// 	$('#idview').on('click',function(){
//        var id=$('#example').find('tr.selected').find('td').eq(1).text();
//      if(id=="")
//      {
//        alert ("please Select customer details..");
//      }
//      else
//      {
//         var pdf_link = $(this).parent('a').attr('href')+'/'+id;
//         //alert(pdf_link);
//         var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
//         //alert(iframe);
//         $.createModal({
//         title:'Staff Details',
//         message: iframe,
//         closeButton:true,
//         scrollable: true,
//     });
       
//        } return false; 

//     }); 
	
	
	
	
	
//  $('#idshare').click(function ()
//   { 
//     var Result=$("#myModalsh .modal-body");
//      //   $(this).parent().parent().toggleClass('selected');
//          //var id =  $(this).attr('id');
//      var id=$('#example').find('tr.selected').find('td').eq(1).text();
//      //alert(id);
//      if(id=="")
//      {
//        alert ("please Select customer details..");
//      }
//      else
//      {
// 		$('#idshare').attr('data-target','#myModalsh'); 
		
//       jQuery.ajax({
//       type: "POST",
//       url: "<?php echo base_url(); ?>" + "Staff/share_staff",
//       dataType: 'html',
//       data:{id:id},
//       success: function(res) {
//       Result.html(res);
//        }
//       });
//      }
//         }); 


//       $('#idsave').click(function ()
//     {   
//         //var Result=$("#myModalsa .modal-body");
//      //   $(this).parent().parent().toggleClass('selected');
//          //var id =  $(this).attr('id');
//          var id=$('#example').find('tr.selected').find('td').eq(1).text();
//      //alert(id);
//          if(id=="")
//          {
//              alert ("please Select customer details..");
//          }
//          else
//          {
//             jQuery.ajax({
//             type: "POST",
//             url: "<?php echo base_url(); ?>" + "Staff/save_staff",
//             dataType: 'html',
//             data:{uid:id},
//             success: function(res) {
//             Result.html(res);
//              }
//             });
//          }
//         }); 
  
  
// $('#iddelete').click(function () 
//     {
// 	var id=$('#example').find('tr.selected').find('td').eq(1).text();	
// 	if(id=="")
//          {
//              alert ("please Select customer details..");
//          }
//          else
//          {	
// 		var res=confirm("Delete this coustomer details?");
// 		if(res)
// 		{
//         jQuery.ajax({
//         type: "POST",
//         url: "<?php echo base_url(); ?>" + "Staff/del_entry",
//         dataType: 'html',
//         data: {sid: id},
//         success: function(res) {
//         alert("Satff details deleted..");
//         }
// });
//        }
// 	 }
//     });     

		
//       $(document).on("click", "#del_conf", function () {
//             return confirm('Are you sure you want to delete this entry?');
//         });

// setInterval(function(){ $('#idh5').html(''); }, 3000);

 //});
 
</script>



