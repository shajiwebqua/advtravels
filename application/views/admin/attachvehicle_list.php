<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Attached Vehicle List</b> </h1>
     <ol class="breadcrumb" style='font-size:15px;'>
	    <li><a id='attach' href="<?php echo site_url('attachvehicle/new_vehicle') ?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-cab" aria-hidden="true"></i> Attach Vehicle</button></a></li>
		<li><a id='view-pdf' href="<?php echo site_url('Pdf/pdf_attach') ?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-print" aria-hidden="true"></i> Print/Save</button></a></li>
		<?php 
		//if(checkisAdmin()){
			//echo "<li><a id='iddelete' href='' style='color:#4b88ed;'><i class='fa fa-trash' aria-hidden='true'></i>Delete</a></li>";
		//}
		?>
     </ol> 
  </section>
<?php
echo date('Y-m-d',strtotime("-3 months"));
?>
  
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 	  
  <section class="content"> 
   <!--<div style="color:blue;font-size:12px;"> Select vendor details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Delete)  </div> -->
            <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details -->
	

	 <div style="background-color:#fff;padding:15px; ">

							
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">

                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="example" width='100%'>
                        <thead>
								<tr>
  							    <th>Action</th>
								<th>ID</th>
								<th>Vehicle Type</th>
								<th>R/C owner Name</th>
								<th>Mobile</th>
								<th>Driver Name</th>
								<th>Mobile</th>
								<th>Start-KM</th>
								
								</tr>
								</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>

         <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Add</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>         
  
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Edit</h4>
								</div>
							</div>
							</div>
						<!-- /.modal-dialog -->
						</div>
                   </div>	

				   
                     <div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">View</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>						
				
	</div>
	<!-- End user details -->
	</div>
	
	</div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 getvehicledetails();
 $("#idmsg").hide();
//sweet alert ----------------------------
	if($("#idh5").html()!="")
	{
		swal( $("#idh5").html(),"","success")
		$("#idh5").html("");
	}
// sweet alert box -------------------------- 
 
 function getvehicledetails()
 {
 
      $('#example').dataTable( {
		 "ordering":false,
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax":{
                url :"<?php echo base_url(); ?>" + "Attachvehicle/vehicle_ajax",// json datasource
               },
		"columnDefs":[
		{"width":"10%","targets":0},
		{"width":"7%","targets":1},
		{"width":"20%","targets":3},
		],
			   
        "columns": [
            { "data": "edit"},
            { "data": "id" },
  		    { "data": "vtype" },
            { "data": "rcname"},
            { "data": "rcmobile"},
            { "data": "drname"},
            { "data": "drmobile" },
			{ "data": "startkm" },
       ]
  } );
 }
 
 
   var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });	

      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Attachvehicle/edit_vehicle",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 

// delete button with sweetalert--------------------------------------		
		$('#example tbody').on('click', '.delete', function () {
			var id =  $(this).attr('id');
		swal({
			  title: "Are you sure?",
			  text: "You will not be able to recover this student details!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Yes, delete it!",
			  closeOnConfirm: false
			},
			function(){

			jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>" +  "Attachvehicle/del_entry",
					dataType: 'html',
					data: {id: id},
					success: function(res) 
					{
						getvehicledetails();
					}
						});
					
			  swal("Deleted!", "Vehicle details removed.", "success");
			});	
    
        });
	
	
	/*$('#iddelete').click(function () 
    {
    	 var id=$('#example').find('tr.selected').find('td').eq(2).text();
    	  if(id=="")
         {
             alert ("please Select Vendor details..");
         }
         else
         {
    var res=confirm("Delete this Vendor details?");
    if(res)
    {
    var id=$('#example').find('tr.selected').find('td').eq(2).text();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Owner/del_entry",
        dataType: 'html',
        data: {vid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
      
    }  
     } 
   });   */

   
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

 //});

 $('#view-pdf').on('click',function(){
       var id=$('#example').find('tr.selected').find('td').eq(2).text();
        var pdf_link = $(this).attr('href');
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Vendors Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
      return false; 

    }); 

  


</script>



