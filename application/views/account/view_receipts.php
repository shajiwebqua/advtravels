<?php include('application/views/common/header.php'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1 class='heading' style='color:#00adee;padding-bottom:15px; font-weight:bold;'>Ledgers
           </h1>
	  <ol class="breadcrumb" style="margin-right:20px;maring-bottom:0px;">
        <li ><a href="<?php echo base_url('Ledger/Add');?>"><button class='btn btn-primary btn-xs' style='margin-top:5px;'><i class="fa fa-plus" aria-hidden="true"></i> Add Ledger</button></a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Ledger Lists</h3> 
				<center><input type='hidden' id="msg" value='<?php echo $this->session->flashdata('message'); ?>'></CENTER>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style='min-height:450px;'>
              <div class="row" style='margin-left:20px;'>
	            <div class="col-md-8">
				<table class="table table-hover table-bordered" id="example"  width='100%' border=0 style="font-size:14px">
				<thead>
                <tr>
				<th width='10%'>Edit</th> 
				<th width='8%'>ID</th> 
				<th >Ledger</th>
                 <th>Group</th> 
                 </tr>
                </thead>
				<tbody>
					<?php
					foreach($result as $r)
					{
						$del=anchor('Ledger/delete_ledger/'.$r->ledg_id,'<span class="glyphicon glyphicon-trash" aria-hidden="true" style="font-size:14px;color:red;"></span>', array('id' =>'del_conf'));
					?>
						<tr>
						<td class='dtable' align='center'> 
							<a href='#' id='<?=$r->ledg_id;?>' class='abtn abtn-edit edit' data-toggle="modal" data-target="#ModalEdit" style='padding-right:7px;'>
							<span class="glyphicon glyphicon-edit" aria-hidden="true" style='font-size:16px;'></span></a>
						<!--	<?=" " .$del;?>  -->
						</td> 						
						 <td ><?=$r->ledg_id;?></td>
						  <td ><?=$r->ledg_name;?></td>
						 <td ><?=$r->grp_name;?></td>
						 </tr>
					<?php
					}
					?>	
				</tbody> 
				</table>
			  </div>
        </div>
       </div>
    </div>
   </div>
</div>
</section>
</div>


<!-- /.content-wrapper -->
<!---EDIT  modal dialog start -->
<!-- Modal -->
<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style='top:50px;border:3px solid #b7b7b7;'>
	 <div class="modal-body" style='padding:0px;'>
            
     </div>
    </div>
  </div>
</div>

<?php include('application/views/common/footer.php'); ?>
   
<script type="text/javascript">

$("#example").dataTable({
	"ordering":false,
	});

var msg=$("#msg").val().split('#');
if(msg[0]=='1')
{
	swal('',msg[1],'success'); //Saved
}
else if(msg[0]=='2')
{
	swal('',msg[1],'success'); //Updated.
}
else if(msg[0]=='3')
{
	swal('',msg[1],'success'); //Removed.
}
else if(msg[0]=='4')
{
	swal('',msg[1],'error'); //Try Again.
}

 $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });


  $('#example tbody').on('click', '.edit', function (){
        var Result=$("#ModalEdit .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Ledger/edit_ledger",
        dataType: 'html',
        data: {lid: id,op:1},
        success: function(res) 
		{
				Result.html(res);
        }
        });
   }); 
 
</script>  
