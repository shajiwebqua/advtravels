<?php
include('application/views/common/header.php');
?>
<style>
input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
	
.addcon .glyphicon-plus-sign {
    color: #675e5e;
}

.addcon .glyphicon-plus-sign:hover {
    color:#5b9c7e !important;
}

.edit
{
	display:inline;
	background-color:#blue;
	float:left;
	position:relative;
}
.save
{
	display:none;
	float:left;
	position:relative;
}
.delete
{
	float:left;
	position:relative;
}
#data_table1 td, th 
{
	padding:3px;
}

#data_table2 td, th
{
	padding:3px;
}

</style>
<?php
$tabstatus=1;
?>
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#00adee'>Journal
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
     
     </section>
	
        <!-- Main content -->
    <section class="content">
	
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box" style="border:1px solid #f5f3f3;">
            <div class="box-header">
          <h3 class="box-title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp; Create Journal Voucher</h3>
		  <label id='mes'><?php echo $this->session->flashdata('message');?></label>
        </div>
		
        <div class="box-body">
				
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item <?php if($tabstatus!='2') echo "active";?>" id='tb1' >
						<a class="nav-link tp1" data-toggle="tab" href="#vpayment"  role="tab"><i class="fa fa-bars" aria-hidden="true"></i> Journal</a>
					</li>
					<li class="nav-item <?php if($tabstatus=='2') echo "active";?>" id='tb2'>
						<a class="nav-link tp1 " data-toggle="tab" href="#vlist"  role="tab"><i class="fa fa-bars" aria-hidden="true"></i> Journal Details</a>
					</li>
					
                 </ul>
                <!-- Tab panes -->
			<div class="tab-content">
			<div class="tab-pane <?php if($tabstatus!='2') echo "active";?>" id="vpayment" role="tabpanel">
		
        	<div class='row'>
			<div class='col-md-8'>
			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Voucher/save_contra')?>" enctype="multipart/form-data">
                  
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label">Voucher Type :</label>
								<div class="col-md-9">
									<input type="text" class="form-control"  name="cvtype" value='JOURNAL' readonly>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label">Date :</label>
								<div class="col-md-9">
									<input type="text" class="form-control"  name="cdate" value='<?php echo date('d-m-Y');?>' readonly>
								</div>
							</div>
						</div>
	
			</form>
				
			</div>
			</div>
<div class='row'>
<div class='col-md-12'>
<label style='font-weight:600;'>Enter Particulars:</label>
<hr style='margin:0px;'>
</div>
</div>			
			
			

			
<div class='row' style='margin-top:15px;'>
<!-- first column ------------------------------------->

<div class='col-md-6' style='border-right:solid 1px red;'>
<table align='center' cellspacing=0 cellpadding=0 id="data_table1" border=1 width='100%'>
<tr>
<th>By</th>
<th>Particulars</th>
<th>Debit</th>
<th></th>
</tr>

<tr>
<td ><input type="text" id="new_name" size="5" value='By'  readonly></td>
<td ><select id="new_country">
<option value=''>----------</option>
<?php
	$lquery=$this->db->select("*")->from('acc_ledgers')->get()->result();
	foreach($lquery as $l)
	{
		echo "<option value='".$l->acc_ledgname."' ref='".$l->acc_ledgname."'>".$l->acc_ledgname."</option>";
	}
  ?>
</select>
</td>
<td ><input type="text" id="new_age" size="10" style='text-align:right;'></td>
<td ><button type="button" class="add" style='color:green;' onclick="add_row();" > <i class="fa fa-save" aria-hidden="true"></button></td>
</tr>
</table>
</div>


<!--- second column------------------------------------->
		
<div class='col-md-6' style='border-left:solid 1px red;' >

<table align='center' cellspacing=2 cellpadding=5 id="data_table2" border=1 width='100%'>
<tr>
<th>To</th>
<th>Particulars</th>
<th>Credit</th>
<th></th>
</tr>

<tr>
<td ><input type="text" id="new_name1" size="5" value='To'  readonly></td>
<td ><select id="new_country1">
<option value=''>----------</option>
<?php
	$lquery=$this->db->select("*")->from('acc_ledgers')->get()->result();
	foreach($lquery as $l)
	{
		echo "<option value='".$l->acc_ledgname."' ref='".$l->acc_ledgname."'>".$l->acc_ledgname."</option>";
	}
  ?>
</select>
</td>
<td ><input type="text" id="new_age1" size="10" style='text-align:right;'></td>
<td ><button type="button" class="add"  style='color:green;' onclick="add_row1();" > <i class="fa fa-save" aria-hidden="true"></button></td>
</tr>
</table>
</div>

</div>

<div class='row'>
<div class='col-md-8'>
</div>
</div>


<div class='row' style='margin-top:15px;'>
<div class='col-md-8'>

<div class="form-group">
   <div class="row">
	  <label class="col-md-3 control-label la-right">Narration :</label>
		<div class="col-md-9">
			<textarea rows=3 class="form-control"  name="cnarrat" required></textarea>
	  </div>
	</div>
 </div>
</div>
</div>


<div class='row' style='margin-top:15px;'>
<div class='col-md-12 col-md-offset-2'>
<button class='btn btn-primary' style='padding:5px 20px 5px 20px;'>Submit voucher Details:</button>
<hr style='margin:0px;'>
</div>
</div>	









			
			</div> <!-- first tab end -->
			
			<div class="tab-pane <?php if($tabstatus=='2') echo "active";?> " id="vlist" role="tabpanel">
			
			<div class='row' style='margin:0px 5px 0px 5px;background-color:#e4e4e4;padding:5px;'>
			
			<form role="form" method="POST" action="<?php echo base_url('Voucher/contra/2')?>" enctype="multipart/form-data">
             			
			<div class='col-md-2' style='padding-top:5px;'>	<label class='control-label'>Select Date [from-to] : </label>	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			<div class='col-md-2' style='padding-top:2px;'><input type='submit' name='btndetails' class='btn btn-primary' value='Get Details'> </div>
			
			</form>
			</div>
		
			
			
			<div class='row' style='margin-left:10px;margin-top:15px;'>
			<label style='padding-bottom:3px; border-bottom:1px solid #e4e4e4;font-size:15px;'><b><?php echo $this->session->flashdata('ptitle');?></b></label>
			</div>
					
			<div class='row' style='padding:10px;'>
			<div class='col-md-12'>
			 <table class="table table-striped table-hover table-bordered" id="example" border=0 STYLE='font-size:14px;width:100%'>
			 
			 <thead>
                <tr>
				 <th  style='text-align:center !important;'>Edit</th>
                 <th>ID</th>
				 <th>Date</th>
                 <th>Account</th> 
                 <th>Particulers</th>
				 <th>Narration</th>
				 <th style='text-align:right !important;' >Amount</th>
				 
                </tr>
                </thead>
				<tbody>
					<?php
					$gtotC=0;
					if(isset($result))
					{
					foreach($result as $r)
					{
					?>
						<tr height='33px'>
						 <td align='center' class='dtable'><a href='#' class='edit abtn abtn-edit' data-toggle="modal" data-target="#exampleModal"><span class="glyphicon glyphicon-edit" aria-hidden="true" ></span></a>
						  <a href='#' class='del abtn abtn-del'> <span class="glyphicon glyphicon-trash" aria-hidden="true" ></span></a>
						 </td>
						 <td><?=$r->acc_jou_id?></td>
					     <td><?=date_format(date_create($r->acc_jou_date),'d-m-Y');?></td>
						 <td><?=$r->acc_jou_by;?></td>
						 <td><?=$r->acc_jou_to;?></td> 
						  <td><?=$r->acc_jou_narration;?></td> 
						 <td align='right' style='font-weight:600;'><?=number_format($r->acc_jou_toamount,'2','.','');?></td>
						</tr>
					<?php
					$gtotC+=$r->acc_jou_toamount;
					}
					
					}
					?>	
				</tbody>
			 
				</table>
				<div class='row' style='margin:10px 0px;background-color:#e2e2e2;padding-top:3px;'>
				<div class='col-md-9' style='text-align:right;'>
				<label class='control-label' style='font-size:16px;font-weight:600;'> Total Received Amount : </label>
				</div>
				<div class='col-md-3' style='text-align:right;'>
				<label class='control-label' style='font-size:18px;font-weight:600;'> &#8377;&nbsp; <?=number_format($gtotC,"2",".","");?></label>
				</div>
				</div>
				</div>
								
			</div>
					
			</div> <!-- second tab end --->
			</div> <!-- tab content end -->
           
        </div>
	</div>
	</div>
	
</section>
    <!-- content wrapper -->
	
	 <!---EDIT  modal dialog start ---->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style='top: 50px;border:1px solid #b7b7b7;'>
      <div class="modal-header" style='background-color:#cacaca;'>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true">&times;</span>
        </button>
		<h4 class="modal-title" id="exampleModalLabel" >Edit contra voucher</h4>
      </div>
	  
	  <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Voucher/edit_contra')?>" enctype="multipart/form-data">
	  
		<div class="modal-body">
					<input type="hidden" class="form-control"  name="mcid"  id="mcid" required>
				<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Account :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="mcaccount"  id="mcaccount" value='CASH' readonly>
								</div>
							</div>
						</div>
				
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Particulers :</label>
								<div class="col-md-7">
									<select  class="form-control" name='mcledger' id='mcledger' required>
									  <option value="">---select----</option>
									  <option value="CASH">CASH</option>
									<?php
									/*	$lquery=$this->db->select("*")->from('acc_ledgers')->get()->result();
										foreach($lquery as $l)
										{
											echo "<option value='".$l->acc_ledgid."'>".$l->acc_ledgname."</option>";
										}*/
									  ?>
								</select>
								</div>
							</div>
						</div>
			
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Amount :</label>
								<div class="col-md-7">
									<input type="number" class="form-control"  name="mcamount" id="mcamount" required>								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Narration :</label>
								<div class="col-md-7">
									<textarea rows=3 class="form-control"  name="mcnarrat" id='mcnarrat' required></textarea>
							  </div>
							</div>
						</div>
						
										
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Save changes</button>
		  </div>
	  </form>
    </div>
  </div>
</div>

<!-- modal dialog end ----->
			
	
</div>
     <?php
  include('application/views/common/footer.php');
  ?>
</body>
</html>
  <script type="text/javascript">
$("#mes").hide();
  
$("#ctrl1").hide();
$("#ctrl2").hide();
$("#ctrl3").hide();
$("#ctrl4").hide();
$("#ctrl5").hide();

  
$('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    autoclose:true,
	
});
   $('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});
  
 $("#addctrl1").click(function()
 {
	 $("#ctrl1").show();
	 $("#addctrl1").hide();
 });
  
 
 $("#addctrl2").click(function()
 {
	 $("#ctrl2").show();
	 $("#addctrl2").hide();
 });
 
 
 $("#addctrl3").click(function()
 {
	 $("#ctrl3").show();
	 $("#addctrl3").hide();
 });
  
  $("#addctrl4").click(function()
 {
	 $("#ctrl4").show();
	 $("#addctrl4").hide();
	 $("#addctrl5").hide();
 });
 
  
  $("#example").dataTable({
	  "ordering":false,
  });
  
  
  $("#jbyledger").change(function()
  {
	 $("#jbyamt").focus(); 
  });
    $("#jtoledger").change(function()
  {
	 $("#jtoamt").focus(); 
  });
   $("#jtoledger1").change(function()
  {
	 $("#jtoamt1").focus(); 
  });
   $("#jtoledger2").change(function()
  {
	 $("#jtoamt2").focus(); 
  });
  
  $("#jtoledger3").change(function()
  {
	 $("#jtoamt3").focus(); 
  });
  
  $("#jtoledger4").change(function()
  {
	 $("#jtoamt4").focus(); 
  });
 
  
  var msg=$("#mes").html().split("#");
  
  if($("#mes").html()!="")
  {
	  var msg=$("#mes").html().split("#");
	if(msg[0]=='1')
	  swal("Saved.!",msg[1],"success");
	else if(msg[0]=='2')
	  swal("Updated.!",msg[1],"success");
    else if(msg[0]=='3')
	  swal("Removed.!",msg[1],"success");
	else if(msg[0]=='4')
	  swal("Try Again.",msg[1],"error");
     $("#mes").html("");
  }
  
$('#example tbody').on('click','.edit', function()
{
  //var rt1=$(this).parent('tr')
  var cid=$(this).closest('tr').find('td').eq(1).text();
  var dt=$(this).closest('tr').find('td').eq(2).text();
  var acc=$(this).closest('tr').find('td').eq(3).text();
  var part=$(this).closest('tr').find('td').eq(4).text();
  var narr=$(this).closest('tr').find('td').eq(5).text();
  var amt=$(this).closest('tr').find('td').eq(6).text();
  	  $("#mcid").val(cid);
	  $("#mcledger").val(part);
	  $("#mcamount").val(amt);
	  $("#mcnarrat").val(narr);
 });	
  
$('#example tbody').on('click','.del', function()
{
  var cid=$(this).closest('tr').find('td').eq(1).text();
  
		  swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this voucher.!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "Cancel",
		  closeOnConfirm: true,
		  closeOnCancel: true
		},
		function(isConfirm) 
		{
		  if (isConfirm)
 		  {
			  jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>" + "Voucher/delete_contra",
					dataType: 'html',
					data: {coid:cid},
					success:function(res)
					{
						window.location.href="<?php echo base_url('Voucher/contra/3');?>";
					}
					}); 
			
		  }
		});
 });	
 
 
// edit table script------------------------------------------------------------------------------------------------
/*function edit_row(no)
{
 document.getElementById("edit_button"+no).style.display="none";
 document.getElementById("save_button"+no).style.display="block";
	
 var name=document.getElementById("name_row"+no);
 var country=document.getElementById("country_row"+no);
 var age=document.getElementById("age_row"+no);

 var name_data=name.innerHTML;
 var country_data=country.innerHTML;
 var age_data=age.innerHTML;

	
 name.innerHTML="<input type='text' id='name_text"+no+"' value='"+name_data+"'>";
 <?php
	$lquery=$this->db->select("*")->from('acc_ledgers')->get()->result();
	$opt="";
	foreach($lquery as $l)
	{
		$opt="<option value='".$l->acc_ledgname."' ref='".$l->acc_ledgname."'>".$l->acc_ledgname."</option>";
	}
  ?>
 country.innerHTML="<select id='country_text"+no+"'><?php echo $opt;?><select>";
 age.innerHTML="<input type='text' id='age_text"+no+"' value='"+age_data+"'>";
}
*/




/*function save_row(no)
{
 var name_val=document.getElementById("name_text"+no).value;
 var country_val=document.getElementById("country_text"+no).value;
 var age_val=document.getElementById("age_text"+no).value;

 document.getElementById("name_row"+no).innerHTML=name_val;
 document.getElementById("country_row"+no).innerHTML=country_val;
 document.getElementById("age_row"+no).innerHTML=age_val;
 
 document.getElementById("edit_button"+no).style.display="block";
 document.getElementById("save_button"+no).style.display="none";
}
*/


function delete_row(no)
{
 document.getElementById("row"+no+"").outerHTML="";
}


function add_row()
{
 var new_name=document.getElementById("new_name").value;
 var new_country=document.getElementById("new_country").value;
 var new_age=document.getElementById("new_age").value;
 
 if(new_country=="" || new_country==null)
 {
	 alert("Particulars Missing, Try again.");
	 $("#new_country").focus();
 }
 else if(new_age=="" || new_age==null || new_age=="0")
 {
	 alert("Debit Amount Missing, Try again.");
	 $("#new_age").focus();
 }
 
 
 else
 {
	
 var table=document.getElementById("data_table1");
 var table_len=(table.rows.length)-1;
 var row = table.insertRow(table_len).outerHTML="<tr id='row"+table_len+"'>" +
 "<td id='name_row"+table_len+"'>"+new_name+"</td>"+
 "<td id='country_row"+table_len+"'>"+new_country+"</td>"+
 "<td id='age_row"+table_len+"'>"+new_age+"</td>"+
 "<td ><button type='button' class='delete' style='color:#ff5722;' onclick='delete_row("+table_len+")'><i class='fa fa-trash' aria-hidden='true'></i></button></td></tr>";

 /*"<td ><button type='button' id='edit_button"+table_len+"' class='edit' onclick='edit_row("+table_len+")'><i class='fa fa-pencil' aria-hidden='true'></i> </button><button type='button' id='save_button"+table_len+"'  class='save' onclick='save_row("+table_len+")'><i class='fa fa-save' aria-hidden='true'></i></button> <button type='button' class='delete' onclick='delete_row("+table_len+")'><i class='fa fa-trash' aria-hidden='true'></i></button></td></tr>";*/
 
 document.getElementById("new_name").value="By";
 document.getElementById("new_country").value="";
 document.getElementById("new_age").value="0";
 }
}


 // second column code-------------------------------------------
 
function delete_row1(no)
{
 document.getElementById("row"+no+"").outerHTML="";
}


function add_row1()
{
 var new_name1=document.getElementById("new_name1").value;
 var new_country1=document.getElementById("new_country1").value;
 var new_age1=document.getElementById("new_age1").value;
 
 var table2=document.getElementById("data_table2");
 var table_len=(table2.rows.length)-1;
 
if(new_age1=="" || new_age1==null)
 {
	 alert("Credit Amount Missing, Try again.");
	 $("#new_age1").focus();
 }
 else
 {	
 
 var row = table2.insertRow(table_len).outerHTML="<tr id='row"+table_len+"'>" +
 "<td id='name_row"+table_len+"'>"+new_name1+"</td>"+
 "<td id='country_row"+table_len+"'>"+new_country1+"</td>"+
 "<td id='age_row"+table_len+"'>"+new_age1+"</td>"+
 "<td ><button type='button' class='delete' style='color:#ff5722;' onclick='delete_row("+table_len+")'><i class='fa fa-trash' aria-hidden='true'></i></button></td></tr>";

 /*"<td ><button type='button' id='edit_button"+table_len+"' class='edit' onclick='edit_row("+table_len+")'><i class='fa fa-pencil' aria-hidden='true'></i> </button><button type='button' id='save_button"+table_len+"'  class='save' onclick='save_row("+table_len+")'><i class='fa fa-save' aria-hidden='true'></i></button> <button type='button' class='delete' onclick='delete_row("+table_len+")'><i class='fa fa-trash' aria-hidden='true'></i></button></td></tr>";*/
 
 document.getElementById("new_name1").value="By";
 document.getElementById("new_country1").value="";
 document.getElementById("new_age1").value="0";
 }
}
 
 
 
 //edit table script end --------------------------------------------------------------------------------------------
 
  </script>
</body>
</html>

  
  