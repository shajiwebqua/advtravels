<?php
 include('application/views/common/header.php');?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <section class="content-header">
      <h1 style='margin-left:5px;padding-bottom:10px;'>
	  <b>Trip Invoice</b>
      <small><b>(Ctrl+P -> Print Invoice)</b></small> 
      </h1>
      <ol class="breadcrumb" style='font-size:15px;'>
	  <li><a href="<?=site_url('Trip/completed_trip');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-backward" aria-hidden="true"></i> Back to List</button></a></li>
	  <li><a href="#" onclick="PrintDiv();" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-print" aria-hidden="true"></i> Print</button></a></li>
      <li><a id='view-pdf' href="<?=site_url('Pdf/pdf_trip/'.$inv_results->trip_id);?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-save" aria-hidden="true"></i> PDF</button></a></li>
     </ol>
    </section>
<hr style='margin:0px;'>
	
  <div id="print" style="margin:0px 20px 0px 20px;";>
  <style type="text/css">
 /*td
   {
   padding:5px;
   }*/

  .brd1
  {
    border:1px solid #e4e4e4;
    
  }  
   </style>
  
  
  </head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="500px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td><td width="150px" style="padding: 0px">
<?php $date = date('Y/m/d');?>
Date: <?=$date;?>
  </td>
</tr>
<tr>
<td width="500px" style="padding: 0px"></td>
<td width="150px" style="padding: 0px">
	Invoice No :<?=$inv_results->trip_id;?>
  </td>
</tr>
</table>
<hr>

<table style="border:none;width:100%;">
  <tr>
      <td>From
          <address>
            <strong>Advance World Group</strong><br>
            Advance World Group LLC,P.O.Box : 7192<br>
            Al jurf Industrial Area, Al Jurf 1, Ajman UAE<br>
            Tel: +971 6 748 7636<br>
            Fax: +971 6 748 7637
          </address></td>
          <td align='right'> To
          <address>
            <strong><?=$inv_results->customer_name;?></strong><br>
            <?=$inv_results->customer_address;?><br>
            Mob:<?=$inv_results->customer_mobile;?><br>
            Phone:<?=$inv_results->customer_landline;?><br>
            Email: <?=$inv_results->customer_email;?>
          </address></td>
  </tr>
</table>
<hr>
  <h4>Details:</h4>
<table width="100%" >
<tr>
<th align='left'>Purpose</th><td>:<?=$inv_results->trip_purpose;?></td>
<th align='left'>Time(Start-End)</th><td>:<?=$inv_results->trip_starttime.' '.'-'.$inv_results->trip_endtime;?></td></tr>
<tr><th align='left'>Vehicle No</th><td >:<?=$inv_results->trip_vehicle_regno;?></td>
<th align='left'>Km(Start-End)</th><td>:<?=$inv_results->trip_startkm.' '.'=>'.$inv_results->trip_endkm;?></td></tr>
<tr><th align='left'>Driver Name</th><td>:<?=$inv_results->trip_drivername;?></td>
<th align='left'>Running KM</th><td>:<?php $end=$inv_results->trip_endkm;$start=$inv_results->trip_startkm; ?> 
<?=abs($end-$start);?></td></tr>
<tr><th align='left'>Trip(From - To )</th><td>:<?=$inv_results->trip_from.' '.'-'.$inv_results->trip_to;?></td>
<th align='left'>Date(Start-End)</th><td>:<?=$inv_results->trip_startdate.' '.'-'.$inv_results->trip_enddate;?></td></tr>
<tr><th align='left'>Minimum Charge KM</th><td>:<?=$inv_results->trip_minchargekm;?></td><td>Minimum Charge: </td> <td>:<?=$inv_results->trip_mincharge;?></td></tr>
</table>
<hr>

<table width='100%' cellpadding=0 cellspacing=0>
<tr><td colspan='6' height=40px ><b>Minimum Charges:</b></td>
<tr><td class='brd1' width='100px'>Min. Days:</td>
<td class='brd1'>:&nbsp;&nbsp;<?=$inv_results->trip_mindays;?></td>
<td class='brd1'  style='text-align:right; padding-right:10px;'>Minimum Charge</td>
<td class='brd1' width='100px'>:&nbsp;&nbsp;<?=$inv_results->trip_mincharge;?></td>
<td class='brd1' style='text-align:right; padding-right:10px;'>Minimum Total</td>
<td class='brd1' width='120px'> :&nbsp;&nbsp;<?=$inv_results->trip_mintotal;?></td>
</tr>
<tr><td colspan=6 height=40px><b>Additional Charges:</b></td></tr>
<tr><td class='brd1' width='100px' class='brd1'>Addi.KM:</td>
<td class='brd1'>:&nbsp;&nbsp;<?=$inv_results->trip_addikm;?></td>
<td class='brd1' style='text-align:right; padding-right:10px;'>KM Charge:</td>
<td class='brd1' width='100px'>:&nbsp;&nbsp;<?=$inv_results->trip_addikmcharge;?></td>
<td class='brd1' style='text-align:right; padding-right:10px;'>Addi. Total:</td>
<td class='brd1' width='120px'>:&nbsp;&nbsp;<?=$inv_results->trip_additotal;?></td>
</tr>

<tr><td colspan=6 height=40px><b>Other Charges:</b></td></tr>
<tr><td class='brd1' width='100px' class='brd1'>Toll/Parking:</td>
    <td  class='brd1'>:&nbsp;&nbsp;<?=$inv_results->trip_tollparking;?></td>
    <td  class='brd1' style='text-align:right; padding-right:10px;'>Interstate Permit:</td>
    <td  class='brd1'> :&nbsp;&nbsp;<?=$inv_results->trip_interstate;?></td>
    <td class='brd1' style='text-align:right; padding-right:10px;'>Other Charges:</td>
    <td class='brd1' >:&nbsp;&nbsp;<?=$inv_results->trip_othercharge;?></td>
</tr>
</table>

 <hr>
 <?php $otherchargetotal=$inv_results->trip_tollparking+$inv_results->trip_interstate+$inv_results->trip_othercharge;?>
  <table width="100%">
   <tr height='25px'>  <th width='85%' style='text-align:right;padding-right:25px;'>Total Trip Charges</th><td width='20px;'>:</td>
			   <?php
			   $tc=$inv_results->trip_charge-$otherchargetotal;
			   if($tc<0)
			   {
				   $tc=0;
			   }
			   ?>
   
                <td width='10%' style='text-align:right;'><?=number_format($tc,2,".","");?></td>
				<td width='3%'></td>
              </tr>
			  
              <tr height='25px'>
                <th width='85%' style='text-align:right;padding-right:25px;'>Other Charges</th>
				<td width='20px;'>:</td>
                <td width='10%' style='text-align:right;'><?=number_format($otherchargetotal,2);?></td>
				<td width='3%'></td>
              </tr>
			  
             <tr height='25px'>
                <th width='85%' style='text-align:right;padding-right:25px;'>Grand Total</th>
				<td width='20px;'>:</td>
                <td width='10%' style='text-align:right;'><b><?=number_format($inv_results->trip_gtotal,2);?></b></td>
				<td width='3%'></td>
              </tr>
 </table>
 <table width="100%">
 <tr><td colspan='3' height='30px'></td> </tr>
             <tr>
                <th width='86%' style='text-align:right;padding-right:25px;font-size:25px;'>RS : </th>
                <td width='10%' style='text-align:right;font-size:25px;'><b><?=number_format($inv_results->trip_gtotal,2);?></b></td>
				<td width='3%'></td>
              </tr>
			  <tr>
                <th width='86%' style='text-align:right;padding-right:25px;font-size:25px;'>=====</th>
                <td width='10%' style='text-align:right;font-size:25px;'><b>===========</b></td>
				<td width='3%'></td>
              </tr>
 </table>

</div>

    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
  <?php
 include('application/views/common/footer.php');?>
 <script type="text/javascript">     
    function PrintDiv() {    
       var divToPrint = document.getElementById('print');
       var popupWin = window.open('', '_blank', 'width=1000,height=600');
       popupWin.document.open();
       popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
       popupWin.document.close();
        }
 </script>
 <script type="text/javascript">
   $('#view-pdf').on('click',function(){
    var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Trip Invoice',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); 
       
     return false; 

    });    
 </script>