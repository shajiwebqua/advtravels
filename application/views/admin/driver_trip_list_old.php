<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}
</style>

 <!-- Content Wrapper. Contains page content -->
 
<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Driver Trip Details</b></h1>

 </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header  end -->	  
	  
  <section class="content"> 
    <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>  
  <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">
    <div style="background-color:#fff;border-radius:3px;padding:15px; ">
					
	<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
<!--- search ---------------------------------------- -->
				
		<div class="row">
         <div class="col-md-6" style='border-right:1px solid #e4e4e4;'>
              		<label class="col-md-5  control-label" style="padding-top:3px;text-align:right;"><b>Select Driver Name:</b> </label>
					<div class="col-md-7">
					<select class="form-control" id="drivers" name="drivers">
						<option value="default">--Select Driver--</option>
						<?php
							foreach($drivers as $driver)
							{
								echo "<option value='".$driver->driver_id."'>".$driver->driver_name."</option>";
							}
						?>
					</select>
					</div>
          </div>
		  
          <div class="col-md-4">
            <div class="form-group">
              <label class="col-md-5 control-label" style="padding-top:3px;" >Select From Date : </label>
              <div class="col-md-5">
                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                  <input type="text" class="form-control date-pick" readonly id="datepicker1" value="<?php echo date('d-m-Y'); ?>">
                </div> 
              </div>
			   <div class="col-md-2">
            <button class="btn btn-success" onclick="load_drivers()">Get</button>
			</div>
            </div>
          </div> 
          </div> 
	</div>

              <label style="background-color:#cecece;width:100%;height:1px;margin:20px 0px 20px 0px;"></label>
				</div>
			
				</form>
<!-- ***********************************************  search end  ******************************************** -->

				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                 <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
									<table class="table table-striped table-hover table-bordered" id="example">
									  <thead>
										<tr style='color:#4b88ed'>
										 <th>Trip To</th>
										 <th>Start Date</th>
										 <th>End Date</th>
										 <th>No of Days</th>
										 <th>Trip Charge</th>
										 <th>Driver Charge</th>
										 <th>Driver Bata</th>
										 <th>Total</th>
									   </tr>
									 </thead>
									 <tfoot>
									   <td>Total</td>
									   <td></td>
									   <td></td>
									   <td></td>
									   <td></td>
									   <td id="driver_charge"></td>
									   <td id="driver_bata"></td>
									   <td id="driver_total"></td>
									 </tfoot>
								   </table>
                        </div>
						<label style="background-color:#cecece;width:100%;height:1px;"></label>
                        <div class="row"></div>
                        </div>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>

</section>
</div>
<!-- /.content-wrapper --> 
<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------
 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
    endDate: 'now'
   });
   
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
   });
    
  $("#drivers").change(function(){
      load_drivers()
      load_drivers_payments();
  });
  
    function load_drivers()
	{
      var driver_id = $('#drivers').val();
      var date = $("#datepicker1").val();
      if(driver_id == "default"){
        return;
      }
    
    $('#example').dataTable( {
     "ordering": false,
         destroy: true,
        "processing": true,
        "initComplete": function () {
            var api = this.api();
              length = api.rows().data().length;
              if(length > 0){
                driverCharge  = 0
                driverBata    = 0
                var i         = 0
                for(i;i<length;i++){
                  driverCharge  += parseInt(api.row(i).data().dcharge)
                  driverBata  += parseInt(api.row(i).data().dbata)
                }
                $("#driver_charge").text(driverCharge);
                $("#driver_bata").text(driverBata);
                $("#driver_total").text(driverCharge + driverBata);
                $("#driver_id").val(driver_id);
              }
            
        },
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Driver/driver_triplist_ajax/" + driver_id + "/" + date
               },
    "columnDefs":[
    {"width":"10%","targets":0},
    {"width":"12%","targets":1},
    {"width":"12%","targets":2},
    ],
         
        "columns": [
      { "data": "tripid" },
      { "data": "startdate"},
      { "data": "enddate"},
      { "data": "days"},
      { "data": "tcharge"},
      { "data": "dcharge" },
      { "data": "dbata" },
      { "data": "total" }
       ]
    });
    }

var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });	

     function load_drivers_payments(){
      var driver_id = $('#drivers').val();
      if(driver_id == "default"){
        return;
      }
      return;
      jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Driver/driver_cash",
        dataType: 'html',
        data: {driverid: driver_id},
        success: function(data) {
          data = JSON.parse(data)
          $("#ttl").val(data.total)
          $("#payed").val(data.payed)
          $("#payable").val(data.payable)
        }
      });
    }

 function check(obj)
	{
    pay = parseFloat($(obj).val());
    balance = parseFloat($("#balance").val());
    if(pay > balance){
      pay = pay.toString()
      pay = pay.substr(0, pay.length - 1);
      console.log("pay : " + pay);
      $(obj).val(pay)
    }
  }
   
		$(document).ready(function(){
        load_drivers();
        load_drivers_payments();
    });	

  
  
/*  $('#view-pdf').on('click',function(){
       var id=$('#example').find('tr.selected').find('td').eq(0).text();
     if(id=="")
     {
       alert ("please Select customer details..");
     }
     else
     {
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
		var pdf_link = $(this).attr('href')+'/'+id;
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Customer Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
       } return false; 

    });  */  

		$(document).ready(function(){
        load_drivers();
    });
  
	
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

      //$.fn.dataTable.ext.errMode = 'throw';
</script>



