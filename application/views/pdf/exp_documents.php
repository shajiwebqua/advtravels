<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
td{
	padding:5px 10px 5px 10px;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

 <table width="100%"><tr><td><h3><u><?php echo $dcap;?></u></h3></td><td align='right' >Date : <?php echo date('d-m-Y');?></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
 
 		<table class="table table-striped table-hover table-bordered" id="example" width='100%'>
              <thead>
                <tr>
                 <tr>
				 <th>SlNo</th>
                 <th>Vehicle</th>
				 <th>Type</th>
                 <th>Document</th>
				 <th>Doc-No</th>
                 <!--<th>Validity from</th> -->
				 <th>Expiry_Date</th>
				 <th>Status</th>
                </tr>
				<tr><td colspan='7'><hr style='margin:0px;'></td></tr>
                </thead>
				
				<tbody>
			<?php
			  $rgname="";
			  $loca="";
			  $slno=1;
			foreach ($docs as $r)
			{
				$d1=strtotime($r->doc_valid_to);
					$d2=strtotime(date('Y-m-d'));
					$days=floor(($d1-$d2) / (60*60*24));
					if($days<=$r->doc_reminder)
					{
					
					$vto=$r->doc_valid_to;
					$d1=explode("-",$vto);
					$exdate=$d1[2]."-".$d1[1]."-".$d1[0];
					
					if($days<=0)
					{
					$stat="<font color=red>Expired</font>";
					$exdt="<font color=red>".$exdate."</font>";
					}
					else
					{
						$stat="Expired with in <b>".$days. "</b> days";
						$exdt=$exdate;
					}
				?>
							
				 <tr style='border-bottom:1px solid #000;'></tr>
				 <td><?=$slno;?></td>
                 <td><?=$r->vehicle_regno;?></td> 
                 <td><?=$r->veh_type_name;?></td>
				 <td><?=strtoupper($r->doc_document);?></td>
                 <td><?=$r->doc_document_no;?></td>
				 <th><?=$exdt;?></th>
				 <td><?=$stat;?></td>
                </tr>
				
				<?php
				$slno++;
					}
					
				}
				?>
				</tbody>
            </table>
</body>
</html>