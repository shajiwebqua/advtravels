<?php
 include('application/views/common/header.php');?>
<style>
.colpad
{
	padding-left:3px;
	padding-right:3px;
}

.numericCol
{
	text-align:right;
}

.chkCol
{
	text-align:center;
}


/*create invoice box style */
.inv-div
{
	position :fixed;
	top:0;
	left:0px;
	width:100%;
    height:100%;
background-color:;
z-index:9999;
display:none;	
}
.inv-div .content
 {
   border:2px solid #416796;
   position :absolute;
   top:100px;
   right:10px;
   width:470px;
   height:550px;
   background-color:#fff;
   z-index:9999;
   padding:0px;
}

.inv-div.open-div
{
	display:block;

}
a.view{padding: 3px; border: 1px solid transparent; border-radius:3px}
a.view:hover{background-color:#ddd !important; border-color: #999;}

a.invtrips{padding: 3px; border: 1px solid transparent; border-radius:3px}
a.invtrips:hover{background-color:#ddd !important; border-color: #999;}

.head-row
{
   background-color:#416796;
   color:#fff;
   padding:5px 2px 5px 2px;
   margin-left:0px;
   margin-right:0px;
}
.dt-row
{
   padding:5px 2px 5px 10px;
   margin-left:0px;
   margin-right:0px;
}

.p-col
{
	padding-left:2px;
	padding-right:0px;
}

.p-btn{
	padding:5px 15px 5px 15px;
}
/*---------------------------- */

.inv-btn-active{
	border:1px solid red;
	padding:3px;
}

.inv-btn-leave{
	border:0px;
	padding:0px;
}


</style>

<?php 
$cres=$this->db->select('customer_id,customer_name')->from('customers')->get()->result();
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
  <!-- Content Header (Page header) -->
  <section class="content-header">
  <h1><b>Paid Invoice Details</b></h1>
  <ol class="breadcrumb" style="font-size:15px;">
    <li><a id='view-pdf' href="<?php echo site_url('Pdf/paid_invoice') ?>" style='color:#4b88ed;'><button class='btn btn-info'><i class="fa fa-print" aria-hidden="true"></i> Print</button></a></li>
	<li> <a  href="<?php echo base_url('Invoice/Unpaid');?>"  style="color:#fff;"> <button  class='btn btn-primary' > <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Unpaid Invoice</button></a></li>
	<li> <a  href="<?php echo base_url('Invoice/Trips');?>"  style="color:#fff;"> <button  class='btn btn-warning' > <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Generate Invoice</button></a></li>
	
	
  </ol>
  
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->   
    
  <section class="content"> 
  <!-- <div style="color:#4b88ed;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , And click on the button (Print,Share,Edit,View,Delete)  </div> -->
    <div style="padding:2px 0px 2px 0px;">
		 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
		 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div>
		 <div id='mserr'><?php echo $this->session->flashdata('message1'); ?></div>
		 </center>
		 </div>
	</div>
				
  <div class="row">
  
<!-- place custom model here ---------------->

<div class="col-md-12">
<div class="portlet-body">
<div style="background-color:#fff;padding:15px; ">
              
		<div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
        <div class="page-content">

		<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
			
		<!-- search options -------------------------------------------->
		
	    <div class='row' style='margin:0px;background-color:#e4e4e4;padding:5px;'>
	    <form role="form" method="POST" action="<?php echo base_url('Invoice/Paid/1')?>" enctype="multipart/form-data">
		
			<div class='col-md-1' style='padding-top:5px;'>	<label class='control-label'><b>Select:</b> </label>	</div>

			<div class='col-md-4 ' style='padding-top:2px;'>
				<select class="form-control select2" id="cltype" name="cltype" required>
					<option value="">--select client---</option>
						<?php
						foreach($cres as $cs)
						{
							echo "<option value='".$cs->customer_id."'>".$cs->customer_name."</option>";
						}
					?>
				</select>
				</div>
				<div class='col-md-2 ' style='padding-top:2px;'>
					<input type='text' class='form-control' name='sdate' id='datepicker1' placeholder='date' required>
				</div>
				<div class='col-md-2 ' style='padding-top:2px;'>
					<input type='text' class='form-control' name='edate' id='datepicker2' placeholder='date' required>
				</div>
				<div class='col-md-1 ' style='margin-top:2px;'><input type="submit" id="btnget" class="form-control btn btn-success" style="text-align:center;" name='btnget'  value="Get" > </div>
		</form>
				
		<form role="form" method="POST" action="<?php echo base_url('Invoice/Paid/2')?>" enctype="multipart/form-data">
				<div class='col-md-2 text-right' style='margin-top:2px;'><input type="submit" id="btnall" class="btn btn-info" style="text-align:center;padding:3px 15px 3px 15px;" name='btnall'  value="All" > </div>
		</form>

		</div>
	
	<!-- search options -------------------------------------------->
					
		 <div class="row" style='margin-top:10px;'>
                   <div class="col-md-12">
				   <label id="btitle" style='font-weight:bold;margin-bottom:20px;'><font color=#19a895><u>Paid Invoice List</u></font></label>
				   </div>
				   </div>
		
            <div class="row" id='booklist'>
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
				   <div class="table-toolbar">
					<div class="row">
					<div class="col-md-12">
                  
                   </div>
                   </div>
                   </div>
             
            <table class="table table-striped table-hover table-bordered" id="example" width='100%'>
              <thead>
                <tr>
				 <th>Id</th> 
 				 <th>Date</th> 
                 <th>Invoice_No</th>
				 <th width='25%'>Customer</th>
				 <th >Status</th>
				 <th>Amount</th>
				 <th>GST%</th>
				 <th>GST</th>
				 <th>Total</th>

				 </tr>
                </thead>
				<tbody>
				
				<?php
				$tot=0;
				$gtot=0;
				foreach($invopaid as $pd)
				{
					if($pd->invo_status==1)
					{$st="<font color='green'>Paid</font>";}
					else{$st="<font color='#ef6e6e'>unpaid</font>";}
					
					$tot+=($pd->invo_amount+$pd->invo_gst);
					$gtot+=$tot;
					$invview="<center><a  href='".site_url('Pdf/pdf_invoice')."' id='".$pd->invo_invoiceid."' class='view'><b><font color=green> INV-".$pd->invo_invoiceid."</font></b></a></center>";
				?>
				
				 <tr>
				 <td><?=$pd->invo_id;?></td> 
 				 <td><?=date_format(date_create($pd->invo_date),'d-m-Y');?></td> 
				 <!--<td>INV-<?=$pd->invo_invoiceid;?></td>-->
				 <td><?=$invview;?></td>
                 <td width='25%'><?=$pd->customer_name;?><br><?=nl2br($pd->customer_address);?></td>
				 <td style='color:green;'><?=$st;?></td>
				 <td align='right'><?=number_format($pd->invo_amount,'2','.','');?></td>
				 <td align='right'><?=$pd->invo_gstper;?>%</td>
				 <td align='right'><?=number_format($pd->invo_gst,'2','.','');?></td>
				 <td align='right'><b><?=number_format($tot,'2','.','');?></b></td>
				 </tr>
				
				<?php
				}
				?>

				</tbody>
				<tfoot>
					 <tr style='font-size:17px !important;text-align:right;'>
					 <td colspan=7>Total Paid Amount:</td>
					 <td colspan=2>
							<b><?=number_format($gtot,'2','.','');?></b>
						</td>
					 </tr>
				</tfoot>
            </table>

        </div>
                <!-- END BORDERED TABLE PORTLET-->
		</div>
		</div>
	
                <!-- END CONTENT BODY -->
	</div>
	</div>
            <!-- END QUICK SIDEBAR -->
</div>
  
	 <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Record Payments</h4>
				</div>
				<div id='mcontent'></div>
			</div>
			</div>
		<!-- /.modal-dialog -->
		</div>
	 </div> 
					 
  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
<!-- /.content --> 
</section>
  <!-- /.content-wrapper -->
</div>
 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">


var table="";
$(".select2").select2();

$('#example').dataTable();
  
  
setInterval(function(){ $('#idh5').html(''); }, 3000);

$("#idmsg").hide();
$("#msgerr").hide();

//sweet alert ----------------------------
	if($.trim($("#idh5").html())!="")
	{
		swal( $("#idh5").html(),"","success")
		$("#idh5").html("");
	}
	
	if($.trim($("#msgerr").html())!="")
	{
		swal($("#idh5").html(),"","error")
		$("#idh5").html("");
	}
	
// sweet alert box -------------------------- 

 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
 
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   }); 
		
$('#view-pdf').on('click',function(){

        var pdf_link = $(this).attr('href');

		var iframe = '<div class="iframe-container"><div id="overlay"><div id="loading"></div><iframe id="myDiv" class="animate-bottom" src="'+pdf_link+'"></iframe></div></div>';
      $.createModal({
        title:'Paid Invoices',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
     return false; 

 });    
	
	
	
$('#example tbody').on('click', '.view',function(){

        var pdf_link = $(this).attr('href');
		var ivid=$(this).attr('id');
		
		//alert(pdf_link+"/"+ivid);
		
		var iframe = '<div class="iframe-container"><div id="overlay"><div id="loading"></div><iframe id="myDiv" class="animate-bottom" src="'+pdf_link+'/'+ivid+'"></iframe></div></div>';
      $.createModal({
        title:'Paid Invoices',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
     return false; 

 });    	
 
/*
/*var tid="";
    $.each($("#example tr.selected"),function(){ 
        tid+=","+$(this).find('td').eq(1).text(); 
    });
    $("#trip_ids").val(tid);
	if($.trim(tid).length<=0)
		{
			swal("Cancelled", "Please select unpaid trips.","");
		}
		else
		{
		$(".inv-div").addClass('open-div');
		}
	*/

	
// -----------------------------------------------------------------------------------------------------------------------------------    
    
  $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this booking?');
   });

		
  $(document).on("click", "#del_conf1", function () {
           return confirm('Are you sure you want to Cancel this booking?');
  });

		
</script>



