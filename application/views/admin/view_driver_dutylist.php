<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Drivers Duty List</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px'>
   </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->
              
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
   <div class="page-content">
   
   <div style="padding:0px 15px 0px 15px;">
				 <div id="idmsg"  style="background-color:#fff;height:25px;margin-bottom:5px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div>
				 <div id='msgerr'><?php echo $this->session->flashdata('message1'); ?></div>
				 <div id='msgdel'><?php echo $this->session->flashdata('message2'); ?></div>
				 </center></div>
	</div>
 
        <div class="col-md-12" style='padding-left:0px;padding-right:0px;'>
                     <div style="background-color:#fff;padding:10px; ">
       
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                
                <div class="row" style='background-color:#e4e4e4;margin:0px;padding-top:8px;padding-bottom:8px;'>
                   <div class="col-md-12">
					<form role="form" method="POST" action="<?php echo base_url('General/drivers_duty/1')?>" enctype="multipart/form-data">
		
					<div class='col-md-2 ' style='padding-top:5px;text-align:right;'>	<label class='control-label'>Select Date  : </label>	</div>
					<div class='col-md-2 ' style='padding-top:2px;'><input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
					
					
					<div class='col-md-1 ' style='margin-top:2px;'><input type="submit" class="form-control btn btn-success" style="text-align:center;"  value="Get" name='btnget' id="btnget"> </div>
					</form>
					<form role="form" method="POST" action="<?php echo base_url('General/drivers_duty/2')?>" enctype="multipart/form-data">
					<div class='col-md-2 ' style='padding-top:5px;text-align:right;'>	<label class='control-label' >Select Month  : </label>	</div>
					<div class='col-md-2 ' style='padding-top:2px;'>
						<select class="form-control" id="cltype" name="month">
							<option value="">--month---</option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>
					</div>
					   <div class="col-md-1" style='padding:2px 0px 0px;'>
						   <select name='syear' class='form-control' id="syear" required>
						   <option value='' >--year--</option>
								<?php
								$y=date('Y')+1;
								for($x=$y;$x>=2017;$x--)
								{
								?>
								<option value='<?=$x;?>' <?php if($x==date('Y')) echo "selected";?>><?=$x;?></option>
								<?php
								}
								?>
							</select>
						   </div>
					
				<div class='col-md-1 ' style='margin-top:2px;'><input type="submit" class="form-control btn btn-success" style="text-align:center;"  value="Get" name='btnget' id="btnget"> </div>
				</form>
						
				</div>
                </div>
				
								
				<div class="row" style='margin-top:10px;'>
                        <div class="col-md-12" >
						<label style='font-size:16px;'>Duty List of : <b><?php echo $sop;?></b></label><br>
						<hr style='margin:0px 0px 20px 0px;'>
						</div>
                </div>
                
				
                <table class="table table-striped table-hover table-bordered" id="example">
					<thead>
					<tr>
					 <th>Action</th>
					 <th>ID</th>
					 <th>Date</th>
					 <th>Driver Name</th>
					 <th>Mobile</th>
					 <th>Shift</th>
					 <th>Time</th>
					</tr>
					</thead>
					<tbody>
				<?php
				if(isset($dresult))
				{
				foreach($dresult as $r) 
               {
               	$edit="<a href='#myModal2' id='".$r->drv_id."' data-toggle='modal'  class='edit open-AddBookDialog2'>
				<button class='btn btn-warning btn-xs' data-title='edit' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></a>";
				$mm='<button class="btn btn-danger btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button>';
				$del=anchor('General/del_duty_entry/'.$r->drv_id, $mm, array('id' =>'del_conf'));
             
				 if($r->drv_shift=='1')
					 $st='DAY';
				 else
					 $st='NIGHT';
			   ?>
					<tr>
					 <td width='70px'><?=$edit."&nbsp;".$del;?></td>
					 <td><?=$r->drv_id;?></td>
					 <td><?=date_format(date_create($r->drv_date),'d-m-Y');?></td>
					 <td><?=$r->driver_name;?></td>
					 <td><?=$r->drv_mobile;?></td>
					 <td><?=$st;?></td>
					 <td><?=$r->drv_time;?></td>
					</tr>
	 
               <?php
				}
				}
				?>
					
				</tbody>
                </table>
                </div>
				      <!-- END BORDERED TABLE PORTLET-->
                </div>
                </div>
                </div>
		</div> <!-- end Row --->
                <!-- END CONTENT BODY -->
            <!-- </div> -->
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
			  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times </button>
                  <h4 class="modal-title">Edit driver duty</h4>
                </div>
                <div class="modal-body" >
                			
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
        
 
  <!-- End user details -->
  </div>
  
   <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 

</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">


 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	/*endDate:'now'*/
   });
//sweet alert box ----------------------
$("#idmsg").hide();
if($.trim($("#msg").html())!="")
	{
		swal("Saved",$("#msg").html(),"success")
		$("#msg").html("");
		$("#msgerr").html("");
	}
	
$("#msgerr").hide();
if($.trim($("#msgerr").html())!="")
	{
		swal("Try Again",$("#msgerr").html(),"error")
		$("#msgerr").html("");
		$("#msg").html("");
	}

	$("#msgdel").hide();
if($.trim($("#msgdel").html())!="")
	{
		swal("Deleted",$("#msgdel").html(),"success")
		$("#msgdel").html("");
	}
//-------------------------------------
 
 $("#drname").change(function()
 {
	var did=$("#drname").val();
	
	jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/Get_driver_mobile",
        dataType: 'html',
        data: {drid: did},
        success: function(res)
 		{
			$("#drmobile").val(res);
            }
            });
	
 });
 
 
 $("#drshift").change(function()
 {
	 var st=$("#drshift").val();
	 if(st=='1')
	 {
		 $("#drtime").val("6 AM - 8 PM");
	 }
	 else if(st=='2')
	 {
		 $("#drtime").val("8 PM - 6 AM");
	 }
	 $("idsubmit").focus();
 });
 
   $("#example").dataTable();
   

   $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/drv_duty_edit",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 
 	
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#msg').html(''); }, 3000);
 //});
  
</script>



