<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Customers</b></h1>
  <ol class="breadcrumb" style="font-size:15px;">
  <li><a href="<?php echo base_url('Customer/add_customer');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-address-card" aria-hidden="true"></i> New Customer</button></a></li> 
  <li><a id='view-pdf' href="<?php echo site_url('Pdf/customer') ?>" style='color:#4b88ed;' ><button class='btn btn-primary'><i class="fa fa-print" aria-hidden="true"></i> Print</button></a></li> 
  <li><a id='idsave' href="" style='color:#4b88ed;' ><button class='btn btn-primary'><i class="fa fa-save" aria-hidden="true"></i> Save</button></a></li> 
  
	<!--data-target="#myModalsh" --><li><a id='idshare' href="" data-toggle='modal' style='color:#4b88ed;' ><button class='btn btn-primary'><i class="fa fa-share-alt" aria-hidden="true"></i> Share</button></a></li> 
	<!--data-target="#myModalv" --><li><a id='idview' href="" data-toggle='modal' style='color:#4b88ed;' ><button class='btn btn-primary'><i class="fa fa-eye" aria-hidden="true"></i> View</button></a></li>
  
  <?php
  //if(checkisAdmin()){
    //	echo "
      //  <li><a id='idedit' href='#' data-target='#myModal3' data-toggle='modal' style='color:#4b88ed;' ><i class='fa fa-pencil' aria-hidden='true'></i>Edit</a></li>
      //  <li><a id='iddelete' href='' style='color:#4b88ed;' ><i class='fa fa-trash' aria-hidden='true'></i>Delete</a></li> 
      //";
   //} 
  ?>
  </ol>
  
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->   
    
  <section class="content"> 
  <div class='row'>
   <label class='col-md-8' style="color:#4b88ed;font-size:12px;"> Select Customer details (<b>CLICK ON THE TABLE ROW</b>) , And click on the button (Print,Save,Share,View)  </label>
  <!-- <label class='col-md-4 ' style="text-align:right;"><a href="#" data-target="#myModalCOR" data-toggle='modal' class="btn btn-info"> Corporate Contacts</a>  </label> -->
  </div>
  
      <div style="padding:2px 0px 2px 0px;">
		 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
		 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div></center>
		 </div>
	</div>
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->

<div class="portlet-body">
<div style="background-color:#fff;padding:15px; ">
              
		<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                        
        <!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
        
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
					<div class="table-toolbar">
					<div class="row">
					<div class="col-md-12">
                  
                   </div>
                   </div>
                   </div>
                
         <table class="table table-striped table-hover table-bordered" id="example">
              <thead>
                <tr>
				 <th>Action</th>
                 <th>ID</th>
                 <th>Code</th>
				  <th>Type</th>
                 <th>Date</th>
                 <th>Name</th>
                 <th>Mobile</th>
                 <th>C/o Agent</th>
                 <th>Country</th>
                 <th>Area</th>
                </tr>
                </thead>
            </table>
        </div>
                       <!-- END BORDERED TABLE PORTLET-->
		</div>
		</div>
                <!-- END CONTENT BODY -->
	</div>
	</div>
            <!-- END QUICK SIDEBAR -->
</div>
  
<div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Add</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
                   </div> 




        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Add</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
                   </div> 

          <div class="modal fade draggable-modal" id="myModalv" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">View</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
          </div> 

                     <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div> 

                    <div class="modal fade draggable-modal" id="myModalsh" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Share</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div> 
                    
		<!--<div class="modal fade draggable-modal" id="myModalCOR" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Corporate Contacts</h4>
				  <hr style='margin-top:5px;'>
					<div class="row" style='padding-top:10px;'>
					<div class="col-md-12">
					  <table id="TblCorporate" class="table table-striped table-hover table-bordered" cellspacing="0">
						<thead>
						<tr>
							<th>ID</th>
							<th>Corporate Name</th>
							<th>Key Person</th>
							<th>Contact Person</th>
							<th>Account Person</th>
						</tr>
						</thead>
					 </table>
					</div>
					</div>				 
				  
                </div>
				
				 <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
				</div>
				
              </div>
              </div>
            </div>
          </div> -->
					
      
  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
<!-- /.content --> 
</section>
  <!-- /.content-wrapper -->
</div>
 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 $("#idmsg").hide();

//sweet alert ----------------------------
	if($("#idh5").html()!="")
	{
		swal( $("#idh5").html(),"","success")
		$("#idh5").html("");
	}
// sweet alert box -------------------------- 

 $(document).ready(function()
 {
    $('#view-pdf').on('click',function(){
       var id=$('#example').find('tr.selected').find('td').eq(1).text();
     if(id=="")
     {
       alert ("please Select customer details..");
     }
     else
     {
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
		var pdf_link = $(this).attr('href')+'/'+id;
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Customer Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
       } return false; 

    });    

  
  $('#example').dataTable({
    "ordering":true,
         destroy: true,
        "processing": true,

        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Customer/customer_ajax",// json datasource
               },
  
    "columns": [
	{ "data": "action" },
      { "data": "custid" },
      { "data": "code" },
	  { "data": "ctype"},
      { "data": "date" },
      { "data": "name"},
	  { "data": "mobile"},
      { "data": "coagent"},
      { "data": "country"},
      { "data": "area"},
        ]
  } );
  
  
  $('#TblCorporate').dataTable({
    "ordering":true,
         destroy: true,
        "processing": true,

        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Customer/corporate_ajax",// json datasource
               },
  
    "columns": [
      { "data": "custid" },
      { "data": "name"},
	  { "data": "kperson"},
      { "data": "cperson"},
      { "data": "aperson"},
       ]
  } );
  
  
  
  
  
  
  
  
  
  
  
 var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
          table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );

  

  $('#example tbody').on('click', '.add', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         // var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Customer/add_customer",
        dataType: 'html',
        // data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
        }); 
    
    
    
    
    
//   new code  --------------------------------------------------------------------------------------------------------------
   //   $('#example tbody').on('click', '.edit', function () {
     
  $('#example tbody').on('click','.edit',function(e)
  { 
    var Result=$("#myModal2 .modal-body");
    var id=$(this).attr('id'); //#example').find('tr.selected').find('td').eq(0).text();
    if(id=="")
     {
       alert ("please Select customer details..");
       e.stopPropagation();
     }
     else
     {
      jQuery.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "Customer/edit_customer",
      dataType: 'html',
      data:{uid:id},
      success: function(res) {
      Result.html(res);
       }
      });
     }
 }); 

 

 $('#idshare').click(function ()
  { 
    var Result=$("#myModalsh .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         //var id =  $(this).attr('id');
     var id=$('#example').find('tr.selected').find('td').eq(1).text();
     //alert(id);
     if(id=="")
     {
       alert ("please Select customer details..");
     }
     else
     {
	  $('#idshare').attr('data-target','#myModalsh'); 
      jQuery.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "Customer/share_customer",
      dataType: 'html',
      data:{id:id},
      success: function(res) {
      Result.html(res);
       }
      });
     }
   }); 



    $('#idsave').click(function ()
    {   
        //var Result=$("#myModalsa .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         //var id =  $(this).attr('id');
         var id=$('#example').find('tr.selected').find('td').eq(1).text();
     //alert(id);
         if(id=="")
         {
             alert ("please Select customer details..");
         }
         else
         {
            jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "Customer/save_customer",
            dataType: 'html',
            data:{uid:id},
            success: function(res) {
            Result.html(res);
             }
            });
         } 
        }); 
  
  
    $('#idview').click(function () 
    {
        var Result=$("#myModalv .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
        //var id =  $(this).attr('id');
        var id=$('#example').find('tr.selected').find('td').eq(1).text();
    if(id=="")
    {
       alert ("please Select customer details..");
    }
    else
    {
		$('#idview').attr('data-target','#myModalv');
    //alert(id); 
		 var id=$('#example').find('tr.selected').find('td').eq(1).text();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Customer/view_customer",
        dataType: 'html',
        data: {id:id},
        success: function(res) {
        Result.html(res);
            }
          });
    }
        }); 
    
  
    
    
// -----------------------------------------------------------------------------------------------------------------------------------    
    
     $(document).on("click", "#del_conf", function ()
	 {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#idh5').html(''); }, 3000);


});
</script>



