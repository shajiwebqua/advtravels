<?php include('application/views/common/header.php'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1 class='heading' style='color:#0a1b58;padding-bottom:15px; font-weight:bold;'>Profit & Loss Accounts
           </h1>
	  <ol class="breadcrumb" style="margin-right:20px;font-size:13px;">
        <li><a id="view-pdf" href="<?php echo base_url('Pdf/profit_loss');?>"><button class='btn btn-info'>Print </button> </a> </li>
      </ol>
	  
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
		  
         <!--   <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Details</h3> 
            </div>
			
			<!---------  filter row --->
			
			<div class='row' style='margin:7px 5px 0px 5px;background-color:#e4e4e4;padding:5px;'>
			<form role="form" method="POST" action="<?php echo base_url('Account/profit_loss/1')?>" enctype="multipart/form-data">
             			
			<div class='col-md-2' style='padding-top:5px;'>	<label class='control-label'>Select Date [from-to] : </label>	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			<div class='col-md-1' style='padding-top:2px;'><input type='submit' name='btndetails' class='btn btn-primary' value='Get'></div>
			</form>
			</div>
			
			<!--------  filter row --->

            <!-- /.box-header -->
            <div class="box-body" style='min-height:450px;'>
			<div class='row'>
				<div class='col-md-12'>
				<label style='font-weight:700;margin-left:20px;'> <?php echo $this->session->userdata('acperiod1');?></label>
				</div>
			</div>
			  <div class="row">
	            <div class="col-md-10">
				<table class="table table-hover table-bordered" id="example"  width='100%' border=0 style="font-size:14px">
             <thead>
                <tr>
				<th width='20px'></th> 
				<th>Particulars</th> 
				<th style='width:120px;text-align:right;'>Debit</th>
                <th style='width:120px;text-align:right;'>Credit</th> 
                </tr>
                </thead>
				<tbody>
				
				<?php
				$drtot=0;
				$crtot=0;

				foreach ($incomes as $pr1)
				{
					$drtot+=$pr1->debit;
					$crtot+=$pr1->credit;
				?>
				
				<tr>
				<td width='20px'></td> 
				<td><a href="#myModal3" id='<?=$pr1->ledg_id;?>' class='view-dt' data-toggle='modal'><?=ucwords(strtolower($pr1->ledg_name));?></a></th> 
				<td style='width:120px;text-align:right;'><?=number_format($pr1->debit,2,".","");?></td>
                <td style='width:120px;text-align:right;'><?=number_format($pr1->credit,2,".","");?></td> 
                </tr>
				<?php
				}
				// direct expenses details

				
				foreach ($expenses as $pr2)
				{
				if($pr2->debit==0 or $pr2->credit==0)
				{					
						$drtot+=$pr2->debit;
						$crtot+=$pr2->credit;
				?>
					<tr>
					<td width='20px'></td> 
					<td><a href="#myModal3" id='<?=$pr2->ledg_id;?>' class='view-dt' data-toggle='modal'><?=ucwords(strtolower($pr2->ledg_name));?></a></th> 
					<td style='width:120px;text-align:right;'><?=number_format($pr2->debit,2,".","");?></td>
					<td style='width:120px;text-align:right;'><?=number_format($pr2->credit,2,".","");?></td> 
					</tr>
				<?php
				}
				}
				
				$gpl=$crtot-$drtot;
				if($gpl>0){	$gpl_text="GROSS PROFT";}else{$gpl_text="GROSS LOSS";}
				
				?>
				<!----1st- totals----------------------------------->
				<tr >
				<td width='20px'></td> 
				<td style='font-weight:700;font-size:13px;color:#000;'>TOTAL</td> 
				<td style='width:120px;text-align:right;border-top:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><b><?=number_format($drtot,2,".","");?></b></td>
                <td style='width:120px;text-align:right;border-top:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><b><?=number_format($crtot,2,".","");?></b></td> 
                </tr>
				
				<tr >
				<td width='20px'></td> 
				<td style='font-weight:700;font-size:13px;color:#000;'><?=$gpl_text;?></td> 
				<td style='width:120px;text-align:right;border-bottom:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'></td>
                <td style='width:120px;text-align:right;border-bottom:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><?=number_format($gpl,2,".","");?></b></td> 
                </tr>
				
				<tr ><td></td> 	<td></td> <td></td> <td></td> </tr>
				
				<!---- totals---ends----------------------------------->
				<?php
				$drtot1=0;
				// indirect expenses details
				foreach ($expenses1 as $pr3)
				{
					$drtot1+=$pr3->debit;
				?>
				<tr>
				<td width='20px'></td> 
				<td><a href="#myModal3" id='<?=$pr3->ledg_id;?>' class='view-dt' data-toggle='modal'><?=ucwords(strtolower($pr3->ledg_name));?></a></td> 
				<td style='width:120px;text-align:right;'><?=number_format($pr3->debit,2,".","");?></td>
                <td style='width:120px;text-align:right;'><?=number_format($pr3->credit,2,".","");?></td> 
                </tr>
				<?php
				}
				
				$gpl1=$crtot-($drtot+$drtot1);
				if($gpl1>0){	$gpl_text1="NET PROFT";}else{$gpl_text1="NET LOSS";}

				?>
				<!----net proft----------------------------------->
				
				<tr >
				<td width='20px'></td> 
				<td style='font-weight:700;font-size:13px;color:#000;'>TOTAL</td> 
				<td style='width:120px;text-align:right;border-top:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><b><?=number_format($drtot1,2,".","");?></b></td>
                <td style='width:120px;text-align:right;border-top:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><b><?=number_format($gpl,2,".","");?></b></td> 
                </tr>
				<tr >
				<td width='20px'></td> 
				<td style='font-weight:700;font-size:13px;color:#000;'><?=$gpl_text1;?></td> 
				<td style='width:120px;text-align:right;border-bottom:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'></td>
                <td style='width:120px;text-align:right;border-bottom:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><?=number_format($gpl1,2,".","");?></b></td> 
                </tr>
								
				
				</tbody> 
				</table>
			  </div>
        </div>

       </div>

   
</div>
</section>
</div>

<div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria- hidden="true">
	    <div class="modal-dialog modal-lg">
			<div class="modal-content">
			    <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Details</h4>
				</div>
				<div class="modal-body" style='max-height:460px;overflow:auto;'>
				
				
				</div>
				 <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
   </div>  

  
<?php include('application/views/common/footer.php'); ?>
   
<script type="text/javascript">

/*$("#example").dataTable({
	"ordering":false,
	});*/
	
 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
   
  $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
 
 $("#view-pdf").click(function(){
      
	 var pdf_link = $('#view-pdf').attr('href'); 
	     //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Report:',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;

  });
  
   $(".view-dt").click(function(){
	 var Result=$("#myModal3 .modal-body");
        var lid =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Account/get_selected_account_details",
        dataType: 'html',
        data: {ledgid: lid},
        success: function(res) {
        Result.html(res);
                    }
        });
 });
 
</script>  
