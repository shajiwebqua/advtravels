<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Agent Entries</b></h1>
	 <ol class="breadcrumb" style='font-size:15px'>
	 	<!-- <li><a href="<?php echo base_url('General/country');?>" style='color:#4b88ed;'><i class="fa fa-globe" aria-hidden="true"></i>Countries</a></li>
		<li><a href="<?php echo base_url('General/area');?>" style='color:#4b88ed;'><i class="fa fa-area-chart" aria-hidden="true"></i>Areas</a></li>
		<li><a href="<?php echo base_url('General/category');?>" style='color:#4b88ed;'><i class="fa fa-reorder" aria-hidden="true"></i>Categories</a></li>
		<li><a href="<?php echo base_url('General/role');?>" style='color:#4b88ed;'><i class="fa fa-gears" aria-hidden="true"></i>Role </a></li>
		<li><a href="<?php echo base_url('General/branch');?>" style='color:#4b88ed;'><i class="fa fa-snowflake-o" aria-hidden="true"></i>Branches</a></li>
		<li><a href="<?php echo base_url('General/profession');?>" style='color:#4b88ed;'><i class="fa fa-user-circle-o" aria-hidden="true"></i>Profession</a></li> -->
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->
              
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
   <div class="page-content">
   
   <div style="padding:0px 15px 0px 15px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:5px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?> <div id='msgerr'><?php echo $this->session->flashdata('message1'); ?></div></center></div>
	</div>
				 
		
                    <div class="col-md-4">
                    <div style="background-color:#fff;padding:10px; ">
					   
                        <!-- <div class="row"> -->
       <!-- <div class="col-md-12">
			   <h4>Agent Details</h4>
			   <hr>
          </div> -->
             <!-- <label style="background-color:#cecece;height:1px;width:100%"></label> -->
                   <!-- </div>/.box-header -->
          <form onsubmit="return checkdata();" class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/add_agent')?>">
                  
						<div class="form-group">
							 <div class="row" style="margin-top:0px;">
							 <div class="col-md-10 col-md-offset-1">
									<label class="control-label" style="padding-top:0px">Agent Code</label>
							 </div>
							 </div>
							 <div class='row' >
								 <div class="col-md-10 col-md-offset-1">
								   <input type="text" class="form-control"  name="agent_code" required>
								 </div>
							 </div>
                        </div>
 
 
						<div class="form-group" >
							 <div class="row" style="margin-top:0px;">
							 <div class="col-md-10 col-md-offset-1">
									<label class="control-label" style="padding-top:0px">Name</label>
							 </div>
							 </div>
							 <div class='row'>
								 <div class="col-md-10 col-md-offset-1">
								   <input type="text" class="form-control"  name="agent_name" required>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
							 <div class="col-md-10 col-md-offset-1">
									<label class="control-label" style="padding-top:0px">Address</label>
							 </div>
							 </div>
							 <div class='row'>
								 <div class="col-md-10 col-md-offset-1">
								   <textarea rows=2 cols=30 class="form-control"  name="agent_address" required></textarea>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
							 <div class="col-md-10 col-md-offset-1">
									<label class="control-label" style="padding-top:0px">Mobile</label>
							 </div>
							 </div>
							 <div class='row'>
								 <div class="col-md-10 col-md-offset-1">
								   <input type="text" pattern="[0-9]*" class="form-control"  name="agent_mobile" id="mobile" required>
                    <label id="mobmessage" style="color:red;"></label>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
							 <div class="col-md-10 col-md-offset-1">
									<label class="control-label" style="padding-top:0px">Email</label>
							 </div>
							 </div>
							 <div class='row'>
								 <div class="col-md-10 col-md-offset-1">
								   <input type="email" class="form-control"  name="agent_email" required>
								 </div>
							 </div>
                        </div>
 
                                <hr>
					                        
 
                    <div class="form-group">
                     <div class="row">
                      <div class="col-md-2 left1">
                      </div>
                               <div class="col-md-9" style="padding-left:25px;">
                                   <button type="submit" class="btn btn-primary">Submit</button>
                               </div>
                        </div>
                       </div>



                        </form>
                       </div>
                    </div>

                    
                <!--  <CENTER>
                    <h5 style="color:green;">
                       <?php echo $this->session->flashdata('message'); ?>
                    </h5>
                </CENTER> -->
        
        <!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
        
                    <!-- <div class="row"> -->
                   <div class="col-md-8">
                     <div style="background-color:#fff;padding:10px; ">
       
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                <div class="col-md-12">
                </div>
                </div>
                </div>
            <table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
                <tr>
                 <th>Edit</th>
                 <th>Del</th>
                 <th>Code</th>
				 <th>Name</th>
				 <th>Address</th>
				 <th>Mobile</th>
				 <th>Email</th>
                 <th>Status</th>
                </tr>
                </thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                      </div>
                        </div>
		</div> <!-- end Row --->
                <!-- END CONTENT BODY -->
            <!-- </div> -->
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
        
 
  <!-- End user details -->
  </div>
  
   <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 

</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

 function checkdata()
  {
   var mob=$("#mobile").val();
    if(mob.length<10 || mob.length>10)
    {
    $("#mobmessage").show();
      $("#mobmessage").html("Invalid mobile no,10 digits only.");
      return false;
    }
   
   else
    {
    $("#mobmessage").hide();
    $("#mobmessage").html("");
    return true;
    }
  }

//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal("Saved",$("#msg").html(),"success")
		$("#msg").html("");
	}
	
	
$("#msgerr").hide();
if($("#msgerr").html()!="")
	{
		swal("Try Again",$("#msgerr").html(),"error")
		$("#msgerr").html("");
	}	
	
//-------------------------------------
 
      $('#example').dataTable( {
		  "ordering":false,
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/agent_ajax",// json datasource
               },
        "columnDefs": [
		{"width":"7%","targets":0},
		{"width":"7%","targets":1},
		{"width":"9%","targets":7},
		],
        "columns": [
            { "data": "edit" },
            { "data": "delete" },
            { "data": "code"},
			{ "data": "name"},
			{ "data": "address"},
			{ "data": "mobile"},
			{ "data": "email"},
            { "data": "status" },
      
       ]
  } );

  
      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/edit_agent",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 
    
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#msg').html(''); }, 3000);
		

 //});
  


</script>



