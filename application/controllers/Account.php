<?php  

if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		if (!isset($user))
		{ 
			//redirect('/');
		} 
		else
		{ 
			return true;
		}
		$this->load->helper(array('form', 'url','html'));
	}
		
  public function profit_loss()
	{
		$op=$this->uri->segment(3);
		
		if($op==1)
		{
			$sdt=$this->input->post('startdate');
			$edt=$this->input->post('enddate');
		}	
		else if($op==null or $op=="0")
		{
			$sdt="01-04-".date('Y');
			$edt="31-03-".(date('Y')+1);
		}
		
		$d1=explode("-",$sdt);
		$sdt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$edt);
		$edt1=$d2[2]."-".$d2[1]."-".$d2[0];
		
		$period="Date :&nbsp;&nbsp;".$sdt." &nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp; ".$edt;
		
		$adt=array('plsdate'=>$sdt1,'pledate'=>$edt1,'acperiod1'=>$period); //for pdf
		$this->session->set_userdata($adt);
						
		$this->load->model('Model_accounts');
		$ploss['incomes']=$this->Model_accounts->profit_loss_incomes($sdt1,$edt1); //direct&indirect incomes
		$ploss['expenses']=$this->Model_accounts->profit_loss_expenses($sdt1,$edt1); //direct expense
		$ploss['expenses1']=$this->Model_accounts->profit_loss_expenses1($sdt1,$edt1); //indirect expense
		
		$this->load->view('account/view_profitloss',$ploss);
	}
	
   public function balance_sheet()
	{
		$op=$this->uri->segment(3);
		if($op==1)
		{
			$sdt=$this->input->post('startdate');
			$edt=$this->input->post('enddate');
		}	
		else if($op==null)
		{
			$sdt="01-04-".date('Y');
			$edt="31-03-".(date('Y')+1);
		}
		
		$d1=explode("-",$sdt);
		$sdt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$edt);
		$edt1=$d2[2]."-".$d2[1]."-".$d2[0];
		
		$period="Date :&nbsp;&nbsp;".$sdt." &nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp; ".$edt;
		
		$bsdt=array('bssdate'=>$sdt1,'bsedate'=>$edt1,'acperiod2'=>$period); //for pdf
		$this->session->set_userdata($bsdt);
		
		$this->load->model('Model_accounts');
		/*$bsheet['plamount']=$this->Model_accounts->bsheet_profitloss_amount($sdt1,$edt1);*/
		$bsheet['assets']=$this->Model_accounts->bsheet_asset_values($sdt1,$edt1);
		$bsheet['liability']=$this->Model_accounts->bsheet_liability_values($sdt1,$edt1);
		
		$this->load->view('account/view_balancesheet',$bsheet);
	}
	

   public function trial_balance()
	{
		$op=$this->uri->segment(3);
		if($op==1)
		{
			$sdt=$this->input->post('startdate');
			$edt=$this->input->post('enddate');
		}	
		else if($op==null)
		{
			$sdt="01-04-".date('Y');
			$edt="31-03-".(date('Y')+1);
		}
		
		$d1=explode("-",$sdt);
		$sdt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$edt);
		$edt1=$d2[2]."-".$d2[1]."-".$d2[0];
		
		$period="Date :&nbsp;&nbsp;".$sdt." &nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp; ".$edt;
		
		$bsdt=array('tbsdate'=>$sdt1,'tbedate'=>$edt1,'acperiod3'=>$period); //for pdf
		$this->session->set_userdata($bsdt);
			
		$this->load->model('Model_accounts');
		$tbalance['ledgervalues']=$this->Model_accounts->tbalance_ledger_values($sdt1,$edt1);
				
		$this->load->view('account/view_trialbalance',$tbalance);
	}
	

 public function ledger()
  {
	$this->load->model("Model_accounts");
	$data['ledgers']=$this->Model_accounts->ledger_list();
	$this->load->view("account/acc_ledgers",$data); 
  }
 
 
 public function ledger_wise_list()
 {
	$op=$this->uri->segment(3);
	$ldata['ledwiselist']=array();
	if($op!="" || $op!=null)
	{
	$lid=$this->input->post('ledger');
	$sd=$this->input->post('startdate');
	$ed=$this->input->post('enddate');
		$d1=explode("-",$sd);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$ed);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];

	$this->load->model("Model_accounts");
	$ldata['ledwiselist']=$this->Model_accounts->ledger_wise_list($dt1,$dt2,$lid);
	}
	$this->load->view("account/view_ledger_wiselist",$ldata); 
 }
 
 
public function delete_account()  // ledger wise account deleteon
{
	$eid=$this->uri->segment(3);
	$rw=$this->db->select('entry_accuid')->from('ac_account_entry')->where('entry_id',$eid)->get()->row();
	$aid=$rw->entry_accuid;
	$this->load->model("Model_accounts");
	$this->Model_accounts->del("ac_accounts",array("accu_id"=>$aid));
	$this->Model_accounts->del("ac_account_entry",array("entry_accuid"=>$aid));
	$this->Model_accounts->del("ac_cashbook",array("cashbook_headid"=>$aid));
	$this->session->set_flashdata('message', '2#Account sccessfully Removed.'); 
	redirect('Account/ledger_wise_list');
}

public function get_selected_account_details()
{
	$ledgid=$this->input->post('ledgid');
	$this->load->model("Model_accounts");
	$results=$this->Model_accounts->view_account_details($ledgid) ;
	$dt="<style> .mtable tr,th,td{border:1px solid #000;padding:0px 3px 0px 3px;}</style><table class='mtable' width='100%'><tr><th>Id</th><th>Date</th><th>Ledger</th><th>Debit</th><th>Credit</th><th>Narration</th></tr>";
	$dtot=0;
	$ctot=0;
	$diff=0;
	foreach($results  as $r)
	{
		$dt.="<tr><td>".$r->accu_id."</td><td>".date_format(date_create($r->accu_date),'d-m-Y')."</td>";
		$dt.="<td>".$r->ledg_name."</td><td align='right'>".number_format($r->entry_debit,"2",".","")."</td>";
		$dt.="<td align='right'>".number_format($r->entry_credit,"2",".","")."</td><td>".$r->accu_narration."</td></tr>";
		
		$dtot+=$r->entry_debit;
		$ctot+=$r->entry_credit;
	}
	
	if($dtot<=0)
	{
		$dt.="<tr><td></td><td></td><td><b>Totals</b></td><td align='right'><b>0.00</b></td><td align='right'><b>".number_format($ctot,"2",".","")."</b></td><td></td></tr>";
	}
	else if($ctot<=0)
	{
		$dt.="<tr><td></td><td></td><td><b>Totals</b></td><td align='right'><b>".number_format($dtot,"2",".","")."</b></td><td align='right'><b>0.00</b></td><td></td></tr>";
	}
	else
	{
		$diff=$dtot-$ctot;
	}
	if($diff!=0)
	{
	  $dt.="<tr><td></td><td></td><td><b>Totals</b></td><td align='right'><b>".number_format($dtot,"2",".","")."</b></td><td align='right'><b>".number_format($ctot,"2",".","")."</b></td><td></td></tr>";	
	  $dt.="<tr><td></td><td></td><td colspan=2>Account Balance</td><td align='right'><b>".number_format($diff,"2",".","")."</b></td><td></td></tr>";
	}
	$dt."</table>";
	echo $dt;
}


/*
// to load account group page
	public function groups()
	{
		//$op=$this->uri->segment(3);
		$this->load->model("Model_accounts");
		$data['result']=$this->Model_accounts->group_list();
        $this->load->view("account/acc_groups",$data); 
	}
		
//to load voucher type adding page
	public function voucher()
	{
		$this->load->model("Model_accounts");
		$data['result']=$this->Model_accounts->voucher_type_list();
        $this->load->view("account/acc_vouchertypes",$data); 
	}
	
//to get nature of groups to adding/editing page	
	public function get_group_nature()
	{
		$gid=$this->input->post('gid');
		$row=$this->db->select('acc_grpnature')->from('acc_groups')->where('acc_grpid',$gid)->get()->row();
		echo $row->acc_grpnature;
	}

//to get ledger details for editing
	public function get_ledger_details()
	{
		$str="";
		$lid=$this->input->post('ledgid');
		$row=$this->db->select('*')->from('acc_ledgers')->where('acc_ledgid',$lid)->get()->row();
		
		$str.=$row->acc_ledgid.",";
		$str.=$row->acc_ledgname.",";
		$str.=$row->acc_ledunder.",";
		$str.=$row->acc_ledsubgroup.",";
		echo $str;
	}
	
// to save account groups to table	
		
	public function save_group()
	{
	$this->load->model("Model_accounts");
	
	
	$grpcode=$this->input->post('grpcode');
	$grpname=$this->input->post('grpname');
	$grpunder=$this->input->post('grpunder');
	$gp=explode("#",$grpunder);
	
//	$row=$this->db->select('acc_grpunder')->from('acc_groups')->where('acc_grpid',$grpunder)->get()->row();
	//$grpunder1=$row->acc_grpunder;
	
	$data1=array(
			'acc_grp_code'=>$grpcode,
			'acc_grp_description'=>strtoupper($grpname),
			'acc_grp_parentid'=>$gp[0],
			'acc_grp_type'=>$gp[1],
			);
		
	$row=$this->db->select('*')->from('acc_groups')->where('acc_grp_description',strtoupper($grpname))->get();
	if($row->num_rows()>0)
	{
	$this->session->set_flashdata('message', '4#Account Group exist.'); 
	redirect('Account/groups');
	}
	else
	{
	$this->Model_accounts->insert("acc_groups",$data1);
	$this->session->set_flashdata('message', '1#Account Group sccessfully added.'); 
	redirect('Account/groups');
	}
}


public function edit_group()
	{
		$grid=$this->input->post('gid');
		
		$grow=$this->db->select("*")->from('acc_groups')->where('acc_grp_id',$grid)->get()->row();

	echo '<div class="modal-header" style="background-color:#cacaca ;">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true" >&times</span>
        </button>
		<h4 class="modal-title" id="exampleModalLabel">Edit Account Group</h4>
      </div>
	  
	  <form class="form-horizontal" role="form" method="POST" action="'. base_url("Account/update_group").'" enctype="multipart/form-data">
	  		
	  <input type="hidden" class="form-control"  name="mgrpid" value="'.$grid.'" required>
				<div class="form-group" style="margin-top:10px;">
				   <div class="row">
				   	  <label class="col-md-4 control-label">Group Name :</label>
						<div class="col-md-7">
							<input type="text" class="form-control"  name="mgrpname" value="'. $grow->acc_grp_description .'"  required>
						</div>
					</div>
					
					<div class="row" style="margin-top:10px;">
					  <label class="col-md-4 control-label">Under :</label>
					<div class="col-md-7">
					  <select  class="form-control" name="mgrpunder" required>
						<option value="">---type----</option>';
	
							$gquery=$this->db->select("*")->from('acc_group_master')->get()->result();
							foreach( $gquery as $g)
							{
								echo "<option value='".$g->acc_mgrp_id."#".$g->acc_mgrp_id."'"; if($g->acc_mgrp_id==$grow->acc_grp_parentid) echo "selected"; echo ">".$g->acc_mgrp_type."</option>";
								
							}
							
						echo '</select>
					  </div>
					</div>
					
				</div>
	
		  <div class="modal-footer" style="margin-right:30px;">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Save changes</button>
		  </div>
	  </form>
	';
}

//to update account group to table
	public function update_group()
	{
	$this->load->model("Model_accounts");
	
	$grpid=$this->input->post('mgrpid');
	$grpname=$this->input->post('mgrpname');
	$grpunder=$this->input->post('mgrpunder');
	$gp=explode("#",$grpunder);
	$data1=array(
			'acc_grp_description'=>strtoupper($grpname),
			'acc_grp_parentid'=>$gp[0],
			'acc_grp_type'=>$gp[1],
			);
 
	$where="acc_grp_id=".$grpid;
	$this->Model_accounts->update("acc_groups",$data1,$where);
	$this->session->set_flashdata('message', '2#Group sccessfully updated.'); 
	redirect('Account/groups');
	}
//delete groups ----------------------
	
	public function delete_group()
	{
		$grpid=$this->uri->segment(3);
		$this->load->model("Model_accounts");
	
	    $rec=$this->db->select('COUNT(*) as cnt')->from('acc_ledgers')->where('acc_led_groupid',$grpid)->get()->row();
		if($rec->cnt<=0)
		{
			$where="acc_groups.acc_grp_id=".$grpid;
			$this->db->where($where)->delete("acc_groups");
			$this->session->set_flashdata('message', '3#Group sccessfully removed.'); 
			redirect('Account/groups');
		}
		else
		{
			$this->session->set_flashdata('message', '4#Could not remove this Group. It is link with another data.'); 
			redirect('Account/groups');
		}
	}
*/
// to load account ledger adding page

	
/*	
	public function get_ledger_no()
	{
		$lno=1001;
		$res=$this->db->select("MAX(acc_led_code) as nos")->from("acc_ledgers")->get()->result();
		foreach($res as $rs)
		{
			if($rs->nos<=0)
			{
				return $lno;
			}
			else
			{
				return $lno=$rs->nos+1;
			}
		}
	}
	
//to save account ledger to table
	public function save_ledger()
	{
	$this->load->model("Model_accounts");
		
	$lcode=$this->input->post('lcode');
	$lname=$this->input->post('lname');
	$lunder=$this->input->post('lunder');
		
	$data1=array(
			'acc_led_code'=>$lcode,
			'acc_led_description'=>strtoupper($lname),
			'acc_led_groupid'=>$lunder,
			);
			
	$row=$this->db->select('*')->from('acc_ledgers')->where('acc_led_code',$lcode)->or_where('acc_led_description',strtoupper($lname))->get();
	if($row->num_rows()>0)
	{
	$this->session->set_flashdata('message', '4#Ledger Code/Name already exist.'); 
	redirect('Account/ledger');
	}
	else
	{
	$this->Model_accounts->insert("acc_ledgers",$data1);
	$this->session->set_flashdata('message', '1#Ledger sccessfully added.'); 
	redirect('Account/ledger');
	}
}
*/

	
//to edit ledger details and update

/*
    public function delete_ledger()
	{
		$lid=$this->uri->segment(3);
		$rec=$this->db->select('COUNT(*) as cnt')->from('acc_transactions')->where('acc_tra_ledgerid',$lid)->get()->row();
		if($rec->cnt<=0)
		{
		$where="acc_ledgers.acc_led_id=".$lid;
		$this->db->where($where)->delete("acc_ledgers");
		$this->session->set_flashdata('message', '3#Ledger sccessfully removed.'); 
		redirect('Account/ledger');
		}
		else
		{
		$this->session->set_flashdata('message', '4#Could not remove this ledger. It is link with another data.'); 
		redirect('Account/ledger');
		}
	}

// to save voucher type to table
	public function save_vtype()
	{
	$this->load->model("Model_accounts");
		
	$vname=$this->input->post('vname');
	$vtype=$this->input->post('vtype');
		
	$data1=array(
			'acc_vtypename'=>strtoupper($vname),
			'acc_vtype_no'=>$vtype,
			);
			
	$row=$this->db->select('*')->from('acc_voucher_types')->where('acc_vtypename',strtoupper($vname))->get();
	if($row->num_rows()>0)
	{
	$this->session->set_flashdata('message', '4#Voucher type already exist.'); 
	redirect('Account/voucher');
	}
	else
	{
	$this->Model_accounts->insert("acc_voucher_types",$data1);
	$this->session->set_flashdata('message', '1#Voucher type sccessfully added.'); 
	redirect('Account/voucher');
	}		
	
}
// to update voucher type

	public function edit_vtype()
	{
	$mvtid=$this->input->post('mvtid');
	$mvtype=$this->input->post('mvtype');

	$data1=array
	(
		'acc_vtypename'=>strtoupper($mvtype),
	);
	
	$this->load->model("Model_accounts");
	
	$where="acc_vtypeid=".$mvtid;
	$this->Model_accounts->update("acc_voucher_types",$data1,$where);
	$this->session->set_flashdata('message', '2#Voucher type updated.'); 
	redirect('Account/voucher');
	}

	public function delete_voucher_type()
	{
	$lid=$this->uri->segment(3);
	$where="acc_voucher_types.acc_vtypeid=".$lid;
	$this->db->where($where)->delete("acc_voucher_types");
	$this->session->set_flashdata('message', '3#Voucher type removed.'); 
	redirect('Account/voucher');
	}
	
    public function BSheet()
	{
	   $op=$this->uri->segment(3);
	   $this->load->model("Model_accounts");
	   
	if($op=="" || $op==null)
	{	
		   $bsheetDt['bsdata1']=$this->Model_accounts->balance_sheet_data1("","",$op) ;
		   $bsheetDt['bsdata2']=$this->Model_accounts->balance_sheet_data2("","",$op) ;
		   $bsheetDt['plexp']=$this->Model_accounts->diff_expense("","",$op);
		   $bsheetDt['plinc']=$this->Model_accounts->diff_income("","",$op);
	}
	else if($op==2)
	{
		$sd=$this->input->post('startdate');
		$ed=$this->input->post('enddate');

		$d1=explode("-",$sd);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$ed);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
		
		$bsheetDt['bsdata1']=$this->Model_accounts->balance_sheet_data1($dt1,$dt2,$op);
		$bsheetDt['bsdata2']=$this->Model_accounts->balance_sheet_data2($dt1,$dt2,$op);
		$bsheetDt['plexp']=$this->Model_accounts->diff_expense($dt1,$dt2,$op);
		$bsheetDt['plinc']=$this->Model_accounts->diff_income($dt1,$dt2,$op);
	}
		$this->load->view("account/acc_balancesheet",$bsheetDt); 
    }

	
	/*public function profit_loss()
	{
		$op=$this->uri->segment(3);
		$this->load->model("Model_accounts");
		if($op=="" || $op==null)
		{
			   $prolossDt['plexpense']=$this->Model_accounts->profit_direct_expense("","",$op) ;
			   $prolossDt['plincome']=$this->Model_accounts->profit_direct_income("","",$op) ;
		}
		else if ($op==1)
		{
			$sd=$this->input->post('startdate');
			$ed=$this->input->post('enddate');

			$d1=explode("-",$sd);
			$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
			
			$d2=explode("-",$ed);
			$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			
			$prolossDt['plexpense']=$this->Model_accounts->profit_direct_expense($dt1,$dt2,$op) ;
			$prolossDt['plincome']=$this->Model_accounts->profit_direct_income($dt1,$dt2,$op) ;
		}
			
		$this->load->view("account/acc_profit_loss",$prolossDt); 
	}*/
	
	/*
		
	public function View_trans()
	{
		$dt1="";
		$dt2="";
		$vt="";
		$op=$this->uri->segment(3);
			if($op==1)
			{
				$sdate=$this->input->post('startdate');
				$edate=$this->input->post('enddate');
			
				$d1=explode("-",$sdate);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$edate);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			}
			else if($op==2)
			{
				$vt=$this->input->post('vtype');
			}
		
		$this->load->model("Model_accounts");
		$tdata['alltrans']=$this->Model_accounts->all_trans_list($dt1,$dt2,$op,$vt);
        $this->load->view("account/acc_alltransactions",$tdata); 
	}
		
	public function ledgerwise_list()
	{
		$ldg="";
		$op=$this->uri->segment(3);
			if($op==1)
			{
				$ldg=$this->input->post('sledger');
			}
		$this->load->model("Model_accounts");
		$ldata['alltrans']=$this->Model_accounts->all_trans_list2($op,$ldg);
        $this->load->view("account/acc_ledgerwise_list",$ldata); 
	}
	
	public function View_cashbook()
	{
		$dt1="";
		$dt2="";
		$vt="";
		$op=$this->uri->segment(3);
			if($op==1)
			{
				$sdate=$this->input->post('startdate');
				$edate=$this->input->post('enddate');
			
				$d1=explode("-",$sdate);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$edate);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			}
					
		$this->load->model("Model_accounts");
		$cbdata['cblist']=$this->Model_accounts->cashbook_list($dt1,$dt2,$op);
        $this->load->view("account/acc_cashbook_list",$cbdata); 
	}
	
		
	public function delete_transactions1()  // from all transactions
	{
		$tid=$this->uri->segment(3);
		$this->load->model("Model_accounts");	
		$where="acc_trans_id=".$tid;
		$this->Model_accounts->del("acc_trans_head",$where);
		
		$where="acc_tra_headid=".$tid;
		$this->Model_accounts->del("acc_transactions",$where);
		
		$where="acc_cashbook_id=".$tid;
		$this->Model_accounts->del("acc_cashbook",$where);
		
		$this->session->set_flashdata('message', '3#Voucher sccessfully removed.'); 
		redirect('Account/View_trans');
	}
	
	public function delete_transactions2() // from ledger wise transactions
	{
		$tid=$this->uri->segment(3);
		$this->load->model("Model_accounts");	
		$where="acc_trans_id=".$tid;
		$this->Model_accounts->del("acc_trans_head",$where);
		
		$where="acc_tra_headid=".$tid;
		$this->Model_accounts->del("acc_transactions",$where);
		
		$where="acc_cashbook_id=".$tid;
		$this->Model_accounts->del("acc_cashbook",$where);
		
		$this->session->set_flashdata('message', '3#Voucher sccessfully removed.'); 
		redirect('Account/Ledgerwise_list');
	} */

}
?>
