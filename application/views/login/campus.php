<html>
<head>
<title>Login</title>
<link rel='stylesheet' href="<?php echo base_url('assets/login_style.css');?>" type='text/css'>
</head>
<body>


<div class='content_form'>
<img src="<?php echo base_url('assets/images/logo-campus-overseas.gif');?>" style='margin-left:25%;margin-bottom:25px;'>
<?php $attributes = array('class' => 'log_form', 'id' => 'myform');
 echo form_open('login',$attributes ); ?>

<div class='header'>Login</div>

<fieldset>
<section>
<input type="text" name="username" value="<?php echo set_value('username'); ?>" placeholder='Username' size="50" />
<?php echo form_error('username','<div class="error">', '</div>'); ?>
</section>
<section>
<input type="password" name="password" size="50" placeholder='Password' />
<?php echo form_error('password','<div class="error">', '</div>'); ?>
<?php if(isset($msg)) {echo $msg;} ?>
</section>


<div><input type="submit" value="Submit" class='button' /></div>
</fieldset>
</form>
</div>
</body>
</html>
