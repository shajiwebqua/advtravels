 <?php
 // $ivid=$this->input->post('ivid');
 // $tamt=$this->db->select('invo_total')->from('invoices')->where('invo_invoiceid',$ivid)->get()->row()->invo_total;
  
 // $ledg=$this->db->select('ledg_id,ledg_name')->from('ac_ledgers')->get()->result();
?>           
    <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Record payments4566</font></h4>
    </div>
    <br>
  
	<form  class='form-horizontal' role='form' method='post' action='<?php echo site_url('Invoice/save_payments');?>' enctype='multipart/form-data'>
	<input type='hidden' name='invoid'  value='<?=$ivid;?>'/>
  <div class='row'>
    <div class='col-md-12'>
   
          <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:4px;'>Date </label>
          <div class='col-md-4'>
          <input type='text' class='form-control' id='dtpicker1' name='edate' value='<?=date('d-m-Y');?>'> 
          </div>
          </div>
		  
		  <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:4px;'>Amount Received</label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='amount' value='<?=number_format($tamt,'2','.','');?>' required> 
          </div>
          </div>
		  
		  <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:4px;'>Received From</label>
          <div class='col-md-8'>
		  <select class='form-control select2' name='ledgerid' style='width:100%;' required>
		  <option value=''>---select ledger---</option>";
		  <?php
		  
		  foreach($ledg as $lg)
		  {
			echo "<option value='".$lg->ledg_id."'>".$lg->ledg_name."</option>";
		  }
		  ?>
		  
		  </select>
		  </div>
          </div>
		  
		  <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:4px;'>Payment Mode</label>
          <div class='col-md-4'>
		  <select class='form-control' name='paymode' style='width:100%;' required>
		  <option value='' >---select---</option>
		  <option value='1' >CASH</option>
		  <option value='2' >BANK</option>
		  </select>
		  </div>
          </div>
		  
		  		  
    </div>
    </div> 
    <div class='modal-footer'>
      <input type='submit' class='btn btn-primary'  style='padding:5px 15px 5px 15px;'  value='Submit Payment'/>
        <button type='button' class='btn btn-default' style='padding:5px 15px 5px 15px;' data-dismiss='modal'>Close</button>
     </div>     
  
</form>   
 <script src="<?php echo base_url('assets/dist/js/select2.min.js');?>" type="text/javascript"></script>
<script type='text/javascript'>
$('.select2').select2();

$('#dtpicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   </script>
