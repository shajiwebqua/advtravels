<?php

defined("BASEPATH") OR exit("'No direct script access allowed'");


class Model_loan extends CI_Model{


	public function __construct(){
		parent::__construct();
	}

	public function insert($table,$data){
		return $this->db->insert($table,$data);
	}

	public function select_all($table)
	{
		$this->db->select("*");
		$this->db->from($table);
		$query = $this->db->get();
		return $query->result();
	}

	public function select($table,$where, $column = "*")
	{
		$this->db->select($column);
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	public function update($table, $values, $where){
		$this->db->where($where);
		$this->db->update($table, $values);
	}

	public function delete($table, $where){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function get_ltlreference($value){
		$this->db->where("loan_ltlrefno", $value);
		$query = $this->db->get("loan_vehicle_master");
		if($query->num_rows() > 0){ return TRUE; }
		else{ return FALSE; }
	}

	public function checkregno($value){
		$this->db->where("loan_vehicleno", $value);
		$query = $this->db->get("loan_vehicle_master");
		if($query->num_rows() > 0){ return TRUE; }
		else{ return FALSE; }
	}

	public function downpayment($value){
	  $this->db->select("loan_downpayment");
	  $this->db->where("loan_vehicleno", $value);
	  $query = $this->db->get("loan_vehicle_master");
	  
	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->loan_downpayment;
	  }
	  else{ return FALSE; }
	}

	public function loanamount($value){
	  $this->db->select("loan_amount");
	  $this->db->where("loan_vehicleno", $value);
	  $query = $this->db->get("loan_vehicle_master");
	  
	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->loan_amount;
	  }
	  else{ return FALSE; }
	}

	public function totalinterest($value){
	  $this->db->select("loan_interest");
	  $this->db->where("loan_vehicleno", $value);
	  $query = $this->db->get("loan_vehicle_master");

	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->loan_interest;
	  }
	  else{ return FALSE; }
	}

	public function paidamount($value){
	  $this->db->select("SUM(loan_instalment) as instalment");
	  $this->db->where("status = '1' AND ltl_ref_no IN (SELECT loan_ltlrefno FROM loan_vehicle_master where loan_vehicleno ='". $value ."' )");
	  $query = $this->db->get("loan_shedule");

	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->instalment;
	  }
	  else{ return FALSE; }
	}

	public function vehicleamount($value){
	  $this->db->select("loan_vehicleamount");
	  $this->db->where("loan_vehicleno", $value);
	  $query = $this->db->get("loan_vehicle_master");

	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->loan_vehicleamount;
	  }
	  else{ return FALSE; }
	}

	public function vehiclecost($value){
	  $this->db->select("(loan_vehicleamount + loan_interest) as vehicle_amount");
	  $this->db->where("loan_vehicleno", $value);
	  $query = $this->db->get("loan_vehicle_master");

	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->vehicle_amount;
	  }
	  else{ return FALSE; }
	}
	  public function delete_loan($id)
{
    $this->db->where('loan_ltlrefno', $id);
    $this->db->delete('loan_vehicle_master');
    return $this->db->affected_rows();
}

 public function delete_loan1($id)
{
    $this->db->where('ltl_ref_no', $id);
    $this->db->delete('loan_schedule');
    return $this->db->affected_rows();
}
public function view_loan(){

		$this->db->select('*');
		$this->db->from('loan_vehicle_master');
		$query = $this->db->get();
		return $query->result();
	}
}