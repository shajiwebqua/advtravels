<?php
class Model_users extends CI_Model {
	public function add_customer($fname,$lname,$daddress,$contact,$email,$shopname,$password)
	{
		$data = array(
	   		'fname' => $fname ,
			'lname' => $lname ,
			'daddress' => $daddress,
            'contact' => $contact ,
            'email' => $email ,
            'shopname' => $shopname,
			'password' => $password
            		);
		$this->db->insert('customers', $data); 
         
      $this->db->select('*')->from('customers');
      $this->db->where('password',$password);
      $query = $this->db->get();
      $ret = $query->row();
        $pass= $ret->id;
      

        $data1 = array(
            'username' => $fname,
            'password' => md5($password),
            'level' => 2,
            'user_id' => $pass
            );
        $this->db->insert('login', $data1); 
	}
    public function add_staff($fname,$lname,$contact,$email,$empidno,$password)
    {
        $data = array(
            'fname' => $fname ,
            'lname' => $lname ,
            'contact' => $contact ,
            'email' => $email ,
            'empidno' => $empidno,
            'password' => $password
                    );
        $this->db->insert('staffs', $data); 

        $this->db->select('*')->from('staffs');
      $this->db->where('password',$password);
      $query = $this->db->get();
      $ret = $query->row();
        $pass= $ret->id;
      

        $data1 = array(
            'username' => $fname,
            'password' => md5($password),
            'level' => 1,
            'user_id' => $pass
            );
        $this->db->insert('login', $data1); 
    }

	 public function list_customers()
    {
        $user_data=$this->session->all_userdata();
    $this->db->select('*')->from('customers');
    $query = $this->db->get();
        return $query->result();
    }  
  
   public function list_staffs()
    {
        $user_data=$this->session->all_userdata();
    $this->db->select('*')->from('staffs');
    $query = $this->db->get();
        return $query->result();
    }  
    public function del_customers($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('customers'); 
                 return $this->db->affected_rows();
     	}
 public function edit_customers($id)
    {
       
    $this->db->select('*')->from('customers');
    $this->db->where('id',$id);
    $query = $this->db->get();
        return $query->result();
    }
    public function edit_customers1($id,$data)
    {

       
        $this->db->where('id',$id);
        $this->db->update('customers',$data); 
        }  

         public function del_staffs($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('staffs'); 
                 return $this->db->affected_rows();
        }
 public function edit_staffs($id)
    {
       
    $this->db->select('*')->from('staffs');
    $this->db->where('id',$id);
    $query = $this->db->get();
        return $query->result();
    }
    public function edit_staffs1($id,$data)
    {

       
        $this->db->where('id',$id);
        $this->db->update('staffs',$data); 
        } 

        public function customer_resetpassword($password){
           $user_data=$this->session->all_userdata();
        $data = array(
            
            'password' => md5($password)
     
                    );
        $this->db->where('id', $user_data['userid']);
        $this->db->where('level', 2);
        $this->db->update('login', $data); 
    }    

    public function staff_resetpassword($password){
           $user_data=$this->session->all_userdata();
        $data = array(
            
            'password' => md5($password)
     
                    );
        $this->db->where('id', $user_data['userid']);
        $this->db->where('level', 1);
        $this->db->update('login', $data); 
    }    

     public function get_customerprofile()
     {
        $user_data=$this->session->all_userdata();
         $this->db->select('*');
         $this->db->from('customers');
         $this->db->join("login","customers.id=login.user_id","inner");
        $this->db->where('login.level',2);
        $this->db->where('login.id',$user_data['userid']);
 
        $query = $this->db->get();
        return $query->result();
     }

     public function get_staffprofile()
     {
        $user_data=$this->session->all_userdata();
         $this->db->select('*');
         $this->db->from('staffs');
         $this->db->join("login","staffs.id=login.user_id","inner");
        $this->db->where('login.level',1);
        $this->db->where('login.id',$user_data['userid']);
 
        $query = $this->db->get();
        return $query->result();
     }

      public function edit_customerprofile($id)
    {
       
    $this->db->select('*')->from('customers');
    $this->db->where('fname',$id);
    $query = $this->db->get();
        return $query->result();
    }
    public function edit_customerprofile1($id,$data)
    {

       
        $this->db->where('id',$id);
        $this->db->update('customers',$data); 
        }  

    public function edit_staffprofile($id)
    {
       
    $this->db->select('*')->from('staffs');
    $this->db->where('fname',$id);
    $query = $this->db->get();
        return $query->result();
    }
    public function edit_staffprofile1($id,$data)
    {

        $this->db->where('id',$id);
        $this->db->update('staffs',$data); 
        }  

public function delete_customers($id)
{
    $this->db->where('id', $id);
    $this->db->delete('customers');
    return $this->db->affected_rows();
}


public function delete_staffs($id)
{
    $this->db->where('id', $id);
    $this->db->delete('staffs');
    return $this->db->affected_rows();
}

}
?>