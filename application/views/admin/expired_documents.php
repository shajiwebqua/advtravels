<?php
 include('application/views/common/header.php');?>
<style>
.nav-tabs {
    margin: 0;
    padding: 0;
    border: 0;    
}
.nav-tabs > li > a {
    background: #f2f2f2;
    border-radius: 0;
    //box-shadow: inset 0 -8px 7px -9px rgba(0,0,0,.4),-2px -2px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li.active > a:hover {
    background: #fff;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li > a:hover {
    background: #e4e4e4;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}
/* Tab Content */
.tab-pane {
    background: #fff;
    //box-shadow: 0 0 4px rgba(0,0,0,.4);
    border-radius: 0;
    border:1px solid #e4e4e4;
    text-align: left;
    padding: 10px;
}

.cli
{
padding:5px 0px 5px 0px;
margin-top:3px;

}

.selclass > div:hover
{
background-color:#f4f4f4;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
 <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <b>Renewal Vehicle Documents</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px;'>
		<li><a id='view-pdf' href="<?php echo base_url('Reports/Documents');?>" style='color:#4b88ed;'><button style='padding:2px 20px 2px 20px;'><i class="fa fa-print" aria-hidden="true"></i> Print</button></a></li>
		<!--
		<li><a href="<?php echo base_url('General/category');?>" style='color:#4b88ed;'><i class="fa fa-reorder" aria-hidden="true"></i>Categories</a></li>
		<li><a href="<?php echo base_url('General/agents');?>" style='color:#4b88ed;'><i class="fa fa-user-circle-o" aria-hidden="true"></i>Agents</a></li>
		<li><a href="<?php echo base_url('General/role');?>" style='color:#4b88ed;'><i class="fa fa-gears" aria-hidden="true"></i>Role </a></li>
		<li><a href="<?php echo base_url('General/branch');?>" style='color:#4b88ed;'><i class="fa fa-snowflake-o" aria-hidden="true"></i>Branches</a></li>
		<li><a href="<?php echo base_url('General/profession');?>" style='color:#4b88ed;'><i class="fa fa-user-circle-o" aria-hidden="true"></i>Profession</a></li>
		 -->
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">
  <!-- Add new user details -->
              
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
   <div class="page-content">
   <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
   
<?php
		$dtab=$this->session->userdata('doctab');
?>
	
		<div style="background-color:#fff;padding:3px 10px 3px 10px;">
			<div class='row'>
			  <div class='col-md-6'><h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Documents Details</h4> </div>
			  <div class='col-md-6' >
		         
				<!-- <label class='col-md-3' style='padding-top:5px;'>Select Vehicle : </label>
				 <div class='col-md-7' >
				   <select name='sregno' class='form-control' id='sregno' required>
				   <option value="0">----------</option>
				   <?php
				   //foreach($result as $r1)
				   //{
					  // echo "<option value='". $r1->vehicle_id."'>".$r1->vehicle_regno."</option>";
				   //}
				   ?>
				   </select>
				 </div>
			 
				  <!--<button type="submit" class="btn_new btn btn-success"  style='padding-left:5px;'><span class='	glyphicon glyphicon-th-list'></span> Details View</button> -->
				  
		      </div>
			
            </div>
			
			 <div class='row'>
			<hr style='margin:0px 0px 15px 0px;'>
			</div>
			
				<div class='row' style='padding:5px 15px 5px 15px;' >
				<table class="table table-striped table-hover table-bordered" id="example" width='100%'>
                <thead>
                <tr>
				<th>Action</th>
				<th>SlNo</th>
                 <th>Vehicle</th>
				 <th>Type</th>
                 <th>Document</th>
				 <th>Doc-No</th>
                 <!--<th>Validity from</th> -->
				 <th>Expiry_Date</th>
				 <th>Status</th>
                                  
                </tr>
                </thead>
                </table>
				</div>
                            <!-- END BORDERED TABLE PORTLET-->
		</div>						

       
</div>
  </div>
</div>
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<!-- modal windows --->

 <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >

              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
		 
<!--- End -------------------------->


<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
  {
    swal($("#msg").html(),"","success")
    $("#msg").html("");
  }
  
  get_dataTable("0");

$("#sregno").change(function()
{
	var regn=$("#sregno").val();
	get_dataTable(regn);
});

function get_dataTable(vid)
{ 
      $('#example').dataTable( {
         destroy: true,
        "processing": true,
        
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/renewal_doc_ajax/"+vid,// json datasource
               },
        "columnDefs":[
	{"width":"7%","targets":0},
	],
        "columns": [
		    { "data": "action"},
		    { "data": "slno"},
            { "data": "vehicle"},
			{ "data": "vtype"},
            { "data": "docna"},
			{ "data": "docno"},
			//{ "data": "vfrom"},
			{ "data": "vto"},
            { "data": "status" },
       ]
  } );
}

 $('#view-pdf').on('click',function(){
     
		var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        $.createModal({
        title:'Renewal Documents List',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
	   return false; 

    });  
	
	 $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/doc_validity_update",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        });
    
    
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

	

 //});
  


</script>



