<?php
class Model_invoice extends CI_Model{
	
  public function form_insert($dt)
    {
       $this->db->insert('unpaid_trips', $dt);  
    }
	
   public function insert_invoice($dt)
    {
       $this->db->insert('invoices', $dt);  
    }

   public function view_unpaid_trips($where)
	{
		$this->db->select('*');
		$this->db->from('unpaid_trips');
		$this->db->join('customers', 'customers.customer_id=unpaid_trips.up_custid','inner');
		if(!empty($where))
		{
			$this->db->where($where);
		}
		$this->db->where("unpaid_trips.up_invoiceno","0");
		$this->db->where("unpaid_trips.up_status","0");
		$this->db->order_by("unpaid_trips.up_id","desc");
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_amount_of_trip($tid)
	{
		$this->db->select('up_amount');
		$this->db->from('unpaid_trips');
		$this->db->where("up_tripid",$tid);
		$query = $this->db->get();
		return $query->row()->up_amount;
	}
	
	public function get_customer_id($trid)
	{
		$this->db->select('up_custid,up_gstper');
		$this->db->from('unpaid_trips');
		$this->db->where('up_tripid',$trid);
		$query=$this->db->get();
		return $query->row();
	}
		
	public function view_unpaid_invoices($up1,$cl)
	{
		$this->db->select('*');
		$this->db->from('invoices');
		$this->db->join('customers', 'customers.customer_id=invoices.invo_customer','inner');
		if($up1==0 or $up1==1)
		{
			$where="invoices.invo_status=".$up1." and invoices.invo_customer=".$cl;
		}
		else if($up1==2)
		{
			$where="invoices.invo_customer=".$cl;
		}
		else
		{
			$where="invoices.invo_status=0";
		}
		$this->db->where($where);
		$this->db->order_by("invoices.invo_id","desc");
		$query = $this->db->get();
		return $query->result();
	}
	
	public function paid_invoices($where)
	{
		$this->db->select('*');
		$this->db->from('invoices');
		$this->db->join('customers', 'customers.customer_id=invoices.invo_customer','inner');
		$this->db->where($where);
		$this->db->order_by("invoices.invo_id","asc");
		$query = $this->db->get();
		return $query->result();
	}
	
	public function unpaid_invoices($where)
	{
		$this->db->select('*');
		$this->db->from('invoices');
		$this->db->join('customers', 'customers.customer_id=invoices.invo_customer','inner');
		$this->db->where($where);
		$this->db->order_by("invoices.invo_id","asc");
		$query = $this->db->get();
		return $query->result();
	}
	
		
	public function get_customer_name($custid)
	{
		$this->db->select('customer_name');
		$this->db->from('customers');
		$this->db->where('customer_id',$custid);
		$qry=$this->db->get()->row();
		return $qry->customer_name;
	}
	
	public function del_invoice($id)
	{
		$this->db->where('invo_invoiceid', $id);
		$this->db->delete('invoices');
		return $this->db->affected_rows();
	}
	
	
	//----------------------------------------------------
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    public function view_UPCtrip(){

    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','left');
    $this->db->where("trip_management.trip_status=1");
	$this->db->where("trip_management.trip_startdate>",date('y-m-d'));
	$this->db->order_by("trip_management.trip_id","desc");
	
    $query = $this->db->get();
    return $query->result();
}


 public function view_completed($op,$dt1,$dt2)
 {
    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration','driverregistration.driver_id=trip_management.trip_driver_id','left');
    $this->db->where("trip_management.trip_status=2");
		
	if($op==2)
	{
		$where="trip_management.trip_enddate>='".$dt1."'";
		$this->db->where($where);
	}
	
	else if($op==3)
	{
		$dc="trip_management.trip_enddate>='".$dt1."' and trip_management.trip_enddate<='".$dt2."'";
		$this->db->where($dc);
	}	
	else if($op==4)
	{
	$dc="MONTH(trip_management.trip_enddate)='".$dt1."'";
		$this->db->where($dc);
	}	
		
	$this->db->order_by("trip_management.trip_id","desc");
	
    $query = $this->db->get();
    return $query->result();
}


   public function view_trip_starter($op)
   {
    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    //$this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
    $this->db->where("trip_management.trip_status=1");
	if($op==0)
	{
	$this->db->where("trip_management.trip_startdate<=",date('Y-m-d'));
	}
	else if($op==1)
	{
	$this->db->where("trip_management.trip_startdate>",date('Y-m-d'));	
	}
	$this->db->order_by("trip_management.trip_id","desc");
	$query = $this->db->get();
    return $query->result();
}

public function view_trip_details($id){

    $this->db->select("*");
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
    $this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=trip_management.trip_vehicle_id','inner');
   $this->db->where("trip_management.trip_id",$id);
  
    $query = $this->db->get();
    return $query->result();

}

 public function update_trip($id,$userdt)
  {
    $this->db->where('trip_id', $id);
    $this->db->update('trip_management',$userdt); 
  } 

  //update vehicle status ---------------
  
  public function vehicle_update($id,$vudt)
  {
    $this->db->where('vehicle_id', $id);
    $this->db->update('vehicle_registration',$vudt); 
  } 
   //-----update driver trip date---------------------
  
  public function driver_update($id,$dudt)
  {
    $this->db->where('driver_id', $id);
    $this->db->update('driverregistration',$dudt); 
  } 
    
  
  public function delete_trip($id)
{
    $this->db->where('trip_id', $id);
    $this->db->delete('trip_management');
    return $this->db->affected_rows();
}
//-------------------------------------------
public function view_driver()
{
    $this->db->select('*');
    $this->db->from('driverregistration');
    $this->db->where('driver_status', '1');
	$this->db->where('driver_available', '0');
    $query = $this->db->get();
    return $query->result();
}

/*public function view_driver1($sdt,$edt)
{
    $this->db->select('*');
    $this->db->from('driverregistration');
	//$search="driverregistration.driver_enddate<='".$sdt."'";
	//$this->db->where($search);
    $this->db->where('driver_status', '1');
	$this->db->where('driver_available', '0');
    $query = $this->db->get();
    return $query->result();
}*/

public function view_driver1()
{
    $this->db->select('*');
    $this->db->from('driverregistration');
	//$search="driverregistration.driver_enddate<='".$sdt."'";
	//$this->db->where($search);
    $this->db->where('driver_status', '1');
	$this->db->where('driver_available', '0');
    $query = $this->db->get();
    return $query->result();
}



public function view_driverDT($id)
{
    $this->db->select('*');
    $this->db->from('driverregistration');
    $this->db->where('driver_id',$id);
	$query = $this->db->get();
    return $query->result();
}

//------------------------------------------

public function view_vehicles()
{
    $this->db->select('*');
    $this->db->from('vehicle_registration');
	$this->db->join('vehicle_types','vehicle_types.veh_type_id=vehicle_registration.vehicle_type','inner');
	$this->db->where('vehicle_registration.vehicle_available=0');
	$this->db->where('vehicle_registration.vehicle_status=1');
 	$query = $this->db->get();
    return $query->result();
}


public function view_atta_vehicles()
{
    $this->db->select('*');
    $this->db->from('attached_vehicle');
	$this->db->where('attached_vehicle.attached_status=0');
 	$query = $this->db->get();
    return $query->result();
}



public function view_vehicleDT($id)
{
    $this->db->select('*');
    $this->db->from('vehicle_registration');
    $this->db->where('vehicle_id',$id);
	$query = $this->db->get();
    return $query->result();
}


public function view_atta_vehicleDT($id)
{
    $this->db->select('*');
    $this->db->from('attached_vehicle');
    $this->db->where('attached_vehicleid',$id);
	$query = $this->db->get();
    return $query->result();
}


//------------------------------------------

public function view_customers()
{
    $this->db->select('*');
    $this->db->from('customers');
 	$query = $this->db->get();
    return $query->result();
}

public function view_customerDT($id)
{
    $this->db->select('*');
    $this->db->from('customers');
    $this->db->where('customer_id',$id);
	$query = $this->db->get();
    return $query->result();
}

//--------------------------------------------
//update service  kilometers

public function update_service_km($id,$vser1)
  {
    $this->db->where('ser_vehicle_id', $id);
    $this->db->update('services',$vser1); 
  } 

public function insert_service_km($vser1)
  {
      $this->db->insert('services',$vser1); 
  } 

   function getLastInserted() {
        $this->db->select('*');
        $this->db->from('trip_management');
        //$this->db->where("recent_searches.user_id",$uid);
    $this->db->order_by("trip_id", "desc");
        $this->db->limit(1);
    $query = $this->db->get();
    return $query->row();
       }



        public function approve_trip($id)
    {
        $data = array(
            'trip_status' => '2',
                   );
    $this->db->where('trip_id', $id);
    $this->db->update('trip_management', $data); 
    } 
    public function view_ntrip()
 {
    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    //$this->db->join('driverregistration','driverregistration.driver_id=trip_management.trip_driver_id','inner');
    $this->db->where("trip_management.trip_status=3");
    $query = $this->db->get();
    return $query->result();
}
    public function approve_ntrip($id)
    {
        $data = array(
            'trip_status' => '1',
                   );
    $this->db->where('trip_id', $id);
    $this->db->update('trip_management', $data); 
    } 

	public function approve_rejecttrip($id)
    {
        $data = array(
            'trip_status' => '-1',
                   );
    $this->db->where('trip_id', $id);
    $this->db->update('trip_management', $data); 
    } 
	
    public function insert($table, $values){
      $this->db->insert($table, $values);
    }

  public function close_trip($id, $comment)
  {
	 $data = array(
            'trip_status' => '3',
            'trip_closedcomment' => $comment
                   ); 
    $this->db->where('trip_id', $id);
    $this->db->update('trip_management',$data); 
  }

  
  public function get_quotationlist($where = null){
    $this->db->select(" * , (trip_mincharge + trip_othercharge + acm_cost + food_cost) as total");
    $this->where($where);
    $this->db->order_by("tripquot_id", "desc");
    $query = $this->db->get("quotation");
    return $query->result();
}

private function where($condation){
  if($condation != null){
    $this->db->where($condation);
  }
}

public function checkregno($value){
    //$this->db->where("trip_vehicle_id IN (SELECT vehicle_id  FROM vehicle_registration where vehicle_regno = '". $value ."')");
	$this->db->where("trip_vehicle_id",$value);
    $this->db->where("trip_status", "2");
    $query = $this->db->get("trip_management");
    if($query->num_rows() > 0){ return TRUE; }
    else{ return FALSE; }
  }
    
public function check_regno($vid){
	$regno=$this->db->select('vehicle_regno')->from('vehicle_registration')->where("vehicle_id",$vid)->get()->row();
	$this->db->where("loan_vehicleno",$regno->vehicle_regno);
    $query = $this->db->get("loan_vehicle_master");
    if($query->num_rows() > 0){ return TRUE; }
    else{ return FALSE; }
  }
  
  

public function tripcharge($value){
  $this->db->select("SUM(trip_charge) as ");
  $this->db->where("trip_vehicle_id IN (SELECT vehicle_id  FROM vehicle_registration where vehicle_regno = '". $value ."')");
  $this->db->where("trip_status", "2");
  $query = $this->db->get("trip_management");
    
  if($query->num_rows() > 0){
     $result = $query->result();
     return  $result[0]->trip_charge;
  }
  else{ return FALSE; }
} 




public function mindate($value){
  $this->db->select("MIN(trip_enddate) as mindate");
  $this->db->where("trip_status", "2");
  $query = $this->db->get("trip_management");
  
  if($query->num_rows() > 0){
     $result = $query->result();
     return  $result[0]->mindate;
  }
  else{ return FALSE; }
} 


public function view_farelist()
{
    $this->db->select('*');
    $this->db->from('fare_list');
	$this->db->join('vehicle_types','vehicle_types.veh_type_id=fare_list.fare_vehicle','inner');
	$this->db->join('customers','customers.customer_id=fare_list.fare_corporate','inner');
	$query = $this->db->get();
    return $query->result();
}

public function view_dutylist()
{
    $this->db->select('*');
    $this->db->from('driver_schedule');
	$this->db->join('driverregistration','driverregistration.driver_id=driver_schedule.drv_name','inner');
	$this->db->where('MONTH(drv_date)',date('m'));
	$query = $this->db->get();
    return $query->result();
}

public function view_booking_trip($op,$where)
	{
	$w="";
    $this->db->select('*');
    $this->db->from('trip_booking');
	$this->db->join('vehicle_types','vehicle_types.veh_type_id=trip_booking.book_vehicle','inner');
	$this->db->join('customers','customers.customer_id=trip_booking.book_corpid','left');
	if($op=="0")
	{
		$this->db->where("trip_booking.book_status","1");	
	}
	else if($op=='1')
	{
		$this->db->where($where); 
		$this->db->where("trip_booking.book_status","1");	
	}
	else if($op=='2')
	{
		$this->db->where($where); 
		$this->db->where("trip_booking.book_status","2"); 	
	}
	
	else if($op=='3')
	{	
		$this->db->where($where); 
		$this->db->where("trip_booking.book_status","3"); 
	}
	
	
	$this->db->order_by("trip_booking.book_id","desc"); 
	$query = $this->db->get();
    return $query->result();
}

public function view_booking_trip1($op,$where)
	{
	$w="";
    $this->db->select('*');
    $this->db->from('trip_booking');
	$this->db->join('vehicle_types','vehicle_types.veh_type_id=trip_booking.book_vehicle','inner');
	$this->db->join('customers','customers.customer_id=trip_booking.book_corpid','left');
	$this->db->where($where); 
	$query = $this->db->get();
    return $query->result();
}




public function booking_trip_report($where)
	{
    $this->db->select('*');
    $this->db->from('trip_booking');
	$this->db->join('vehicle_types','vehicle_types.veh_type_id=trip_booking.book_vehicle','inner');
	$this->db->join('customers','customers.customer_id=trip_booking.book_corpid','left');
	$this->db->where($where); 
	
	$query = $this->db->get();
    return $query->result();
}




public function edit_booking_trip($bid)
	{
	$w="";
    $this->db->select('*');
    $this->db->from('trip_booking');
	$this->db->where('book_id',$bid); 
	$query = $this->db->get();
    return $query->row();
}


/*
public function view_booking_trip($op,$where)
	{
	$w="";
    $this->db->select('*');
    $this->db->from('trip_booking');
	$this->db->join('vehicle_types','vehicle_types.veh_type_id=trip_booking.book_vehicle','inner');
	$this->db->join('customers','customers.customer_id=trip_booking.book_corpid','left');
	if($op==1)
	{
		$this->db->where("trip_booking.book_status","1");	
	}
	else if($op==2)
	{
		$this->db->where("trip_booking.book_tripdate>=",$where); 
		$this->db->where("trip_booking.book_status","1"); 
	}
	else if($op==3)
	{
		$w="MONTH(trip_booking.book_tripdate)=".$where;
		$this->db->where($w); 
		$this->db->where("trip_booking.book_status","1"); 	
	}
	else if($op==4)
	{
		$this->db->where("trip_booking.book_status","2"); 
	}
	$this->db->order_by("trip_booking.book_id","desc"); 
	$query = $this->db->get();
    return $query->result();
}

*/

 public function booking_update($id,$userdt)
  {
    $this->db->where('book_id', $id);
    $this->db->update('trip_booking',$userdt); 
  } 



public function delete_bookingtrip($bid)
{
    $this->db->where('book_id', $bid);
    $this->db->delete('trip_booking');
    return $this->db->affected_rows();
}

public function cancel_bookingtrip($bid,$ndt)
{
	
    $this->db->where('book_id', $bid);
    $this->db->update('trip_booking',$ndt);
}


public function close_bookingtrip($bid,$ndt)
{
    $this->db->where('book_id', $bid);
    $this->db->update('trip_booking',$ndt);
	return true;
}

 public function update_booking_cancel($id,$ndt)
  {
    $this->db->where('book_id', $id);
    $this->db->update('trip_booking',$ndt); 
  } 

public function get_corp_fare_list($id,$op)
{
	$this->db->select('*');
    $this->db->from('fare_list');
	$this->db->join('vehicle_types','vehicle_types.veh_type_id=fare_list.fare_vehicle','inner');
	$this->db->join('customers','customers.customer_id=fare_list.fare_corporate','inner');
	$this->db->join('category','category.category_id=fare_list.fare_trip','left');
	$this->db->where("fare_list.fare_corporate",$id);
	if($op=='1')
	{
	   $this->db->where("fare_list.fare_category","1");
	}
	else if($op=='2')
	{
	   $this->db->where("fare_list.fare_category","2");
	}
	else if($op=='3')
	{
	   $this->db->where("fare_list.fare_category","3");
	}

	$qry=$this->db->get();
	return $qry->result();
}

public function get_gst()
{
    $this->db->select('*');
    $this->db->from('general');
	$query = $this->db->get()->row();
    return $query->gen_gst;
}
  
  
  public function insert_unpaid_trip($dt)
  {
	   $this->db->insert('unpaid_trips', $dt);  
  }
  
  
  
  
}
?>