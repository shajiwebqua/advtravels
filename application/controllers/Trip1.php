<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trip extends CI_Controller {

function __construct(){
parent::__construct();
$this->load->helper(array('form', 'url'));
$this->load->model('Model_trip');
}

public function index()
{
   $this->load->view('admin/add_trip');
}

public function view_trip()
{
   $this->load->view('admin/view_trip');
}
  
public function add_new_trip()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('name', 'name', 'required');
$this->form_validation->set_rules('purpose', 'purpose', 'required');


 if($this->form_validation->run())
        {
    $name=$this->input->post('name');
    $purpose=$this->input->post('purpose');
    $vehicle=$this->input->post('vehicle');
    $driver=$this->input->post('driver');
    $from=$this->input->post('from');
    $to=$this->input->post('to');
    $startdate=$this->input->post('startdate');
    $enddate=$this->input->post('enddate');
    $starttime=$this->input->post('starttime');
    $endtime=$this->input->post('endtime');
    $startkm=$this->input->post('startkm');
    $endkm=$this->input->post('endkm');
    $charge=$this->input->post('charge');
    $tollparking=$this->input->post('tollparking');
    $interstate=$this->input->post('interstate');
    $gtotal=$this->input->post('gtotal');
    $description=$this->input->post('description');
      $status=$this->input->post('status');
   
            


       $userdt = array(
                'trip_cust_id' => $name,
                
                'trip_purpose' => $purpose,
        'trip_vehicle_id' =>$vehicle,
                'trip_from' =>$from,
                'trip_driver_id' =>$driver,
                'trip_to' =>$to,
                'trip_startdate' => $startdate,
                
                'trip_enddate' => $enddate,
        'trip_starttime' =>$starttime,
                'trip_endtime' =>$endtime,
                'trip_startkm' =>$startkm,
                'trip_endkm' =>$endkm,
                'trip_charge' => $charge,
                
                'trip_tollparking' => $tollparking,
        'trip_interstate' =>$interstate,
                'trip_gtotal' =>$gtotal,
                'trip_description' =>$description,
        'trip_status'=>"1"
               );
         $this->load->Model('Model_trip');
        $this->Model_trip->form_insert($userdt);
        $this->session->set_flashdata('usermsg', '<font color="green">Trip details successfully added</font>');
        redirect('Trip/index');  
              
       

  }
 else{
         $this->session->set_flashdata('usermsg', 'Data Not Inserted Successfully');
        //$data['message']="Data Inserted Successfully";

      redirect('Trip/index');
       }  
}
 
                
public function trip_ajax()
{

        $this->load->model('Model_trip');
        $results=$this->Model_trip->view_trip();
		
        $data1 = array();
        foreach($results  as $r) 
               {
             $edit="<a href='".base_url()."Trip/edit_trip/".$r->trip_id."' id='$r->trip_id' data-toggle='modal' class='edit open-AddBookDialog2'>
<center><button class='btn btn-primary btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
   $view="<a href='#myModal1' id='$r->trip_id' data-toggle='modal' class='view open-AddBookDialog2'>
<center><button class='btn btn-warning btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-list-alt'></span></button></center></a>";
			
             $del=anchor('Trip/del_entry/'.$r->trip_id, 'Delete', array('id' =>'del_conf'));
            
          $d1=$r->trip_startdate;
      $dt1=date("d-m-Y", strtotime($d1));
      $d2=$r->trip_enddate;
      $dt2=date("d-m-Y", strtotime($d2));
      
      
      $date1=date_create($r->trip_startdate);
            $date2=date_create($r->trip_enddate);
            $diff=date_diff($date1,$date2);
      //$diff1=$diff->format("%R%a days");
      $diff1=$diff->format("%a");
      $km=$r->trip_endkm - $r->trip_startkm ;
		if($r->trip_status==1)
		{
			$st="<font color='green'>Active</font>";
		}
		else
		{
			$st="<font color='red'>Inactive</font>";
		}
                      array_push($data1, array(
                         "view"=>"$view",
                            "edit"=>"$edit",
                            "tripid"=>"$r->trip_id",
                            "customer"=>"$r->customer_name",
                            // "purpose"=>"$r->trip_purpose",
                            "driver"=>"$r->driver_name",
                             "date"=>"$dt1"."<font color='red'> =></font> "."$dt2" ,
                            "days"=>$diff1,
                            "totalkm"=>$km,
                            "gtotal"=>"$r->trip_gtotal",
                            // "vehicle"=>"$r->trip_vehicle_id",
                            // "from"=>"$r->trip_from",
                            // "startdate"=>"$r->trip_startdate",
                            // "enddate"=>"$r->trip_enddate",
                            // "starttime"=>"$r->trip_starttime",
                            // "endtime"=>"$r->trip_endtime",
                            // "startkm"=>"$r->trip_startkm",
                            //   "endkm"=>"$r->trip_endkm",
                            // "enddate"=>"$r->trip_enddate",
                            // "charge"=>"$r->trip_charge",
                            // "tollparking"=>"$r->trip_tollparking",
                            // "interstate"=>"$r->trip_interstate",
                            //  "gtotal"=>"$r->trip_gtotal",
                            // "description"=>"$r->trip_description",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

public function trip_details() {

        $data=$this->session->all_userdata();
        $id=$this->input->post('tid');
        //$query = $this->db->select('*')->from('trips')->where('table_customer.customer_id',$id)->get();
        //$row = $query->row();
 
        $this->load->model('Model_trip');
        $results=$this->Model_trip->view_trip_details($id);
    
   foreach($results  as $row) 
    {
    
        $d1=$row->trip_startdate;
      $dt1=date("d-m-Y", strtotime($d1));
      $d2=$row->trip_enddate;
      $dt2=date("d-m-Y", strtotime($d2));
    
    
    echo "             
       <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Trip Details</font></h4>
    </div>
    <br>

           <form  class='form-horizontal' role='form' method='post' action='". site_url('customer/add_new_customer'). "' enctype='multipart/form-data'>
                    <!-- text input -->
                   <div class='col-md-6'>  
                     <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:10px;'>Customer Name </label>
          <div class='col-md-9'>
          <input type='text' class='form-control' name='name' value='".$row->customer_name."' readonly> 
          </div>
          </div>
   
          <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:10px;'>Purpose</label>
          <div class='col-md-9'>  
          
          <input type='text' name='purpose' class='form-control' value='".$row->trip_purpose."' readonly/>
          </div>                   
          </div>
          
          <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:10px;'>Driver</label>
          <div class='col-md-9'>
          <input type='text' class='form-control' name='driver' value='".$row->driver_name."' readonly>
          </div>
          </div>
      
      <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:10px;'>Vehicle</label>
          <div class='col-md-9'>
          <input type='text' class='form-control' name='vehicle' value='".$row->vehicle_regno."' readonly>
          </div>
          </div>
      
      <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:10px;'>From</label>
          <div class='col-md-9'>
          <input type='text' class='form-control' name='from' value='".$row->trip_from."' readonly>
          </div>
          </div>
      
      <div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:10px;'>To </label>
          <div class='col-md-9'>
          <input type='text' class='form-control' name='to' value='".$row->trip_to."' readonly>
          </div>
          </div>

      <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:10px;'>Date (start-End): </label>
          <div class='col-md-9'>
          <input type='text' class='form-control' name='dstart' value='".$dt1." => ".$dt2."' readonly>
          </div>
          </div>
      
      <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:10px;'>Time (Start-End) : </label>
          <div class='col-md-9'>
          <input type='text' class='form-control' name='tstart' value='".$row->trip_starttime." => ".$row->trip_endtime."' readonly>
          </div>
          </div>
      
      
                    
                </div> 
                 <div class='col-md-6'> 
                 <div class='row'>

                 <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:10px;'>KM (Start-End) : </label>
          <div class='col-md-9'>
          <input type='text' class='form-control' name='kmstart' value='".$row->trip_startkm." => ".$row->trip_endkm."' readonly>
          </div>
          </div>
     
     <div class='form-group'>
          <label class='col-md-3' style='padding-top:10px; text-align:left;'>Total Km: </label>
          <div class='col-md-9'>
          <input type='text' class='form-control tx' name='kmend' value='".($row->trip_endkm-$row->trip_startkm)."' readonly>
          </div>
          </div>
          <div class='form-group'>
          <label class='col-md-3' style='padding-top:10px; text-align:left;'>Description </label>
          <div class='col-md-9'>
         <textarea class='form-control' name='description' row='2' readonly>$row->trip_description</textarea>
          </div>
          </div>
</div>

<div class='row'> 
<label class='col-md-12 control-label' style='padding-top:10px;'><hr style='margin:0px;'> </label>  
<br>

   <div class='form-group'>
          <label class='col-md-4 control-label col-sm-offset-3' style='padding-top:10px;'>Charge</label>
          <div class='col-md-5 pull-right'>
          <input type='text' class='form-control tx' name='toll' value='".$row->trip_charge."' readonly>
          </div>
          </div>
      <div class='form-group'>
          <label class='col-md-4 control-label col-sm-offset-3' style='padding-top:10px;'>Toll/Parking : </label>
          <div class='col-md-5 pull-right'>
          <input type='text' class='form-control tx' name='toll' value='".$row->trip_tollparking."' readonly>
          </div>
          </div>
      
      <div class='form-group'>
          <label class='col-md-4 control-label col-sm-offset-3' style='padding-top:10px;'>Inter State Permit: </label>
          <div class='col-md-5 pull-right'>
          <input type='text' class='form-control tx' name='inter' value='".$row->trip_interstate."' readonly>
          </div>
          </div>
            

<label class='col-md-12 control-label' style='padding-top:10px;'><hr style='margin:0px;'> </label>
<br>
      <div class='form-group'>
      <label class='col-md-4 control-label col-sm-offset-3' style='padding-top:8px;font-size:15px;font-weight:bold;'>Grand Total: </label>
          <div class='col-md-5'>
           <input type='text' class='form-control fnt'  name='gtotal' value='".$row->trip_gtotal."' readonly>
          </div>
          </div>
</div>    

                 </div>  

           </form> 
           <div class='modal-footer'>
      
      </div> ";
  
   }
         
}

public function edit_trip() 
{
	
        
       $id = $this->uri->segment(3);
        $query = $this->db->select('*')->from('trip_management')->where('trip_id',$id)->get();
        $data['result'] = $query->result();
         $this->load->view('admin/edit_trip',$data);

    }
public function update_trip()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('name', 'usrname', 'required');
$this->form_validation->set_rules('purpose', 'usrpurpose', 'required');
$this->form_validation->set_rules('vehicle', 'usrvehicle', 'required');

 
       if($this->form_validation->run())
        {
        
    $this->load->model('Model_trip');
	
    $name=$this->input->post('name');
    $purpose=$this->input->post('purpose');
    $vehicle=$this->input->post('vehicle');
    $driver=$this->input->post('driver');
    $from=$this->input->post('from');
    $to=$this->input->post('to');
    $startdate=$this->input->post('startdate');
    $enddate=$this->input->post('enddate');
    $starttime=$this->input->post('starttime');
    $endtime=$this->input->post('endtime');
    $startkm=$this->input->post('startkm');
    $endkm=$this->input->post('endkm');
    $charge=$this->input->post('charge');
    $tollparking=$this->input->post('tollparking');
    $interstate=$this->input->post('interstate');
    $gtotal=$this->input->post('gtotal');
    $description=$this->input->post('description');
      $status=$this->input->post('status');
    $uid=$this->input->post('id');
    
     $userdt = array(
                'trip_cust_id' => $name,
                
                'trip_purpose' => $purpose,
        'trip_vehicle_id' =>$vehicle,
                'trip_from' =>$from,
                'trip_driver_id' =>$driver,
                'trip_to' =>$to,
                'trip_startdate' => $startdate,
                
                'trip_enddate' => $enddate,
        'trip_starttime' =>$starttime,
                'trip_endtime' =>$endtime,
                'trip_startkm' =>$startkm,
                'trip_endkm' =>$endkm,
                'trip_charge' => $charge,
                
                'trip_tollparking' => $tollparking,
        'trip_interstate' =>$interstate,
                'trip_gtotal' =>$gtotal,
                'trip_description' =>$description,
        'trip_status'=>$status
               );
  
        $this->Model_trip->update_trip($uid,$userdt);
        $this->session->set_flashdata('usermsg',  '<font color="green">Trip details Updated..!</font>');
      redirect('Trip/view_trip');

       }
        else 

        {
           redirect('Trip/view_trip');
        }                   
}
	


  public function del_entry()
  {
    
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_trip');
            $result=$this->Model_trip->delete_trip($id);
            if($result)
            {
                    $this->session->set_flashdata('usermsg', '<font color="green"> User details has been deleted..!</font>');
                   redirect('Trip/view_trip');
            }
           else
           {
            $this->session->set_flashdata('usermsg', '<font color="red">Please try again</font>');
            redirect('Trip/view_trip');
           }
  }

  public function add_customer() 
  {
  
     
    
         
    echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Add customer</font></h4>
    </div>
    <br>

           <form  class='form-horizontal' role='form' method='post' action='". site_url('Trip/add_new_customer'). "' enctype='multipart/form-data'>
                    <!-- text input -->
                    
                    <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Name : </label>
                    <div class='col-md-8'>  
          
          <input type='text' name='name' class='form-control'  id='mask_date2' />
          </div>                   
          </div>
                    

                 
            
         <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Email: </label>
                    <div class='col-md-8'>  
          
          <input type='text' name='email' class='form-control'  id='mask_date2' />
          </div>                   
          </div>

          <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Address </label>
                    <div class='col-md-8'>  
          
         <textarea name='address' row='2' class='form-control'></textarea>
          </div>                   
          </div>
       
        <div class='form-group'>
          <label class='col-md-3  control-label'>Mobile : </label>
          <div class='col-md-8'>
          <input type='text' class='form-control' placeholder='Enter mobile no' name='mobile'>
            </div>
          </div>
          
          <div class='form-group'>
          <label class='col-md-3  control-label'>Landline </label>
          <div class='col-md-8'>
          <input type='text' class='form-control' placeholder='Enter landline' name='landline' value='' required/>
          </div>
          </div>
     
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-9'>
                <input type='submit' class='btn btn-primary' value='Add'/>
                </div>
                </div>                                
           </form> <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div> ";
  
  
    }

    public function add_new_customer()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('name', 'name', 'required');
$this->form_validation->set_rules('email', 'email', 'required');


 if($this->form_validation->run())
        {

    
      
             
    $name=$this->input->post('name');
    $email=$this->input->post('email');
    $mobile=$this->input->post('mobile');
    $address=$this->input->post('address');
    $landline=$this->input->post('landline');
    $status=$this->input->post('status');

	$userdt = array(
                'customer_name' => $name,
                
                'customer_email' => $email,
        'customer_phone' =>$mobile,
                'customer_landline' =>$landline,
                'customer_address' =>$address,
        'customer_status'=>"1"
               );
         $this->load->Model('Model_customer');
        $this->Model_customer->form_insert($userdt);
        $this->session->set_flashdata('usermsg', '<font color="green">New Customer added successfully added</font>');
        redirect('Trip/index');  
              
       

  }
 else{
         $this->session->set_flashdata('usermsg', 'Data Not Inserted Successfully');
        //$data['message']="Data Inserted Successfully";

      redirect('Trip/index');
       }  
}





}