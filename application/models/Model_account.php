<?php
defined("BASEPATH") OR exit("'No direct script access allowed'");

class Model_account extends CI_Model{


	public function __construct(){
		parent::__construct();
	}

	public function insert($table,$data){
		return $this->db->insert($table,$data);
	}
	
	public function select_all($table)
	{
		$this->db->select("*");
		$this->db->from($table);
		$query = $this->db->get();
		return $query->result();
	}


	public function select($table,$where, $column = "*")
	{
		$this->db->select($column);
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	public function select_one($table,$where, $column = "*")
	{
		$this->db->select($column);
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->row();
	}
	
	
	public function update($table, $values, $where){
		$this->db->where($where);
		$this->db->update($table, $values);
	}

	public function delete($table, $where)
	{
		$this->db->where($where);
		$this->db->delete($table);
		//return $this->db->affected_rows();
	}

	public function get_ltlreference($value){
		$this->db->where("loan_ltlrefno", $value);
		$query = $this->db->get("loan_vehicle_master");
		if($query->num_rows() > 0){ return TRUE; }
		else{ return FALSE; }
	}

	public function checkregno($value){
		$this->db->where("loan_vehicleno", $value);
		$query = $this->db->get("loan_vehicle_master");
		if($query->num_rows() > 0){ return TRUE; }
		else{ return FALSE; }
	}

	public function downpayment($value){
	  $this->db->select("loan_downpayment");
	  $this->db->where("loan_vehicleno", $value);
	  $query = $this->db->get("loan_vehicle_master");
	  
	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->loan_downpayment;
	  }
	  else{ return FALSE; }
	}

	public function loanamount($value){
	  $this->db->select("loan_amount");
	  $this->db->where("loan_vehicleno", $value);
	  $query = $this->db->get("loan_vehicle_master");
	  
	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->loan_amount;
	  }
	  else{ return FALSE; }
	}

	public function totalinterest($value){
	  $this->db->select("loan_interest");
	  $this->db->where("loan_vehicleno", $value);
	  $query = $this->db->get("loan_vehicle_master");

	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->loan_interest;
	  }
	  else{ return FALSE; }
	}

	public function paidamount($value){
	  $this->db->select("SUM(loan_instalment) as instalment");
	  $this->db->where("status = '1' AND ltl_ref_no IN (SELECT loan_ltlrefno FROM loan_vehicle_master where loan_vehicleno ='". $value ."' )");
	  $query = $this->db->get("loan_shedule");

	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->instalment;
	  }
	  else{ return FALSE; }
	}

	public function vehicleamount($value){
	  $this->db->select("loan_vehicleamount");
	  $this->db->where("loan_vehicleno", $value);
	  $query = $this->db->get("loan_vehicle_master");

	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->loan_vehicleamount;
	  }
	  else{ return FALSE; }
	}

	public function vehiclecost($value){
	  $this->db->select("(loan_vehicleamount + loan_interest) as vehicle_amount");
	  $this->db->where("loan_vehicleno", $value);
	  $query = $this->db->get("loan_vehicle_master");

	  if($query->num_rows() > 0){
	     $result = $query->result();
	     return  $result[0]->vehicle_amount;
	  }
	  else{ return FALSE; }
	}
	  public function delete_loan($id)
{
    $this->db->where('loan_ltlrefno', $id);
    $this->db->delete('loan_vehicle_master');
    return $this->db->affected_rows();
}

 public function delete_loan1($id)
{
    $this->db->where('ltl_ref_no', $id);
    $this->db->delete('loan_schedule');
    return $this->db->affected_rows();
}
public function view_loan(){

		$this->db->select('*');
		$this->db->from('loan_vehicle_master');
		$query = $this->db->get();
		return $query->result();
	}
	
public function driver_balance($table,$where, $column = "*")
	{
		$this->db->select($column);
		$this->db->from($table);
		$this->db->where($where);
		$this->db->order_by('driver_id','desc');
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->row();
	}
	
public function select_payments($op,$dt1,$dt2)
{
	    $this->db->select('*');
		$this->db->from('acc_payments');
		if($op=='1')
		{
			$this->db->join('driverregistration','driverregistration.driver_id=acc_payments.acc_vname','inner');
		}
		else if ($op=='2')
		{
			$this->db->join('staffregistration','staffregistration.staff_id=acc_payments.acc_vname','inner');
		}
		
		$this->db->join('voucher_group','voucher_group.vg_id=acc_payments.acc_vgroup','inner');
		$w="acc_payments.acc_vdate>='".$dt1."' and acc_payments.acc_vdate<='".$dt2."'";
		$this->db->where($w);
		$this->db->where('acc_vcategory',$op);
		$this->db->order_by('acc_vid','desc');
		$query = $this->db->get();
		return $query->result();
}	

public function corporate_receipts()
{
	    $this->db->select('*');
		$this->db->from('acc_receipts');
		$this->db->join('customers','customers.customer_id=acc_receipts.acc_rename','inner');
		$this->db->where("MONTH(acc_receipts.acc_redatefrom)='".date('m')."'");
		$this->db->order_by('acc_reid','desc');
		$query = $this->db->get();
		return $query->result();
}		


public function search_receipts($dt1,$dt2,$coid)
{
	    $this->db->select('*');
		$this->db->from('acc_receipts');
		$this->db->join('customers','customers.customer_id=acc_receipts.acc_rename','inner');
		$w="acc_receipts.acc_redatefrom>='".$dt1."' and acc_receipts.acc_redateto<='".$dt2."' and  acc_rename='".$coid."'";
		$this->db->where($w);
		$this->db->order_by('acc_reid','desc');
		$query = $this->db->get();
		return $query->result();
}

public function driver_account($dt1,$dt2,$drid)
{
	    $this->db->select('*');
		$this->db->from('acc_payments');
		$this->db->join('driverregistration','driverregistration.driver_id=acc_payments.acc_vname','inner');
		$this->db->join('voucher_group','voucher_group.vg_id=acc_payments.acc_vgroup','inner');
		$w="acc_payments.acc_vdate>='".$dt1."' and acc_payments.acc_vdate<='".$dt2."' and  acc_vname='".$drid."'";
		$this->db->where($w);
		$this->db->where("acc_payments.acc_vcategory","1");
		$this->db->order_by('acc_vid','desc');
		$query = $this->db->get();
		return $query->result();
}
public function staff_account($dt1,$dt2,$stid)
{
	    $this->db->select('*');
		$this->db->from('acc_payments');
		$this->db->join('staffregistration','staffregistration.staff_id=acc_payments.acc_vname','inner');
		$this->db->join('voucher_group','voucher_group.vg_id=acc_payments.acc_vgroup','inner');
		$w="acc_payments.acc_vdate>='".$dt1."' and acc_payments.acc_vdate<='".$dt2."' and  acc_vname='".$stid."'";
		$this->db->where($w);
		$this->db->where("acc_payments.acc_vcategory","2");
		$this->db->order_by('acc_vid','desc');
		$query = $this->db->get();
		return $query->result();
}
public function vgroup_account($dt1,$dt2,$vgroup,$op)
{
	    $this->db->select('*');
		$this->db->from('acc_payments');
		$this->db->join('voucher_group','voucher_group.vg_id=acc_payments.acc_vgroup','inner');
		if($op=='1')
		{
			$w="acc_payments.acc_vdate>='".$dt1."' and acc_payments.acc_vdate<='".$dt2."'";
		}
		else
		{
			$w="acc_payments.acc_vdate>='".$dt1."' and acc_payments.acc_vdate<='".$dt2."' and  acc_vgroup='".$vgroup."'";
		}
		$this->db->where($w);
		$this->db->order_by('acc_vid','desc');
		$query = $this->db->get();
		return $query->result();
}


public function get_revenue_list($dt1,$dt2)
{
	    $this->db->select('*');
		$this->db->from('acc_payments');
		$this->db->join('staffregistration','staffregistration.staff_id=acc_payments.acc_vname','inner');
		$this->db->join('voucher_group','voucher_group.vg_id=acc_payments.acc_vgroup','inner');
		$w="acc_payments.acc_vdate>='".$dt1."' and acc_payments.acc_vdate<='".$dt2."' and  acc_vname='".$stid."'";
		$this->db->where($w);
		$this->db->where("acc_payments.acc_vcategory","2");
		$this->db->order_by('acc_vid','desc');
		$query = $this->db->get();
		return $query->result();
}
}