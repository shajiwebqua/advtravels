<?php
include('application/views/common/header.php');
?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1><b>Vehicle Expences</b></h1>
		<ol class="breadcrumb" style='font-size:15px;'>
			<li><a href="<?php echo base_url('Vehicle/Expenses');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Expense List</button></a></li>
			
	    </ol> 
         <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
  
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!--Add new user details -->

   <div style="background-color:#fff;border-radius:2px;padding:15px; ">
    
		<div class="portlet-title">
		 <div class="caption" >
				<h4 style='margin-top:0px;'><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Expenses</h4>
		</div>
		</div>
 <hr style="background-color:#cecece,border:1px;width:100%;top:10px;margin:0px;padding:3px 0px 10px 0px;">
 
 <div class="portlet-body form">
     <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Vehicle/add_expense1')?>" enctype="multipart/form-data">
             
		<div class="form-group">
		<label class="col-md-2" style="padding-top:5px;" align="right">Vehicle RegNo : </label>
        <div class="col-md-4">
		 <div id="regno1">
			<select name="regno" id="regno" class="form-control" required>
             <option value=''>------------</option>
             <?php
			$vehicleid=$this->db->select('*')->from('vehicle_registration')->get()->result();
			foreach($vehicleid as $values6)
			{
			?>
			  <option value='<?=$values6->vehicle_id;?>'> <?=$values6->vehicle_regno;?> </option>
                <?php } ?>
                </select>
            </div>
		  </div> 
		</div> 
		
		<div class="form-group">
		<label class="col-md-2" style="padding-top:5px;" align="right">Expense Type : </label>
			<div class="col-md-4">
			<div id="regno1">
				<select name="etype" id="etype" class="form-control" required>
				 <option value=''>------------</option>
				 <?php
				  $etype=$this->db->select('*')->from('expense_type')->where('etype_group','1')->get()->result();	
					foreach($etype as $values6)
					{
					?>
						<option value='<?=$values6->etype_id;?>'> <?=$values6->etype_name;?> </option>
					<?php 
					} 
					?>
				</select>
            </div>
				
			</div> 
				<button  type='button' id='btnadd'  data-target='#myModal3' data-toggle='modal' class='btn btn-primary' style='position: relative;float: left;margin-left: -14px; margin-top: 2px;padding:2px 8px 2px 8px;'><i class="fa fa-plus" aria-hidden="true"></i></button>
			</div> 
			
		<div class="form-group">
		<label class="col-md-2" style="padding-top:5px;" align="right">Date : </label>
        <div class="col-md-2">
		<input type='date' name="edate" class='form-control' value="<?php echo date('Y-m-d');?>" required>
		</div> 
		</div> 
		
		<div class="form-group">
		<label class="col-md-2" style="padding-top:5px;" align="right">Bill No : </label>
        <div class="col-md-2">
		<input type='number' name="billno" class='form-control'  required>
		</div> 
		</div> 
		
		<div class="form-group">
		<label class="col-md-2" style="padding-top:5px;" align="right">Bill Date : </label>
        <div class="col-md-2">
		<input type='date' name="bdate" class='form-control' value="<?php echo date('Y-m-d');?>" required>
		</div> 
		</div> 
		
		<div class="form-group">
		<label class="col-md-2" style="padding-top:5px;" align="right">Amount : </label>
        <div class="col-md-2">
		<input type='text' name="amount" class='form-control'  required>
		</div> 
		</div> 
	
	
	<div class="form-group">
		<label class="col-md-2" style="padding-top:5px;" align="right">Narration : </label>
        <div class="col-md-4">
		<textarea rows=4 name="narra" class='form-control'  required>Nil</textarea>
		</div> 
		</div> 
	
		
		<!---  attache your proof ----------------------------------->
					
					<div class='form-group' style='margin-top:20px;'> 			
					
					  <div class="row">
					  <label class='col-md-2 control-label' style='padding-top:3px;'>Upload Bill Copy: </label>
					  
					  <div class="control-group col-md-6" id="fields" style='padding-left:20px;'>
						  <div class="controls">
						   	  <div class="entry input-group col-xs-3" style='margin-top:5px;'>
								<input class="btn btn-primary" name="files[]" type="file">
								<span class="input-group-btn">
							  <button class="btn btn-success btn-add" type="button">
												<span class="glyphicon glyphicon-plus"></span>
								</button>
								</span>
							  </div>
						 
						</div>
					  </div>
					</div>
					</div>
		
		<!--------------------------------------------------------------------->
		
				<label style="background-color:#cecece;width:100%;height:1px;margin:0px padding:0px;"></label> 
					<div class='form-group'> 
					<label class='col-md-2 control-label' style='padding-top:3px;'></label>
						<div class='col-md-7'>
							<input type='submit' class='btn btn-primary' value='Save Details'style='padding:7px 20px 7px 20px;' />
							</center>
						</div>
					</div> 

</form>
<!--- row closed -->
 

       </div>
 </div>
</div>    
<br>  
  </div>
  </div>
</section>

								<div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body" style="height:-30px;">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Edit</h4>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
								</div><!-- row end -->

								
								<div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria- hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body" style='height:150px;'>
												
												<div class="form-group">
												<div class='row'>
												<label class="col-md-6" style="padding-top:5px;Font-size:16px;" >Add Expense Type </label>
												<div class='col-md-6'>
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
												</div>
												</div>
												</div>
												<div class='row'style='margin-top:0px;margin-bottom:15px;'>
												<hr style='margin:0px;'>
												</div>
												
                                                <div class="form-group">
												<div class='row'>
												<label class="col-md-3" style="padding-top:5px;" align="right">Expense Type: </label>
												<div class="col-md-8">
													<input type='text' name="exptype1" id='exptype1' class='form-control'  required>
													<input type='hidden' name="expgroup" id='expgroup' value='1' class='form-control'  required>
												</div> 
												</div> 
												</div>
																								
												<div class="form-group">
												<div class='row'>
												<label class="col-md-3" style="padding-top:5px;" align="right"></label>
												<div class="col-md-8" align='right'>
												<button type="button" id='btnexp1' data-dismiss="modal" class="btn btn-primary"> Save </button>
												<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
												</div> 
												</div> 
												</div>
												
                                            </div>

                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
								</div>

<?php include('application/views/common/footer.php');?>
  </div>
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script> 

<script type="text/javascript">
 $("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
	
$("#btnexp1").click(function()
{
	var etype1=$("#exptype1").val();
	var egroup=$("#expgroup").val();
	jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Vehicle/add_exp_type",
        dataType: 'html',
        data: {exptype:etype1,expgroup:egroup},
        success: function(res) {
		$("#etype").append("<option value='"+res+"'>"+etype1+"</option>")
                    }
            });
});

</script>



  </body>
</html>
