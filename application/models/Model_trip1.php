<?php
class Model_trip extends CI_Model{
	
    public function form_insert($userdt)
    {
       $this->db->insert('trip_management', $userdt);  
    }

    public function view_trip(){

    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');

    $query = $this->db->get();
    return $query->result();
}

public function view_trip_details($id){

    $this->db->select("*");
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
    $this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=trip_management.trip_vehicle_id','inner');
  $this->db->where("trip_management.trip_id",$id);
  
    $query = $this->db->get();
    return $query->result();

}


 public function update_trip($id,$userdt)
  {
    $this->db->where('trip_id', $id);
    $this->db->update('trip_management',$userdt); 
  } 

  public function delete_trip($id)
{
    $this->db->where('trip_id', $id);
    $this->db->delete('trip_management');
    return $this->db->affected_rows();
}
}
?>