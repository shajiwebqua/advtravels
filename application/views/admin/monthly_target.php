<?php
 include('application/views/common/header.php');?>
<style>
.numericCol
{
	text-align:right;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <b>Set Monthly Target</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px;'>
		<!-- <li><a href="<?php echo base_url('General/country');?>" style='color:#4b88ed;'><i class="fa fa-globe" aria-hidden="true"></i>Countries</a></li>
		<li><a href="<?php echo base_url('General/category');?>" style='color:#4b88ed;'><i class="fa fa-reorder" aria-hidden="true"></i>Categories</a></li>
		<li><a href="<?php echo base_url('General/agents');?>" style='color:#4b88ed;'><i class="fa fa-user-circle-o" aria-hidden="true"></i>Agents</a></li>
		<li><a href="<?php echo base_url('General/role');?>" style='color:#4b88ed;'><i class="fa fa-gears" aria-hidden="true"></i>Role </a></li>
		<li><a href="<?php echo base_url('General/branch');?>" style='color:#4b88ed;'><i class="fa fa-snowflake-o" aria-hidden="true"></i>Branches</a></li>
		<li><a href="<?php echo base_url('General/profession');?>" style='color:#4b88ed;'><i class="fa fa-user-circle-o" aria-hidden="true"></i>Profession</a></li>
		 -->
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->
              
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
   <div class="page-content">
   
   <div style="padding:0px 15px 0px 15px;">
	 <div id="idmsg"  style="background-color:#fff;height:25px;margin-bottom:5px;">
	 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center></div>
	</div>
				 
		
                    <div class="col-md-5">
                    <div style="background-color:#fff;padding:10px; ">
					   
                        <!-- <div class="row"> -->
        <div class="col-md-12">
		<label class="control-label" style='font-size:18px;'><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Enter monthly targets</label>
			   <hr style='margin:2px 0px 15px 0px;'>
          </div>
             <!-- <label style="background-color:#cecece;height:1px;width:100%"></label> -->
                   <!-- </div>/.box-header -->
          <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/add_mon_targets')?>" enctype="multipart/form-data">
                  
					<?php
						$mon=$this->session->userdata('tmonth');
						$yr=$this->session->userdata('tyear');
					
					?>
				  
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Select Month</label>
                           <div class="col-md-5">
						   <select name='month' class='form-control' required>
								<option value='' <?php if($mon=="") echo "selected";?> >---month----</option>
								<option value='1' <?php if($mon==1) echo "selected";?>>JANUARY</option>
								<option value='2' <?php if($mon==2) echo "selected";?>>FERUARY</option>
								<option value='3'<?php if($mon==3) echo "selected";?>>MARCH</option>
								<option value='4' <?php if($mon==4) echo "selected";?>>APRIL</option>
								<option value='5' <?php if($mon==5) echo "selected";?>>MAY</option>
								<option value='6' <?php if($mon==6) echo "selected";?>>JUNE</option>
								<option value='7' <?php if($mon==7) echo "selected";?>>JULY</option>
								<option value='8' <?php if($mon==8) echo "selected";?>>AUGUST</option>
								<option value='9' <?php if($mon==9) echo "selected";?>>SEPTEMBER</option>
								<option value='10' <?php if($mon==10) echo "selected";?>>OCTOBER</option>
								<option value='11' <?php if($mon==11) echo "selected";?>>NOVEMBER</option>
								<option value='12'<?php if($mon==12) echo "selected";?> >DECEMBER</option>
						   </select>
						   </div>
						   
						   <div class="col-md-2" style='padding-left:0px;padding-right:0px;'>
						   <select name='year' class='form-control' required>
								<option value='' <?php if($mon=="") echo "selected";?> >-year-</option>
								<?php
								$y=date('Y')+1;
								for($x=$y;$x>=2017;$x--)
								{
								?>
								<option value='<?=$x;?>' <?php if($yr==$x) echo "selected";?>><?=$x;?></option> 
								<?php
								}
								?>
								
						   </select>
						   </div>
                        </div>
                     </div>
					 
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Select Vehicle</label>
                           <div class="col-md-7" style='padding-right:0px;'>
						   <select name='vehicle' class='form-control' required>
						   <option value=''>----Vehicle----</option>
						   <?php
						   $result=$this->db->select('vehicle_id,vehicle_regno')->from('vehicle_registration')->get()->result();
						   
						   foreach($result as $r1)
						   {
							   echo "<option value='". $r1->vehicle_id."'>".$r1->vehicle_regno."</option>";
						   }
						   ?>
						   </select>
						   </div>
                        </div>
                     </div>
				 
                     <div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Target Amount</label>
                           <div class="col-md-7" style='padding-right:0px;'>
                            <input type="number" class="form-control"  name="amount" required>
                           </div>
                        </div>
                     </div>
					 
					 <hr>

                    <div class="form-group">
                     <div class="row">
                          <div class="col-md-11" style="text-align:right;">
                                   <button type="submit" class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add details</button>
                          </div>
                        </div>
                       </div>



                        </form>
                       </div>
                    </div>
        <!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
        
            <!-- <div class="row"> -->
            <div class="col-md-7" style='padding-left:2px;padding-right:2px;'>
            <div style="background-color:#fff;padding:10px; ">
                                 <!-- BEGIN BORDERED TABLE PORTLET-->
                <div class="portlet light portlet-fit bordered">
                          
                <div class="portlet-body">
                     <div class="row" style='padding:0px 10px 3px 10px;'>
					 <div class='col-md-12' style='background-color:#e4e4e4;padding:5px;'>
					 <label class="col-md-3 control-label" style='margin-top:5px;'><b><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Monthly List</b></label>
							 <label class="col-md-3 control-label" style='text-align:right;margin-top:5px;'>Select Month</label>
						<div class="col-md-4">
							 <select name='smonth' class='form-control' id='smonth' required>
								<option value='' >---month----</option>
								<option value='1'>JANUARY</option>
								<option value='2'>FERUARY</option>
								<option value='3'>MARCH</option>
								<option value='4'>APRIL</option>
								<option value='5'>MAY</option>
								<option value='6'>JUNE</option>
								<option value='7'>JULY</option>
								<option value='8'>AUGUST</option>
								<option value='9'>SEPTEMBER</option>
								<option value='10'>OCTOBER</option>
								<option value='11'>NOVEMBER</option>
								<option value='12'>DECEMBER</option>
						   </select>
						
						</div>
						<div class="col-md-2" style='padding-left:0px;padding-right:0px;'>
						   <select name='syear' class='form-control' id="syear" required>
						   <option value='' <?php if($mon=="") echo "selected";?> >-year-</option>
								<?php
								$y=date('Y')+1;
								for($x=$y;$x>=2017;$x--)
								{
								?>
								<option value='<?=$x;?>' <?php if($x==date('Y')) echo "selected";?>><?=$x;?></option>
								<?php
								}
								?>
							</select>
						   </div>
						
					</div>
					</div>
					<div class='row'  style='margin-top:10px;margin-bottom:15px;'>
					<hr style='margin:0px 10px;'>
					</div>
					
						<table class="table table-striped table-hover table-bordered" id="example">
						<thead>
						   <tr>
							 <th>Action</th>
							 <th>Vehicle</th>
							 <th>Month</th>
							 <th>Year</th>
							 <th>Amount</th>
							</tr>
							</thead>
						</table>
							
					</div>
                           <!-- END BORDERED TABLE PORTLET-->
                 </div>
                </div>
              </div>
		</div> <!-- end Row --->
        <!-- </div> -->
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  </div>
    

</section>
</div>

<div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
        
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"success")
	if(mg[0]==2)
		swal("Updated",mg[1],"success")
	if(mg[0]==3)
		swal("Deleted",mg[1],"success")
	if(mg[0]==4)
		swal("Try Again",mg[1],"error")
    $("#msg").html("");
  }
//---------

get_dataTable("0");

$("#smonth").change(function()
{
var mon=$("#smonth").val();
get_dataTable(mon);
		
});
 
function get_dataTable(mon)
{ 
	$('#example').dataTable( {
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/targets_ajax/"+mon,// json datasource
               },
		"aoColumnDefs": [
		 { "sClass": "numericCol", "aTargets": [ 4 ] }
        ],
			   
		"columnDefs":[
		{"width":"10%","targets":0},
		{"width":"20%","targets":2},
		{"width":"12%","targets":3},
		{"width":"15%","targets":4},
		],
        "columns": [
            { "data": "action" },
            { "data": "vehicle" },
            { "data": "month"},
            { "data": "year"},
			{ "data": "amount"},
       ]
  });

}

      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/edit_targets",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 


  // $('#example tbody').on('click', '.change', function () {
  //       var Result=$("#myModal2 .modal-body");
  //    //   $(this).parent().parent().toggleClass('selected');
  //        var id =  $(this).attr('id');
  //        //alert(id);
  //        //alert(id);
  //       jQuery.ajax({
  //       type: "POST",
  //       url: "<?php echo base_url(); ?>" + "userprofile/change_password",
  //       dataType: 'html',
  //       data: {uid: id},
  //       success: function(res) {
  //       Result.html(res);
  //                   }
  //           });
    
  //       });     
    
    
    
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#idmsg').html(''); }, 3000);
		

 //});
  


</script>



