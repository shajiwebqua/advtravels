<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');
?>

<style type="text/css">
.labelfont
{
	border:1px solid #e4e4e4;
	padding:5px;
	width:100%;
	font-size:18px;
	font-weight:bold;
	text-align:right;
}

</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1 class='heading' style='color:#00adee'>Loan Re-Payment 
		</h1>
		<ol class="breadcrumb">
			<!-- <li><a href="#" data-target="#myModal1" data-toggle='modal'><button class='btn btn-primary' name='setpayment'>Set Re-payment</button></a></li> -->
		</ol>
	</section>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
	<!-- Content Header end -->
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				<!-- <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;"> -->
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 <!--</div>-->
			</div>

		<div class="row">
			<div class="col-md-12">
				<div style="background-color:#fff;border-radius:10px;padding:2px 15px 5px 15px;">
				  <div class="portlet-body form" style="margin:0px;">
						<div class="box-body">

					  <?php
							$query=$this->db->select('*')->from('loan_shedule')->where(array('MONTH(loan_date)'=>date('m'),"status"=>'1'))->get()->result();
							
							$instot=0;
							$inttot=0;
							$pritot=0;
							foreach($query as $r2)
							{
								 $instot+=$r2->loan_instalment;
								 $inttot+=$r2->loan_intrest;
								 $pritot+=$r2->loan_principal;
							}
							
							
							
							
					  ?>
						<!-- display schedule ------------------------>
						
						<div class="row">
						<label class='control-label col-md-10' style='font-size:18px;'>Loan Re-payment of this month</label>
						<div class='col-md-2'>
						<!--<a  id='setpayment' href="#" data-toggle='modal'><button class='btn btn-primary' name='setpayment' >Set Re-payment</button></a>-->
						</div>
						</div>
						<div class="row" >
						<hr  style='margin-top:0px; padding:0px;'>
						</div>
						
						<div class="row">
						<table class="table table-hover table-bordered" id="example">
	                            <thead>
	       								<tr style="color:#5068f8;">
	       								 <th>ID</th>
										 <th>LTL Ref.NO.</th>
	       								 <th>Date</th>
	       								 <th>Loan</th>
	       								 <th>Installment</th>
	       								 <th>Interest</th>
	       								 <th>Principle</th>
	       								 <th>Check No</th>
	       								 <th>Voucher No</th>
	       								 </tr>
	       								</thead>
	       						<tbody id="loan_shedule">			
	       						</tbody>		
	                     </table>
						</div>
						
						
						<div class='row'>
						<label style="background-color:#cecece;width:100%;height:1px;"></label>
						</div>
						
						<div class='row'>
						<label class='control-label col-md-2 col-md-offset-4' >Total Installment:</label>
						<label class='control-label col-md-2' >Total Interrest :</label>
						<label class='control-label col-md-2' >Total Principle :</label>
						<label class='control-label col-md-2' ></label>
										
						</div>
						
						
						
						
						<div class='row'>
						<div class='col-md-2'>	<label class='control-label'> </label></div>
						<div class='col-md-2'>	</div>
						<div class='col-md-2'>	<label class='control-label labelfont' ><?php echo $instot;?></label></div>
						<div class='col-md-2'>	<label class='control-label labelfont' ><?php echo $inttot;?></label></div>
						<div class='col-md-2'>	<label class='control-label labelfont' ><?php echo $pritot;?></label>	</div>
						<div class='col-md-2'>	</div>
					
						</div>
	  				</div>
	  			</div>

	  </div>	
	</div>   
</div>
</section>
</div>

<div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Re-Payment</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>          



<?php include('application/views/common/footer.php');?>
<script type="text/javascript">

   $("#msg").hide();
//sweet alert ----------------------------
	if($("#msg").html()!="")
	{
		
		alert($("#msg").css('color'));
		
		swal( $("#msg").html(),"","success");
		$("#msg").html("");
	}

	
		$('#example').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
            
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Loan/loanpayment_ajax",// json datasource
               },
			   
		"columnDefs":[
		{"width":"8%","targets":0},
		{"width":"20%","targets":1},
		{"width":"8%","targets":4},
		{"width":"8%","targets":5},
		{"width":"8%","targets":6},
		],
			   
       "columns": [

			{ "data": "lsid"},
			{ "data": "ltlno"},
			{ "data": "ldate"},
			{ "data": "lamount"},
			{ "data": "linstall"},
            { "data": "linter"},
            { "data": "lprin" },
            { "data": "lcheque" },
            { "data": "lvoucher" },
       ]
    });

  var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });	
 
  
  
 /* $('#setpayment').click(function ()
  {
        var Result=$("#myModal1 .modal-body");
		var ltl=$('#ltf_ref_no').val();
     	// var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(ltl=="")
		 {
			 alert ("please Select LTL Ref No..");
		 }
		 else
		 {			
			jQuery.ajax({
			type: "POST",
			url: "<?php //echo base_url(); ?>" + "Loan/set_repayment",
			dataType: 'html',
			data: {ltlref: ltl},
			success: function(res)
			{
			Result.html(res);
						}
			});
		 }
  }); */
    
 
	 $('.datepicker').click(function(){
	 	$(this).datepicker({
		format: 'dd-mm-yyyy',
		
	    autoclose: true,
	    todayHighlight: true
	});
	   });  
	   

///dropdown list  customer name
   $('#ltf_ref_no').selectize({
     create: false
     , sortField: {
     //field: 'text',
     direction: 'asc'
   }
   , dropdownParent: 'body'
   });
	   
	   
</script>