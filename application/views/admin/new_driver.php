<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');
?>
<style>
	/* hide number  spinner*/
	input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
	.ttext
	{
		background-color:#fff;
		font-weight:bold;
		color:Green;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 

	<!-- Main content -->

	<?php 
    //include("application/views/common/top_menu.php");
	?>

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><b>New Driver Details</b> </h1>
		<ol class="breadcrumb">
			<li> <a href="<?php echo base_url("Driver");?>" style="color:#4b88ed;font-size:15px;"> <button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Drivers List</button></a></li>
		</ol> 
	</section>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
	<!-- Content Header end -->
	<section class="content"> 
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12">

				<!--Add new user details -->
				<div style="background-color:#fff;border-radius:2px;padding:15px; ">

	<div class="portlet-title">
  <!--<div class="caption">
        <div style="color:blue;" >
         <h4>Add driver details </h4>
		 <hr>
      </div>-->
      <center><h5 style="color:green;" id="msg" ><?php echo $this->session->flashdata("message"); ?></h5>
	  <h5 style="color:green;" id="msgerr" ><?php echo $this->session->flashdata("message1"); ?></h5>
	  </center>
      <!--</div> -->
  </div>
  <div class="portlet-body form">
  		<!-- text input -->
  		<div class="box-body">
  			<div class='col-md-12'>
 <form  class='form-horizontal' role='form' method='post' action="<?php echo base_url('Driver/add_new_driver') ?>" enctype='multipart/form-data' onsubmit='return checkdata();'>
    <!-- text input -->
 <div class='form-group'>
<label class='col-md-2 control-label' style='padding-top:5px;'>Photo :</label>
 <div class='col-md-5'>
		 <div class='fileinput fileinput-new' data-provides='fileinput'>
		 <div class='fileinput-new thumbnail' style='width: 145px;height: 110px;'>
		  <img src='<?php echo base_url('assets/dist/img/user.jpg');?>' alt='' /> </div>
		  
		   <div class='fileinput-preview fileinput-exists thumbnail' style='max-width: 150px; max-height: 155px;'> </div>
		 <div>
		  <span class='btn btn-default btn-file'>
		  <span class='fileinput-new'> Select Photo </span>
		  <span class='fileinput-exists'> Change </span>
		  <input type='file' name='image' > </span>
		  <a href='javascript:;' class='btn btn-default fileinput-exists' data-dismiss='fileinput'> Remove </a>
	 </div>
	</div>
	<input type='hidden' value='<?php echo base_url('assets/dist/img/user.jpg');?>' name='drimage'>
  </div>
 </div> 


          <div class='form-group'>
          <label class='col-md-2 control-label' style='padding-top:5px;'>Name :</label>
          <div class='col-md-5' >  
          <input type='text' name='name' class='form-control ' required/>
          </div>                   
          </div>
		  

          <div class='form-group'>
                   <label class='col-md-2 control-label' style='padding-top:5px;'>Address :</label>
                    <div class='col-md-5''> <textarea name='address' rows='3' class='form-control' required></textarea>
          </div>                   
          </div>
      
      <div class='form-group'>
                     <label class='col-md-2 control-label' style='padding-top:5px;'>Email :</label>
                    <div class='col-md-5' >  
            <input type='email' name='email' class='form-control' style='padding-left:19px;' required/>
          </div>                   
          </div>


        <div class='form-group'>
          <label class='col-md-2 control-label'>Mobile : </label>
          <div class='col-md-5'>
      <input type='number' class='form-control' placeholder='Enter mobile no' name='mobile' id='mobile' required>
        <label id='mobmessage' style='color:red;'></label>    
            </div>
          </div>
          
          <div class='form-group'>
          <label class='col-md-2 control-label'>Landline :</label>
          <div class='col-md-5'>
      <input type='number' class='form-control' placeholder='Enter landline' name='landline' value='' required id='landline' required/>
      <label id='landmessage' style='color:red;'></label>
          </div>
          </div>
         
          <div class='form-group'>
          <label class='col-md-2  control-label'>Licence No :</label>
          <div class='col-md-3'>
			<input type='text' class='form-control' placeholder='Enter licence' name='licence' value='' required />
          </div>
          </div>
		  
		  <div class='form-group'>
          <label class='col-md-2  control-label'>Expiry Date :</label>
          <div class='col-md-3'>
			<input type='date' class='form-control' placeholder='Expiry Date' name='expdate' required />
          </div>
          </div>

          <div class='form-group'>
          <label class='col-md-2  control-label'>Badge No :</label>
          <div class='col-md-3'>
				<input type='text' class='form-control' placeholder='Enter Bagde No' name='badge' value='' required/>
          </div>
          </div>
		  
		   <div class='form-group'>
          <label class='col-md-2  control-label'>Driver Salary :</label>
          <div class='col-md-3'>
				<input type='text' class='form-control' placeholder='Enter Salary' name='drsalary'  id='drsal' value='' required/>
          </div>
          </div>
		  

 <label style='width:100%;height:1px; background-color:#e4e4e4;'></label>
                <div class='form-group'> 
                <div class='col-md-7'>
                <center><input type='submit' class='btn btn-primary' value='Save Driver Details'/></center>
                </div>
                </div>   
<br>        
           </form>
           </div>

</div>
</div>	
</div>   
</div>
</div>
</section>
</div>

<?php include("application/views/common/footer.php");?>


<script type="text/javascript">
$("#msg").hide();

if($.trim($("#msg").html())!="")
{
	swal("Saved",$("#msg").html(), "", "success");
	$("#msg").html("");
	$("#msgerr").html("");
}
  
$("#msgerr").hide();

if($.trim($("#msgerr").html())!="")
{
	swal("Try Again" ,$("#msgerr").html(), "error");
	$("#msgerr").html("");
	$("#msg").html("");
}

 function checkdata()
  {
    
    var mob=$('#mobile').val();
    var landline=$('#landline').val();
	var dsal=$('#drsal').val();
      
  
    if(mob.length<10 || mob.length>10)
    {
     $("#mobmessage").show();
      $('#mobmessage').html('Invalid mobile no,10 digits only.');
      return false;
    }

    else
    {
      $("#mobmessage").hide();
      $("#landmessage").hide();
      $('#mobmessage').html('');
      $('#landmessage').html('');
      return true;
    }
  }
 

</script> 

