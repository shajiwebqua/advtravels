<?php
 include('application/views/common/header.php');
 ?>
<!-- <link rel="stylesheet" href="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.css');?>" rel="stylesheet" type="text/css" />
 <script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
 <script src="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.js');?>"></script> -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/bootstrap/bootstrap.min.css');?>">-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/font-awesome/css/font-awesome.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/search_tabs/selectize.css');?>">
  
 
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.ttext
{
	background-color:#fff;
	font-weight:bold;
	color:Green;
}

.search-btn
{
	padding-top:3px;
	z-index:1000;
	right:17px;
	position:absolute;
}

.select_btn1 ,.select_btn2{
  
    position: absolute;
    top: 3px;
    z-index: 999;
   
}
.select_btn1{
	 right: 19px;
}
.select_btn2 {
    right: 19px;
}

.lblmap
{
	background-color:#e6e6e6;
	padding-top:12px;
	padding-bottom:12px;
	height:45px;
}

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id='bdy'> 
  
  <!-- Main content -->
    
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Edit Trip Sheet</b></h1>
    <ol class="breadcrumb">
   <li> <a href="<?php echo base_url('Trip/view_trip');?>" style="color:#4b88ed;font-size:15px;"> <i class="fa fa-backward" aria-hidden="true"></i>Back to Trip List</a></li>
    </ol> 
    </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
  <!-- Content Header end -->
  <section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg'style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id="msg"><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
  
    <!-- Small boxes (Stat box) -->
    <div class="row">
  <div class="col-md-12">

  <!--Add new user details -->
<div style="background-color:#fff;border-radius:10px;padding:15px; ">
    
   <!--   <div class="portlet-title">
      <div class="caption">
        <div style="color:blue;" >
            Trip Details                   
        </div> 
        </div>
       </div> -->
              
			  
 <div class="portlet-body form">
    <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Trip/update_trip')?>" enctype="multipart/form-data"  onsubmit='return checkdata();'>

		<?php
           foreach($result as $values){ 
        ?>        
	
        <div class="row">
        <div class="col-md-8" > 
			<input type='hidden' id='idtripid' name='tripid' value='<?=$values->trip_id;?>'>
		   <input type='hidden' id='idcustid' name='custid' value='<?=$values->customer_id;?>'>
			
		<div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Trip ID : </label>
			  <div class='col-md-4'>
			  <input type='text' class='form-control' name='trid' id='trid'  value='<?=$values->trip_id;?>' required>
			  </div>
         </div>
			
			
			<div class='form-group'  id='custna1'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Customer Name : </label>
			  <div class='col-md-8'>
			  <input type='text' class='form-control' name='custname' id='custname'  value='<?=$values->customer_name;?>' required>
			  </div>
         </div>
		 
		  <div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Customer Mobile : </label>
			  <div class='col-md-8'>
			  <input type='text' class='form-control' name='cmobile' id='idcmobile' value='<?=$values->customer_mobile;?>' required  >
			  </div>
          </div>
		 
          <div class='form-group'>
			  <label class='col-md-3  control-label' style='padding-top:5px;'>Nature of Jurney : </label>
			  <div class='col-md-8'>
			 <input type='text' class='form-control' name='nojourney' id='nojourney' value='<?=$values->trip_purpose;?>'required> 
			  </div>
          </div>
		  
		<div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Date Start : </label>
			  <div class='col-md-4'>
			  <input type='date' class='form-control' name='startdate' id='startdate' value='<?=$values->trip_startdate;?>' required>
			  </div>
			  <label class='col-md-1 control-label'  > End : </label>
          <div class='col-md-3'>
          <input type='date' class='form-control' name='enddate' id='enddate'  value='<?=$values->trip_enddate;?>'required>
          </div>
       </div>
	   
	    <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Time Start : </label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='starttime' id="starttime" value='<?=$values->trip_starttime;?>'>
          </div>
		  <label class='col-md-1 control-label' > End : </label>
			<div class='col-md-3'>
          <input type='text' class='form-control' name='endtime' id="endtime" value='<?=$values->trip_endtime;?>'>
          </div>
        </div>

	      
        <div class='form-group' style='padding-top:20px;'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Trip From :</label>
          <div class='col-md-8'>
          <input type='text' class='form-control' name='tripfrom' id='tripfrom' value='<?=$values->trip_from;?>' required>
          </div>
        </div>
      
        <div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:5px;'>Trip To : </label>
          <div class='col-md-8'>
          <input type='text' class='form-control' name='tripto' id="tripto"  value='<?=$values->trip_to;?>'required>
          </div>
        </div>
		<!--<div class='row' >
          <label class='col-md-3 control-label ' style='padding-top:5px;font-weight:600;'> </label>
		  <label class='ol-md-6' style='margin-top:0px;padding-left:15px;font-size:12px; color:blue'> Enter visiting places bellow separate with COMMA (<font style='color:red'>Eg : Kozhikode,Palakad,Ooty...etc</font>)</label>
        </div> -->
				
		<div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:5px;font-weight:600;'>Visiting Places : </label>
          <div class='col-md-8'>
          <textarea rows='2' class='form-control' name='vplace' id="vplace"  ><?=$values->trip_visitingplace;?>'</textarea>
          </div>
		<!--  <div class='col-md-2'>
          <button type='button' class='btn btn-warning' name='btndistance' id="btndistance" style='margin-top:15px;'>Distance</button> 
          </div> -->
        </div>
		
		<div class='form-group' style='padding:15px; text-align:right; background-color:#eff0f2;'>
		<div class='row' style='margin-bottom:5px;'>	
			
          <label class='col-md-3 control-label ' style='padding-top:5px;'></label>
          <div class='col-md-8'>
           <a href="" data-target="#myModal_Atta" data-toggle='modal' ><button id='attavehiclesearch' class='btn btn-default btn-xs'>Attached Vehicle</button></a>
		  <a id='addattached' data-target="#myModaladd" data-toggle='modal' ><button class='btn btn-primary btn-xs' name='addattached'>Add</button></a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
		  
		  <a id='vehiclesearch' data-target="#myModals" data-toggle='modal' ><button  class='btn btn-default btn-xs'>Our Vehicle</button></a>
					<!--	<button class='btn btn-primary btn-xs' name='addvehicle' id="addvehicle">Add</button></a> -->
		  </div>
            </div>
		<div class='row'> 
          <label class='col-md-3 control-label ' >Vehicle : </label>
          <div class='col-md-8' id="regs1">
		   <div class='search-btn'> 
			<!--<a href="" data-target="#myModals" data-toggle='modal' ><button id='vehiclesearch' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button></a>-->
		  </div>
		  <input type='text' class='form-control' name='vehicle' id='vregno' value='<?=$values->trip_vehicle_regno;?>' required>
		  <input type='hidden' class='form-control' name='vehicleid' id='vehicleid'  value='<?=$values->trip_vehicle_id;?>'>
          </div>
		  </div>
 
			<div class='row' style='margin-top:5px;'> 
			<label class='col-md-3 control-label ' >Driver Name : </label>
			<div class='col-md-8'>
			 <div class='search-btn'> 
			   <a id='driversearch' data-target="" data-toggle='modal' ><button  class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button></a>
			   </div>
			<input type='text' class='form-control' name='driver' id='driver' value="<?=$values->trip_drivername;?>" required>
	 	<input type='hidden' class='form-control' name='driverid' id='driverid' value='<?=$values->trip_driver_id;?>' value=''> 
			</div>
        </div>
		</div>
		
		
		<div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:5px;'>Additional Information : </label>
          <div class='col-md-8'>
          <textarea rows='3' class='form-control' name='addiinfo'>Nil</textarea>
          </div>
        </div>

		<div class='row'>
			 <label style="background-color:#cecece;width:100%;height:1px;"></label>
		</div>
		
		
		<div class='row'>
		<div class='col-md-3'>
		</div>
			<div class='col-md-8'>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Minimum Kilometers : </label>
				<div class='col-md-5'>
				 <input type='number' class='form-control' name='minckm' id='idminckm' value='<?=$values->trip_minchargekm;?>'>
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Minimum Kilometer Charge : </label>
				<div class='col-md-5'>
				 <input type='number' class='form-control' name='mincharge' id='mincharge' value='<?=$values->trip_mincharge;?>'>
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Trip Starting Kilometer (Vehicle) : </label>
				<div class='col-md-5'>
				<input type='number' class='form-control' name='startkm' id='startkm' value='<?=$values->trip_startkm;?>'>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Trip Ending Kilometer (Vehicle) : </label>
				<div class='col-md-5'>
				<input type='number' class='form-control' name='endkm' id='endkm' value='<?=$values->trip_endkm;?>'>
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Running KM (Up&Down) : </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='totalkm' id="totalkm" value='<?=$values->trip_endkm-$values->trip_startkm;?>' >
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'> </label>
				<div class='col-md-5'>
				 <input type='button' class='btn btn-primary' name='btnSet' id='btnSet' value='Get Values'>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Minimum Days : </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='mindays' id='mindays'  value='<?=$values->trip_mindays;?>' required>
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Minimum Days Total : </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' style='font-weight:bold' name='mintotal' id='mintotal' value='<?=$values->trip_mintotal;?>' readonly>
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Additional Kilometers : </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='addikm' id='addikm'  value='<?=$values->trip_addikm;?>' required>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Additional Kilometers Charge: </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='addikmcharge' id='addikmcharge' value='<?=$values->trip_addikmcharge;?>' required>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Additional KM Total Charge : </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' style='font-weight:bold' name='additotal' id='additotal' value='<?=$values->trip_additotal;?>' readonly>
				</div>
			</div>
		
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Toll and Parking Charges: </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='tollparking' id='tollparking'  value='<?=$values->trip_tollparking;?>' required>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Interstate Permit Charge: </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='ispermit' id='ispermit' value='<?=$values->trip_interstate;?>' required>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Other Charges: </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='ocharge' id='ocharge' value='0'>
				</div>
			</div>
						
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Other Description: </label>
				<div class='col-md-5'>
				<textarea rows=2 class='form-control' name='otherdesc' id='otherdesc'></textarea>
				</div>
			</div>
					
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'> </label>
				<div class='col-md-5'>
				 <input type='button' class='btn btn-primary' name='btncalc' id='idbtncalc' value='Calculate'>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Trip Cost: </label>
				<div class='col-md-5'>
				 <input type='number' class='form-control ttext' style="background-color:#fff;" name='tripcost' id='tripcost' value="<?=$values->trip_gtotal;?>" readonly>
				</div>
			</div>
			<?php
		   }
		   ?>
			
		<div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:5px;'></label>
          <label class="col-md-4 control-label" style="font-size:25px; text-align:right;color:blue;">RS : </label> 
		  <label id="lblgtotal" class="col-md-3 control-label" style="font-size:25px; text-align:left;color:blue;"><?=round($values->trip_gtotal,2);?></label>
        </div>
			
		</div>

		</div>
		</div>
 
		  
 <!-- second column --------------------------->
		  
	<!---	  <div class='col-md-4' style='padding-left:20px;' >
		  <div class="row">
		  <label class='col-md-12 lblmap' ><b>Places and Distance</b></label>
		  </div>
		  <div style='width:100%;' id='distMap' >
			  <div class='row' style='margin-top:5px;'>
			  <div class='col-md-12'>
			  <select id='sou0' class='form-control'>
			  
			  </select>
			  </div>
			  </div>
			  <div class='row' style='margin-top:5px;'>
			  <div class='col-md-12'>
			  <select id='des0' class='form-control'>
			  
			  </select>
			  </div>
			  </div>
		  </div>
		  
		  <div class='row' style='margin-top:5px;'>
		   <div class='col-md-6' >
		   <div class='row' id='dvDistance1' style='padding-left:15px;'>
		   Distance :
		   </div>
		   <div class='row' id='dvDistance2'  style='padding-left:15px;'>
		   Duration :
		   </div>
		   </div>
		   
		   <div class='col-md-6' style='text-align:right;margin-top:10px;'>
			  <button id='viewMap' type='button'>View Distance</button>
			</div>
		 </div>
		 <div class='row' style='margin-top:15px;padding-left:15px;'>
          <div class='col-md-12' id='dvViewMap' style='background-color:#f7f7f7;height:400px;max-width:330px;'>
          
          </div>
		  </div>
		 		  
        </div> -->
	  </div>
 
 

<label style="background-color:#cecece;width:100%;height:1px;"></label>

<div class='form-group'>
<label class='col-md-4 control-label '></label>
<div class='col-md-3'>
	<button type="submit" class="btn btn-success" style="padding:10px 50px 10px 50px;" id="idsavebtn"> <b>Update Trip Details</b></button>
</div>
</div>

</form>
</div>

 <label style="background-color:#cecece;width:100%;height:1px;"></label>

 
 </div>
 
   <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-body" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit</h4>
				</div>
			</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
   </div>  
   
 
	<div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit</h4>
				</div>
			</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
    </div>  
	
	<div class="modal fade draggable-modal" id="myModaladd" tabindex="-1" role="basic" aria- hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" style='height:500px;'>
				<div class="modal-header">
				<button type='button' class='close' data-dismiss='modal'>&times;</button>
				<h4 class='modal-title' > <font color='blue'>Attached Vehicle Details</font></h4>
					
				</div>
				
				<div class='col-md-12'>
  			<!--<form  class='form-horizontal' role='form' method='post' action=".base_url('Attachvehicle/modal_addvehicle')." enctype='multipart/form-data' onsubmit='return checkdata();' >  -->
                    <!-- text input -->
			<div class='row'>
			<div class='col-md-11'>
	   
            <div class='form-group'>
            <div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label'>Vehicle Type : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<select name='mvtype' class='form-control' id='mvtype' required>
				<option value=''>---------</option>
				
				<?php
				$qry=$this->db->select('*')->from('vehicle_types')->get()->result();
				foreach($qry as $r1)
				{
					echo "<option value='".$r1->veh_type_id."'>".$r1->veh_type_name."</option>";
					
				}
				?>
				</select>	</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Register No : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='mregno' class='form-control'  id='mregno' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >No os seats : </label>
				<div class='col-md-2' style='padding-left:19px;'>
				<input type='text' name='mnseats' class='form-control' id='mnseats'  required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' >R/C Owner Name : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='mrcname' class='form-control'  id='mrcname' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Mobile : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='number' name='mrcmobile' class='form-control' id='mrcmobile' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' >Driver Name : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='mdrname' class='form-control' id='mdrname' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Mobile No : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='number' name='mdrmobile' class='form-control'  id='mdrmobile' required />
				</div>           
			</div>
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' style='color:blue'>Kilometers : </label>	
			</div>	
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Starting : </label>
				<div class='col-md-3' style='padding-left:19px;'>
				<input type='text' name='mstartkm' class='form-control'  id='mstartkm' required />
				</div>           
				<label class='col-md-2 control-label' >Ending : </label>
				<div class='col-md-3' style='padding-left:19px;'>
				<input type='text' name='mendkm' class='form-control'  id='mendkm' required />
				</div>           
			</div>
        </div>
      
 </div>
 </div>
     <label style='height:1px;width:100%;background-color:#cecece;'></label>
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-8'>
                <button class='btn btn-primary' id='idaddvehicle' >Add Vehicle</button>
                </div>
                </div>                                
          <!-- </form> -->
           </div>
				
			</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
    </div>  
	
	
	<div class="modal fade" id="myModal_Atta" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Attached Vehicles List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class=" max-width">
                <table id="TblAttaVehicle" class="table table-striped table-hover table-bordered" cellspacing="0">
				<thead>
            <tr>
                <th>Select</th>
                <th>Registration No</th>
                <th>Vehicle Type</th>
				<th>Driver Name</th>
				<th>Driver Mobile</th>
                <th>No Of Seats</th>
            </tr>
        </thead>
    </table>
    </div>
   </div>

    </div>
</div>
</div>
</div>

  
<div class="modal fade" id="myModals" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Vehicles List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class=" max-width">
                <table id="TblVehicle" class="table table-striped table-hover table-bordered" cellspacing="0">
				<thead>
            <tr>
                <th>Select</th>
                <th>Registration No</th>
                <th>Vehicle Type</th>
                <th>No Of Seats</th>
				<th>Status</th>
            </tr>
        </thead>
    </table>
    </div>
   </div>


    </div>
</div>
</div>
</div>

<!--- Drivers List --> 


<div class="modal fade" id="myModald" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Drivers List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class="col-md-12">
            <table id="TblDriver" class="table table-striped table-hover table-bordered" cellspacing="0">
            <thead>
            <tr>
                <th>Select</th>
				<th>ID</th>
                <th>Driver Name</th>
                <th>Mobile</th>
				<th>Status</th>
            </tr>
        </thead>
      
		<!--	  <tbody>
        <?php  foreach($results2  as $r) 
               {   ?>

              <?php $edit="<a href='#myModald' id='$r->driver_id'  class='select open-AddBookDialog2'>Select</a>";?>
            <tr>
                <td><?=$edit;?></td>
                <td><?=$r->driver_name;?></td>
                <td><?=$r->driver_mobile;?></td>
                
                <?php if($r->driver_status==1)
                {
                 $st="<font color='green'>Available</font>";
                }
                else
                {
                 $st="<font color='red'>Not Available</font>";
                } ?>
                <td><?=$st;?></td>
                
            </tr>
            <?php } ?>
           
        </tbody> -->
		
    </table>
        </div>
      </div>
 	 </div>
 	</div>
   </div>
    
  </div>
  <!------------------------------------->
 
  
  <div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria- hidden="true">
	    <div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Check Kilometer</h4>
				</div>
			</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
   </div>  
   
  
 </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 

</section>
</div>
<!-- /.content-wrapper --> 



<!-- first Model for add agents -------------------------------------------------------->

 <div class="modal fade draggable-modal" id="myModalA" tabindex="-1" role="basic" aria- hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
				<h4 class="modal-title">Add New Agent Details</h4>
			</div>
			
			<!-- content -------------------------------------------->
			
			<div style="background-color:#fff;padding:10px; ">
   
				<!--<form onsubmit="return checkdata();" class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/add_agent')?>"> -->
                  
						<div class="form-group">
							 <div class="row" style="margin-top:0px;">
									<label class="col-md-3 control-label" style="padding-top:0px">Agent Code</label>
								   <div class="col-md-4">
								   <input type="text" class="form-control"  name="agent_code" id='acode' required>
								 </div>
							 </div>
                        </div>
  
						<div class="form-group" >
							 <div class="row" style="margin-top:0px;">
								<label class="col-md-3 control-label" style="padding-top:0px">Agent Name</label>
								 <div class="col-md-7">
								   <input type="text" class="form-control"  name="agent_name"  id='aname' required>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
								<label class="col-md-3 control-label" style="padding-top:0px">Address</label>
								 <div class="col-md-7">
								   <textarea rows=2 cols=30 class="form-control"  name="agent_address"  id='aaddress' required></textarea>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
							 	<label class="col-md-3 control-label" style="padding-top:0px">Mobile</label>
								 <div class="col-md-7">
								   <input type="number" pattern="[0-9]*" class="form-control"  name="agent_mobile" id="amobile" required>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
									<label class="col-md-3 control-label" style="padding-top:0px">Email</label>
							 	 <div class="col-md-7">
								   <input type="email" class="form-control"  name="agent_email" id='aemail' required>
								 </div>
							 </div>
                        </div>
                                 <hr>

                    <div class="form-group">
                     <div class="row"><center>
                            <button class="btn btn-primary" id='submitagent'>Submit</button>
								   &nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
								   </center>
                           </div>
                       </div>
					   
                       <!-- </form> -->
                       </div>

				<!------------------------------------------------------------>
		</div>
		</div>
	<!-- /.modal-dialog -->
	</div>
</div>

<!--- MODEL 2 DISPLAY AGENT LIST -------------------->

<div class="modal fade" id="myModalSA" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Agents List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class="col-md-12">
            <table id="tblagents" class="table table-striped table-hover table-bordered" cellspacing="0">
            <thead>
            <tr>
                <th>Select</th>
				<th>ID</th>
                <th>Agent Name</th>
                <th>Mobile</th>
            </tr>
        </thead>
    </table>
        </div>
      </div>
 	 </div>
 	</div>
   </div>
  </div>

<!--- AGENTS END -------------------->

<?php include('application/views/common/footer.php');?>
<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>  -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBx1BJxGlTRJyF2W_AdaCfRf-U0FLRxif4&libraries=places">
</script>
<script type="text/javascript">

//-- driver list
//sweet alert box ----------------------
//$("#custna1").hide();
//$("#custname").hide();
/$("#custmobile").hide();
//$("#viewMap").prop('disabled',true);
//$("#agents").hide();

$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------

//search customer

$('#id2').selectize({
     create: false
     , sortField: {
     //field: 'text',
     direction: 'asc'
   }
   , dropdownParent: 'body'
   });

 $(".clientname").hide();
 $(".cpurpose").hide();
 
 
 $("#category").change(function()
 {
	 var op=$("#category").val();
		
		if (op==1)
		{
		 $(".clientname").val("");
		 $(".cpurpose").val(""); 
		 $(".clientname").hide();
		 $(".cpurpose").hide(); 
		 $("#custna").show();
		 $("#custna1").hide();
		}
		else if(op==2)
		{	
		 $(".clientname").show();
		 $(".cpurpose").show(); 
		 $("#custna").show();
		 //$("#custna1").hide();
		}
		else if(op==3)
		{	
	
		 $(".clientname").val("");
		 $(".cpurpose").val(""); 
		 $(".clientname").hide();
		 $(".cpurpose").hide(); 
		// $("#custna").hide();
		$("#custna1").hide();
		 $("#agents").show();
		 
		}
 });

 
 $("#enquiry").change(function()
 {
	 var op=$("#enquiry").val();
	 jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/GetEnquiry",
        dataType: 'html',
        data: {opt:op},
        success: function(res) {
			//alert(res);
			var obj = jQuery.parseJSON(res);
			
			//alert(obj[0].enquiry_id);
			
			$("#custname").show();
			$("#idd2").hide();
			$(".select_btn1").hide();
			
					
			$("#cust_name").val(obj[0].party_name);
			$("#idcmobile").val(obj[0].mobile_no);
			
			$("#nojourney").val(obj[0].nature_of_journey);
			$("#startdate").val(obj[0].starting_date);
			$("#enddate").val(obj[0].returning_date);
			$("#starttime").val(obj[0].starting_time);
			$("#endtime").val(obj[0].returning_time);
			$("#tripfrom").val(obj[0].starting_place);
			$("#tripto").val(obj[0].to_place);
        }
		});
 });
 
 
/*$('#btncheck').click(function () {
	var Result=$("#myModal3 .modal-body");
	var km=$("#startkm").val();
	var vid=$("#vehicleid").val();
	
	if (vid=='')
	{
		swal("Please select vehicle...");
		//alert("Please select vehicle...");	
	}
	else if(km=='')
	{
		//alert("Please type stating kilometer...");	
		swal("Please type starting kilometer.");
	}
	else
	{
		$("#btncheck").attr("data-target","#myModal3");
	    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/check_kilometer",
        dataType: 'html',
        data: {skm:km,vid:vid},
        success: function(res) {
        Result.html(res);
        }
		});
	}
});*/


//-----------------------------------------

$("#id2").change(function()
{
	    var id =  $("#id2").val();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_cust_mobile",
        dataType: 'html',
        data: {cid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#idcmobile").val(obj.mobile);
		       }
            });
});

//drop down script .................
//..................................

$('#TblDriver').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/driver_ajax/0",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"7%","targets":1},
		],
			   
        "columns": [
            { "data": "select"},
			{ "data": "drid"},
		    { "data": "name"},
            { "data": "mobile"},
			{ "data": "status"},
       ]
  } );

  

$("#driversearch").click(function()
{
	var sdate=$("#startdate").val();
	var edate=$("#enddate").val();
  //alert(sdate);
	
	if(sdate=="" || edate=="")
	{
		alert("please select trip starting date and Ending date.")
	}
	else
	{
		$("#driversearch").attr("data-target","#myModald");
		$('#TblDriver').dataTable({
			 destroy: true,
			"processing": true,
			"ajax": {
					url :"<?php echo base_url(); ?>" + "Trip/driver_ajax/"+sdate+"/"+edate,// json datasource
				   },
			"columnDefs":[
			{"width":"6%","targets":0},
			{"width":"7%","targets":1},
			],
				   
			"columns": [
				{ "data": "select"},
				{ "data": "drid"},
				{ "data": "name"},
				{ "data": "mobile" },
				{ "data": "status"},
		   ]
		} );
	}
});
  //-- vehicle List

  
  
  $('#TblVehicle').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/vehicle_ajax",
				
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		],
			   
        "columns": [
            { "data": "select"},
		    { "data": "regno"},
            { "data": "vtype" },
			{ "data": "noofseats" },
			{ "data": "status" },
       ]
  } );
  
  //attached vehile list
    
  getattached_vehicle();  // call vehicle details
  
  function getattached_vehicle()
  {
   $('#TblAttaVehicle').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/atta_vehicle_ajax",
				
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		],
			   
        "columns": [
            { "data": "select"},
		    { "data": "regno"},
            { "data": "vtype" },
			{ "data": "drname" },
			{ "data": "drmobile" },
			{ "data": "noofseats" },
       ]
  } );
  }

  //Customer list

 /* $('#TblCustomer').dataTable({
         destroy: true,
        "processing": true,
        "ajax":{
                url :"<?php echo base_url(); ?>" + "Trip/customer_ajax",// json datasource
				
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		],
			   
        "columns": [
            { "data": "select"},
		    { "data": "cname"},
            { "data": "cmobile"},
       ]
  } ); */
     
  // selection ---- vechicle and driver -------------------

  $('#TblDriver tbody').on('click', '.drselect', function () {
  
	    var id =  $(this).attr('id');
         jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_driverDT",
        dataType: 'html',
        data: {did: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#driver").val(obj.name);
				$("#driverid").val(obj.driverid);
		       }
            });
        }); 
  
  
 $('#TblVehicle tbody').on('click', '.vselect', function () {
         var id =  $(this).attr('id');
         jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_vehicleDT",
        dataType: 'html',
        data: {vid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#vregno").val(obj.name);
				$("#vehicleid").val(obj.vehicleid);
		       }
            });
        }); 

		
$('#TblAttaVehicle tbody').on('click', '.atta_vselect', function () {
        var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_atta_vehicleDT",
        dataType: 'html',
        data: {avid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#vregno").val(obj.regno);
				$("#vehicleid").val(obj.avehicleid);
				$("#driver").val(obj.drname);
		       }
            });
    
        });
 
 /*$('#TblCustomer tbody').on('click', '.cselect', function () {
     //   $(this).parent().parent().toggleClass('selected');
        var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_customerDT",
        dataType: 'html',
        data: {cid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#custname").val(obj.name);
				$("#idcmobile").val(obj.mobile);
        $("#idcid").val(obj.id);
		       }
            });
    
        }); */
 
  $('#TblCustomer tbody').on('click', '#custadd', function () 
  {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         // var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Customer/add_customer",
        dataType: 'html',
        // data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
        }); 
 //---------------------------------------------------
  
 
$(document).ready(function () {
	$("#idsavebtn").prop('disabled', true);
	$("#idbtnget").prop('disabled', true);
	//$("#idbtncalc").prop('disabled', true);
        setTimeout(function(){ $("#msg").html(""); }, 3000);
    }); 
	
</script>

<script>
$('#example1 tbody').on('click', '.select', function () {
 var Result=$("#myModals .modal-body");
 var id =  $(this).attr('id');
 $('#regno').val( id );
 $('#myModals').modal('hide')
});
</script>


<script>
$('#example2 tbody').on('click', '.select', function () {
 var Result=$("#myModald .modal-body");
 var id =  $(this).attr('id');
 $('#driver').val(id);
 $('#myModald').modal('hide')
});

/* -------- button desable/enanble ------------------*/

$('#idminckm').focus(function () {
	$("#idbtnget").prop('disabled', false);
});

/* -------- button desable/enanble ------------------*/


 function checkDate() {
            var stDate =new Date($("#startdate").val()) //for javascript
            var enDate =new Date($("#enddate").val()); // For JQuery
		
			var g=stDate-enDate;
            if (g>0) {
                return false;
            }
            else {
               return true;
            }
        }




/* get Button  ----------------------*/

$('#btnSet').click(function () {
	
	var skm=$.trim(parseInt($('#startkm').val()));
	var ekm=(parseInt($.trim(skm))+ parseInt($.trim($('#totalkm').val())));

	$("#endkm").val(ekm);
	
	var sdate=$('#startdate').val();
	var edate=$('#enddate').val();
	
	var mk1=$.trim($('#idminckm').val());
	
	if(checkDate()==false)
	{
		alert("Please select end date correctly.");
	}
	else if(sdate=="" || edate=="")
	{
		swal("Please choose Trip Start and End Date.");
	}
	
	else if(isNaN(skm) || isNaN(ekm) || skm=="0" || ekm=="0" || (parseInt(skm) > parseInt(ekm)))
	{
		alert("Please enter Starting KM and Ending KM.");
	}
	else if(mk1.length==0)
	{
		alert("please type minimum charge KM..!");
	}
	
	else
	{
	var rkm=(parseInt(ekm)-parseInt(skm));	
	
	var edate1=new Date(edate);
	var sdate1=new Date(sdate);
	
	var days = (parseInt(edate1.getTime() - sdate1.getTime())) / (1000 * 60 * 60 * 24);
		
	var adkm=parseInt(rkm)-(parseInt(days)*parseInt(mk1))
	
	//var days = (edate1.getTime() - sdate1.getTime())
	$("#runningkm").val(rkm);
	$("#mindays").val(days);
	$("#addikm").val(adkm);
	var mtot=(parseInt($("#mindays").val())*parseInt($("#mincharge").val()))
	$("#mintotal").val(mtot);
	
	$("#idbtncalc").prop('disabled', false);
	$("#idsavebtn").prop('disabled', false);
	$("#addikmcharge").focus();
	}

});
/*----------------------------------------*/

/* claulate totals ----------------------*/
$('#idbtncalc').click(function () {

var mdays=$('#mindays').val();	
var mcharge=$('#mincharge').val();	

var akm=$('#addikm').val();	
var akmc=$('#addikmcharge').val();
	
var tpc=$('#tollparking').val();	
var isp=$('#ispermit').val();	
var mtot1=$('#mintotal').val();

//var mtot=(parseFloat(mdays)* parseFloat(mcharge)).toFixed(2);
var adtot=(parseFloat(akm)* parseFloat(akmc)).toFixed(2);
var ttot=(parseFloat(mtot1)+parseFloat(adtot)).toFixed(2);
var gtot=(parseFloat(ttot)+parseFloat(tpc)+parseFloat(isp)).toFixed(2);

$("#additotal").val(adtot);
$("#tripcost").val(gtot);
$("#lblgtotal").html(gtot);

//$("#idsavebtn").prop('disabled', false);
});
/*----------------------------------------*/


/// Add  new Atached Vehicle details
 
  $('#idaddvehicle').click(function()
	{
		   alert('ok');
		   var vtyp=$('#mvtype').val();
		   var nseat=$('#mnseats').val();
		   var regn=$('#mregno').val();
		   var rcna=$('#mrcname').val();
		   var rcmob=$('#mrcmobile').val();
		   var drna=$('#mdrname').val();
		   var drmob=$('#mdrmobile').val();
		   var skm=$('#mstartkm').val();
		   var ekm=$('#mendkm').val();
		   
			jQuery.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>' + 'Attachvehicle/modal_addvehicle',
			dataType: 'html',
			data: {vtype:vtyp,nseats:nseat,regno:regn,rcname:rcna,rcmobile:rcmob,drname:drna,drmobile:drmob,startkm:skm,endkm:ekm},
			success: function(res) 
			{
				getattached_vehicle();
			}			   
	   });
  });
  
  /*----------------------------------- Agents ------------------------ */
  
  var table= $('#tblagents').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/agentlist_ajax",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"7%","targets":1},
		],
			   
        "columns": [
            { "data": "select"},
			{ "data": "agid"},
		    { "data": "agname"},
            { "data": "agmobile"},
       ]
  });
  
  
  
  
  $("#acode").blur(function(){
	if($("#acode").val()==""){$("#acode").css('border','1px solid red');}
	else {$("#acode").css('border','1px solid #c6c6c6');}
	
});

$("#aname").blur(function(){
	if($("#aname").val()==""){$("#aname").css('border','1px solid red');}
	else {$("#aname").css('border','1px solid #c6c6c6');}
});

$("#aaddress").blur(function(){
	if($("#aaddress").val()==""){$("#aaddress").css('border','1px solid red');}
	else {$("#aaddress").css('border','1px solid #c6c6c6');}
});
	
$("#amobile").blur(function(){
	if($("#amobile").val()==""){$("#amobile").css('border','1px solid red');}
	else {$("#amobile").css('border','1px solid #c6c6c6');}
});

$("#aemail").blur(function(){
	if($("#aemail").val()==""){$("#aemail").css('border','1px solid red');}
	else {$("#aemail").css('border','1px solid #c6c6c6');}
});


function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test($email );
}


$("#submitagent").click(function()
		{

		var acod=$("#acode").val();
		var ana=$("#aname").val();
		var aadd=$("#aaddress").val();
		var amob=$("#amobile").val();
		var aema=$("#aemail").val();

		var res1=true;
		var res2=true;
		var res3=true;
		var res4=true;
		var res5=true;
		var res6=true;
		

if(acod==""){res1=false;$("#acode").css('border','1px solid red'); }else{res1=true;$("#acode").css('border','1px solid #c6c6c6');}
if(ana==""){res2=false;$("#aname").css('border','1px solid red');}else{res2=true;$("#aname").css('border','1px solid #c6c6c6');}
if(aadd==""){res3=false;$("#aaddress").css('border','1px solid red');}else{res3=true;$("#aaddress").css('border','1px solid #c6c6c6');}
if(amob==""){res4=false;$("#amobile").css('border','1px solid red');}else{res4=true;$("#amobile").css('border','1px solid #c6c6c6');}
if(aema==""){res5=false;$("#aemail").css('border','1px solid red');}else{res5=true;$("#aemail").css('border','1px solid #c6c6c6');}
	
		if(res1!=false && res2!=false && res3!=false && res4!=false && res5!=false )
			{
	
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "General/add_agent1",
				dataType: 'html',
				data: {acode:acod,aname:ana,aaddress:aadd,amobile:amob,aemail:aema},
				success: function(data) 
				 {
					 
				  if(parseInt(data)>0)
				  {
					  $("#agentid").val(data);
					  $("#agent").val(ana);
					  swal("Saved","Agent details saved..","success");
					  
				  }
				  else
				  {
					  swal("Cancelled","Agent details missing, Try again..","error");
				  }
				}
				});
			}
			else
			{
				swal("Missing","Agent details missing, Try again..","error");
			}
		});
	
	
	var table= $('#tblagents').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/agentlist_ajax",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"7%","targets":1},
		],
			   
        "columns": [
            { "data": "select"},
			{ "data": "agid"},
		    { "data": "agname"},
            { "data": "agmobile"},
       ]
  });
  
  
  $('#tblagents tbody').on('click', '.agselect', function ()
  {
	    var id =  $(this).attr('id');
		
		jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/get_agentsDT",
        dataType: 'html',
        data: {aid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#agent").val(obj.agname);
				$("#agentid").val(obj.agid);
		       }
            });
   }); 
   
  
  
  
  
</script>



 