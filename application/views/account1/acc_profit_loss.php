<?php
include('application/views/common/header.php');
?>
<style>
input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
	
</style>

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#00adee;margin-bottom:10px;font-weight:bold;'>Profit & Loss A/C 
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
        </section>
	
        <!-- Main content -->
    <section class="content">
	
	<div class='row' style="padding:5px 15px 5px 15px;">

	<div class="box " style="border:1px solid #f5f3f3;">
	    <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp; Profit & Loss A/C</h3>
		  <label id='mes'><?php echo $this->session->flashdata('message');?></label>
        </div>
		
        <div class="box-body">
		
		<div class='row' style='margin:0px 5px 0px 5px;background-color:#e4e4e4;padding:5px;'>
			
			<form role="form" method="POST" action="<?php echo base_url('Account/profit_loss/1')?>" enctype="multipart/form-data">
             			
			<div class='col-md-2' style='padding-top:5px;'>	<label class='control-label'>Select Date [from-to] : </label>	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			<div class='col-md-2' style='padding-top:2px;'><input type='submit' name='btndetails' class='btn btn-primary' value='Get Details'> </div>
			</form>
			
			<div class='col-md-4' style='text-align:right; padding-right:15px;'>
			<form role="form" method="POST" action="<?php echo base_url('Account/profit_loss')?>" enctype="multipart/form-data">
			<input type='submit' name='btndetails' class='btn btn-warning' value='Back'>
			</form>
			</div>
						
			</div>

			<div class='row' style='margin-left:10px;margin-top:15px;'>
			<label style='padding-bottom:3px; border-bottom:1px solid #e4e4e4;font-size:15px;'><b><?php echo $this->session->flashdata('ptitle');?></b></label>
			</div>
			
						
			<div class='row' style='padding:0px 10px;'>
			<div class='col-md-6'>
			 <table class="table table-striped table-hover table-bordered" id="example1" border=0 STYLE='font-size:14px;width:100%'>
			 
			 <thead>
                <tr>
				 <th >Particulars</th>
                 <th width='15%'></th>
				 <th width='15%'></th>
                </tr>
                </thead>
				<tbody>
				<?php
					$pldtot=0;
					$plitot=0;
					$diff1=0;
					$diff2=0;
					if(isset($plexpense))
					{
					foreach($plexpense as $r) // to get total expenses
					{
						$pldtot+=$r->debit;
					}
					}
					
					if(isset($plincome))
					{
					foreach($plincome as $r1) // to get total income
					{
						$plitot+=$r1->credit;
					}
					}
					
					
					?>
					 <tr >
						 <td ><b>DIRECT EXPENSES</b></td>
						 <td style='text-align:right;'></td> 
						 <td style='text-align:right;padding-left:50px;'><b><?=number_format($pldtot,2,".","");?></b></td> 
						</tr>
				<?php		
					if(isset($plexpense))
					{	
					foreach($plexpense as $r) // to print taxes and duties
					{
				    ?>
						<tr >
						 <td style='padding-left:25px;'><?=$r->acc_trans_description;?></td>
						 <td style='text-align:right;padding-left:50px;'><?=number_format($r->debit,2,".","");?></td> 
						 <td style='text-align:right;'></td> 
						</tr>
					<?php
					}
					if($pldtot>0)
					{
					?>
					
					   <tr >
						 <td style='padding-left:25px;'></td>
						 <td style='text-align:right;padding-left:50px; text-decoration:overline  underline;'><?=number_format($pldtot,2,".","");?></td> 
						 <td style='text-align:right;'></td> 
						</tr>
					<?php
					}
					}
		
					if($pldtot<$plitot)
					{
						$diff1=$plitot-$pldtot;
					
					?>
						<tr>
						 <td style='padding-left:25px;'>Net Pofit (p&L)</td>
						 <td style='text-align:right;padding-left:50px;'><?=number_format($diff1,2,".","");?></td> 
						 <td style='text-align:right;'></td> 
						</tr>
					<?php
					}
										
					?>	
									
				</tbody>
				</table>
				
				<!--<div class='row' style='margin:10px 0px;background-color:#e2e2e2;padding-top:3px;'>
				<div class='col-md-9' style='text-align:right;'>
				<label class='control-label' style='font-size:16px;font-weight:600;'> Total Payed Amount : </label>
				</div>
				<div class='col-md-3' style='text-align:right;'>
				<label class='control-label' style='font-size:18px;font-weight:600;'> &#8377;&nbsp; <?=number_format($gtotp,"2",".","");?></label>
				</div>
				</div> -->
				</div>
				
			<div class='col-md-6'>
			 <table class="table table-striped table-hover table-bordered" id="example2" border=0 STYLE='font-size:14px;width:100%'>
			 <thead>
                <tr>
				 <th>Particulars</th>
                 <th width='15%'></th>
				 <th width='15%'></th>
                </tr>
                </thead>
				<tbody>

					 <tr >
						 <td ><b>DIRECT INCOME</b></td>
						 <td style='text-align:right;'></td> 
						 <td style='text-align:right;padding-left:50px;'><b><?=number_format($plitot,2,".","");?></b></td> 
						</tr>
				<?php		
					if(isset($plincome))
					{	
					foreach($plincome as $r1) // to print taxes and duties
					{
				    ?>
						<tr >
						 <td style='padding-left:25px;'><?=$r1->acc_trans_description;?></td>
						 <td style='text-align:right;padding-left:50px;'><?=number_format($r1->credit,2,".","");?></td> 
						 <td style='text-align:right;'></td> 
						</tr>
					<?php
					}
					if($plitot>0)
					{
					?>
					<tr >
						 <td style='padding-left:25px;'></td>
						 <td style='text-align:right;padding-left:50px; text-decoration:overline  underline;'><?=number_format($plitot,2,".","");?></td> 
						 <td style='text-align:right;'></td> 
						</tr>
					<?php
					}
					}
					if($plitot<$pldtot)
					{
						$diff2=$pldtot-$plitot;
					
					?>
						<tr>
						 <td style='padding-left:25px;'>Net Loss (p&L)</td>
						 <td style='text-align:right;'></td> 
						 <td style='text-align:right;padding-left:50px;'><?=number_format($diff2,2,".","");?></td> 
						 </tr>
					<?php
					}
					?>	
				</tbody>
			 
				</table>
				
				<!--<div class='row' style='margin:10px 0px;background-color:#e2e2e2;padding-top:3px;'>
				<div class='col-md-9' style='text-align:right;'>
				<label class='control-label' style='font-size:16px;font-weight:600;'> Total Payed Amount : </label>
				</div>
				<div class='col-md-3' style='text-align:right;'>
				<label class='control-label' style='font-size:18px;font-weight:600;'> &#8377;&nbsp; <?=number_format($gtotp,"2",".","");?></label>
				</div>
				</div> -->
								
				</div>

			</div>
			
			<div class='row' style='padding:0px 10px;'>
			
			<div class='col-md-6' style='background-color:#eeeeee;border-right:5px solid #fff;'>
				<table width='100%'><tr>
				<td style='font-size:16px;padding-left:15px;'><b>Total</b></td>
				<td style='text-align:right;padding-right:10px;font-size:16px;'><b><?=number_format($pldtot+$diff1,2,".","");?></b></td>
				</tr></table>
			</div>
			
			<div class='col-md-6' style='background-color:#eeeeee;border-left:5px solid #fff;'>
				<table width='100%'><tr>
				<td style='font-size:16px;padding-left:15px;'><b>Total</b></td>
				<td style='text-align:right;padding-right:10px;font-size:16px;'><b><?=number_format($plitot+$diff2,2,".","");?></b></td>
				</tr></table>
			</div>
			</div>
						
			</div> <!-- second tab end --->
			</div> <!-- tab content end -->
        </div>
	</div>

</section>

<?php
  include('application/views/common/footer.php');
  ?>
</body>
</html>

<script type="text/javascript">
  
   $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
   
   $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
   
   
  /*
  $("#example1").dataTable({
	  "ordering":false,
  });
  
    $("#example2").dataTable({
	  "ordering":false,
  });*/
  
  $("#mes").hide();
    
  var msg=$("#mes").html().split("#");
  
  if($("#mes").html()!="")
  {
	  var msg=$("#mes").html().split("#");
	if(msg[0]=='1')
	  swal("",msg[1],"success"); //Saved.!
	else if(msg[0]=='2')
	  swal("",msg[1],"success");  //Updated.!
    else if(msg[0]=='3')
	  swal("",msg[1],"success"); //Removed.!
	else if(msg[0]=='4')
	  swal("",msg[1],"error"); //Try Again.
     $("#mes").html("");
  }
  
  $(document).on("click", "#del_conf", function () {
       return confirm('Are you sure you want to delete this entry?');
    });
   </script>
</body>
</html>

  
  