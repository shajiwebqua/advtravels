<?php  

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ledger extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		if (!isset($user))
		{ 
			//redirect('/');
		} 
		else
		{ 
			return true;
		}
		$this->load->helper(array('form', 'url','html'));
	}

	
	public function View()
	{
		$this->load->model("Model_accounts");
		$data['result']=$this->Model_accounts->ledger_list();
        $this->load->view("account/view_ledgers",$data); 
	}
	
	public function Add()
	{
		$this->load->model("Model_accounts");
		$data['ledgers']=$this->Model_accounts->ledger_list();
        $this->load->view("account/acc_ledgers",$data); 
	}

public function save_ledger()
	{
	$this->load->model("Model_accounts");
		
	$lname=$this->input->post('lname');
	$lunder=$this->input->post('lunder');
		
	$data1=array(
			'ledg_name'=>strtoupper($lname),
			'ledg_group'=>$lunder,
			);
	$this->Model_accounts->insert("ac_ledgers",$data1);
	$this->session->set_flashdata('message', '1#Ledger sccessfully added.'); 
	redirect('Ledger/Add');
	}	
	
	
public function edit_ledger()
	{
	   $id=$this->input->post('lid');
	   $op=$this->input->post('op');
       $query = $this->db->select('*')->from('ac_ledgers')->where('ledg_id',$id)->get();
       $row = $query->row();
	
	
	echo '<div class="modal-header" style="background-color:#cacaca;margin:0px ;padding:5px 10px 5px 20px;">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding:5px;background-color:#red;">
          <span aria-hidden="true" >&times;</span>
        </button>
		<h4 class="modal-title" id="exampleModalLabel" >Edit Ledger</h4>
      </div>
	  
	  <form class="form-horizontal" role="form" method="POST" action="'. base_url('Ledger/update_ledger').'" enctype="multipart/form-data">
		<div class="modal-body">
			<input type="hidden" name="mledgid"  id="mledgid" value="'.$row->ledg_id.'" required>
			<input type="hidden" name="mop"  id="mop" value="'.$op.'" >
				<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Ledger Name :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="mlname"  id="mlname"  value="'. $row->ledg_name.'"required>
								</div>
							</div>
						</div>
						
									
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Under :</label>
								<div class="col-md-7">
									<select  class="form-control" name="mlunder"  id="mlunder" required>
									  <option value="">---under----</option>';
									  
										$gquery=$this->db->select("*")->from("ac_groups")->get()->result();
										foreach( $gquery as $g)
										{
											echo "<option value='".$g->grp_id."'"; if($g->grp_id==$row->ledg_group) echo "selected"; echo ">".$g->grp_name."</option>";
										}
									  
									  echo '
														  
									</select>
								</div>
							</div>
						</div>

		  </div>
		  <div class="modal-footer" style="padding-right:50px;">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Save changes</button>
		  </div>
	  </form>';
}

	
	public function update_ledger()
	{
	$mledgid=$this->input->post('mledgid');
	$mlname=$this->input->post('mlname');
	$mlunder=$this->input->post('mlunder');
	$mop=$this->input->post('mop');
		
	$data1=array(
			'ledg_name'=>strtoupper($mlname),
			'ledg_group'=>$mlunder,
			);
			
	$this->load->model("Model_accounts");
	$where="ledg_id=".$mledgid;
	$this->Model_accounts->update("ac_ledgers",$data1,$where);
	$this->session->set_flashdata('message', '2#Ledger sccessfully updated.'); 
	if($mop==0)
	{
	redirect('Ledger/Add');
	}
	else if($mop==1)
	{
	redirect('Ledger/View');	
	}
}


}
?>
