<html>
<head>
<title>Eshop: Admin Login</title>
<link rel='stylesheet' href="<?php echo base_url('assets/login_style.css');?>" type='text/css'>
</head>
<body style="background-color: rgb(12, 77, 162)">
<center>
<img class="logo" src="<?php echo base_url('assets/images/eshop.png');?>" alt="eshop-logo"  style="margin-top:10px;" > 
</center>
<hr style="size:1px;"  color="#282fb2">
<div class='content_form'>
<?php $attributes = array('class' => 'log_form', 'id' => 'myform');
 echo form_open('login',$attributes ); ?>
<!--<div class='header' style="text-align: center;">Login</div>-->
<fieldset>
<section>
<h2 style="padding:0px;margin:0px;"> LOGIN </h2>
<hr>
<br>
<input type="text" name="username" value="<?php echo set_value('username'); ?>" placeholder='Username' size="50" />
<?php echo form_error('username','<div class="error">', '</div>'); ?>
</section>
<section>
<input type="password" name="password" size="50" placeholder='Password' />
<?php echo form_error('password','<div class="error">', '</div>'); ?>
<?php if(isset($msg)) {echo $msg;} ?>
</section>

<div><input type="submit" value="Submit" class='button' /></div>
<!-- <a href="<?php echo base_url('User/index');?>" class="text-center">Register a new membership</a>
 -->
</fieldset>
</form>
</div>
<br>
</body>
</html>
