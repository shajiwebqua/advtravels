<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Voucher extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		if (!isset($user))
		{ 
			//redirect('/');
		} 
		else
		{ 
			return true;
		}
		$this->load->helper(array('form', 'url','html'));
	}

	
//to load pay ment adding/listing page
    function payments()
	{
		$op=$this->uri->segment(3);
		$dt1="";
		$dt2="";
		if($op==2)
		{
			$sdate=$this->input->post('startdate');
			$edate=$this->input->post('enddate');
			if($sdate!=="" and $edate!="")
			{
				$d1=explode("-",$sdate);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$edate);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			}
			else
			{
				$dt1="";
				$dt2="";
			}
		
		}
        $this->load->view("account/acc_payments"); 
	}
		
//to load pay ment adding/listing page
	function receipts()
	{
		$op=$this->uri->segment(3);
		$dt1="";
		$dt2="";
		if($op==2)
		{
			$sdate=$this->input->post('startdate');
			$edate=$this->input->post('enddate');
			if($sdate!=="" and $edate!="")
			{
				$d1=explode("-",$sdate);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$edate);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			}
			else
			{
				$dt1="";
				$dt2="";
			}
		}
		$this->load->view("account/acc_receipts"); 
	}
	
	
	//to load pay ment adding/listing page
/*	function contra()
	{
		$op=$this->uri->segment(3);
		$dt1="";
		$dt2="";
		$data['tabstatus']=1;
		if($op==2)
		{
			$sdate=$this->input->post('startdate');
			$edate=$this->input->post('enddate');
			if($sdate!=="" and $edate!="")
			{
				$d1=explode("-",$sdate);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$edate);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			}
			else
			{
				$dt1="";
				$dt2="";
			}
			$data['tabstatus']=2;
		}
		else if($op==3)
		{
		$data['tabstatus']=2;			
		}
		
		$this->load->model("Model_accounts");
		$data['result']=$this->Model_accounts->contra_list($dt1,$dt2);
		$this->load->view("account/acc_contra",$data); 
	}
*/

	//to load pay ment adding/listing page
/*	function journal()
	{
		$op=$this->uri->segment(3);
		$dt1="";
		$dt2="";
		$data['tabstatus']=1;
		if($op==2)
		{
			$sdate=$this->input->post('startdate');
			$edate=$this->input->post('enddate');
			if($sdate!=="" and $edate!="")
			{
				$d1=explode("-",$sdate);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$edate);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			}
			else
			{
				$dt1="";
				$dt2="";
			}
			$data['tabstatus']=2;
		}
		else if($op==3)
		{
		$data['tabstatus']=2;			
		}
		
		$this->load->model("Model_accounts");
		$data['result']=$this->Model_accounts->journal_list($dt1,$dt2);
		$this->load->view("account/acc_journal",$data); 
	}
*/
/*
	public function save_journal()
	{
		
	$jtype=$this->input->post('jtype');
	$jno=$this->input->post('jno');
	$jdate=$this->input->post('jdate');
	$lbyledger=$this->input->post('jbyledger');
	$lbyamt=$this->input->post('jbyamt');
	$jtoledger=$this->input->post('jtoledger');
	$ltoamt=$this->input->post('jtoamt');
	$jnarra=$this->input->post('jnarra');
		
	$d1=explode("-",$jdate);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
	$data1=array(
			'acc_jou_type'=>"PAYMENTS",
			'acc_jou_jno'=>$dt1,
			'acc_jou_date'=>$paccount,
			'acc_jou_ledger'=>$pledger,
			'acc_pay_amount'=>$pamount,
			'acc_pay_narration'=>$pnarrat,
			);

	$this->load->model("Model_accounts");	
	$this->Model_accounts->insert("acc_payments",$data1);
	$this->session->set_flashdata('message', '1#Payments sccessfully added.'); 
	redirect('Voucher/payments');
	}
*/

//to get details for edit--------------------------
	/*function get_edit_payments()
	{
		$str="";
		$pid=$this->input->post('payid');
		$row=$this->db->select('*')->from('acc_payments')->where('acc_pay_id',$pid)->get()->row();
		
		$str.=$row->acc_pay_id.",";
		$str.=$row->acc_pay_ledger.",";
		$str.=$row->acc_pay_amount.",";
		$str.=$row->acc_pay_narration;
		echo $str;
	}
*/

// update payment details ----------------------------
/*	function edit_payment()
	{
	$mpid=$this->input->post('mpid');
	$mpledger=$this->input->post('mpledger');
	$mpamount=$this->input->post('mpamount');
	$mpnarrat=$this->input->post('mpnarrat');
		
	$data1=array(
			'acc_pay_ledger'=>$mpledger,
			'acc_pay_amount'=>$mpamount,
			'acc_pay_narration'=>$mpnarrat,
			);

	$this->load->model("Model_accounts");	
	$where="acc_pay_id=".$mpid;
	$this->Model_accounts->update("acc_payments",$data1,$where);
	$this->session->set_flashdata('message', '2#Payment sccessfully updated.'); 
	redirect('Voucher/payments/3');
	}
	*/
	/*
	function delete_payment()
	{
	$pid=$this->input->post('payid');
	$this->load->model("Model_accounts");	
	$where="acc_pay_id=".$pid;
	$this->Model_accounts->del("acc_payments",$where);
	$this->session->set_flashdata('message', '3#Payment sccessfully removed.'); 
	//redirect('Voucher/payments/3');
	}
	*/

	
	public function save_receipts()
	{
	$rvdate=$this->input->post('rvdate');
	$rvno=$this->input->post('rvno');
	$rvfrom=$this->input->post('rvfrom');
	
	$rvnarrat=$this->input->post('rvnarrat');
	$rvamount=$this->input->post('rvamount');
	
	$pmode=$this->input->post('paymode');
			
	$d1=explode("-",$rvdate);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	
	$ledger=explode("#",$rvfrom);
	
	
	if($pmode==1)
	{
		$data1=array(
		'accu_date'=>date('Y-m-d'),
		'accu_description'=>$ledger[1],
		'accu_voucherno'=>$rvno,
		'accu_narration'=>$rvnarrat,
		);
	}
	else
	{		
	$rvchno=$this->input->post('rvchno');
	$rvchdate=$this->input->post('rvchdate');
		
	$d3=explode("-",$rvchdate);
	$dt3=$d3[2]."-".$d3[1]."-".$d3[0];

	$data1=array(
		'accu_date'=>date('Y-m-d'),
		'accu_description'=>$ledger[1],
		'accu_voucherno'=>$rvno,
		'accu_narration'=>"(Received-".$rvamount.") ".$rvnarrat,
		'accu_chequeno'=>$rvchno,
		'accu_chequedate'=>$dt3,
		);
	}

	$this->load->model("Model_accounts");	
	$insid=$this->Model_accounts->insert_data("ac_accounts",$data1);
	
	
	if($pmode==1){ $ldg1=5;} else{ $ldg1=3; }

	  //ENTRY 1
		 $accdata2=array(
		 "entry_date"=>date('Y-m-d'),
		 "entry_accuid"=>$insid,
		 "entry_ledgerid"=>$ldg1, 	
		 "entry_debit"=>$rvamount, 
		 "entry_credit"=>0,
		 );
		 $this->Model_accounts->insert("ac_account_entry",$accdata2);
		 
		 //ENTRY 2
		 $accdata3=array(
		 "entry_date"=>date('Y-m-d'),
		 "entry_accuid"=>$insid,
		 "entry_ledgerid"=>$ledger[0],  			
		 "entry_debit"=> 0,
		 "entry_credit"=>$rvamount,
		 );
		 
		 $this->Model_accounts->insert("ac_account_entry",$accdata3);
		 
		 // add data to cash book   
		 $acccash=array(
		 "cashbook_date"=>date('Y-m-d'),
		 "cashbook_headid"=>$insid,
		 "cashbook_debit"=>0,
		 "cashbook_credit"=>$rvamount, 
		 "cashbook_narration"=>"(Received-".$rvamount.") ".$rvnarrat,
		 );
		 $this->Model_accounts->insert("ac_cashbook",$acccash);	

	$this->session->set_flashdata('message', '1#Receipt sccessfully added.'); 
	redirect('Voucher/receipts');
	}
		
	public function save_payments()
	{
		$pvdate=$this->input->post('pvdate');
		$pvno=$this->input->post('pvno');
		$pvby=$this->input->post('pvby');
		$pvto=$this->input->post('pvto');
		$pvnarrat=$this->input->post('pvnarrat');
		$pvamount=$this->input->post('pvamount');
		
		$pmode=$this->input->post('paymode');
				
		$d1=explode("-",$pvdate);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$ledger=explode("#",$pvto);
		$ldg1=explode("#",$pvby);

		$data1=array(
			'accu_date'=>date('Y-m-d'),
			'accu_description'=>$ldg1[1]." charges",
			'accu_voucherno'=>$pvno,
			'accu_narration'=>$pvnarrat,
			);
				
	$this->load->model("Model_accounts");	
	$insid=$this->Model_accounts->insert_data("ac_accounts",$data1);
	
		//ENTRY 1
		 $accdata2=array(
		 "entry_date"=>date('Y-m-d'),
		 "entry_accuid"=>$insid,
		 "entry_ledgerid"=>$ldg1[0],		
		 "entry_debit"=>$pvamount, 
		 "entry_credit"=>0,
		 );
		 $this->Model_accounts->insert("ac_account_entry",$accdata2);
		 
		 //ENTRY 2
		 $accdata3=array(
		 "entry_date"=>date('Y-m-d'),
		 "entry_accuid"=>$insid,
		 "entry_ledgerid"=>$ledger[0],  	
		 "entry_debit"=>0, 
		 "entry_credit"=>$pvamount,
		 );
		 
		 $this->Model_accounts->insert("ac_account_entry",$accdata3);
		 
		 // add data to cash book   
		 $acccash=array(
		 "cashbook_date"=>date('Y-m-d'),
		 "cashbook_headid"=>$insid,
		 "cashbook_debit"=>$pvamount,
		 "cashbook_credit"=>0,
		 "cashbook_narration"=>$pvnarrat,
		 );
		 $this->Model_accounts->insert("ac_cashbook",$acccash);	

	$this->session->set_flashdata('message', '1#Payments sccessfully completed.'); 
	redirect('Voucher/payments');
	}
	
	
	// update payment details ----------------------------
/*	function edit_receipt()
	{
	$mrid=$this->input->post('mrid');
	$mrledger=$this->input->post('mrledger');
	$mramount=$this->input->post('mramount');
	$mrnarrat=$this->input->post('mrnarrat');
		
	$data1=array(
			'acc_re_ledger'=>$mrledger,
			'acc_re_amount'=>$mramount,
			'acc_re_narration'=>$mrnarrat,
			);

			
	$this->load->model("Model_accounts");	
	$where="acc_re_id=".$mrid;
	$this->Model_accounts->update("acc_receipts",$data1,$where);
	$this->session->set_flashdata('message', '2#Receipt sccessfully updated.'); 
	redirect('Voucher/receipts/3');		
	
	}*/
	
	//to get details for edit--------------------------
	/*function get_edit_receipts()
	{
		$str="";
		$pid=$this->input->post('reid');
		$row=$this->db->select('*')->from('acc_receipts')->where('acc_re_id',$pid)->get()->row();
		
		$str.=$row->acc_re_id.",";
		$str.=$row->acc_re_ledger.",";
		$str.=$row->acc_re_amount.",";
		$str.=$row->acc_re_narration;
		echo $str;
	}*/
	
	
  /*	function delete_receipt()
	{
	$rid=$this->input->post('reid');
	$this->load->model("Model_accounts");	
	$where="acc_trans_id=".$rid;
	$this->Model_accounts->del("acc_trans_head",$where);
	
	$where="acc_tra_headid=".$rid;
	$this->Model_accounts->del("acc_transactions",$where);
	
	$this->session->set_flashdata('message', '3#Receipt sccessfully removed.'); 
	redirect('Voucher/payments/3');
	}
	*/
	
	//Contra vouchers
	
	function save_contra()
	{
	$cvtype=$this->input->post('cvtype');
	$cdate=$this->input->post('cdate');
	$caccount=$this->input->post('caccount');
	$cledger=$this->input->post('cledger');
	$cnarrat=$this->input->post('cnarrat');
	$camount=$this->input->post('camount');
		
	$d1=explode("-",$cdate);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
	$data1=array(
			'acc_con_type'=>"CONTRA",
			'acc_con_date'=>$dt1,
			'acc_con_account'=>$caccount,
			'acc_con_ledger'=>$cledger,
			'acc_con_amount'=>$camount,
			'acc_con_narration'=>$cnarrat,
			);

	$this->load->model("Model_accounts");	
	$this->Model_accounts->insert("acc_contra",$data1);
	$this->session->set_flashdata('message', '1#Contra voucher sccessfully added.'); 
	redirect('Voucher/contra');
	}
	
	function edit_contra()
	{
	$mcid=$this->input->post('mcid');
	$mcledger=$this->input->post('mcledger');
	$mcamount=$this->input->post('mcamount');
	$mcnarrat=$this->input->post('mcnarrat');
		
	$data1=array(
			'acc_con_ledger'=>$mcledger,
			'acc_con_amount'=>$mcamount,
			'acc_con_narration'=>$mcnarrat,
			);

			
	$this->load->model("Model_accounts");	
	$where="acc_con_id=".$mcid;
	$this->Model_accounts->update("acc_contra",$data1,$where);
	$this->session->set_flashdata('message', '2#Voucher sccessfully updated.'); 
	redirect('Voucher/contra/3');		
	
	}
	
	function delete_contra()
	{
	$cid=$this->input->post('coid');
	$this->load->model("Model_accounts");	
	$where="acc_con_id=".$cid;
	$this->Model_accounts->del("acc_contra",$where);
	$this->session->set_flashdata('message', '3#Voucher sccessfully removed.'); 
	//redirect('Voucher/payments/3');
	}
		
	

	
	
	
}
?>
