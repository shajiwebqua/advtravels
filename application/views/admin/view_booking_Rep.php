<?php
 include('application/views/common/header.php');?>
<style>
.colpad
{
	padding-left:3px;
	padding-right:3px;
}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
  <!-- Content Header (Page header) -->
  <section class="content-header">
  <h1><b>Booking List Report</b></h1>
  <ol class="breadcrumb" style="font-size:15px;">
	<!-- <li> <a id='view-pdf' href="<?php echo base_url('Reports/booking_report');?>" style="color:#4b88ed;"><button><i class="fa fa-print" aria-hidden="true"></i> Print</button> </a></li> -->
	<li> <a href="<?php echo base_url('Trip/booking_trip');?>" style="color:#fff;"><button class='btn btn-primary'> <i class="fa fa-cab" aria-hidden="true"></i> New Booking</a></button></li>
	 <li> <a href="<?php echo base_url('Trip/booking_list');?>" style="color:#4b88ed;"><button class='btn btn-info'><i class="fa fa-list" aria-hidden="true"></i> Booking List</button> </a></li>
  </ol>
  
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->   
    
  <section class="content"> 
  
    <div style="padding:2px 0px 2px 0px;">
		 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
		 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div>
		 <div id='mserr'><?php echo $this->session->flashdata('message1'); ?></div>
		 </center>
		 </div>
	</div>
					
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->

<div class="portlet-body">
<div style="background-color:#fff;padding:15px; ">
              
		<div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
			
			<!--<form role="form" method="POST" action="" enctype="multipart/form-data"> -->
			<div class='row' style='margin-top:15px;'>
			<div class='col-md-8'>
			<label class='col-md-4 control-label la-right'>Select Date [from-to] : </label>
			<div class='col-md-3 ' style='padding-top:2px;'><input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-3 ' style='padding-top:2px;'><input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			</div>
			</div>
			
			<div class='row' style='margin-top:15px;'>
			<div class='col-md-8'>
			<label class='col-md-4 control-label la-right'>Client Type : </label>
			<div class='col-md-4 ' style='padding-top:2px;'>
				<select class="form-control" id="cltype" name="cltype" >
					<option value="0" selected>--Client Type---</option>
					<option value="1">INDIVIDUAL</option>
					<option value="2">CORPORATE</option>
					<option value="3">AGENTS</option>
				</select>
			</div>
			</div>
			</div>
				
			<div class='row' style='margin-top:15px;'>
			<div class='col-md-8'>
			<label class='col-md-4 control-label la-right'>Select Client : </label>
			<div class='col-md-6 ' style='padding-top:2px;'>
				<select class="form-control" id="client" name="client">
					
				</select>
			</div>
			</div>
			</div>
			
			
			<div class='row' style='margin-top:15px;margin-bottom:25px;'>
			<div class='col-md-8'>
				<div class='col-md-4'> </div>
				<div class='col-md-2 ' style='margin-top:2px;'><input type="button" id='btn_get' rel="<?php echo base_url('Reports/booking_report')?>" class="form-control btn btn-success" style="text-align:center;"  value="Get List" name='btnget' > </div>
				<div class='col-md-2 ' style='margin-top:2px;padding-left:3px;'><input type="button" id='btn_cancel' rel="<?php echo base_url('Reports/booking_report')?>" class="btn btn-warning" style="text-align:center;padding-top:4px;padding-bottom:4px;border-radius:0px;"  value="Cancelled List" name='btncancel'></div>
				<div class='col-md-3 ' style='margin-top:2px;'><input type="button" id='btn_close' rel="<?php echo base_url('Reports/booking_report')?>" class="btn btn-primary" style="text-align:center;padding-top:4px;padding-bottom:4px;border-radius:0px;"  value="Closed List" name='btnclose'></div>
			</div>
			</div>
			<!--</form> -->
			
			
			
                <!-- END CONTENT BODY -->
	</div>
	</div>
            <!-- END QUICK SIDEBAR -->
</div>
  
                     <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div> 

 
      
  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
<!-- /.content --> 
</section>
  <!-- /.content-wrapper -->
</div>
 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

setInterval(function(){ $('#idh5').html(''); }, 3000);

$("#idmsg").hide();
$("#msgerr").hide();
//sweet alert ----------------------------
	if($.trim($("#idh5").html())!="")
	{
		swal( $("#idh5").html(),"","success")
		$("#idh5").html("");
	}
	
	if($.trim($("#msgerr").html())!="")
	{
		swal($("#idh5").html(),"","error")
		$("#idh5").html("");
	}
// sweet alert box -------------------------- 

 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
 
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   }); 
  
 $("#cltype").change(function()
 {
	 var op=$(this).val();
	 $("#client").empty();
	 if(op==2 || op==3)
	 {
	 jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Reports/get_corporate_names",
        dataType: 'html',
        data: {cltype: op},
        success: function(data) {
			$("#client").prop("disabled",false);
			$("#client").prop("required",true);
			$("#client").html(data);
			 }
		});
	 }
	 else
	 {
		 $("#client").prop("required",false);
		 $("#client").prop("disabled",true);
	 }
 });
 
 
    $('#btn_get').on('click',function(){
		var plink = $(this).attr('rel');
		var sdate =$.trim($("#datepicker1").val());
		var edate=$.trim($("#datepicker2").val());
		
		if(sdate!="" && edate!="")
		{
		get_Report(1,plink,sdate,edate);
		}

    });  
	
	$('#btn_cancel').on('click',function(){
		var plink = $(this).attr('rel');
		var sdate =$.trim($("#datepicker1").val());
		var edate=$.trim($("#datepicker2").val());

		if(sdate!="" && edate!="")
		{
		get_Report(2,plink,sdate,edate);
		}

    });  
	
	$('#btn_close').on('click',function(){
		var plink = $(this).attr('rel');
		var sdate =$.trim($("#datepicker1").val());
		var edate=$.trim($("#datepicker2").val());
		
		if(sdate!="" && edate!="")
		{
		get_Report(3,plink,sdate,edate);
		}

    });  
	
	
	function get_Report(bt,pdf,sdate,edate)
	{
		var client="";
			
		//var sdate = $("#datepicker1").val();
		//var edate=$("#datepicker2").val();
		
			var ctype=$("#cltype").val();
			if(ctype=="0" || ctype=="1")
			{
				client="0";
			}else
			{
			var client=$("#client").val();
			}
			
			var btn=bt;

			var plink=pdf+"/"+sdate+"/"+edate+"/"+ctype+"/"+client+"/"+btn;

			var iframe = '<div class="iframe-container"><iframe src="'+plink+'"></iframe></div>';
			$.createModal({
			title:'Booking List',
			message: iframe,
			closeButton:true,
			scrollable: true,
			});
	}
	
  
	
		
</script>



