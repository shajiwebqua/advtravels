<?php
 include('application/views/common/header.php');?>
  <!-- Content Wrapper. Contains page content -->
 
  
  <div class="content-wrapper">
    <section class="content-header">
      <h1 style='margin-left:5px;padding-bottom:10px;'>
    <b>Trip Quotation</b>
      <small><b>(Ctrl+P -> Print Quotation)</b></small> 
      </h1>
      <ol class="breadcrumb" style='font-size:15px;'>
    <li><a href="<?=site_url('Trip/trip_qutationlist');?>" style='color:#4b88ed;'><i class="fa fa-backward" aria-hidden="true"></i>Back to Trip Quotation</a></li>
    <li><a href="#" onclick="PrintDiv();" style='color:#4b88ed;'><i class="fa fa-print" aria-hidden="true"></i>Print</a></li>
       <li><a class='view-pdf' href="<?=site_url('Pdf/pdf_tripquotation/');?>" id= "<?= nl2br($trip_quot[0]->tripquot_id) ?>" style='color:#4b88ed;'><i class="fa fa-save" aria-hidden="true"></i>PDF</a></li>
     </ol>
    </section>
<hr style='margin:0px;'>
  
  <div id="print" style="margin:0px 20px 0px 20px;";>
  <style type="text/css">
 /*td
   {
   padding:5px;
   }*/

  .brd1
  {
    border:1px solid #e4e4e4;
    
  }  
   </style>
  
  
  </head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="500px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td><td width="150px" style="padding: 0px">
<?php $date = date('Y/m/d');?>
Date:<?=$date;?> 
  </td>
</tr>
<tr>
<td width="500px" style="padding: 0px"></td>
<td width="150px" style="padding: 0px">
  Trip Quotation No :<?= nl2br($trip_quot[0]->tripquot_id) ?>
  </td>
</tr>
</table>
<hr>

<table style="border:none;width:100%;">
  <tr>
      <td>From
          <address>
            <strong>Advance World Group</strong><br>
            Advance World Group LLC,P.O.Box : 7192<br>
            Al jurf Industrial Area, Al Jurf 1, Ajman UAE<br>
            Tel: +971 6 748 7636<br>
            Fax: +971 6 748 7637
          </address></td>
          <td align='right'> To : 
          <address>
           <strong><?= nl2br($trip_quot[0]->cust_name) ?></strong><br>
            <?= nl2br($trip_quot[0]->cust_address) ?><br>
           Phone:<?= nl2br($trip_quot[0]->cust_phone) ?><br>
          </address></td>
  </tr>
</table>
  <hr>
  <h4>Food Menu :</h4>
  <table width="100%">
    <tr>
      <!-- <th align='left'></th> -->
      <td><?= nl2br($trip_quot[0]->food_menu) ?></td>
    </tr>
  </table>
  <hr>
  <h4>Accomodation Menu :</h4>
  <table width="100%">
    <tr>
      <!-- <th align='left'></th> -->
      <td><?= nl2br($trip_quot[0]->acm_menu) ?></td>
    </tr>
  </table>
  <hr>
  <h4>Places Visiting :</h4>
  <table width="100%">
    <tr>
      <!-- <th align='left'></th> -->
      <td><?= nl2br($trip_quot[0]->place_visit) ?></td>
    </tr>
  </table>
<hr>
  <h4>Charges :</h4>
  <table width="25%">
    <tr>
      <th align='left'>Minimum Charge KM</th>
      <td><?= $trip_quot[0]->trip_mincharge ?></td>
    </tr>
    <tr>
      <th align='left'>Per Km Charge</th>
      <td><?= $trip_quot[0]->trip_perkmcharge ?></td>
    </tr>
    <tr>
      <th align='left'>Other Charges</th>
      <td><?= $trip_quot[0]->trip_othercharge ?></td>
    </tr>
    <tr>
      <th align='left'>Accomadation Charge</th>
      <td><?= $trip_quot[0]->acm_cost ?></td>
    </tr>
    <tr>
      <th align='left'>Food Charge</th>
      <td><?= $trip_quot[0]->food_cost ?></td>
    </tr>
  </table>
 <table width="100%">
 <tr><td colspan='3' height='30px'></td> </tr>
             <tr>
                <th width='86%' style='text-align:right;padding-right:25px;font-size:25px;'>RS : <?= $trip_quot[0]->total ?></th>
                <td width='10%' style='text-align:right;font-size:25px;'><b></b></td>
        <td width='3%'></td>
              </tr>
        <tr>
                <td width='10%' style='text-align:right;font-size:25px;'><b>===========</b></td>
        <td width='3%'></td>
              </tr>
 </table>

</div>

    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
  <?php
 include('application/views/common/footer.php');?>
 <script type="text/javascript">     
    /*function PrintDiv() {    
       var divToPrint = document.getElementById('print');
       var popupWin = window.open('', '_blank', 'width=1000,height=600');
       popupWin.document.open();
       popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
       popupWin.document.close();
        }*/
 </script>
 <script type="text/javascript">
   $('.view-pdf').on('click',function(){
      var id = $(this).attr('id');
    var pdf_link = $(this).attr('href')+'/'+id;
      var id = $(this).attr('id');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(pdf_link);
        //alert(id);
        $.createModal({
        title:'Trip Quotation Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); 
       
     return false; 

    });  
</script>

    <script type="text/javascript">     
    function PrintDiv() {    
       var divToPrint = document.getElementById('print');
       var popupWin = window.open('', '_blank', 'width=1000,height=600');
       popupWin.document.open();
       popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
       popupWin.document.close();
        }
 </script>  
 </script>