<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Travels</title>

  <style type="text/css">
 td
   {
   padding:5px;
   }

  .brd1
  {
	  border:1px solid #e4e4e4;
	  
  }  
   </style>
  
  
  </head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="upload/images/atravels_logo.png" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

<div id="title">Trip Details</div>
</span>
<hr>

<table width="100%" >
<tr><td width='150px'>Customer Name </td><td>:&nbsp;&nbsp;&nbsp;<?=$results->customer_name;?></td><tr>

<tr><td>Customer Mobile </td><td>:&nbsp;&nbsp;&nbsp;<?=$results->customer_mobile;?></td><tr>

<tr><td>Poupose</td><td>:&nbsp;&nbsp;&nbsp;<?=$results->customer_purpose;?></td><tr>

<tr><td>Vehicle No</td><td>:&nbsp;&nbsp;&nbsp;<?=$results->vehicle_regno;?></td><tr>

<tr><td>Driver Name</td><td>:&nbsp;&nbsp;&nbsp;<?=$results->driver_name;?></td><tr>

<tr><td>Trip (From - To )</td><td >:&nbsp;&nbsp;&nbsp;<?=$results->trip_from.' '.'-'.$results->trip_to;?></td><tr>

<tr><td>Date (Start - End )</td><td>:&nbsp;&nbsp;&nbsp;<?=$results->trip_startdate.' '.'-'.$results->trip_enddate;?></td><tr>

<tr><td>Time (Start - End )</td><td >:&nbsp;&nbsp;&nbsp;<?=$results->trip_starttime.' '.'-'.$results->trip_endtime;?></td><tr>

<tr><td>KM (Start - End )</td><td>:&nbsp;&nbsp;&nbsp;<?=$results->trip_startkm.' '.'=>'.$results->trip_endkm;?></td><tr>

<tr><td>Running KM</td><td>:&nbsp;&nbsp;&nbsp;
<?php 
$end=$results->trip_endkm;
$start=$results->trip_startkm; ?> 
<?=abs($end-$start);?></td><tr>

<tr><td>Minimum Charge Km</td><td>:&nbsp;&nbsp;&nbsp;<?=$results->trip_minchargekm;?></td><tr>

<tr><td colspan='2' style='color:#1255ba;'>Minimum Charges:</td><tr>
<tr><td colspan='2'>

<table width='100%' class='brd1' cellpadding=0 cellspacing=0>
<tr><td width='100px'>Min. Days:</td>
<td width='70px' class='brd1'>:&nbsp;&nbsp;<?=$results->trip_mindays;?></td>
<td width='100px' class='brd1'>Min. Charge:</td>
<td width='70px' class='brd1'>:&nbsp;&nbsp;<?=$results->trip_mincharge;?></td>
<td width='100px' class='brd1'>Min. Total:</td>
<td class='brd1'> :&nbsp;&nbsp;<?=$results->trip_mintotal;?></td>
</tr>
</table>
</td></tr>

<tr><td colspan='2' style='color:#1255ba;'>Additional Charges:</td><tr>
<tr><td colspan='2'>
<table width='100%' class='brd1' cellpadding=0  cellspacing=0>
<tr><td width='100px' class='brd1'>Addi.KM:</td>
<td width='70px' class='brd1'>:&nbsp;&nbsp;<?=$results->trip_addikm;?></td>
<td width='100px' class='brd1'>KM Charge:</td>
<td width='70px' class='brd1'>:&nbsp;&nbsp;<?=$results->trip_addikmcharge;?></td>
<td width='100px' class='brd1'>Addi. Total:</td>
<td class='brd1'>:&nbsp;&nbsp;<?=$results->trip_additotal;?></td>
</tr>
</table>
</td></tr>

<tr><td colspan='2'>
<table width='100%' >
<tr><td width='100px'></td><td width='70px'></td><td width='100px'></td><td colspan='2' style='color:blue;'>Total Trip Charge </td>
<td width='92px'>:&nbsp;&nbsp;<b><?=$results->trip_charge;?></b></td>
</tr>
</table>
</td></tr>


<tr><td colspan='2' style='color:#1255ba;'>Other Charges</td><tr>

<tr><td colspan='2'>
<table width='100%' class='brd1' cellpadding=0  cellspacing=0>
<tr><td width='100px' class='brd1'>Toll/Parking:</td>
		<td width='65px' class='brd1'><?=':'.$results->trip_tollparking;?></td>
		<td width='120px' class='brd1'>Interstate Permit:</td>
		<td width='70px' class='brd1'><?=':'.$results->trip_interstate;?></td>
		<td width='100px' class='brd1'>Other Charges:</td>
		<td class='brd1' ><?=':'.$results->trip_othercharge;?></td>
</tr>
</table>
</td></tr>

<tr><td>
<table width='100%'><tr><td width='170px'>Other charge Description</td><td>:&nbsp;&nbsp;<?=$results->trip_otherdesc;?></td></tr></table>
</td></tr>
<tr><td>
<table width='100%'><tr><td width='170px'>Addition Information</td><td>:&nbsp;&nbsp;<?=$results->trip_addiinfo;?></td></tr></table>
</td>
</tr>

<tr><td colspan='2'>
<table width='100%' >
<tr><td width='100px'></td><td width='70px'></td><td width='100px'></td><td style='color:blue;'><b>Grand Total</b> </td>
<td width='100px' style='font-size:18px;' colspan='2'>:&nbsp;&nbsp;<b><?='<b>Rs: '.$results->trip_gtotal;?></b></td>
</tr>
<tr><td width='100px'></td><td width='70px'></td><td width='100px'></td><td  colspan=3>-------------------------------------------------------</td>

</tr>
</table>
</td></tr>

</table>
                       
</html>