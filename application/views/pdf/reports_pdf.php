<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>
	<?php
	foreach ($results as $key => $r) {
		   		if($r->trip_status==2)
		{
			$st1="Completed Trips";
		}
		else if($r->trip_status==1 && $r->trip_startdate<date('Y-m-d'))
		{
			$st1="Running Trips";
		}
		else if($r->trip_status==0)
		{
			$st1="New Trips"; 
		}
		else if($r->trip_status==3)
		{
			$st1="Closed Trips"; 
		}
		else
		{
			$st1="Upcoming Trips"; 
		} 
		} ?>

	
 <table width="100%"><tr><td><h4><?=$st1;?> Reports 
 <?php 
 if(trim($dte1)!=="" and trim($dte2)!=="")
 {
 echo "[".$dte1." => ".$dte2."]"; 
 }
 
 if(trim($dte2)=="r2")
 {
 echo "[on ".date('Y-m-d')."]"; 
 }
 ?>
 
 
 </h4></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
		<table style="width:100%;border:1px solid #e4e4e4;" cellpadding=5 cellspacing=0>
		   <thead>

	  	<?php
		   	foreach ($results as $key => $r) {
		/*if($r->trip_status==2)
		{
			$st="<font color='blue'>Completed</font>";
		}
		else if($r->trip_status==1 && $r->trip_startdate<date('Y-m-d'))
		{
			$st="<font color='blue'>Running</font>";
		}
		else if($r->trip_status==0)
		{
			$st="<font color='blue'>New</font>"; 
		}
		else if($r->trip_status==3)
		{
			$st="<font color='blue'>Closed</font>"; 
		}
		else
		{
			$st="<font color='blue'>Upcoming</font>"; 
		} */
		}  ?>
			<tr>
			   <tr style='color:#4b88ed;border:1px solid #e4e4e4;'>
			     <th class='th1'>Slno</th>
				 <th class='th1' width='75px'>Trip Id</th>
				 <th class='th1'>Customer</th>
				 <th class='th1'>Mobile</th>
				 <th class='th1' width='100px'>Driver Name</th>
				 <th class='th1'>Date (Start/End)</th>
				 <th class='th1'>Days</th>
				 <th class='th1'>Purpose</th>
				 <!--<th class='th1'>Status</th> -->
				</tr>
			</tr>
		</thead>
		<tbody>
			<?php
			$slno=1;
				foreach ($results as $key => $value) {
					
					$date1=date_create($value->trip_startdate);
					$date2=date_create($value->trip_enddate);
					$diff=date_diff($date1,$date2);
					$days=$diff->format("%a");

					?>
				<tr >
				<td class='td1'><?=$slno;?> </td> 
				<td class='td1'><?=$value->trip_id;?> </td> 
				<td class='td1'><?=$value->customer_name;?></td>
				<td class='td1'><?=$value->customer_mobile;?></td>
				<td class='td1'><?=$value->driver_name;?> </td>
				<td class='td1'><?=$value->trip_startdate." => ".$value->trip_enddate;?> </td>
				<td class='td1'><?=$days;?> </td>
				<td class='td1'><?=$value->trip_purpose;?> </td></tr>
				<!--<td class='td1'><?=$st;?></td> -->  
				</tr>
				<?php
				
				$slno++;
				}
				?>
		  
		</tbody>
		</table>
</body>
</html>