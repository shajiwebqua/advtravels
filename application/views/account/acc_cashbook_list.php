<?php
include('application/views/common/header.php');
?>
<style>
input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
	
</style>


<!--<link href="vendor/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="vendor/select2/dist/js/select2.min.js"></script> -->



        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#00adee;margin-bottom:10px;font-weight:bold;'>Cash Book </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
          </section>
	
        <!-- Main content -->
    <section class="content">
	<?php
//var_dump($alltrans);
?>
	<div class='row' style="padding:5px 15px 5px 15px;">
	<!--<div class="box box-info1 box-solid " style="border:1px solid #f5f3f3;"> -->
	<div class="box " style="border:1px solid #f5f3f3;">
	    <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp; Details</h3>
		  <label id='mes'><?php echo $this->session->flashdata('message');?></label>
        </div>
		
        <div class="box-body">
			<div class='row' style='margin:0px 5px 0px 5px;background-color:#e4e4e4;padding:5px;'>
			<form role="form" method="POST" action="<?php echo base_url('Account/View_cashbook/1')?>" enctype="multipart/form-data">
             			
			<div class='col-md-2' style='padding-top:5px;'>	<label class='control-label'>Select Date [from-to] : </label>	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			<div class='col-md-1' style='padding-top:2px;'><input type='submit' name='btndetails' class='btn btn-primary' value='Get'></div>
			</form>
			
			<form role="form" method="POST" action="<?php echo base_url('Account/View_cashbook')?>" enctype="multipart/form-data">
				<div class='col-md-5' style='padding-top:2px;text-align:right;'>	<input type='submit' name='btndetails2' class='btn btn-warning' value='Back' style='margin-left:10px;'>	</div>
			</form>
			</div>
					
<!-------------------------------------->
					
			<div class='row' style='margin-left:10px;'>
			<label style='padding-bottom:3px; border-bottom:1px solid #e4e4e4;font-size:15px;'><b><?php echo $this->session->flashdata('ptitle');?></b></label>
			</div>
					
			<div class='row' style='padding:10px;'>
			<div class='col-md-12'>
			 <table class="table table-striped table-hover table-bordered" id="example" border=0 STYLE='font-size:14px;width:100%'>
			 
			 <thead>
                <tr>
				 <th width='5%' style='text-align:center !important;'></th>
                 <th width='7%'>ID</th>
				 <th width='10%'>Date</th>
                 <th width='25%'>Description</th> 
                 <th width='10%'>Dedit</th>
				 <th width='10%'>Credit</th>
				 <th >Narration</th>
                </tr>
                </thead>
				<tbody>
					<?php
					$cbdtot=0;
					$cbctot=0;
					if(isset($cblist))
					{
					foreach($cblist as $cr)
					{
						$del=anchor('Account/delete_transactions1/'.$cr->acc_cashbook_id,'<span class="glyphicon glyphicon-trash" aria-hidden="true" style="font-size:14px;color:red;"></span>', array('id' =>'del_conf'));
					?>
						<tr height='33px'>
						 <td align='center'>
						 <?=$del;?>
						 </td>
						 <td><?=$cr->acc_cashbook_id?></td>
					     <td><?=date_format(date_create($cr->acc_trans_date),'d-m-Y');?></td>
						 <td><?=$cr->acc_trans_description;?></td>
						 <td style='text-align:right;'><?=number_format($cr->acc_cashbook_debit,2,".","");?></td> 
						 <td style='text-align:right;'><?=number_format($cr->acc_cashbook_credit,2,".","");?></td> 
						 <td><?=$cr->acc_cashbook_narration;?></td> 
						</tr>
					<?php
					$cbdtot+=$cr->acc_cashbook_debit;
					$cbctot+=$cr->acc_cashbook_credit;
					}
					
					}
					?>	
				</tbody>
					<tr>
					 <td style='text-align:center !important;'></td>
					 <td></td>
					 <td ></td>
					 <td ><b>Total</b></td>
					 <td style='text-align:right;font-size:16px;'><b><?=number_format($cbdtot,2,".","");?></b></td>
					 <td style='text-align:right;font-size:16px;' ><b><?=number_format($cbctot,2,".","");?></b></td>
			 		  <td ></td> 
					</tr>
		 
					<tr>
					 <td style='text-align:center !important;'></td>
					 <td></td>
					 <td ></td>
					 <td ><b>Difference</b></td>
					 <td colspan=2 style='text-align:right;font-size:16px;'><b><?=number_format($cbdtot-$cbctot,2,".","");?></b></td>
			 		 <td ></td> 
					</tr>
		 
				</table>

				</div>
			</div>	
			</div> <!-- second tab end --->
			</div> <!-- tab content end -->
        </div>
	</div>

</section>

<?php
  include('application/views/common/footer.php');
  ?>
</body>
</html>
  <script type="text/javascript">
  
  $("#example").dataTable({
	  "ordering":false,
  });
  
   $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
   
   $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
       
  
  $("#mes").hide();
    
  var msg=$("#mes").html().split("#");
  
  if($("#mes").html()!="")
  {
	  var msg=$("#mes").html().split("#");
		if(msg[0]=='1')
		  swal("",msg[1],"success"); //Saved.!
		else if(msg[0]=='2')
		  swal("",msg[1],"success");  //Updated.!
		else if(msg[0]=='3')
		  swal("",msg[1],"success"); //Removed.!
		else if(msg[0]=='4')
		  swal("",msg[1],"error"); //Try Again.
		 $("#mes").html("");
  }
  
  $(document).on("click", "#del_conf", function () {
        return confirm('Are you sure you want to delete this entry?');
    });
 </script>
</body>
</html>

  
  