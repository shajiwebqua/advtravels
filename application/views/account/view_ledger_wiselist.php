<?php include('application/views/common/header.php'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1 class='heading' style='color:#004f6d;padding-bottom:15px; font-weight:bold;'>Account-Ledger wise list
           </h1>
	  <ol class="breadcrumb" style="margin-right:20px;maring-bottom:0px;">
       <!-- <li ><a href="<?php echo base_url('Ledger/Add');?>"><button class='btn btn-primary btn-xs' style='margin-top:5px;'><i class="fa fa-plus" aria-hidden="true"></i> Add Ledger</button></a></li> -->
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="col-md-12">
          <div class="box">
		  	<!---------  filter row --->
			<div class='row' style='margin:7px 5px 0px 5px;background-color:#e4e4e4;padding:5px;'>
			<form role="form" method="POST" action="<?php echo base_url('Account/ledger_wise_list/1')?>" enctype="multipart/form-data">
				<div class='col-md-2' style='padding-top:5px;text-align:right;'> Select ledger :</div>
				<div class='col-md-5'>
				<select  class="form-control select2" name='ledger'>
				<option value="">----------</option>
				
				<?php
				$res=$this->db->select('ledg_id,ledg_name')->from('ac_ledgers')->order_by('ledg_name','asc')->get()->result();
				foreach($res as $lr)
				{
				?>
					<option value="<?=$lr->ledg_id;?>"><?=$lr->ledg_name;?></option>
				<?php
				}
				?>
				</select>
				</div>
			
			<div class='col-md-1' style='padding-top:5px;padding-left:0px;padding-right:0px;'>	<label class='control-label'>Date [from-to] : </label>	</div>
			<div class='col-md-1'  style='padding-left:0px;padding-right:0px;'>	<input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-1'  style='padding-left:0px;padding-right:0px;'>	<input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			<div class='col-md-1' style='padding-top:2px;'><input type='submit' name='btndetails' class='btn btn-primary' value='Get'></div>
			</form>
			</div>
			
			<!--------  filter row --->
		  
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Details</h3> 
				<center><input type='hidden' id="msg" value='<?php echo $this->session->flashdata('message'); ?>'></CENTER>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style='min-height:450px;'>
              <div class="row" style='margin-left:5px; margin-right:5px;'>
	            <div class="col-md-12">
				<table class="table table-hover table-bordered" id="example"  width='100%' border=0 style="font-size:14px">
				<thead>
                <tr>
				<th width='5%'>Action</th> 
				<th width='8%'>ID</th> 
				<th width='13%'>Date</th> 
				<th >Ledger</th>
                 <th style='width:120px;text-align:right;'>Debit</th> 
				 <th style='width:120px;text-align:right;'>Credit</th> 
                 </tr>
                </thead>
				<tbody>
					<?php
					
					//if(isset($ledwiselist))
					//{
					foreach($ledwiselist as $r)
					{
						$del=anchor('Account/delete_account/'.$r->entry_id,'<button class="btn btn-danger btn-xs">Del</button>', array('id' =>'del_conf'));					
					?>
						
						<tr>
						<td class='dtable' align='center'> 
							<!--<a href='#' id='<?=$r->ledg_id;?>' class='abtn abtn-edit edit' data-toggle="modal" data-target="#ModalEdit" style='padding-right:7px;'>
							<span class="glyphicon glyphicon-edit" aria-hidden="true" style='font-size:16px;'></span></a>-->
							<?=" ".$del;?>  
						</td> 						
						 <td ><?=$r->entry_id;?></td>
						 <td ><?=$r->entry_date;?></td>
						 <td ><?=$r->ledg_name;?></td>
						 <td style='width:120px;text-align:right;'><?=number_format($r->entry_debit,"2",".","");?></td>
						 <td style='width:120px;text-align:right;' ><?=number_format($r->entry_credit,"2",".","");?></td>
						 </tr>
					<?php
					}
					//}
					?>	
				</tbody> 
				</table>
			  </div>
        </div>
       </div>
    </div>
   </div>
</div>
</section>
</div>


<!-- /.content-wrapper -->
<!---EDIT  modal dialog start -->
<!-- Modal -->
<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style='top:50px;border:3px solid #b7b7b7;'>
	 <div class="modal-body" style='padding:0px;'>
            
     </div>
    </div>
  </div>
</div>

<?php include('application/views/common/footer.php'); ?>
   
<script type="text/javascript">

  $('.select2').select2();
  
$("#example").dataTable({
	"ordering":false,
	});

var msg=$("#msg").val().split('#');
if(msg[0]=='1')
{
	swal('',msg[1],'success'); //Saved
}
else if(msg[0]=='2')
{
	swal('',msg[1],'success'); //Updated.
}
else if(msg[0]=='3')
{
	swal('',msg[1],'success'); //Removed.
}
else if(msg[0]=='4')
{
	swal('',msg[1],'error'); //Try Again.
}

 $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });


  $('#example tbody').on('click', '.edit', function (){
        var Result=$("#ModalEdit .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Ledger/edit_ledger",
        dataType: 'html',
        data: {lid: id,op:1},
        success: function(res) 
		{
				Result.html(res);
        }
        });
   }); 
   
   
   $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
   
  $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
 
</script>  
