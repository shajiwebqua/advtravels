<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_userprofile extends CI_Controller {

    function __construct(){
parent::__construct();
$this->load->model('Model_userprofile');
$this->load->model('Model_place');
}

public function index()
{
   $res_cat['results1']=$this->Model_userprofile->get_app_places();
   $res_cat['results2']=$this->Model_userprofile->get_user_levels();
   $res_cat['place']=$this->Model_place->show_place();
   $this->load->view('admin/V_userprofile',$res_cat);
}

public function changepassword()
{
   //$res_cat['results1']=$this->Model_userprofile->get_app_places();
   //$res_cat['results2']=$this->Model_userprofile->get_user_levels();
    $this->load->view('admin/v_changepassword');
}


public function update_password()
{
 $this->load->library('form_validation');

$this->form_validation->set_rules('oldpass', 'oldpass', 'required');
$this->form_validation->set_rules('newpass', 'newpass', 'required');
$this->form_validation->set_rules('rnewpass', 'rnewpass', 'required');
 
    if($this->form_validation->run())
    {
        
    $this->load->model('Model_userprofile');
    $opass=$this->input->post('oldpass');
    $npass=$this->input->post('newpass');
    $rnpass=$this->input->post('rnewpass');
	
    $uid=$this->session->userdata('userid');
    
    if ($npass==$rnpass)
    {   
		$npass1=md5($npass);
        $usr_dt = array(
                    'password'=>$npass1
        );
  
        $this->Model_userprofile->update_user_pass($uid,$usr_dt);
        $this->session->set_flashdata('cp_message',  'Password Successfully Changed..!');
        redirect('C_userprofile/changepassword');
    }
      else
      {
       $this->session->set_flashdata('cp_message',  '<font color=red>Passwords not match, try again..</font>');
       redirect('C_userprofile/changepassword');
  
      }
     }
        else 

        {
           redirect('C_userprofile/changepassword');
        }                   

   //$this->load->view('admin/V_changepassword');
}

  
public function add_userprofile()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('usrname', 'usrname', 'required');
$this->form_validation->set_rules('usremail', 'usremail', 'required');
$this->form_validation->set_rules('usrphone', 'usrphone', 'required');
$this->form_validation->set_rules('usrgender', 'usrgender', 'required');

$this->form_validation->set_rules('day', 'usrdob', 'required');
$this->form_validation->set_rules('mon', 'usrdob', 'required');
$this->form_validation->set_rules('year', 'usrdob', 'required');

$this->form_validation->set_rules('usrpass', 'usrpass', 'required');
$this->form_validation->set_rules('usrlevel', 'usrlevel', 'required');
$this->form_validation->set_rules('usrstatus', 'usrstatus', 'required');
$this->form_validation->set_rules('usrappid', 'usrappid', 'required');


 if($this->form_validation->run())
        {
       if(isset($_FILES['usrimage']) && $_FILES['usrimage']['size']>0){
        $config['upload_path'] ='uploads/user_images';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|jpe';
       // $image_info = getimagesize($_FILES['subcaticon']['tmp_name']);
       // $image_width=$image_info[0];
       // $image_height=$image_info[1];
        
        $this->load->library('upload', $config);
        // echo $image_width;
        $imgna="usrimage";
            if (!$this->upload->do_upload($imgna))
            {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('usermsg',$config['upload_path'].$this->upload->display_errors());
                $this->load->view("admin/V_userprofile", "<font color=red>File can't uploaded.</font>");
            }
            else
            {
        //$bcatid= $this->input->post('bcatid');
    
    $usrname=$this->input->post('usrname');
    $usremail=$this->input->post('usremail');
    $usrphone=$this->input->post('usrphone');
    $usrgender=$this->input->post('usrgender');
    
    $day1=$this->input->post('day');
    $month1=$this->input->post('mon');
    $year1=$this->input->post('year');
    $udob="$day1/$month1/$year1";
    
    $usrpass=$this->input->post('usrpass');
    $usrrepass=$this->input->post('usrrepass');
    $usrlevel=$this->input->post('usrlevel');
    $usrstatus=$this->input->post('usrstatus');
    $usrappid=$this->input->post('usrappid');
$udate=date('Y-m-d');
      
    if($day=='0' || $month1=='0' || $year1=='0' || $usrappid=='0' || $usrlevel=='0')
    {
    $this->session->set_flashdata('usermsg', '<font color=red>User details missing, try again..!</font>');
        redirect('C_userprofile/index');  
    } 

            
        $data = array('upload_data'=> $this->upload->data());
        $file_name=$data['upload_data']['file_name'];
        $fname=base_url('/uploads/user_images')."/".$data['upload_data']['file_name'];
        
    $userdt = array(
                'name' => $usrname,
		'user_date'=>$udate,
                'image_url'=>$fname,
                'phone' =>$usrphone,
                'email' =>$usremail,
                'gender' => $usrgender,
                'dob' =>$udob,
                'password' =>md5($usrpass),
                'user_level' => $usrlevel,
                'status' => $usrstatus,
                'app_place_id' =>$usrappid
               );
        $this->load->Model('Model_userprofile');
        $this->Model_userprofile->form_insert($userdt);
        $this->session->set_flashdata('usermsg', 'User details successfully added..!');
        redirect('C_userprofile/index');  
        }
      
    }
  }
  else
  {
      $this->session->set_flashdata('usermsg', '<font color=red>Some data is missing, try again..!</font>');
      redirect('C_userprofile/index');  
  }
}

 public function view_all_users()
  {
   $this->load->view('admin/v_all_userprofiles');
  }
        
                
public function view_userprofiles_ajax()
{

        $this->load->model('Model_userprofile');
	$ulevel=$this->session->userdata('userlevel');
	if($ulevel=='1')
	{
        $results=$this->Model_userprofile->view_userprofiles_super();
	}
	else
	{
	$results=$this->Model_userprofile->view_userprofiles();
	}
		
        $data1 = array();
        foreach($results  as $r) 
               {
             $edit="<a href='#myModal2' id='$r->user_profile_id' data-toggle='modal' class='edit open-AddBookDialog2'>Edit</a>";
             $del=anchor('c_userprofile/del_entry/'.$r->user_profile_id, 'Delete', array('id' =>'del_conf'));
             $img="<img src='$r->image_url' class='img-thumbnail' class='img-responsive' alt='Responsive image' style='width:80px; height:82px;' />";
    
		if($r->status==1)
		{
			$st="<font color='green'>Active</font>";
		}
		else
		{
			$st="<font color='red'>Inactive</font>";
		}

if($r->user_level==1 or $r->user_level==2)
{
			array_push($data1, array(
                            "Edit"=>"$edit",
                            "Delete"=>"$del",
			    "Id"=>"<b>$r->user_profile_id</b>",
			"user_date"=>"<b>$r->user_date</b>",
			"user_level"=>"<b>$r->user_level_name</b>",
                            "name"=>"<b>$r->name</b>",
                            "image_url"=>$img,
                            "phone"=>"<b>$r->phone</b>",
                            "email"=>"<b>$r->email</b>",
                            "app_place_id"=>"<b>$r->place_name</b>",
                            "status"=>"<b>$st</b>"
                      ));

}
else
{
                      array_push($data1, array(
                            "Edit"=>"$edit",
                            "Delete"=>"$del",
			    "Id"=>"$r->user_profile_id",
			    "user_date"=>"<b>$r->user_date</b>",
			    "user_level"=>"$r->user_level_name",
                            "name"=>"$r->name",
                            "image_url"=>$img,
                            "phone"=>"$r->phone",
                            "email"=>"$r->email",
                            "app_place_id"=>"$r->place_name",
                            "status"=>$st
                      ));
}
             }
         echo json_encode(array('data' => $data1));
}

    public function edit_userprofile() {
	
    $m="";
    $f="";
    
        $data=$this->session->all_userdata();
        $id=$this->input->post('id');
         // $query = $this->db->select('*')->select('table_business_catogories.business_catogory_id As catid')->from('table_business_catogories')->join('news_image', 'news.news_id=news_image.news_id','inner')->where('news.news_id',$id)->get();
         $query = $this->db->select('*')->from('table_user_profiles')->where('table_user_profiles.user_profile_id',$id)->get();
         $row = $query->row();
     
     if($row->gender=="Male")
     {
       $m="checked";
       $f="";
     }
     else
     {
       $m="";
       $f="checked";
     }
         
echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit User Profile</font></h4>
    </div>
    <br>

           <form  class='form-horizontal' role='form' method='post' action='". site_url('C_userprofile/update_userprofile'). "' enctype='multipart/form-data'>
                    <!-- text input -->
                    
                    <div class='form-group'>
                     <label class='col-md-4 control-label' style='padding-top:10px;'>Name : </label>
                    <div class='col-md-8'>  
          <input type='hidden' name='usrid'  value='$row->user_profile_id'/>
                    <input type='text' name='usrname' class='form-control'  id='mask_date2' value='$row->name'/>
          </div>                   
          </div>
                    

                 <div class='form-group' >
                         <label class='col-md-4 control-label'>Image : </label>
                             <div class='col-md-8'>
                             <div class='fileinput fileinput-new' data-provides='fileinput'>
                             <div class='fileinput-new thumbnail' style='width: 150px; height: 152px;''>
                              <img src='$row->image_url' alt='' /> </div>
                               <div class='fileinput-preview fileinput-exists thumbnail' style='max-width: 150px; max-height: 152px;'> </div>
                             <div>
                              <span class='btn default btn-file'>
                              <span class='fileinput-new'> Select image </span>
                              <span class='fileinput-exists'> Change </span>
                              <input type='file' name='usrimage'> </span>
                              <a href='javascript:;' class='btn default fileinput-exists' data-dismiss='fileinput'> Remove </a>
                         </div>
                        </div>
                      </div>
                    <input type='hidden' name='tmpimage' value='$row->image_url'>
                   </div> 
                                               


           <div class='form-group'>
          <label class='col-md-4  control-label'>Phone : </label>
          <div class='col-md-8'>
          <input type='text' class='form-control'  name='usrphone' value='$row->phone' required/>
            </div>
          </div>
          
          <div class='form-group'>
          <label class='col-md-4  control-label'>Email : </label>
          <div class='col-md-8'>
          <input type='text' class='form-control'  name='usremail' value='$row->email' required/>
          </div>
          </div>
          
          <div class='form-group'>
             <label class='col-md-4 control-label'>Gender </label>
          <div class='col-md-8'>
            <div class='icheck-inline'>
            <label style='padding-left:10px;'>
             <input type='radio' name='usrgender' ".$m."  class='icheck' data-radio='iradio_square-grey' value='Male'>Male</label>
            <label>
            <input type='radio' name='usrgender' ".$f." class='icheck' data-radio='iradio_square-grey' value='Female'>Female</label>
            </div>
          </div>
          </div>
       
           <div class='form-group'>
          <label class='col-md-4  control-label'>Date of birth : </label>
          <div class='col-md-8'>
          <input type='text' class='form-control'  name='usrdob' value='$row->dob' required/>
          </div>
          </div>
                                       
    <!--    <div class='form-group'>
        <label class='col-md-4 control-label' >User Level</label>
        <input type='hidden' class='form-control' name='usrpass' value='$row->password'>
        <div class='col-md-8'>   -->";
		
		//$section = $this->db->select("*")->from("table_user_level")->get();
		//	  $sect=$section->result();
	//		  echo'  <select type="text" name="usrlevel" class="form-control " >';
//			  foreach ($sect as $ro):
 //                 echo '<option value="'.$ro->user_level_id.'"';
   //               if($row->user_level==$ro->user_level_id)echo"selected";
     //             echo' >'.$ro->user_level_name.'</option>';
       //       endforeach;
			  
              echo" <!-- </select>
        </div>
        </div>  -->
        
        <div class='form-group'>
        <label class='col-md-4  control-label'>App place Id/Name : </label>
         <div class='col-md-8'>";
		 
			  $section = $this->db->select("*")->from("app_place_table")->get();
			  $sect=$section->result();
			  echo'  <select type="text" name="usrappid" class="form-control " >';
			  foreach ($sect as $ro):
                  echo '<option value="'.$ro->id.'"';
                  if($row->app_place_id==$ro->id)echo"selected";
                  echo' >'.$ro->place_name.'</option>';
              endforeach;
			  
              echo"</select >
          </div>
         </div>
        <div class='form-group'>
          <label class='col-md-4  control-label' >Status : </label>
          <div class='col-md-8'>		  
		  <select type='text' name='usrstatus' class='form-control'>";
		  
		  if($row->status=="1")
		  {
			  echo "<option value=1 selected>Active</option>";
			  echo "<option value=0>Inactive</option>";
		  }
		  else
		  {
			  echo "<option value=1>Active</option>";
			  echo "<option value=0 selected>Inactive</option>";
		  }
  

              echo"</select >
		  
		  
<!--		   <input type='text' class='form-control'  name='usrstatus' value='$row->status' required/>-->
         </div>
        </div>

                <div class='form-group'> 
                <div class='col-md-4'>
                </div>
                <div class='col-md-8'>
                <input type='submit' class='btn btn-primary' value='Update'/>
                </div>
                </div>                                
           </form> <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div> ";
  
        //Either you can print value or you can send value to database
        //echo json_encode($data);
    }

public function update_userprofile()
{
 $this->load->library('form_validation');

$this->form_validation->set_rules('usrname', 'usrname', 'required');
$this->form_validation->set_rules('usremail', 'usremail', 'required');
$this->form_validation->set_rules('usrphone', 'usrphone', 'required');
$this->form_validation->set_rules('usrgender', 'usrgender', 'required');
$this->form_validation->set_rules('usrdob', 'usrdob', 'required');
//$this->form_validation->set_rules('usrlevel', 'usrlevel', 'required');
$this->form_validation->set_rules('usrstatus', 'usrstatus', 'required');
$this->form_validation->set_rules('usrappid', 'usrappid', 'required');
 
        if($this->form_validation->run())
        {
        $upload1 = $_FILES['usrimage']['name'];
        $fname=$this->input->post('tmpimage');
         
    if(isset($_FILES['usrimage']) && $_FILES['usrimage']['size']!=0)
    {

        $config['upload_path'] = 'uploads/user_images';                        
        $config['allowed_types'] = 'jpg|png|jpeg|gif|jpe';
        // $config['max_size'] = '0'; // 0 = no file size limit
        // $config['file_name']='userfiles';          
        // $config['overwrite'] = false;
        $this->load->library('upload', $config);
    
        $this->upload->do_upload('usrimage');
        $upload_data = $this->upload->data();
        $file_name = $upload_data['file_name'];
        $fname=base_url('/uploads/user_images')."/".$_FILES['usrimage']['name'];
        }
    
        
        $this->load->model('Model_userprofile');
    
        $usrname=$this->input->post('usrname');
    $usremail=$this->input->post('usremail');
    $usrphone=$this->input->post('usrphone');
    $usrgender=$this->input->post('usrgender');
    $usrdob=$this->input->post('usrdob');
    $usrpass=$this->input->post('usrpass');
    //$usrlevel=$this->input->post('usrlevel');
    $usrappid=$this->input->post('usrappid');
    $usrstatus=$this->input->post('usrstatus');
    $uid=$this->input->post('usrid');
    
    
    $usr_newdt = array(
                'image_url'=>$fname,
                'name' => $usrname,
                'phone' =>$usrphone,
                'email' =>$usremail,
                'gender' => $usrgender,
                'dob' =>$usrdob,
                'password'=>$usrpass,
                'status' => $usrstatus,
                'app_place_id' =>$usrappid
               );
  
        $this->Model_userprofile->update_user_profile($uid,$usr_newdt);
        $this->session->set_flashdata('usermsg',  'Data successfully updated..!');
        redirect('C_userprofile/view_all_users');

       }
        else 

        {
           redirect('C_userprofile/edit_userprofile');
        }                   

}

        public function del_entry()
  {
    
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_userprofile');
            $result=$this->Model_userprofile->delete_userprofile($id);
            if($result=1)
            {
                    $this->session->set_flashdata('usermsg', 'User has been removed..!');
                    redirect('C_userprofile/view_all_users');
            }
      else
      {
            $this->session->set_flashdata('usermsg', '<font color=red>Please try again..!</font>');
            redirect('C_userprofile/view_all_users');
           }
  }
}