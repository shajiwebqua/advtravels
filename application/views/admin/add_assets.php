<?php
include('application/views/common/header.php');
?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1><b>Asset Details</b></h1>
		<ol class="breadcrumb" style='font-size:15px;'>
			<li><a id='view-pdf' href="<?php echo base_url('Reports/assets');?>" style='color:#4b88ed;'><button ><i class="fa fa-print" aria-hidden="true"></i> Print </button></a></li>
			
	    </ol> 
         <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
  
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
  <div class="col-md-12">

   <div style="background-color:#fff;border-radius:2px;padding:15px; ">
    
            <div class="portlet-title">
                     <div class="caption" >
							<h4 style='margin-top:0px;'><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Assets</h4>
                     </div>
            </div>
			
  <hr style="background-color:#cecece,border:1px;width:100%;top:10px;margin:0px;padding:3px 0px 10px 0px;">
             
 <div class="portlet-body form">
 <div class='row'>
 <div class="col-md-4">
        <form enctype="multipart/form-data" method="post" id="addassets" name="addassets" action="">
        <div class="form-group">
        <label for="date">Date</label>
        <input type="text" class="form-control" id="datepicker1" name="date" required>
        </div>
        <div class="form-group">
        <label for="asset_name">Asset Name :</label>
        <input type="text" class="form-control" id="asset_name" name="asset_name" required>
        </div>
        
        <div class="form-group">
        <label for="actual_cost">Actual Cost :</label>
        <input type="text" class="form-control" id="actual_cost" name="actual_cost" required>
        </div>

        <div class="form-group">
        <label for="quantity">Quantity :</label>
        <input type="text" class="form-control" id="quantity" name="quantity" required>
        </div>

        <div class="form-group">
        <label for="description">Description :</label>
        <textarea rows='3' name="description" id="description" class="form-control" required></textarea>
        </div>
              <button  class='btn btn-primary' type="submit" style='padding:5px 25px 5px 25px;'>Submit</button>
        </div> 

        <div class="col-md-8">
            <!-- <a href="<?php echo base_url('Asset/assets_pdf');?>" target="_blank" class="form-control btn btn-primary btn-sm" style="width: 10%;margin-bottom: 10px;float: right;">Get pdf</a> -->
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th width="5%">ID</th>
                <th width='15%'>Date</th>
                <th>Asset_Name</th>
                <th>Cost</th>
                <th>Qty</th>
                <th>Description</th>
                <th width="3%">Action</th>
                
            </tr>
        </thead>
        
        <tbody>
        <?php $i=1;foreach($result as $row){?>
            <tr>
                <td><?php echo $row->asset_id;?></td>
                <th><?php echo date('d-m-Y',strtotime($row->asset_date));?></th>
                <th><?php echo $row->asset_name;?></th>
                <td><?php echo $row->asset_cost?></td>
                <td><?php echo $row->asset_qty?></td>
                <td><?php echo $row->asset_description?></td>
                <td><div class="btn-group">
				
				<a href="#" onclick="edit_assets('<?php echo $row->asset_id;?>')" data-toggle="modal" class="edit "><button class="btn btn-primary btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>
				<a href="#" onclick="delete_assets('<?php echo $row->asset_id;?>')" data-toggle="modal" class="delete "><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>
			
				</div>
				   
				</td>
                
            </tr>
         <?php $i++;} ?>   
        </tbody>
    </table> 
  </div>
  </div>
   
  </div> <!--portlet-body-->
 
  </div>
  </div>    
<br>  
  </div>
  </div>
</section>


<div class="modal fade" id="edit_myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content" id="result">
      
    </div>
  </div>
</div>


<?php include('application/views/common/footer.php');?>
  </div>

<script type="text/javascript">
$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
  
  $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
 
$('#example').DataTable();

$(document).on("submit", "#addassets", function (event) {
          
    event.preventDefault();
    var date =  $('#datepicker1').val();
    var asset_name =  $('#asset_name').val();
    var actual_cost =  $('#actual_cost').val();
    var quantity =  $('#quantity').val();
    var description =  $('#description').val();

    $.ajax({
            url: "<?php echo base_url();?>Asset/add_assets",
            type: 'POST',
            data: {date:date,asset_name:asset_name,actual_cost:actual_cost,quantity:quantity,description:description},
            
            success: function (data, status) {
           
              if(data="success")
              {
                swal({
					  title: "Saved.",
					  text: "Asset details successfully added.!",
					  type: "success",
					  showCancelButton: false,
					  confirmButtonClass: "btn-success",
					  confirmButtonText: " Ok ",
					  closeOnConfirm: false
					},
					function(){
					   window.location.reload();
					});
                }
              else
              {
                swal("Error", "Something went wrong", "error");    
              }  
            }
          });
});


function delete_assets(id)
{
  swal({
        title: "Are you sure?",
        text: "You will not be able to recover",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: true,
        closeOnCancel: true 
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
        type: "post",
        url: "<?php echo base_url();?>Asset/delete_assets?id="+id,
        success: function(data){ 

        if(data="success")
        {
          window.location.reload();
        }
        else
        {
          swal("Error", "Something went wrong", "error");    
        }  
        }
        });
      }
      else
      {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    }
    );
}

function edit_assets(id)
{
  $.ajax({
  type: "post",
  url: "<?php echo base_url();?>Asset/edit_assets?id="+id,
    success: function(data){ 
      $('#edit_myModal2').modal('show');
      $('#result').html(data);
    }
  });
}


$(document).on("submit", "#editAssets", function (event) {
          
    event.preventDefault();
    var date =  $('#edit_date').val();
    var asset_name =  $('#edit_asset_name').val();
    var actual_cost =  $('#edit_actual_cost').val();
    var quantity =  $('#edit_quantity').val();
    var description =  $('#edit_description').val();
    var id = $('#edit_assets_id').val();

    $.ajax({
            url: "<?php echo base_url();?>Asset/update_assets",
            type: 'POST',
            data: {id:id,date:date,asset_name:asset_name,actual_cost:actual_cost,quantity:quantity,description:description},
            success: function (data, status) {
           
              if(data="success")
              {
			  swal({
					  title: "Updated.",
					  text: "Details successfully updated.!",
					  type: "success",
					  showCancelButton: false,
					  confirmButtonClass: "btn-success",
					  confirmButtonText: " Ok ",
					  closeOnConfirm: false
					},
					function(){
					   window.location.reload();
					});
              }
              else
              {
                swal("Error", "Something went wrong", "error");    
              }  
            }
          });
});

$("#view-pdf").click(function()
{
		 var pdf_link = $(this).attr('href');
         var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
            $.createModal({
				title:'Asset Details',
				message: iframe,
				closeButton:true,
				scrollable: true,
			});
			return false; 

});	




</script>


  </body>
</html>
