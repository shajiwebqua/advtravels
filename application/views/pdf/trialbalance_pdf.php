<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
<title>Travels</title>

 <style type="text/css">
 td{padding:2px 5px 3px 5px; }
 table{font-size:14px;}
 </style>
  
</head>

<body>
<!-- heading ----------------------------------------------------->
<br>
<table style="border:none;width:100%;margin-top:20px;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
<td width="150px" style="padding: 0px" align='right'>
<?php $date = date('d/m/Y');?>
Date: <?=$date;?>
  </td>
</tr>

<tr>
<td width="150px" style="padding: 0px">
<Strong>Advance World Holidays</strong><br>
PK & Sons Complex, East Moozhikkal<br>
NH 212, Calicut, Kerala, India-673571<br>
Tel: 0091 495 2732 500, 0091 495 3100 700<br>
Fax: 0091 495 2732 400
</td>
<td width="150px" style="padding: 0px" align='right'>

</td>
</tr>
</table>
<!-- heading end----------------------------------------------------->
<hr>

<div class="box-body" style='min-height:450px;'>

        <div class="row">
	        <div class="col-md-12" style='padding-right:5px;'>
				<table class="table table-hover table-bordered" id="example"  width='100%' border=0 style="font-size:14px">
             <thead>
			 
			  <tr>
				 <td colspan=4 height='35px' align='center'><h3>Trial Balance</h3></td>
				 </tr>

				 <tr>
				 <td colspan=4> <?php echo $this->session->userdata('acperiod2');?></td>
				 </tr>
				 <tr>
				 <td colspan=4><hr></td>
				 </tr>
				 
                <tr height='30px'>
				<th width='20px'></th> 
				<th align='left'>Particulars</th> 
				<th style='width:120px;text-align:right;'>Debit</th>
                <th style='width:120px;text-align:right;'>Credit</th> 
                </tr>
                </thead>
				<tbody>
				<?php
				$drtot=0;
				$crtot=0;

				foreach ($ledgervalues as $pr1)
				{
					
				if($pr1->debit!=0 and $pr1->credit!=0)
					{
						if($pr1->debit>$pr1->credit)
						 {
							 $deb=$pr1->debit-$pr1->credit;
							 $cre=0;
						 }
						 else
						 {
							 $cre=$pr1->credit-$pr1->debit;
							 $deb=0;
						 }
					}
					else
					{
						$deb=$pr1->debit;
						$cre=$pr1->credit;
					}
					
					$drtot+=$deb;
					$crtot+=$cre;
					if($deb!=0 or $cre!=0)
					{	
				?>
				<tr>
				<td width='20px'></td> 
				<td><?=ucwords(strtolower($pr1->ledg_name));?></th> 
				<td style='width:120px;text-align:right;'><?=number_format($deb,2,".","");?></td>
                <td style='width:120px;text-align:right;'><?=number_format($cre,2,".","");?></td> 
                </tr>
				<?php
				}
				}
				$diff=$crtot-$drtot;
				if($diff<0)
				{
					$crtot+=number_format(abs($diff),"2",".","");
				?>
				<tr>
				<td width='20px'></td> 
				<td>Difference </th> 
				<td style='width:120px;text-align:right;'>0.00</td>
                <td style='width:120px;text-align:right;'><?=number_format(abs($diff),2,".","");?></td> 
                </tr>
				<?php
				}
				else
				{
					$drtot+=abs(number_format($diff,"2",".",""));
				?>
				<tr>
				<td width='20px'></td> 
				<td>Difference</th> 
				<td style='width:120px;text-align:right;'><?=abs(number_format($diff,2,".",""));?></td>
                <td style='width:120px;text-align:right;'>0.00</td> 
                </tr>
				<?php
				}
				?>
				<tr ><td colspan=4><hr style='margin:2px;'></td></tr>
				<!----1st- totals----------------------------------->
				<tr >
				<td width='20px'></td> 
				<td style='font-weight:700;font-size:13px;color:#000;'><b>TOTAL</b></td> 
				<td class='td-style' align='right'><b><?=number_format($drtot,2,".","");?></b></td>
                <td class='td-style' align='right'><b><?=number_format($crtot,2,".","");?></b></td> 
                </tr>
				
				<tr ><td colspan=4><hr style='margin:2px;'></td></tr>
				
				<!---- totals---ends----------------------------------->
			
				</tbody> 
				</table>
			  </div>
        </div>

       </div>

</body>
</html>