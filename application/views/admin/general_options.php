<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>General Options</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px'>
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->
              
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
   <div class="page-content">
   
   <div style="padding:0px 15px 0px 15px;">
				 <div id="idmsg"  style="background-color:#fff;height:25px;margin-bottom:5px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div>
				 <div id='msgerr'><?php echo $this->session->flashdata('message1'); ?></div>
				 </center></div>
	</div>
			
      <div class="col-md-6">
        <div style="background-color:#fff;padding:10px; padding-bottom:30px;">
					   
                        <!-- <div class="row"> -->
        <div class="col-md-12">
			   <h5 style='padding-bottom:0px;margin-bottom:2px;'><b>GST</b></h5>
			   <hr style='margin:0px;'>
          </div>
             <!-- <label style="background-color:#cecece;height:1px;width:100%"></label> -->
                   <!-- </div>/.box-header -->
			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/update_GST')?>" enctype="multipart/form-data">
                  
				  <div class="form-group" style='padding-left:15px;'>
	          		 <div class='row'>
						<div class='col-md-12'>
							<label style='margin:3px 0px 5px 25px;'>To set the corporate client GST %, type value and click update.</label>
						</div>
					 </div>
					 	   <label class="col-md-2 control-label">GST %</label>
                        <div class="col-md-3">
						<?php
						$gst=$this->db->select('gen_gst')->from('general')->get()->row();
						?>
                            <input type="text" class="form-control"  name="gstper" value='<?=$gst->gen_gst;?>' required>
                        </div>
						<div class="col-md-3">
                            <input type="submit" class="form-control"  style='background-color:#f7d0d0;' name="btngst" value='Update'>
                        </div>
                      </div>
                </form>

                </div>
              </div>
			  

          <div class="col-md-6">
             <!-- <div style="background-color:#fff;padding:10px; ">
                        
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                            <div class="col-md-12">
							</div>
							</div>
						</div>
					</div>
                    </div>
                </div>  -->
          </div>
	</div>
  </div>
            <!-- END QUICK SIDEBAR -->
 </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
         
  <!-- End user details -->
  </div>
  
   <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 

</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal("Saved",$("#msg").html(),"success")
		$("#msg").html("");
	}


if($("#msgerr").html()!="")
	{
		swal("Try Again",$("#msgerr").html(),"error")
		$("#msgerr").html("");
	}
	
	
//-------------------------------------
 
      $('#example').dataTable( {
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/country_ajax",// json datasource
               },
	"columnDefs":[
	{"width":"7%","targets":0},
	{"width":"7%","targets":1},
	{"width":"9%","targets":3},
	],
        
        "columns": [
            { "data": "edit" },
            { "data": "delete" },
            { "data": "country"},
            { "data": "status" },
      
       ]
  } );



      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/edit_country",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 


    
    
    
    
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#msg').html(''); }, 3000);
		

 //});
  


</script>



