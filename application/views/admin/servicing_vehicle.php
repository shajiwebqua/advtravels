<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
 <!-- Main content -->
 <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Servicing Vehicles</b> </h1>
    <ol class="breadcrumb">
	<li> <a href="<?php echo site_url('Vehicle/service_vehicles');?>" style="color:#4b88ed;font-size:15px;"> <i class="fa fa-wrench" aria-hidden="true"></i>Service Required Vehicles</a></li>
	 <li> <a href="" style="color:#4b88ed;font-size:15px;"> <i class="fa fa-wrench" aria-hidden="true"></i>Service History</a></li>
<!--<li><a href="<?php echo base_url('Attached_vehicle/get_add_vehicle');?>" data-target="#myModal2" data-toggle='modal' style="color:blue;"><h5><b>Add New Vehicle</b></h5></a></li>-->
	</ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 
	  
	<section class="content"> 
    <!-- Small boxes (Stat box) -->
	<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
			
    <div class="row">
	<div class="col-md-12">

	<!------------------------------------ Add new user details ----------------------------------->
	
	<div style="background-color:#fff;padding:15px; ">
	<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
                   <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
					<table class="table table-striped table-hover table-bordered" id="example" >
                        <thead>
								<tr>
								 <td>ID</td>
								 <td>Register No</td>
								 <td>Vehicle Type</td>
								 <td>Killometers</td>
								 <td>Status</td>
								 <td>Set to Service</td> 
								 <td>Return Vehicle</td> 
								 </tr>
								</thead>
                        </table>

						
                        </div>
                        <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
          <!-- END CONTENT BODY -->
          </div>
          </div>
            <!-- END QUICK SIDEBAR -->
        </div>
			<div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body" >
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Edit</h4>
						</div>
					</div>
					</div>
				<!-- /.modal-dialog -->
				</div>
            </div>							
	</div>
	<!------------------------------------ End user details ----------------------------------->
	</div>
	</div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
    <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------
 setTimeout(function(){ $("#msg").html(""); }, 3000);
 
       $('#example').dataTable( {
		 "ordering":false,
		 "bInfo" : false,
		 "destroy": true,
        "processing": true,
		"pageLength": 5,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Vehicle/periodic_service_ajax/2",// json datasource
                },
		"columnDefs":[
		{"width":"12%","targets":5},
		{"width":"12%","targets":6},
		],	   
        "columns": [
            { "data": "sid"},
            { "data": "regno" },
			{ "data": "vtype" },
			{ "data": "kilometer" },
			{ "data": "status" },
			{ "data": "service" },
			{ "data": "vreturn" },
      ]
  } );
  
  var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });			   

      $('#example tbody').on('click', '.edit1', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Vehicle/Set_toService",
        dataType: 'html',
        data: {vid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
        }); 

$('#example tbody').on('click', '.edit2', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Vehicle/returnvehicle",
        dataType: 'html',
        data: {vid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
        }); 
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });



 //});
  


</script>



