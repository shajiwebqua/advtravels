<?php
class Model_staff extends CI_Model{
	
    public function form_insert($userdt)
    {
       $this->db->insert('staffregistration', $userdt);  
    }

    public function view_staff(){

    $this->db->select('*');
    $this->db->from('staffregistration');
    $this->db->join('profession', 'profession.profession_id=staffregistration.staff_profession','inner');
    $this->db->order_by("staff_id", "asc");
    $query = $this->db->get();
    return $query->result();
}

 public function view_staffid($id){

    $this->db->select('*');
    $this->db->from('staffregistration');
    $this->db->where('staff_id', $id);
    $query = $this->db->get();
    return $query->result();
}

 public function view_code($code){

    $this->db->select('*');
    $this->db->from('staffregistration');
     $this->db->where('staff_id', $code);
    $query = $this->db->get();
    return $query->result();
}

public function view_staffPDF($id){

    $this->db->select('*');
    $this->db->from('staffregistration');
   $this->db->join('role', 'role.role_id=staffregistration.staff_role','inner');
      $this->db->join('profession', 'profession.profession_id=staffregistration.staff_profession','inner');
    //$this->db->order_by("staff_status", "desc");
     $this->db->where('staff_id', $id);
    $query = $this->db->get();
    return $query->row();
}
public function view_stafflistPDF(){

    $this->db->select('*');
    $this->db->from('staffregistration');
   $this->db->join('role', 'role.role_id=staffregistration.staff_role','inner');
      $this->db->join('profession', 'profession.profession_id=staffregistration.staff_profession','inner');
    //$this->db->order_by("staff_status", "desc");
     //$this->db->where('staff_id', $id);
    $query = $this->db->get();
    return $query->result();
}


public function update_staff_profile($id,$newdt)
  {
    $this->db->where('staff_id', $id);
    $this->db->update('staffregistration', $newdt); 
  } 
public function update_staff_pass($uid,$usr_dt)
  {
    $this->db->where('staff_id', $uid);
    $this->db->update('staffregistration', $usr_dt); 
  } 

public function delete_staff($id)
{
    $this->db->where('staff_id', $id);
    $this->db->delete('staffregistration');
    return $this->db->affected_rows();
}

public function select($column,$where = null)
{
    $this->db->select($column);
    $this->db->from('staffregistration');
    if($where != null)
        $this->db->where($where);
    
    $query=$this->db->get();
    return $query->result();
}

public function view_menucategory(){

    $this->db->select('*');
    $this->db->from('menu_category');
    $query = $this->db->get();
    return $query->result();
}


public function view_menuoptions($cat)
{
    $this->db->select('*');
    $this->db->from('menus');
    $this->db->where('menus.menu_category',$cat);
    $query = $this->db->get();
    return $query->result();
}


public function get_menuoptions($where)
{

    $this->db->select('*');
    $this->db->from('menus');
    $this->db->where($where);
    $query = $this->db->get();
    return $query->result();
}

public function insert_user_menus($values)
{
   $this->db->insert('menu_user', $values);  
}

public function get_user_menus($where)
{

    $this->db->select('menu_options');
    $this->db->from('menu_user');
    $this->db->where($where);
    $query = $this->db->get();
    return $query->result();
}

public function delete_menu_user($where)
{
    $this->db->where($where);
    $this->db->delete('menu_user');
    return $this->db->affected_rows();
}

public function get_menus($where){
 $this->db->select('menu_options');
 $this->db->where($where);
 $this->db->order_by("menu_options","asc");
 $this->db->from('menu_user');
 $query = $this->db->get();
 return $query->result();   
}


}
