<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function checkMenu($menuCategory, $menu){
	$CI =& get_instance();
	
	$user_role = $CI->session->userdata('user_role');
	if($user_role == "0" || $user_role == "1"){
		return true;
	}
	
	$CI->load->model('Model_staff');
	$CI->load->model('Model_menu');

	$CI->db->select("menu_cat_id");
	$CI->db->from("menu_category");
	$CI->db->where("menu_cat_name", $menuCategory);
	$query = $CI->db->get();
	$result = $query->result();
	$menu_cat_id = $result[0]->menu_cat_id;
	
	$CI->db->select("menu_id");
	$CI->db->from("menus");
	$where = array(
			"menu_category" => $menu_cat_id,
			"menu_name" => $menu
	);
	$CI->db->where($where);
	$query = $CI->db->get();
	$result = $query->result();
	$menu_id = $result[0]->menu_id;

	$staff_id = $CI->session->userdata('usercode');
	$where = array(
		"menu_options" => $menu_id,
		"staff_id" => $staff_id
	);

	$CI->db->where($where);
    $query = $CI->db->get('menu_user');
    if ($query->num_rows() > 0){
        return true;
    }
    else{
        return false;
    }

} 


function checkisAdmin(){
	$CI =& get_instance();	
	$user_role = $CI->session->userdata('user_role');
	if($user_role == "0" || $user_role == "1"){
		return true;
	}

	return false;
}