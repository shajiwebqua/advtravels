<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
td{
	padding:5px 10px 5px 10px;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

 <table width="100%"><tr><td><h3><u><?php echo $dcap;?></u></h3></td><td align='right' >Date : <?php echo date('d-m-Y');?></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
 
 		<div class='row'>
				<div class='col-md-8'>
				
		<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style='font-size:14px !important;'>
        <thead>
            <tr>
			   
                <th width="5%">ID</th>
                <th align='left'>Client</th>
                <th align='left'>Feedback</th>
                <th align='left'>Action_Taken</th>
                               
            </tr>
        </thead>
        
        <tbody>
		<tr><td colspan=4><hr style='margin:0px;'></td></tr>
        <?php $i=1;foreach($eva_result as $row){
			?>
            <tr>
				<td><?=$i;?></td>
                <td><B>Client:&nbsp;</b><?php if($row->se_service_type==1)echo "INDIVIDUAL"; else echo "CORPORATE";?>
				<br><b>Guest:&nbsp; </b><?=$row->se_guest_name?><br><b>Mob:&nbsp; </b><?=$row->se_contact_no?>
				<br><b>Date:&nbsp;</b><?=date_format(date_create($row->se_date),'d-m-Y')?>
				<br><b>Vehicle:&nbsp;</b><?=  $row->vehicle_regno?><br><b>Chauffeur:&nbsp; </b><?=$row->driver_name?>
				</td>
                <td><?=$row->se_feedback?></td>
                <td><?=$row->se_actiontaken?></td>
            </tr>
			<tr><td colspan=4><hr style='margin:0px;'></td></tr>
         <?php $i++;} ?>   
        </tbody>
    </table>

			</div>
		</div>
</body>
</html>