<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rupees extends CI_Controller 
{
    public $ones=array();
	public $tens=array();
	public $others=array();
	
    function __construct()
	{
		parent::__construct();
	}
	
	public function Words($num)
	{
		$ones=array(1=>"One",
		2=>" Two",
		3=>" Three",
		4=>" Four",
		5=>" Five",
		6=>" Six",
		7=>" Seven",
		8=>" Eight",
		9=>" Nine",
		10=>" Ten",
		11=>" Eleven",
		12=>" Twelve",
		13=>" Thirteen",
		14=>" Fourteen",
		15=>" Fifteen",
		16=>" Sixteen",
		17=>" Seventeen",
		18=>" Eighteen",
		19=>" Nineteen",
		);
		
		$tens=array(2=>" Twenty",
		3=>" Thirty",
		4=>" Fourty",
		5=>" Fifty",
		6=>" Sixty",
		7=>" Seventy",
		8=>" Eighty",
		9=>" Ninety",
	    );
		
		$others=>array(1=>" crore",
		2=>" Lakh",
		3=>" Thousend"
		);
		
		$d1=0;$d2=0;$rs="",$i;
		
		$numstr = number_format($num,'2','.','');
		
		for($i=0;$i<=3;$i++)
		{
			$d1 = substr($numstr, i * 2 - 1, 1))
            $d2 = substr($numstr, i * 2, 1))
            If($d1 > 0 Or $d2 > 0)
			{
                $rs .=$this->twodigit($d1, $d2). $others[$i];
		    }
		}
		
		$d1 = substr($numstr, 7, 1);
        If($d1 > 0)
		{
            $rs.= $ones($d1) . " Hundred";
		}
        
        $d1 = substr($numstr, 8, 1);
        $d2 = substr($numstr, 9, 1);
		
        if ($d1 > 0 Or $d2 > 0)
		{
            if($rs !=""){
				$rs = $rs. " and" . twodigit($d1, $d2);}
            else{
				$rs = $this->twodigit($d1, $d2);}
		}
        
		// Check No Rupee........
		
        if($rs!=""){
            $rs = "Rupees " . $rs;
		}
        
        $d1 = substr($numstr, 11, 1);
        $d2 = substr($numstr, 12, 1);
        
		if($d1 > 0 Or $d2 > 0){
            if($rs!="")	{
				$rs = $rs ." and" . twodigit($d1, $d2)." Paise";}
            else{
				$rs = $this->twodigit($d1, $d2) . " Paise";}
		}

        if ($rs!="")
		{ $words = $rs . " Only";}
        else
		{$words = "Nil";}
        
		return $words;
    }
	

    public function twodigit($d1 ,$d2)
	{
       $d12=0;
        $tdigit = "";
        $d12 = $d1 * 10 + $d2;
        if ($d12 > 0)
		{
            if ($d12 < 20)
                4tdigit = $ones($d12);
            else
                $tdigit = $tens($d1) . ((d2 = 0)?"":$ones($d2);)
	    }
		return $tdigit;
        
	}
	
}
?>