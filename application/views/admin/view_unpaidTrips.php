<?php
 include('application/views/common/header.php');?>
<style>
.colpad
{
	padding-left:3px;
	padding-right:3px;
}

.numericCol
{
	text-align:right;
}

.chkCol
{
	text-align:center;
}


/*create invoice box style */
.inv-div
{
	position :fixed;
	top:0;
	left:0px;
	width:100%;
    height:100%;
background-color:;
z-index:9999;
display:none;	
}
.inv-div .content
 {
   border:2px solid #416796;
   position :absolute;
   top:0px;
   right:10px;
   width:500px;
   height:600px;
   background-color:#fff;
   z-index:9999;
   padding:0px;
  
}

.inv-div.open-div
{
	display:block;

}

.head-row
{
   background-color:#416796;
   color:#fff;
   padding:5px 2px 5px 2px;
   margin-left:0px;
   margin-right:0px;
}
.dt-row
{
   padding:5px 2px 5px 10px;
   margin-left:0px;
   margin-right:0px;
}

.p-col
{
	padding-left:2px;
	padding-right:0px;
}

.p-btn{
	padding:5px 15px 5px 15px;
}
/*---------------------------- */
</style>

<?php 
$cres=$this->db->select('customer_id,customer_name')->from('customers')->get()->result();
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 

  <!-- Main content -->
  <!-- Content Header (Page header) -->
  
  <section class="content-header">
  <h1><b>Create Invoice (<span style='font-size:15px;color:red'>Unpaid trips</span>)</b></h1>
  <ol class="breadcrumb" style="font-size:15px;">
     <li> <a  href="" id='btncinvoice' data-target="" data-toggle='modal' style="color:#fff;"> <button  class='btn btn-warning' > <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Create Invoice</button></a></li>
	 <li><a  href="<?php echo base_url('Invoice/Payments');?>" style="color:#fff;"><button class='btn btn-info' > <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Record Payments </a></button></li> 
  </ol>
  </section>
  
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->   
    
  <section class="content"> 
  <!-- <div style="color:#4b88ed;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , And click on the button (Print,Share,Edit,View,Delete)  </div> -->
    <div style="padding:2px 0px 2px 0px;">
		 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
		 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div>
		 <div id='mserr'><?php echo $this->session->flashdata('message1'); ?></div>
		 </center>
		 </div>
	</div>
				
  <div class="row">
  
<!-- create invoice box ------------------------------------------------------>
	  <div class="col-md-12 inv-div">
		<div class="content" >
			<div class='row head-row' >
			<div class='col-md-8' style='padding-left:2px;font-size:18px;'>Create Invoice</div>
				<div class='col-md-4' style='text-align:right;padding-right:2px;'>
					<button class="close-btn btn btn-default btn-xs"  >X</button>
				</div>
			</div>
			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Invoice/Create')?>" enctype="multipart/form-data">
			<div id="inputform"  style="overflow-y:auto !important;height:560px;overflow-x:hidden;">

			
			</div>	
			</form>
		 
		</div>
	  </div>
<!-- create invoice box end------------------------------------------------------>
	    
<div class="col-md-12">
<div class="portlet-body">
<div style="background-color:#fff;padding:15px; ">
              
		<div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">

			<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->

		<!-- search options -------------------------------------------->

	       <div class='row' style='margin:0px;background-color:#e4e4e4;padding:5px;'>

			<!--<form role="form" method="POST" action="<?php echo base_url('Invoice/Trips/1')?>" enctype="multipart/form-data">-->
		
			<div class='col-md-2 ' style='padding-top:5px;'>	<label class='control-label'>Select Date [from-to] : </label>	</div>
			
			<div class='col-md-3 ' style='padding-top:2px;'>
				<select class="form-control select2" id="cltype" name="cltype" required>
					<option value="">--select client---</option>
						<?php
						foreach($cres as $cs)
						{
							echo "<option value='".$cs->customer_id."'>".$cs->customer_name."</option>";
						}
					?>
				</select>
			</div>
			<div class='col-md-2 ' style='padding-top:2px;'><input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-2 ' style='padding-top:2px;'><input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			<div class='col-md-1 ' style='margin-top:2px;'><input type="button" class="form-control btn btn-success" style="text-align:center;"  value="Get" name='btnget' id="btnget"> </div>
			<!--<div class="col-md-3" style="margin-top:2px;border-left:2px solid red;">
				<input type="submit" class="btn btn-warning" style="text-align:center;padding-top:4px;padding-bottom:4px;border-radius:0px;"  value="Cancelled List" name='btncancel' id="btncancel">
				<input type="submit" class="btn btn-primary" style="text-align:center;padding-top:4px;padding-bottom:4px;border-radius:0px;"  value="Closed List" name='btnclose' id="btnclose">
			</div> -->
		<!--</form>-->

		</div>
	
	<!-- search options -------------------------------------------->

			
			<div class="row" style='margin-top:10px;'>
                   <div class="col-md-12">
				   <label id="btitle" style='font-weight:bold;margin-bottom:20px;'><font color=#19a895><u>Unpaid Trips List</u></font></label>
				   </div>
				   </div>
		
            <div class="row" id='booklist'>
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
				   <div class="table-toolbar">
					<div class="row">
					<div class="col-md-12">
                  
                   </div>
                   </div>
                   </div>
				   
             
            <table class="table table-striped table-hover table-bordered" id="example" width='100%'>
              <thead>
                <tr>
				<th style='text-align:center;padding-left:20px;'><input type="checkbox" id="master" style='width:17px;height:17px;'></th>
                 <th>Trip_ID</th>
                 <th>Date</th> 
				 <th width='25%'>Customer</th>
				 <th>Amount</th>
				 <th>GST%</th>
				 <th>GST</th>
				 <th>Total</th>
				 
                </tr>
                </thead>
				<tbody>
			
				</tbody>
            </table>

        </div>
                       <!-- END BORDERED TABLE PORTLET-->
		</div>
		</div>
	
                <!-- END CONTENT BODY -->
	</div>
	</div>
            <!-- END QUICK SIDEBAR -->
</div>
  
	 <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit</h4>
				</div>
			</div>
			</div>
		<!-- /.modal-dialog -->
		</div>
	 </div> 
		 
					 
  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
<!-- /.content --> 
</section>
  <!-- /.content-wrapper -->
</div>
 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">


var table="";
$(".select2").select2();

setInterval(function(){ $('#idh5').html(''); }, 3000);

$("#idmsg").hide();
$("#msgerr").hide();

//sweet alert ----------------------------
	if($.trim($("#idh5").html())!="")
	{
		swal("",$("#idh5").html(),"success")
		$("#idh5").html("");
	}
	
	if($.trim($("#msgerr").html())!="")
	{
		swal("",$("#idh5").html(),"error")
		$("#idh5").html("");
	}
	
// sweet alert box -------------------------- 

 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
 
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   }); 
   
  //for invoice box ---------------
$('#dtpicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
 
 $('#dtpicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   }); 
 $('#dtpicker3').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });     
 //-------------------------------
  
 get_datatable_trips("","","");
 
 $('#btnget').click(function()
 {
	var dt1=$("#datepicker1").val();
	var dt2=$("#datepicker2").val();
	var cl=$("#cltype").val(); 
	get_datatable_trips(cl,dt1,dt2);
	
 });


function get_datatable_trips(cl,dt1,dt2)
{
	
    table=$('#example').dataTable({
    "ordering":false,
    "destroy": true,
    "processing": true,
	
   
	"ajax": {
			url :"<?php echo base_url(); ?>" + "Invoice/unpaid_trip_ajax/"+cl+"/"+dt1+"/"+dt2,// json datasource
		   },
	   "aoColumnDefs": [
	   { "sClass": "chkCol", "aTargets": [0] },
      { "sClass": "numericCol", "aTargets": [4,5,6,7] }
		],	

	   "columns": [
			{"data":"check"},
            { "data": "uptripid"},
            { "data": "update"},
            { "data": "upcust" },
	        { "data": "upamt" },
            { "data": "upgstper" },
            { "data": "upgst" },
			{ "data": "total" },
            
       ]
  });
}
  
 jQuery('#master').on('click', function(e) {
	 if($(this).is(':checked',true))  
	 {
		 $(".sub_chk").prop('checked', true);  
		 $(".sub_chk").closest('tr').toggleClass('selected');
	 }  
	 else  
	 {  
		 $(".sub_chk").prop('checked',false);  
		 $(".sub_chk").closest('tr').removeClass('selected');
	 }  
 });

  
  $('#example tbody').on( 'click', '.sub_chk', function ()
  {
        
		$(this).closest('tr').toggleClass('selected');
  });  
	
/*  $('#example tbody').on( 'click', '.sub_chk', function ()
  {
        $(this).toggleClass('selected');
		if ($(this).hasClass('selected') ) {
             $(this).closest('tr.selected').find('.sub_chk').prop('checked',true);
         }
         else {
			$(this).closest('tr').find('.sub_chk').prop('checked',false);
         }
  });*/
   
//get create invoice box---------------------


$("#btncinvoice").click(function(){
	
$("#inputform").empty();
	
var tid="";
var amt=0;
    $.each($("#example tr.selected"),function(){ 
        tid+=","+$(this).find('td').eq(1).text(); 
    });
    $("#trip_ids").val(tid);
	if($.trim(tid).length<=0)
		{
			swal("Cancelled", "Please select trips.","");
		}
		else
		{
		var myurl="<?php echo base_url(); ?>" + "Invoice/invoice_form";	
		var Result=$("#inputform");
        jQuery.ajax({
        type: "POST",
        url: myurl,
        dataType: 'html',
		data:{trids:tid},
        success: function(res) 
		{
        Result.html(res);
        }
     });

   $(".inv-div").addClass('open-div');
   
		}
		/*
	
	var tid="";
    $.each($("#example tr.selected"),function(){ 
        tid+=","+$(this).find('td').eq(1).text(); 
    });
    $("#trip_ids").val(tid);
	if($.trim(tid).length<=0)
		{
			swal("Cancelled", "Please select unpaid trips.","");
		}
		else
		{
		$(".inv-div").addClass('open-div');
		}*/
		
		
});

$(".close-btn").click(function(){
    $(".inv-div").removeClass('open-div');
});

//-------------------------------------------
  
   $('#idsave').on('click',function(){
       //var id=$('#example').find('tr.selected').find('td').eq(2).text();
          var dt1=$('#datepicker1').val();
          var m=$('#lmonth').val();
       var pdf_link = $(this).attr('href')+'/'+m+'/'+dt1;
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
		window.open(pdf_link);
      return false; 
    
   }); 


  
   $('#view-pdf').on('click',function(){
     
		var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        $.createModal({
        title:'Booking List',
        message: iframe,
        closeButton:true,
        scrollable: true,
   });
       
	return false; 

   });  
  	
// -----------------------------------------------------------------------------------------------------------------------------------    
    
  $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this booking?');
   });

		
  $(document).on("click", "#del_conf1", function () {
           return confirm('Are you sure you want to Cancel this booking?');
  });

		
</script>



