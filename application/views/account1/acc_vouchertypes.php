<?php include('application/views/common/header.php'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	 <h1 class='heading' style='color:#00adee;padding-bottom:15px; font-weight:bold;'>Voucher Types
           </h1>
      <ol class="breadcrumb" style="margin-right:20px;font-size:13px;">
        <li><a href="#"></a></li>
      </ol>
	  
    </section>

    <!-- Main content -->
    <section class="content">
   
      <div class="row">
        <div class="col-md-5">
          <div class="box" style="border:1px solid #f5f3f3;">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Create Voucher Type</h3> 
				<center><input type='hidden' id="msg" value='<?php echo $this->session->flashdata('message'); ?>'></CENTER>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="row">
	        <div class="col-md-12">
				
				 <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Account/save_vtype')?>" enctype="multipart/form-data">
						
						<div class="form-group" style='margin-top:20px;'>
						   <div class="row">
							  <label class="col-md-4 control-label" style='padding-top:3px;'>Voucher Name :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="vname" required>
								</div>
							</div>
						</div>
				
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Type of Voucher :</label>
								<div class="col-md-7">
									<select  class="form-control" name='vtype'  id='vtype' required>
									  <option value="">---type----</option>
									  <?php
										$gquery=$this->db->select("*")->from('acc_primary_vtypes')->get()->result();
										foreach( $gquery as $g)
										{
											echo "<option value='".$g->acc_pvtypeid."'>".$g->acc_pvtype."</option>";
										}
									  ?>
														  
									</select>
								</div>
							</div>
						</div>
									
						<div class="form-group" style='margin-top:20px;margin-bottom:30px;'>
						   <div class="row">
							  <label class="col-md-4 control-label"></label>
								<div class="col-md-7">
									<input type="submit" class="btn btn-primary "  name="btnsubmit" value="Save Voucher Type">
								</div>
							</div>
						</div>
						
				</form>
			 </div>
             </div>
             </div>
    </div>
   </div>
   <!-- end create group---->
   <!-- group list ---->
    <div class="col-md-7">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Voucher Types</h3> 
				
            </div>
            <!-- /.box-header -->
            <div class="box-body" style='min-height:450px;'>
              <div class="row">
	            <div class="col-md-12">
				
				<table class="table table-hover table-bordered" id="example" width='100%' border=0 style="font-size:14px">
                <thead>
                <tr>
				<th width='10%'>Action</th>
                 <th width='10%'>ID</th>
				 <th width='40%'>Voucher_Type</th>
                 <th >Type Of</th> 
                 </tr>
                </thead>
				<tbody>
					<?php
					foreach($result as $r)
					{
						$del=" ".anchor('Account/delete_voucher_type/'.$r->acc_vtypeid,'<span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:red;"></span>', array('id' =>'del_conf'));
					?>
						<tr>
						 <td class='dtable' align='center'> <a href='#' id='edit' class='abtn abtn-edit' data-toggle="modal" data-target="#exampleModal" ><span class="glyphicon glyphicon-edit" aria-hidden="true" style='font-size:14px;'></span></a>&nbsp;
							<?=$del;?>	
							
						 </td>
						 <td><?=$r->acc_vtypeid;?></td>
						 <td><?=$r->acc_vtypename;?></td>
						 <td><?=$r->acc_pvtype;?></td> 
						</tr>
					<?php
					}
					?>	
					
				</tbody>
				</table>
			  </div>
        </div>

       </div>
    </div>
   </div>
   
</div>
</section>
</div>
          
 <!-- /.content-wrapper -->
  
  <!---modal dialog start ---->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style='top: 50px;border:1px solid #b7b7b7;'>
      <div class="modal-header" style='background-color:#cacaca;'>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true">&times;</span>
        </button>
		<h4 class="modal-title" id="exampleModalLabel">Edit Voucher Type</h4>
      </div>
	  
	  <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Account/edit_vtype')?>" enctype="multipart/form-data">
	  
		<div class="modal-body">
	  
				<div class="form-group">
				   <div class="row">
				   <input type="hidden" class="form-control"  name="mvtid"  id='mvtid' width='100px' required>
					  <label class="col-md-4 control-label">Voucher Type :</label>
						<div class="col-md-7">
							<input type="text" class="form-control"  name="mvtype"  id='mvtype'  required>
						</div>
					</div>
					<div class="row" style='margin-top:10px;'>
					  <label class="col-md-4 control-label">Type of Voucher :</label>
					  <label class="col-md-7 control-label-left" id='mtypeof' style='margin-top: 7px;'></label>
					</div>

				</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Save changes</button>
		  </div>
	  </form>
    </div>
  </div>
</div>

<!-- modal dialog end ----->
  
  
<?php include('application/views/common/footer.php'); ?>
   
<script type="text/javascript">

$("#example").dataTable({
	"ordering":false
	});

var msg=$("#msg").val().split('#');
if(msg[0]=='1')
{
	swal('',msg[1],'success'); //Saved
}
else if(msg[0]=='2')
{
	swal('',msg[1],'success'); //Update.
}
else if(msg[0]=='3')
{
	swal('',msg[1],'success'); //Delete.
}
else if(msg[0]=='4')
{
	swal('',msg[1],'error'); //Try Again.
}

$('#example tbody').on('click','#edit', function()
{
  //var rt1=$(this).parent('tr')
  var vid=$(this).closest('tr').find('td').eq(1).text();
  var vna=$(this).closest('tr').find('td').eq(2).text();
  var vtof=$(this).closest('tr').find('td').eq(3).text();
 
  $("#mvtid").val(vid);
  $("#mvtype").val(vna);
  $("#mtypeof").html(vtof);
});	

 $(document).on("click", "#del_conf", function () {
      return confirm('Are you sure you want to delete this entry?');
  });


</script>  
