<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
 <!-- Main content -->
 <!-- Content Header (Page header) -->

  <section class="content-header">
    <h1><b>Salary Entries List</b> </h1>
    <ol class="breadcrumb" style='font-size:14px;'>
	<li><a  href="<?php echo site_url('Vehicle/vehicle_expense') ?>" style='color:#4b88ed;'><button class='btn btn-primary' style='margin-bottom:2px;' ><i class="fa fa-plus" aria-hidden="true"></i> Add Expense</button></a></li>
	<li><a  href="<?php echo base_url('Reports/expenses');?>" style='color:#4b88ed;'><button style='padding:2px 20px 2px 20px;'><i class="fa fa-print" aria-hidden="true"></i> Print</button></a></li>
    </ol> 
  </section>
  
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 	  
  <section class="content"> 
<!--   <div style="color:blue;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Print,Share,Save,View,Edit,Delate)  </div> -->
			<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
   
    <!-- Small boxes (Stat box) -->
    <div class="row">
	
	<div class='col-md-2' style='padding-right:0px;'>
  <div style="background-color:#fff;padding:8px;">
  
  <div class='row'>
  <div class='col-md-12'>
	<span><label class='control-form' style='height:25px;border-bottom:1px solid #c2c2c2;'><b>SEARCH</b></label></span>
 	<span style='float:right;'><a href='<?php echo base_url('Vehicle/allexpenses');?>'><button class='btn btn-warning btn-xs' >&nbsp;&nbsp;&nbsp;All&nbsp;&nbsp;&nbsp;</button></a></span>
  </div>
  </div>
		 
  
  <div style='width:100%;background-color:#e4e4e4;padding:5px;'>
  <form onsubmit='return checkdata1();'  method='POST' action='<?php echo base_url('Vehicle/expenses/1');?>'>
	  <label class='control-form'>From Date</label>
	  <input class='form-control' data-provide="datepicker" id="datepicker1" value="<?php echo date('d-m-Y');?>" name='fdate'>
	  <label class='control-form'>From Date</label>
	  <input class='form-control' data-provide="datepicker" id="datepicker2" value="<?php echo date('d-m-Y');?>" name='tdate'>
	  <label class='control-form'>Vehicle</label>
	  <select name='vehicleid1' class='form-control' id='vehicleid1'>
	  <option value=''> ------------</option>
	  <?php
	  $vresult1=$this->db->select('*')->from('vehicle_registration')->get()->result();
	  foreach($vresult1 as $vr1)
	  {
	  ?>
	  <option value='<?=$vr1->vehicle_id;?>'><?=$vr1->vehicle_regno;?></option>
	  <?php } ?>
	  </select>
	  <input type='submit' class='btn btn-primary' style='margin-top:3px;' name='submit1' id='submit1'>
</form> 
  </div>
  
  <label style="background-color:#000;width:100%;height:1px;margin-top:15px;margin-bottom:10px;"></label>
  
  <div style='width:100%;background-color:#e4e4e4;padding:5px;'>
  
  <form onsubmit='return checkdata2();' method='POST' action='<?php echo base_url('Vehicle/expenses/2');?>'>
  
  <label class='control-form'>Select Month</label>
  <select name='mon' class='form-control'>
  <option value=''> ------------</option>
  <option value='1'> January</option>
  <option value='2'> February</option>
  <option value='3'> March</option>
  <option value='4'> April</option>
  <option value='5'> May</option>
  <option value='6'> June</option>
  <option value='7'> July</option>
  <option value='8'> August</option>
  <option value='9'> September</option>
  <option value='10'> October</option>
  <option value='11'> Novenber</option>
  <option value='12'> December</option>
  </select>
  <label class='control-form'>Vehicle</label>
  <select name='vehicleid2' class='form-control' id='vehicleid2'>
  <option value=''> ------------</option>
  <?php
  foreach($vresult1 as $vr1)
  {
  ?>
  <option value='<?=$vr1->vehicle_id;?>'><?=$vr1->vehicle_regno;?></option>
  <?php } ?>
  </select>
  <input type='submit' class='btn btn-primary' style='margin-top:3px;' name='submit2'>
  </form>
  </div>
  </div>
  </div>

 <div class="col-md-10">

<!------------------------------------ Add new user details ----------------------------------->

<div style="background-color:#fff;padding:15px; ">
							
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" >
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
                      <div class="row">
                      <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered" >
                               
                                <div class="portlet-body" >
                                    <div class="row">
                                    <div class="col-md-12">
									<h4 style='margin-top:0px;margin-bottom:3px;'><?php echo $this->session->flashdata('exphead'); ?></h4>
									</div>
									<label style="background-color:#e4e4e4;width:100%;height:1px;margin-top:0px;margin-bottom:10px;"></label>
								</div>
					  <table class="table table-striped table-hover table-bordered" id="example" width='100%' style='font-size:14px;'>
							<thead>
								<tr style="color:#5068f8;">
								 <th>Del</th>
								 <th>Date</th>
								 <th>Staff</th>
								 <th>Voucher_No</th>
								  <th>Voucher_date</th>
								 <th>Advance</th>
								 <th>Balance</th>
								 <th>Paid_Amount</th>
								 <th>Narration</th>
								 </tr>
							</thead>
							<tbody>
							<?php
							
							if(isset($vsalary))
							{
								$gtotal=0;
							foreach($vsalary as $r1)
							{
							?>
							<tr>
								 <td width='50px' align='center'><a href="<?php echo base_url('StaffSalary/del_salary/'.$r1->salary_id);?>" id='del_conf'><button class='btn btn-danger btn-xs'><i class="fa fa-trash-o" aria-hidden="true"></i></button></a></td>
								 <td><?=$r1->salary_date;?></td>
								 <td><?=$r1->staff_name;?></td>
								 <td><?=$r1->salary_voucherno;?></td>
								 <td><?=$r1->salary_voucherdate;?></td>
								 <td align='right' width='80px'><?=number_format($r1->salary_advance,"2",".","");?></td>
								 <td align='right' width='80px'><?=number_format($r1->salary_balance,"2",".","");?></td>
								 <td align='right' width='80px'><?=number_format($r1->salary_paidamount,"2",".","");?></td>
								 <td width='200px'><?=$r1->salary_narration;?></td>
								 </tr>
							<?php 
							//$gtotal+=$r1->expense_amount;
							} 
							}?>
							</tbody>
							<thead>
								<!--<tr style="color:#5068f8;font-size:18px;">
								 	 <th colspan='7' style='text-align:right;padding-right:5px;'>Total : &nbsp;&nbsp;&#8377;&nbsp;&nbsp <b> <?=number_format($gtotal,"2",".","");?></b></th>
								 <th></th>
								 </tr>  -->
							</thead>
							
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>

		
         <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            </div>
		 </div>
                 

  </div>
  <!-- End user details -->
  </div>
  
   
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 
 $('#example').DataTable({
	 "ordering":false,
 });

//sweet alert ----------------------------
	$("#idmsg").hide();
	if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
// sweet alert box -------------------------- 
 
 $('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});


$('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});

function checkdata1()
{
	var dt1=$('#datepicker1').val();
	var dt2=$('#datepicker2').val();
	var vid=$('#vehicleid1').val();
	
	if(dt1=="" || dt2=="" || vid=="")
	{
		alert("Select date and vehicle.");
		return false;
	}
	else
	{
	return true;
	}
}

function checkdata2()
{
	var mon=$('#mon').val();
	var vid=$('#vehicleid2').val();
	
	if(mon=="" || vid=="")
	{
		alert("Select month and vehicle.");
		return false;
	}
	else
	{
	return true;
	}
}
			   
 var table = $('#example').DataTable();
      $('#example tbody').on( 'click', 'tr', function () {
         if ( $(this).hasClass('selected') ) {
             $(this).removeClass('selected');
         }
         else {
 	        table.$('tr.selected').removeClass('selected');
             $(this).addClass('selected');
         }
   });			    
 
 
 
     $('#view-pdf').on('click',function(){
       var slno=$('#example').find('tr.selected').find('span.sln').text();
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
    var pdf_link = $(this).attr('href')+'/'+slno;
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Given Enquiry',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
        return false
    });

	
    $(document).on("click", "#del_conf", function () 
	{
            return confirm('Are you sure you want to delete this entry?');
        });

 //});
    


</script>



