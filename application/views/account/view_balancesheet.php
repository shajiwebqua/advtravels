<?php include('application/views/common/header.php'); ?>
<style>
.td-tot
{
	width:120px;
	text-align:right;
	font-size:16px;
	font-weight:700;
	border-top:2px solid #c3c3c3 !important;
	border-bottom:2px solid #c3c3c3 !important;
}
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1 class='heading' style='color:#0a1b58;padding-bottom:15px; font-weight:bold;'>Balance Sheet </h1>
	  <ol class="breadcrumb" style="margin-right:20px;font-size:13px;">
        <li><a id="view-pdf" href="<?php echo base_url('Pdf/balance_sheet');?>"><button class='btn btn-info'>Print </button> </a></li>
      </ol>
    </section>
	
    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-md-12">
          <div class="box">
  
         <!-- <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Details</h3> 
            </div>
			
			<!---------  filter row --->
			<div class='row' style='margin:7px 5px 0px 5px;background-color:#e4e4e4;padding:5px;'>
			<form role="form" method="POST" action="<?php echo base_url('Account/balance_sheet/1')?>" enctype="multipart/form-data">
             			
			<div class='col-md-2' style='padding-top:5px;'>	<label class='control-label'>Select Date [from-to] : </label>	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			<div class='col-md-1' style='padding-top:2px;'><input type='submit' name='btndetails' class='btn btn-primary' value='Get'></div>
			</form>
			
			</div>
			<?php
			$drn1=0;
			$crn1=0;
			$plamt=0;

			/*foreach ($plamount as $pn1)
				{
					$crn1+=$pn1->credit;
					$drn1+=$pn1->debit;
				}

				var_dump($plamount);
				
				$plamt=$drn1-$crn1; // profit & loss account value;
				var_dump($plamt);*/
			?>
			
			
			<!--------  filter row --->
			
            <!-- /.box-header -->
            <div class="box-body" style='min-height:450px;'>
			<div class='row'>
				<div class='col-md-12'>
				<label style='font-weight:700;margin-left:20px;'> <?php echo $this->session->userdata('acperiod2');?></label>
				</div>
			</div>
            <div class="row">
	            <div class="col-md-10">
				<table class="table table-hover table-bordered" id="example"  width='100%' border=0 style="font-size:14px">
             <thead>
                <tr>
				<th width='20px'></th> 
				<th>Particulars</th> 
				<th style='width:120px;text-align:right;'>Debit</th>
                <th style='width:120px;text-align:right;'>Credit</th> 
                </tr>
                </thead>
				<tbody>
				<tr ><td></td> 	<td><b>ASSETS</b></td> <td></td> <td></td> </tr>

				<?php
				$drtot=0;
				$crtot=0;

				$deb=0;
				$cre=0;
			
				foreach ($assets as $pr1)
				{
				
				if($pr1->debit!=0 and $pr1->credit!=0)
					{
						if($pr1->debit>$pr1->credit)
						 {
							 $deb=$pr1->debit-$pr1->credit;
							 $cre=0;
						 }
						 else
						 {
							 $cre=$pr1->credit-$pr1->debit;
							 $deb=0;
						 }
					}
					else
					{
						$deb=$pr1->debit;
						$cre=$pr1->credit;
					}
				
			/*	if($pr1->debit<=0)	
				{
					$deb=$pr1->debit;
					$cre=$pr1->credit;
				}
				else
				{
					$deb=$pr1->debit;
					$cre=$pr1->credit;
				}	
				*/
				
				$drtot+=$deb;
				$crtot+=$cre;

				?>
				<tr>
				<td width='20px'></td> 
				<td><a href="#myModal3" id='<?=$pr1->ledg_id;?>' class='view-dt' data-toggle='modal'><?=ucwords(strtolower($pr1->ledg_name));?></a></th> 
				<td style='width:120px;text-align:right;'><?=number_format($deb,2,".","");?></td>
                <td style='width:120px;text-align:right;'><?=number_format($cre,2,".","");?></td> 
                </tr>
								
				<?php
				}

				foreach ($liability as $la2)  // adding liability amounts to find total
				{
					$drtot+=$la2->debit;
				    $crtot+=$la2->credit;
				}
		
				$plamt=$drtot-$crtot;

					//to print profit & loss amount
					if($plamt<0)
					{
						
					?>
					<tr>
					<td width='20px'></td> 
					<td><?="(Profit & loss - <b>Net loss</b>)";?></th> 
					<td style='width:120px;text-align:right;'><?=number_format(abs($plamt),2,".","");?></td>
					<td style='width:120px;text-align:right;'>0.00</td> 
					</tr>  
				<?php
					}
				?>
				<tr ><td></td> 	<td></td> <td></td> <td></td> </tr>

				<tr ><td></td> 	<td><b>LIABILITIES</b></td> <td></td> <td></td> </tr>
								
				<?php

				//var_dump($plres1);
				$deb=0;
				$cre=0;
				foreach ($liability as $pr2)
				{
				?>
				<tr>
				<td width='20px'></td> 
				<td><a href="#myModal3" id='<?=$pr2->ledg_id;?>' class='view-dt' data-toggle='modal'><?=ucwords(strtolower($pr2->ledg_name));?></a></th> 
				<td style='width:120px;text-align:right;'><?=number_format($pr2->debit,2,".","");?></td>
                <td style='width:120px;text-align:right;'><?=number_format($pr2->credit,2,".","");?></td> 
                </tr>
				
				<?php
				}
				?>
				<?php //to print profit & loss amount
					if($plamt>0)
					{
						$crtot+=$plamt;
					?>
					<tr>
					<td width='20px'></td> 
					<td><?="(Profit & loss - <b>Net Profit</b>)";?></th> 
					<td style='width:120px;text-align:right;'>0.00</td> 
					<td style='width:120px;text-align:right;'><?=number_format(abs($plamt),2,".","");?></td>
					</tr>
				<?php
					}
				?>
				<tr ><td></td> 	<td></td> <td></td> <td></td> </tr>
				<tr>
				<td width='20px'></td> 
				<td style='font-size:16px;font-weight:700;'>Totals</th> 
				<td class='td-tot'><?=number_format($drtot,2,".","");?></td>
                <td class='td-tot'><?=number_format($crtot,2,".","");?></td> 
                </tr>
				
				</tbody> 
				</table>
			  </div>
        </div>
       </div>
</div>
</section>
</div>

 <div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria- hidden="true">
	    <div class="modal-dialog modal-lg">
			<div class="modal-content">
			    <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Details</h4>
				</div>
				<div class="modal-body" style='max-height:460px;overflow:auto;'>
				
				
				</div>
				 <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
   </div>  

  
<?php include('application/views/common/footer.php'); ?>
   
<script type="text/javascript">

/*$("#example").dataTable({
	"ordering":false,
	});*/
	
 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
   
  $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
 
  $("#view-pdf").click(function(){
 	 var pdf_link = $('#view-pdf').attr('href'); 
	    // alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Report:',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;

  });
 
 
 $(".view-dt").click(function(){
	 var Result=$("#myModal3 .modal-body");
        var lid =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Account/get_selected_account_details",
        dataType: 'html',
        data: {ledgid: lid},
        success: function(res) {
        Result.html(res);
                    }
        });
 });
 
 
</script>  
