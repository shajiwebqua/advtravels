<div class="modal-header">
<style>
.la-right
{
	text-align:right;
}
</style>
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel" >Edit Service Evaluation</h4>
      </div>
      
          <div class="modal-body">
          <form enctype="multipart/form-data" method="post" action="<?php echo site_url('Services/update_evaluation') ?>" >
            <div class="form-group">
        <input type="hidden" id="edit_service_id" name="edit_service_id" value="<?php echo $seva->se_id;?>" />
		<div class='row'>
        <label class='col-md-3 control-label la-right' for="service_type">Service type:</label>
		<div class='col-md-6'>
        <select name="service_type" id="service_type" class="form-control" required>
          <option value="">----select----</option>
          <option value="1" <?php if($seva->se_service_type==1) echo "selected";?>>INDIVIDUAL</option>
          <option value="2" <?php if($seva->se_service_type==2) echo "selected";?>>CORPORATE</option>
        </select>
        </div></div></div>
				
        <div class="form-group">
        <div class='row'>
        <label class='col-md-3 control-label la-right' for="guest_name">Guest Name:</label>
		<div class='col-md-8'>
        <input type="text" class="form-control" id="guest_name" name="guest_name" value="<?php echo $seva->se_guest_name?>" required>
        </div></div></div>
        <div class="form-group">
        <div class='row'>
        <label class='col-md-3 control-label la-right'for="contact_no">Contact No:</label>
		<div class='col-md-8'>
        <input type="text" class="form-control" id="contact_no" name="contact_no" value="<?php echo $seva->se_contact_no?>" required>
        </div></div></div>
        <div class="form-group">
        <div class='row'>
        <label class='col-md-3 control-label la-right' for="contact_no">Date</label>
		<div class='col-md-8'>
        <input type="date" class="form-control" id="date" name="date" value="<?php echo $seva->se_date?>" required>
        </div>
		</div></div>
        <div class="form-group">
        <div class='row'>
        <label class='col-md-3 control-label la-right' for="vehicle_no">Vehicle No:</label>
		<div class='col-md-8'>
			<select id="vehicle_no" name="vehicle_no" class="form-control" required>
			<option value="">----select----</option>
			<?php 
			$ves1=$this->db->select('vehicle_id,vehicle_regno')->from('vehicle_registration')->get()->result();
			foreach($ves1 as $vs1)
			{
			?>
			  <option value="<?=$vs1->vehicle_id;?>" <?php if($seva->se_vehicle_no==$vs1->vehicle_id) echo "selected";?>   ><?=$vs1->vehicle_regno;?></option>
			<?php
			}
			?>
			</select>
        
        </div>
		</div></div>
        <div class="form-group">
        <div class='row'>
        <label class='col-md-3 control-label la-right' for="chauffeur_name">Chauffeur Name:</label>
		<div class='col-md-8'>
		<select id="chauffeur_name" name="chauffeur_name" class="form-control" required>
				  <option value="">----select----</option>
			<?php 
			$res1=$this->db->select('driver_id,driver_name')->from('driverregistration')->get()->result();
			foreach($res1 as $rs1)
			{
			?>
			  <option value="<?=$rs1->driver_id;?>" <?php if($seva->se_chauffeur_name==$rs1->driver_id) echo "selected";?> ><?=$rs1->driver_name;?></option>
			<?php
			}
			?>
			</select>
		
        </div>
		</div></div>
        <div class="form-group">
        <div class='row'>
        <label class='col-md-3 control-label la-right'for="feedback">Feedback:</label>
		<div class='col-md-8'>
        <textarea rows=3 name="feedback" id="feedback" class="form-control" required><?php echo $seva->se_feedback?></textarea>
        </div>
		</div></div>
		<div class="form-group">
        <div class='row'>
        <label class='col-md-3 control-label la-right'for="feedback">Action Taken:</label>
		<div class='col-md-8'>
        <textarea rows=3 name="actiontaken" id="actiontaken" class="form-control" required><?php echo $seva->se_actiontaken;?></textarea>
        </div>
		</div></div>
		
		
		
		<div class='row'>
		<div class='col-md-10' style='text-align:right;'>
         <button type="submit" class="btn btn-primary">Update Details</button> 
		 <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
        </div></div>
		</form>
		
</div>
</div>
