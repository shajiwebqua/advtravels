<?php
include('application/views/common/acc_header.php');
?>
  <style>
  .dashbtn
  {
	  width:75px;
	  height:75px;
	  background-color:#f4f4f4;
	  border:1px solid #fff;
	  /*border-radius:10px;*/
	  padding:5px;
	  text-align:center;
  }
  
.sh4
{
 border-bottom:1px solid #e4e4e4;
 width:100%;
 color:#00adee;
 padding-bottom:5px;
 margin-top:0px;"
}

<link rel="stylesheet" href="<?php echo base_url('assets/chart/css/material-charts.css');?>">
<script src="<?php echo base_url('assets/chart/js/material-charts.js');?>"></script>

</style>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#00adee'> Dashboard </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
     <label style="background-color:#cecece;width:100%;height:1px;"></label>   
     </section>
     <!-- Main content -->
    <section class="content">
	
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header">
          <h3 class="box-title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Account Groups</h3>
		  <label id='mes' ><?php echo $this->session->flashdata('message');?></label>
        </div>
		
        <div class="box-body">
        	<div class='row' style='padding:15px;'>
		<!-- <div class='col-md-5'>
			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Account/add_group')?>" enctype="multipart/form-data">
                  
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Group Name :</label>
								<div class="col-md-8">
									<input type="text" class="form-control"  name="grpname" id='grpname' required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Under :</label>
								<div class="col-md-8">
								<select name='grpunder' class='form-control' id='grpunder' required>
								<option value="">---------</option>
								<option value="Dr">Dr</option>
								<option value="Cr">Cr</option>
								</select>
								</div>
							</div>
						</div>
						
					 <hr style='margin:15px;'>
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1"></label>
								<div class="col-md-7">
									<input type="submit" class="btn btn-primary" name="btnsubmit" value='Add Group'>
								</div>
							</div>
					    </div>
			</form>
			</div>  -->
			
			<!-- second column start------------------------- -->
			<div class='col-md-7'>
			
			<table class="table table-striped table-hover table-bordered"  width='100%' id='example'>
			   <thead><tr >
			   	<!-- <th width='70px'>Action</th> -->
			     <th width='70px'>ID</th>
				 <th >Groups</th>
				 <th width='70px'>Type</th>
				</tr>
				</thead>
				<tbody>
				<?php
				foreach ($result as $r1)
				{
				?>
				<tr >
			   	<!-- <td><button class='edit btn btn-warning btn-xs'id='<?=$r1->acc_grp_id;?>'>Edit</button></td> -->
			     <td><?=$r1->acc_grp_id;?></td>
				 <td><?=$r1->acc_grp_name;?></td>
				 <td><?=$r1->acc_grp_under;?></td>
				</tr>
				<?php }
				?>
				</tbody>
			</table>
			</div>
			<!-- second colunn -->

			</div>
           
        </div>
	</div>
	</div>
	
</section>
    <!-- content wrapper -->
</div>
     <?php
  include('application/views/common/acc_footer.php');
  ?>
   
</body>
</html>
  <script type="text/javascript">
  $("#mes").hide();
  
  if($("#mes").html()!="")
  {
	  swal("Saved.!",$("#mes").html(),"success");
	  $("#mes").html("");
  }
    
  $('#example').DataTable({
		"ordering":false,
  });
	
  
  $("#example").on('click','.edit',function()
  {
	 var id=$(".edit").attr('id');
	 alert(id); 
	 
	 
  });
  
  
  </script>
</body>
</html>

  
  