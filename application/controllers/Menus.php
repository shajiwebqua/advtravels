<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends CI_Controller {

	function __construct(){
		parent::__construct();
		$user = $this->session->userdata('userid');
               if (!isset($user))
               { 
                       redirect('/');
               } 
               else
               { 
                   return true;
               }
$this->load->helper(array('form', 'url'));
	}

	public function listOfSubmenus(){

		if($this->input->post() != null){
			$this->load->model('Model_staff');

			$menu_cat_id = $this->input->post("catid");
			$user['staff_id'] = $this->input->post("user");
			$result['menu'] = $this->Model_staff->view_menuoptions($menu_cat_id);
			$result['user_menu'] = $this->Model_staff->get_menus($user);
			echo json_encode($result);
		}

	}

	public function addSubmenus(){

		if($this->input->post() != null){
			$this->load->model('Model_staff');
			$menu_options = $this->input->post("submunu_val");

			$data['menu_cat_id'] = $this->input->post("menuhead");
			$data['staff_id'] = $this->input->post("user");

			//To Get The Saved menu_options From database 
			$db_menu = $this->Model_staff->get_user_menus($data);
			
			//To make a single array
			$menu = array();
			foreach($db_menu as $value){
				array_push($menu,$value->menu_options);
			}

			//db_menu that needed to delete
			$db_menu = array_values(array_diff($menu, $menu_options));

			//Deleate the menu
			for($i = 0; $i < sizeof($db_menu); $i++){
				$data['menu_options'] = $db_menu[$i];
				$this->Model_staff->delete_menu_user($data);
			}

			//Insert the menu
			for($i = 0; $i < sizeof($menu_options); $i++){
				if($menu_options[$i] != 0){
					$data['menu_options'] = $menu_options[$i];
					if(count($this->Model_staff->get_user_menus($data)) <= 0){
						$this->Model_staff->insert_user_menus($data);
					}
				}
			}

			redirect("staff/menu/".$data['staff_id']);
		}

	}

	public function hai(){
		$date = strtotime( "+". 0 ." month" , strtotime("23-02-2017"));
		for($i=1;$i<10;$i++){
			echo date("Y-m-d", $date);
			echo "<br />";
			$date = strtotime( "+". $i ." month" , strtotime("23-02-2017"));
		}
	}
}
