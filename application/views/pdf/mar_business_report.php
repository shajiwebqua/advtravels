<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
td{
	padding:5px 10px 5px 10px;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

 <table width="100%"><tr><td><h3><u><?php echo $dcap;?></u></h3></td><td align='right' >Date : <?php echo date('d-m-Y');?></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
 
 		<div class='row'>
				<div class='col-md-8'>
				
		<table class="table table-striped table-hover table-bordered" id="example" width='100%'>
                <thead>
                <tr>
					 <th>Date</th>
					 <th>Staff_Name</th>
					 <th>Customer</th>
					 <th>Business</th>
					 <th>Follow_up</th>
                </tr>
				 <tr>
					 <td colspan='5'><hr style='margin:0px;'></td>
                </tr>
                </thead>
				
				 <tbody>
				 <?php
				 if(isset($marres))
				 {
				 foreach($marres as $mr)
				 {
				 if($mr->mar_cust_group=='1')
				 {
					 $cgroup="INDIVIDUAL";
				 }					 
				 ELSE
				 {
					 $cgroup="CORPORATE";
				 }
				
				$mm='<button class="btn btn-warning btn-xs">Delete</button>';
				$del=anchor('General/del_marketing/'.$mr->mar_id, $mm, array('id' =>'del_conf'));
            
					 
				 ?>
                <tr>
					 <td width='80px;'><?=$mr->mar_edate;?></td>
					 <td width='120px;'><b>Staff: </b><?=$mr->staff_name;?><br><?=$mr->profession_name;?></td>
					 
					 <td><b>Type:&nbsp; </b><?=$cgroup;?>
					 <br><b>Name:&nbsp; </b><?=$mr->mar_cust_name;?>
					 <br><b>Address:&nbsp; </b><?=$mr->mar_cust_address;?>
					 <br><label style='color:blue;font-size:14px;margin-top:2px;'>---------Contact Person---------</label>
					 <br><b>Name:&nbsp; </b><?=$mr->mar_cont_person;?>
					 <br><b>Desig:&nbsp; </b><?=$mr->mar_designation;?>
					 <br><b>Mob:&nbsp; </b><?=$mr->mar_cont_number;?></td>
					 
					 <td><p><b>Business:&nbsp; </b><?=$mr->mar_business;?></p>
					 <p><b>Discussion:&nbsp; </b><?=$mr->mar_discussion;?></p>
					 <p><b>Time Start:&nbsp; </b><?=$mr->mar_start_time;?> &nbsp;&nbsp;&nbsp; <b>End: </b><?=$mr->mar_end_time;?></p>
					 <p><b>Reponse:&nbsp;</b> <?=$mr->mar_res_status;?></p>
					 <p><b>Steps-Taken:&nbsp;</b> <?=$mr->mar_business_steps;?></p></td>
					 
					 <td><?=$mr->mar_followup_date;?></td>
                </tr>
				<tr>
					 <td colspan='5'><hr style='margin:0px;'></td>
                </tr>
				<?php
				 }}
				 ?>
				 
                </tbody>

                </table>

			</div>
		</div>
</body>
</html>