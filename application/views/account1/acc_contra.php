<?php
include('application/views/common/header.php');
?>
<style>
input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
</style>

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
		  <h1 class='heading' style='color:#00adee'>Contra
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
     
     </section>
	
        <!-- Main content -->
    <section class="content">
	
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box" style="border:1px solid #f5f3f3;">
            <div class="box-header">
          <h3 class="box-title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp; Create Contra Voucher</h3>
		  <label id='mes'><?php echo $this->session->flashdata('message');?></label>
        </div>
		
        <div class="box-body">
				
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item <?php if($tabstatus!='2') echo "active";?>" id='tb1' >
						<a class="nav-link tp1" data-toggle="tab" href="#vpayment"  role="tab"><i class="fa fa-bars" aria-hidden="true"></i> Contra</a>
					</li>
					<li class="nav-item <?php if($tabstatus=='2') echo "active";?>" id='tb2'>
						<a class="nav-link tp1 " data-toggle="tab" href="#vlist"  role="tab"><i class="fa fa-bars" aria-hidden="true"></i> Contra Details</a>
					</li>
					
                 </ul>
                <!-- Tab panes -->
			<div class="tab-content">
			<div class="tab-pane <?php if($tabstatus!='2') echo "active";?>" id="vpayment" role="tabpanel">
		
        	<div class='row' style='padding:15px;'>
			<div class='col-md-8'>
			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Voucher/save_contra')?>" enctype="multipart/form-data">
                  
				  <div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Voucher Type :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="cvtype" value='CONTRA' readonly>
								</div>
							</div>
						</div>
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Date :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="cdate" value='<?php echo date('d-m-Y');?>' readonly>
								</div>
							</div>
						</div>
								
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Account :</label>
								<div class="col-md-4">
									<select  class="form-control" name='caccount' required>
									  <option value="">---select----</option>
									  <option value="CASH">CASH</option>
									  <option value="BANK">BANK</option>
									  
									  <?php
										//$gquery=$this->db->select("*")->from('acc_groups')->limit('24')->get()->result();
										//foreach( $gquery as $g)
										//{
											//echo "<option value='".$g->acc_grpid."'>".$g->acc_grpname."</option>";
										//}
									  ?>
									  
									</select>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Ledger :</label>
								<div class="col-md-7">
								<select  class="form-control" name='cledger' required>
									  <option value="">---select----</option>
									  <?php
										$lquery=$this->db->select("*")->from('acc_ledgers')->get()->result();
										foreach($lquery as $l)
										{
											echo "<option value='".$l->acc_ledgid."'>".$l->acc_ledgname."</option>";
										}
									  ?>
								</select>
								</div>
							</div>
						</div>
			
			
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Amount :</label>
								<div class="col-md-4">
									<input type="number" class="form-control"  name="camount" required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Narration :</label>
								<div class="col-md-7">
									<textarea rows=3 class="form-control"  name="cnarrat" required></textarea>
							  </div>
							</div>
						</div>
						
						<hr>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label"></label>
								<div class="col-md-7">
									<input type="submit" class="btn btn-primary "  name="btnsubmit" value="Save Voucher">
								</div>
							</div>
						</div>
						
			</form>
				
			</div>
			</div>
			</div> <!-- first tab end -->
			
			<div class="tab-pane <?php if($tabstatus=='2') echo "active";?> " id="vlist" role="tabpanel">
			
			<div class='row' style='margin:0px 5px 0px 5px;background-color:#e4e4e4;padding:5px;'>
			
			<form role="form" method="POST" action="<?php echo base_url('Voucher/contra/2')?>" enctype="multipart/form-data">
             			
			<div class='col-md-2' style='padding-top:5px;'>	<label class='control-label'>Select Date [from-to] : </label>	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			<div class='col-md-2' style='padding-top:2px;'><input type='submit' name='btndetails' class='btn btn-primary' value='Get Details'> </div>
			
			</form>
			</div>

			
			<div class='row' style='margin-left:10px;margin-top:15px;'>
			<label style='padding-bottom:3px; border-bottom:1px solid #e4e4e4;font-size:15px;'><b><?php echo $this->session->flashdata('ptitle');?></b></label>
			</div>
					
			<div class='row' style='padding:10px;'>
			<div class='col-md-12'>
			 <table class="table table-striped table-hover table-bordered" id="example" border=0 STYLE='font-size:14px;width:100%'>
			 
			 <thead>
                <tr>
				 <th  style='text-align:center !important;'>Edit</th>
                 <th>ID</th>
				 <th>Date</th>
                 <th>Account</th> 
                 <th>Particulers</th>
				 <th>Narration</th>
				 <th style='text-align:right !important;' >Amount</th>
				 
                </tr>
                </thead>
				<tbody>
					<?php
					$gtotC=0;
					if(isset($result))
					{
					foreach($result as $r)
					{
					?>
						<tr height='33px'>
						 <td align='center' class='dtable'><a href='#' class='edit abtn abtn-edit' data-toggle="modal" data-target="#exampleModal"><span class="glyphicon glyphicon-edit" aria-hidden="true" ></span></a>
						  <a href='#' class='del abtn abtn-del'> <span class="glyphicon glyphicon-trash" aria-hidden="true" ></span></a>
						 </td>
						 <td><?=$r->acc_con_id?></td>
					     <td><?=date_format(date_create($r->acc_con_date),'d-m-Y');?></td>
						 <td><?=$r->acc_con_account;?></td>
						 <td><?=$r->acc_con_ledger;?></td> 
						  <td><?=$r->acc_con_narration;?></td> 
						 <td align='right' style='font-weight:600;'><?=number_format($r->acc_con_amount,'2','.','');?></td>
						</tr>
					<?php
					$gtotC+= $r->acc_con_amount;
					}
					
					}
					?>	
				</tbody>
			 
				</table>
				<div class='row' style='margin:10px 0px;background-color:#e2e2e2;padding-top:3px;'>
				<div class='col-md-9' style='text-align:right;'>
				<label class='control-label' style='font-size:16px;font-weight:600;'> Total Received Amount : </label>
				</div>
				<div class='col-md-3' style='text-align:right;'>
				<label class='control-label' style='font-size:18px;font-weight:600;'> &#8377;&nbsp; <?=number_format($gtotC,"2",".","");?></label>
				</div>
				</div>
				</div>
								
			</div>
					
			</div> <!-- second tab end --->
			</div> <!-- tab content end -->
           
        </div>
	</div>
	</div>
	
</section>
    <!-- content wrapper -->
	
	 <!---EDIT  modal dialog start ---->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style='top: 50px;border:1px solid #b7b7b7;'>
      <div class="modal-header" style='background-color:#cacaca;'>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true">&times;</span>
        </button>
		<h4 class="modal-title" id="exampleModalLabel" >Edit contra voucher</h4>
      </div>
	  
	  <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Voucher/edit_contra')?>" enctype="multipart/form-data">
	  
		<div class="modal-body">
					<input type="hidden" class="form-control"  name="mcid"  id="mcid" required>
				<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Account :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="mcaccount"  id="mcaccount" value='CASH' readonly>
								</div>
							</div>
						</div>
				
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Particulers :</label>
								<div class="col-md-7">
									<select  class="form-control" name='mcledger' id='mcledger' required>
									  <option value="">---select----</option>
									  <option value="CASH">CASH</option>
									<?php
									/*	$lquery=$this->db->select("*")->from('acc_ledgers')->get()->result();
										foreach($lquery as $l)
										{
											echo "<option value='".$l->acc_ledgid."'>".$l->acc_ledgname."</option>";
										}*/
									  ?>
								</select>
								</div>
							</div>
						</div>
			
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Amount :</label>
								<div class="col-md-7">
									<input type="number" class="form-control"  name="mcamount" id="mcamount" required>								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Narration :</label>
								<div class="col-md-7">
									<textarea rows=3 class="form-control"  name="mcnarrat" id='mcnarrat' required></textarea>
							  </div>
							</div>
						</div>
						
										
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Save changes</button>
		  </div>
	  </form>
    </div>
  </div>
</div>

<!-- modal dialog end ----->
			
	
</div>
     <?php
  include('application/views/common/footer.php');
  ?>

</body>
</html>
  <script type="text/javascript">
  

  
$('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    autoclose:true,
	
});
   $('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});
  
  
  $("#example").dataTable({
	  "ordering":false,
  });
  
  $("#mes").hide();
  
  
  var msg=$("#mes").html().split("#");
  
  if($("#mes").html()!="")
  {
	  var msg=$("#mes").html().split("#");
	if(msg[0]=='1')
	  swal("",msg[1],"success"); //Saved.!
	else if(msg[0]=='2')
	  swal("",msg[1],"success");  //Updated.!
    else if(msg[0]=='3')
	  swal("Removed.!",msg[1],"success"); //Removed.!
	else if(msg[0]=='4')
	  swal("",msg[1],"error"); //Try Again.
     $("#mes").html("");
  }
  
$('#example tbody').on('click','.edit', function()
{
  //var rt1=$(this).parent('tr')
  var cid=$(this).closest('tr').find('td').eq(1).text();
  var dt=$(this).closest('tr').find('td').eq(2).text();
  var acc=$(this).closest('tr').find('td').eq(3).text();
  var part=$(this).closest('tr').find('td').eq(4).text();
  var narr=$(this).closest('tr').find('td').eq(5).text();
  var amt=$(this).closest('tr').find('td').eq(6).text();
  	  $("#mcid").val(cid);
	  $("#mcledger").val(part);
	  $("#mcamount").val(amt);
	  $("#mcnarrat").val(narr);
 });	
  
$('#example tbody').on('click','.del', function()
{
  var cid=$(this).closest('tr').find('td').eq(1).text();
  
		  swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this voucher.!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "Cancel",
		  closeOnConfirm: true,
		  closeOnCancel: true
		},
		function(isConfirm) 
		{
		  if (isConfirm)
 		  {
			  jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>" + "Voucher/delete_contra",
					dataType: 'html',
					data: {coid:cid},
					success:function(res)
					{
						window.location.href="<?php echo base_url('Voucher/contra/3');?>";
					}
					}); 
			
		  }
		});
 });	
    
  </script>
</body>
</html>

  
  