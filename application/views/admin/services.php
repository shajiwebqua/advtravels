<?php
include('application/views/common/header.php');
?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1><b>Service Invoice Entry</b></h1>
		<ol class="breadcrumb" style='font-size:15px;'>
			<li><a href="<?php echo base_url('Vehicle/Index');?>" style='color:#4b88ed;'><i class="fa fa-backward" aria-hidden="true"></i>Back</a></li>
			<!-- <li><a href="<?php echo base_url('Services/service_invoicelist');?>" style='color:#4b88ed;'><i class="fa fa-file-text-o"  aria-hidden="true"></i>Service History</a></li> -->
			<!--<li> <a href="<?php echo base_url('Services/Service_invoicelist');?>" style="color:blue;font-size:15px;"> <h5><b>Service History</b></h5></a></li>-->
	    </ol> 
         <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
  
  <section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div>		
				<h5 id="h5msg" ><?php echo $this->session->flashdata('message1'); ?></h5></CENTER>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
  <div class="col-md-12">

  <!--Add new user details -->

   <div style="background-color:#fff;border-radius:2px;padding:10px 15px; ">
    
        <div class="portlet-title">
        <div class="caption">
        <div style="color:blue;font-weight:bold;padding:0px; margin:0px;" >
           Invoice details                  
        </div> 
        </div>
        </div>
      <hr style="background-color:#cecece,border:1px;width:100%;top:10px;margin:0px;padding:3px 0px 10px 0px;">
             
             
 <div class="portlet-body form">
             
<div class="row">
<div class="col-md-4" style='border-right:1px solid #e4e4e4;'>             
<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Services/add_services')?>" enctype="multipart/form-data">
             
  <div class="form-group" style='margin-left:10px;'>
		<div class='row'>
		<label class="col-md-12" style="padding-top:5px;" >Vehicle RegNo : </label>
		</div>
		
        <div class="row col-md-12">
		<div id="regno1">
			<select name="regno" id="regno" class="form-control" required>
			<option value=''>------------</option>
             <?php
				foreach($vehicleid as $values6)
                {
                ?>
				<option value='<?=$values6->vehicle_id;?>'> <?=$values6->vehicle_regno;?> </option>
                <?php } ?>
                </select>
        </div>
        </div>
        </div>  

			 <div class="form-group" style='margin-left:10px;'>
			 <div class='row'> 
				<label class="col-md-12"> Description</label>
			 </div>
				<div class="row col-md-12">
					<textarea rows=2  class="form-control"  name="description" required></textarea>
				</div>
              </div>
			  
			  <div class="form-group" style='margin-left:10px;'>
			  <div class='row'>
				<label class='col-md-12'>Invoice Date </label>
			  </div>
				<div class=" row col-md-12">
				<input  id="datepicker" class='form-control' size='4' type='text' name="date" placeholder="DD MM YY"  required>
				</div>
				</div>
			  
			   <div class="form-group" style='margin-left:10px;'>
			   <div class='row'>
                      <label class="col-md-12">Invoice No</label>
				</div>
                <div class=" row col-md-12">
			 	<input type="number" class="form-control" name="invoiceno" required>
                </div>
               </div>  
			  
			   <div class="form-group" style='margin-left:10px;'>
			   <div class="row" >
                      <label class="col-md-12">invoice Amount</label>
				</div>
                <div class="row col-md-12">
				<input type="number" class="form-control" name="invoiceamount" required>
               </div>
              </div> 

 		  			<div class="form-group" style='margin-left:10px;' >
					<div class='row'>
						<label class="col-md-12" style="color:blue;font-size:13px;"> To upload a scanned copy of invoice, Select file</label>
					</div>
						<div class="row col-md-12">
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<span class="btn btn-primary btn-file">
									<span class="fileinput-new" > Choose file </span>
									<span class="fileinput-exists" > Change </span>
										<input type="file" name="image" multiple="true" id="f" required> </span>
										<span class="fileinput-filename"> </span> &nbsp;
										<a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                 </div>
                        </div>
                    </div>
		<hr style='margin:5px 0px 5px 0px;'>			
 
   <div class="form-group" >
   <div class="row">
       <div class="col-md-12 " >
       <center><button type="submit" class="btn btn-success"><center>Save Invoice Details</button></center>
       </div>
       </div>
   </div>
</form>
</div>
<div class='col-md-8'>
				  
	 <table class="table table-striped table-hover table-bordered" id="example1" style='color:#000;font-size:13px;'>
            <thead>
                <tr>
                 <th>Edit</th>
                 <th>Delete</th>
                 <th>Service Date</th>
                 <th>Vehicle RegNo</th>
                 <th>Description</th>
                 <th>Invoice Date</th>
                 <th>Invoice No</th>
                 <th>Invoice Amount</th>
               
                 </tr>
            </thead>
       </table>
	
</div>
</div>  

 </div>
</div>    
<br>  
<!---- carty end -->


  </div>
  </div>
  <!-- End user details -->
  </div>
  
    <!-- /.row --> 
    <!-- /.row (main row) --> 
    <!-- /.content --> 

</section>

<div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body" style="height:-30px;">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Edit</h4>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                   </div><!-- row end -->


<?php include('application/views/common/footer.php');?>
</div>    
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script> 

<script type="text/javascript">
   
//sweet alert ----------------------------
	$("#idmsg").hide();
	if($("#idh5").html()!="")
	{
		swal( $("#idh5").html(),"","success")
		$("#idh5").html("");
	}
// sweet alert box -------------------------- 

 $("#regno").change(function()
   {   
   var regno= $("#regno").val();

    $('#example1').dataTable({
         destroy: true,
        "processing": true,
        "scrollX":true,
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Services/services_ajax",// json datasource
                data: {regno: regno},
               },
        "columns": [
           
           
            { "data": "edit"},
            { "data": "delete" },
            { "data": "date"},
            { "data": "regno" },
            { "data": "description" },
            { "data": "invoicedate" },
            { "data": "invoiceno" },
            { "data": "invoiceamt" },
           // { "data": "files" },
           
        ]
  } );
  
  
 $('#example tbody').on('click', '.edit', function ()  {
        var Result=$("#myModal2 .modal-body");

     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Services/edit_services",
        dataType: 'html',
        data: {service_id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
 
        }); 
    
 } );

  $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
     });


</script>

</body>
</html>
