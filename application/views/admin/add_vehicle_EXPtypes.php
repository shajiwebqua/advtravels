<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 <style>
 .right1
 {
	 text-align:right;
	 padding-right:0px;
 }
 </style>
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <b> Expense Types</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px;'>

    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

     
  <section class="content"> 

  <div class="row">
  <div class="col-md-12">


              
<div class="page-content-wrapper">
 
   <div class="page-content">
   
   <div style="padding:0px 15px 0px 15px;">
				 <div id="idmsg"  style="background-color:#fff;height:25px;margin-bottom:5px;">
         <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center></div>
	</div>
				 
		
                    <div class="col-md-5">
                    <div style="background-color:#fff;padding:10px; ">
					   

        <div class="col-md-12">
			   <h4>Add Expense Type</h4>
			   <hr>
          </div>
 
          <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/add_expense_type')?>" enctype="multipart/form-data">
                  
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label right1" >Expense Group</label>
                           <div class="col-md-7" style='margin-top:5px;'>
						   <select name='expgroup' class='form-control' required>
						   <option value=''>-------</option>
						   <option value='1'>VEHICLE</option>
						   <option value='2'>GENERAL</option>
                            </select>
                           </div>
                        </div>
                     </div>
					
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label right1">Expense Type</label>
                           <div class="col-md-7" style='margin-top:5px;'>
                            <input type="text" class="form-control"  name="extype" required>
                           </div>
                        </div>
                     </div>
					 
 
					<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label right1">Under (for Accounts)</label>
								<div class="col-md-7">
									<select  class="form-control" name='exunder'  id='exunder' required>
									  <option value="">---under----</option>
									  <?php
										$gquery=$this->db->select("*")->from('ac_groups')->get()->result();
										foreach( $gquery as $g)
										{
											echo "<option value='".$g->grp_id."'>".$g->grp_name."</option>";
										}
									  ?>
														  
									</select>
								</div>
							</div>
						</div>
 
                       <hr>
					                        
 
                    <div class="form-group">
                     <div class="row">
                      <div class="col-md-2 left1">
                      </div>
                               <div class="col-md-9" style="padding-left:25px;">
                                   <button type="submit" class="btn btn-primary">Save Type</button>
                               </div>
                        </div>
                       </div>



                        </form>
                       </div>
                    </div>

                    
                <!--  <CENTER>
                    <h5 style="color:green;">
                       <?php echo $this->session->flashdata('message'); ?>
                    </h5>
                </CENTER> -->
        
        <!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
        
                    <!-- <div class="row"> -->
                   <div class="col-md-7">
                     <div style="background-color:#fff;padding:10px; ">
       
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                <div class="col-md-12">
                </div>
                </div>
                </div>
            <table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
                <tr>
                 <th>Action</th>
                 <th>ID</th>
				 <th>Group</th>
				 <th>Expense Type</th>
                </tr>
                </thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                      </div>
                        </div>
		</div> <!-- end Row --->
                <!-- END CONTENT BODY -->
            <!-- </div> -->
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
        
 
  <!-- End user details -->
  </div>
  
   <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 

</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
//---------
 
      $('#example').dataTable( {
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/exptype_ajax",// json datasource
               },
        "columnDefs":[
	{"width":"15%","targets":0},
	{"width":"15%","targets":1},
	],
        "columns": [
            { "data": "action" },
            { "data": "exid"},
			{ "data": "exgroup"},
            { "data": "extype"},
       ]
  } );



      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/edit_extype",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 


  // $('#example tbody').on('click', '.change', function () {
  //       var Result=$("#myModal2 .modal-body");
  //    //   $(this).parent().parent().toggleClass('selected');
  //        var id =  $(this).attr('id');
  //        //alert(id);
  //        //alert(id);
  //       jQuery.ajax({
  //       type: "POST",
  //       url: "<?php echo base_url(); ?>" + "userprofile/change_password",
  //       dataType: 'html',
  //       data: {uid: id},
  //       success: function(res) {
  //       Result.html(res);
  //                   }
  //           });
    
  //       });     
    
    
    
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#idmsg').html(''); }, 3000);
		

 //});
  


</script>



