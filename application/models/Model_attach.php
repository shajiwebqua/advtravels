<?php
class Model_attach extends CI_Model{
	
    public function form_insert($newdt)
    {
       $this->db->insert('attached_vehicle', $newdt);  
    }

    public function view_vehicles(){

    $this->db->select('*');
    $this->db->from('attached_vehicle');
	$this->db->order_by('attached_vehicleid','desc');
    $query = $this->db->get();
    return $query->result();
}

 public function view_owner1($id){

    $this->db->select('*');
    $this->db->from('vehicle_ownerreg');
    $this->db->where('owner_id', $id);
    $query = $this->db->get();
    return $query->result();
}

 public function update_vehicle($id,$vdt)
  {
    $this->db->where('attached_vehicleid', $id);
    $this->db->update('attached_vehicle',$vdt); 
  } 

  public function delete_vehicle($id)
{
    $this->db->where('attached_vehicleid', $id);
    $this->db->delete('attached_vehicle'); 
    return $this->db->affected_rows();
}

}
?>