<?php include('application/views/common/header.php'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1 class='heading' style='color:#00adee; padding-bottom:15px; font-weight:bold;'>Account Groups
           </h1>
      
      <ol class="breadcrumb" style="margin-right:20px;font-size:13px;">
        <li><a href="#"></a></li>
      </ol>
	   
    </section>

    <!-- Main content -->
    <section class="content">
   
      <div class="row">
        <div class="col-md-5">
          <div class="box" style="border:1px solid #f5f3f3;">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Create Group</h3> 
				<center><input type='hidden' id="msg" value='<?php echo $this->session->flashdata('message'); ?>'></CENTER>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
	            <div class="col-md-12">
				 <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Account/save_group')?>" enctype="multipart/form-data">
						
						<?php
						$gcode=$this->db->select("MAX(acc_grp_code) as gcode")->from('acc_groups')->get()->row()->gcode;
						$gcode=$gcode+1;
						?>
		
						
						<div class="form-group" style='margin-top:15px;'>
						   <div class="row">
							  <label class="col-md-4 control-label">Group Code :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="grpcode" value= "<?=$gcode;?>"  style='background-color:#fff;' required readonly>
								</div>
							</div>
						</div>
						
						<div class="form-group" style='margin-top:15px;'>
						   <div class="row">
							  <label class="col-md-4 control-label">Group Name :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="grpname" required>
								</div>
							</div>
						</div>
							
				
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Group Under :</label>
								<div class="col-md-7">
									<select  class="form-control" name='grpunder'  id='grpunder' required>
									  <option value="">---type----</option>
									  <?php
										$gquery=$this->db->select("*")->from('acc_group_master')->order_by('acc_mgrp_id','asc')->get()->result();
										foreach( $gquery as $g)
										{
											echo "<option value='".$g->acc_mgrp_id."#".$g->acc_mgrp_nid."'>".$g->acc_mgrp_type."</option>";
										}
									  ?>
									</select>
								</div>
							</div>
						</div>
						
						<div class="form-group" style='margin-top:20px;margin-bottom:20px;'>
						   <div class="row">
							  <label class="col-md-4 control-label"></label>
								<div class="col-md-7">
									<input type="submit" class="btn btn-primary "  name="btnsubmit" value="Save Group">
								</div>
							</div>
						</div>
				</form>
				
			    </div>
        </div>

       </div>
    </div>
   </div>
   <!-- end create group---->
   <!-- group list ---->
    <div class="col-md-7">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Account Groups</h3> 
				
            </div>
            <!-- /.box-header -->
            <div class="box-body" style='min-height:450px;'>
              <div class="row">
	            <div class="col-md-12">
				<table class="table table-hover table-bordered" id="example" width='100%' border=0 style="font-size:14px" >
                <thead>
                <tr>
				<th width='8%'>Edit</th>
                 <th width='10%'>ID</th>
				 <th>Code</th>
                 <th>Description</th> 
                 <th>Parent</th>
				 <th>Type</th>
                </tr>
                </thead>
				<tbody>
					<?php
					foreach($result as $r)
					{
						if($r->acc_grp_id>6)
						{
							$edit="<a href='#' id='".$r->acc_grp_id."' class='abtn abtn-edit edit' data-toggle='modal' data-target='#editModal'><span class='glyphicon glyphicon-edit' aria-hidden='true' style='font-size:16px;'></span></a>";
							$del=anchor('Account/delete_group/'.$r->acc_grp_id,'<span class="glyphicon glyphicon-trash" aria-hidden="true" style="font-size:14px;color:red;"></span>', array('id' =>'del_conf'));
							$edit1=$edit." ".$del;
						}
						else
						{
							$edit1="";
						}
					?>
						<tr>
						 <td align='center' class='dtable'> <?=$edit1;?>	 </td>
						 <td><?=$r->acc_grp_id;?></td>
						 <td><?=$r->acc_grp_code;?></td>
						 <td><?=$r->acc_grp_description;?></td> 
						 <td><?=$r->acc_mgrp_type;?></td>
						 <td><?=$r->acc_mgrp_nature;?></td> 
						</tr>
					<?php
					}
					?>	
				</tbody>
				</table>
			  </div>
        </div>
       </div>
    </div>
   </div>
     
   
</div>
</section>
</div>
<!---modal dialog start ---->

<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document" >
    <div class="modal-content" style='top:50px;border:1px solid #b7b7b7;'>
      <div class="modal-body"  style='padding:0px;' >
	  </div>
	    
	  
    </div>
  </div>
</div>

<!-- modal dialog end ----->

          
  <!-- /.content-wrapper -->
  
<?php include('application/views/common/footer.php'); ?>
   
<script type="text/javascript">

$("#example").dataTable({"ordering":false});

//sweet alert start-----------------------

var msg=$("#msg").val().split('#');
if(msg[0]=='1')
{
	swal('',msg[1],'success'); //Saved
}

else if(msg[0]=='2')
{
	swal('',msg[1],'success'); //Updated
}
else if(msg[0]=='3')
{
	swal('',msg[1],'success'); //Removed.
}
else if(msg[0]=='4')
{
	swal('',msg[1],'error'); //Try Again.
}

//sweet alert end -----------------------
	$('#example tbody').on('click', '.edit', function () {
        var Result=$("#editModal .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
        var id =  $(this).attr('id');
		
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Account/edit_group",
        dataType: 'html',
        data: {gid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 
		

      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

</script>  
