<?php
include('application/views/common/header.php');
?>
  <style>
  .dashbtn
  {
	  width:75px;
	  height:75px;
	  background-color:#f4f4f4;
	  border:1px solid #fff;
	  /*border-radius:10px;*/
	  padding:5px;
	  text-align:center;
  }
  
.sh4
{
 border-bottom:1px solid #e4e4e4;
 width:100%;
 color:#00adee;
 padding-bottom:5px;
 margin-top:0px;"
}

/* tab pages------------------------------- */

 <style>
 .nav-tabs {
    margin: 0;
    padding: 0;
    border: 0;    
}
.nav-tabs > li > a {
    background: #f2f2f2;
    border-radius: 0;
    //box-shadow: inset 0 -8px 7px -9px rgba(0,0,0,.4),-2px -2px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li.active > a:hover {
    background: #fff;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li > a:hover {
    background: #e4e4e4;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}
/* Tab Content */
.tab-pane {
    background: #fff;
    //box-shadow: 0 0 4px rgba(0,0,0,.4);
    border-radius: 0;
   // border:1px solid #e4e4e4;
    text-align: left;
    padding: 10px;
}

.cli
{
padding:5px 0px 5px 0px;
margin-top:3px;

}

.selclass > div:hover
{
background-color:#f4f4f4;
}

/*------------------------------------------------*/

.sptkm
{
font-size:22px;
font-weight:700;	
}



</style>
 

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

	  <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#3356b7'><b>Driver's Performance</b>
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
		<label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
		<div class='row' style="padding:3px 20px 3px 20px;" >
		
		</div>

        <!-- Main content -->
    <section class="content">
	
	<input type='hidden' name='tkm' id='tkm' value='<?php echo $this->session->userdata('tkm');?>'>
		<input type='hidden' name='drna' id='drna' value='<?php echo $this->session->userdata('drna');?>'>
		<input type='hidden' name='tkm1' id='tkm1' value='<?php echo $this->session->userdata('tkm1');?>'>
		<input type='hidden' name='dr_id' id='dr_id' value='<?php echo $this->session->userdata('drvid');?>'>
		<input type='hidden' name='dr_id1' id='dr_id1' value='<?php echo $this->session->userdata('drvid1');?>'>
		
	<div class='row'>	
	<div class='col-md-12'>
	
	<div style='padding-left:3px;'> 
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item active" id='tb1' >
						<a class="nav-link tp1" data-toggle="tab" href="#drptab1"  role="tab"><i class="fa fa-compass"></i> Driver's Monthly and Yearly Performance Chart</a>
					</li>
					<!--<li class="nav-item " id='tb2'>
						<a class="nav-link tp1 " data-toggle="tab" href="#drptab2"  role="tab"> <i class="fa fa-compass"></i> Yearly Driver Performance Chart</a>
					</li> -->
					
                 </ul>
	<div class="tab-content">
	
	<?php
			$mon=array("1"=>"January","2"=>"February","3"=>"March","4"=>"April","5"=>"May","6"=>"June","7"=>"july","8"=>"August","9"=>"September","10"=>"October","11"=>"November","12"=>"December");
				
				if($dmon==date('m'))
				{
					$pmon=date('F',strtotime("-1 month"));
					$cmon=date('F');
					$cyear=date('Y');
					$pyear=date('Y');
				}
				else
				{
					if($dmon==1)
					{
						$pmon='December';
						$pyear=date('Y')-1;
						
						$cmon=$mon[$dmon];
						$cyear=date('Y');
					}
					else
					{
					$pmon=$mon[$dmon-1];
					$cmon=$mon[$dmon];
					
					$cyear=date('Y');
					$pyear=date('Y');
					}
				}

			$y1=0;
			
			$dr1=array();
			$i=0;
			$s="";
			for($x=0;$x<count($pr);$x++)
			{
				$s="";
				for ($y=0;$y<12;$y++)
				{
					$s.=$pr[$x][$y].",";
				}
				$s=substr($s,0,strlen($s)-1);
				$dr1[$i++]=$s;
			}
			
			$dnam=array();
			$z=1;
			foreach($drvnames as $dv)
			{
				$dnam[$z]=$dv->driver_image.",".$dv->driver_name.",".$dv->driver_mobile.",".$dv->driver_id;
				$z++;
			}

			?>
	
		<div class="tab-pane active" id="drptab1" role="tabpanel"> <!-- TAB PAGE 1 ---------------------------------------------------->
		<div class='row' style="padding:5px 15px 5px 15px;">
		<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4;">
	    <div class="box-header" style='background-color:#fff;' >
		<div class='col-md-5'>
          <h3 class="box-title"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Monthly Performance Details</h3>
		  </div>
		  <div class='col-md-7' style='text-align:right; '>
		   <form action="<?php echo base_url('Driver/performs');?>" method="post">
            <label name="message" class="col-md-4 control-label" style='margin-top:5px;' >Select Month :</label>
			 <div class='col-md-4'>
				<select name='dmonth' class='form-control'>
					<option value='1' <?php if (1==$dmon) echo "selected";?>  >January</option>
					<option value='2' <?php if (2==$dmon) echo "selected";?> >February</option>
					<option value='3' <?php if (3==$dmon) echo "selected";?> >March</option>
					<option value='4' <?php if (4==$dmon) echo "selected";?> >April</option>
					<option value='5' <?php if (5==$dmon) echo "selected";?> >May</option>
					<option value='6' <?php if (6==$dmon) echo "selected";?> >June</option>
					<option value='7' <?php if (7==$dmon) echo "selected";?> >July</option>
					<option value='8' <?php if (8==$dmon) echo "selected";?> >August</option>
					<option value='9' <?php if (9==$dmon) echo "selected";?> >September</option>
					<option value='10' <?php if (10==$dmon) echo "selected";?> >October</option>
					<option value='11' <?php if (11==$dmon) echo "selected";?> >November</option>
					<option value='12' <?php if (12==$dmon) echo "selected";?> >December</option>
				</select>
	        </div>
			<div class='col-md-2' style='padding-right:0px;'>
			<select name='dyear' class='form-control'>
			<?php
			for($x=2016;$x<=(date('Y')+1);$x++)
			{
			echo "<option value='".$x."'"; if ($x==date('Y')) echo "selected";echo">".$x."</option>";
			}
			?>
			</select>
			</div>
			<div class='col-md-2'>
			<input type='submit' value='Get' class='btn btn-warning'>
			</div>
          </form>
		  </div>
        </div><!-- /.box-header -->
		
        <div class="box-body">
		 <div class='row' style='padding:15px;'>
		 <div class='col-md-12' >

           <!-- <div class="box-header with-border" style='padding-top:0px;'>
              <h3 class="box-title" style='font-size:14px;'>Performance Chart</h3>
            </div>  -->

		<div class='row'>
		<div class='col-md-6'>
			
		<table height='100%' width='100%' style='text-align:center;'>
		  <tr><td colspan='3' height='25px;' style='text-align:left;border-bottom:1px solid #e4e4e4;'>Performar of the Month</td></tr>
		  <tr><td colspan='3'height="10px;" ></td></tr>
		  <tr><td width="50px" style='padding:5px;'><div style='width:20px;height:20px;background-color:#00a65a;'></div></td>
		  <td colspan='2' style='text-align:left;'><b><?php echo $cmon . " - ". $cyear ;?> </b></tr>
		  
		   <?php 
		   $mdrkm1=0;
			$did1=$this->session->userdata('drvid1');
			
			 foreach($monthlykm1 as $m1)
			 {
				if($m1->trip_driver_id==$did1)
				{
					$mdrkm1=($m1->endkm-$m1->startkm);
					break;
				}					
			 }

			$drrow1=$this->db->select('*')->from('driverregistration')->where('driver_id',$did1)->get()->row(); 
			if($drrow1)
			{
				$drimg1=$drrow1->driver_image;
				$drname1=$drrow1->driver_name;
			}
			else
			{
				$drimg1=base_url('upload/images/placeholder.png');
				$drname1="....................";
			}
		  ?>
		  <tr><td></td><td width="100px" style='padding:5px;'><img src='<?php echo $drimg1;?>' width='100px' height='105px'></td>
		  <td style='text-align:left;padding-left:20px;'> 
		  <?php  
		  echo strtoupper($drname1);
		  echo "<br><span style='font-size:20px;'>KM : </span> <span class='sptkm'>". $mdrkm1."</span> ";
		  ?>
		  		  
		  </td></tr>
		  
		  <tr><td colspan='3' height="10px;" ></td></tr>
		  
		  </table>
		</div>
		
		
		<div class='col-md-6'>
		<table height='100%' width='100%' style='text-align:center;'>
		<tr><td colspan='3' height='25px;' style='text-align:left;border-bottom:1px solid #e4e4e4;'></td></tr>
		<tr><td colspan='3'height="10px;" ></td></tr>
		 <tr><td width="50px" style='padding:5px;'><div style='width:20px;height:20px;background-color:#d2d6de;'></div></td>
			<td colspan='2' style='text-align:left;'><b><?php echo $pmon." - ". $pyear ;?></b></tr>
			<?php 
			$did=$this->session->userdata('drvid');
			$mdrkm2=0;
			foreach($monthlykm2 as $m2)
			 {
				if($m2->trip_driver_id==$did)
				{
					$mdrkm2=($m2->endkm-$m2->startkm);
					break;
				}					
			 }
			
			
			$drrow=$this->db->select('*')->from('driverregistration')->where('driver_id',$did)->get()->row(); 
			if($drrow)
			{
				$drimg2=$drrow->driver_image;
				$drname2=$drrow->driver_name;
			}
			else
			{
				$drimg2=base_url('upload/images/placeholder.png');
				$drname2="....................";
			}
		  ?>	

		  <tr><td></td><td width="50px" style='padding:5px;'><img src='<?php echo $drimg2;?>' width='100px' height='105px'></td>
		  <td style='text-align:left;padding-left:20px;'>
		  <?php  echo strtoupper($drname2);
		   echo "<br><span style='font-size:20px;'>KM : </span><span class='sptkm'>".$mdrkm2."</span> ";
		  ?></td> </tr>
		  </table>
			
		</div>
		</div>
		
		<!-- chart begin --------------------------------------------------------------------------------->
		
		
		<div class="box-header with-border" style='padding-top:0px; margin-top:20px;'>
            <h3 class="box-title" style='font-size:14px;color:blue;'> Chart</h3>
        </div>  
		
           <div class="chart" style='margin-top:25px;'>
                <canvas id="barChart" style="height:350px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
		  </div>
		  </div>
		
		</div>
		</div>
		</div>
				
		
		<!--<div class="tab-pane " id="drptab2" role="tabpanel"> 
		</div> -->
				
	</div>
	
	</div>
	</div>
	</div>
	
	
	<div class='row year1' style="padding:5px 15px 5px 15px;">
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header" style='background-color:#fff;'>
		<div class='col-md-6'>
          <h3 class="box-title"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Yearly Performance details</h3>
		  </div>
		  <div class='col-md-6' style='text-align:right'>
		   <form action="#" method="post">
            <div class="input-group">
             <label name="message" class="control-form" ></label>
              <!--<span class="input-group-btn">
                <a href="<?php echo site_url('Trip/view_trip');?>"><button type="button" class="btn btn-primary btn-flat">More</button></a>
              </span> -->
            </div>
          </form>
		  </div>

        </div><!-- /.box-header -->

		<!-- LINE CHART STARTING ------------>
		
        <div class="box-body">

			<?php 
			
			for($x1=1;$x1<=count($dr1);$x1++)
			 {
				  $dnam1=explode(",",$dnam[$x1]);
				  
			 ?>
			 <div class='row' style='padding:15px;'>
			 <div class='col-md-12'>
			 <div class='col-md-3'>
			 
			 <table width='100%' style='text-align:center'>
			 <tr><td height='20px;'></td></tr>
			 <tr><td><img src='<?php echo $dnam1[0];?>' width='150px' height='155px'></td></tr>
			 <tr><td height='25px;'><?php echo strtoupper($dnam1[1]);?></td></tr>
			 <tr><td height='25px;'><i class="fa fa-mobile fa-lg" aria-hidden="true"></i> &nbsp;&nbsp;  <?php echo $dnam1[2];?></td></tr>
			 <tr><td height='25px;'></td></tr>
			 </table>
			 
			 </div>
			 <div class='col-md-2' style='height:200px;margin-top:20px;'>
			 <?php
			 $z=0;
			 $lkm=array();
			 
			 foreach($totalkm as $t1)
			 {
				$lkm[$z][0]=$t1->trip_driver_id;
				$lkm[$z][1]=($t1->endkm-$t1->startkm);
				$z++;
			 }
			 	
				array_multisort(array_column($lkm, 1), SORT_DESC, $lkm);
				$ltkm=$lkm[0][1];
				
			    foreach($totalkm as $tk)
				{				
					if($tk->trip_driver_id==$dnam1[3])
					{
						$dkm=($tk->endkm-$tk->startkm);
						if($ltkm==$dkm)
						{
						echo "<center><span> Running KM :<span></center>";
						echo "<center><span style='font-size:23px;color:green;font-weight:700;'>".$dkm ."</span></center>";
						break;
						}
						else
						{
						
						echo "<center><span> Running KM :<span></center>";
						echo "<center><span style='font-size:20px;'>".$dkm ."</span></center>";
						break;
						}
					}
				}
			 ?>
			 </div>
			 
			 <div class='col-md-7'>
				<!-- LINE CHART -->
              <div class="chart">
                <canvas id="lineChart<?php echo $x1;?>" style="height:250px"></canvas>
              </div>
          <!-- /.Line End -->
				</div>
					</div>
				</div>
				<?php
			  }
			  ?>

        </div> 
		<!--LINE CHART ENDING-->
       
	</div>
	</div>
		
</section>

    <!-- content wrapper -->
</div>
     <?php
  include('application/views/common/footer.php');
  ?>
  
</body>
</html>

<script type='text/javascript'>


  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- BAR CHART -
    //--------------
 
   var d11=$("#tkm").val();
   var d2=d11.split(",");
   
   var d12=$("#tkm1").val();
   var d21=d12.split(",");
      
   var dr_na=$("#drna").val();
   var lbl=dr_na.split(",");

   var a1=["January", "February", "March", "April", "May", "June", "July"];

    var areaChartData = {
		
      labels: lbl,
	  
      datasets: [
        {
          label: "<?php echo $pmon; ?>",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: d2
		  },
		  
        {
          label: "<?php echo $cmon; ?>",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          //data: [28, 48, 40, 19, 36, 27, 20]
		  data: d21
        }
      ]
    };
	
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

		
    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
  });
  

$(function()
{

	
	//- LINE CHART -
    //--------------
	
	var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };
	
	<?php 
	for($x=1;$x<=count($dr1);$x++)
	{ 
	?>
	
    var lineChartData = {
		
      labels: ["January", "February", "March", "April", "May", "June", "July","September","October","November","December"],
	  
      datasets: [
        {
          //label: "<?php echo date('F',strtotime("-1 month"));?>",
		  label : ["January", "February", "March", "April", "May", "June", "July","September","October","November","December"],

          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
         //data: [28, 48, 40, 19, 36, 27, 20]
		 data: [ <?php echo $dr1[$y1];?> ]
		 }
      ]
    };
	
	
    var lineChartCanvas = $("#lineChart<?php echo $x;?>").get(0).getContext("2d");
    var lineChart = new Chart(lineChartCanvas);
    var lineChartOptions = areaChartOptions;
    lineChartOptions.datasetFill = false;
    lineChart.Line(lineChartData, lineChartOptions);
    	
	<?php $y1++;}?>
});
   
	

</script>
</body>
</html>

  
  