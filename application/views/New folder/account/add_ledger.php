<?php
include('application/views/common/acc_header.php');
?>
  <style>
  .dashbtn
  {
	  width:75px;
	  height:75px;
	  background-color:#f4f4f4;
	  border:1px solid #fff;
	  /*border-radius:10px;*/
	  padding:5px;
	  text-align:center;
  }
  
.sh4
{
 border-bottom:1px solid #e4e4e4;
 width:100%;
 color:#00adee;
 padding-bottom:5px;
 margin-top:0px;"
}
.right1
{
	text-align:left;
	padding:0px;
}
  </style>
  
 <link rel="stylesheet" href="<?php echo base_url('assets/chart/css/material-charts.css');?>">
 <script src="<?php echo base_url('assets/chart/js/material-charts.js');?>"></script>
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
  
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#00adee'> Dashboard 
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
     <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
        <!-- Main content -->
    <section class="content">
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header">
          <h3 class="box-title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Create Ledgers</h3>
		  <label id='mes' ><?php echo $this->session->flashdata('message');?></label>
		  </div>
		
        <div class="box-body">
			<div class='row' style='margin-top:10px;'>
			<div class='col-md-4' >
			<label><b> New ledger Details</b></label>
			<hr style='margin:0px;'>
			</div>
			<div class='col-md-8' >
			<label><b>Ledger List</b></label>
			<hr style='margin:0px;'>
			</div>
			</div>
				
        	<div class='row'style='padding:10px;'>
			<div class='col-md-4' style='border-right:1px solid #e4e4e4; background-color:#f5e3e3;padding-top:10px;'>
			<div style='padding-right: 10px;'>
				 <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Account/add_ledger')?>" enctype="multipart/form-data">
                  
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 right1" style='text-align:right;padding:0px;'>Ledger :</label>
							  
								<div class="col-md-9">
									<input type="text" class="form-control"  name="lname" value='' required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 " style='text-align:right;padding:0px;'>Under :</label>

								<div class="col-md-9">
								
									<select name='grpunder' class='form-control' required>
									<option value=''>----------</option>
									<?php
									$agrps=$this->db->select('*')->from('acc_groups')->get()->result();
									foreach($agrps as $a1)
									{
									?>
									<option value='<?=$a1->acc_grp_name;?>'><?=$a1->acc_grp_name;?></option>
									<?php
									}
									?>
									</select>
								</div>
							</div>
						</div>		
				
					<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 " style='text-align:right;padding:0px;'>Name :</label>

								<div class="col-md-9">
									<input type="text" class="form-control"  name="mname">
								</div>
							</div>
						</div>	

						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 " style='text-align:right;padding:0px;'>Address :</label>
								 
								<div class="col-md-9">
									<textarea rows='3' class="form-control"  name="address"></textarea>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 " style='text-align:right;padding:0px;'>Op.Balance:</label>
							
								<div class="col-md-9">
									<input type="text" class="form-control"  name="opbal" value='0'>
								</div>
							</div>
						</div>

						<hr style='margin:15px 0px 15px 0px;'>
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3"></label>
								<div class="col-md-9" style='margin-bottom:15px;'>
									<input type="submit" class="btn btn-primary" name="btnsubmit" value='Create Ledger'>
								</div>
							</div>
						</div>
					</form>
					</div>
			</div>
			
			<div class='col-md-8'>
			
			<table class="table table-striped table-hover table-bordered"  width='100%' id='example'>
			   <thead><tr >
			   	<th width='40px'>Action</th>
			     <th width='40px'>ID</th>
				 <th >Ledger_Name</th>
				 <th >Group</th>
				 <th >Address</th>
				 <th width='60px'>OP_bal.</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$aledgers=$this->db->select('*')->from('acc_ledgers')->order_by('acc_ledg_id','desc')->get()->result();
				foreach ($aledgers as $al)
				{
				?>
				<tr >
			   	<td ><a href='#' data-toggle='modal' data-target='#myModalL'><button class='edit btn btn-warning btn-xs' id='<?=$al->acc_ledg_id;?>'>&nbsp;&nbsp;Edit&nbsp;&nbsp;&nbsp;</button></a>
				<a href="<?php echo base_url('Account/delete_ledger/'.$al->acc_ledg_id );?>" class='conf1'><button class='conf btn btn-danger btn-xs'>Delete</button></a>
				</td>
				 <td><?=$al->acc_ledg_id;?></td>
				 <td><?=$al->acc_ledg_lname;?></td>
				 <td><?=$al->acc_ledg_under;?></td>
				 <td><?="Name:".$al->acc_ledg_name."<br>Address:".$al->acc_ledg_address;?></td>
				 <td><?=$al->acc_ledg_opbalance;?></td>
				</tr>
				<?php }
				?>
				</tbody>
			</table>
			</div>
           
        </div>
	</div>
	</div>
</section>

    <!-- content wrapper -->
	
	<div class="modal fade draggable-modal" id="myModalL" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body" >
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Edit Ledger</h4>
            </div>
          </div>
          </div>
        <!-- /.modal-dialog -->
        </div>
	</div>
	
	
</div>
     <?php
  include('application/views/common/acc_footer.php');
  ?>
   
</body>
</html>
  <script type="text/javascript">
  $("#mes").hide();
  var swalop=$("#mes").html().split("#");
  
  if($("#mes").html()!="")
  {
		if(swalop[0]==1)
			swal("Saved.!",swalop[1],"success");
		else if(swalop[0]==2)
			swal("Updated.!",swalop[1],"success");
		else if(swalop[0]==3)
			swal("Removed.!",swalop[1],"success");
		else if(swalop[0]==4)
			swal("Try Again.",swalop[1],"error");
	  $("#mes").html("");
  }
  
  
  /*$("#mes").hide();
  if($("#mes").html()!="")
  {
	  swal("Saved.!",$("#mes").html(),"success");
	  $("#mes").html("");
  }*/
   
  
  function confirm_form()
  {
	  return confirm("Are you sure, submit this details?");
  }
  
  $('#example').dataTable( {
		 "ordering":false,
  });
  
  $('#example tbody').on('click', '.edit', function () 
  {
        var Result=$("#myModalL .modal-body");
        var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Account/edit_ledger",
        dataType: 'html',
        data: {lid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
        }); 
$('#example tbody').on('click', '.conf', function ()
{
        return confirm("Are you sure, delete this ledger?");
    });

 
</script>
</body>
</html>

  
  