<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attached_vehicle extends CI_Controller {

function __construct(){
parent::__construct();
$user = $this->session->userdata('userid');
               if (!isset($user))
               { 
                       redirect('/');
               } 
               else
               { 
                   return true;
               }
$this->load->helper(array('form', 'url'));
$this->load->model('Model_userprofile');
}

public function index()
{
   $this->load->view('admin/add_users');
}
  
public function add_new_vehicle()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('vehicleno', 'vehicleno', 'required');
$this->form_validation->set_rules('ownername', 'ownername', 'required');
$this->form_validation->set_rules('type', 'type', 'required');
$this->form_validation->set_rules('payperiod', 'payperiod', 'required');
$this->form_validation->set_rules('amount', 'amount', 'required');
$this->form_validation->set_rules('status', 'status', 'required');

 if($this->form_validation->run())
        {
    $vehicleno=$this->input->post('vehicleno');
    $ownername=$this->input->post('ownername');
    $type=$this->input->post('type');
    $payperiod=$this->input->post('payperiod');
    $amount=$this->input->post('amount');
    $status=$this->input->post('status');
          


       $userdt = array(
                'attached_vehicleno' => $vehicleno,
                'attached_ownerid'=>$ownername,
                'attached_type' => $type,
        'attached_payperiod' =>$payperiod,
                'attached_amount' =>$amount,
                'attached_status' =>$status
               );
         $this->load->Model('Model_attachedvehicle');
        $this->Model_attachedvehicle->form_insert($userdt);
        $this->session->set_flashdata('message', '<font color="green">Vehicle details successfully added</font>');
        redirect('Attached_vehicle/view_vehicle');  
        }
       
  else{
         $this->session->set_flashdata('message', 'Data Not Inserted Successfully');
        //$data['message']="Data Inserted Successfully";

      redirect('Attached_vehicle/view_vehicle');
       }  
}
 
 public function get_add_vehicle()
  {
    $this->load->model('Model_attachedvehicle');
   $data['result']= $this->Model_attachedvehicle->get_ownerid();
   $this->load->view('admin/add_vehicle1.php',$data);
  }   
      
       
 public function view_vehicle()
  {
   $this->load->view('admin/view_vehicles');
  }
        
                
public function vehicle_ajax()
{

        $this->load->model('Model_attachedvehicle');
        $results=$this->Model_attachedvehicle->view_vehicle();
    
        $data1 = array();
        foreach($results  as $r) 
               {
             $edit="<a href='#myModal2' id='$r->attached_id' data-toggle='modal' class='edit open-AddBookDialog2'><center><button class='btn btn-primary btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
     $mm="<center><button class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></button></center>";
      $del=anchor('Attached_vehicle/del_entry/'.$r->attached_id, $mm, array('id' =>'del_conf'));    

      $date1= $r->attached_date;
      $date2= date('d-m-Y', strtotime($date1));
if($r->attached_status==1)
    {
      $st="<font color='green'>Active</font>";
    }
    else
    {
      $st="<font color='red'>Inactive</font>";
    }
                      array_push($data1, array(
                            "edit"=>"$edit",
                           "delete"=>"$del",
                            "date"=>"$date2",
                            "vehicleno"=>"$r->attached_vehicleno",
                            "ownername"=>"$r->owner_name",
              "type"=>"$r->attached_type",
                            "payperiod"=>"$r->attached_payperiod",
                            "amount"=>"$r->attached_amount",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

    public function edit_vehicle() 
  {
  $m="";
    $f="";
    
        $data=$this->session->all_userdata();
        $id=$this->input->post('vid');
         $query = $this->db->select('*')->from('attached_vehicledetails')->where('attached_id',$id)->get();
        $row = $query->row();
        $query1 = $this->db->select('*')->from('vehicle_ownerreg')->get();
        $row1 = $query1->result();
       
    echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Vehicle Details</font></h4>
    </div>
    <br>

           <form  class='form-horizontal' role='form' method='post' action='". site_url('Attached_vehicle/update_vehicle'). "' enctype='multipart/form-data'>
                    <!-- text input -->
                    
                    <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Vehicle No : </label>
                    <div class='col-md-9'>  
          <input type='hidden' name='id'  value='$row->attached_id'/>
          <input type='text' name='vehicleno' class='form-control'   value='$row->attached_vehicleno'/>
          </div>                   
          </div>
                    

        <div class='form-group'>
                      <label class='col-md-3  control-label'>Owner Name</label>
          <div class='col-md-9'>
                      <select type='text' name='ownername' class='form-control'>";
        echo"
            <option value='0'> ---------- </option>";
                
                
                foreach($row1 as $v1)
                {
                  
                    echo "<option value='$v1->owner_id' selected>$v1->owner_name</option>";
                   echo "<option value='$v1->owner_id'>$v1->owner_name</option>";
                  
                } 
                
              echo " </select>
                               </div>                     
                       </div>

         <div class='form-group'>
                      <label class='col-md-3  control-label'>Type</label>
          <div class='col-md-9'>
                      <select type='text' name='type' class='form-control'>";
 
             echo '<option value="rent" ';if($row->attached_type=='rent')echo "selected"; echo">Rent</option>
               </select>
                               </div>                     
                       </div>

             <div class='form-group'>
                      <label class='col-md-3  control-label'>Pay Period</label>
          <div class='col-md-9'>
                      <select type='text' name='payperiod' class='form-control'>";
 
             echo '<option value="daily" ';if($row->attached_payperiod=='daily')echo "selected"; echo'>Daily</option>
             <option value="weekly" ';if($row->attached_payperiod=='weekly')echo "selected"; echo'>Weekly</option>
              <option value="monthly"';if($row->attached_payperiod=='monthly')echo "selected"; echo ">Monthly</option>
              </select>
                               </div>                     
                       </div>

               <div class='form-group'>
                      <label class='col-md-3  control-label'>Amount</label>
          <div class='col-md-9'>
                      <input type='text' name='amount' class='form-control'   value='$row->attached_amount'/>
          
                               </div>                     
                       </div>

            

                      <div class='form-group'>
                      <label class='col-md-3  control-label'>Status </label>
          <div class='col-md-9'>
                      <select type='text' name='status' class='form-control'>";
 
             echo '<option value="0" ';if($row->attached_status=='0')echo "selected"; echo'>Inactive</option>
              <option value="1"';if($row->attached_status=='1')echo "selected"; echo ">Active</option>
              </select>
                               </div>                     
                       </div>
     

     
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-9'>
                <input type='submit' class='btn btn-primary' value='Update'/>
                </div>
                </div>                                
           </form> <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div> ";
  
  
    }
public function update_vehicle()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('vehicleno', 'vehicleno', 'required');
$this->form_validation->set_rules('ownername', 'ownername', 'required');
$this->form_validation->set_rules('type', 'type', 'required');
$this->form_validation->set_rules('payperiod', 'payperiod', 'required');
$this->form_validation->set_rules('amount', 'amount', 'required');
$this->form_validation->set_rules('status', 'status', 'required');

 if($this->form_validation->run())
   {
            
    $vehicleno=$this->input->post('vehicleno');
    $ownername=$this->input->post('ownername');
    $type=$this->input->post('type');
    $payperiod=$this->input->post('payperiod');
    $amount=$this->input->post('amount');
    $status=$this->input->post('status');
    $id=$this->input->post('id');
    
    $usr_newdt = array('attached_vehicleno' => $vehicleno,
                'attached_ownerid'=>$ownername,
                'attached_type' => $type,
				'attached_payperiod' =>$payperiod,
                'attached_amount' =>$amount,
                'attached_status' =>$status
               );

        $this->load->model('Model_attachedvehicle');
        $this->Model_attachedvehicle->update_vehicle($id,$usr_newdt);
        $this->session->set_flashdata('usermsg',  '<font color="green">Vehicle details Updated..!</font>');
        redirect('Attached_vehicle/view_vehicle');

       }
        else 

        {
           redirect('Attached_vehicle/view_vehicle');
        }                   
}
  

  public function del_entry()
  {
    
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_attachedvehicle');
            $result=$this->Model_attachedvehicle->delete_vehicle($id);
            if($result)
            {
                    $this->session->set_flashdata('usermsg', '<font color="green">Vehicle details has been deleted..!</font>');
                    redirect('Attached_vehicle/view_vehicle');
            }
           else
           {
            $this->session->set_flashdata('usermsg', '<font color="red">Please try again</font>');
            redirect('Attached_vehicle/view_vehicle');
           }
  }
  
}