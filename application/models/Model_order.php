<?php
class Model_order extends CI_Model{
	

    public function __construct(){
        parent::__construct();
    }

    public function insert($table, $values)
    {
       $this->db->insert($table, $values);  
    }

    public function select_all($table,$column = "*")
	{
		$this->db->select($column);
		$this->db->from($table);
		$query = $this->db->get();
		return $query->result();
	}

	public function select($table,$where, $column = "*")
	{
		$this->db->select($column);
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	public function delete($table, $where){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function view_enqueryPDF()
	{
	    $this->db->select('*');
	    $this->db->from('enquiry');
	    $this->db->order_by("enquiry_id","desc");
	    $query = $this->db->get();
	    return $query->result();
	}

}