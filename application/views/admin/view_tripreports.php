<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}
</style>

 <!-- Content Wrapper. Contains page content -->
 
<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Trip list Reports</b></h1>
  <label style="background-color:#cecece;width:100%;height:1px;margin-top:0px;"></label>
 </section>
  
<!-- Content Header  end -->	  
	  
  <section class="content"> 
 <!--  <div style="color:blue;font-size:12px;"> Select Trip details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Print,Share,Save,View,Delete)  </div> -->
  <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>  
  <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details-->
<div style="background-color:#fff;border-radius:10px;padding:15px; ">
		
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
               
<div class="page-content">
				 
<!--- search running trips ---------------------------------------- -->
	<form class="form-horizontal" role="form" method="POST" id="myformr" action="<?php echo base_url('Reports/pdf_rtrip')?>" >
		<div style='margin-top:20px;'>
		<label ><b>Running Trips</b></label>
		<label style="background-color:#cecece;width:100%;height:1px;margin-top:0px;"></label>
			<div class="row">
                  <div class="col-md-10">
                    <div class="form-group">
                    	<br><label class="col-md-2 control-label" style="padding-top:3px;" >Select Date : </label>
						<div class="col-md-2">
						<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" readonly id="datepickerr1" value="<?php echo date('d-m-Y'); ?>">
						</div> 
						</div>
						<div class="col-md-1" style="width:20px;padding:4px 0px;">=>
						</div>
						<div class="col-md-2">
						<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" readonly id="datepickerr2" value="<?php echo date('d-m-Y'); ?>">
						</div>
						</div>
						<div class="col-md-2" style='border-right:1px solid #c2c2c2;'>
							<input class="form-control btn btn-primary" style="text-align:center;" type="submit" value="Get Reports" id="btngetr">
						</div>
						<div class="col-md-2">
							<input class="form-control btn btn-primary btnrA" style="text-align:center;" type="submit" value="Get All" id="r1">
						</div>
					</div>
				</div> 
			</div>
	</div>
		</form>
<!-- search end running trips ------------------------------------- -->
<!--- search upcoming trips ---------------------------------------- -->

	<form class="form-horizontal" role="form" method="POST" id="myformu" action="<?php echo base_url('Reports/pdf_utrip')?>" >
	<div style='margin-top:20px;'>
	<label ><b>Upcoming Trips</b></label>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
			<div class="row">
                  <div class="col-md-10">
                    <div class="form-group">
                    	<br><label class="col-md-2 control-label" style="padding-top:3px;" >Select Date : </label>
						<div class="col-md-2">
						<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" readonly id="datepickeru1" value="<?php echo date('d-m-Y'); ?>">
						</div> 
						</div>
						<div class="col-md-1" style="width:20px;padding:4px 0px;">=>
						</div>
						<div class="col-md-2">
						<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" readonly id="datepickeru2" value="<?php echo date('d-m-Y'); ?>">
						</div>
						</div>
						<div class="col-md-2">
							<input class="form-control btn btn-primary" type="submit" value="Get Reports" id="btngetu">
						</div>
						<div class="col-md-2">
							<input class="form-control btn btn-primary btnuA" type="submit" value="Get All" id="r2">
						</div>
					</div>
				</div> 
			</div>
		</div>
	</form>
<!-- search end upcoming trips ------------------------------------- -->
<!--- search completed trips ---------------------------------------- -->

<form class="form-horizontal" role="form" method="POST" id="myformco" action="<?php echo base_url('Reports/pdf_cotrip')?>" >
<div style='margin-top:20px;'>
	 <label ><b>Completed Trips</b></label>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
			<div class="row">
                  <div class="col-md-10">
                    <div class="form-group">
                    	<br><label class="col-md-2 control-label" style="padding-top:3px;" >Select Date : </label>
						<div class="col-md-3">
						<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" readonly id="datepickerco1" value="<?php echo date('d-m-Y'); ?>">
						</div> 
						</div>
						<div class="col-md-1" style="width:20px;padding:4px 0px;">=>
						</div>
						<div class="col-md-3">
						<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" readonly id="datepickerco2" value="<?php echo date('d-m-Y'); ?>">
						</div>
						</div>
						<div class="col-md-2">
							<input class="form-control btn btn-primary" type="submit" value="Get Reports" id="btngetco">
						</div>
					</div>
				</div> 
			</div>
			</div>
		</form>
<!-- search end completed trips ------------------------------------- -->
<!--- search closed trips ---------------------------------------- -->

	<form class="form-horizontal" role="form" method="POST" id="myformcl" action="<?php echo base_url('Reports/pdf_cltrip')?>" >
	<div style='margin-top:20px;'>
	 <label ><b>Closed Trips</b></label>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
		<div class="row">
                  <div class="col-md-10">
                    <div class="form-group">
                    	<br><label class="col-md-2 control-label" style="padding-top:3px;" >Select Date : </label>
						<div class="col-md-3">
						<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" readonly id="datepickercl1" value="<?php echo date('d-m-Y'); ?>">
						</div> 
						</div>
						<div class="col-md-1" style="width:20px;padding:4px 0px;">=>
						</div>
						<div class="col-md-3">
						<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" readonly id="datepickercl2" value="<?php echo date('d-m-Y'); ?>">
						</div>
						</div>
						<div class="col-md-2">
							<input class="form-control btn btn-primary" type="submit" value="Get Reports" id="btngetcl">
						</div>
					</div>
				</div> 
			</div>
		</form>
<!-- search end closed trips ------------------------------------- -->
							
		</div>       
			
	</div>
	
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------
 $('#datepickerco1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
 $('#datepickerco2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   }); 


   $('#datepickercl1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
 $('#datepickercl2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });   


  $('#datepickerr1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
 $('#datepickerr2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });  


     $('#datepickeru1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
   
 $('#datepickeru2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });    
  
  
  $("#btngetco").click(function(){
      var dt1=$('#datepickerco1').val();
	  var dt2=$('#datepickerco2').val();
	  //alert(dt2);
	  
	 var pdf_link = $('#myformco').attr('action')+'/'+dt1+'/'+dt2; 
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;

  });


  $("#btngetcl").click(function(){
      var dt1=$('#datepickercl1').val();
	  var dt2=$('#datepickercl2').val();
	  //alert(dt2);
	  
	 var pdf_link = $('#myformcl').attr('action')+'/'+dt1+'/'+dt2; 
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;

  });


    $("#btngetr").click(function(){
      var dt1=$('#datepickerr1').val();
	  var dt2=$('#datepickerr2').val();
	  //alert(dt2);
	  
	 var pdf_link = $('#myformr').attr('action')+'/'+dt1+'/'+dt2; 
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;

  });

  
   $(".btnrA").click(function(){
      var dt1=$('.btnrA').attr('id');
	  var dt2="";
	  //alert(dt2);
	  
	 var pdf_link = $('#myformr').attr('action')+'/'+dt1+'/'+dt2; 
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;

  });
  
  
  
     $("#btngetu").click(function(){
      var dt1=$('#datepickeru1').val();
	  var dt2=$('#datepickeru2').val();
	  //alert(dt2);
	  
	 var pdf_link = $('#myformu').attr('action')+'/'+dt1+'/'+dt2; 
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;

  });
   
     $(".btnuA").click(function(){
       var dt1=$('.btnuA').attr('id');
	   var dt2="";
	  
	 var pdf_link = $('#myformu').attr('action')+'/'+dt1+'/'+dt2; 
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;

  }); 
  
</script>



