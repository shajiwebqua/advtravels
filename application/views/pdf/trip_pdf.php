<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Travels</title>

  <style type="text/css">
 td
   {
   padding:5px;
   }
th{
	text-align:right;
}
  .brd1
  {
	  border:1px solid #e4e4e4;
	  
  }  
   </style>
  
  
  </head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
<td width="150px" style="padding: 0px">
<?php $date = date('Y/m/d');?>
Date: <?=$date;?>
  </td>
</tr>
</table>
<hr>

<table style="border:none;width:100%;">
  <tr>
      <td>From
          <address>
            <strong>Advance World Holidays</strong><br>
            PK & Sons Complex, East Moozhikkal<br>
            NH 212, Calicut, Kerala, India-673571<br>
			Tel: 0091 495 2732 500, 0091 495 3100 700<br>
            Fax: 0091 495 2732 400
          </address>
		  
		  </td>
          <td> To
          <address>
            <strong><?=$results->customer_name;?></strong><br>
           <?=$results->customer_address;?><br>
            Mob:<?=$results->customer_mobile;?><br>
            Phone:<?=$results->customer_landline;?><br>
            Email: <?=$results->customer_email;?>
          </address></td>
          <th>Invoice No: <?=$results->trip_id;?>
          </th>
  </tr>
</table>
<hr>
  <h2>Basic Details:</h2>
<table width="100%" >
<tr><th >Purpose</th><td>:<?=$results->trip_purpose;?></td>
<th>Time(Start-End)</th><td>:<?=$results->trip_starttime.' '.'-'.$results->trip_endtime;?></td></tr>
<tr> <th>Vehicle No</th><td>:<?=$results->vehicle_regno;?></td>
<th>Km(Start-End)</th><td>:<?=$results->trip_startkm.' '.'=>'.$results->trip_endkm;?></td></tr>
<tr><th>Driver Name</th><td>:<?=$results->driver_name;?></td>
<th >Running KM</th><td>:<?php $end=$results->trip_endkm;$start=$results->trip_startkm; ?> 
<?=abs($end-$start);?></td></tr>
<tr><th>Trip(From - To )</th><td>:<?=$results->trip_from.' '.'-'.$results->trip_to;?></td>
<th  >Date(Start-End)</th><td>:<?=$results->trip_startdate.' '.'-'.$results->trip_enddate;?></td></tr>
<tr><th >Minimum Charge KM</th><td>:<?=$results->trip_minchargekm;?></td><td></td></tr>
</table>
<hr>
<h2><center>Charges:</center></h2>
 <h4><u>Minimum Charges:</u></h4>
 <table width="100%">
  <tr>
    <th width='20%'>Min.Days:</th>
     <td width='10%'><?=$results->trip_mindays;?></td>
               <th width='20%'>Min.Charge:</th>
              <td width='15%'><?= number_format($results->trip_mincharge,"2",".","");?></td>
              <th width='20%'>Min.Total:</th>
              <td><?= number_format($results->trip_mintotal,"2",".","");?></td>
           </tr>
         </table>
            <h4><u>Additional Charges:</u></h4>
 <table width="100%" >
  <tr>
             <th width='20%'>Addi.KM:</th>
              <td width='10%'><?=$results->trip_addikm;?></td>
               <th width='20%'>KM Charge:</th>
              <td width='15%'><?=number_format($results->trip_addikmcharge,"2",".","");?></td>
              <th width='20%'>Addi.Total: </th>
              <td><?= number_format($results->trip_additotal,"2",".","");?></td>
           </tr>
 </table>
  <h4><u>Other Charges:</u></h4>
 <table width="100%" >
  <tr>
             <th width='20%'>Toll/Parking:</th>
              <td width='10%'><?=number_format($results->trip_tollparking,"2",".","");?></td>
               <th width='20%'>Interstate Permit:</th>
              <td width='15%'><?= number_format($results->trip_interstate,"2",".","");?></td>
              <th width='20%'>Other Charges:</th>
              <td><?= number_format($results->trip_othercharge,"2",".","");?></td>
              <?php $otherchargetotal=$results->trip_tollparking+$results->trip_interstate+$results->trip_othercharge;?>
           </tr>
 </table>
 <hr>
 <h2><center>Description:</center></h2>
 <table width="100%">
  <tr>
    <th align='left'>Other Charge Description:</th>
    <td align='left'><?=$results->trip_otherdesc;?>
           </td>
           
       </tr>
 </table>
 <table width="100%">
  <tr>
    <th align='left'>Additional Information:</th>
    <td align='left'> <?=$results->trip_addiinfo;?>
           </td>
           
       </tr>
 </table>
 <hr>
  <h2>Amount:</h2>
   <table width="100%" style='font-size:17px;'>
   <tr>
              <td style="width:50%">Total Trip Charges:</td>
                <td align='right' >Rs.</td><td width='150px' align='right'> <?=number_format(($results->trip_charge-$otherchargetotal),"2",".","");?></td>
              </tr>
              <tr>
                <td>Other Charges</td>
                <td align='right'>Rs.</td><td width='150px' align='right'> <?=number_format($otherchargetotal,"2",".","");?></td>
              </tr>
			  <tr><td colspan='3' align='right'>------------------------------</td></tr>
             <tr>
                <td>Grand Total:</td>
                <td align='right'><b>Rs.</td><td style="width:150px;text-align:right;font-size:25px;" ><?=number_format($results->trip_gtotal,"2",".","");?></b></td>
              </tr>
			   <tr><td colspan='3' align='right'>============</td></tr>
 </table>

</html>