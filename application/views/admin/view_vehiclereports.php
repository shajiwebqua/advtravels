<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}
</style>

 <!-- Content Wrapper. Contains page content -->
 
<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Vehicle Reports</b></h1>
  <label style="background-color:#cecece;width:100%;height:1px;margin-top:0px;"></label>
 </section>
  
<!-- Content Header  end -->	  
	  
  <section class="content"> 
 <!--  <div style="color:blue;font-size:12px;"> Select Trip details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Print,Share,Save,View,Delete)  </div> -->
  <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div>
				 <div id='errmsg'><?php echo $this->session->flashdata('message1'); ?></div></center>
				 </div>
			</div>  
  <!-- Small boxes (Stat box) -->
<div class="row">
<div class="col-md-6">
<!-- Add new user details-->
<div style="background-color:#fff;border-radius:3px;padding:15px; ">

<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style='margin-bottom:50px;'>
<div style='width:100%;border-bottom:1px solid #e4e4e4;' >
<h3 style='margin-top:0px;font-size:17px;font-weight:600;'><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Vehicle wise Reports </h3> 
</div>

<!--- search all ---------------------------------------- -->

	
	<div style='margin-top:20px;'>
 	 <label style='margin-bottom:10px;'><b>To get vehicle wise report</b></label>
	</div>
	<form class="form-horizontal" role="form" method="POST" id="myform" action="<?php echo base_url('Vehicle/vreport')?>" >
	<div class="row">
        <div class="col-md-12">
        <div class="form-group">
		 <label class='col-md-3 control-label'> Select Vehicle</label>
          <div class="col-md-7">
			<select class="form-control" id="vregid" name="vregid">
			 <option value="0">-----</option>
				 <?php
				 $vres1=$this->db->select('*')->from('vehicle_registration')->get()->result();
				 foreach($vres1 as $vr1)
				 {
					 echo '<option value="'.$vr1->vehicle_id.'">'.$vr1->vehicle_regno.'</option>';
				 }
				  ?>
		 </select> 
		 </div>
		 <div class='col-md-2'>
		 <input class="btn btn-primary" style="border-radius:0px;padding:3px 20px 3px 20px;" type="submit" value="Go" id="btn_get">
		 </div>
		            
         </div>
		</div>
		</div> 
		</form>
		<hr>
	
	<div style='margin-top:20px;'>
 	 <label style='margin-bottom:10px;'><b>To get date wise vehicle report</b></label>
	</div>
	<!--<label style="background-color:#cecece;width:100%;height:1px;"></label><br> -->
	
	<form class="form-horizontal" role="form" method="POST" id="myform" action="<?php echo base_url('Vehicle/vdreport')?>" >
	
  		<div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <div class="col-md-12">
		 <table width='100%'>
		 <tr><td>From Date</td><td>To Date</td><td>Vehicle</td><td></td></tr>
		 <tr>
		 
		 <td><input data-provide="datepicker" id="datepicker1" value="<?php echo date('d-m-Y');?>" name='sdate'></td>
		 <td><input data-provide="datepicker" id="datepicker2" value="<?php echo date('d-m-Y');?>" name='edate'></td>
		 <td> 
			<select class="form-control" id="vregno" name="vregno">
			 <option value="0">-----</option>
				 <?php
				 $vres1=$this->db->select('*')->from('vehicle_registration')->get()->result();
				 foreach($vres1 as $vr1)
				 {
					 echo '<option value="'.$vr1->vehicle_id.'">'.$vr1->vehicle_regno.'</option>';
				 }
				  ?>
		 </select> 
		 </td>
		 
		 <td style='padding-left:5px;'> <input class="form-control btn btn-primary" style="text-align:center;" type="submit" value="Go" id="btnget"></td></tr>
		 </table>
    
           
         </div>
			</div>
			</div> 
			</div>
		</form>
		
		<hr>
	<div style='margin-top:20px;'>
 	 <label style='margin-bottom:10px;'><b>To get date wise all vehicles report</b></label>
	</div>
	<!--<label style="background-color:#cecece;width:100%;height:1px;"></label><br> -->
	
	<form class="form-horizontal" role="form" method="POST" id="myform" action="<?php echo base_url('Vehicle/vall_report')?>" >
	
  		<div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <div class="col-md-12">
		
		 <table width='100%'>
		 <tr><td>From Date</td><td>To Date</td><td></td><td></td></tr>
		 <tr>
		 
		 <td><input data-provide="datepicker" id="datepicker1" value="<?php echo date('d-m-Y');?>" name='sdate1'></td>
		 <td><input data-provide="datepicker" id="datepicker2" value="<?php echo date('d-m-Y');?>" name='edate1'></td>
		 <td style='padding-left:5px;'> <input class="form-control btn btn-primary" style="text-align:center;" type="submit" value='Get report'  name='allveh_report' ></td></tr>
		 </table>
		              
         </div>
			</div>
			</div> 
			</div>
		</form>
		
		<hr>
		
	<form class="form-horizontal" role="form" method="POST" id="myform" action="<?php echo base_url('Vehicle/vallreport')?>" >
	
  		<div class="row">
        <div class="col-md-12">
        <div class="form-group">
		<div class='col-md-5'>
		<label style='margin-bottom:10px;'><b>To get all vehicles report</b></label>
		</div>
        <div class="col-md-6">
		 <table width='100%'>
		 <td></td>
		 <td style='padding-left:5px;'><input class="btn btn-primary" style="text-align:center;" type="submit" value='Get all vehicle report'  name='allveh_report' ></td></tr>
		 </table>
		              
         </div>
			</div>
			</div> 
			</div>
		</form>
		
		

		</div>       

	</div>
</div>
</div>


<!-- second column ----------------------------------------------------------------------------------->

<div class="col-md-6">

<!-- Add new user details-->

<div style="background-color:#fff;border-radius:3px;padding:15px; ">

<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->

<div class="page-content" style='margin-bottom:50px;'>
<div style='width:100%;border-bottom:1px solid #e4e4e4;' >
<h3 style='margin-top:0px;font-size:17px;font-weight:600;'> <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Vehicle List Reports </h3> 
</div>

<!--- search all ---------------------------------------- -->

  <form class="form-horizontal" role="form" method="POST" id="myform1" action="<?php echo base_url('Reports/pdf_allvehicles')?>" >
	<div style='margin-top:20px;'>
 	 <label><b>To get vehicles report</b></label>
	</div>
	<!--<label style="background-color:#cecece;width:100%;height:1px;"></label><br> -->
  		<div class="row">
        <div class="col-md-11">
			<div class="form-group">
			<div class="col-md-8" style="padding-top:3px;">
				<label style="padding-left:10px;">
				<input type="radio" name="vehicle" class="icheck" data-radio="iradio_square-grey" value="vehicles" checked>All Vehicles</label>
				<label style='margin-left:20px;'> <input type="radio" name="vehicle" class="icheck" data-radio="iradio_square-grey" value="servicing">Servicing Vehicles</label>
			 </div>
					<div class="col-md-3">
						<input class="form-control btn btn-primary" style="text-align:center;" type="submit" value="Get Reports" id="bget1">
					</div>
				</div>
		 </div> 
		</div>
   </form>
	
<!-- search all ------------------------------------- -->
<form class="form-horizontal" role="form" method="POST" id="myformm" action="<?php echo base_url('Reports/pdf_month_vehicles')?>" >

<div style='margin-top:20px;'>
 	<label><b>To get servicing vehicles (month wise)</b></label>
</div>
<!--<label style="background-color:#cecece;width:100%;height:1px;"></label> <br> -->
			<div class="row">
              <div class="col-md-11">
                <div class="form-group">
                  	<br><label class="col-md-3 control-label" style="padding-top:3px;" >Select Month : </label>
				<div class="col-md-5">
					<select class="form-control" id="month" name="pmonth1">
					<option value="0">-----</option>
					<option value="1">January</option>
					<option value="2">February</option>
					<option value="3">March</option>
					<option value="4">April</option>
					<option value="5">May</option>
					<option value="6">June</option>
					<option value="7">July</option>
					<option value="8">August</option>
					<option value="9">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
					</select> 
				</div>
						
				<div class="col-md-3">
					<input class="form-control btn btn-primary" type="submit" value="Get Reports" id="btngetm">
				</div>
			  </div>
			 </div> 
		  </div>
		</form>
		
	</div>       
</div>
</div>
</div>

</section>
</div>

<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal("Success",$("#msg").html(),"success")
		$("#msg").html("");
	}
	
$("#errmsg").hide();
if($("#errmsg").html()!="")
	{
		swal("Try Again.",$("#errmsg").html(),"error")
		$("#errmsg").html("");
	}	
	
//-------------------------------------

$('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});


$('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});


 $("#bget1").click(function(){
        var radioValue = $("input[name='vehicle']:checked").val();
	  //alert(radioValue);
	 var pdf_link = $('#myform1').attr('action')+'/'+radioValue;  
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Vehicle Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;

});

 $("#btngetm").click(function(){
         var month=$('#month').val();
         if(month=="0")
         {
			alert('Please Select Month');
         }
         else
         {
			var pdf_link = $('#myformm').attr('action')+'/'+month;  
			//alert(pdf_link);
			var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
			//alert(iframe);
			$.createModal({
			title:'Vehicle Details',
			message: iframe,
			closeButton:true,
			scrollable: true,
    }); return false;

	} 
  });
  
  
</script>



