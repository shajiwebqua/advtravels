<?php
include('application/views/common/header.php');
?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
p
{
padding-top:0px;
padding-bottom:0px;	
maring:0px;
}

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1><b>Vehicle Repairs & Service Request List</b></h1>
		<ol class="breadcrumb" style='font-size:15px;'>
			<li><a href="<?php echo base_url('Services/service_parts');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Add Service Request</button></a></li>
			
	    </ol> 
         <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
  
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
  <div class="col-md-12">

   <div style="background-color:#fff;border-radius:2px;padding:15px; ">
    
           <!-- <div class="portlet-title">
                     <div class="caption" >
							<h4 style='margin-top:0px;'><i class="fa fa-plus-circle" aria-hidden="true"></i> Requested Parts Details</h4>
                     </div>
            </div> -->
			
<div class='row' style=' background-color:#e4e4e4;margin:0px;padding:5px;'>
	<form onsubmit='return checkdata1();' method="POST" action="<?php echo base_url('Services/request_list/1')?>" enctype="multipart/form-data">
	<label class='col-md-2 la-right'>Select Date:</label>
	<div class='col-md-3' style='padding:0px;'>
			<div class='row'>
				<div class='col-md-6' >
				<input class='form-control' data-provide="datepicker" id="datepicker1" name='sdate'>
				</div><div class='col-md-6' >
				<input class='form-control' data-provide="datepicker" id="datepicker2" name='edate'>
				</div>
			</div>
	</div>
	<div class='col-md-1' style='padding:2px;margin-left:10px;'>
	<button type='submit' name='btnget1' style='padding-left:15px; padding-right:15px;' >Get</button>
	</div>
	</form>
	</div>
			
  <hr style="background-color:#cecece,border:1px;width:100%;top:10px;margin:0px;padding:3px 0px 10px 0px;">
             
 <div class="portlet-body form">
 <div class='row' style='margin-bottom:15px;'>
 <div class='col-md-12'>
 <label style='font-size:16px;font-weight:bold;'><?php echo $dcap;?></label>
 </div>
 </div>
 

 <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>Print</th>
                <th width="7%">ID</th>
                <th>Date</th>
                <th>Vehicle</th>
                <th>Service</th>
                <th>Requirements</th>
                <th width="3%">Action</th>
                
            </tr>
        </thead>
        
        <tbody>
        <?php $i=1;foreach($srresult as $row){
			
			?>
            <tr>
				<td><a id="view-pdf" href="<?php echo base_url('Reports/request_report')."/".$row->srequest_id;?>" style='color:#4b88ed;'><button class='btn btn-primary btn-xs'>&nbsp;&nbsp;Print&nbsp;&nbsp;</button></a></td>
                <td><?=$row->srequest_id;?></td>
                <th><?=$row->srequest_date;?></th>
                <td><p><b>Reg.No: </b><?=$row->vehicle_regno?><br></p><p><b>Vehicle Type: </b><?=$row->veh_type_name?></p>
                 <p><b>Service Type: </b><?php if($row->srequest_category==1)echo "Toyota Servicing"; else echo "General";?>
				 </td>
				<td><p><b>Expected Date:</b><?=$row->srequest_datetime?></p><p><b>Duration :</b><?= $row->srequest_duration?></p><p>
				<b>Service Centre:</b><?= $row->srequest_workshop?></p></td>
                <td> <table  style="margin-top:0px;width:100%;">
				<?php $res = $this->db->where('srequest_id',$row->srequest_id)->get('service_requirements')->result();
				 foreach($res as $row1)
				{
					if(!is_numeric($row1->price))
					{
						$price=0;
					}
					else
					{
						$price=$row1->price;
					}
				
                  echo "<tr><td>".$row1->parts."</td><td align='right'>&#8377;&nbsp;".number_format($price,"2",".","")."</td></tr>";
                }
				?></table></td>
				
                <td>
					<a href="<?php echo base_url()?>Services/edit_request?id=<?php echo $row->srequest_id?>" data-toggle="modal" class="edit open-AddBookDialog2"><button class='btn btn-warning btn-xs' >&nbsp;&nbsp;Edit&nbsp;&nbsp;&nbsp;</button></a>
					<br><a href="#" onclick="delete_service_request('<?php echo $row->srequest_id;?>')" data-toggle="modal" class="delete open-AddBookDialog2"><button class='btn btn-danger btn-xs'>Delete</button></a>
                </td>
                
            </tr>
         <?php $i++;} ?>   
        </tbody>
    </table> 
 
 </div> <!--portlet-body-->
 
 </div>
</div>    
<br>  
  </div>
  </div>
</section>

<?php include('application/views/common/footer.php');?>
  </div>

<script type="text/javascript">
$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }

 $('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});

$('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});

  
 var table=$('#example').DataTable({
	 "ordering":false,
 });
  /*$('#example tbody').on( 'click', 'tr', function () {
         if ( $(this).hasClass('selected') ) {
             $(this).removeClass('selected');
         }
         else {
 	        table.$('tr.selected').removeClass('selected');
             $(this).addClass('selected');
         }
   });	*/		   
	
function delete_service_request(id)
{
  swal({
        title: "Are you sure?",
        text: "You will not be able to recover",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel pls!",
        closeOnConfirm: true,
        closeOnCancel: true 
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
        type: "post",
        url: "<?php echo base_url();?>Services/delete_service_request?id="+id,
        success: function(data){ 

        if(data="success")
        {
          window.location.reload();
        }
        else
        {
          swal("Error", "Something went wrong", "error");    
        }  
        }
        });
      }
      else
      {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    }
    );
}
	
$("#view-pdf").click(function()
{
	  //var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 
		 var pdf_link = $(this).attr('href');
         var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
            $.createModal({
				title:'Trip Details',
				message: iframe,
				closeButton:true,
				scrollable: true,
			});
			return false; 
		 });	
 
/*$("#btnexp1").click(function()
{
	var etype1=$("#exptype1").val();
	var egroup=$("#expgroup").val();
	jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Vehicle/add_exp_type",
        dataType: 'html',
        data: {exptype:etype1,expgroup:egroup},
        success: function(res) {
		$("#etype").append("<option value='"+res+"'>"+etype1+"</option>")
                    }
            });
});
*/

</script>


  </body>
</html>
