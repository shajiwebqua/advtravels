<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stationary extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Model_staffsalary');	
	}

	public function Purchase_request()
	{
		$this->load->view('admin/purchase_request');
	}
	
	
	public function add_purchase_request()
	{
		$tableData = $this->input->post('pTableData');
        		
		// Decode the JSON array
        /*$tableData = json_decode($tableData,TRUE);
		
	   for($i=0;$i<count($tableData);$i++)
       {
            $data = array('sta_particulars'=>$tableData[$i]['Parts'],'sta_qty'=>$tableData[$i]['Qty'],'sta_price'=>$tableData[$i]['Price']);
            $this->db->insert('stationary_request',$data);
       }*/
       echo $tableData;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function Salary()
	{
		$op=$this->uri->segment(3);
		$this->load->Model('Model_staffsalary');
		
		if(is_null($op))
		{
			$this->session->set_userdata('saltab','1');
		}
		
		if($op==null or $op=="" or $op==0)
		{
		$saldata['vsalary']=$this->Model_staffsalary->view_salary_details();
		}
		else if($op==1)
		{
		
		$sdate=$this->input->post('startdate');
		$edate=$this->input->post('enddate');
		$d1=explode("-",$sdate);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$edate);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
		
		$saldata['vsalary']=$this->Model_staffsalary->view_salary_details1($dt1,$dt2,$op);
		$this->session->set_userdata('saltab','2');
		}
		
		else if($op==2)
		{
		$smonth=$this->input->post('smonth');
		$syear=$this->input->post('syear');
		$saldata['vsalary']=$this->Model_staffsalary->view_salary_details1($smonth,$syear,$op);
		$this->session->set_userdata('saltab','2');
		}
		
		$this->load->view("admin/add_salary",$saldata);
	}
	
	
public function add_salary()
{
	$fname="";
		if (!empty($_FILES['files']['name']))
		{
			$filesCount = count($_FILES['files']['name']);
			
			$fname="";	
			for($i = 0; $i < $filesCount; $i++)
			{
                $_FILES['ivf']['name'] = $_FILES['files']['name'][$i];
                $_FILES['ivf']['type'] = $_FILES['files']['type'][$i];
                $_FILES['ivf']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['ivf']['error'] = $_FILES['files']['error'][$i];
                $_FILES['ivf']['size'] = $_FILES['files']['size'][$i];
				
			    $uploadPath = 'upload/expense/';

                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|jpe';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				
                if($this->upload->do_upload('ivf'))
				{
                 $fileData=array('upload_data'=> $this->upload->data());
				 
						if($i==$filesCount-1)
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'];
						 }
						 else
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'].",";
						 }
				}
			}
		}		

	$staff=$this->input->post('staffna');
    $edate=$this->input->post('edate');
    $vno=$this->input->post('voucherno');
    $vdate=$this->input->post('vdate');
	$advance=$this->input->post('advance');
	$balance=$this->input->post('balance');
	$amount=$this->input->post('amount');
	$narra=$this->input->post('narra');
		
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
	
    	
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
    
       $sadv = array(
         'salary_staffid' => $staff,
		 'salary_date' => $edate,
		 'salary_voucherno' => $vno,
		 'salary_voucherdate' => $vdate,
		 'salary_advance' => $advance,
		 'salary_balance' => $balance,
		 'salary_paidamount' => $amount,
		 'salary_narration' => $narra,
		 'salary_image' => $fname,
		  );

		  $this->load->Model('Model_staffsalary');
		  $ins_id="";
          $ins_id=$this->Model_staffsalary->insert_salary($sadv);
		  		  
		  //update salary advance
		  $this->db->where("sal_adv_staffid",$staff)->update("salary_advance",array("sal_adv_status"=>"2"));
		  		  
		  
          $uid = $this->db->insert_id();
          $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Salary Details Added'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
		  
		  $this->session->set_userdata('saltab','1');
		  
		  $this->session->set_flashdata('message', '1#Salary details added.!');
      	  redirect('StaffSalary/Salary');  
    }

	public function del_salary()
	{
		$sid=$this->uri->segment(3);
		$this->load->Model('Model_staffsalary');
        $this->Model_staffsalary->delete_salary($sid);
		$this->session->set_userdata('saltab','2');
		$this->session->set_flashdata('message', '3#Salary details Deleted.!');
      	redirect('StaffSalary/Salary/0');  
	}
		
	public function Advance()
	{
		$op=$this->uri->segment(3);
	
		$this->load->Model('Model_staffsalary');
		if(is_null($op))
		{
			$this->session->set_userdata('asaltab','1');
		}
			
		if($op==null or $op=="" or $op==0)
		{
		$asdata['vadvsalary']=$this->Model_staffsalary->view_advance_salary();
		}
		else if($op==1)
		{
		
		$sdate=$this->input->post('startdate');
		$edate=$this->input->post('enddate');
		$d1=explode("-",$sdate);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$edate);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
		
		$asdata['vadvsalary']=$this->Model_staffsalary->view_advance_salary1($dt1,$dt2,$op);
		$this->session->set_userdata('asaltab','2');
		}
		
		else if($op==2)
		{
		$smonth=$this->input->post('smonth');
		$syear=$this->input->post('syear');
		
		$asdata['vadvsalary']=$this->Model_staffsalary->view_advance_salary1($smonth,$syear,$op);
		$this->session->set_userdata('asaltab','2');
		}
		
		$this->get_total_salary_advance();
		$asdata['advtotal']=$this->Model_staffsalary->get_advance_total();
		
		$this->load->view("admin/add_advance_salary",$asdata);
	}
		
public function add_Advance()
{
		$fname="";
		if (!empty($_FILES['files']['name']))
		{
			$filesCount = count($_FILES['files']['name']);
			
			$fname="";	
			for($i = 0; $i < $filesCount; $i++)
			{
                $_FILES['ivf']['name'] = $_FILES['files']['name'][$i];
                $_FILES['ivf']['type'] = $_FILES['files']['type'][$i];
                $_FILES['ivf']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['ivf']['error'] = $_FILES['files']['error'][$i];
                $_FILES['ivf']['size'] = $_FILES['files']['size'][$i];
				
			    $uploadPath = 'upload/expense/';

                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|jpe';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				
                if($this->upload->do_upload('ivf'))
				{
                 $fileData=array('upload_data'=> $this->upload->data());
				 
						if($i==$filesCount-1)
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'];
						 }
						 else
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'].",";
						 }
				}
			}
		}		

	$staff=$this->input->post('staffna');
    $edate=$this->input->post('edate');
    $vno=$this->input->post('voucherno');
    $vdate=$this->input->post('vdate');
	$amount=$this->input->post('amount');
	$narra=$this->input->post('narra');
	
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
    	
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
    
       $sadv = array(
         'sal_adv_staffid' => $staff,
		 'sal_adv_date' => $edate,
		 'sal_adv_vno' => $vno,
		 'sal_adv_vdate' => $vdate,
		 'sal_adv_amount' => $amount,
		 'sal_adv_narration' => $narra,
		 'sal_adv_image' => $fname,
		 'sal_adv_status' => "1",
		  );

		  $this->load->Model('Model_staffsalary');
		  $ins_id="";
          $ins_id=$this->Model_staffsalary->insert_advance($sadv);
		  $this->session->set_flashdata('message', '1#Salary Advance added..!');
          $uid = $this->db->insert_id();
          $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Salary Advance Details Added'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
      	  redirect('StaffSalary/Advance');  
    }
	
	public function del_advance()
	{
		$sid=$this->uri->segment(3);
		$this->load->Model('Model_staffsalary');
        $this->Model_staffsalary->delete_advance($sid);
		$this->session->set_userdata('asaltab','2');
		
		$this->session->set_flashdata('message', '3#Salary Advance Deleted.!');
      	redirect('StaffSalary/Advance/0');  
	}
	
	public function get_details()
	{
		$sid=$this->input->post('staffid');
		if($sid!="")
		{
		$row=$this->db->select('*')->from('staffregistration')->where('staff_id',$sid)->get()->row();
		$sal=$row->staff_basicsalary;

		$res=$this->db->select('*')->from('salary_advance')->where('sal_adv_staffid',$sid)->where("sal_adv_status","1")->get()->result();
		$dt="";
		$amt=0;
		foreach($res as $r)
		{
			$dt.="<tr><td>".$r->sal_adv_vdate."</td><td>".$r->sal_adv_vno."</td><td class='tda'>".number_format($r->sal_adv_amount,2,'.','')."</td></tr>";
			$amt+=$r->sal_adv_amount;
		}
		$bal=$sal-$amt;
		$dta=$dt."#".number_format($sal,2,'.','')."#".number_format($amt,2,'.','')."#".number_format($bal,2,'.','');
		echo $dta;
		}
	}

	
	public function get_total_salary_advance()
	{
		$this->load->Model('Model_staffsalary');
        $advtotal=$this->Model_staffsalary->get_advance_total();
	}
	
	
	
	
}	

