<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8" />
  <title>Travels</title>
  <style type="text/css">
 #tbl1 td
    {
      padding: 10px;
	  border:1px solid #e4e4e4;
    }
  </style>
</head>
<body>

<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>
<table width="100%" cellpadding=0 cellspacing=0>
<tr><td style='font-wheight:bold;font-size:15px;'><p><?php echo $optext;?></p></td><td style='text-align:right;font-size:15px;'><?php echo date('d-m-Y');?> </td></tr>
<tr><td colspan=2><hr></td></tr>
</table>

<table width="100%" style='border:1px solid #e4e4e4;font-size:20px;'id="tbl1" cellpadding=0 cellspacing=0>
<tr height="25px">
   <td>Trip-ID</td>
   <td style='width:100px'><b>Ref No</b></td>
   <td style='width:230px'><b>Client</b></td>
   <td style='width:150px'><b>Vehicle</b></td>
   <td style='width:150px'><b>Driver</b></td>
   <td style='width:80px'><b>Room/ Guest</b></td>
   <td style='width:150px'><b>Trip_Dates</b></td>
   <td style='width:50px'><b>Days</b></td>
   <td style='width:70px;'><b>KM</b></td>
   <td style='width:100px'><b>Payment</b></td>
   <td style='width:100px'><b>Amount</b></td>
</tr>

<?php

$tot=0;

foreach($results1 as $r)
{
	  $d1=$r->trip_startdate;
      $dt1=date("d-m-Y", strtotime($d1));
      $d2=$r->trip_enddate;
      $dt2=date("d-m-Y", strtotime($d2));
      
      $date1=date_create($r->trip_startdate);
            $date2=date_create($r->trip_enddate);
            $diff=date_diff($date1,$date2);
      //$diff1=$diff->format("%R%a days");
      $diff1=$diff->format("%a");
      $km=$r->trip_endkm - $r->trip_startkm ;
  
  $tot=$tot+$r->trip_gtotal;
  
  if($sec=='3')
  {
	  $drv=$r->trip_drivername;
  }
  else
  {
	 $drv=$r->driver_name; 
  }
  
  
echo "<tr>
   <td>" .$r->trip_id. "</td>
   <td>" .$r->trip_refno. "</td>
   <td>" .$r->customer_name. "</td>
   <td>" .$r->trip_vehicle_regno ."</td>
   <td>" .$drv. "</td> 
   <td>" .$r->trip_roomno. "</td> 
   <td >" .$dt1. " => ".$dt2."</td> <td>" .$diff1 ."</td>
   <td style='text-align:right;'>" .$km ."</td> 
   <td>" .$r->trip_paymentmode. "</td> 
   <td style='text-align:right;'>" .number_format($r->trip_gtotal,"2")  ."</td></tr>";
}
?>
<tr><td colspan='9' style='font-weight:700;text-align:right;'>Grand Total : </td>
<td colspan='2' style='text-align:right;font-weight:700;'><?php echo "₹ ". number_format($tot,2);?></td></tr>

</table>

</body>                             
                       
</html>