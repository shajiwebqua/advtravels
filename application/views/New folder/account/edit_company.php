<?php
include('application/views/common/acc_header.php');
?>
  <style>
  .dashbtn
  {
	  width:75px;
	  height:75px;
	  background-color:#f4f4f4;
	  border:1px solid #fff;
	  /*border-radius:10px;*/
	  padding:5px;
	  text-align:center;
  }
  
.sh4
{
 border-bottom:1px solid #e4e4e4;
 width:100%;
 color:#00adee;
 padding-bottom:5px;
 margin-top:0px;"
}

 <link rel="stylesheet" href="<?php echo base_url('assets/chart/css/material-charts.css');?>">
 <script src="<?php echo base_url('assets/chart/js/material-charts.js');?>"></script>

  </style>
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#00adee'> Edit Company 
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
     <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
	

        <!-- Main content -->
    <section class="content">
	
	<?php
		
	foreach($result as $c1)
	{

	?>
	
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header">
          <h3 class="box-title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Details</h3>
		  <label id='mes' ><?php echo $this->session->flashdata('message');?></label>
        </div>
		
        <div class="box-body">
        	<div class='row' style='padding:15px;'>
			<div class='col-md-12'>
				 <form onsubmit='return confirm_form();'class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Account/update_company')?>" enctype="multipart/form-data">
                  
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Company Name :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="cmpname" value='<?=$c1->acc_comp_name;?>' required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Address Name :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="cmpadd" value='<?=$c1->acc_comp_address;?>' required>
								</div>
							</div>
						</div>		
				
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Country :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="cmpcountry" value='<?=$c1->acc_comp_country;?>' required>
								</div>
							</div>
						</div>	

						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Telephone No :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="cmpphone" value='<?=$c1->acc_comp_phone;?>' required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Mobile No :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="cmpmobile" value='<?= $c1->acc_comp_mobile;?>' required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Email :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="cmpemail" value='<?= $c1->acc_comp_email;?>' required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Web Site :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="cmpweb" value='<?= $c1->acc_comp_website;?>' required>
								</div>
							</div>
						</div>
						
					<?php
						}
					?>
						<hr style='margin:15px;'>
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1"></label>
								<div class="col-md-7">
									<input type="submit" class="btn btn-primary" name="btnsubmit" value='Update Company Details'>
								</div>
							</div>
						</div>
						
			</form>
		
		
			</div>
			</div>
           
        </div>
	</div>
	</div>
	
</section>
    <!-- content wrapper -->
</div>
     <?php
  include('application/views/common/acc_footer.php');
  ?>
   
</body>
</html>
  <script type="text/javascript">
  $("#mes").hide();
  
  if($("#mes").html()!="")
  {
	  swal("Saved.!",$("#mes").html(),"success");
	  $("#mes").html("");
  }
  
  function confirm_form()
  {
	  return confirm("Are you sure, submit this details?");
  }
  </script>
</body>
</html>

  
  