<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');
?>
<style>
	/* hide number  spinner*/
	input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
	.ttext
	{
		background-color:#fff;
		font-weight:bold;
		color:Green;
	}
	.ta-right
	{
		text-align:right;
		margin-top:3px;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 

	<section class="content-header">
		<h1><b>Service Evaluation Details</b> </h1>
		<ol class="breadcrumb" style='font-size:15px;'>
	    <li><a id='attach' href="<?php echo site_url('Services/Evaluation_list') ?>" ><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Evaluation List</button></a></li>
		</ol>
	</section>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
	<!-- Content Header end -->
	<section class="content"> 

	<div class="row">
	<div class="col-md-12">

	<div style="background-color:#fff;border-radius:3px;padding:2px 15px; ">
		<div class="portlet-title">
		<div id="idmsg" style="background-color:#fff;margin-bottom:3px;">
			 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
		</div>
      <!--</div> -->
	</div>
	<div class="portlet-body form">
  		<!-- text input -->
  		<div class="box-body">
		<div class='row'>
		<div class='col-md-12' style='padding-top:0px;'> <h4 style='margin:0px;'> <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Add Details</h4>
		</div>
		<div class='row' style='margin-top:3px;'>
		<hr>
		</div>
		
		</div>
				
  		<div class='col-md-12'>
  		<form enctype="multipart/form-data" method="post"  action="<?php echo site_url('Services/add_ser_evaluation') ?>">
        <div class="form-group">
		<div class='row'>	
			<label class='col-md-2 control-label ta-right' for="service_type">Service type</label>
			<div class='col-md-3'>
				<select name="service_type" id="service_type" class="form-control" required>
				  <option value="">----select----</option>
				  <option value="1">Individual</option>
				  <option value="2">Corporate</option>
				</select>
			</div>
		</div>
		</div>
		
        <div class="form-group">
		<div class='row'>
			<label class='col-md-2 control-label ta-right' for="guest_name">Guest Name</label>
			<div class='col-md-6'>
			<input type="text" class="form-control" id="guest_name" name="guest_name" required>
			</div>
		</div>
		</div>
		
        <div class="form-group">
        <div class='row'>
			<label class='col-md-2 control-label ta-right' for="contact_no">Contact No</label>
			<div class='col-md-6'>
			<input type="text" class="form-control" id="contact_no" name="contact_no" required>
			</div>
		</div>
		</div>
		
        <div class="form-group">
        <div class='row'>
			<label class='col-md-2 control-label ta-right' for="contact_no">Date</label>
			<div class='col-md-3'>
			<input type='text' class='form-control' name='date' id='datepicker1' placeholder="date" required >
			</div>
		</div>
		</div>
		
        <div class="form-group">
        <div class='row'>
			<label class='col-md-2 control-label ta-right' for="vehicle_no">Vehicle No</label>
			<div class='col-md-4'>
			
			<select id="vehicle_no" name="vehicle_no" class="form-control" required>
			<option value="">----select----</option>
			<?php 
			$ves1=$this->db->select('vehicle_id,vehicle_regno')->from('vehicle_registration')->get()->result();
			foreach($ves1 as $vs1)
			{
			?>
			  <option value="<?=$vs1->vehicle_id;?>"><?=$vs1->vehicle_regno;?></option>
			<?php
			}
			?>
			</select>
			</div>
		</div>
		</div>
		
        <div class="form-group">
        <div class='row'>
			<label class='col-md-2 control-label ta-right' for="chauffeur_name">Chauffeur Name</label>
			<div class='col-md-4'>
			<select id="chauffeur_name" name="chauffeur_name" class="form-control" required>
				  <option value="">----select----</option>
			<?php 
			$res1=$this->db->select('driver_id,driver_name')->from('driverregistration')->get()->result();
			foreach($res1 as $rs1)
			{
			?>
			  <option value="<?=$rs1->driver_id;?>"><?=$rs1->driver_name;?></option>
			<?php
			}
			?>
			</select>
			</div>
		</div>
		</div>
		
        <div class="form-group">
        <div class='row'>
			<label class='col-md-2 control-label ta-right' for="feedback">Feedback</label>
			<div class='col-md-6'>
			<textarea rows=5 name="feedback" id="feedback" class="form-control" required></textarea>
			</div>
		</div>
		</div>
		
		<div class="form-group">
        <div class='row'>
			<label class='col-md-2 control-label ta-right' for="feedback">Action Taken</label>
			<div class='col-md-6'>
			<textarea rows=3 name="actiontaken" id="actiontaken" class="form-control" required></textarea>
			</div>
		</div>
		</div>
		
		<hr style='margin:10px;'>
		<div class="form-group">
        <div class='row'>
			<label class='col-md-2 control-label ta-right' for="feedback"></label>
			<div class='col-md-6'>
			<button type="submit" class="btn btn-primary"> <i class="fa fa-plus" aria-hidden="true"></i> Save Details</button>
			</div>
		</div>
		</div>
        </form> 
        </div>

</div>
</div>	
</div>   
</div>
</div>
</section>
</div>

<?php include("application/views/common/footer.php");?>

<script type="text/javascript">

$("#msg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"success")
	if(mg[0]==2)
		swal("Updated",mg[1],"success")
	if(mg[0]==3)
		swal("Deleted",mg[1],"success")
	if(mg[0]==4)
		swal("Try Again",mg[1],"error")
    $("#msg").html("");
  }
  
$('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
</script> 

