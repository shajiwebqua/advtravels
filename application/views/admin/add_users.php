<?php
include('application/views/common/header.php');;
?>
<style>
.left1
{
	padding-left:30px;
}
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" >
        <section class="content-header">
         
        </section>
         <!-- Main content -->
        <section class="content" style="padding-bottom:10px;">
		<div style="background-color:#fff;padding:15px;">
		   <div class="row">
		    <div class="col-md-12">
		           <h4><b>New user Details</b></h4>
		    </div>
		            <label style="background-color:#cecece;height:1px;width:100%"></label>
            <div id='response'>	<?php echo $this->session->flashdata('message'); ?></div>
                </div><!-- /.box-header -->
                <!-- form start -->
                 <?php echo form_open_multipart('userprofile/add_new_user');?>
                  <div class="box-body">
			                     <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('userprofile/add_new_user')?>" enctype="multipart/form-data">
					   
											<div class="form-group">
											<div class="row">
											<label class="col-md-2  control-label left1">Name</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control"  name="name" required>
                                                </div>
										    </div>
                                            </div>
					   
					   
											<div class="form-group">
											<div class="row">
                                                 <label class="col-md-2  control-label left1">Image</label>
                                                 <div class="col-md-6">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="max-width:105px; max-height:110px;">
                                                                        <img src="<?php echo base_url('assets/dist/img/user.jpg')?>" alt="" /> </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width:105px; max-height:110px;"> </div>
                                                                    <div>
                                                                        <span class="btn btn-default btn-file">
                                                                            <span class="fileinput-new "> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="image"  > </span>
                                                                        <a href="javascript:;" class="btn btn-default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                                 </div>
                                                            </div>
										</div>
										<div class="form-group">
										<div class="row">
                                                <label class="col-md-2  control-label left1">Phone</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control"  name="phone" required>
                                                </div>
                                        </div>
										</div>

										
                                        <div class="form-group">
										<div class="row">
                                                <label class="col-md-2  control-label left1">Email</label>
                                                <div class="col-md-6">
                                                    <input type="email" class="form-control"  name="email" required>
                                                </div>
                                        </div>
										</div>
                                           

                                         <div class="form-group">
										 <div class="row">
                                                    <label class="col-md-2  control-label left1">Gender </label>
                                                    <div class="icheck-inline">
                                                    <label style="padding-left:10px;">
                                                     <input type="radio" name="gender" class="icheck" data-radio="iradio_square-grey" value="Male">Male</label>
                                                    <label>
                                                    <input type="radio" name="gender" class="icheck" data-radio="iradio_square-grey" value="Female">Female</label>
                                                    </div>
                                                    </div>
                                         </div>
										 <div class="form-group">
										 <div class="row">
                                        <label class="col-md-2   control-label left1" >Username</label>
                                        <div class="col-md-6">
                                           <input type="text" class="form-control"  name="username" required>
                                        </div>
                                        </div>
										</div>
										<div class="form-group">
										<div class="row">
                                        <label class="col-md-2 control-label left1" >Password</label>
                                        <div class="col-md-6">
                                           <input type="password" class="form-control"  name="userpass" required>
                                        </div>
                                        </div>
										</div>
                    
                                        <div class="form-group">
										<div class="row">
                                                <label class="col-md-2 control-label left1">User level</label>
                                                <div class="col-md-6">
                                                    <select name='userlevel' class='form-control'>";
                                                <option value='0'>----</option>
												<option value='1'>SuperAdmin</option>
												<option value='2'>Admin</option>
												<option value='3'>User</option>
												
                                                </select>
                                                </div>
                                        </div>
										</div>

                                        <hr>
										<div class="form-group">
                                            <div class="row">
											<div class="col-md-2 left1">
											</div>
                                                <div class="col-md-9" style="padding-left:25px;">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                   
                                                </div>
                                            </div>
                              </div>
</div><!-- /.box -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <?php include 'application/views/common/footer.php'; ?>
     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class='control-sidebar-bg'></div>
    </div><!-- ./wrapper -->
      
<script>
$("#ufile").change(function()
{
$("#response").html("");
});
</script>


  </body>
</html>