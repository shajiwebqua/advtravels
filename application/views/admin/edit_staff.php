<?php
 include("application/views/common/header.php");?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.ttext
{
	background-color:#fff;
	font-weight:bold;
	color:Green;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
  
   <?php 
    //include("application/views/common/top_menu.php");
    ?>
    
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Edit Staff Details</b></h1>
    <ol class="breadcrumb">
   <li> <a href="<?php echo base_url("Staff/view_staff");?>" style="color:blue;font-size:15px;"> <input type="button" class="btn btn-primary add" value="Back to Staff List"></a></li>
    </ol> 
    </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
  <!-- Content Header end -->
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
    <div class="row">
  <div class="col-md-12">

  <!--Add new user details -->
<div style="background-color:#fff;border-radius:2px;padding:15px; ">
    
      <div class="portlet-title">
 <!--   <div class="caption">
        <div style="color:blue;" >
          Details                   
        </div> -->
        <center><h5 style="color:green;" id="h5msg" ><?php echo $this->session->flashdata("message"); ?></h5></CENTER>
      <!--</div> -->
     </div>
 <label style="background-color:#e4e4e4;width:100%;height:1px"></label>
 <div class="portlet-body form">
 <form  onsubmit="return checkdata();" class="form-horizontal" role="form" method="post" action="<?php echo site_url("Staff/update_staff");?>" enctype="multipart/form-data">
                    <!-- text input -->
<div class="box-body">
<?php foreach($resulte as $values)
{ 
     ?> 

 <div class="row">
 <div class="col-md-7" style="border-right:1px solid #e4e4e4;">		

<div class='row'>
<div class='col-md-12'>
<div class='col-md-8'>
		<div class="form-group">
		<label class="col-md-3 control-label">Staff ID</label>
			<div class="col-md-9">
			 <input type='text' class='form-control' name='staff_id' value='<?=$values->staff_id;?>' required>
		</div>
		</div> 
		  
		<div class="form-group">
		   <label class="col-md-3 control-label">Join Date</label>
				<div class="col-md-9">
					<input type="date" class="form-control" name="sdate" value='<?=$values->staff_startdate;?>' required>
		</div>
		</div> 
		  <div class="form-group">
		   <label class="col-md-3 control-label"> Name</label>
				<div class="col-md-9">
					<input type="text" class="form-control" name="name" value='<?=$values->staff_name;?>' required>
			  </div>
			  </div>
			  
		<div class="form-group">
		 <label class="col-md-3 control-label">Address</label>
			<div class="col-md-9">
			   <textarea rows=2 cols=50 class="form-control"  name="address"  required><?php echo $values->staff_address;?></textarea>
			   </div>
		  </div>

</div>
<div class='col-md-4'>
					<div class="form-group" >
					  <!-- <label class="col-md-3 control-label">Photo</label>-->
                       <div class="col-md-12">
					   <center>
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                             <div class="fileinput-new thumbnail" style="width: 130px;height:135px;">
                                 <img src="<?=$values->staff_image;?>" alt=""/> </div>
                                 <div class="fileinput-preview fileinput-exists thumbnail" style="width:130px; height:135px;"> </div>
                             <div>
                              <span class="btn btn-default btn-file">
                              <span class="fileinput-new"> Select </span>
                              <span class="fileinput-exists"> Change </span>
                              <input type="file" name="photo" > </span>
                              <a href="javascript:;" class="btn btn-default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                         </div>
                        </div>
						</center>
                    </div>
                      <input type='hidden' name='simage' value='<?=$values->staff_image;?>'>
					</div>
</div>
</div>
</div>
<label style='width:100%;height:1px;background-color:#e4e4e4;margin-top:0px;'></label>
	<div class="row">
	<div class='col-md-6'>
				<div class="form-group" style="padding-top:10px;">
					 <label class="col-md-4 control-label" style='color:blue'>Staff Role</label>
					 <div class="col-md-8">
					  <select name='srole' class="form-control" required>
					<?php 
              foreach($result as $r1){
           echo "<option value='$r1->role_id'";
       if($values->staff_role==$r1->role_id)echo "selected";
         echo" > $r1->role_name </option>";
                                     }  
                         echo "</select>";?> 
                       </div>
				  </div>
				  
				  <div class="form-group">
					 <label class="col-md-4 control-label">Profession</label>
					 <div class="col-md-8">
				    <select name='position' class="form-control" required>
				           <?php
				foreach($result1 as $r1)
						   {
				echo "<option value= '$r1->profession_id'";
				echo ">$r1->profession_name</option>";
						   }
						    echo "</select>"; 
						   ?>
					 </select>
					 </div>
				  </div>
					 
				  <div class="form-group">
					 <label class="col-md-4 control-label">Date of birth</label>
					 <div class="col-md-8">
						 <input type="date" class="form-control" name="bdate"  value='<?=$values->staff_dob;?>' required>
					 </div>
				  </div>
				
					 <div class="form-group">
					 <label class="col-md-4 control-label" style='padding-top:2px;'>Gender</label>
					 <div class="col-md-8">
						 <input type="radio" name="gender"  value='Male' <?php 
                     echo set_value('gender', $values->staff_gender) == 'Male' ? "checked" : "";                     ?> >Male
						 <input type="radio" name="gender"  value='Female' <?php echo set_value('gender', $values->staff_gender) == 'Female' ? "checked" : ""; 
                      ?> style='margin-left:15px;'>Female
					 </div>
				     </div> 
					 
					 <div class='form-group'>
					 <label class='col-md-4 control-label'>Blood Group</label>
					 <div class='col-md-8'>
						 <select name='bgroup' class='form-control'  required>
						<?php
                 echo "<option value='O+'";if($values->staff_bloodgroup=='O+')echo "selected"; echo">O+</option>
                        <option value='A+' ";if($values->staff_bloodgroup=='A+')echo "selected"; echo ">A+</option>
                        <option value='B+' ";if($values->staff_bloodgroup=='B+')echo "selected"; echo ">B+</option>
                        <option value='AB+' ";if($values->staff_bloodgroup=='AB+')echo "selected"; echo ">AB+</option>
                        <option value='A-' ";if($values->staff_bloodgroup=='A-')echo "selected"; echo ">A-</option>
                        <option value='B-' ";if($values->staff_bloodgroup=='B-')echo "selected"; echo ">B-</option>
                        <option value='AB-' ";if($values->staff_bloodgroup=='AB-')echo "selected"; echo ">AB-</option>
                        <option value='O-' ";if($values->staff_bloodgroup=='O-')echo "selected"; echo ">O-</option>	
                     </select>";?>
		             </div>
				     </div> 
				          </div>
					 
		
		<div class='col-md-6'>
					<label style='margin-top:0xp;'>Mobile</label>
					<div class="form-group" style='margin-bottom:3px;'>
					 <div class="col-md-11">
						 <input type="number" class="form-control" name="mobile" value='<?=$values->staff_mobile;?>' required>
					 </div>
				  </div>
				 <label style='margin-top:0xp;'>Email</label>	 
				  <div class="form-group" style='margin-bottom:3px;'>
					 <div class="col-md-11">
						 <input type="email" class="form-control" name="email" value='<?=$values->staff_email;?>' required>
					 </div>
				  </div>
				  <!--  <label style='margin-top:0xp;'>Password</label>	 
				  <div class="form-group" style='margin-bottom:3px;'>
					 <div class="col-md-11">
						 <input type="password" class="form-control" name="password" value='<?=$values->staff_password;?>' required>
					 </div>
				  </div> -->
				<label style='margin-top:0xp;'>Basic Salary</label>	 
				  <div class="form-group" style='margin-bottom:3px;'>
					 <div class="col-md-11">
						 <input type="number" class="form-control" name="bsalary" value='<?=$values->staff_basicsalary;?>' required>
					 </div>
				  </div>
		</div>
	</div>	
</div>	  

<div class="col-md-5"> 
 
				  <div class="form-group" style='margin-bottom:3px;'>
				  <label  class='col-md-4 la-right' >Guardian/Spouse Name</label>	
					 <div class="col-md-8">
						 <input type="text" class="form-control" name="guardian" value='<?=$values->staff_guardian;?>' required>
					 </div>
				  </div>

				<div class="form-group" style='margin-bottom:3px;'>
				  <label  class='col-md-4 la-right' >Contact No</label>	
					 <div class="col-md-8">
						 <input type="text" class="form-control" name="contactno" value='<?=$values->staff_contactno;?>' required>
					 </div>
				  </div>


					 <div class="form-group" style='margin-top:15px;'>
						 <label class="col-md-4 control-label">Aadhar Card No</label>
					 <div class="col-md-8">
														   
						 <input type="text" class="form-control" name="aadhar" id="aadhar" value='<?=$values->staff_aadharcard;?>' required>
						<!--  <label id="mobmessage" style="color:red;"></label> -->                              
					 </div>
					 </div>
                        
                        <div class="form-group">
                      <label class="col-md-4 control-label">Pan Card Number</label>
                        <div class="col-md-8">
                                                   
						<input type="text" class="form-control" name="pancard" id="pancard" value='<?=$values->staff_pancard;?>' required>
                        <!-- <label id="landmessage" style="color:red;"></label> -->                            
                        </div>
                      </div>

					  <div class="form-group">
                      <label class="col-md-4 control-label">Bank Name</label>
                        <div class="col-md-8">
							<input type="text" class="form-control" name="bankname" value='<?=$values->staff_bankname;?>' required>
                        </div>
                      </div>
                    
                     <div class="form-group">
                      <label class="col-md-4 control-label">Bank Account No</label>
                        <div class="col-md-8">
							<input type="text" class="form-control" name="account" value='<?=$values->staff_bankaccount?>'  required>
                        </div>
                     </div>
					 		 
					 
					 <div class="form-group">
                      <label class="col-md-4 control-label">Bank IFSC Code</label>
                        <div class="col-md-8">
							<input type="text" class="form-control" name="ifsc" value='<?=$values->staff_ifsc;?>' required>
                        </div>
                     </div>
					 
					 
<!--<label style="color:blue;font-size:13px;margin-left:50px;"> Scan Your (Certificates,Licence,Id Card) and create a Zip file.<br> Then select file and upload. </label>-->

<!--<label style="color:blue;font-size:13px;margin-left:50px;"> Upload Your scanned copy of Certificates,Licence and Id Card.</label> -->
  
<?php
//$myString11 = $values->staff_certificate;
//$myArray11 = explode('/', $myString11);
?>	
		<!--<div class="form-group" >
             <label class="col-md-3 control-label" >Certificate : </label>
                <div class="col-md-8" style="padding-top:3px;">
                   <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-primary btn-file">
						<span class="fileinput-new"> Select file </span>
                        <span class="fileinput-exists"> Change </span>
						<input type="file" name="certificate" id="f" > </span>
						<span class="fileinput-filename"> </span> &nbsp;
						<a href="javascript:;"" class="close fileinput-exists" data-dismiss="fileinput"> </a><?php //echo ($myArray11[7]);?>
                   </div>
				</div>
				  <input type='hidden' name='certi' value='<?=$values->staff_certificate;?>'>
        </div> -->
		
		
         <?php
//$myString1 = $values->staff_licence;
//$myArray1 = explode('/', $myString1);
?>	
	<!--	<div class="form-group" >
             <label class="col-md-3 control-label" >Licence : </label>
                <div class="col-md-8">
                   <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-primary btn-file">
						<span class="fileinput-new"> Select file </span>
                        <span class="fileinput-exists"> Change </span>
						<input type="file" name="licence" id="f" > </span>
						<span class="fileinput-filename"> </span> &nbsp;
						<a href="javascript:;"" class="close fileinput-exists" data-dismiss="fileinput"> </a><?php //echo ($myArray1[7]);?>
                   </div>
                     <input type='hidden' name='lic' value='<?=$values->staff_licence;?>'>
				</div>
        </div> -->

        <?php
//$myString = $values->staff_idcard;
//$myArray = explode('/', $myString);

?>	
     <!--        <div class="form-group" >
             <label class="col-md-3 control-label" >Id Card : </label>
                <div class="col-md-8">
                   <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-primary btn-file">
						<span class="fileinput-new"> Select file </span>
                        <span class="fileinput-exists"> Change </span>
						<input type="file" name="idcard" id="f" > </span>
						<span class="fileinput-filename"> </span> &nbsp;
						<a href="javascript:;"" class="close fileinput-exists" data-dismiss="fileinput"> </a><?php //echo ($myArray[7]);?>
                   </div>
                     <input type='hidden' name='id' value='<?=$values->staff_idcard;?>'>
				</div>
        </div> -->


        <?php
//$myStringo = $values->staff_others;
//$myArrayo = explode('/', $myStringo);
?>		

   <!--       <div class="form-group" >
             <label class="col-md-3 control-label" >Others : </label>
                <div class="col-md-8">
                   <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-primary btn-file">
						<span class="fileinput-new"> Select file </span>
                        <span class="fileinput-exists"> Change </span>
						<input type="file" name="othersdoc" id="f" > </span>
						<span class="fileinput-filename"> </span> &nbsp;
						<a href="javascript:;"" class="close fileinput-exists" data-dismiss="fileinput"> </a><?php //echo ($myArrayo[0]);?>
                   </div>
                     <input type='hidden' name='others' value='<?=$values->staff_others;?>'>
				</div>
        </div>	
-->

         <div class='form-group'>
					 <label class='col-md-4 control-label'>Status:</label>
					 <div class='col-md-8'>
						 <select name='status' class='form-control'  required>
						<?php
                 echo "<option value='1'";if($values->staff_status=='1')echo "selected"; echo">Active</option>
                       
                        <option value='0' ";if($values->staff_status=='0')echo "selected"; echo ">Inactive</option>	
                     </select>";?>
		             </div>
				     </div> 						
                    <!--   <div class="form-group">
                         <label class="col-md-3 control-label">Status</label>
							<div class=" col-md-8">
						<select class="form-control" name="status">
							<option value="1">Active</option>
							<option value="0">Inactive</option>
						</select>
					</div>
					</div> -->
</div>
<?php } ?>
</div>
<label style="width:100%;height:1px;background-color:#e4e4e4;"></label>


				  <div class="form-group">
					   <div class="col-md-12">
							<center>
							<button type="submit" class="btn btn-success" style="padding:5px 20px 5px 20px;">Update Details</button>
							</center>
                       </div>
                  </div> 
</div>
 
</form>
  </div>	
</div>   
</div>
</div>
</div>

<?php include("application/views/common/footer.php");?>
 

     <script type="text/javascript">
	 
 
//sweet alert ----------------------------
	$("#h5msg").hide();
	if($("#h5msg").html()!="")
	{
		swal( $("#h5msg").html(),"","success")
		$("#h5msg").html("");
	}
// sweet alert box -------------------------- 
	 
     $("#mobmessage").hide();
     $("#landmessage").hide();  
  function checkdata()
  {
    
    var mob=$("#mobile").val();
    var landline=$("#landline").val();
      
  
    if(mob.length<10 || mob.length>10)
    {
	  $("#mobmessage").show();
      $("#mobmessage").html("Invalid mobile no,10 digits only.");
      return false;
    }
    else if(landline.length<11 || landline.length>11)
    {
	  $("#mobmessage").hide();
      $("#landmessage").show();
      $("#landmessage").html("Invalid mobile no,11 digits only.");
      return false;
    }
    
    
    else
    {
	  $("#mobmessage").hide();
      $("#landmessage").hide();
      $("#mobmessage").html("");
      $("#landmessage").html("");
      return true;
    }
  }

</script> 
 
 