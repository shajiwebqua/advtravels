<?php
 include('application/views/common/header.php');?>
<style>
.colpad
{
	padding-left:3px;
	padding-right:3px;
}

.numericCol
{
	text-align:right;
}

.chkCol
{
	text-align:center;
}


/*create invoice box style */
.inv-div
{
	position :fixed;
	top:0;
	left:0px;
	width:100%;
    height:100%;
background-color:;
z-index:9999;
display:none;	
}
.inv-div .content
 {
   border:2px solid #416796;
   position :absolute;
   top:100px;
   right:10px;
   width:470px;
   height:550px;
   background-color:#fff;
   z-index:9999;
   padding:0px;
}

.inv-div.open-div
{
	display:block;

}
a.view{padding: 3px; border: 1px solid transparent; border-radius:3px}
a.view:hover{background-color:#ddd !important; border-color: #999;}

a.invtrips{padding: 3px; border: 1px solid transparent; border-radius:3px}
a.invtrips:hover{background-color:#ddd !important; border-color: #999;}

.head-row
{
   background-color:#416796;
   color:#fff;
   padding:5px 2px 5px 2px;
   margin-left:0px;
   margin-right:0px;
}
.dt-row
{
   padding:5px 2px 5px 10px;
   margin-left:0px;
   margin-right:0px;
}

.p-col
{
	padding-left:2px;
	padding-right:0px;
}

.p-btn{
	padding:5px 15px 5px 15px;
}
/*---------------------------- */

.inv-btn-active{
	border:1px solid red;
	padding:3px;
}

.inv-btn-leave{
	border:0px;
	padding:0px;
}


</style>

<?php 
$cres=$this->db->select('customer_id,customer_name')->from('customers')->get()->result();
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
  <!-- Content Header (Page header) -->
  <section class="content-header">
  <h1><b>Invoice list</b></h1>
  <ol class="breadcrumb" style="font-size:15px;">
     <li> <a  href="<?php echo base_url('Invoice/Trips');?>"  style="color:#fff;"> <button  class='btn btn-primary' > <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Generate Invoice</button></a></li>
	<!-- <li><a  href="<?php echo base_url('Invoice/Payments');?>"  data-target="" data-toggle='modal' style="color:#fff;"><button class='btn btn-info' > <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Record Payments </a></button></li>  -->
  </ol>
  
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->   
    
  <section class="content"> 
  <!-- <div style="color:#4b88ed;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , And click on the button (Print,Share,Edit,View,Delete)  </div> -->
    <div style="padding:2px 0px 2px 0px;">
		 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
		 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div>
		 <div id='mserr'><?php echo $this->session->flashdata('message1'); ?></div>
		 </center>
		 </div>
	</div>
				
  <div class="row">
  
<!-- create invoice box ------------------------------------------------------>
	  <div class="col-md-12 inv-div">
		<div class="content">
			<div class='row head-row' >
			<div class='col-md-8' style='padding-left:2px;font-size:18px;'>Create Invoice</div>
				<div class='col-md-4' style='text-align:right;padding-right:2px;'>
					<button class="close-btn btn btn-default btn-xs"  >X</button>
				</div>
			</div>
			<form  class='form-horizontal' role='form' method='post' action='<?php echo site_url('Invoice/save_payments');?>' enctype='multipart/form-data'>
			<div id="inputform">
	
	
			</div>	
			</form>
		 
		</div>
	  </div>
<!-- create invoice box end------------------------------------------------------>

<div class="col-md-12">
<div class="portlet-body">
<div style="background-color:#fff;padding:15px; ">
              
		<div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">

			<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->

		<!-- search options -------------------------------------------->

	       <div class='row' style='margin:0px;background-color:#e4e4e4;padding:5px;'>

	    <!--<form role="form" method="POST" action="<?php echo base_url('Trip/booking_list/1')?>" enctype="multipart/form-data">-->
		
			<div class='col-md-1' style='padding-top:5px;'>	<label class='control-label'>Select: </label>	</div>
			<div class='col-md-2 ' style='padding-top:2px;'>
				<select class="form-control" id="unpaid" name="unpaid">
					<option value="">----------</option>
					<option value="0">Unpaid Invoices</option>
					<option value="1">Paid Invoices</option>
					<option value="2">All</option>
				</select>
			</div>
			
			 <div class='col-md-4 ' style='padding-top:2px;'>
				<select class="form-control select2" id="cltype" name="cltype">
					<option value="">--select client---</option>
						<?php
						foreach($cres as $cs)
						{
							echo "<option value='".$cs->customer_id."'>".$cs->customer_name."</option>";
						}
					?>
				</select>
				</div>
				<div class='col-md-1 ' style='margin-top:2px;'><input type="button" id="btnget" class="form-control btn btn-success" style="text-align:center;" name='btnget'  value="Get" > </div>
				<div class='col-md-1 ' style='margin-top:2px;'>&nbsp;</div>
				<div class='col-md-3 text-right' style='margin-top:2px;'><input type="button" id="btnall" class="btn btn-info" style="text-align:center;padding:3px 15px 3px 15px;" name='btnall'  value="All" > </div>
				
				
		<!--</form> -->

		</div>
	
	<!-- search options -------------------------------------------->
					
		 <div class="row" style='margin-top:10px;'>
                   <div class="col-md-12">
				   <label id="btitle" style='font-weight:bold;margin-bottom:20px;'><font color=#19a895><u>Invoice List</u></font></label>
				   </div>
				   </div>
		
            <div class="row" id='booklist'>
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
				   <div class="table-toolbar">
					<div class="row">
					<div class="col-md-12">
                  
                   </div>
                   </div>
                   </div>
             
            <table class="table table-striped table-hover table-bordered" id="example" width='100%'>
              <thead>
                <tr>
				 <th></th> 
				 <th>Id</th> 
 				 <th>Date</th> 
				 <th>Trips</th>
                 <th>Invoice_No</th>
				 <th width='25%'>Customer</th>
				 <th >Status</th>
				 <th>Amount</th>
				 <th>GST%</th>
				 <th>GST</th>
				 <th>Total</th>

				 </tr>
                </thead>
				<tbody>
			
				</tbody>
            </table>

        </div>
                       <!-- END BORDERED TABLE PORTLET-->
		</div>
		</div>
	
                <!-- END CONTENT BODY -->
	</div>
	</div>
            <!-- END QUICK SIDEBAR -->
</div>
  
	 <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" >

			</div>
			</div>
		<!-- /.modal-dialog -->
		</div>
	 </div> 
					 
  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
<!-- /.content --> 
</section>
  <!-- /.content-wrapper -->
</div>
 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">


var table="";
//$(".select2").select2();

$.fn.modal.Constructor.prototype.enforceFocus = function () {}; // to work select2 in model box
$(".select2").select2();

setInterval(function(){ $('#idh5').html(''); }, 3000);

$("#idmsg").hide();
$("#msgerr").hide();

//sweet alert ----------------------------
	if($.trim($("#idh5").html())!="")
	{
		swal("", $("#idh5").html(),"success")
		$("#idh5").html("");
	}
	
	if($.trim($("#msgerr").html())!="")
	{
		swal("",$("#idh5").html(),"error")
		$("#idh5").html("");
	}
	
// sweet alert box -------------------------- 

 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
 
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   }); 

get_invoice_datatable("","");

function get_invoice_datatable(up1,cl)
{   
$('#example').dataTable({
    "ordering":false,
    "destroy": true,
    "processing": true,
   
	"ajax": {
			url :"<?php echo base_url(); ?>" + "Invoice/unpaid_invoice_ajax/"+up1+"/"+cl,// json datasource
		   },
	   "aoColumnDefs": [
	    { "sClass": "numericCol", "aTargets": [7,8,9,10] }
		],	

		"columns": [
		    { "data": "pay"},
		    { "data": "invid"},
		    { "data": "invdate"},
			{ "data": "invtrips"},
			{ "data": "invno"},
            { "data": "invcust" },
			{ "data": "invstat" },
			{ "data": "invamt" },
            { "data": "invgstper" },
            { "data": "invgst" },
			{ "data": "invtotal" },
       ]
});
}
  
$('#btnget').click(function () 
{
	var up1=$("#unpaid").val();
	var cl=$("#cltype").val();
	if( cl=="" || up1=="")
	{
		swal("","Search option missing.","error");
	}
	else
	{
		get_invoice_datatable(up1,cl);
	}
});  
  
$('#btnall').click(function () 
{
	get_invoice_datatable("3","");
});   
  
 
  
  
$(".view").mousemove(function()
{
	$(this).addClass('inv-btn-active');
}); 
  
 jQuery('#master').on('click', function(e) {
	 if($(this).is(':checked',true))  
	 {
		 $(".sub_chk").prop('checked', true);  
		 $(".sub_chk").closest('tr').toggleClass('selected');
	 }  
	 else  
	 {  
		 $(".sub_chk").prop('checked',false);  
		 $(".sub_chk").closest('tr').removeClass('selected');
	 }  
 });

  
  $('#example tbody').on( 'click', '.sub_chk', function ()
  {
        
		$(this).closest('tr').toggleClass('selected');
  });  
	
/*  $('#example tbody').on( 'click', '.sub_chk', function ()
  {
        $(this).toggleClass('selected');
		if ($(this).hasClass('selected') ) {
             $(this).closest('tr.selected').find('.sub_chk').prop('checked',true);
         }
         else {
			$(this).closest('tr').find('.sub_chk').prop('checked',false);
         }
  });*/
   
		
		
$('#example tbody').on('click', '.view',function(){

        var pdf_link = $(this).attr('href');
		var ivid= $(this).attr('id');
		//alert(pdf_link+"/"+ivid);
		
		var iframe = '<div class="iframe-container"><div id="overlay"><div id="loading"></div><iframe id="myDiv" class="animate-bottom" src="'+pdf_link+'/'+ivid+'"></iframe></div></div>';
      $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
     return false; 

 });    
	
	
	
$('#example tbody').on('click', '.invtrips',function(){

        var pdf_link = $(this).attr('href');
		var ivid= $(this).attr('id');
		var cid= $(this).attr('ref');
		
		//alert(pdf_link+"/"+ivid);
		
		var iframe = '<div class="iframe-container"><div id="overlay"><div id="loading"></div><iframe id="myDiv" class="animate-bottom" src="'+pdf_link+'/'+ivid+'/'+cid+'"></iframe></div></div>';
      $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
     return false; 

 });    	

/*var tid="";
    $.each($("#example tr.selected"),function(){ 
        tid+=","+$(this).find('td').eq(1).text(); 
    });
    $("#trip_ids").val(tid);
	if($.trim(tid).length<=0)
		{
			swal("Cancelled", "Please select unpaid trips.","");
		}
		else
		{
		$(".inv-div").addClass('open-div');
		}
	*/

   $('#example tbody').on('click', '.btnpay', function () {

	 var Result=$("#myModal2 .modal-body");
         var id =  $(this).attr('id');
		 $(".btnpay").attr('data-target','#myModal2')
			jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "Invoice/record_payment",
			dataType: 'html',
			data: {ivid: id},
			success: function(res) {
				Result.html(res);
            }
            });
    
        });
	
		
		
		
		
		
		
		
		

$(".close-btn").click(function(){
    $(".inv-div").removeClass('open-div');
});

//-------------------------------------------
  
   $('#idsave').on('click',function(){
       //var id=$('#example').find('tr.selected').find('td').eq(2).text();
          var dt1=$('#datepicker1').val();
          var m=$('#lmonth').val();
       var pdf_link = $(this).attr('href')+'/'+m+'/'+dt1;
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
		window.open(pdf_link);
      return false; 
   }); 

  
   $('#view-pdf').on('click',function(){
     
		var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        $.createModal({
        title:'Booking List',
        message: iframe,
        closeButton:true,
        scrollable: true,
   });
       
	return false; 

   });  
  	
// -----------------------------------------------------------------------------------------------------------------------------------    
    
  $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this invoice?');
   });

		
  $(document).on("click", "#del_conf1", function () {
           return confirm('Are you sure you want to Cancel this invoice?');
  });

		
</script>



