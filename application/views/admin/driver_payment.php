<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 class='heading' style='color:#00adee'>Driver Payments 
    </h1>
    <ol class="breadcrumb">
      <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
    </ol>
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
  <!-- Content Header end -->
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">

        <!--Add new user details -->
        <div style="background-color:#fff;border-radius:10px;padding:15px; ">

          <div class="portlet-title">
   <!--   <div class="caption">
          <div style="color:blue;" >
            Details                   
        </div> -->
        <center><h5 style="color:green;display: none;" id="msg"><?php echo $this->session->flashdata("message"); ?></h5></center>
        <center><div id='drivers'><?php echo $this->input->post("driver_id"); ?></div></center>
        <!--</div> -->
    </div>
    <div class="portlet-body form">
      <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('Driver/pay_amount'); ?>" >
        <!-- text input -->
        <div class="box-body">

          <div class="row">
                
                  <div class='col-md-5' style="border-right:1px solid #e4e4e4;">
                  <h3>Payment</h3>
                    <div class="form-group">
                      <label class="col-md-4 control-label">Driver Name : </label>
                      <div class="col-md-8">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <input type="text" class="form-control" onkeydown="return false" placeholder='Total payed' name='payed' value="<?php echo $driver_name[0]->driver_name ?>" id='payed'>
                        </div>
                      </div> 
                    </div>                    

                    <div class="form-group">
                      <label class="col-md-4 control-label">Total : </label>
                      <div class="col-md-8">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <span class="input-group-addon">Rs</span>
                          <input type="text" class="form-control" onkeydown="return false" placeholder='Total payed' name='totla' style="text-align:right;" id='total' required>
                        </div>
                      </div> 
                    </div>

                    <div class="form-group">
                      <label class="col-md-4 control-label">Payed : </label>
                      <div class="col-md-8">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <span class="input-group-addon">Rs</span>
                          <input type="text" class="form-control" onkeydown="return false" placeholder='Total payed' name='payed' style="text-align:right;" id='payed' value="<?php echo $payment_charges[0]->debit; ?>" required>
                        </div>
                      </div> 
                    </div>

                    <div class="form-group">
                      <label class="col-md-4 control-label">Total payable : </label>
                      <div class="col-md-8">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                          <span class="input-group-addon">Rs</span>
                          <input type="text" class="form-control" onkeydown="return false" placeholder='Balance' name='balance' style="text-align:right;" id='balance' value="<?php echo $payment_charges[0]->credit - $payment_charges[0]->debit; ?>" required>
                        </div>
                      </div> 
                    </div>

                    <label style='width:100%;height:1px;background-color:#e4e4e4;margin-top:0px;'></label>
                                
                                <div class="col-md-8 col-md-offset-4">
                    <div class="form-group">
                      <label class="col-md-4 control-label">Amount : </label>
                      <div class="col-md-8">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0" style="border:1px solid #cccccc;">
                          <span class="input-group-addon" style="border:none;">Rs</span>
                          <input type="text" class="form-control" onkeydown="return isNumber(event);" onkeyup="check(this)" placeholder='Pay Amount' name='amount' style="text-align:right;" id='amount' required>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-4 control-label">Balance : </label>
                      <div class="col-md-8">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0" style="border:1px solid #cccccc;">
                          <span class="input-group-addon" style="border:none;">Rs</span>
                          <input type="text" class="form-control" onkeydown="return isNumber(event);" onkeyup="check(this)" placeholder='Pay Amount' name='lst_bal' style="text-align:right;" id='lst_bal' value="<?php echo $payment_charges[0]->credit - $payment_charges[0]->debit; ?>" required>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-12 text-right">
                        <button class="btn btn-primary">payed</button>
                      </div>
                    </div>

                  </div>
              
          </div>
          <div class="col-md-7">
          <h3>Driver Trip List</h3>
            <table class="table table-striped table-hover table-bordered" id="example">
              <thead>
                <tr style="color:#5068f8;">
                     <th>Id</th>
                     <th>Start Date</th>
                     <th>End Date</th>
                     <th>No of Days</th>
                     <th>Trip Charge</th>
                     <th>Driver Charge</th>
                     <th>Driver Bata</th>
                     <th>Total</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>

      </form>
    </div>  
  </div>   
</div>
</div>
</section>

</div>

<?php include('application/views/common/footer.php');?>
<script type="text/javascript">
//-------------------------------------
 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
   });
   
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
   });
  var driver_id = $('#drivers').text();

       var table = $('#example').dataTable( {
        "ordering": false,
         destroy: true,
        "processing": true,
        "initComplete": function () {
            var api = this.api();
              length = api.rows().data().length;
              if(length > 0){
                total = 0
                for(i=0;i<length;i++){
                  total  += parseInt(api.row(i).data().total)
                }
                $("#total").val(total);
              }
            
        },
        "ajax":{
                url :"<?php echo base_url(); ?>" + "Driver/cashpayed_ajax/" + driver_id
        },
    "columnDefs":[
    {"width":"10%","targets":0},
    {"width":"12%","targets":1},
    {"width":"12%","targets":2},
    ],
         
        "columns": [
      { "data": "tripid" },
      { "data": "startdate"},
      { "data": "enddate"},
      { "data": "days"},
      { "data": "tcharge"},
      { "data": "dcharge" },
      { "data": "dbata" },
      { "data": "total" }
       ]
    });

    function load_drivers_payments(driver_id){
      jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Driver/driver_payment_ajax",
        dataType: 'html',
        data: {driverid: driver_id},
        success: function(data) {
          data = JSON.parse(data)
          $("#payed").val(data.totalPaid)
          $("#balance").val(data.balance)
          $("#amount").prop("readonly", false)
        }
      });
    }

var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });	

  function check(obj){
    pay = parseFloat($(obj).val());
    balance = parseFloat($("#balance").val());
    if(pay > balance){
      pay = pay.toString()
      pay = pay.substr(0, pay.length - 1);
      console.log("pay : " + pay);
      $(obj).val(pay)
    }
  }

		$(document).ready(function(){
      driver_id = $('#drivers').val()
      if(driver_id != "default"){
        load_drivers_trips(driver_id)
        load_drivers_payments(driver_id)
      }
      
        if($("#msg").html()!="")
          {
            swal($("#msg").html(),"","success")
            $("#msg").html("");
          }
    });
  
	
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

      //$.fn.dataTable.ext.errMode = 'throw';
</script>



