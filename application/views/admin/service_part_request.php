<?php
include('application/views/common/header.php');
?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1><b> Vehicle  Repairs & Service  Request </b></h1>
		<ol class="breadcrumb" style='font-size:15px;'>
			<li><a href="<?php echo base_url('Services/request_list');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Service Request List</button></a></li>
			
	    </ol> 
         <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
  
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
  <div class="col-md-12">

   <div style="background-color:#fff;border-radius:2px;padding:15px; ">
    
            <div class="portlet-title">
                     <div class="caption" >
							<h4 style='margin-top:0px;'><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Parts Details</h4>
                     </div>
            </div>
			
  <hr style="background-color:#cecece,border:1px;width:100%;top:10px;margin:0px;padding:3px 0px 10px 0px;">
             
 <div class="portlet-body form">
 <div class='row'>
 <div class="col-md-5">
 <form enctype="multipart/form-data" method="post" id="addservicerequest" name="addservicerequest" action="">
        <div class="form-group">
        <label for="date">Date</label>
		<input type='text' class='form-control' name='sdate' id='datepicker1' placeholder="date"  required >
        </div>
        <div class="form-group">
        <label for="reg_no">Reg No:</label>
		
		<select id="reg_no" name="reg_no" class="form-control" required>
          <option value="">----select----</option>
          <?php 
		  $vehregno=$this->db->select('vehicle_id,vehicle_regno')->from('vehicle_registration')->get()->result();
		  foreach($vehregno as $row)
		  {
		   ?>
          <option value="<?= $row->vehicle_id;?>"><?=$row->vehicle_regno;?></option>
          <?php 
		  } 
		  ?>
        </select>
        
        </div>
        <div class="form-group">
        <label for="vehicle_type">Vehicle Type:</label>
        <select name="vehicle_type" id="vehicle_type" class="form-control" required>
          <option value="">----select----</option>
          <?php 
		  $vtypes=$this->db->select('*')->from('vehicle_types')->get()->result();
		  foreach($vtypes as $row)
		  {
		   ?>
          <option value="<?= $row->veh_type_id ?>"><?= $row->veh_type_name;?></option>
          <?php
		  } 
		  ?>
        </select>
        </div>
        <div class="form-group">
        <label for="service_category">Service Category:</label>
        <select name="service_category" id="service_category" class="form-control" required>
          <option value="">----select----</option>
          <option value="1">Company Service</option>
          <option value="2">Outside Service</option>
        </select>
        </div>


        <div class="form-group">
        <label for="date_time">Expected date & time for Service Entry :</label>
		<input type='text' class='form-control' name="date_time" id='datepicker2' placeholder="date"  required >
         </div>
        <div class="form-group">
        <label for="duration">Expected duration for delivery :</label>
        <input type="text" class="form-control" id="duration" name="duration" required>
        </div>
        <div class="form-group">
        <label for="center/workshop">Service centre/Workshop :</label>
        <textarea name="center_workshop" id="center_workshop" class="form-control" required></textarea>
        </div>
        <!--<button type="submit" class="btn btn-default">Submit</button>-->
        <!--</form>-->
        </div>
<div class="col-md-1">	</div>

<div class="col-md-6">
        <!--<form>-->
		<div class='row'>
		<label style='margin-left:15px;'><b>Add Service Parts here</b></label>
		</div>
		<div class='row' style='margin-bottom:15px;'>
		<hr style='margin:0px;'>
		</div>
		
        <div class="row">
        <div class="col-md-7">
        <input type="text" id="parts" placeholder="Parts" class="form-control">
        </div>
        <div class="col-md-3">
        <input type="text" id="price" placeholder="Price" class="form-control">
        </div>
        <div class="col-md-2"></div>
        <input type="button" class="add-row btn btn-default btn-sm" value="Add Row" >
        </div>

        <!--</form>-->
        <br>
            <table id="example" class="table table-striped table-bordered display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <td width="8%">No</td>
                <td width="55%">Parts</td>
                <td width="20%">Price</td>
                <!--<td width="10%">Action</td>-->
            </tr>
        </thead>
        <tbody>
           
        </tbody>
	</table> 
    <button type="submit" class="btn btn-primary" style='padding:5px 20px 5px 15px;' > <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
  
  </div>
  </form>
</div>
  
 
 </div> <!--portlet-body-->
 
 </div>
</div>    
<br>  
  </div>
  </div>
</section>

<?php include('application/views/common/footer.php');?>
  </div>
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script> 

<script type="text/javascript">
/*$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
*/
  
  $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
 
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
	//endDate:'now'
   }); 
    
	
var i=1;
$(".add-row").click(function(){
    var parts = $("#parts").val();
    var price = $("#price").val();
    $("#parts").val('');
    $("#price").val('');
    var markup = "<tr id='"+i+"'><td>"+i+"</td><td>" + parts + "</td><td>" + parseFloat(price).toFixed(2)+ "</td></tr>";
    $("table tbody").append(markup);
    i++;
});
  
  
function storeTblValues()
{
    var TableData = new Array();

    $('#example tr').each(function(row, tr){
        TableData[row]={
            "No" : $(tr).find('td:eq(0)').text()
            , "Parts" :$(tr).find('td:eq(1)').text()
            , "Price" : $(tr).find('td:eq(2)').text()
        }    
    }); 
    TableData.shift();  // first row will be empty - so remove
    return TableData;
}


$(document).on("submit", "#addservicerequest", function (event) {
          
    event.preventDefault();
    var edate =  $('#datepicker1').val();
    var reg_no =  $('#reg_no').val();
    var vehicle_type =  $('#vehicle_type').val();
    var service_category =  $('#service_category').val();
    var date_time =  $('#datepicker2').val();
    var duration =  $('#duration').val();
    var centerworkshop =  $('#center_workshop').val();
	
    var TableData;
    TableData = $.toJSON(storeTblValues());
		
    $.ajax({
		url: "<?php echo base_url();?>Services/add_service_request",
		type: 'POST',
		data: {pTableData:TableData,edate:edate,reg_no:reg_no,vehicle_type:vehicle_type,service_category:service_category,date_time:date_time,duration:duration,centerworkshop:centerworkshop},
		
		success: function (data, status) {
	   
		  if(data=="success")
		  {
			  swal({
				  title: "Saved",
				  text: "Service parts request details added.!",
				  type: "success",
				  showCancelButton: false,
				  confirmButtonClass: "btn-success",
				  confirmButtonText: " Ok ",
				  closeOnConfirm: false
				},
				function(){
				  window.location.reload(); 
				});
		  }
		  else
		  {
			swal("Error", "Something went wrong", "error");    
		  }  
		}
	  });
});
  
  
/*$("#btnexp1").click(function()
{
	var etype1=$("#exptype1").val();
	var egroup=$("#expgroup").val();
	jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Vehicle/add_exp_type",
        dataType: 'html',
        data: {exptype:etype1,expgroup:egroup},
        success: function(res) {
		$("#etype").append("<option value='"+res+"'>"+etype1+"</option>")
                    }
            });
});
*/

</script>


  </body>
</html>
