<?php
 include('application/views/common/header.php');?>
<style>
tr{
	height:25px;
}
table.tb1 td
{
	text-align:right;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
 <!-- Main content -->
 
 
<!-- Content Header (Page header) -->
  <section class="content-header">
     <h1><b>Vehicle Date wise Report</b></h1>
				   
    <ol class="breadcrumb" style='font-size:15px;'>
	<li><label> Print Report <b>( Ctrl+P)</b></label> </li>
	<li><a  href="<?php echo base_url('Reports/vehiclereport') ;?>"  style='color:#4b88ed;'><button class='btn btn-warning btn-xs'><i class="fa fa-backward" aria-hidden="true"></i> &nbsp;&nbsp;Back&nbsp;&nbsp;&nbsp; </button></a></li>
  	<!--<li><a  id="view-pdf" href="<?php echo base_url('Pdf/pdf_vehicle');?>"  style='color:#4b88ed;'><i class="fa fa-print" aria-hidden="true"></i>Print/Save</a></li>-->
	</ol> 
	
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 	  
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
		
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details -->
	
	 <div style="background-color:#fff;padding:15px; ">
							
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                <div id='idmsg' style="background-color:#fff;height:20px;">
					<center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div></center>
				</div>
				
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				<div class="row" style='padding:0px 20px 5px 20px;'>
				<label class='col-md-8' style='font-size:18px;'> Report of the Vehicle : <span><b><?php echo $vehicleno;?></b></span></label>
				<label class='col-md-4' style='text-align:right;font-size:14px;'> Date: <?php echo Date('d-m-Y');?></label>
				</div >				
				<hr style='margin-top:0px;'>
				
				<div class="row" style='padding-left:20px;padding-right:20px;'>
					<div class="col-md-12">
					 <table style="border:none;width:100%;">
					  <tr>
						  <td>
							  <address>
								<strong>Advance World Group</strong><br>
								Advance World Group LLC,P.O.Box : 7192<br>
								Al jurf Industrial Area, Al Jurf 1, Ajman UAE<br>
								Tel: +971 6 748 7636<br>
								Fax: +971 6 748 7637
							  </address></td>
					  </tr>
					</table>
						<div class='row col-md-12' style='text-align:right;'>
							<label class='control-label' style='font-size:16px;'>Vehicle Date wise Report : <b><?php echo $sedates;?></b></label>
						</div>
						<div class='row col-md-12' style='text-align:right;margin-bottom:15px;'>
							<hr style='margin:0px;'>	
						</div>
					
					
									
					<?php 
					   $gtotalexp=0;
					   $gtotalinc=0;
					   $lvcost=0;
					   
					   $lvpr=$this->db->select('vehicle_amount')->from('vehicle_registration')->where('vehicle_id',$regid)->get()->row();
					   $lvcost=$lvpr->vehicle_amount;
					   
					if(if_data_exist($regid, "Model_trip", "check_regno"))
					{ 
					   foreach($loan as $l1)
					   {
								     ?>
					<div class='row' style='padding-top:7px;'>
					<div class='col-md-12'>
						<h4 style='margin-top:3px;'>Vehicle Cost :</h4>
						<table width='97%' class='tb1'>
						<tr><td>Cost of Vehicle ( Price + Extra + Loan interest)</td><td > </td><td><b>₹ <?= number_format($l1->loan_vehicleamount+$l1->loan_othercharges+$l1->loan_interest,"2",".","");?></b></td></tr>
						</table>
					</div>
					</div>				 
									 
					<?php
						$ltot=$l1->loan_amount+$l1->loan_interest+$l1->loan_othercharges;
						$lpaid=230000+$l1->loan_downpayment;
						$lvcost=$l1->loan_amount+$l1->loan_interest+$l1->loan_othercharges+$l1->loan_downpayment;
						?>
						
					<div class='row' style='padding-top:7px;'>
					<div class='col-md-12'>
						<h4 style='margin-top:0px;'>Loan Details :</h4>
						<table width='97%' class='tb1' >
						<tr><td>Loan Amount</td><td>₹ <?=number_format($l1->loan_amount,"2",".","");?></td><td>Down Payment</td><td>₹ <?=number_format($l1->loan_downpayment,"2",".","");?></td></tr>
						<tr><td>Total Intrest</td><td>₹ <?=number_format($l1->loan_interest,"2",".","");?></td><td>Total Loan Amount Paid</td><td>₹ <?=number_format($lpaid,"2",".","");?></td>     </tr>
						<tr><td>Other Charges</td><td>₹ <?=number_format($l1->loan_othercharges,"2",".","");?></td><td></td><td></td></tr>
						<tr><td colspan='2'>------------------------------------------------------------------------</td> <td colspan='2'>------------------------------------------------------------------------</td></tr>
						
						<tr><td>Total Loan</td><td><b>₹ <?=number_format($ltot,"2",".","");?></b></td><td>Loan Paid</td><td><b>₹ <?=number_format($lpaid,"2",".","");?></b></td></tr>
						<tr><td colspan='2'>------------------------------------------------------------------------</td> <td colspan='2'>------------------------------------------------------------------------</td></tr>
						
						<tr style='font-size:16px;font-weight:600;'><td colspan='3'>Loan Blance( <?=$ltot." - ".$lpaid;?> ) </td><td cospan='4'>₹ <?=number_format($ltot-$lpaid,"2",".","");?></td></tr>
						<tr><td colspan='4'>===============================================</td> 

						</table>
					</div>
					</div>
					
					<!--<label style='margin:0px;width:100%;height:1px;background-color:#c0c0c0;'></label>-->
					<hr style='margin:0px;'>
					
					<?php 
					   }
					}
					
					?>

					<div class='row' style='padding-top:7px;'>
					<div class='col-md-12'>
						<h4 style='margin-top:3px;'>Vehicle Trip Details</h4>
						<table width='97%' class='tb1'>
						<tr><td >Total vehicle cost</td><td width='200px' ></td><td width='150px'><b>₹ <?php echo number_format($lvcost,"2",".",""); ?></b></td></tr>
						<tr><td >Total Trip Charges</td><td width='200px' ><b>₹ <?php echo number_format($tripcharge,"2",".",""); ?></b></td><td width='150px'></td></tr>
						
						<?php
						$gtotalinc+=$tripcharge;

						if($expense!=='0')
						{

						foreach($expense as $exp)
						 {
						 ?>
						 <tr><td><?=$exp->etype_name;?></td><td ></td> <td><b> ₹ <?= number_format($exp->expense_amount,"2",".","");?></b></td></tr>
						 <?php
						 $gtotalexp+= $exp->expense_amount;
						 }
						 $gtotalexp+=$lvcost;
						}
						else
						{
							$gtotalexp+= $lvcost;
						}
						 ?>
						<tr><td>Periodic Service Charges</td><td width='200px' > </td><td width='150px'><b>₹ <?= number_format($service,"2",".","");?></b></td></tr>						
						</table>
					</div>
					</div>
						<?php
						 $gtotalexp+= $service;
						 ?>	
					<label style='margin:0px;width:100%;height:1px;background-color:#c0c0c0;'></label>
								
					
					<div class='row' style='padding-top:7px;'>
					<div class='col-md-12'>
						<table width=97%' class='tb1' style='font-size:18px;'>
						<tr><td>Totals</td><td width='200px' ><b>₹ <?php echo number_format($gtotalinc,"2",".","");?></b></td>
								<td width='150px'><b>₹ <?php echo number_format($gtotalexp,"2",".","");?></b></td></tr>
								<tr><td colspan='3' align='right'>----------------------------------------------------</td></tr>
						<tr style='font-size:16px;font-weight:600;'>		
						<?php
						$netlp=$gtotalinc-$gtotalexp;
						
						$gtotalinc=number_format($gtotalinc,"2",".","");
						$gtotalexp=number_format($gtotalexp,"2",".","");
						
						$netlp=number_format($netlp,"2",".","");
						
						if($netlp<0)
						{
							echo "<td>Net <b>Loss</b> ( ₹ <span style='font-size:14px;'>".$gtotalinc." - ₹ ".$gtotalexp."</span>)</td><td colspan='2' style='font-size:25px;'>₹ &nbsp;&nbsp;".$netlp."</td>";
						}
						else
						{
							echo "<td>Net Profit ( ₹ <span style='font-size:14px;'>".$gtotalinc." - ₹ ".$gtotalexp." </span> )</td><td colspan='2' style='font-size:25px;'>₹ &nbsp;&nbsp;".$netlp."</td>";
						}
						?>
						</tr>	
						<tr><td colspan='3' align='right'>=================================</td></tr>
						
						</table>
					</div>
					</div>
					
								<!-- BEGIN BORDERED TABLE PORTLET-->
					</div>
					<!-- END CONTENT BODY -->
				</div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  

     						
				
	</div>
	<!-- End user details -->
	</div>
	
	</div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>


<script type="text/javascript">


</script>



