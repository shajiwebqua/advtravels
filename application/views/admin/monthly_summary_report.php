<?php
include('application/views/common/header.php');
?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.td_style
{
 text-align:right;
 padding-right:5px; 
 }
 table td
 {
	 font-size:18px;
 }
 p{ margin:0px;padding:0px;}

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1><b>Monthly Summary Report</b></h1>
		<ol class="breadcrumb" style='font-size:15px;'>
			<!--<li><a href="<?php echo base_url('Services/request_list');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Service Request List</button></a></li> -->
			<li> <a id="view-pdf" href="<?php echo base_url('Reports/summary_report');?>" style="color:#4b88ed;"><button><i class="fa fa-print" aria-hidden="true"></i> Print </button> </a></li>
			</ol> 
         <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
  
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
  <div class="col-md-12">

   <div style="background-color:#fff;border-radius:2px;padding:15px; ">
     <div class='row' style=' background-color:#e4e4e4;margin:0px;padding:5px;'>
 <form action="<?php echo base_url('Reports/Summary')?>" method="POST" id="report_form">
	<label class='col-md-2 la-right'>Select Date:</label>
	<div class='col-md-3' style='padding:0px;'>
        	 <select type="text" name="month" id="month" class="form-control">
        	 	    <option value="">---------</option>
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05">May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                 </select>
	</div>
	<div class='col-md-2'>
	 <select name='year' class='form-control' required>
		<option value='' >-year-</option>
		<?php
		$y=date('Y')+1;
		for($x=$y;$x>=2017;$x--)
		{
		?>
		<option value='<?=$x;?>' <?php if(date('Y')==$x) echo "selected";?> ><?=$x;?></option>
		<?php
		}
		?>
		
   </select>
	</div>

	<div class='col-md-1' style='padding:2px;margin-left:10px;'>
	<button type='submit' name='btnget1' style='padding-left:15px; padding-right:15px;' >Get</button>
	</div>
	</form>
	</div>
 
		<!--    <div class="portlet-title">
                     <div class="caption" >
							<h4 style='margin-top:0px;'><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Parts Details</h4>
                     </div>
            </div> -->
			
  <hr style="background-color:#cecece,border:1px;width:100%;top:10px;margin:0px;padding:3px 0px 10px 0px;">
             
 <div class="portlet-body form">
 <div class='row' style='margin-bottom:15px;'>
 <div class='col-md-12'>
 <?php
 $dcap=$msmonth. " -" .$msyear;
 $total_expense=0;
 $total_income=0;
 $tot_aincome=0;
 $tot_income=0;
 ?>
 <h4><b>Summary Report of : &nbsp;&nbsp; <?=$dcap;?></b></h4>
 </div>
 </div>
 
 
 
 <div class='row'>
        <div class='col-md-12'>
		<table style="width:70%;border:1px solid #e4e4e4;" cellpadding=5 cellspacing=0 class="table table-stripped" border=1 >
		   <thead>
			<tr>
			   <tr style='color:#4b88ed;border:1px solid #e4e4e4;font-size:16px;'>
			     <th width='10%'>Sl No</th>
				 <th>Particulars</th>
				 <th width='15%' style='text-align:right;'>Income</th>
				 <th width='15%' style='text-align:right;'>Expense</th>
				</tr>
			</tr>
		</thead>
		<tbody>
			    <tr>
					<td>1</td>
					<td>Vehicle Loan Paid</td>
					<td class="td_style">-</td>
					<td class="td_style"><?php $total_expense = 0;$total_income=0;
						$loan = number_format((float)$vehicle_loan->LOAN_EXPENSE,2,'.','');
						echo $loan;
						$total_expense = $total_expense + $loan;?> </td>
				</tr>
				<tr>
					
					<td>2</td>
					<td>Trip Collections </td>
              	    <td class="td_style"><?php
					 $tot_income=number_format($trip_total->TRIP_AMOUNT,2,'.','');
					 echo $tot_income;
					 $total_income = $total_income + $tot_income;
					  ?>
					 </td>
          		    <td class="td_style">0.00</td>
				</tr>
				
				<tr>
					
					<td>3</td>
					<td>Additional Income </td>
              	    <td class="td_style"><?php
					 $tot_aincome=number_format($additional_income->ADDI_INCOME,2,'.','');
					 echo $tot_aincome;
					 $total_income = $total_income + $tot_aincome;
					 ?>
					 </td>
          		    <td class="td_style">0.00</td>
				</tr>
				
				
				
				<!--<tr>
					<td>3</td>
					<td>Vehicle Service Charges</td>
					<td class="td_style">-</td>
					<td class="td_style"><?php //$service = number_format((float)$vehicle_service->SERVICE_EXPENSE,2,'.','');
						//echo $service;
						//$total_expense = $total_expense + $service;?></td>
				</tr>-->
			    <tr>
					<td>4</td>
					<td>General Expense </td>
					<td class="td_style">-</td>
					<td class="td_style"><?php $general = number_format((float)$general_expense->GEN_EXPENSE,2,'.','');
						echo $general;
						$total_expense = $total_expense + $general;?></td>
				</tr>
				<tr>
					<td>5</td>
					<td>Vehicle Expense </td>
					<td class="td_style">-</td>
					<td class="td_style"><?php $vehicle = number_format((float)$vehicle_expense->EXPENSE,2,'.','');
						echo $vehicle;
						$total_expense = $total_expense + $vehicle;?></td>
				</tr>
				
				<tr>
					<td>6</td>
					<td>Salary Advance</td>
					<td class="td_style">-</td>
					<td class="td_style"><?php $advance = number_format((float)$advance->ADVANCE,2,'.','');
						echo $advance;
						$total_expense = $total_expense + $advance;?></td>
				</tr>
				
				
				<tr>
					<td>7</td>
					<td>Staff Salary</td>
					<td class="td_style">-</td>
					<td class="td_style"><?php $salary = number_format((float)$salary->SALARY,2,'.','');
						echo $salary;
						$total_expense = $total_expense + $salary;?></td>
				</tr>
				 
			    <tr>
			    	<td></td>
			    	<td><b>Total<b></td>
			    	<td class="td_style"><b><?= number_format($total_income,2,'.','');?></b></td>
			    	<td class="td_style"><b><?= number_format($total_expense,2,'.','');?></b></td>
			    </tr>
			    
				<?php
				
				$lp=$total_income-$total_expense;
				if($lp<0)
				{
					$lptext="Net Loss  :"; 
					$lpamt=number_format($lp,2,'.','');
				}
				else
				{
					$lptext="Net Profit:"; 
					$lpamt=number_format($lp,2,'.','');
				}
				?>
				<tr> <td colspan="4" height=20px></td> </tr>
				<tr>
			    	<td colspan="3" class="td_style">Total Income :<br>Total Expense :<br><br>Net Amount :</td>
			    	<td class="td_style"><b><?=number_format($total_income,2,'.','');?></b>
					<p><b><?=number_format($total_expense,2,'.','');?></b></p>
					<p>------------------</p>
					<p><b><?=number_format($lp,2,'.','');?></b></p>
					<p>------------------</p></td>
			    </tr>
				<tr height='50px'>
			    	<td colspan="4" style='font-size:20px;text-align:right;'><span style='padding-right:20px;'><b><?=$lptext;?></span> &nbsp;&nbsp;&nbsp;<?=$lpamt;?></b><br>===================</td>
			    	
			    </tr>

		</tbody>
		</table>

</div>
  
 </div> <!--portlet-body-->
 
 </div>
</div>    
</div>

  </div>
  </div>
</section>

<?php include('application/views/common/footer.php');?>
  </div>
<script>
 $('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});
  </script> 

<script type="text/javascript">

/*$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
*/

   $('#view-pdf').on('click',function(){
     
		var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        $.createModal({
        title:'Booking List',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
	   return false; 

    });  
  
</script>

  </body>
</html>
