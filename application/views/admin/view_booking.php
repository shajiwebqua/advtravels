<?php
 include('application/views/common/header.php');?>
<style>
.colpad
{
	padding-left:3px;
	padding-right:3px;
}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
  <!-- Content Header (Page header) -->
  <section class="content-header">
  <h1><b>Booking List</b></h1>
  <ol class="breadcrumb" style="font-size:15px;">
<!--  <li><a id='view-pdf' href="<?php echo site_url('Pdf/pdf_log') ?>" style='color:#4b88ed;' ><i class="fa fa-print" aria-hidden="true"></i>Print</a></li> 
  <li><a id='idsave' href="<?php echo site_url('Pdf/pdf_log') ?>" style='color:#4b88ed;' target="_blank"><i class="fa fa-save" aria-hidden="true"></i>Save</a></li>
  <li><a id='idshare' href="" data-toggle='modal' style='color:#4b88ed;' ><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a></li>
  -->
     <li> <a href="<?php echo base_url('Trip/booking_trip');?>" style="color:#fff;"><button class='btn btn-primary'> <i class="fa fa-cab" aria-hidden="true"></i> New Booking</button></a></li>
	 <li> <a href="<?php echo base_url('Reports/Booking');?>" style="color:#4b88ed;"><button><i class="fa fa-print" aria-hidden="true"></i> Print </button> </a></li>
  </ol>
  
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->   
    
  <section class="content"> 
  <!-- <div style="color:#4b88ed;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , And click on the button (Print,Share,Edit,View,Delete)  </div> -->
    <div style="padding:2px 0px 2px 0px;">
		 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
		 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div>
		 <div id='mserr'><?php echo $this->session->flashdata('message1'); ?></div>
		 </center>
		 </div>
	</div>
	
				
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->

<div class="portlet-body">
<div style="background-color:#fff;padding:15px; ">
              
		<div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">

			<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->

		<!-- search options -------------------------------------------->

	       <div class='row' style='margin:0px;background-color:#e4e4e4;padding:5px;'>

			<form role="form" method="POST" action="<?php echo base_url('Trip/booking_list/1')?>" enctype="multipart/form-data">
		
			<div class='col-md-2 ' style='padding-top:5px;'>	<label class='control-label'>Select Date [from-to] : </label>	</div>
			<div class='col-md-2 ' style='padding-top:2px;'><input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-2 ' style='padding-top:2px;'><input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			<div class='col-md-2 ' style='padding-top:2px;'>
				<select class="form-control" id="cltype" name="cltype">
					<option value="">--Client Type---</option>
					<option value="1">INDIVIDUAL</option>
					<option value="2">CORPORATE</option>
					<option value="3">AGENTS</option>
				</select>
			</div>
			
			<div class='col-md-1 ' style='margin-top:2px;'><input type="submit" class="form-control btn btn-success" style="text-align:center;"  value="Get" name='btnget' id="btnget"> </div>
			<div class="col-md-3" style="margin-top:2px;border-left:2px solid red;">
				<input type="submit" class="btn btn-warning" style="text-align:center;padding-top:4px;padding-bottom:4px;border-radius:0px;"  value="Cancelled List" name='btncancel' id="btncancel">
				<input type="submit" class="btn btn-primary" style="text-align:center;padding-top:4px;padding-bottom:4px;border-radius:0px;"  value="Closed List" name='btnclose' id="btnclose">
			</div>
		</form>

			</div>
	
	<!-- search options -------------------------------------------->
		
			
		 <div class="row" style='margin-top:10px;'>
                   <div class="col-md-12">
				   <label id="btitle" style='font-weight:bold;margin-bottom:20px;'><font color=#19a895><u>Trip Booking List</u></font></label>
				   </div>
				   </div>
		
		
            <div class="row" id='booklist'>
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
				   <div class="table-toolbar">
					<div class="row">
					<div class="col-md-12">
                  
                   </div>
                   </div>
                   </div>
             
            <table class="table table-striped table-hover table-bordered" id="example">
              <thead>
                <tr>
				 <th style='text-align:left;width:8%;'>Action</th>
                 <th>ID</th>
                 <th>Client/Type</th> 
                 <th width='15%'>Room/Guest</th>
				 <th width='20%'>Vehicle/Location</th>
				 <th>Trip/Description</th>
				 <th>Status</th>
                </tr>
                </thead>
				<tbody>
			<?php
			  $rgname="";
			  $loca="";
			if(isset($booking))
			{
			foreach ($booking as $r)
			{
				if($r->book_roomno!=="")
				{
					$rg=$r->book_roomno;
				}
			
				if($r->book_clienttype=="1"){ 
				$ct="INDIVIDUAL"; 
				$cna =$r->book_guestname;
				}
				else if($r->book_clienttype=="2"){
				$ct="CORPORATE";
				$cna=$r->customer_name;
				}
				else if($r->book_clienttype=="3"){
					$ct="AGENT";
					$cna=$r->customer_name;
				}
					
				if($r->book_tripoption=="ST"){ $trop="SHORT TRIP"; }
				else if($r->book_tripoption=="LT"){ $trop="OUT STATION"; }
				else if($r->book_tripoption=="AD"){ $trop="AIRPORT DROP"; }
				else if($r->book_tripoption=="AP"){ $trop="AIRPORT PICKUP"; }
				else if($r->book_tripoption=="RD"){ $trop="RAILWAY DROP"; }
				else if($r->book_tripoption=="RP"){ $trop="RAILWAY PICKUP"; }
				else if($r->book_tripoption=="FP"){ $trop="FIXED PACKAGE"; }
				else if($r->book_tripoption=="CM"){ $trop="COMPLIMENTARY"; }
								
				$edit=anchor('Trip/edit_booking_trip/'.$r->book_id,'<button class="btn btn-info btn-xs">&nbsp;&nbsp;&nbsp;Edit&nbsp;&nbsp;&nbsp;&nbsp;</button>');
				$del=anchor('Trip/delete_booking_trip/'.$r->book_id,'<button class="btn btn-danger btn-xs">&nbsp;Delete&nbsp;</button>', array('id' =>'del_conf'));
				//$close=anchor('Trip/close_booking_trip/'.$r->book_id,'<button class="btn btn-success btn-xs">&nbsp;&nbsp;Close&nbsp;&nbsp;</button>', array('id' =>'del_conf1'));
				$close='<a href="" id="'.$r->book_id.'" data-toggle="modal" data-target="#myModal_tripno" class="bclose"><button class="btn btn-success btn-xs">&nbsp;&nbsp;Close&nbsp;&nbsp;</button></a>';
				$cancel='<a href="" id="'.$r->book_id.'" data-toggle="modal"  data-target="#myModal2" class="cancel"><button class="btn btn-warning btn-xs">&nbsp;&nbsp;Cancel&nbsp;&nbsp; </button></a>';
				
				if($r->book_status=="1"){
					$act=$edit.$close.$cancel.$del;
					$stat="<font color=green><b>New</b></font>"; 
					}
					
				else if($r->book_status=="2"){
					$act=$del;
					$stat="<font color=red><b>Cancelled</b></font>"; 
					}
				else if($r->book_status=="3"){
					$act=$del;
					$stat="<font color=purple><b>Closed</b></font>";
					}
				
				$rgname="<b>Room No :</b> ".$rg."<br><b>Contact : </b>".$r->book_contactno."<Br><b>Reporting : </b> <br>&nbsp;&nbsp;&nbsp;Date : ".date_format(date_create($r->book_tripdate),'d-m-Y')."<br>&nbsp;&nbsp;&nbsp;Time : ".$r->book_reporttime;
			    $loca="<b>From : </b>".$r->book_location."<br><b>To : </b>".$r->book_tripto."<br><b>Vehicle Type : </b>".$r->book_vcategory."<br><b>Vehicle : </b>".$r->veh_type_name."<br><b>Reg.No : </b>".$r->book_vehicleregno."<br><b>Driver : </b>".$r->book_drivername."<br><b>Mobile : </b>".$r->book_drivermobile;
				?>
							
				
				<tr>
				 <td ><?=$act;?></td>
                 <td><?=$r->book_id;?></td>
                 <td width='25%'><b>Type: </b><?=$ct;?><br><b>Client: </b><?=$cna;?><br><br><b>Payment: </b><?=$r->book_payment;?></td> 
                 <td><?=$rgname;?></td>
				 <td width='20%'><?=$loca;?></td>
				 <td><b>Trip : </b><?=$trop?><br><b>Trip ID : </b><font color='#ff5722'><b><?=$r->book_tripno?></b></font><br><b>Description : </b><?=$r->book_description;?></td>
				 <td width='8%'><?=$stat;?></td>
                </tr>
				
				<?php
				}
			  }
				?>
				</tbody>
            </table>

        </div>
	
                       <!-- END BORDERED TABLE PORTLET-->
		</div>
		</div>
	
                <!-- END CONTENT BODY -->
	</div>
	</div>
            <!-- END QUICK SIDEBAR -->
</div>
  
                     <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                     </div> 

					 
					 <div class="modal fade draggable-modal" id="myModal_tripno" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >

                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                     </div> 

					 
					 
  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
<!-- /.content --> 
</section>
  <!-- /.content-wrapper -->
</div>
 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

setInterval(function(){ $('#idh5').html(''); }, 3000);
$("#idmsg").hide();
$("#msgerr").hide();
//sweet alert ----------------------------
	if($.trim($("#idh5").html())!="")
	{
		swal( $("#idh5").html(),"","success")
		$("#idh5").html("");
	}
	
	if($.trim($("#msgerr").html())!="")
	{
		swal($("#idh5").html(),"","error")
		$("#idh5").html("");
	}
// sweet alert box -------------------------- 

 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
 
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   }); 
   
  $('#example').dataTable({
    "destroy": true,
  "processing": true,
  "scrollX":true,
  });
   
  // var table = $('#example').DataTable();
 //     $('#example tbody').on( 'click', 'tr', function () {
 //        if ( $(this).hasClass('selected') ) {
 //            $(this).removeClass('selected');
 //        }
 //        else {
 //          table.$('tr.selected').removeClass('selected');
 //            $(this).addClass('selected');
 //        }
 //    } );
 
    $('#idsave').on('click',function(){
       //var id=$('#example').find('tr.selected').find('td').eq(2).text();
          var dt1=$('#datepicker1').val();
          var m=$('#lmonth').val();
       var pdf_link = $(this).attr('href')+'/'+m+'/'+dt1;
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
		window.open(pdf_link);
      return false; 
    
}); 
      $('#example tbody').on('click', '.cancel', function () {
        var Result=$("#myModal2 .modal-body");
        var id =  $(this).attr('id');
		 jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/cancel_booking",
        dataType: 'html',
        data: {bid: id},
        success: function(res)
		{
        Result.html(res);
        }
        });
        }); 		 
		
		
	 $('#example tbody').on('click', '.bclose', function () {
        var Result=$("#myModal_tripno .modal-body");
        var id =  $(this).attr('id');
		 jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/close_booking",
        dataType: 'html',
        data: {bid: id},
        success: function(res)
		{
        Result.html(res);
        }
        });
        }); 		 
		
 
  
    $('#view-pdf').on('click',function(){
     
		var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        $.createModal({
        title:'Booking List',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
	   return false; 

    });  
  
	
// -----------------------------------------------------------------------------------------------------------------------------------    
    
  $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this booking?');
   });

		
  $(document).on("click", "#del_conf1", function () {
           return confirm('Are you sure you want to Cancel this booking?');
  });


		
</script>



