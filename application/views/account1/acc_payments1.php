<?php
include('application/views/common/header.php');
?>
<style>
input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
</style>

<!--<link href="vendor/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="vendor/select2/dist/js/select2.min.js"></script> -->



        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#00adee'>Payments
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
     <label style="background-color:#cecece;width:100%;height:1px;"></label>   
     </section>
	
        <!-- Main content -->
    <section class="content">
	
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header">
          <h3 class="box-title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp; Create Payments</h3>
		  <label id='mes'><?php echo $this->session->flashdata('message');?></label>
        </div>
		
        <div class="box-body">
				
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item <?php if($tabstatus!='2') echo "active";?>" id='tb1' >
						<a class="nav-link tp1" data-toggle="tab" href="#vpayment"  role="tab"><i class="fa fa-bars" aria-hidden="true"></i> Payments</a>
					</li>
					<li class="nav-item <?php if($tabstatus=='2') echo "active";?>" id='tb2'>
						<a class="nav-link tp1 " data-toggle="tab" href="#vlist"  role="tab"><i class="fa fa-bars" aria-hidden="true"></i> Payment Details</a>
					</li>
					
                 </ul>
                <!-- Tab panes -->
			<div class="tab-content">
			<div class="tab-pane <?php if($tabstatus!='2') echo "active";?>" id="vpayment" role="tabpanel">
			
<form action="echo.php" class="repeater" enctype="multipart/form-data">
 <div data-repeater-list="group-a">
   <div data-repeater-item>
	<div class='row' style='padding:5px 15px 5px 15px;'>
		<div class='col-md-3'>
			<div class="input-group">
			<select  class="form-control" name='pledger' id='pledger'  required>
				  <option value="">---select----</option>
				<?php
					$lquery=$this->db->select("*")->from('acc_ledgers')->get()->result();
					foreach($lquery as $l)
					{
						echo "<option value='".$l->acc_ledgid."'>".$l->acc_ledgname."</option>";
					}
				  ?>
			</select>
			<span class="input-group-addon" style="padding:0px 5px 0px 5px; "><span class="glyphicon glyphicon-plus-sign" style='font-size:20px;'></span></span>
			</div>
			
		</div>
		
			<div class='col-md-5' style='padding-left:0px;'>
				<input type="number" class="form-control"  name="pamount" required>
			</div>
			<div class='col-md-2' style='padding-left:0px;'>
				<input type="number" class="form-control"  name="pamount" required>
			</div>		
			<div class='col-md-1' style='padding-left:0px;'>
				<input type="number" class="form-control"  name="pamount" required>
			</div>	
			<div class='col-md-1' style='padding-left:0px;'>
				<input type="number" class="form-control"  name="pamount" required>
			</div>
			
	</div>
	</div>
	</div>
	 <input data-repeater-create type="button" value="Add"/>
	</form>
	
		
	<!-- --------------------------------------------------------------------------------------------------------------------------- -->	
		
        	<div class='row' style='padding:15px;'>
			<div class='col-md-6'>
			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Voucher/save_payments')?>" enctype="multipart/form-data">
				  
				  <div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Voucher Type :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="pvtype" value='PAYMENTS' readonly>
								</div>
							</div>
						</div>
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Date :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="pdate" value='<?php echo date('d-m-Y');?>' readonly>
								</div>
							</div>
						</div>
								
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Account :</label>
								<div class="col-md-7">
									<select  class="form-control" name='paccount' required>
									  <option value="">---select----</option>
									  <option value="CASH">CASH</option>
									  <?php
										//$gquery=$this->db->select("*")->from('acc_groups')->limit('24')->get()->result();
										//foreach( $gquery as $g)
										//{
											//echo "<option value='".$g->acc_grpid."'>".$g->acc_grpname."</option>";
										//}
									  ?>
														  
									</select>
								</div>
							</div>
						</div>
						
						
						
						
						
						<head>
  						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Particulars :</label>
								<div class="col-md-7">
								<div class="input-group">
									
 
								<select  class="form-control" name='pledger' id='pledger'  required>
									  <option value="">---select----</option>
									<?php
										$lquery=$this->db->select("*")->from('acc_ledgers')->get()->result();
										foreach($lquery as $l)
										{
											echo "<option value='".$l->acc_ledgid."'>".$l->acc_ledgname."</option>";
										}
									  ?>
								</select>
								<span class="input-group-addon" style="padding:0px 5px 0px 5px; "><span class="glyphicon glyphicon-plus-sign" style='font-size:20px;'></span></span>
								</div>
							</div>
						</div>
						
						</div>
						
			
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Amount :</label>
								<div class="col-md-7">
									<input type="number" class="form-control"  name="pamount" required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Narration :</label>
								<div class="col-md-7">
									<textarea rows=3 class="form-control"  name="pnarrat" required></textarea>
							  </div>
							</div>
						</div>
						
						<hr>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label"></label>
								<div class="col-md-7">
									<input type="submit" class="btn btn-primary "  name="btnsubmit" value="Save Payments">
								</div>
							</div>
						</div>
						
			</form>
		
		
			</div>
			</div>
			</div> <!-- first tab end -->
			
			<div class="tab-pane <?php if($tabstatus=='2') echo "active";?> " id="vlist" role="tabpanel">
			
			<div class='row' style='margin:0px 5px 0px 5px;background-color:#e4e4e4;padding:5px;'>
			
			<form role="form" method="POST" action="<?php echo base_url('Voucher/payments/2')?>" enctype="multipart/form-data">
             			
			<div class='col-md-2' style='padding-top:5px;'>	<label class='control-label'>Select Date [from-to] : </label>	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-2'>	<input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			<div class='col-md-2' style='padding-top:2px;'><input type='submit' name='btndetails' class='btn btn-primary' value='Get Details'> </div>
			
			</form>
			</div>

			
			<div class='row' style='margin-left:10px;margin-top:15px;'>
			<label style='padding-bottom:3px; border-bottom:1px solid #e4e4e4;font-size:15px;'><b><?php echo $this->session->flashdata('ptitle');?></b></label>
			</div>
					
			<div class='row' style='padding:10px;'>
			<div class='col-md-12'>
			 <table class="table table-striped table-hover table-bordered" id="example" border=0 STYLE='font-size:14px;width:100%'>
			 
			 <thead>
                <tr>
				 <th  style='text-align:center !important;'>Edit</th>
                 <th>ID</th>
				 <th>Date</th>
                 <th>Account</th> 
                 <th>Particulers</th>
				 <th>Narration</th>
				 <th style='text-align:right !important;' >Amount</th>
				 
                </tr>
                </thead>
				<tbody>
					<?php
					$gtotp=0;
					if(isset($result))
					{
					foreach($result as $r)
					{
					?>
						<tr height='33px'>
						 <td align='center' class='dtable'><a href='#exampleModal' class='edit abtn abtn-edit' data-toggle="modal" data-target="#exampleModal"><span class="glyphicon glyphicon-edit" aria-hidden="true" ></span></a>
						  <a href='#' class='del abtn abtn-del'> <span class="glyphicon glyphicon-trash" aria-hidden="true" ></span></a>
						 
						 </td>
						 <td><?=$r->acc_pay_id?></td>
					     <td><?= date_format(date_create($r->acc_pay_date),'d-m-Y');?></td>
						 <td><?=$r->acc_pay_account;?></td>
						 <td><?=$r->acc_ledgname;?></td> 
						  <td><?=$r->acc_pay_narration;?></td> 
						 <td align='right' style='font-weight:600;'><?=number_format($r->acc_pay_amount,'2','.','');?></td>
						</tr>
					<?php
					$gtotp+= $r->acc_pay_amount;
					}
					
					}
					?>	
				</tbody>
			 
				</table>
				<div class='row' style='margin:10px 0px;background-color:#e2e2e2;padding-top:3px;'>
				<div class='col-md-9' style='text-align:right;'>
				<label class='control-label' style='font-size:16px;font-weight:600;'> Total Payed Amount : </label>
				</div>
				<div class='col-md-3' style='text-align:right;'>
				<label class='control-label' style='font-size:18px;font-weight:600;'> &#8377;&nbsp; <?=number_format($gtotp,"2",".","");?></label>
				</div>
				</div>
				</div>
								
			</div>
					
			</div> <!-- second tab end --->
			</div> <!-- tab content end -->
           
        </div>
	</div>
	</div>
	
</section>
    <!-- content wrapper -->
	
	 <!---EDIT  modal dialog start ---->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style='top: 50px;border:1px solid #b7b7b7;'>
      <div class="modal-header" style='background-color:#cacaca;'>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true">&times;</span>
        </button>
		<h4 class="modal-title" id="exampleModalLabel" >Edit Payments</h4>
      </div>
	  
	  <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Voucher/edit_payment')?>" enctype="multipart/form-data">
	  
		<div class="modal-body">
					<input type="hidden" class="form-control"  name="mpid"  id="mpid" required>
				<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Account :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="mpaccount"  id="mpaccount" value='CASH' readonly>
								</div>
							</div>
						</div>
				
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Particulers :</label>
								<div class="col-md-7">
									<select class="form-control" name='mpledger' id='mpledger' required>
									  <option value="">---select----</option>
									<?php
										$lquery=$this->db->select("*")->from('acc_ledgers')->get()->result();
										foreach($lquery as $l)
										{
											echo "<option value='".$l->acc_ledgid."'>".$l->acc_ledgname."</option>";
										}
									  ?>
								</select>
								
								</div>
							</div>
						</div>
				
						
			
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Amount :</label>
								<div class="col-md-7">
									<input type="number" class="form-control"  name="mpamount" id="mpamount" required>								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Narration :</label>
								<div class="col-md-7">
									<textarea rows=3 class="form-control"  name="mpnarrat" id='mpnarrat' required></textarea>
							  </div>
							</div>
						</div>
						
										
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Save changes</button>
		  </div>
	  </form>
    </div>
  </div>
</div>

<!-- modal dialog end ----->

		
</div>
     <?php
  include('application/views/common/footer.php');
  ?>

</body>
</html>
  <script type="text/javascript">
  
   $(document).ready(function() {
	   $("#pledger").select2(); 
	   });

  
$('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    autoclose:true,
	
});
   $('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});
  
  
  $("#example").dataTable({
	  "ordering":false,
  });
  
  $("#mes").hide();
  
  
  var msg=$("#mes").html().split("#");
  
  if($("#mes").html()!="")
  {
	  var msg=$("#mes").html().split("#");
	if(msg[0]=='1')
	  swal("Saved.!",msg[1],"success");
	else if(msg[0]=='2')
	  swal("Updated.!",msg[1],"success");
    else if(msg[0]=='3')
	  swal("Removed.!",msg[1],"success");
	else if(msg[0]=='4')
	  swal("Try Again.",msg[1],"error");
     $("#mes").html("");
  }
  
$('#example tbody').on('click','.edit', function()
{
  //var rt1=$(this).parent('tr')
  var pid=$(this).closest('tr').find('td').eq(1).text();
  
  jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Voucher/get_edit_payments",
        dataType: 'html',
        data: {payid:pid},
        success: function(res) 
		{
			var ldt=res.split(',');
			  $("#mpid").val(ldt[0]);
			  $("#mpledger").val(ldt[1]);
			  $("#mpamount").val(ldt[2]);
			  $("#mpnarrat").val(ldt[3]);
        }
		});
 });	
  
$('#example tbody').on('click','.del', function()
{
  var pid=$(this).closest('tr').find('td').eq(1).text();
  
		  swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this payments.!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "Cancel",
		  closeOnConfirm: true,
		  closeOnCancel: true
		},
		function(isConfirm) 
		{
		  if (isConfirm)
 		  {
			  jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>" + "Voucher/delete_payment",
					dataType: 'html',
					data: {payid:pid},
					success:function(res)
					{
						window.location.href="<?php echo base_url('Voucher/payments/3');?>";
					}
					}); 
			
		  }
		});
 });	
    
  </script>
  
  
  
   <script>
    $(document).ready(function () {
        'use strict';

        $('.repeater').repeater({
            defaultValues: {
                'textarea-input': 'foo',
                'text-input': 'bar',
                'select-input': 'B',
                'checkbox-input': ['A', 'B'],
                'radio-input': 'B'
            },
            show: function () {
                $(this).slideDown();
            },
            hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            },
            ready: function (setIndexes) {

            }
        });

        window.outerRepeater = $('.outer-repeater').repeater({
            isFirstItemUndeletable: true,
            defaultValues: { 'text-input': 'outer-default' },
            show: function () {
                console.log('outer show');
                $(this).slideDown();
            },
            hide: function (deleteElement) {
                console.log('outer delete');
                $(this).slideUp(deleteElement);
            },
            repeaters: [{
                isFirstItemUndeletable: true,
                selector: '.inner-repeater',
                defaultValues: { 'inner-text-input': 'inner-default' },
                show: function () {
                    console.log('inner show');
                    $(this).slideDown();
                },
                hide: function (deleteElement) {
                    console.log('inner delete');
                    $(this).slideUp(deleteElement);
                }
            }]
        });
    });
    </script>
</body>
</html>

  
  