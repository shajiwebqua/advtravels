<?php
defined("BASEPATH") or exit('No direct script access allowed');

class Model_menu extends CI_model{

	public function __construct(){
		parent::__construct();
	}

	public function get_distinct_menu_cat_id($where){
		$this->db->distinct();
		$this->db->select('menu_cat_id');
		$this->db->order_by("menu_cat_id");
	    $this->db->from('menu_user');
	    $this->db->where($where);
	    $query = $this->db->get();
	    return $query->result();
	}

	public function get_menu_link($where){
		$this->db->select("menu_id,menu_name,menu_link");
	    $this->db->from("menus");
	    $this->db->where($where);
	    $query = $this->db->get();
	    return $query->result();	
	}

	public function get_user_menu_options($where){
		$this->db->select("menu_options");
	    $this->db->from("menu_user");
	    $this->db->where($where);
	    $query = $this->db->get();
	    return $query->result();		
	}

	public function view_menucategory(){
		$this->db->select('*');
	    $this->db->from('menu_user');
	    $this->db->order_by("menu_cat_id");
	    $query = $this->db->get();
	    return $query->result();
	}
}