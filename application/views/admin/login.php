<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Advance World Group</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css');?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/square/blue.css');?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/advlogin.css');?>">
  <style>
  .full-bg {
	background:url("<?php echo base_url('assets/images/home.png');?>");
	background-size: cover;
}

.full-bg1{
	background-color:#fff;
	border:1px solid #fff;
	padding:20px;
	-webkit-box-shadow: 2px 2px 20px 0px rgba(0,0,0,0.25);
	-moz-box-shadow: 2px 2px 20px 0px rgba(0,0,0,0.25);
	box-shadow: 2px 2px 20px 0px rgba(0,0,0,0.25);
}

  </style>
</head>
<!--<body class="hold-transition login-page"> -->
<body class="hold-transition">
<div class='row' style='height:35 !importatnt;background-color:#0093dd;'>
<center><label style='padding:5px;color:#fff;font-weight:600;'>* ADVANCE WORLD HOLIDAYS *</label></center>
</div>
<center>
<div class="full-bg1" style='width:360px;margin-top:75px;margin-bottom:50px;'>
  <div class="">
  	<center><img src="<?php echo base_url('upload/images/logo_dark.png');?>" height='75px' width='150px'></center><br>
    <center><a href="#" style='font-size:18px;'><b>Login</b></a></center>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" style="background: rgba(238, 238, 238, 0.31);">
    <p class="login-box-msg">Sign in to start your session</p>
	
	<label style='margin-top:3px;background-color:#e4e4e4;width:100%;height:1px;'></label>
	
    <form action="<?php echo base_url('Login/index');?>" method="post">
      <div class="form-group has-feedback" style='margin-top:20px;'>
	     <input type="text" name='email' class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback" style='margin-top:10px;'>
        <input type="password" name='password' class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
         <!-- /.col -->
               <center> <button type="submit" id='signin'  class="log-btn btn btn-primary btn-block btn-flat" style="width:155px;margin-top: 20px;">Sign In</button></center>
         <!-- /.col -->
      </div>
  </form>
	
	<div class="row" style='margin-top:10px;'>
	<hr style='maring:0px;'>
	</div>
	<div class="row" style='margin-top:30px;'>
	    <div class="col-xs-12">
		
        </div>
	</div>
	
  </div>
  <!-- /.login-box-body -->
</div>
</center>
<div class='row' style='background-color:#0093dd;position:fixed;bottom:0px;width:100%;margin:0px;'>
<center><label style='padding:5px;color:#fff;font-size:13px;'><i class="fa fa-copyright" aria-hidden="true"></i> Copyright 2016 - Advance world holidays - All Rights Reserved. | <a href="http://www.advanceworldholidays.com/" style='color:#e6ce97;'>www.advanceworldholidays.com</a></label></center>
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/plugins/iCheck/icheck.min.js');?>"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  
</script>
</body>
</html>
