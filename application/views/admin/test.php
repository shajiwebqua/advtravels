
<?php
$invno1=$this->db->select("MAX(invo_invoiceid) as invno")->from('invoices')->get()->row();
if(!empty($invno1->invno))
{
	$inv_no=$invno1->invno;
}
else
{
	$inv_no=1001;
}
$inv_no++;
$ivno1="INV-".$inv_no;
?>

	<div class='row dt-row'  style='margin-top:10px;'>
		<div class='col-md-12'>
		 <input type='text'  name='trip_ids' id='trip_ids' value='<?php echo $trids;?>'/>	
			
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-4 control-label p-col' style='padding-top:5px;'>Invoice No : </label>
		  <div class='col-md-5'>
		  <input type='text' class='form-control' name='invno'  value='<?=$ivno1;?>'/>
		  </div>
		  </div>
		  </div>
						  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-4 control-label p-col' style='padding-top:5px;'>Invoice Date : </label>
		  <div class='col-md-5'>
		  <input type='text' class='form-control' id='dtpicker1' name='invdate'  value='<?php echo date('d-m-Y');?>' required/>
		  </div>
		  </div>
		  </div>
		  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-4 control-label p-col' style='padding-top:5px;'>Delivery Note : </label>
		  <div class='col-md-8'>
		  <textarea rows=2 class='form-control' name='delinote' id='delinote' ></textarea>
		  </div>
		  </div>
		  </div>
		  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-4 control-label p-col' style='padding-top:5px;'>Mode/Terms of payment : </label>
		  <div class='col-md-4'>
		  
		  <select name='terms' id='terms' class='form-control' required>
			  <option value=''>-----</option>
			  <option value='1'>1 day</option>
			  <option value='5'>5 days</option>
			  <option value='10'>10 days</option>
			  <option value='15' selected>15 days</option>
			  <option value='20'>20 days</option>
			  <option value='25'>25 days</option>
			  <option value='30'>30 days</option>
		  </select>
				  
		  </div>
		  </div>
		  </div>
		  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-4 control-label p-col' style='padding-top:5px;'>Supplier's Ref : </label>
		  <div class='col-md-8'>
		   <textarea rows=2 class='form-control' name='suppref' id='suppref' ></textarea>
		  </div>
		  </div>
		  </div>
		  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-4 control-label p-col' style='padding-top:5px;'>Dated : </label>
		  <div class='col-md-5'>
		  <input type='text' class='form-control' id='dtpicker2' name='supdated'  value=''/>
		  </div>
		  </div>
		  </div>
		  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-4 control-label p-col' style='padding-top:5px;'>Despatch document no: </label>
		  <div class='col-md-5'>
		  <input type='text' class='form-control' name='desdno' id='desdno' value=''/>
		  </div>
		  </div>
		  </div>
		  
		  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-4 control-label p-col' style='padding-top:5px;'>Delivery Note Date : </label>
		  <div class='col-md-5'>
		  <input type='text' class='form-control' name='delindate'  id='dtpicker3' value=''/>
		  </div>
		  </div>
		  </div>
		  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-4 control-label p-col' style='padding-top:5px;'>Despatch Through : </label>
		  <div class='col-md-8'>
		  <input type='text' class='form-control' name='despatch'  id='despatch' value=''/>
		  </div>
		  </div>
		  </div>
		  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-4 control-label p-col' style='padding-top:5px;'>Destination : </label>
		  <div class='col-md-8'>
		  <input type='text' class='form-control' name='destin' id='destin'  value=''/>
		  </div>
		  </div>
		  </div>
		  
		  <div class='row' style='padding-right:5px;'>  <hr style='margin:5px 0px;'>  </div>
		  
	 <div class='row' style='margin-top:10px;'>
	  <div class='col-md-12' style='text-align:center;'>
		<input type='submit' class='p-btn btn btn-primary'  value='Create Invoice'/>
		<button type='button' class='close-btn p-btn btn btn-default'>Close</button>
		</div>
	 </div>
				  
		</div>
			
	  </div>



<script>
  //for invoice box ---------------
$('#dtpicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
 
 $('#dtpicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   }); 
 $('#dtpicker3').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });     
 //-------------------------------
 
$(".close-btn").click(function(){
    $(".inv-div").removeClass('open-div');
});
</script>