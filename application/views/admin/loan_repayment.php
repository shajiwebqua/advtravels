<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');
?>
<style>
.pad{
	padding:0px 10px 0px 10px;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1 class='heading'>Loan Re-Payment
		</h1>
		<ol class="breadcrumb">
			<!-- <li><a href="#" data-target="#myModal1" data-toggle='modal'><button class='btn btn-primary' name='setpayment'>Set Re-payment</button></a></li> -->
		</ol>
	</section>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
	<!-- Content Header end -->
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				<!-- <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;"> -->
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 <!--</div>-->
			</div>

		<div class="row">
			<div class="col-md-12">
	<div style="background-color:#fff;border-radius:2px;padding:2px 15px 5px 15px;">
	  <div class="portlet-body form" style="margin:0px;">
	  		<div class="box-body">

			<div class="container-fluid">
			<div class="row" >
			 <form  role='form' method='post' action='<?php echo site_url('Loan/add_repayment');?>' enctype='multipart/form-data'>
  				<div class="col-md-6" style='padding:3px;'>

				<div class='row' style="margin-right:3px;background-color:#f4f4f4;padding:10px;border-radius:10px;">
				
						  <?php
							//$query=$this->db->select('*')->from('account_master')->where('ltl_refrence',$lrno)->get()->row();
						  ?>
						  
							<div class="form-group">
								<label class="control-label col-sm-12" for="bank_name"><b>Details :</b></label>
								<label style="background-color:#cecece;width:100%;height:1px;padding:0px;"></label>
							</div>

							<div class='form-group pad' >
						 <div class='row'>
									<label class="control-label col-sm-3" for="ltf_ref_no">LTL Ref No</label>
						
										<div class='col-md-9'>
									  <section class="form-control " style='padding:0px;'>
										<select class="form-controll_prop" id="ltf_ref_no" placeholder="Choose ltl ref. no" name="ltf_ref_no">
										  <option value="">----------</option>
										   <?php
											$cquery=$this->db->select("loan_ltlrefno")->from('loan_vehicle_master')->where('loan_vehiclestatus','1')->get()->result();
											foreach( $cquery as $r)
											{
												echo "<option value='".$r->loan_ltlrefno."'>".$r->loan_ltlrefno."</option>";
											}
										  ?>
		  
										</select>
						
									</section>
								
								  </div>
						</div>
						 </div>
							
							
							<div class="form-group pad">
							<div class='row'>
								<label class="control-label col-sm-3" for="bank_name">Vehicle Name</label>
								<div class="col-sm-9">
									<input type="text" name="cname" id='vname' class="form-control" >
								</div>
							</div>
							</div>
							
							<div class="form-group pad">
							<div class='row'>
								<label class="control-label col-sm-3" >Registration No</label>
								<div class="col-sm-9">
									<input  name="regno" id='regno' class="form-control" >
								</div>
							</div>
							</div>
							</div>
					</div>
			
					
		<div class='col-md-6' style='padding:3px;'>
				
				<div class='row' style="margin-left:3px;background-color:#f4f4f4;padding:10px;border-radius:10px;">
				
							<div class="form-group">
								<label class="control-label col-sm-12" for="bank_name"><b>Loan Amount :</b></label>
								<label style="background-color:#cecece;width:100%;height:1px;padding:0px;"></label>
							</div>
							
							<div class="form-group pad">
							<div class='row'>
								<label class="control-label col-sm-3"  style='margin-top:3px;'>Loan Amount</label>
								<div class="col-sm-9">
									<input type="text" name="bankname" class="form-control" id="loanamt" >
								</div>
							</div>
							</div>
							
							<div class="form-group  pad">
							<div class='row'>
								<label class="control-label col-sm-3"  style='margin-top:3px;'>Interest</label>
								<div class="col-sm-9"> 
									<input name="interest" class="form-control" id="interest" >
								</div>
							</div>
							</div>
							
							<div class="form-group pad">
							<div class='row'>
								<label class="control-label col-sm-3" for="loan_amt" style='margin-top:3px;'>Loan Total</label>
								<div class="col-sm-4">
									<input type="text" name="loantotal" class="form-control" style='margin-top:1px;' id="loantotal" >
								</div>
								<label class="control-label col-sm-2" for="loan_amt" style='margin-top:3px;text-align:right;'>Period</label>
								<div class="col-sm-3">
									<input type="text" name="period" class="form-control" style='margin-top:1px;' id="period">
								</div>
								
							</div>
							</div>

			</div> 
		</form>							
  						
	</div>
	</div>
						
						<!-- display schedule ------------------------>
						
						<div class="row" style='margin-top:20px;'>
						<label class='control-label col-md-10' style='font-size:18px;'><b>Re-payment Schedule</b></label>
						<div class='col-md-2'>
						<!--<a  id='setpayment' href="#" data-toggle='modal'><button class='btn btn-primary' name='setpayment' >Set Re-payment</button></a>-->
						</div>
						</div>
						<div class="row" >
						<hr  style='margin-top:0px; padding:0px;'>
						</div>
						
						<div class="row">
						<table class="table table-hover table-bordered" id="example">
	                            <thead>
	       								<tr style="color:#5068f8;">
	       								 <th>SL</th>
										 <th>ID</th>
	       								 <th>Date</th>
	       								 <th>Loan</th>
	       								 <th>Installment</th>
	       								 <th>Interest</th>
	       								 <th>Principle</th>
	       								 <th>Check No</th>
	       								 <th>Voucher No</th>
	       								 <th>Repayment</th>
	       								 </tr>
	       								</thead>
	       						<tbody id="loan_shedule">			
	       						</tbody>		
	                     </table>
						</div>

	  				</div>
	  			<!--	<center>
	  					<button class="btn btn-success" id="save_details" disabled="disabled">Save Details</button>
	  				</center> -->
	  			</div>

	  </div>	

</div>
</div>
</div>
</section>

</div>


<div class="modal fade draggable-modal" id="repayment_box" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Re-Payment</h4>
                                </div>
                                <div class="modal-body" >
								                               
                                	<!--The id of loan shedule-->
                                	<input type="hidden" name="shedule_id" id="shedule_id">
                                	<!--*****************************************************-->
                                	<div class="form-group  pad">
                                	<div class='row'>
                                		<label class="control-label col-sm-3"  style='margin-top:3px;'>Date : </label>
                                		<div class="col-sm-9"> 
                                			<input name="date" class="form-control" id="date" onfocus="$(this).blur()">
                                		</div>
                                	</div>
                                	</div>
                                	<div class="form-group  pad">
                                	<div class='row'>
                                		<label class="control-label col-sm-3"  style='margin-top:3px;'>Payment Mode</label>
                                		<div class="col-sm-9"> 
                                			<select class="form-control" name="paymentmode" id='paymentmode'>
                                				<option>--Select Payment Mode--</option>
                                				<option value="Transfer">Transfer</option>
                                				<option  value="Cheque">Cheque</option>
                                				<option  value="Cash">Cash</option>
                                			</select>
                                		</div>
                                	</div>
                                	</div>
                                	<div class="form-group  pad">
                                	<div class='row'>
                                		<label class="control-label col-sm-3"  style='margin-top:3px;'>Cheque No</label>
                                		<div class="col-sm-9"> 
                                			<input name="cheque" class="form-control" id="cheque" required>
                                		</div>
                                	</div>
                                	</div>
                                	<div class="form-group  pad">
                                	<div class='row'>
                                		<label class="control-label col-sm-3"  style='margin-top:3px;'>Voucher No</label>
                                		<div class="col-sm-9"> 
                                			<input name="voucher" class="form-control" id="voucher" >
                                		</div>
                                	</div>
                                	</div>
                                	<div class='row'>
                                		<div class="col-sm-6 col-sm-offset-3"> 
                                			<button type="button" class="btn btn-success btn-md" id="repayment_add" data-dismiss="modal" >Set Payment</button>
											<button type="button" class="btn btn-default btn-md" data-dismiss="modal" >Close</button>
                                		</div>
                                	</div>
                                	</div>
                                	
                            	</div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
     



<?php include('application/views/common/footer.php');?>
<script type="text/javascript">

   $("#msg").hide();
//sweet alert ----------------------------
	if($("#msg").html()!="")
	{
		swal( $("#msg").html(),"","success");
		$("#msg").html("");
	}
	
$('#vdate').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
   }); 

  
$('#chequeno').prop('readonly',true);
$('#pmode').change(function()
{
	var pmod=$(this).val();
	if(pmod=='CHEQUE')
	{
		$('#chequeno').prop('readonly',false);
	}		
	else
	{
		$('#chequeno').prop('readonly',true);
	}
});


$('#ltf_ref_no').change(function(){
	
	var ltlref=$(this).val();
		
		jQuery.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>" + "Loan1/get_details",
		dataType: 'html',
		data: {lrno: ltlref},
		success: function(res)
		{
			var obj = jQuery.parseJSON(res);
			$("#vname").val(obj.vname);
			$("#regno").val(obj.vregno);
			$("#loanamt").val(obj.lamount);
			$("#interest").val(obj.linterest);
			$("#loantotal").val(obj.ltotoal);
			$("#period").val(obj.period);
		}
		
		});
		if(ltlref!="")
		{
			get_schedule(ltlref);
		}
});




$('#repayment_add').click(function(){
   	    var ltlref=$("#ltf_ref_no").val();
		
		var pmode=$("#paymentmode").val();
		var chno=$("#cheque").val();
		var vno=$("#voucher").val();
		var sid=$("#shedule_id").val();
		
		if(pmode=='Transfer')
		{
			chno="Transfer";
		}
				
		jQuery.ajax({
		type: "POST",
		url: "<?php echo base_url("Loan1/update_shedule") ?>",
		dataType: 'html',
		data: {sid:sid,vno:vno,chno:chno},
		success: function(res)
		{
		}
		});
		if(ltlref!="")
		{
			get_schedule(ltlref);
		}
});



function get_schedule(lrno)
{
		//var ltlref=$('#ltlrefno').val();
		
		$('#example').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
        "initComplete": function(settings, json) {
        	    
        },
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Loan1/repayment_ajax/"+lrno,// json datasource
               },
			   
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"6%","targets":1},
		
		],
			   
       "columns": [

			{ "data": "lslno"},
			{ "data": "lsid"},
			{ "data": "ldate"},
			{ "data": "lamount"},
			{ "data": "linstall"},
            { "data": "linter"},
            { "data": "lprin" },
            { "data": "lcheque" },
            { "data": "lvoucher" },
            { "data": "repayment" },
       ]
    });
}

function setpayment(obj)
  {
        id = $(obj).parent().parent();
        $("#shedule_id").val(id.find("span.id").text());
        $("#date").val(id.find("span.date").text());
  }


  $("#paymentmode").change(function(){
  	if($(this).val() != "Cheque"){
  		$("#cheque").prop("disabled", true);
  		$("#voucher").focus();
  	}
  	else{
  		$("#cheque").prop("disabled", false);
  		$("#cheque").focus();
  	}
  });

/*var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });	
*/
 
 
 /* $('#setpayment').click(function ()
  {
        var Result=$("#myModal1 .modal-body");
		var ltl=$('#ltf_ref_no').val();
     	// var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(ltl=="")
		 {
			 alert ("please Select LTL Ref No..");
		 }
		 else
		 {			
			jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "Loan/set_repayment",
			dataType: 'html',
			data: {ltlref: ltl},
			success: function(res)
			{
			Result.html(res);
						}
			});
		 }
  }); */
    
 
	 $('.datepicker').click(function(){
	 	$(this).datepicker({
		format: 'dd-mm-yyyy',
		
	    autoclose: true,
	    todayHighlight: true
	});
	   });  
	   

///dropdown list  customer name
   $('#ltf_ref_no').selectize({
     create: false
     , sortField: {
     //field: 'text',
     direction: 'asc'
   }
   , dropdownParent: 'body'
   });
	   
	   
</script>