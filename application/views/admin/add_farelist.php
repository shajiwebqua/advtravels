<?php
 include('application/views/common/header.php');?>
 <style>
 .nav-tabs {
    margin: 0;
    padding: 0;
    border: 0;    
}
.nav-tabs > li > a {
    background: #f2f2f2;
    border-radius: 0;
    //box-shadow: inset 0 -8px 7px -9px rgba(0,0,0,.4),-2px -2px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li.active > a:hover {
    background: #fff;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li > a:hover {
    background: #e4e4e4;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}
/* Tab Content */
.tab-pane {
    background: #fff;
    //box-shadow: 0 0 4px rgba(0,0,0,.4);
    border-radius: 0;
    border:1px solid #e4e4e4;
    text-align: left;
    padding: 10px;
}

.cli
{
padding:5px 0px 5px 0px;
margin-top:3px;

}

.selclass > div:hover
{
background-color:#f4f4f4;
}

</style>
 
 
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

input[type=number] {
    -moz-appearance:textfield;
}

.ttext
{
	background-color:#fff;
	font-weight:bold;
	color:Green;
}

.search-btn
{
	padding-top:3px;
	z-index:1000;
	right:17px;
	position:absolute;
}

.select_btn1 ,.select_btn2{
  
    position: absolute;
    top: 3px;
    z-index: 999;
   
}
.select_btn1{
	 right: 19px;
}
.select_btn2 {
    right: 19px;
}

.lblmap
{
	background-color:#e6e6e6;
	padding-top:12px;
	padding-bottom:12px;
	height:45px;
}

/* radio button ----------------*/

#m-radio {
	margin: 0 0px;
}

#m-radio label {
  width: 200px;
  border-radius: 3px;
  border: 1px solid #D1D3D4
  	color:#888 ;
}

/* hide input */
#m-radio input.radio:empty {
	margin-left: -999px;
}

/* style label */
#m-radio input.radio:empty ~ label {
	position: relative;
	float: left;
	line-height: 2.5em;
	text-indent: 3.25em;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

#m-radio input.radio:empty ~ label:before {
	position: absolute;
	display: block;
	top: 0;
	bottom: 0;
	left: 0;
	content: '';
	width: 2.5em;
	background: #D1D3D4;
	border-radius: 3px 0 0 3px;

}

/* toggle hover */
#m-radio input.radio:hover:not(:checked) ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #C2C2C2;

}

#m-radio input.radio:hover:not(:checked) ~ label {
	color:#777 ;
}

/* toggle on */
#m-radio input.radio:checked ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #9CE2AE;
	background-color: #4DCB6D;
}

#m-radio input.radio:checked ~ label {
	/*color: #888;*/
}

/* radio focus */
.form-horizontal .checkbox, .form-horizontal .radio {
    min-height: 0px;
}
.no-pad {
	padding-left: 0px;
	padding-right: 0px;
}


/* fare list -----------*/
table#fare tbody tr:hover {
    background-color: #afc0db;
    cursor: pointer;
	color:#fff;
}

/* fixedfare list -----------*/
table#fixedfare tbody tr:hover {
    background-color: #afc0db;
    cursor: pointer;
	color:#fff;
}


/* clubfare list -----------*/
table#clubfare tbody tr:hover {
    background-color: #afc0db;
    cursor: pointer;
	color:#fff;
}


</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Fare Details</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px'>
   </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

<div class="page-content-wrapper">
   <!-- BEGIN CONTENT BODY -->
   <div class="page-content">
   
   <div style="padding:0px 15px 0px 15px;">
		 <div id="idmsg"  style="background-color:#fff;height:25px;margin-bottom:5px;">
		 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div>
		 <div id='msgerr'><?php echo $this->session->flashdata('message1'); ?></div>
		 <div id='msgdel'><?php echo $this->session->flashdata('message2'); ?></div>
		 </center></div>
	</div>
   
  <div style="background-color:#fff;padding:5px; ">
  
  <div style='padding-left:3px;'> 
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item <?php if($ftab==1) echo "active";?> " >
						<a class="nav-link" data-toggle="tab" href="#fare"  role="tab"><i class="fa fa-user"></i> Add Fare</a>
					</li>
					<li class="nav-item <?php if($ftab==2) echo "active";?>" >
						<a class="nav-link " data-toggle="tab" href="#farelst"  role="tab"> <i class="fa fa-cab"></i> Fare List</a>
					</li>
					
                 </ul>
                <!-- Tab panes -->
<div class="tab-content">
<div class="tab-pane <?php if($ftab==1) echo "active";?> " id="fare" role="tabpanel">  <!-- class list -->
          <!-- datatable list here ------------>
<div style='padding:10px 5px;width:100%'> 
	  
 <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/add_fare_list')?>" enctype="multipart/form-data">
	
	<div class='row' style='padding-left:20px;'>
	<div class='col-md-3'>
	
		<div class='row' >
		 <label> Fare Category: </label>
		 <hr style='margin-top:3px;'>
		</div>
		
	
		<div class='row col-md-offset-1'>
		 <div id="m-radio" class="col-md-11 no-pad">
		  <input type="radio" name="fcat" id="radio1" class="radio" value='1' <?php if($facat=='1') echo 'checked';?>/>
		  <label for="radio1" style='float:left;'><b><FONT color='green'>NORMAL</font> TRIP FARE</b></label>
		  </div>
		</div>
		  <br>
		<div class='row col-md-offset-1' >
		  <div id="m-radio" class="col-md-11 no-pad">
		  <input type="radio" name="fcat" id="radio2" class="radio" value='2'  <?php if($facat=='2') echo 'checked';?> />
		  <label for="radio2" ><b><FONT color='green'>FIXED</font> TRIP FARE</b></label>
		  </div>
		  </div>
		  <br>
		<div class='row col-md-offset-1' >
		  <div id="m-radio" class="col-md-11 no-pad">
		  <input type="radio" name="fcat" id="radio3" class="radio" value='3'  <?php if($facat=='3') echo 'checked';?> />
		  <label for="radio3" ><b><FONT color='green'>CLUBED</font> TRIP FARE</b></label>
		  </div>
		</div>				  
	</div>
	
	<div class='col-md-8'>
	
	<div class='row' style='padding-left:10px;' >
		 <label> Fare Details: </label>
		 <hr style='margin-top:3px;'>
		</div>
	
						<div class="form-group" style='margin-top:10px;'>
						   <div class="row">
							  <label class="col-md-3 control-label right1">Corporate Name :</label>
								<div class="col-md-7">
								<select name='coname' class='form-control' required>
								<option value="">-----------</option>
								<?php
								$res=$this->db->select('*')->from('customers')->where('customer_type','2')->get()->result();
								foreach($res as $r)
								{
									?>
									<option value='<?=$r->customer_id;?>'><?=$r->customer_name;?></option>
								<?php
								} ?>
								</select>
								</div>
							</div>
						</div>
						
						
						<div  class="form-group" id ='triptype' style='margin-top:10px;'>
						   <div class="row">
							  <label class="col-md-3 control-label right1">Trip Type:</label>
								<div class="col-md-7">
								<select name='tripop'  id='tripop' class='form-control'>
								<option value="0">-----------</option>
								<?php
								$res=$this->db->select('*')->from('category')->get()->result();
								foreach($res as $r)
								{
									?>
									<option value='<?=$r->category_id;?>'><?=$r->category_name;?></option>
								<?php
								} ?>
								</select>
								</div>
							</div>
						</div>

						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label right1">Vehicle :</label>
								<div class="col-md-7">
								
								<select name='vehicle' class='form-control' required>
								<option value="">-----------</option>
								<?php
								$res=$this->db->select('*')->from('vehicle_types')->get()->result();
								foreach($res as $r)
								{
									?>
									<option value='<?=$r->veh_type_id;?>'><?=$r->veh_type_name;?></option>
								<?php
								} ?>
								</select>

								</div>
							</div>
						</div>	

						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label right1">Fare Hrs :</label>
								<div class="col-md-7">
								<input type="text" class="form-control"  name="farehrs" id='farehrs' required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label right1">Fare KM :</label>
								<div class="col-md-7">
								<input type="text" class="form-control"  name="farekm" id='farekm' required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label right1">Amount :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="amount" id='amount' required>
								</div>
							</div>
						</div>
						
                       <hr>
					                        
 
                    <div class="form-group">
						<div class="row">
						<div class="col-md-2 left1">
						</div>
                               <div class="col-md-9" style="padding-left:25px;">
                                   <button type="submit" id='idsubmit' class="btn btn-primary">Submit Fare Details</button>
                               </div>
                        </div>
                       </div>
	
	</div>
	</div>
	</form>
  
 </div>
</div>
<!--first tabe pane-->

<div class="tab-pane <?php if($ftab==2) echo "active";?>" id="farelst" role="tabpanel">  <!-- class list -->

	<div class='row' style='margin-top:15px;'>
	<div class='col-md-12' style='border-right:1px solid #e4e4e4;'>
	<table class="table table-striped table-hover table-bordered" id="example1">
		  <thead>
			 <tr>
				 <th>Edit/Del</th>
				 <th>ID</th>
				 <th>Corporate Name</th>
				 <th>Vehicle</th>
				 <th>Category</th>
				 <th>Trip</th>
				 <th>Hrs</th>
				 <th>KM</th>
				 <th>Amount</th>
				</tr>
		</thead>
		<tbody>
		<?php
		foreach($result as $rs1)
		{
			if($rs1->fare_category=='1')
					{
						$fcat="NORMAL";
					}
					else if($rs1->fare_category=='2')
					{
						$fcat="FIXED";
					}
					else if($rs1->fare_category=='3')
					{
						$fcat="CLUB";
					}
			
			$mm='<button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button>';
				$del=anchor('General/del_fare_entry/'.$rs1->fare_id, $mm, array('id' =>'del_conf'));
				
				$mm1='<button class="btn btn-default btn-xs" data-title="edit" ><span class="glyphicon glyphicon-edit"></span></button>';
				$edit=anchor('General/edit_fare/'.$rs1->fare_id, $mm1,"");
			?>
			<tr>
			<td><?=$edit."&nbsp;".$del;?></td>
				 <td><?=$rs1->fare_id;?></td>
				 <td><?=$rs1->customer_name;?></td>
				 <td><?=$rs1->veh_type_name;?></td>
				 <td><?=$fcat;?></td>
				 <td><?=$rs1->category_name;?></td>
				 <td><?=$rs1->fare_description;?></td>
				 <td><?=$rs1->fare_km;?></td>
				 <td align='right'><b><?=number_format($rs1->fare_amount,'2','.','');?></b></td>
				</tr>
		<?php
		}
		?>
		</tbody>

   </table>	
  </div>
  </div>

</div>
</div>
</div>
</div>
</div> <!-- end Row --->
   
                <!-- END CONTENT BODY -->
            <!-- </div> -->
            </div>
			
			
			
            <!-- END QUICK SIDEBAR -->
        </div>
</div>

</section>
</div>

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">


 $('#example1').dataTable();
 
 
//sweet alert box ----------------------
$("#idmsg").hide();

var cop=$("#radio1").attr('checked');

if(cop=='checked')
{
 $("#triptype").hide();
}


$("#radio1").hide();
$("#radio2").hide();
$("#radio3").hide();


$("#radio1").change(function()
{
	$("#triptype").hide();
	$("#tripop").removeAttr("required");
});

$("#radio2").change(function()
{
	$("#triptype").show();
	$("#tripop").attr("required","required");
	$("#farehrs").val('0');
	$("#farekm").val('0');
	
});

$("#radio3").change(function()
{
	$("#triptype").show();
	$("#tripop").attr("required","required");
	$("#farehrs").val('0');
	$("#farekm").val('0');
});


if($.trim($("#msg").html())!="")
	{
		swal("Saved",$("#msg").html(),"success")
		$("#msg").html("");
		$("#msgerr").html("");
	}
	
$("#msgerr").hide();
if($.trim($("#msgerr").html())!="")
	{
		swal("Try Again",$("#msgerr").html(),"error")
		$("#msgerr").html("");
		$("#msg").html("");
	}

	$("#msgdel").hide();
if($.trim($("#msgdel").html())!="")
	{
		swal("Deleted",$("#msgdel").html(),"success")
		$("#msgdel").html("");
	}
//-------------------------------------
 
 $("#drshift").change(function()
 {
	 var st=$("#drshift").val();
	 if(st=='1')
	 {
		 $("#drtime").val("6 AM - 8 PM");
	 }
	 else if(st=='2')
	 {
		 $("#drtime").val("8 PM - 6 AM");
	 }
	 $("idsubmit").focus();
 });
  
 
  
  
   $('#example1 tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/edit_fare_list",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 
		
 	
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#msg').html(''); }, 3000);
 //});
 


</script>



