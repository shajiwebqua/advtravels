<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquiry extends CI_Controller {

	function __construct(){
		parent::__construct();
		$user = $this->session->userdata('userid');
		if (!isset($user))
		{ 
			redirect('/');
		} 
		else
		{ 
			return true;
		}
		
	}

	public function index(){
		$this->load->view("admin/Enquiry_form");
	}

	public function add_order(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('sl_no', 'SLNO', 'required');
		$this->form_validation->set_rules('party_name', 'Party Name', 'required');
		$this->form_validation->set_rules('phone_no', 'Phone No', 'required');
		$this->form_validation->set_rules('mobile_no', 'Mobile No', 'required');
		$this->form_validation->set_rules('address', 'Postal Address', 'required');
		$this->form_validation->set_rules('passenger', 'No of Passenger', 'required');
		$this->form_validation->set_rules('no_days', 'No of Days', 'required');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required');
		$this->form_validation->set_rules('start_time', 'Start Time', 'required');
		$this->form_validation->set_rules('return_date', 'Return Date', 'required');
		$this->form_validation->set_rules('return_time', 'Return Time', 'required');
		$this->form_validation->set_rules('return_place', 'Return Place', 'required');
		$this->form_validation->set_rules('journy_details', 'Journey Details', 'required');
		$this->form_validation->set_rules('njourney', 'Nature Journey', 'required');
		$this->form_validation->set_rules('prefered_vehicle', 'Prefered Vehicle', 'required');
		$this->form_validation->set_rules('amount', 'Amount', 'required');
		$this->form_validation->set_rules('upto', 'Upto', 'required');
		$this->form_validation->set_rules('running', 'Running Charge', 'required');
		$this->form_validation->set_rules('from', 'From', 'required');
		//$this->form_validation->set_rules('advance', 'Advance', 'required');
		//$this->form_validation->set_rules('staff_batha', 'Staff Daily Batha', 'required');
		$this->form_validation->set_rules('permit_charge', 'Permit Charge', 'required');
				
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata("message", "4#Details missing.");
			redirect("Enquiry");
		}
		else
		{
			$this->load->model("Model_order");
			$data = array(
				"sl_no" =>$this->input->post("sl_no"),
				"party_name" =>$this->input->post("party_name"),
				"phone_no" =>$this->input->post("phone_no"),
				"mobile_no" =>$this->input->post("mobile_no"),
				"postal_address" =>$this->input->post("address"),
				"no_of_passengers" =>$this->input->post("passenger"),
				"no_of_days" =>$this->input->post("no_days"),
				"starting_date" =>date("Y-m-d",strtotime($this->input->post("start_date"))),
				"starting_time" =>$this->input->post("start_time"),
				"returning_date" =>date("Y-m-d",strtotime($this->input->post("return_date"))),
				"returning_time" =>$this->input->post("return_time"),
				"returning_place_time" =>$this->input->post("return_place"),
				"journey_details" =>$this->input->post("journy_details"),
				"nature_of_journey" =>$this->input->post("njourney"),
				"prefered_vehicle" =>$this->input->post("prefered_vehicle"),
				"amount" =>$this->input->post("amount"),
				"upto_km" =>$this->input->post("upto"),
				"running_charge" =>$this->input->post("running"),
				"startfrom" =>$this->input->post("from"),
				//"advance" =>$this->input->post("advance"),
				//"staff_daily_batha" =>$this->input->post("staff_batha"),
				"permit_charges" =>$this->input->post("permit_charge"),
				"corporate_name"=>$this->input->post("corporate"),
				"corporate_mobile"=>$this->input->post("corpmobile"),
				"agentid"=>$this->input->post("agentid"),
				
				"noof_male"=>$this->input->post('nmale'),
				"noof_female"=>$this->input->post('nfemale'),
				"noof_children"=>$this->input->post('nchildren'),
				"noof_infant"=>$this->input->post('ninfant'),
			);
			
			$this->Model_order->insert("enquiry",$data);
			
			 $uid = $this->db->insert_id();
				$staffid=$this->session->userdata('userid');
				  $staffname=$this->session->userdata('name');
				  $date=date("Y/m/d");
				  date_default_timezone_set('Asia/Kolkata');
				  $time=date('h:i:sa'); 
						$userdt1 = array(
						'log_staffid' => $staffid,
						'log_staffname'=>$staffname,
						'log_time'=>$time,
						'log_date'=>$date,
						'log_entryid'=>$uid,
						'log_desc'=>'Enquiry Added'

					);
				  $this->load->model('Model_customer');
				  $this->Model_customer->form_insertlog($userdt1);
				  $this->session->set_flashdata("message", "1#Enquiry Successfully added");
				  redirect("Enquiry");
		}
	}

	public function enquiry_list(){
		$this->load->view("admin/enquiry_list");
	}

	
	
	
	public function order_ajax()  //enquiry ajax
	{
		$this->load->model('Model_order');
		$results=$this->Model_order->select_all("enquiry","*");
		$data1 = array();
		foreach($results  as $r) 
		{
			$mm= "<center><button class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></button></center>";
            $del=anchor('Enquiry/delete_enquiry/'.$r->sl_no, $mm, array('id' =>'del_enq'));

			array_push($data1, array(
				"sl_no"	=>	"<span class='sln'>" . $r->sl_no . "</span>",
				"name"	=>	$r->party_name,
				"phno"	=>	$r->phone_no . ", " . $r->mobile_no,
				"journey"	=>	$r->journey_details,
				"nature"	=>	$r->nature_of_journey,
				"vehicle"	=>	$r->prefered_vehicle,
				"start_date"	=>	$r->starting_date . " " . $r->starting_time,
				"return_date"	=>	$r->returning_date . " " . $r->returning_time,
				"start_from"=>$r->startfrom,
				"amount"	=>	$r->amount,
				"upto_km"	=>	$r->upto_km,
				"per_km"	=>	$r->running_charge,
				"permit"	=>	$r->permit_charges,
				"delete"	=> $del
			));
		}
		echo json_encode(array('data' => $data1));
	}


	public function show_order(){
		$data=$this->session->all_userdata();
		$slno=$this->input->post('slno');
		$this->load->model('Model_order');
		$row=$this->Model_order->select("enquiry",array("sl_no" => $slno));
		echo "
		<style>
			/* hide number  spinner*/
			input[type=number]::-webkit-inner-spin-button, 
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}

		</style>         
		<div class='modal-header'>
			<button type='button' class='close' data-dismiss='modal'>&times;</button>
			<h4 class='modal-title' > <font color='blue'>Party Order</font></h4>
		</div>
		<br>
		<form  class='form-horizontal' role='form' method='post' action='' >

			<!-- text input -->
			<div class='row'> 

				<div class='col-md-6'>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>Sl. No :</label>
						<div class='col-md-7'>  
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->sl_no."' />
						</div>                   
					</div>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>Party Name :</label>
						<div class='col-md-7'>  
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->party_name."' />
						</div>                   
					</div>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>Phone No :</label>
						<div class='col-md-7'>  
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->phone_no."' />
						</div>                   
					</div>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>Mobile No :</label>
						<div class='col-md-7'>  
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->mobile_no."' />
						</div>                   
					</div>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>Postal Address :</label>
						<div class='col-md-7'>  
							<textarea class='form-control' onkeydown='return false' >".$row[0]->postal_address."</textarea>
						</div>                   
					</div>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>No. of Passengers :</label>
						<div class='col-md-7'>  
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->no_of_passengers."' />
						</div>                   
					</div>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>No. of days :</label>
						<div class='col-md-7'>  
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->no_of_days."' />
						</div>                   
					</div>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>Starting Date & Time :</label>
						<div class='col-md-7'>  
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->starting_date."' />
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->starting_time."' />
						</div>                   
					</div>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>Returning Date & Time :</label>
						<div class='col-md-7'>  
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->returning_date."' />
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->returning_time."' />
						</div>                   
					</div>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>Returning Place & Time :</label>
						<div class='col-md-7'>  
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->returning_place_time."' />
						</div>                   
					</div>
					
				</div>

				<div class='col-md-6'>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>Nature of Journy :</label>
						<div class='col-md-7'>  
							<input type='text' class='form-control'onkeydown='return false' value='".$row[0]->nature_of_journey."' />
						</div>                   
					</div>
					<div class='form-group'>
						<label class='col-md-4 control-label' style='padding-top:5px;'>Vehicle arriving place & details of journy :</label>
						<div class='col-md-8'>  
							<textarea onkeydown='return false' >".$row[0]->journey_details."</textarea>
						</div>                   
					</div>
					<div class='form-group'>
						<label class='col-md-4  control-label'>Prefered Vehicle :</label>
						<div class='col-md-7'>
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->prefered_vehicle."' />
						</div>
					</div>
					<div class='form-group'>
						<label class='col-md-4  control-label'>Amount :</label>
						<div class='col-md-7'>
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->amount."' />
						</div>
					</div>
					<div class='form-group'>
						<label class='col-md-4  control-label'>Upto ( per Km ) :</label>
						<div class='col-md-7'>
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->upto_km."' />
						</div>
					</div>
					<div class='form-group'>
						<label class='col-md-4  control-label'>Running (per RKm) :</label>
						<div class='col-md-7'>
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->running_charge."' />
						</div>
					</div>
					<div class='form-group'>
						<label class='col-md-4  control-label'>From :</label>
						<div class='col-md-7'>
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->from."' />
						</div>
					</div>
					<div class='form-group'>
						<label class='col-md-4  control-label'>Advance Rs :</label>
						<div class='col-md-7'>
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->advance."' />
						</div>
					</div>
					<div class='form-group'>
						<label class='col-md-4  control-label'>Staff Daily Batha :</label>
						<div class='col-md-7'>
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->staff_daily_batha."' />
						</div>
					</div>
					<div class='form-group'>
						<label class='col-md-4  control-label'>Permit Charge :</label>
						<div class='col-md-7'>
							<input type='text' class='form-control' onkeydown='return false' value='".$row[0]->permit_charges."' />
						</div>
					</div>
				</div>  
			</div>

			<label style='width:100%;height:1px;background-color:#e4e4e4;'></label>  
			<div class='form-group'> 
				<div class='col-md-12'>
					<center> <button type='button' class='btn btn-default' data-dismiss='modal' style='margin-left:20px;'>Close</button></center>

				</div>
			</div>     
			<br>

		</form>    
	<!-- <div class='modal-footer'>

</div>  -->
";
}

public function delete_enquiry(){
	$slno = $this->uri->segment(3);
	$this->load->model("Model_order");
	$this->Model_order->delete("enquiry", array("sl_no" => $slno));
	 $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
      $date=date("Y/m/d");
      date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa'); 
			$userdt1 = array(
        'log_staffid' => $staffid,
        'log_staffname'=>$staffname,
        'log_time'=>$time,
        'log_date'=>$date,
        'log_entryid'=>$slno,
        'log_desc'=>'Enquiry Deleted'

        );
      $this->load->model('Model_customer');
      $this->Model_customer->form_insertlog($userdt1);
	$this->session->set_flashdata("message", "3#Enquiry Successfully Removed");
	redirect("Enquiry/enquiry_list");
}

}