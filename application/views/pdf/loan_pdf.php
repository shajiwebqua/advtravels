<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

 <table width="100%"><tr><td><h4>View All Vehicles Loan List </h4></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
		<table style="width:100%;border:1px solid #e4e4e4;" cellpadding=5 cellspacing=0>
		   <thead>
			<tr>
			   <tr style='color:#4b88ed;border:1px solid #e4e4e4;'>
			    <th class='th1'>Slno</th>
				<th class='th1'>LTL Ref.No</th>
				 <th class='th1'>Loan Starting Date</th>
				<th class='th1'>Vehicle</th>
				<th class='th1'>Vehicle No</th>
				<th class='th1'>Amount</th>
				<th class='th1'>Loan Amount</th>
				<th class='th1'>Interest/Period</th>
				 <th class='th1'>Interest</th>
				 <th class='th1'>Down Payment</th>
				 <th class='th1'>Other charges</th>
				 <th class='th1'>Loan Total</th>

				</tr>
			</tr>
		</thead>
		<tbody>
			<?php
			$slno=1;
				foreach ($results as $key => $value) {
					
				?>
				<tr >
				<td class='td1'><?=$slno;?> </td> 
				<td class='td1'><?=$value->loan_ltlrefno;?> </td> 
				<td class='td1'><?=$value->loan_startingdate;?> </td> 
				<td class='td1'><?=$value->loan_vehiclename;?></td>
				<td class='td1'><?=$value->loan_vehicleno;?> </td>
				<td class='td1'><?=$value->loan_vehicleamount;?> </td>
				<td class='td1'><?=$value->loan_amount;?> </td>
			    <td class='td1'><?=$value->loan_interestrate.'%,'.$value->loan_term;?>months </td>
			    <td class='td1'><?=$value->loan_interest;?> </td>
			    <td class='td1'><?=$value->loan_downpayment;?> </td>
			    <td class='td1'><?=$value->loan_othercharges;?> </td>
			    <td class='td1'><?=$value->loan_totalamt;?> </td>

			<!-- 	<td class='td1'><?=$value->vehicle_status;?> </td> -->
			</tr>
				<?php
				
				$slno++;
				}
				?>
		  
		</tbody>
		</table>
</body>
</html>