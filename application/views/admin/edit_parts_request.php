<?php
include('application/views/common/header.php');
?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1><b>Edit Service Parts Request</b></h1>
		<ol class="breadcrumb" style='font-size:15px;'>
			<li><a href="<?php echo base_url('');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Service Request List</button></a></li>
			
	    </ol> 
         <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
  
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
  <div class="col-md-12">
   <div style="background-color:#fff;border-radius:2px;padding:15px; ">
    
            <div class="portlet-title">
                     <div class="caption" >
							<h4 style='margin-top:0px;'><i class="fa fa-plus-circle" aria-hidden="true"></i> Edit details</h4>
                     </div>
            </div>
			
  <hr style="background-color:#cecece,border:1px;width:100%;top:10px;margin:0px;padding:3px 0px 10px 0px;">
             
 <div class="portlet-body form">
 <div class='row'>
 <div class="col-md-5">
        <form enctype="multipart/form-data" method="POST" id="editservicerequest" name="editservicerequest" action="">
        <div class="form-group">
        <label for="date">Date</label>
		<?php
		$d=$result->srequest_date;
		$d1=explode("-",$d);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		?>
        <input type="text" class="form-control" id="datepicker1" name="date" required value="<?php echo $dt1?>">
        </div>
        <div class="form-group">
        <label for="reg_no">Reg No:</label>
        <input type="text" class="form-control" id="reg_no" name="reg_no" required value="<?php echo $result->srequest_regno?>">
        <input type="hidden" class="form-control" id="service_id" name="service_id" required value="<?php echo $result->srequest_id?>">
        </div>
        <div class="form-group">
        <label for="vehicle_type">Vehicle Type:</label>
        <select name="vehicle_type" id="vehicle_type" class="form-control" required>
          <option value="">----select----</option>
          <?php foreach($veh_types as $row1){?>
          <option value="<?= $row1->veh_type_id ?>" <?php if($result->srequest_vtype==$row1->veh_type_id) echo "selected";?>><?= $row1->veh_type_name;?></option>
          <?php } ?>
        </select>
        </div>
        <div class="form-group">
        <label for="service_category">Service Category:</label>
        <select name="service_category" id="service_category" class="form-control" required>
          <option value="">----select----</option>
          <option value="1" <?php if($result->srequest_category==1) echo "selected";?>>Toyota Servicing</option>
          <option value="2" <?php if($result->srequest_category==2) echo "selected";?>>General</option>
        </select>
        </div>

        <div class="form-group">
        <label for="date_time">Expected date & time for Service Entry :</label>
		<?php
		$d=$result->srequest_datetime;
		$d1=explode("-",$d);
		$dt2=$d1[2]."-".$d1[1]."-".$d1[0];
		?>
        <input type="text" class="form-control" id="datepicker2" name="date_time" required value="<?php echo $dt2?>">
        </div>
        <div class="form-group">
        <label for="duration">Expected duration for delivery :</label>
        <input type="text" class="form-control" id="duration" name="duration" required value="<?php echo $result->srequest_duration?>">
        </div>
        <div class="form-group">
        <label for="center/workshop">Service centre/Workshop :</label>
        <textarea name="center_workshop" id="center_workshop" class="form-control" required><?php echo $result->srequest_workshop?></textarea>
        </div>
        
        </div>
        <div class="col-md-1">
        
        </div>
        <div class="col-md-6">
        <div class='row' style='margin-bottom:10px;'>
		<Label class='col-md-12'><b>Requested Parts Details</b></label>
		</div>
            <table id="example" class="table table-striped table-bordered display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <td width="8%">No</td>
                <td width="55%">Parts</td>
                <td width="20%">Price</td>
                
            </tr>
        </thead>
        <tbody>
           <?php foreach($requirements as $row2){?>
           <tr>
               <td><?php echo $row2->pk_int_id;?></td>
               <td><?php echo $row2->parts;?></td>
               <td><?php echo $row2->price;?></td>
           </tr>
           <?php } ?>
        </tbody>
                
    </table> 
   <button type="submit" class="btn btn-warning" style='padding:5px 20px 5px; 20px;' >Update Details</button>
        </div>
 </div>
 
 </div> <!--portlet-body-->
 
 </div>
</div>    
<br>  
  </div>
  </div>
</section>

<?php include('application/views/common/footer.php');?>

  </div>
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script> 

<script type="text/javascript">

 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
 
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
	//endDate:'now'
   }); 
    
	
$('#example').SetEditable({
columnsEd: "1,2", 

});

function storeTblValues()
{
    var TableData = new Array();

    $('#example tr').each(function(row, tr){
        TableData[row]={
            "No" : $(tr).find('td:eq(0)').text()
            , "Parts" :$(tr).find('td:eq(1)').text()
            , "Price" : $(tr).find('td:eq(2)').text()
        }    
    }); 
    TableData.shift();  // first row will be empty - so remove
    return TableData;
}

$(document).on("submit", "#editservicerequest", function (event) {
          
    event.preventDefault();
    var service_id = $("#service_id").val();
    var edate =  $('#datepicker1').val();
    var reg_no =  $('#reg_no').val();
    var vehicle_type =  $('#vehicle_type').val();
    var service_category =  $('#service_category').val();
    var date_time =  $('#datepicker2').val();
    var duration =  $('#duration').val();
    var centerworkshop =  $('#center_workshop').val();
    var TableData;
    TableData = $.toJSON(storeTblValues());

    $.ajax({
            url: "<?php echo base_url();?>Services/update_service_request",
            type: 'POST',
            data: {pTableData:TableData,service_id:service_id,edate:edate,reg_no:reg_no,vehicle_type:vehicle_type,service_category:service_category,date_time:date_time,duration:duration,centerworkshop:centerworkshop},
            
            success: function (data, status)
			{
              if(data="success")
              {
					 swal({
					  title: "Updated.",
					  text: "Service parts request updated.!",
					  type: "success",
					  showCancelButton: false,
					  confirmButtonClass: "btn-success",
					  confirmButtonText: " Ok ",
					  closeOnConfirm: false
					},
					function(){
					  window.location="request_list"; 
					});
              }
              else
              {
                swal("Error", "Something went wrong", "error");    
              }  
            }
          });
});
  
</script>


  </body>
</html>
