<?php
class Model_reports extends CI_Model{



  public function view_trip(){

    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
   $this->db->order_by("trip_management.trip_id","desc");
	$query = $this->db->get();
    return $query->result();
}
public function view_cotrip_date($dte1,$dte2){

    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
	
    $dc="trip_management.trip_enddate>='".$dte1."' and trip_management.trip_enddate<='".$dte2."'";
	$this->db->where($dc);
	$this->db->where("trip_management.trip_status=2");
	$this->db->order_by("trip_management.trip_id","desc");
	$query = $this->db->get();
    return $query->result();
}

public function view_cltrip_date($dte1,$dte2){

    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
    	$dc="trip_management.trip_enddate>='".$dte1."' and trip_management.trip_enddate<='".$dte2."'";
		$this->db->where($dc);
		$this->db->where("trip_management.trip_status=3");
	$this->db->order_by("trip_management.trip_id","desc");
	$query = $this->db->get();
    return $query->result();
}

public function view_rtrip_date($dte1,$dte2){

    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
	if($dte1!=="" and $dt2!=="")
	{
    	$dc="trip_management.trip_enddate>='".$dte1."' and trip_management.trip_enddate<='".$dte2."'";
		$this->db->where($dc);
	}
		$this->db->where("trip_management.trip_status=1");
		$this->db->where("trip_management.trip_startdate<=",date('Y-m-d'));
	$this->db->order_by("trip_management.trip_id","desc");
	$query = $this->db->get();
    return $query->result();
}

public function view_utrip_date($dte1,$dte2){

    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
	if($dte1!=="" and $dt2!=="")
	{
    	$dc="trip_management.trip_enddate>='".$dte1."' and trip_management.trip_enddate<='".$dte2."'";
		$this->db->where($dc);
	}
		$this->db->where("trip_management.trip_status=1");
		$this->db->where("trip_management.trip_startdate>",date('Y-m-d'));
	$this->db->order_by("trip_management.trip_id","desc");
	$query = $this->db->get();
    return $query->result();
}


public function view_allvehicles(){

        $this->db->select('*');
        //$this->db->join('vehicle_ownerreg', 'vehicle_ownerreg.owner_id=vehicle_registration.vehicle_owner','inner');
        $this->db->join('vehicle_types', 'vehicle_types.veh_type_id=vehicle_registration.vehicle_type','inner');
        $this->db->from('vehicle_registration');
        $query = $this->db->get();
        return $query->result();
    }
	
    public function view_service(){
        $this->db->select('*');
        $this->db->from('vehicle_service');
        $this->db->join('vehicle_registration ','vehicle_service.service_vehicleid=vehicle_registration.vehicle_id','inner');
        // $this->db->join('vehicle_ownerreg ','vehicle_ownerreg.owner_id=vehicle_registration.vehicle_owner','inner');
        $this->db->where("vehicle_registration.vehicle_status=1");
        $query = $this->db->get();
        return $query->result();
    }
	
     public function view_servicing(){
         $this->db->select('*');
        //$this->db->join('vehicle_ownerreg', 'vehicle_ownerreg.owner_id=vehicle_registration.vehicle_owner','inner');
        $this->db->join('vehicle_types', 'vehicle_types.veh_type_id=vehicle_registration.vehicle_type','inner');
		$this->db->join('service_trips', 'service_trips.srtrip_vehicleid=vehicle_registration.vehicle_id','inner');
		$this->db->from('vehicle_registration');
        $this->db->where("vehicle_registration.vehicle_status=2");
         $query = $this->db->get();
        return $query->result();
    }

    public function view_month_service($month){
        $this->db->select('*');
        //this->db->join('vehicle_ownerreg', 'vehicle_ownerreg.owner_id=vehicle_registration.vehicle_owner','inner');
        $this->db->join('vehicle_types', 'vehicle_types.veh_type_id=vehicle_registration.vehicle_type','inner');
		$this->db->join('service_trips', 'service_trips.srtrip_vehicleid=vehicle_registration.vehicle_id','inner');
        $this->db->from('vehicle_registration');
        $this->db->where("vehicle_registration.vehicle_status=2");
        $this->db->where("MONTH(vehicle_registration.vehicle_date)",$month);
        $query = $this->db->get();
        return $query->result();
    }


     public function view_vendors(){
        $this->db->select('*');
        $this->db->from('vehicle_ownerreg');
        $query = $this->db->get();
        return $query->result();
    }


     public function view_logs_date($date){
        $this->db->select('*');
        $this->db->from('log');
        $this->db->where('log.log_date',$date);
        $query = $this->db->get();
        return $query->result();
    }

    public function view_logs_month($month){
        $this->db->select('*');
        $this->db->from('log');
        $this->db->where("MONTH(log.log_date)",$month);
        $query = $this->db->get();
        return $query->result();
    }
    }


