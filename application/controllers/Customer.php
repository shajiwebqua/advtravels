    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Customer extends CI_Controller
	{
      function __construct(){
        parent::__construct();
        $user = $this->session->userdata('userid');
        if (!isset($user))
        { 
          redirect('/');
        } 
        else
        { 
          return true;
        }
        $this->load->helper(array('form', 'url'));
        $this->load->model('Model_customer');
      }

   public function index()
      {
        $this->load->model('Model_customer');
        $this->load->view('admin/customer');
    }
      
   public function add_new_customer()
   {

    $op1=$this->uri->segment(3);   //option for adding cutomer from trip sheet

    $this->load->library('form_validation');

    $this->form_validation->set_rules('name', 'name', 'required');
    $this->form_validation->set_rules('email', 'email', 'required');

    if($this->form_validation->run())
    {

      $ccode=$this->input->post('custcode');
	  $ctype=$this->input->post('custtype');
      $cdate=$this->input->post('custdate');
      $cagent=$this->input->post('coagent1');
      $name=$this->input->post('name');
      $email=$this->input->post('email');
      $mobile=$this->input->post('mobile');
      $address=$this->input->post('address');
      $landline=$this->input->post('landline');
      $country=$this->input->post('country');
      $area=$this->input->post('area');
      $desc=$this->input->post('description');
      $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
	  
	  $staffname=$this->session->userdata('name');
	  
	  $gst=$this->input->post('gst');
	  $gstno=$this->input->post('gstno');
	  
	  
      $date=date("Y/m/d");
      date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa'); 
          //$status=$this->input->post('status');

      $userdt = array(
        'customer_code' => $ccode,
		'customer_type' => $ctype,
        'customer_edate' => $cdate,
        'customer_coagent' => $cagent,
        'customer_name' => $name,
        'customer_email' => $email,
        'customer_mobile' =>$mobile,
        'customer_landline' =>$landline,
        'customer_address' =>$address,
        'customer_country' =>$country,
        'customer_area' =>$area,
        'customer_description' =>$desc,
        'customer_status'=>"1"
        );
			
      $this->load->Model('Model_customer');
      $this->Model_customer->form_insert($userdt);
      $uid = $this->db->insert_id();
	  
	  $gst=array('cg_custid'=>$uid,'cg_gst'=>$gst,'cg_gstno'=>$gstno);
	  $this->Model_customer->insert_gst($gst);
	  
      $this->session->set_flashdata('mesage', 'Customer details successfully added..!');
      $userdt1 = array(
        'log_staffid' => $staffid,
        'log_staffname'=>$staffname,
        'log_time'=>$time,
        'log_date'=>$date,
        'log_entryid'=>$uid,
        'log_desc'=>'Customer Details Added'
        );
      $this->load->model('Model_customer');
      $this->Model_customer->form_insertlog($userdt1);
      if($op1=='1')
       redirect('Customer/index');
     else
       redirect('Trip/index');
    }
    else{
     $this->session->set_flashdata('message1', 'Data missing,try again..');
            //$data['message']="Data Inserted Successfully";

     if($op1=='1')
       redirect('Customer/index');
     else
       redirect('Trip/index');
    }  
    }

     public function add_new_customer1()
      {

    $op1=$this->uri->segment(3);   //option for adding cutomer from trip sheet

    $this->load->library('form_validation');

    $this->form_validation->set_rules('name', 'name', 'required');
    $this->form_validation->set_rules('email', 'email', 'required');

    if($this->form_validation->run())
    {
		
		if (!empty($_FILES['files']['name']))
		{
			$filesCount = count($_FILES['files']['name']);
			
			$fname="";	
			for($i = 0; $i < $filesCount; $i++)
			{
                $_FILES['ivf']['name'] = $_FILES['files']['name'][$i];
                $_FILES['ivf']['type'] = $_FILES['files']['type'][$i];
                $_FILES['ivf']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['ivf']['error'] = $_FILES['files']['error'][$i];
                $_FILES['ivf']['size'] = $_FILES['files']['size'][$i];
				
			    $uploadPath = 'upload/customer/';

                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|jpe';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				
                if($this->upload->do_upload('ivf'))
				{
                 $fileData=array('upload_data'=> $this->upload->data());
				 
						if($i==$filesCount-1)
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'];
						 }
						 else
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'].",";
						 }
				}
				else
				{
					echo $this->upload->display_errors();
					
				}
			}
		}
	
      $ccode=$this->input->post('custcode');
	  $ctype=$this->input->post('custtype');
      $cdate=$this->input->post('custdate');
      $cagent=$this->input->post('coagent1');
      $name=$this->input->post('name');
      $email=$this->input->post('email');
      $mobile=$this->input->post('mobile');
      $address=$this->input->post('address');
      $landline=$this->input->post('landline');
      $country=$this->input->post('country');
      $area=$this->input->post('area');
      $keyperson=$this->input->post('keyperson');
      $kpmobile=$this->input->post('keymobile');
      $conperson=$this->input->post('conperson');
      $cpmobile=$this->input->post('conmobile');
      $accperson=$this->input->post('accperson');
      $apmobile=$this->input->post('accmobile');
      $desc=$this->input->post('description');
	  
	  $gst=$this->input->post('gst');
	  $gstno=$this->input->post('gstno');

	  
      $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
      $date=date("Y/m/d");
	  
      date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa'); 
          //$status=$this->input->post('status');

		  if($cagent=="")
			  $cagent='1';
		  
		  
      $userdt = array(
        'customer_code' => $ccode,
		'customer_type' => $ctype,
        'customer_edate' => $cdate,
        'customer_coagent' => $cagent,
        'customer_name' => $name,
        'customer_email' => $email,
        'customer_mobile' =>$mobile,
        'customer_landline' =>$landline,
        'customer_address' =>$address,
        'customer_country' =>$country,
        'customer_area' =>$area,
		
		'customer_keyperson' => $keyperson,
        'customer_keymobile' =>$kpmobile,
        'customer_conperson' =>$conperson,
        'customer_conmobile' =>$cpmobile,
        'customer_accperson' =>$accperson,
        'customer_accmobile' =>$apmobile,
		
        'customer_description' =>$desc,
		'customer_proof'=>$fname,
        'customer_status'=>"1"
        );
		
		
      $this->load->Model('Model_customer');
      $this->Model_customer->form_insert($userdt);
      $uid = $this->db->insert_id();
     
	   $gst=array('cg_custid'=>$uid,'cg_gst'=>$gst,'cg_gstno'=>$gstno);
	   $this->Model_customer->insert_gst($gst);
	 	 
      $userdt1 = array(
        'log_staffid' => $staffid,
        'log_staffname'=>$staffname,
        'log_time'=>$time,
        'log_date'=>$date,
        'log_entryid'=>$uid,
        'log_desc'=>'Customer Details Added'

        );
      $this->load->model('Model_customer');
      $this->Model_customer->form_insertlog($userdt1);

      $this->session->set_flashdata('message', 'Customer details  added..!');
       }
    else{
     $this->session->set_flashdata('message1', 'Data missing,try again..');
            //$data['message']="Data Inserted Successfully";
   }

	 if($op1=='1')
       redirect('Customer/index');
     else
       redirect('Trip/index');
	}
	
	
    public function customer_ajax()
    {
      $this->load->model('Model_customer');
      $results=$this->Model_customer->view_customer();

      $data1 = array();
      foreach($results  as $r) 
      {
       $chk="<a href='' id='$r->customer_id' class='custselect open-AddBookDialog2'><center><input type='checkbox' class='select-checkbox'></center></a>";           
       $edit="<a href='' id='".$r->customer_id."' data-toggle='modal' class='edit'><center><button class='btn btn-warning btn-xs'  data-toggle='modal' data-target='#myModal2' style='padding-left:11px;padding-right:11px;'>Edit</button></center></a>";
       $view="<a href='#myModal2' id='".$r->customer_id."' data-toggle='modal' class='view'><center><button class='btn btn-info btn-xs'  data-toggle='modal' ><span class='glyphicon glyphicon-list-alt'></span></button></center></a>";
       $mm= "<center><button class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' >Delete</button></center>";
       $del=anchor('Customer/del_entry/'.$r->customer_id, $mm, array('id' =>'del_conf'));
                 //$del=anchor('customer/del_entry/'.$r->customer_id, 'Delete', array('id' =>'del_conf'));

       if($r->customer_type==1)
       {
        $st="Individual";
      }
      else
      {
        $st="Corporate";
      }
	 
	 if($r->customer_coagent=="0")
	  $ana="-";
	else
	  $ana=$r->agent_name;
	  
      array_push($data1, array(
	  "action"=>$edit.$del,
        "custid"=>"$r->customer_id",
        "code"=>"$r->customer_code",
		"ctype"=>"$st",
        "date"=>"$r->customer_edate",
        "name"=>"$r->customer_name",
        "mobile"=>"$r->customer_mobile",
        "coagent"=>$ana,
        "country"=>"$r->country_name",
        "area"=>"$r->area_name",
        ));
    }
    echo json_encode(array('data' => $data1));
              //$this->load->view('admin/view_business_category',$data1);
    }
	
	public function corporate_ajax()
    {
      $this->load->model('Model_customer');
      $results=$this->Model_customer->view_corporates();

      $data1 = array();
      foreach($results  as $r) 
      {
      if($r->customer_type==2)
	  {
		  
		$edit="<a href='#myModal1' id='$r->customer_id' data-toggle='modal' class='edit'><center><button class='btn btn-warning btn-xs' data-title='Edit' data-toggle='modal' >Edit</button></center></a>";
       		  
      array_push($data1, array(
	    "action"=>$edit,
        "custid"=>"$r->customer_id",
        "name"=>"$r->customer_name",
        "kperson"=>$r->customer_keyperson. " - " .$r->customer_keymobile ,
        "cperson"=>$r->customer_conperson. " - " .$r->customer_conmobile ,
        "aperson"=>$r->customer_accperson. " - " . $r->customer_accmobile ,
        ));
	  }
    }
    echo json_encode(array('data' => $data1));
              //$this->load->view('admin/view_business_category',$data1);
    }
		
	function edit_corporate()
	{
		$id=$this->input->post('cid');
		$res=$this->db->select('*')->from('customers')->where('customer_id',$id)->get()->row();
		
		echo" <div class='modal-header'>
                  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                  <h4 class=modal-title>Edit</h4>
                </div>
				<form method='post' role='form' action='". base_url("Customer/update_corporate")."'>
		 <div id='KCA' style='padding-left:10px;'>
				<input type='hidden' name='custid'  value='".$res->customer_id."'/> 
						<div class='row'>
						<div class='row' style='margin-top:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;text-align:right;'>Key Person : </label>
							<div class='col-md-8'>  
								<textarea rows='3' name='keyperson' class='form-control'  id='keyperson'>".$res->customer_keyperson."</textarea>
							</div>                   
						</div>
						
						<div class='row' style='margin-top:10px;margin-bottom:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;text-align:right;'> Mobile: </label>
							<div class='col-md-8'>  
								<input type='text' name='keymobile' class='form-control'  id='keymobile' value='".$res->customer_keymobile."'/>
							</div>                   
						</div>
						</div>
						
						<div class='row' >
						<div class='row' style='margin-top:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;text-align:right;'>Contact Person : </label>
							<div class='col-md-8'>  
								<textarea rows='3' name='conperson' class='form-control'  id='conperson' >".$res->customer_conperson."</textarea>
							</div>                   
						</div>
						
						<div class='row' style='margin-top:10px;margin-bottom:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;text-align:right;'> Mobile: </label>
							<div class='col-md-8'>  
								<input type='text' name='conmobile' class='form-control'  id='conmobile' value='".$res->customer_conmobile."' />
							</div>                   
						</div>
						</div>
						
						<div class='row' >
						<div class='row' style='margin-top:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;text-align:right;'>Account Person : </label>
							<div class='col-md-8'>  
								<textarea rows='3' name='accperson' class='form-control'  id='accperson' >".$res->customer_accperson."</textarea>
							</div>                   
						</div>
						
						<div class='row' style='margin-top:10px;margin-bottom:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;text-align:right;'> Mobile: </label>
							<div class='col-md-8'>  
								<input type='text' name='accmobile' class='form-control'  id='accmobile' value='".$res->customer_accmobile."' />
							</div>                   
						</div>
					
						</div>
					
				  </div>
				  <div class='modal-footer'>
				  <input type='submit' class='btn btn-primary' aria-hidden='true' style='padding-left:30px;padding-right:30px;' value='Update'>
                  <button type='button' class='btn btn-default' data-dismiss='modal' aria-hidden='true'>Close</button>
                 
                </div>
				</form>
		";
	}
	
	
	function update_corporate()
	{
	  
        $this->load->model('Model_customer');

        $cid=$this->input->post('custid');  

        $kperson=$this->input->post('keyperson');
        $kmobile=$this->input->post('keymobile');
        $cperson=$this->input->post('conperson');
        $cmobile=$this->input->post('conmobile');
        $aperson=$this->input->post('accperson');
        $amobile=$this->input->post('accmobile');
        
		
        $contact = array(
        'customer_keyperson' => $kperson,
		'customer_keymobile' => $kmobile,
		'customer_conperson' => $cperson,
		'customer_conmobile' => $cmobile,
		'customer_accperson' => $aperson,
		'customer_accmobile' => $amobile,
        );
		$this->load->model('Model_customer');
        $this->Model_customer->update_corporate($cid,$contact);
        $this->session->set_flashdata('message',  'Updated#Contact Details Updated..!');
		redirect("admin/contacts/");
		
		
	}
	
	

    public function add_customer(){
      $this->load->model('Model_customer');
      $this->db->select_max('customer_code');
      $this->db->from('customers');
      $query=$this->db->get();
      $row=$query->row();
      $data["ccode"]  = $row->customer_code + 1;
      //$data["cdt"]  = date('Y-m-d');

      $this->load->view("admin/add_customer", $data);
    }
		
	
    public function add_tripcustomer() 
    {
      $this->load->model('Model_customer');
      $this->db->select_max('customer_code');
      $this->db->from('customers');
      $query=$this->db->get();
      $row=$query->row();
      $ccode = $row->customer_code + 1;
      $cdt = date('Y-m-d');
      $op = 1;

	  
	echo "    
      <style>

        /* hide number  spinner*/
        input[type=number]::-webkit-inner-spin-button, 
        input[type=number]::-webkit-outer-spin-button { 
          -webkit-appearance: none; 
          margin: 0; 
        }
        .ttext
        {
         background-color:#fff;
         font-weight:bold;
         color:Green;
       }

       .search-btn
       {
         padding-top:3px;
         z-index:1000;
         right:17px;
         position:absolute;
       }


       .select_btn1 ,.select_btn2{

        position: absolute;
        top: 3px;
        z-index: 999;

      }
      .select_btn1{
        right: 19px;
      }
      .select_btn2 {
        right: 19px;
      }

    </style>         
    <div class='modal-header'>
      <button type='button' class='close' data-dismiss='modal'>&times;</button>
      <h4 class='modal-title' > <font color='blue'>Add customer</font></h4>
    </div>
    <br>
	<div style='height:500px; overflow:auto;'>
	<form  class='form-horizontal' role='form' method='post' action=". base_url('Customer/add_new_customer1/2')." enctype='multipart/form-data' onsubmit='return checkdata();' >
  				<!-- text input -->
  				<div class='row' style='width:95%'> 
  					<div class='col-md-10'>
  						<div class='form-group'>
						
						<div class='row' style='margin-top:10px;'>
  							<label class='col-md-4 control-label' style='padding-top:3px;'>Customer Code : </label>
  							<div class='col-md-4'>  
  								<input type='hidden' name='opt' value='$op'/>
  								<input type='text' name='custcode' class='form-control' value='". $ccode."' readonly />
  							</div>                   
  						</div>

						<div class='row' style='margin-top:10px;'>
							<label class='col-md-4 control-label' style='padding-top:3px;'>Customer Category : </label>
							<div class='col-md-4'> 
								<select name='custtype' class='form-control' id='custtype' required>
									<option value=''>----------</option> 
									<option value='1'>INDIVIDUAL</option>
									<option value='2'>CORPORATE</option> 
									<option value='3'>AGENT</option> 
									
								</select>
						</div>                   
						</div>
						
  						<div class='row' style='margin-top:10px;'>
  							<label class='col-md-4 control-label' style='padding-top:3px;'>Date : </label>
  							<div class='col-md-4'>  
  								<input type='date' name='custdate' class='form-control' value='". date('Y-m-d')."' required />
  							</div>                   
  						</div>

  						
						
						<div class='row' style='margin-top:10px;'>
						<div id='co_agent'>
							<label class='col-md-4 control-label' style='padding-top:3px;'>C/O Agent : </label>
							<div class='col-md-4'> 
								<select name='coagent' id='coagent' class='form-control' >
									<option value=''>----------</option> ";
							
							  $result1=$this->Model_customer->view_agents();
							  foreach($result1 as $ro){
								  echo "<option value='".$ro->agent_id."'>".$ro->agent_name."</option>";
							  }
							 
							  echo "
						  </select>
						  <input type='hidden' name='coagent1' class='form-control' id='coagent1'  value='' /> 
						  </div>
						</div>                   
						
						</div>    
						<div class='row' style='margin-top:10px;'>
							<label class='col-md-4 control-label' style='padding-top:3px;'>Name : </label>
							<div class='col-md-7'>  
								<input type='text' name='name' class='form-control'  id='mask_date2' required />
							</div>                   
						</div>

						<div class='row' style='margin-top:10px;'>
							<label class='col-md-4 control-label' style='padding-top:3px;'>Address : </label>
							<div class='col-md-7'>  

								<textarea name='address' rows='3' class='form-control' required></textarea>
							</div>                   
						</div>
						
					<div class='row' style='margin-top:10px;'>
						<label class='col-md-4  control-label'>Mobile : </label>
						<div class='col-md-4'>
							<input type='number' class='form-control' placeholder='Enter mobile no' name='mobile' id='mobile' required>
							<label id='mobmessage' style='color:red;'></label>
						</div>
					</div>

					<div class='row' style='margin-top:10px;'>
						<label class='col-md-4  control-label'>Landline : </label>
						<div class='col-md-4'>
							<input type='number' class='form-control' placeholder='Enter landline' name='landline' value='' required id='landline' required/>
							<label id='landmessage' style='color:red;'></label>
						</div>
					</div>
					
					
					<div class='row' style='margin-top:10px;'>
						<label class='col-md-4 control-label' style='padding-top:3px;'>Email : </label>
						<div class='col-md-7'>  

							<input type='email' name='email' class='form-control'  id='mask_date2' required/>
						</div>                   
					</div>
										
					
					<div class='row' style='margin-top:20px;'>
						<label class='col-md-4 control-label'>Gst% : </label><label style='margin-top:5px;'>Eg: 18</label>
						<div class='col-md-2'>
							<input type='number' class='form-control' placeholder='enter gst%' name='gst' value=''/>
						</div>
					</div>
					
					<div class='row' style='margin-top:10px;'>
						<label class='col-md-4 control-label' style='padding-top:3px;'>Gst Number : </label>
						<div class='col-md-4'>  
							<input type='text' name='gstno' placeholder='enter gst no' class='form-control' required/>
						</div>                   
					</div>
										
					<div class='row' style='margin-top:20px;'>
						<label class='col-md-4 control-label' style='padding-top:3px;'>Country : </label>
						<div class='col-md-4'> 
							<select name='country' class='form-control' id='country1' required>
								<option value=''>----------</option>";
								
								$result1=$this->Model_customer->view_country();

								foreach($result1 as $ro){
								echo "<option value='".$ro->country_id."'>".$ro->country_name."</option>";
							}
						
							echo "
						</select>
					</div>                   
				</div> 

					<div class='row' style='margin-top:10px;'>
						<label class='col-md-4 control-label' style='padding-top:3px;'>Area : </label>
						<div class='col-md-4'>  
							<select name='area' class='form-control' id='idarea' required>

							</select>
						</div>                   
					</div>
				
					
					<!--  new fields ------------------------------------->
					<div id='KCA' style='padding-left:50px;'>

						<div class='row' style='background-color:#f4f4f4;margin-top:10px;'>
						
						<div class='row' style='margin-top:10px;'>
							<label class='col-md-4 control-label' style='padding-top:3px;'>Key Person : </label>
							<div class='col-md-5'>  
								<input type='text' name='keyperson' class='form-control'  id='keyperson' />
							</div>                   
						</div>
						
						<div class='row' style='margin-top:10px;margin-bottom:10px;'>
							<label class='col-md-4 control-label' style='padding-top:3px;'> Mobile: </label>
							<div class='col-md-4'>  
								<input type='text' name='keymobile' class='form-control'  id='keymobile'/>
							</div>                   
						</div>
						</div>
						
						<div class='row' style='background-color:#f4f4f4;margin-top:10px;'>
						<div class='row' style='margin-top:10px;'>
							<label class='col-md-4 control-label' style='padding-top:3px;'>Contact Person : </label>
							<div class='col-md-5'>  
								<input type='text' name='conperson' class='form-control'  id='conperson' />
							</div>                   
						</div>
						
						<div class='row' style='margin-top:10px;margin-bottom:10px;'>
							<label class='col-md-4 control-label' style='padding-top:3px;'> Mobile: </label>
							<div class='col-md-4'>  
								<input type='text' name='conmobile' class='form-control'  id='conmobile' />
							</div>                   
						</div>
						</div>
						
						<div class='row' style='background-color:#f4f4f4;margin-top:10px;'>
						<div class='row' style='margin-top:10px;'>
							<label class='col-md-4 control-label' style='padding-top:3px;'>Account Person : </label>
							<div class='col-md-5'>  
								<input type='text' name='accperson' class='form-control'  id='accperson' />
							</div>                   
						</div>
						
						<div class='row' style='margin-top:10px;margin-bottom:10px;'>
							<label class='col-md-4 control-label' style='padding-top:3px;'> Mobile: </label>
							<div class='col-md-4'>  
								<input type='text' name='accmobile' class='form-control'  id='accmobile' />
							</div>                   
						</div>
					
						</div>
					
					</div>
					<!--  new fields end------------------------------------->
				
					
					<div class='row' style='margin-top:10px;'>
						<label class='col-md-4 control-label' style='padding-top:3px;'>Description : </label>
						<div class='col-md-7'>  
							<textarea name='description' rows='3' class='form-control' required></textarea>
						</div>                   
					</div>

					<!---  attache your proof ----------------------------------->
					
					<div class='form-group' style='margin-top:20px;'> 			
					
					  <div class='row'>
					  <label class='col-md-4 control-label' style='padding-top:3px;'>Upload Id Proof: </label>
					  
					  <div class='control-group col-md-6' id='fields' style='padding-left:20px;'>
						  <div class='controls'>
						   	  <div class='entry input-group col-xs-3' style='margin-top:5px;'>
								<input class='btn btn-primary' name='files[]' type='file'>								
								<span class='input-group-btn'>
							  <button class='btn btn-success btn-add' type='button'>
												<span class='glyphicon glyphicon-plus'></span>
								</button>
								</span>
							  </div>
						 
						</div>
					  </div>
					</div>
					</div>
<!---------------------------------------------------------------->					

					<label style='width:100%;height:1px;'></label>  
					<div class='form-group'> 
					<label class='col-md-4 control-label' style='padding-top:3px;'></label>
						<div class='col-md-7'>
							<input type='submit' class='btn btn-primary' value='Save Details'/>
							</center>
						</div>
					</div> 
					
			</div>  
			</div>
			</div>
			</form>
			</div>
			
<script type='text/javascript'>

$('#mobmessage').hide();
$('#landmessage').hide();
$('#co_agent').hide();	
$('#KCA').hide();
	
$('#idmsg').hide();
if($.trim($('#msg').html())!='')
	{
		swal('Saved',$('#msg').html(),'success');
		$('#msg').html('');
	}
	
$('#msgerr').hide();
if($.trim($('#msgerr').html())!='')
	{
		swal('Try Again',$('#msgerr').html(),'error');
		$('#msgerr').html('');
	}	
	
	
$('#custtype').change(function()
{
	var op=$('#custtype').val();
	if(op==2)
	{
		$('#KCA').show();
	}
	else if(op==3)
	{
		$('#co_agent').show();
		$('#KCA').hide();
	}
	else
	{
		$('#co_agent').hide();
		$('#KCA').hide();
	}
});



		
$('#coagent').change(function()
{
	if($('#coagent').val()!='')
	{
		var aid=$('#coagent').val();
	$('#coagent1').val(aid);
	}
	
});

	   $('#id2').selectize({
	     create: false
	     , sortField: {
	     //field: 'text',
	     direction: 'asc'
	   }
	   , dropdownParent: 'body'
	   });
		  
	
	  
	  function checkdata()
	  {
	    
	    var mob=$('#mobile').val();
	    var landline=$('#landline').val();
	      
	  
	    if(mob.length<10 || mob.length>10)
	    {
	      $('#mobmessage').show('');
	      $('#mobmessage').html('Invalid mobile no,10 digits only.');
	      return false;
	    }
				/*else if(landline.length<11 || landline.length>11)
				{
				 $('#mobmessage').hide('');
				  $('#landmessage').show('');
				  $('#landmessage').html('Invalid mobile no,11 digits only.');
				  return false;
				}*/
	        
	    else
	    {
	    $('#mobmessage').hide();
	      $('#landmessage').hide();
	      $('#mobmessage').html('');
	      $('#landmessage').html('');
	      $(this).submit();
	    }
	  }

  $('#country1').change(function () {
	        var id=$('#country1').val();
			
	        jQuery.ajax({
	        type: 'POST',
	        url: '". base_url(''). "Customer/view_areas',
	    	data:{cid:id},
	        success: function(data)
	    	{
	        	$('#idarea').html(data);
	        }
	        });
	       }); 

		
			</script>
			
			";
    }
	
    public function update_customer()
    {
      $this->load->library('form_validation');
      $this->form_validation->set_rules('name', 'usrname', 'required');
      $this->form_validation->set_rules('email', 'usremail', 'required');
      $this->form_validation->set_rules('mobile', 'usrmobile', 'required');


      if($this->form_validation->run())
      {
        $this->load->model('Model_customer');

        $op1=$this->input->post('opt');  //option for add customer from tripsheet

        $name=$this->input->post('name');
        $email=$this->input->post('email');
        $mobile=$this->input->post('mobile');
        $address=$this->input->post('address');
        $landline=$this->input->post('landline');
        $status=$this->input->post('status');
        $ccode=$this->input->post('custcode');
        $cdate=$this->input->post('custdate');
        $ccat=$this->input->post('category');
        $cagent=$this->input->post('agent');
        $country=$this->input->post('country');
        $area=$this->input->post('area');
        $desc=$this->input->post('description');
        $date=date("Y/m/d");
        date_default_timezone_set('Asia/Kolkata');
        $time=date('h:i:sa');
        $staffid=$this->session->userdata('userid');
        $staffname=$this->session->userdata('name');
      //$status=$this->input->post('status');
        $uid=$this->input->post('uid');
        
		 $gst=$this->input->post('gst');
		 $gstno=$this->input->post('gstno');
		
		
        $userdt = array(
        'customer_code' => $ccode,
        'customer_edate' => $cdate,
		'customer_type'=>$ccat,
		'customer_name'=>$name,
		'customer_address' =>$address,
        'customer_email'=>$email,
        'customer_mobile' =>$mobile,
		'customer_coagent' =>$cagent,
        'customer_landline' =>$landline,
        'customer_country' =>$country,
        'customer_area' =>$area,
        'customer_description' =>$desc,
        'customer_status'=>$status,
        );

        $this->Model_customer->update_customer($uid,$userdt);
		
		
		$cn=$this->db->select("count('*') as cnt")->from('customer_gst')->where('cg_custid',$uid)->get()->row();
		
		if($cn->cnt>0)
		{		
			$gst=array('cg_gst'=>$gst,'cg_gstno'=>$gstno);
			$this->db->where('cg_custid',$uid);
			$this->db->update('customer_gst',$gst);
		}
		else
		{
			$gst=array('cg_custid'=>$uid,'cg_gst'=>$gst,'cg_gstno'=>$gstno);
			$this->Model_customer->insert_gst($gst);
		}
		
        $this->session->set_flashdata('message',  'Customer details Updated..!');
        $userdt1 = array(
          'log_staffid' => $staffid,
          'log_staffname'=>$staffname,
          'log_time'=>$time,
          'log_date'=>$date,
          'log_entryid'=>$uid,
          'log_desc'=>'Customer Details Updated'

          );
        $this->load->model('Model_customer');
        $this->Model_customer->form_insertlog($userdt1);
        
     }  
     redirect('Customer/index');                 
    }


    public function view_areas()
    {
      $id=$this->input->post('cid');
      echo "<option value=''>----------</option>";
      $this->load->model('Model_customer');
      $result=$this->Model_customer->view_area($id);
      foreach ($result as $r)
      {
        echo "<option value='".$r->area_id."'>". $r->area_name."</option>";
      }
    }


    public function edit_customer() 
    {
      $this->load->model('Model_customer');
      $data=$this->session->all_userdata();
      $id=$this->input->post("uid");
      $query = $this->db->select('*')->from('customers')->where('customer_id',$id)->get();
      $row = $query->row();
       $this->load->model('Model_general');
    $results1=$this->Model_general->view_category();
    $results2=$this->Model_general->view_agents();
	
	 $query1 = $this->db->select('*')->from('customer_gst')->where('cg_custid',$id)->get();
     $row1 = $query1->row();
	
      echo "    
      <style>
        /* hide number  spinner*/
        input[type=number]::-webkit-inner-spin-button, 
        input[type=number]::-webkit-outer-spin-button { 
          -webkit-appearance: none; 
          margin: 0; 
        }

      </style>         
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'>&times;</button>
        <h4 class='modal-title' > <font color='blue'>Edit Customer Details</font></h4>
      </div>
      <br>

      <form  class='form-horizontal' role='form' method='post' action='". site_url('customer/update_customer'). "' enctype='multipart/form-data' onsubmit='return checkdata();' >
        <!-- text input -->
        <div class='row'> 

          <div class='col-md-6'>


            <div class='form-group'>
             <label class='col-md-4 control-label' style='padding-top:5px;'>Customer Code</label>
             <div class='col-md-7'>  
              <input type='text' name='custcode' class='form-control' value='$row->customer_code' readonly />
              <input type='hidden' name='uid' class='form-control' value='$id' required />
            </div>                   
          </div>

          <div class='form-group'>
            <label class='col-md-4 control-label' style='padding-top:5px;'>Date</label>
            <div class='col-md-7'>  
              <input type='date' name='custdate' class='form-control' value='$row->customer_edate' required />
            </div>                   
          </div>
		  
		  <div class='form-group'>
            <label class='col-md-4 control-label' style='padding-top:5px;'>Customer Category</label>
            <div class='col-md-7'>  
              <select name='category' class='form-control' id='category'>
			    <option value=''>----------</option>
				<option value='1'";if( $row->customer_type==1) echo "selected"; echo ">Individual</option>
				<option value='2'";if( $row->customer_type==2) echo "selected"; echo ">Corporate</option> 
				<option value='3'";if( $row->customer_type==3) echo "selected"; echo ">Agent</option> ";
                echo "</select>

            </div>                   
          </div>
          
          <!--<div class='form-group'>
            <label class='col-md-4 control-label' style='padding-top:5px;'>Category</label>
            <div class='col-md-7'>  
              <select name='category' class='form-control'>";
               foreach($results1 as $ro){
             echo "<option value='$ro->category_id'";
               if($row->customer_category==$ro->category_id)echo "selected";
                   echo" > $ro->category_name </option>";
                           }  
                           echo "</select>

            </div>                   
          </div> -->
		  
          <div class='form-group' id='agents'>
            <label class='col-md-4 control-label' style='padding-top:5px;'>C/O Agent</label>
            <div class='col-md-7'>  
           <select name='agent' class='form-control' id='agent'>";
               foreach($results2 as $ro){
             echo "<option value='$ro->agent_id'";
               if($row->customer_coagent==$ro->agent_id)echo "selected";
                   echo" > $ro->agent_name </option>";
                           }  
                           echo "</select>
            </div>                   
          </div>
		  
		   <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Name</label>
           <div class='col-md-7'>  
            <input type='text' name='name' class='form-control'  value='$row->customer_name' required />
          </div>                   
        </div>

        <div class='form-group'>
         <label class='col-md-4 control-label' style='padding-top:5px;'>Address </label>
         <div class='col-md-7'>  

           <textarea name='address' rows='3' class='form-control' required>$row->customer_address</textarea>
         </div>                   
       </div>
	   
	   <div class='form-group'>
        <label class='col-md-4  control-label'>Mobile</label>
        <div class='col-md-7'>
          <input type='number' class='form-control' placeholder='Enter mobile no' name='mobile' id='mobile' value='$row->customer_mobile' required>
          <label id='mobmessage' style='color:red;'></label>
        </div>
      </div>

      <div class='form-group'>
        <label class='col-md-4  control-label'>Landline </label>
        <div class='col-md-7'>
          <input type='number' class='form-control' placeholder='Enter landline' name='landline' value='$row->customer_landline' required id='landline' required/>
          <label id='landmessage' style='color:red;'></label>
        </div>
      </div>
	   
	   
     </div>

     <div class='col-md-6'>

      <div class='form-group'>
       <label class='col-md-3 control-label' style='padding-top:5px;'>Email </label>
       <div class='col-md-8'>  

        <input type='email' name='email' class='form-control'  value='$row->customer_email' required/>
      </div>                   
    </div>
	
	<div class='row' style='margin-top:20px;'>
			<label class='col-md-3  control-label'>Gst% : </label><label style='margin-top:5px;'>Eg: 18</label>
		<div class='col-md-3'>
			<input type='number' class='form-control'  name='gst' value='$row1->cg_gst' required/>
		</div>
	</div>
					
	<div class='row' style='margin-top:10px;'>
		<label class='col-md-3 control-label' style='padding-top:3px;'>Gst Number : </label>
		<div class='col-md-8'>  
			<input type='text' name='gstno'  class='form-control' value='$row1->cg_gstno' required/>
		</div>                   
	</div>
		
    <div class='form-group' style='margin-top:20px;'>
     <label class='col-md-3 control-label' style='padding-top:5px;'>Country</label>
     <div class='col-md-8'> 
       <select name='country' class='form-control' id='country1'>
         <option value=''>----------</option>
         ";
         $resulte2=$this->Model_customer->view_country();

         foreach($resulte2 as $ro){
             echo "<option value='$ro->country_id'";
               if($row->customer_country==$ro->country_id)echo "selected";
                   echo" > $ro->country_name </option>";
                           }  
                           echo "</select>
      </div>                   
    </div>

    <div class='form-group'>
      <label class='col-md-3 control-label' style='padding-top:5px;'>Area</label>
      <div class='col-md-8'>  
        <select name='area' class='form-control' id='idarea' required>
         <option value=''>----------</option>
         ";
         $resulte3=$this->Model_general->view_area();

         foreach($resulte3 as $ro){
             echo "<option value='$ro->area_id'";
               if($row->customer_area==$ro->area_id)echo "selected";
                   echo" > $ro->area_name </option>";
                           }  
                           echo "</select>
       
      </div>                   
    </div>

    <div class='form-group'>
     <label class='col-md-3 control-label' style='padding-top:5px;'>Description</label>
     <div class='col-md-8'>  
      <textarea name='description' rows='3' class='form-control' required>$row->customer_description</textarea>
    </div>                   
    </div>

     <div class='form-group'>
      <label class='col-md-3  control-label'>Status </label>
      <div class='col-md-8'>
        <select type='text' name='status' class='form-control'>";

         echo '<option value="0" ';if($row->customer_status=='0')echo "selected"; echo'>Inactive</option>
         <option value="1"';if($row->customer_status=='1')echo "selected"; echo ">Active</option>
       </select>
     </div>                     
    </div>  

    </div>  
    </div>

    <label style='width:100%;height:1px;background-color:#e4e4e4;'></label>  
    <div class='form-group'> 
      <div class='col-md-12'>
        <center><input type='submit' class='btn btn-primary' value='Update Details'/>
          <button type='button' class='btn btn-default' data-dismiss='modal' style='margin-left:20px;'>Close</button>
        </center>
      </div>
    </div>     
    <br>
    </form>
    <!--<div class='modal-footer'>

    </div>  -->

    <script type='text/javascript'>

      $('#mobmessage').hide();
      $('#landmessage').hide();
	  
	  $('#cust_type').hide();
	  
      function checkdata()
      {

        var mob=$('#mobile').val();
        var landline=$('#landline').val();

        if(mob.length<10 || mob.length>10)
        {
          $('#mobmessage').show('');
          $('#mobmessage').html('Invalid mobile no,10 digits only.');
          return false;
        }
        else if(landline.length<11 || landline.length>11)
        {
         $('#mobmessage').hide('');
         $('#landmessage').show('');
         $('#landmessage').html('Invalid mobile no,11 digits only.');
         return false;
       }

       else
       {
        $('#mobmessage').hide();
        $('#landmessage').hide();
        $('#mobmessage').html('');
        $('#landmessage').html('');
        return true;
      }
    }

    /* get area of selected country  */

    $('#country1').change(function () {
      /*alert($('#country1').val());*/
      var id=$('#country1').val();
        $('#idarea').empty();
 jQuery.ajax({
        type: 'POST',
        url: '". base_url('Customer/view_areas1/') ."/'+id,
        data:{id:id},
           dataType: 'html',
        success: function(data)
        {
        $('#idarea').append(data);
        }
      });
    }); 

	$('#category').change(function()
	{
	var op1=$('#category').val();
		if(op1=='3')
		{
			$('#agents').show();
		}
		else
		{
			$('#agents').hide();
		}
		
	});


    </script> ";
    }
	
	
  public function view_areas1()
    {
       $opt="";
   //$mid=$this->input->post('id');
   $mid=$this->uri->segment(3);
   $this->load->model('Model_general');
   $res=$this->Model_general->view_areas1($mid);
   $opt.="<option value=''>---</option>";  
   foreach($res as $r)
   {
   $opt.="<option value='".$r->area_id."'>".$r->area_name."</option>";
   }
   echo $opt;
 }

    public function view_customer() 
    {

      $data=$this->session->all_userdata();
      $id=$this->input->post('id');
       $query = $this->db->select('*')->from('customers')->join('country','country.country_id=customers.customer_country','inner')->join('area','area.area_id=customers.customer_area','inner')->join('category','category.category_id=customers.customer_category','inner')->join('agents','agents.agent_id=customers.customer_coagent','inner')->where('customer_id',$id)->get();
      $row = $query->row();

      echo "
      <style>
        /* hide number  spinner*/
        input[type=number]::-webkit-inner-spin-button, 
        input[type=number]::-webkit-outer-spin-button { 
          -webkit-appearance: none; 
          margin: 0; 
        }

      </style>         
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'>&times;</button>
        <h4 class='modal-title' > <font color='blue'>Customer Details</font></h4>
      </div>
      <br>
      <form  class='form-horizontal' role='form' method='post' action='' >

        <!-- text input -->
        <div class='row'> 

          <div class='col-md-6'>


            <div class='form-group'>
             <label class='col-md-4 control-label' style='padding-top:5px;'>Customer Code</label>
             <div class='col-md-7'>  
              <input type='text' name='custcode' class='form-control'  value='$row->customer_code' readonly />
            </div>                   
          </div>

          <div class='form-group'>
            <label class='col-md-4 control-label' style='padding-top:5px;'>Date</label>
            <div class='col-md-7'>  
              <input type='text' name='custdate' class='form-control' value='$row->customer_edate' readonly  />
            </div>                   
          </div>

          <div class='form-group'>
            <label class='col-md-4 control-label' style='padding-top:5px;'>Category</label>
            <div class='col-md-7'>  
             <input type='text' name='custdate' class='form-control' value='$row->category_name'  readonly />

           </div>                   
         </div>

         <div class='form-group'>
          <label class='col-md-4 control-label' style='padding-top:5px;'>C/O Agent</label>
          <div class='col-md-7'>  
            <input type='text' name='coagent' class='form-control' value='$row->agent_name'  readonly />
          </div>                   
        </div>    

        <div class='form-group'>
         <label class='col-md-4 control-label' style='padding-top:5px;'>Name</label>
         <div class='col-md-7'>  
          <input type='text' name='name' class='form-control'  value='$row->customer_name'  readonly />
        </div>                   
      </div>

      <div class='form-group'>
        <label class='col-md-4 control-label' style='padding-top:5px;'>Address </label>
        <div class='col-md-7'>  
          <textarea name='address' rows='3' class='form-control'  readonly>$row->customer_address</textarea>
        </div>                   
      </div>
    </div>

    <div class='col-md-6'>

      <div class='form-group'>
        <label class='col-md-3  control-label'>Mobile</label>
        <div class='col-md-8'>
          <input type='number' class='form-control' placeholder='Enter mobile no' name='mobile' id='mobile' value='$row->customer_mobile'   readonly>
        </div>
      </div>

      <div class='form-group'>
        <label class='col-md-3  control-label'>Landline </label>
        <div class='col-md-8'>
          <input type='number' class='form-control' placeholder='Enter landline' name='landline' value='$row->customer_landline' id='landline'  readonly/>
        </div>
      </div>
      <div class='form-group'>
       <label class='col-md-3 control-label' style='padding-top:5px;'>Email </label>
       <div class='col-md-8'>  

        <input type='email' name='email' class='form-control'  value='$row->customer_email'  readonly/>
      </div>                   
    </div>

    <div class='form-group'>
     <label class='col-md-3 control-label' style='padding-top:5px;'>Country</label>
     <div class='col-md-8'> 
       <input type='email' name='email' class='form-control'  value='$row->country_name'  readonly/>
     </div>                   
    </div>

    <div class='form-group'>
      <label class='col-md-3 control-label' style='padding-top:5px;'>Area</label>
      <div class='col-md-8'>  
        <input type='email' name='email' class='form-control'  value='$row->area_name'  readonly/>
      </div>                   
    </div>

    <div class='form-group'>
     <label class='col-md-3 control-label' style='padding-top:5px;'>Description</label>
     <div class='col-md-8'>  
      <textarea name='description' rows='3' class='form-control'  readonly>$row->customer_description</textarea>
    </div>                   
    </div>

    <!--    <div class='form-group'>
      <label class='col-md-3  control-label'>Status </label>
      <div class='col-md-8'>
        <select type='text' name='status' class='form-control'  readonly>";

         echo '<option value="0" ';if($row->customer_status=='0')echo "selected"; echo'>Inactive</option>
         <option value="1"';if($row->customer_status=='1')echo "selected"; echo ">Active</option>
       </select>
     </div>                     
    </div>   -->

    </div>  
    </div>

    <label style='width:100%;height:1px;background-color:#e4e4e4;'></label>  
    <div class='form-group'> 
      <div class='col-md-12'>
       <center> <button type='button' class='btn btn-default' data-dismiss='modal' style='margin-left:20px;'>Close</button></center>

     </div>
    </div>     
    <br>

    </form>    
    <!-- <div class='modal-footer'>

    </div>  -->
    ";

    }




    public function Select_customerID()
    {
     $id=$this->input->post('sid');
     $cdata = array(
      'customerid'=>$id,
      );
     $this->session->set_userdata($cdata); 
    }

	
    public function del_entry()
    {

      $id=$this->uri->segment(3);
      //$id=$this->input->post('uid');
      $date=date("Y/m/d");
      date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa');
      $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
      $this->load->model('Model_customer');
      $result=$this->Model_customer->delete_customer($id);
      if($result)
      {
        $this->session->set_flashdata('message', 'Customer details has been deleted..!');
        $userdt1 = array(
          'log_staffid' => $staffid,
          'log_staffname'=>$staffname,
          'log_time'=>$time,
          'log_date'=>$date,
          'log_entryid'=>$id,
          'log_desc'=>'Customer Details Deleted'

          );
        $this->load->model('Model_customer');
        $this->Model_customer->form_insertlog($userdt1);
        redirect('Customer/index');
      }
      else
      {
        $this->session->set_flashdata('message1', 'Data missing, try again');
        redirect('Customer/index');
      }
    }

    public function share_customer()
    {
     $data=$this->session->all_userdata();
     $id=$this->input->post('id');
     echo " <style> .tx{text-align:right;} .fnt{text-align:right;font-size:18px;}</style>            
     <div class='modal-header'>
      <button type='button' class='close' data-dismiss='modal'>&times;</button>
      <h4 class='modal-title' > <font color='blue'>Share</font></h4>
    </div>
    <br>

    <form  class='form-horizontal' role='form' method='post' action='". site_url('Pdf/share_customer'). "' enctype='multipart/form-data'>

      <div class='row'>
        <div class='col-md-12'>

         <div class='form-group'>
          <label class='col-md-2 control-label' style='padding-top:10px;'>Email to </label>
          <div class='col-md-8'>
            <input type='hidden' name='id'  value='$id'/>
            <input type='email' class='form-control' name='email' value='' placeholder='Enter email'> 
          </div>
        </div>
      </div>
    </div> 
    <div class='modal-footer'>
      <input type='submit' class='btn btn-primary' value='Share'/>
      <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
    </div>     

    </form>   
    ";

    }

    public function save_customer()
    {


      $this->load->model('Model_customer');
      $this->load->library('pdf1');

      
      $pdf=$this->pdf1->load();
      $id=$this->input->post('uid');
      $this->load->model('Model_customer');
      $data['results1']=$this->Model_customer->view_customer1($id);
      ini_set('memory_limit','256M');
      $html=  $this->load->view('pdf/customer_pdf',$data,true);
      $pdf->WriteHtml($html);
      $output='customerid_'.$id.'_'.date('Y_m_d_H_i_s').'_.pdf';
    // $pdf->Output("$output",'D');
      $pdf->Output('upload/savedpdf/'.$output,'F');
      $this->session->set_flashdata('message',  'Customer Details Saved Successfully..!');
      redirect('Customer/index');
      exit();

    }

    }