<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {

function __construct(){
parent::__construct();
$user = $this->session->userdata('userid');
    if (!isset($user))
     { 
        redirect('/');
     } 
     else
     { 
      return true;
     }
$this->load->helper(array('form', 'url'));
$this->load->model('Model_staff');
}

public function new_staff()
{
  $this->load->model('Model_general');
   $this->load->model('Model_staff');
  $data['result']=$this->Model_general->view_role();
  $data['result1']=$this->Model_general->view_profession();
  $this->load->view('admin/add_staff',$data);
}

public function add_new_staff()
{
	        
    $this->load->model('Model_staff');
	
	if(isset($_FILES['photo']) && $_FILES['photo']['size']!=0)
	{
	    $config['upload_path'] ='./upload/staff_data';
        $config['allowed_types'] = 'jpg|png|jpe|gif';
        $this->load->library('upload', $config);
	    
		if (!$this->upload->do_upload('photo'))
            {
             	$this->session->set_flashdata('message',$this->upload->display_errors());
                $this->load->view('admin/view_staff');
            }
        else
        {
		  $data = array('upload_photo'=> $this->upload->data());
		 //$file_name=$data['upload_data']['file_name'];
		  $fphoto=base_url('/upload/staff_data')."/".$data['upload_photo']['file_name'];
		}	
	}

		if (!empty($_FILES['files']['name']))
		{
			$filesCount = count($_FILES['files']['name']);
			
			$fname="";	
			for($i = 0; $i < $filesCount; $i++)
			{
                $_FILES['ivf']['name'] = $_FILES['files']['name'][$i];
                $_FILES['ivf']['type'] = $_FILES['files']['type'][$i];
                $_FILES['ivf']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['ivf']['error'] = $_FILES['files']['error'][$i];
                $_FILES['ivf']['size'] = $_FILES['files']['size'][$i];
				
			    $uploadPath = 'upload/staff_data/';

                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|jpe';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				
                if($this->upload->do_upload('ivf'))
				{
                 $fileData=array('upload_data'=> $this->upload->data());
				 
						if($i==$filesCount-1)
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'];
						 }
						 else
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'].",";
						 }
				}
				else
				{
					
					echo $this->upload->display_errors();
					
				}
			}
		} 
	
		$sdate=$this->input->post('sdate');
        $srole=$this->input->post('srole');
		$name=$this->input->post('name');
		$address=$this->input->post('address');
		$position=$this->input->post('position');
        $bdate=$this->input->post('bdate');
        $gender=$this->input->post('gender');
        $bgroup=$this->input->post('bgroup');
		$mobile=$this->input->post('mobile');
		$email=$this->input->post('email');
		$password=$this->input->post('password');
		$bsalary =$this->input->post('bsalary');
		$aadhar =$this->input->post('aadhar');
        $pancard=$this->input->post('pancard');
        $bankname=$this->input->post('bankname');
        $ifsc=$this->input->post('ifsc');
		$account=$this->input->post('account');
		$guardian=$this->input->post('guardian');
        $contactno=$this->input->post('contactno');
				
        $staffid=$this->session->userdata('userid');
        $staffname=$this->session->userdata('name');
	    	  
        $date=date("Y-m-d");
        date_default_timezone_set('Asia/Kolkata');
        $time=date('h:i:sa'); 
		
		$d2=explode("-",$sdate);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
					
			$userdt = array(
					'staff_startdate' => $dt2,
					'staff_name' => $name,
					'staff_address' => $address,
					'staff_role' => $srole,
					'staff_profession' =>$position,
					'staff_dob' => $bdate,
					'staff_gender' => $gender,
					'staff_bloodgroup' => $bgroup,
					'staff_mobile' =>$mobile,
					'staff_email' =>$email,
					'staff_aadharcard' => $aadhar,
					'staff_password'=>$password,
					'staff_basicsalary'=>$bsalary,
					'staff_pancard'=>$pancard,
					'staff_bankname'=>$bankname,
					'staff_bankaccount' =>$account,
					'staff_ifsc' => $ifsc,
					'staff_image'=>$fphoto,
					'staff_certificate'=>$fname,
					'staff_guardian' => $guardian,
					'staff_contactno' => $contactno,
					'staff_status'=>'1'
					   );
  
					$this->Model_staff->form_insert($userdt);
            $uid = $this->db->insert_id();
            $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Staff Details Added'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
		  if($srole!='4')
		  {
          $this->session->set_userdata("staff_id",$userdt['staff_code']);
		  redirect('Staff/menu/'.$stcode);
		  }
		  else
		  {
		 
			$this->session->set_flashdata('message',  '1#Details successfully added.!');
			redirect('Staff/new_staff');
		  }
        
	/*}
	else
	{
		 $this->session->set_flashdata('message',  '4#Details missing..!');
		 redirect('Staff/new_staff');
	}*/
}


public function menu()
{

  /*$user = $this->session->userdata('user_type');
  if ($user != "admin")
   { 
      redirect('/');
   }*/ 

$scode=$this->uri->segment(3);
$this->load->model('Model_staff');
$data['result'] = $this->Model_staff->view_menucategory();

$this->load->view('admin/menu11',$data);
}


	
public function add_staff() 
  {
 echo " 
     <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>             
    <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>New Staff Details</font></h4>
    </div>
      <form  onsubmit='return checkdata();' class='form-horizontal' role='form' method='post' action='". site_url('Staff/add_new_staff'). "' enctype='multipart/form-data'>
                    <!-- text input -->
					
<div class='box-body'>
<div class='row'>
<div class='col-md-6'>

		 
                 <div class='form-group'>
                   <label class='col-md-3 control-label'>Staff Code</label>
                        <div class='col-md-8'>
							<input type='text' class='form-control' name='stcode' required>
            
				 
				 <div class='form-group'>
                   <label class='col-md-3 control-label'>Staff Code</label>
                        <div class='col-md-8'>
							<input type='text' class='form-control' name='stcode' required>
                 </div>
				 
				 <div class='form-group'>
                   <label class='col-md-3 control-label'>Staff Code</label>
                        <div class='col-md-8'>
							<input type='text' class='form-control' name='stcode' required>
                 </div>
				 
                </div> 
			</div class='col-md-4'>
			</div>
</div>

<div class='col-md-5'>
</div>
</div>










 <div class='row'>
 <div class='col-md-6' style='border-right:1px solid #e4e4e4;'>				 
                 <div class='form-group'>
                   <label class='col-md-3 control-label'>Staff Code</label>
                        <div class='col-md-8'>
							<input type='text' class='form-control' name='stcode' required>
                      </div>
                      </div> 

                   <div class='form-group'>
                   <label class='col-md-3 control-label'>Start Date</label>
                        <div class='col-md-8'>
							<input type='date' class='form-control' name='jdate' required>
                      </div>
                      </div> 
				  

                 <div class='form-group'>
                   <label class='col-md-3 control-label'> Name</label>
                        <div class='col-md-8'>
							<input type='text' class='form-control' name='name' required>
                      </div>
                      </div>
					  
					<div class='form-group' >
					  <label class='col-md-3 control-label'>Photo</label>
                       <div class='col-md-8'>
                          <div class='fileinput fileinput-new' data-provides='fileinput'>
                             <div class='fileinput-new thumbnail' style='max-width: 145px; max-height: 152px;''>
                                 <img src='".base_url('assets/dist/img/user.jpg')."' alt='' /> </div>
                                 <div class='fileinput-preview fileinput-exists thumbnail' style='max-width: 135px; max-height: 140px;'> </div>
                             <div>
                              <span class='btn btn-default btn-file'>
                              <span class='fileinput-new'> Select Photo </span>
                              <span class='fileinput-exists'> Change </span>
                              <input type='file' name='photo' > </span>
                              <a href='javascript:;' class='btn btn-default fileinput-exists' data-dismiss='fileinput'> Remove </a>
                         </div>
                        </div>
                    </div>
					</div>
                
                <div class='form-group'>
                 <label class='col-md-3 control-label'>Address</label>
                    <div class='col-md-8'>
                       <textarea rows=2 cols=50 class='form-control'  name='address'  required></textarea>
                       </div>
                  </div>
				  <div class='form-group'>
					 <label class='col-md-3 control-label'>Profession</label>
					 <div class='col-md-8'>
						 <input type='text' class='form-control' name='position' required>
					 </div>
					 </div>

</div>				  

<div class='col-md-6'> 
					

					 <div class='form-group'>
						 <label class='col-md-3 control-label'>Mobile</label>
					 <div class='col-md-8'>
														   
						 <input type='number' class='form-control' name='mobile' id='mobile' required>
						 <label id='mobmessage' style='color:red;'></label>                              
					 </div>
					 </div>
                        
                        <div class='form-group'>
                      <label class='col-md-3 control-label'>Phone</label>
                        <div class='col-md-8'>
                                                   
						<input type='number' class='form-control' name='phone' id='landline' required>
                        <label id='landmessage' style='color:red;'></label>                            
                        </div>
                      </div>

					  <div class='form-group'>
                      <label class='col-md-3 control-label'>Email</label>
                        <div class='col-md-8'>
							<input type='email' class='form-control' name='email' required>
                        </div>
                      </div>

                       <div class='form-group'>
                      <label class='col-md-3 control-label'>UserName</label>
                        <div class='col-md-8'>
                          <input type='text' class='form-control' name='username' required>
                       </div>
                        </div>

                     <div class='form-group'>
                      <label class='col-md-3 control-label'>Password</label>
                        <div class='col-md-8'>
							<input type='text' class='form-control' name='password' required>
                        </div>
                     </div>
					 		 
					 
					 <div class='form-group'>
                      <label class='col-md-3 control-label'>Basic Salary</label>
                        <div class='col-md-8'>
							<input type='text' class='form-control' name='bsalary' required>
                        </div>
                     </div>
					 
					 
					 <div class='form-group'>
                      <label class='col-md-3 control-label'>Commission</label>
                        <div class='col-md-8'>
							<input type='text' class='form-control' name='commission' required>
                        </div>
                     </div>
<!--<label style='color:blue;font-size:13px;margin-left:50px;'> Scan Your (Certificates,Licence,Id Card) and create a Zip file.<br> Then select file and upload. </label>-->

<label style='color:blue;font-size:13px;margin-left:50px;'> Upload Your scanned copy of Certificates,Licence and Id Card.</label>

		<div class='form-group' >
             <label class='col-md-3 control-label' >Select File : </label>
                <div class='col-md-8' style='padding-top:3px;'>
                   <div class='fileinput fileinput-new' data-provides='fileinput'>
                        <span class='btn btn-primary btn-file'>
						<span class='fileinput-new'> Select file </span>
                        <span class='fileinput-exists'> Change </span>
						<input type='file' name='certificate' id='f' required> </span>
						<span class='fileinput-filename'> </span> &nbsp;
						<a href='javascript:;'' class='close fileinput-exists' data-dismiss='fileinput'> </a>
                   </div>
				</div>
        </div>
								 	 
                    <!--   <div class='form-group'>
                         <label class='col-md-3 control-label'>Status</label>
							<div class=' col-md-8'>
						<select class='form-control' name='status'>
							<option value='1'>Active</option>
							<option value='0'>Inactive</option>
						</select>
					</div>
					</div> -->
</div>
</div>
<label style='width:100%;height:1px;background-color:#e4e4e4;'></label>


				  <div class='form-group'>
					   <div class='col-md-12'>
							<center><button type='submit' class='btn btn-primary'>Save Details</button>
								<button type='button' class='btn btn-default' data-dismiss='modal' style='margin-left:20px;'>Close</button>
							</center>
                       </div>
                  </div> 
</div>
</form>
	   
	   
    <script type='text/javascript'>
     $('#mobmessage').hide();
     $('#landmessage').hide();  
  function checkdata()
  {
    
    var mob=$('#mobile').val();
    var landline=$('#landline').val();
      
  
    if(mob.length<10 || mob.length>10)
    {
	  $('#mobmessage').show();
      $('#mobmessage').html('Invalid mobile no,10 digits only.');
      return false;
    }
    else if(landline.length<11 || landline.length>11)
    {
	  $('#mobmessage').hide();
      $('#landmessage').show();
      $('#landmessage').html('Invalid mobile no,11 digits only.');
      return false;
    }
    
    
    else
    {
	  $('#mobmessage').hide();
      $('#landmessage').hide();
      $('#mobmessage').html('');
      $('#landmessage').html('');
      return true;
    }
  }

</script> 
    ";
  }

  
public function view_staff()
{
$this->load->view('admin/view_staff');
}

public function staff_ajax()
{

  $this->load->model('Model_staff');
  $results=$this->Model_staff->view_staff();
  $data1 = array();
  foreach($results  as $r) 
  {
	  if($r->staff_role=='4')
	  {
		  $menu="";
	  }
	  else
	  {
		$menu = "<a href='".base_url()."Staff/menu/".$r->staff_id."'  data-toggle='modal' class='edit'><center><button class='btn btn-primary' style='margin-top: 5px;' data-title='Delete' data-toggle='modal' ><span >Set Menu</span></button></center></a>";
	  }
	 
  
  $edit="<a href='".base_url()."Staff/edit_staff/".$r->staff_id."' data-toggle='modal' class='edit'><center><button class='btn btn-warning btn-xs' style='padding-left:11px;padding-right:11px;'>Edit</button></center></a>";
  $change="<a href='#myModal2' id='$r->staff_id' data-toggle='modal' class='change' ><center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
  $mm="<center><button class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' >Delete</button></center>";
  $del=anchor('Staff/del_entry/'.$r->staff_id, $mm, array('id' =>'del_conf'));
  $img="<img src='".$r->staff_image."' width='70px' height='75px'>";
    
    if($r->staff_status==1)
    {
     $st="<font color='green'>Active</font>";
	 $na=$r->staff_name;
   }
   else
   {
     $st="<font color='red'>Resigned</font>";
	 $na="<font color='red'>".$r->staff_name."</font>";
   }
   array_push($data1, array(
    "edit"=>$edit.$del.$menu,
    "id"=>"$r->staff_id",
    "photo"=>$img,
    "jdate"=>date_format(date_create($r->staff_startdate),"d-m-Y"),
    "name"=>$na,
    "address"=>"$r->staff_address<br><b>Guardian:&nbsp;&nbsp; </b>".$r->staff_guardian."<br><b>Phone:&nbsp;&nbsp;</b>".$r->staff_contactno,
    "position"=>"<b>Desgnation: </b>".$r->profession_name."<br><b>Mobile: </b>".$r->staff_mobile."<br><b>Email: </b>".$r->staff_email."<br><b>Salary :</b> ".$r->staff_basicsalary,
                            //"username"=>"$r->staff_username",
    "status"=>$st
    ));
 }
 echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}


public function edit_staff() 
{
  $this->load->model('Model_general');
  $id=$this->uri->segment(3, 0);
  $query = $this->db->select('*')->from('staffregistration')->join('role', 'role.role_id=staffregistration.staff_role','inner')->join('profession', 'profession.profession_id=staffregistration.staff_profession','inner')->where('staff_id',$id)->get();
  $data['resulte'] = $query->result();
  $data['result']=$this->Model_general->view_role();
  $data['result1']=$this->Model_general->view_profession();
  $this->load->view('admin/edit_staff',$data);
}
	
public function update_staff()
{

    $this->load->model('Model_staff');

	$fphoto=$this->input->post('simage');
	
//upload staff photo	
	if(isset($_FILES['photo']) && $_FILES['photo']['size']!=0)
	{
	    $config['upload_path'] ='./upload/staff_data';
        $config['allowed_types'] = 'jpg|png|jpe|gif|zip';
        $this->load->library('upload', $config);
	    
		if (!$this->upload->do_upload('photo'))
            {
             	$this->session->set_flashdata('message',$this->upload->display_errors());
                $this->load->view('admin/view_staff');
            }
        else
        {
		  $data = array('upload_photo'=> $this->upload->data());
		 //$file_name=$data['upload_data']['file_name'];
		  $fphoto=base_url('/upload/staff_data')."/".$data['upload_photo']['file_name'];
		}	
	}
//-----------------------------
				
		$id=$this->input->post('staff_id');
        $sdate=$this->input->post('sdate');
        $srole=$this->input->post('srole');
        $name=$this->input->post('name');
        $address=$this->input->post('address');
        $position=$this->input->post('position');
        $bdate=$this->input->post('bdate');
        $gender=$this->input->post('gender');
        $bgroup=$this->input->post('bgroup');
        $mobile=$this->input->post('mobile');
        $email=$this->input->post('email');
       // $password=$this->input->post('password');
        $bsalary =$this->input->post('bsalary');
        $aadhar =$this->input->post('aadhar');
        $pancard=$this->input->post('pancard');
        $bankname=$this->input->post('bankname');
        $ifsc=$this->input->post('ifsc');
        $account=$this->input->post('account');
		$guardian=$this->input->post('guardian');
		$contactno=$this->input->post('contactno');
        $status=$this->input->post('status');
		
        $date=date("Y/m/d");
		date_default_timezone_set('Asia/Kolkata');
		$time=date('h:i:sa');
		$staffid=$this->session->userdata('userid');
		$staffname=$this->session->userdata('name');
  

			$newdt = array(
			'staff_startdate' => $sdate,
            'staff_name' => $name,
            'staff_address' => $address,
            'staff_role' => $srole,
            'staff_profession' =>$position,
            'staff_dob' => $bdate,
            'staff_gender' => $gender,
            'staff_bloodgroup' => $bgroup,
            'staff_mobile' =>$mobile,
            'staff_email' =>$email,
            'staff_aadharcard' => $aadhar,
            'staff_basicsalary'=>$bsalary,
            'staff_pancard'=>$pancard,
            'staff_bankname'=>$bankname,
            'staff_bankaccount' =>$account,
            'staff_ifsc' => $ifsc,
            'staff_image'=>$fphoto,
            'staff_guardian'=>$guardian,
			'staff_contactno'=>$contactno,
			'staff_status'=>$status
					   );
 			$this->Model_staff->update_staff_profile($id,$newdt);
  		   
            $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$id,
            'log_desc'=>'Staff Details Updated'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
		  $this->session->set_flashdata('message',  'Details updated..!');
	  	  redirect('Staff/view_staff');
	   
    }
	
	
	
public function view_staff_details() 
  {
	  
	    $data=$this->session->all_userdata();
        $id=$this->input->post('staff_id');
        $query = $this->db->select('*')->from('staffregistration')->where('staff_id',$id)->get();
        $row = $query->row();
    
	  
	  
 echo " 
     <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>             
    <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Staff Details : (ID-$row->staff_id)</font></h4>
    </div>
      <form  onsubmit='return checkdata();' class='form-horizontal' role='form' method='post' action='". site_url('Staff/add_new_staff'). "' enctype='multipart/form-data'>
                    <!-- text input -->
					
<div class='box-body'>
 <div class='row'>
 <div class='col-md-6' style='border-right:1px solid #e4e4e4;'>	

					<div class='form-group' >
					  <label class='col-md-3 control-label'></label>
                       <div class='col-md-8'>
                             <img src='".$row->staff_image."' style='max-width: 145px; max-height: 152px;' />
 							 </div>
                       </div>
				 
                 <div class='form-group'>
                   <label class='col-md-3 control-label'>Staff ID : </label>
                        <div class='col-md-8'>
						<input type='text' class='form-control' value='$row->staff_id'>
					   </div>
                      </div> 

                   <div class='form-group'>
                   <label class='col-md-3 control-label'>Start Date :</label>
                        <div class='col-md-8'>
						<input type='text' class='form-control' value='$row->staff_joindate'>
	
                      </div>
                      </div> 
				  

                 <div class='form-group'>
                   <label class='col-md-3 control-label'> Name :</label>
                        <div class='col-md-8'>
						<input type='text' class='form-control' value='$row->staff_name'>
                      </div>
                      </div>
					  
					
                

</div>				  

<div class='col-md-6'> 
					
				<div class='form-group'>
                 <label class='col-md-3 control-label'>Address :</label>
                    <div class='col-md-8'>
					<textarea rows=2 cols=50 class='form-control'>$row->staff_address</textarea>
                       </div>
                  </div>
				  <div class='form-group'>
					 <label class='col-md-3 control-label'>Profession :</label>
					 <div class='col-md-8'>
					 <input type='text' class='form-control' value='$row->staff_profession'>
					 </div>
					 </div>
					 
					 <div class='form-group'>
						 <label class='col-md-3 control-label'>Mobile :</label>
					 <div class='col-md-8'>
						<input type='text' class='form-control' value='$row->staff_mobile'>  
				                      
					 </div>
					 </div>
                        
                        <div class='form-group'>
                      <label class='col-md-3 control-label'>Phone :</label>
                        <div class='col-md-8'>
						<input type='text' class='form-control' value='$row->staff_landline'>
             
                        </div>
                      </div>

					  <div class='form-group'>
                      <label class='col-md-3 control-label'>Email :</label>
                        <div class='col-md-8'>
						<input type='text' class='form-control' value='$row->staff_email'>
                        </div>
                      </div>

					 
					 <div class='form-group'>
                      <label class='col-md-3 control-label'>Basic Salary :</label>
                        <div class='col-md-8'>
						<input type='text' class='form-control' value='$row->staff_basicsalary'>
                        </div>
                     </div>
					 
					 
					 <div class='form-group'>
                      <label class='col-md-3 control-label'>Commission :</label>
                        <div class='col-md-8'>
						<input type='text' class='form-control' value='$row->staff_commission'>
                        </div>
                     </div>
<!--<label style='color:blue;font-size:13px;margin-left:50px;'> Scan Your (Certificates,Licence,Id Card) and create a Zip file.<br> Then select file and upload. </label>-->

								 	 
                    <!--   <div class='form-group'>
                         <label class='col-md-3 control-label'>Status :</label>
							<div class=' col-md-8'>
					<label class='control-label' style='color:blue;'>$row->staff_status</label>
					</div>
					</div> -->
</div>
</div>
<label style='width:100%;height:1px;background-color:#e4e4e4;'></label>

				  <div class='form-group'>
					   <div class='col-md-12'>
							<center>
								<button type='button' class='btn btn-default' data-dismiss='modal' style='margin-left:20px;'>Close</button>
							</center>
                       </div>
                  </div> 
</div>
</form>
</div> ";
}
	
public function change_password() 
  {
    $data=$this->session->all_userdata();
    $id=$this->input->post('staff_id');
    $query = $this->db->select('*')->from('staffregistration')->where('staff_id',$id)->get();
    $row = $query->row();
     
    echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Change  Password</font></h4>
    </div>
    <br>

           <form onsubmit='return checkdata();'  class='form-horizontal' role='form' method='post' action='". site_url('Staff/update_password'). "' enctype='multipart/form-data'>
                    <!-- text input -->
                    
                    <div class='form-group'>
                     <label class='col-md-4 control-label'>Name : </label>
                    <div class='col-md-7'>  
          <input type='hidden' name='staff_id'  value='$row->staff_id'/>
          <input type='text' name='uname' class='form-control'  id='mask_date2' value='$row->staff_name'/ readonly>
          </div>                   
          </div>
                    
            
	     <!-- <div class='form-group'>
          <label class='col-md-4  control-label'>EmailUsername : </label>
          <div class='col-md-7'>
          <input type='text' class='form-control' placeholder='Enter mobile no' name='usrname' value='$row->staff_email' readonly>
            </div>
          </div>  -->
          
          <div class='form-group'>
          <label class='col-md-4  control-label'>Enter New Password : </label>
          <div class='col-md-7'>
          <input type='password' class='form-control'  name='upass' id='upass' required/>

          </div>
          </div>
	   
	      <div class='form-group'>
          <label class='col-md-4  control-label'>Re-enter Password : </label>
          <div class='col-md-7'>
          <input type='password' class='form-control' name='rupass' id='rpass' required/>
            <label id='rmessage' style='color:red;'></label>
          </div>
          </div>
                <div class='form-group'> 
                <div class='col-md-4'>
                </div>
                <div class='col-md-7'>
                <input type='submit' class='btn btn-primary' value='Change Password'/>
				 <button type='button' class='btn btn-default' data-dismiss='modal' style='margin-left:20px;'>Close</button>
                </div>
                </div>  
<br>				
           </form>

            <script type='text/javascript'>
     function checkdata()
     {

      var rpass=$('#rpass').val();
      var upass=$('#upass').val();

      
      if(rpass!=upass)
      {
        $('#rmessage').show('');
        $('#rmessage').html('New Password Not Match');
        return false;
      }
     

     else
     {
      $('#rmessage').hide();
      $('#rmessage').html('');
     
      return true;
    }
    }
    </script>
	<!-- <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div> --> ";
 }	


public function update_password()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('upass', 'upass', 'required');
$this->form_validation->set_rules('rupass', 'rupass', 'required');

  if($this->form_validation->run())
    {
    $upass1=$this->input->post('upass');
    $upass2=$this->input->post('rupass');
    $upass3=md5($this->input->post('rupass'));
    $uid=$this->input->post('staff_id');
    echo $upass1."-".$upass2;
	
	if($upass1!==$upass2)
	{
		$this->session->set_flashdata('message','New password  not match..!');
        redirect('Staff/view_staff');
	}
	else
	{
		$usr_dt = array(
                'staff_password'=>$upass3
                 );
		$this->load->model('Model_staff');
		$this->Model_staff->update_staff_pass($uid,$usr_dt);
		$this->session->set_flashdata('message',  'Password Changed..!');
       redirect('Staff/view_staff');
       }
}
}

public function del_entry()
  {
   $id=$this->uri->segment(3);
	//$id=$this->input->post('sid');
       $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa');
      $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
            $this->load->model('Model_staff');
            $result=$this->Model_staff->delete_staff($id);
            if($result)
            {
              
              $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$id,
            'log_desc'=>'Staff Details Deleted'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
	  $this->session->set_flashdata('message', 'Staff details removed.');
             redirect('Staff/view_staff');
            }
           else
           {
            $this->session->set_flashdata('message', 'Please try again');
            redirect('Staff/view_staff');
           }
    }
	
public function share_staff()
{
   $data=$this->session->all_userdata();
        //$id=$this->input->post('id');
  echo " <style> .tx{text-align:right;} .fnt{text-align:right;font-size:18px;}</style>            
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Share</font></h4>
    </div>
    <br>
  
<form  class='form-horizontal' role='form' method='post' action='". site_url('Pdf/share_staff'). "' enctype='multipart/form-data'>

  <div class='row'>
    <div class='col-md-12'>
   
         <div class='form-group'>
          <label class='col-md-2 control-label' style='padding-top:10px;'>Email to </label>
          <div class='col-md-8'>
          <input type='email' class='form-control' name='email' value='' placeholder='Enter email'> 
          </div>
          </div>
    </div>
    </div> 
    <div class='modal-footer'>
      <input type='submit' class='btn btn-primary' value='Share'/>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
     </div>     
  
</form>   
    ";
   

}



public function view_staffl()
{
   $data=$this->session->all_userdata();
    $id=$this->input->post('id');
    $this->db->select('*');
    $this->db->from('staffregistration');
    $this->db->join('profession', 'profession.profession_id=staffregistration.staff_profession','inner');
    $this->db->where("staff_id", $id);
    $query = $this->db->get();
           $row = $query->row();
  echo " <style> .tx{text-align:right;} .fnt{text-align:right;font-size:18px;} 
 .sview input[type=text]
   {
	   background-color:#fff;
   }   
</style>    
        
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Staff Details</font></h4>
    </div>
    <br>
 <div class='sview'>
<form  class='form-horizontal' role='form' method='post' action='". site_url('Pdf/share_staff'). "' enctype='multipart/form-data'>

  <div class='row'> 
   <div class='col-md-6'>
        
     <div class='form-group'>
          <label class='col-md-4 control-label' style='padding-top:5px;'></label>
          <div class='col-md-7'>  
      
          <img src='$row->staff_image'  width='100px' height='110px'>
          </div>                   
          </div>
		  
		  <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Staff Name</label>
          <div class='col-md-7'>  
            <input type='text' name='custcode' style='font-weight:600;font-size:17px;' class='form-control'  value='$row->staff_name' readonly />
          </div>                   
          </div>
		  
      <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Staff ID</label>
          <div class='col-md-7'>  
      <input type='hidden' name='opt' value='$id'/>
          <input type='text' name='custcode' class='form-control' value='$row->staff_id' readonly />
          </div>                   
          </div>

          <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Joining Date</label>
          <div class='col-md-7'>  
			
          <input type='text' name='custcode' class='form-control' value='";
		  $d1=explode("-",$row->staff_startdate);
		  $d11=$d1[2]."-".$d1[1]."-".$d1[0];
		    echo  $d11 ."' readonly />
          </div>                   
          </div>
        

          <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Staff Address</label>
          <div class='col-md-7'>  
      
          <textarea name='custcode' class='form-control' style='background-color:#fff;'  readonly>$row->staff_address</textarea>
          </div>                   
          </div>

		    <div class='form-group'>
          <label class='col-md-4 control-label' style='padding-top:5px;'>Mobile</label>
          <div class='col-md-7'>  
      
          <input type='text' name='custcode' class='form-control' value='$row->staff_mobile' readonly />
          </div>                   
          </div>

            <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Email</label>
          <div class='col-md-7'>  
      
          <input type='text' name='custcode' class='form-control' value='$row->staff_email' readonly />
          </div>                   
          </div>
		  
		  
		  
	<div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Profession</label>
          <div class='col-md-7'>  
      
          <input type='text' name='custcode' class='form-control' value='$row->profession_name' readonly />
          </div>                   
          </div>
	  
          </div>

		  
		  
 <div class='col-md-6' style='padding-right:35px;'>

          <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Date Of Birth</label>
          <div class='col-md-8'>  
              <input type='text' name='custcode' class='form-control' value='";
			   $d1=explode("-",$row->staff_dob);
		       $d11=$d1[2]."-".$d1[1]."-".$d1[0];
		    echo  $d11 ."' readonly />
			 
          </div>                   
          </div>

            <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Gender</label>
          <div class='col-md-8'>  
      
          <input type='text' name='custcode' class='form-control' value='$row->staff_gender' readonly />
          </div>                   
          </div>
		<div class='form-group'>
          <label class='col-md-4 control-label' style='padding-top:5px;'>Blood Group</label>
          <div class='col-md-8'>  
      
          <input type='text' name='custcode' class='form-control' value='$row->staff_bloodgroup' readonly />
          </div>                   
          </div>
		  <div class='form-group'>
          <label class='col-md-4 control-label' style='padding-top:5px;'>Guardian Name</label>
          <div class='col-md-8'>  
      
          <input type='text' name='guardian' class='form-control' value='$row->staff_guardian' readonly />
          </div>                   
          </div>
		  <div class='form-group'>
          <label class='col-md-4 control-label' style='padding-top:5px;'>Contact No</label>
          <div class='col-md-8'>  
      
          <input type='text' name='contactno' class='form-control' value='$row->staff_contactno' readonly />
          </div>                   
          </div>

          <div class='form-group'>
          <label class='col-md-4 control-label' style='padding-top:5px;'>Basic Salary</label>
          <div class='col-md-8'>  
      
          <input type='text' name='custcode' class='form-control' value='$row->staff_basicsalary' readonly />
          </div>                   
          </div>

          <div class='form-group' style='margin-top:15px;'>
          <label class='col-md-4 control-label' style='padding-top:5px;'>Aadhar Card No</label>
          <div class='col-md-8'>  
      
          <input type='text' name='custcode' class='form-control' value='$row->staff_aadharcard' readonly />
          </div>                   
          </div>


            <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Pan Card No</label>
          <div class='col-md-8'>  
      
          <input type='text' name='custcode' class='form-control' value='$row->staff_pancard' readonly />
          </div>                   
          </div>

  <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Bank Name</label>
          <div class='col-md-8'>  
      
          <input type='text' name='custcode' class='form-control' value='$row->staff_bankname' readonly />
          </div>                   
          </div>

            <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Bank Account</label>
          <div class='col-md-8'>  
      
          <input type='text' name='custcode' class='form-control' value='$row->staff_bankaccount' readonly />
          </div>                   
          </div>

            <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Bank IFSC No</label>
          <div class='col-md-8'>  
      
          <input type='text' name='custcode' class='form-control' value='$row->staff_ifsc' readonly />
          </div>                   
          </div>

         
          </div>

    </div>
    </div> 
    <div class='modal-footer'>
    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
     </div>     
  
</form>  
</div> 
    ";
   

}

public function save_staff()
{


$this->load->model('Model_customer');
$this->load->library('pdf1');
   
  
  $pdf=$this->pdf1->load();
   $id=$this->input->post('uid');
   $this->load->model('Model_staff');
   $data['results1']=$this->Model_staff->view_staffid($id);
   ini_set('memory_limit','256M');
   $html=  $this->load->view('pdf/staff_pdf',$data,true);
$pdf->WriteHtml($html);
$output='customerid_'.$id.'_'.date('Y_m_d').'_.pdf';
// $pdf->Output("$output",'D');
$pdf->Output('upload/savedpdf/'.$output,'F');
$this->session->set_flashdata('message',  'Staff Details Successfully Saved..!');
           redirect('Staff/view_staff');
    exit();
    
}



public function view_stafflist()
{
$this->load->view('admin/view_stafflist');
}

public function stafflist_ajax()
            {
            
        $this->load->model('Model_staff');
        $results=$this->Model_staff->view_staff();
        $data1 = array();
   foreach($results  as $r) 
     {
       $edit="<a href='".base_url()."Staff/edit_staff/".$r->staff_id."' data-toggle='modal' class='edit open-AddBookDialog2'><center><button class='btn btn-primary btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
       $change="<a href='#myModal2' id='$r->staff_id' data-toggle='modal' class='change' open-AddBookDialog2'><center><button class='btn btn-info btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
       $mm="<center><button class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></button></center>";
       $del=anchor('Staff/del_entry/'.$r->staff_id, $mm, array('id' =>'del_conf'));
	   $img="<img src='".$r->staff_image."' width='70px' height='75px'>";
	  
		/*$myString11 = $r->staff_certificate;
		$myArray11 = explode('/', $myString11);
		$certificate="<a href='$r->staff_certificate' target='_blank'><u>$myArray11[7]</u></a>";
		$myString1 = $r->staff_licence;
		$myArray1 = explode('/', $myString1);
		$licence="<a href='$r->staff_licence' target='_blank'><u>$myArray1[7]</u></a>";
		$myString = $r->staff_idcard;
		$myArray = explode('/', $myString);
		$idcard="<a href='$r->staff_idcard' target='_blank'><u>$myArray[7]</u></a>";
		
       $myStringo = $r->staff_others;
      $myArrayo = explode('/', $myStringo);
      $others="<a href='$r->staff_others' target='_blank'><u>$myArrayo[7]</u></a>";
				 */
    if($r->staff_status==1)
    {
      $st="<font color='green'>Active</font>";
    }
    else
    {
      $st="<font color='red'>Inactive</font>";
    }
	
	
          array_push($data1, array(
              // "edit"=>$edit,
              "id"=>"$r->staff_id",
			  "image"=>$img,
              "name"=>"$r->staff_name",
			  "mobile"=>"$r->staff_mobile",
              "certificate"=>$certificate,
              "licence"=>$licence,
              "idcard"=>$idcard,
              "others"=>$others,
              ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
    }
		
}