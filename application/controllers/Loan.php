<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$user = $this->session->userdata('userid');
		if (!isset($user))
		{ 
			redirect('/');
		} 
		else
		{ 
			return true;
		}
	}

	public function index(){
		$this->load->model("Model_loan");
		$this->load->view("admin/loan_master_list");
	}

	public function add_loan_master(){
		$this->load->model("Model_loan");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('ltl_ref', 'LTF Reference', 'required|trim');
		$this->form_validation->set_rules('vehicle_name', 'Vehicle Name', 'required|trim');
		$this->form_validation->set_rules('vehicle_amount', 'Vehicle Amount', 'required|trim');
		$this->form_validation->set_rules('loan_amount', 'Loan Amount', 'required|trim');
		$this->form_validation->set_rules('intrest_rate', 'Intrest rate', 'required|trim');
		$this->form_validation->set_rules('period', 'Period', 'required|trim');
		$this->form_validation->set_rules('down_paymet', 'Down payment', 'required|trim');
		$this->form_validation->set_rules('vehicle_regno', 'Vehicle Regno', 'required|trim');
		$this->form_validation->set_rules('other_charges', 'Other Charges', 'trim');
		$this->form_validation->set_rules('starting_date', 'Starting Date', 'trim');

		if ($this->form_validation->run() == FALSE)
		{
			redirect("loan/loan_master");
		}
		else
		{

		$this->load->helper("data");
		if(if_data_exist($this->input->post("ltl_ref"), "Model_Loan", "get_ltlreference")){
			$data["ltlerror"] = $this->input->post("ltl_ref") . " reference already exist";
			$this->load->view("admin/loan_master", $data);
			return;
		}

			$loan_intrest = ($this->input->post("loan_amount") * $this->input->post("intrest_rate"))/100;
			$loan_total = $loan_intrest + $this->input->post("loan_amount");
			$data = array(
				"loan_ltlrefno" => $this->input->post("ltl_ref"),
				"loan_vehiclename" => $this->input->post("vehicle_name"),
				"loan_vehicleamount" => $this->input->post("vehicle_amount"),
				"loan_amount" => $this->input->post("loan_amount"),
				"loan_interestrate" => $this->input->post("intrest_rate"),
				"loan_term" => $this->input->post("period"),
				"loan_downpayment" => $this->input->post("down_paymet"),
				"loan_othercharges" => $this->input->post("other_charges"),
				"loan_vehicleno" => $this->input->post("vehicle_regno"),
				"loan_startingdate" => $this->input->post("starting_date"),
				"loan_interest" => $loan_intrest,
				"loan_totalamt" => $loan_total,
				"loan_date" => date("Y-m-d"),
				"loan_vehiclestatus"=>'1'
				);

			$this->Model_loan->insert("loan_vehicle_master", $data);
			$loan_amount = $this->input->post("loan_amount");
			$loan_term = $this->input->post("period");

			$principal = $loan_amount / $loan_term;
			$monthly_intrest = $loan_intrest / $loan_term;

			$instalment = $principal + $monthly_intrest;

			$date = $this->input->post("starting_date");
			
			for($i=0; $i < $loan_term; $i++){
				
				$time = strtotime ( '+1 month' , strtotime ( $date ) ) ;
				$date = date ( 'Y-m-j' , $time );

				$loan_shedule = array(
					"ltl_ref_no	" => $this->input->post("ltl_ref"),
					"loan_date" => $date,
					"loan_amt" => $loan_amount,
					"loan_instalment" => $instalment,
					"loan_intrest" => $monthly_intrest,
					"loan_principal" => $principal,
					);

				$this->Model_loan->insert("loan_shedule", $loan_shedule);

				$loan_amount -= $principal;	
			}
			$staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
      $date=date("Y/m/d");
      date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa'); 
       $uid = $this->db->insert_id();
			$userdt1 = array(
        'log_staffid' => $staffid,
        'log_staffname'=>$staffname,
        'log_time'=>$time,
        'log_date'=>$date,
        'log_entryid'=>$uid,
        'log_desc'=>'New Loan Added'

        );
      $this->load->model('Model_customer');
      $this->Model_customer->form_insertlog($userdt1);
			redirect("loan");
		}
	}

	public function checkltlref(){
		$ltlref = $this->input->post("ltlrefno");
		$this->load->helper("data");
		if(if_data_exist($ltlref, "Model_Loan", "get_ltlreference")){
			echo $ltlref . " reference already exist";
			return;
		}
	}

	public function showshedule(){

		$ltlrefno = $this->uri->segment(3);
		$this->load->model("Model_loan");
		$results = $this->Model_loan->select("loan_shedule", array("ltl_ref_no" => $ltlrefno));
		$data = array();
		$slno = 1;
		foreach($results as $result){
			$check = $result->cheque_no;
			$voucher = $result->voucher_no;
			array_push($data, array(
				"slno" => $slno++,
				"ln_date" => $result->loan_date,
				"ln_amount" => $result->loan_amt,
				"ln_instalment" => $result->loan_instalment,
				"ln_intrest" => $result->loan_intrest,
				"ln_principal" => $result->loan_principal,
				"ln_check" => $check != null?$check:"-",
				"ln_voucher" => $voucher != null?$voucher:"-"
			));
	}
		echo json_encode(array("data" => $data));
	}




















//******************************************************** OLD CODE ***************************************




	public function long_term_loan(){
		$this->load->view("admin/long_term_loan");
	}

	public function loan_master(){
		$this->load->view("admin/loan_master");
	}

	public function loan_master_list(){
		$this->load->view("admin/loan_master_list");
	}

	public function add_long_term_loan(){
		$this->load->model("Model_loan");
		
		$check_existence = $this->Model_loan->select("account_master", array("ltl_refrence" => $this->input->post("ltf_ref_no")), "ltl_refrence");

		if($check_existence == null){
			redirect("Loan/long_term_loan");
			return;
		}

		$long_term_loan = array(
			"ltl_ref_no" => $this->input->post("ltf_ref_no"),
			"bank_name" => $this->input->post("bank_name"),
			"description" => $this->input->post("description"),
			"loan_amt" => $this->input->post("loan_amt"),
			"profit_amt" => $this->input->post("profit_amt"),
			"loan_ac" => $this->input->post("loan_ac"),
			"intrest_ac" => $this->input->post("intrest_ac"),
			"debit_ac" => $this->input->post("debit_ac"),
			"loan_type" => $this->input->post("loan_type"),
			"loan_date" => $this->input->post("loan_date"),
			"intrest_p" => $this->input->post("intrest_p")
			);

		$check_approval = $this->Model_loan->select("long_term_loans", array("ltl_ref_no" => $this->input->post("ltf_ref_no")), "ltl_ref_no");

		if($check_approval != null){
			$this->Model_loan->update("long_term_loans", $long_term_loan ,array("ltl_ref_no" => $this->input->post("ltf_ref_no")));
		}else{
			$long_term_loan['loan_date'] = date("Y-m-d");
			$this->Model_loan->insert("long_term_loans", $long_term_loan);
		}
		$this->Model_loan->update("account_master", array("loan_amount" => $this->input->post("loan_amt"), "acc_status" => "1") ,array("ltl_refrence" => $this->input->post("ltf_ref_no")));
		//redirect("Loan");
		$uid = $this->db->insert_id();
		$staffid=$this->session->userdata('userid');
		$staffname=$this->session->userdata('name');
		$date=date("Y/m/d");
		date_default_timezone_set('Asia/Kolkata');
		$time=date('h:i:sa'); 
		$userdt1 = array(
			'log_staffid' => $staffid,
			'log_staffname'=>$staffname,
			'log_time'=>$time,
			'log_date'=>$date,
			'log_entryid'=>$uid,
			'log_desc'=>'Long term Loan Added'

			);
		$this->load->model('Model_customer');
		$this->Model_customer->form_insertlog($userdt1);
		
		$this->session->set_flashdata('message',"<font color:green>Loan details successfully added..!</font>");
		redirect("Loan/long_term_loan");
		return;
	}


	public function loan_ajax()
	{
		$this->load->model('Model_loan');

		$results = $this->Model_loan->select("account_master",array("acc_status" => "0"));

		$data1 = array();
		$paymentMode = array("y" => "Years", "m" => "Months", "w" => "Weaks");
		foreach($results  as $r) 
		{
			$edit="<a href='".base_url()."Loan/edit_loan_master/".$r->acc_id."' id='$r->acc_id' data-toggle='modal' class='edit open-AddBookDialog2'>
			<center><button class='btn btn-primary btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
			$approve="<a href='#myModal1' id='{$r->acc_id}' data-toggle='modal' class='approve open-AddBookDialog2'>
			<center><button class='btn btn-primary btn-xs' data-title='Delete' data-toggle='modal' >Approve</button></center></a>";
			$print="<a href='pdf_trip/{$r->acc_id}'  class='print open-AddBookDialog2'>
			<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-print'></span></button></center></a>";
			$share="<a href='#myModals' id='{$r->acc_id}' data-toggle='modal' class='share open-AddBookDialog2'><center><button class='btn btn-success btn-xs' data-title='Share' data-toggle='modal' ><span class='glyphicon glyphicon-share'></span></button></center></a>";
			
			$del=anchor('Loan/del_entry/'.$r->acc_id, 'Delete', array('id' =>'del_conf'));
			$st="";
			if($r->acc_status=='0')
			{
				$st="<font color=red>New</font>";
			}

			
			array_push($data1, array(
				"edit"		=>	$edit,
				"approve"=>$approve,
				"ltl_ref"	=>	$r->ltl_refrence,
				"acc_name"	=>	$r->acc_name,
				//"acc_addr"	=>	$r->acc_addr,
				"mobile_no"	=>	$r->mobile_no,
				"amount"=>$r->loan_amount,
				//"phone_no"	=>	$r->phone_no,
				"loan_duration"	=> $r->anual_intrest_rt ."%" .", ". $r->loan_term . ' ' . $paymentMode[$r->payment_mode],
				"description"=>$r->loan_description,
				//"bank_name"	=>	$r->bank_name,
				//"bank_acc_no"	=>	$r->bank_acc_no
				"status"=>$st,
				));
		}
		echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
	}



	public function get_loan_details(){
		$this->load->model('Model_loan');

		$ltl_refrence	=	$this->input->post("ltl_ref");
		$check_approval =	$this->Model_loan->select("account_master",array("ltl_refrence	" => $ltl_refrence	), "acc_status");
		
		$loan_info = $this->Model_loan->select("account_master",array("ltl_refrence	" => $ltl_refrence	));
		if($loan_info != null){
			echo json_encode($loan_info);
		}
		else{
			echo FALSE;
		}
		return;
	}
//*********************************************************
	public function get_shedule(){
		$this->load->model('Model_loan');

		$ltl_refrence = $this->input->post("ltl_ref");
		$start_date = $this->input->post("start_date");
		
		$check = $this->Model_loan->select("loan_shedule",array("ltl_ref_no" => $ltl_refrence));
		if($check != null)
		{
			$temp_loan["loan_shedule"] = $this->Model_loan->select("loan_shedule",array("ltl_ref_no" => $ltl_refrence, "DATE_FORMAT(loan_date,'%Y-%m-%d') >=" => date("Y-m-d", strtotime($start_date))));
			echo json_encode($temp_loan);
			return;
		}

		$check = $this->Model_loan->select("temp_loan",array("ltl_ref_id" => $ltl_refrence));
		
		if($check == null){
			$loan_amount = $this->input->post("loan_amount");
			$intrest_p = $this->input->post("intrest");
			$term = $this->Model_loan->select("account_master",array("ltl_refrence" => $ltl_refrence), "loan_term");
			$term = $term[0]->loan_term;

			$total_intrest = ($loan_amount * ($intrest_p/100) ) * ($term/12);
			$intrest = $total_intrest / $term;

			$Principal = $loan_amount / $term;
			$loan_data = array(
				"ltl_ref_id" => $ltl_refrence,
				"loan_given" => $loan_amount,
				"intrest_given" => $total_intrest,
				"loan_date" => date( "Y-m-d")
				);
			$this->Model_loan->insert("temp_loan", $loan_data);
			$date = strtotime( "+". 0 ." month" , strtotime($start_date));
			for($i = 0, $j =1;$i < $term;$i++ , $j++ ){
				$loan_data = array(
					"ltl_ref_id" => $ltl_refrence,
					"loan_received" => $Principal,
					"intrest_received" => $intrest,
					"loan_date" => date( "Y-m-d", $date),
					);
				$this->Model_loan->insert("temp_loan", $loan_data);
				$date = strtotime( "+". $j ." month" , strtotime($start_date));
			}			
		}
		$temp_loan = $this->Model_loan->select("temp_loan",array("ltl_ref_id" => $ltl_refrence));

		echo json_encode($temp_loan);

	}

	
	
	public function save_details(){
		$this->load->model('Model_loan');
		$ltl_refrence = $this->uri->segment(3);
		$check = $this->Model_loan->select("loan_shedule",array("ltl_ref_no" => $ltl_refrence));
		if($check == null)
		{
			$temp_loan = $this->Model_loan->select("temp_loan",array("ltl_ref_id" => $ltl_refrence));
			$loan_amount = $temp_loan[0]->loan_given;
			for($i=1; $i < count($temp_loan); $i++){
				$loan_data = array(
					"ltl_ref_no" => $ltl_refrence,
					"loan_date" => $temp_loan[$i]->loan_date,
					"loan_amt" => $loan_amount,
					"loan_instalment" => ($temp_loan[$i]->intrest_received + $temp_loan[$i]->loan_received),
					"loan_intrest" => $temp_loan[$i]->intrest_received,
					"loan_principal" => $temp_loan[$i]->loan_received,
					"check_no" => "",
					"voucher_no" => ""
					);
				$this->Model_loan->insert("loan_shedule", $loan_data);	
				if(count($temp_loan) != $i+1)
					$loan_amount = $loan_amount - $temp_loan[$i+1]->loan_received;
			}
			$this->Model_loan->delete("temp_loan", array("ltl_ref_id" => $ltl_refrence));
			$this->Model_loan->update("account_master", array("approved_date" => date("Y-m-d"), "acc_status" => 1), array("ltl_refrence" => $ltl_refrence));
		}
		else{
			redirect("Loan/long_term_loan");
		}
		redirect("Loan/approved_loan");
	}

	public function approved_loan(){
		if($this->input->post() != null){
			$ltl_ref = $this->input->post("ltl_ref");
			echo ' 
			
			
			<table class="table table-striped table-hover table-bordered" id="shedule_list">
				<thead>
					<tr style="color:#5068f8;">
						<th>SL</th>
						<th>Date</th>
						<th>Loan</th>
						<th>Instaltment</th>
						<th>Intrest</th>
						<th>Principle</th>
						<th>Check No</th>
						<th>Voucher No</th>
					</tr>
				</thead>
			</table>

			';

			echo "
			<script>

				$('#shedule_list').dataTable({
					destroy: true,
					'processing': true,
					'ajax': {
						url :'". base_url() ."' + 'Loan/loanshedule_ajax/{$ltl_ref}',

					},
					'columnDefs':[
					{'width':'6%','targets':0},
					],

					'columns': [
					{ 'data': 'slno'},
					{ 'data': 'loan_date'},
					{ 'data': 'loan_amt' },
					{ 'data': 'loan_instalment' },
					{ 'data': 'loan_intrest' },
					{ 'data': 'loan_principal' },
					{ 'data': 'check_no' },
					{ 'data': 'voucher_no' },
					]
				} );

			</script>

			";
			return;
		}
		$this->load->view("admin/approved_loan");
	}

	public function loanshedule_ajax(){
		$this->load->model('Model_loan');
		$ltl_refrence = $this->uri->segment(3);
		$results = $this->Model_loan->select("loan_shedule",array("ltl_ref_no" => $ltl_refrence));
		$i=0;
		$data1 = array();
		foreach($results  as $r) 
		{			
			array_push($data1, array(
				"slno"		=> ++$i,
				"loan_date"	=> $r->loan_date,
				"loan_amt"	=>$r->loan_amt,
				"loan_instalment"=>	$r->loan_instalment,
				"loan_intrest"=> $r->loan_intrest,
				"loan_principal"=> $r->loan_principal,
				"check_no"=> $r->check_no,
				"voucher_no"=> $r->voucher_no,
				));
		}
		echo json_encode(array('data' => $data1));

	}

	public function loanapproved_ajax(){

		$this->load->model('Model_loan');

		$results = $this->Model_loan->select("account_master",array("acc_status" => "1"));

		$data1 = array();
		$paymentMode = array("y" => "Years", "m" => "Months", "w" => "Weeks");
		$i = 0;
		foreach($results  as $r) 
		{
			$edit="<a href='".base_url()."Loan/edit_loan_master/".$r->acc_id."' id='$r->acc_id' data-toggle='modal' class='edit open-AddBookDialog2'>
			<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
			$view="<a href='#myModal1' id='{$r->acc_id}' data-toggle='modal' class='view open-AddBookDialog2'>
			<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-list-alt'></span></button></center></a>";
			$print="<a href='pdf_trip/{$r->acc_id}'  class='print open-AddBookDialog2'>
			<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-print'></span></button></center></a>";
			$share="<a href='#myModals' id='{$r->acc_id}' data-toggle='modal' class='share open-AddBookDialog2'><center><button class='btn btn-success btn-xs' data-title='Share' data-toggle='modal' ><span class='glyphicon glyphicon-share'></span></button></center></a>";
			$shedule="<center><button class='btn btn-success' onclick='loadshedule(this)' data-title='Share' data-value='{$r->ltl_refrence}' ><span class='glyphicon glyphicon-share'></span></button></center>";

			$rpay=anchor('Loan/repayment/'.$r->ltl_refrence, 'Re-payment', array('id' =>'Re-payment','class' =>'btn btn-primary btn-xs'));
			
			array_push($data1, array(
				"slno"		=>	++$i,
				"ltl_ref"	=>	$r->ltl_refrence,
				"amount"	=>	$r->loan_amount,
				"anual_intrest_rt"	=>	$r->anual_intrest_rt . " %",
				"loan_duration"	=> 	$r->loan_term . ' ' . $paymentMode[$r->payment_mode],
				"shedule"	=>	$shedule,
				"repayment"	=>	$rpay
				));
		}
		echo json_encode(array('data' => $data1));
	}


	public function get_details()
	{
		$ltl= $this->input->post("ltl");
		$this->load->model("Model_loan");
		$loanDT = $this->Model_loan->select("account_master",array("ltl_refrence" => $ltl));
		$dt=array();

	//$r2=$this->db->select('*')->from("loan_shedule")->where('ltl_ref_no',$ltl)->get()->row();.

		$iamt=0;$ins=0;
		foreach($loanDT as $r1)
		{
			
			$dt=array('ltlref'=>$r1->ltl_refrence,
				'name'=>$r1->acc_name,
				'address'=>$r1->acc_addr,
				'mobile'=>$r1->mobile_no,
				'phone'=>$r1->phone_no,
				'amount'=>$r1->loan_amount,
				'intrest'=>$r1->anual_intrest_rt."%",
				'period'=>$r1->loan_term,
				'bankname'=>$r1->bank_name,
				'bankacc'=>$r1->bank_acc_no,
				"debitac"=>$r1->debit_ac,
				"intrestac"=>$r1->intrest_ac,
				);
		}
		echo  json_encode($dt);
	}

/// loan - re-payment 

	public function repayment()
	{
		$lrno=$this->uri->segment(3);
		$lrno=str_replace("%20", " ", $lrno);

		$this->load->model('Model_loan');
		$data['lrno']=$lrno;
		$data['results'] = $this->Model_loan->select("account_master",array("ltl_refrence" => $lrno));
		$data['results1'] = $this->Model_loan->select("loan_shedule",array("ltl_ref_no" => $lrno));
		$this->load->view('admin/loan_repayment',$data);
	}


	public function repayment_ajax(){
		$ltlref=$this->uri->segment(3);
		$ltlref=str_replace("%20", " ", $ltlref);

		$this->load->model('Model_loan');
		$results = $this->Model_loan->select("loan_shedule",array("ltl_ref_no" => $ltlref));

		$data1 = array();
		$i=0;
		foreach($results  as $r) 
		{
			$dt=date_create($r->loan_date);
			
			array_push($data1, array(
				"lslno"	=>++$i,
				"lsid"	=>$r->loan_shedule_id,
				"ldate"	=>date_format($dt, 'd-M-Y'),
				"lamount"=>$r->loan_amt,
				"linstall"=>$r->loan_instalment,
				"linter"=>	$r->loan_intrest,
				"lprin"=>	$r->loan_principal,
				"lcheque"=>	$r->check_no,
				"lvoucher"=>$r->voucher_no,
				));
		}
		echo json_encode(array('data' => $data1));
	}
	
	
	
	public function add_repayment()
	{
		$lrno=$this->input->post('ltlrefno');

		$this->load->library('form_validation');

		$this->form_validation->set_rules('ltlrefno', 'ltlrefno', 'required');
		$this->form_validation->set_rules('insmonth', 'insmonth', 'required');
		$this->form_validation->set_rules('pmode', 'pmode', 'required');
		$this->form_validation->set_rules('voucherno', 'voucherno', 'required');

		if ($this->form_validation->run())
		{	
			$lrno=$this->input->post('ltlrefno');
			$mon=$this->input->post('insmonth');
			$cheque=$this->input->post('chequeno');
			$pmode=$this->input->post('pmode');
			$voucher=$this->input->post('voucherno');

			if($pmode!=='Cheque')
			{
				$cheque=$pmode;
			}

	//echo  $lrno.",".$mon.",".$cheque.",".$pmode.",".$voucher;

			$newdt=array('check_no'=>$cheque,
				"voucher_no"=>$voucher,
				"status"=>"1",
				);

			$this->load->model("Model_loan");

			$where=array('ltl_ref_no'=>$lrno,
				'MONTH(loan_date)'=>$mon
				);

			$this->Model_loan->update("loan_shedule", $newdt, $where);
			$this->session->set_flashdata('message', 'Payment Accepted.');
			redirect('Loan/repayment/'.$lrno);
		}
		else
		{
			$this->session->set_flashdata('message', '');
			redirect('Loan/repayment/'.$lrno);
		}
	}

	public function loanpayment()
	{
		$this->load->view('admin/loan_payments');
	}

	public function loanpayment_ajax(){

		$this->load->model('Model_loan');
		$results = $this->Model_loan->select("loan_shedule",array("MONTH(loan_date)" =>date('m'),"status"=>'1'));

		$data1 = array();
		$i=0;
		foreach($results  as $r) 
		{
			array_push($data1, array(
				"ltlno"	=>$r->ltl_ref_no,
				"lsid"	=>$r->loan_shedule_id,
				"ldate"	=>$r->loan_date,
				"lamount"=>$r->loan_amt,
				"linstall"=>$r->loan_instalment,
				"linter"=>	$r->loan_intrest,
				"lprin"=>	$r->loan_principal,
				"lcheque"=>	$r->check_no,
				"lvoucher"=>$r->voucher_no,
				));
		}
		echo json_encode(array('data' => $data1));
	}

//**************************************************************************


/*	public function edit_loan_master(){
		$this->load->model('Model_loan');
		if($this->input->post() != null){
			$data = array(
				"ltl_refrence" => $this->input->post("ltl_ref"),
				"acc_name" => $this->input->post("cust_name"),
				"acc_addr" => $this->input->post("cust_addr"),
				"mobile_no" => $this->input->post("mobile_no"),
				"phone_no" => $this->input->post("phone_no"),
				"anual_intrest_rt" => $this->input->post("anual_intrest_rt"),
				"loan_term" => $this->input->post("loan_term"),
				"payment_mode" => $this->input->post("payment_mode"),
				"bank_name" => $this->input->post("bnk_name"),
				"bank_acc_no" => $this->input->post("bnk_ac_no"),
				);
			$where["acc_id"] = $this->input->post("acc_id");
			$this->Model_loan->update("account_master", $data, $where);
			redirect("loan/loan_master_list");
			return;
		}
		$acc_id	=	$this->uri->segment(3);
		$data["account_master"] = $this->Model_loan->select("account_master",array("acc_id" => $acc_id));
		$staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
       $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
     $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$acc_id,
            'log_desc'=>'New Loan Details Updated'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
		$this->load->view("admin/loan_master",$data);
	}
*/

 public function del_entry()
    {

                //$id=$this->uri->segment(3, 0);
      $id=$this->input->post('id');
      $date=date("Y/m/d");
      date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa');
      $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
      $this->load->model('Model_loan');
      $result=$this->Model_loan->delete_loan($id);
       $result1=$this->Model_loan->delete_loan1($id);
      // if($result)
      // {
        $this->session->set_flashdata('messages', 'Loan details has been deleted..!');
         $date=date("Y/m/d");
      date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa');
      $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
       $id=$this->input->post('id');
        $userdt1 = array(
          'log_staffid' => $staffid,
          'log_staffname'=>$staffname,
          'log_time'=>$time,
          'log_date'=>$date,
          'log_entryid'=>$id,
          'log_desc'=>'Loan Details Deleted'

          );
        $this->load->model('Model_customer');
        $this->Model_customer->form_insertlog($userdt1);
        redirect('Loan/index');
      //}
      // else
      // {
      //   $this->session->set_flashdata('message', 'Data missing, try again');
      //   redirect('Loan/index');
      // }
    }



}