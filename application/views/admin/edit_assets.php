<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title" id="myModalLabel" align="center">Edit Assets</h3>
      </div>
      
          <div class="modal-body">
          <form enctype="multipart/form-data" method="post"  action="<?php echo base_url('Asset/update_assets');?>" data-parsley-validate>
            
        <div class="form-group">
        <label for="date">Date:</label>
        <input type="hidden" id="edit_assets_id" name="edit_assets_id" value="<?php echo $assets->asset_id;?>" />
        <input type="text" class="form-control" id="edit_date" name="edit_date" value="<?php echo $assets->asset_date?>" required>
        </div>
        <div class="form-group">
        <label for="asset_name">Asset Name:</label>
        <input type="text" class="form-control" id="edit_asset_name" name="edit_asset_name" value="<?php echo $assets->asset_name?>" required>
        </div>
        <div class="form-group">
        <label for="actual_cost">Actual Cost</label>
        <input type="text" class="form-control" id="edit_actual_cost" name="edit_actual_cost" value="<?php echo $assets->asset_cost?>" required>
        </div>
        <div class="form-group">
        <label for="quantity">Quantity:</label>
        <input type="text" class="form-control" id="edit_quantity" name="edit_quantity" value="<?php echo $assets->asset_qty?>" required>
        </div>
        
        <div class="form-group">
        <label for="description">Description:</label>
        <textarea name="edit_description" id="edit_description" class="form-control" required><?php echo $assets->asset_description?></textarea>
        </div>
		<div class='row'>
		<div class='col-md-12' style='text-align:right;'>
        <button type="submit" class="btn btn-primary" >Submit</button> 
		<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
		</div>
		</div>
         </form>
<script>
	  $('#edit_date').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
   </script>
 	 
		 