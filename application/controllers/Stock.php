<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Model_staffsalary');	
	}

	public function Purchase_request()
	{
		$this->load->view('admin/purchase_request');
	}
	
	
	public function Add()
	{
		$op=$this->uri->segment(3);
		$this->load->Model('Model_stock');
		if($op==null || $op=="")
		{
		$stodata['stock']=$this->Model_stock->view_stock();
		$stodata['stotab']=1;
		}
		else if($op==0) //list button in delivery sheet
		{
			$stodata['stock']=$this->Model_stock->view_stock();
			$stodata['stotab']=2;
		}
		else if($op==1)
		{
			$cat=$this->input->post('scat');
			$stodata['stock']=$this->Model_stock->view_stock1($cat);
			$stodata['stotab']=2;
		}
		else if($op==2)  //for updation
		{
			$stodata['stock']=$this->Model_stock->view_stock();
			$stodata['stotab']=2;
		}
				
	    $this->load->view('admin/add_stock_items',$stodata);	        
	}
	
	public function add_stock_category()
	{
		$na=strtoupper($this->input->post('cname'));
		$dt=array('stock_cat_name'=>$na);
		$res=$this->db->insert("stock_category",$dt);
		if($res)
		{
			echo $na;
		}
		else
		{
			echo 0;
		}
	}
	
	public function add_stock_items()
	{
		$na=strtoupper($this->input->post('iname'));
		$dt=array('stock_itemname'=>$na);
		$res=$this->db->insert("stock_items",$dt);
		if($res)
		{
			echo $na;
		}
		else
		{
			echo 0;
		}
	}
		
	public function get_item_qty()
	{
		$na=trim(strtoupper($this->input->post('iname')));
		$cat=trim(strtoupper($this->input->post('category')));
		
		$where="item_sta_category='".$cat."' and item_sta_particulars='".$na."'";
		$rw=$this->db->select('item_sta_total,item_sta_units')->from('item_stock')->where($where)->get()->row();
		if(is_object($rw))
		{
		echo $rw->item_sta_total."#".$rw->item_sta_units;
		}
		else
		{
			echo 0;
		}
	}

	public function add_stock()
	{
	$edate=$this->input->post('edate');
	$cat=$this->input->post('category');
    $part=$this->input->post('particulars');
	
	$stinhand=$this->input->post('stinhand');
    $qty=$this->input->post('qty');
	$units=$this->input->post('units');
	$desc=$this->input->post('desc');
		
	$dt1=explode("-",$edate);
	$dt11=$dt1[2]."-".$dt1[1]."-".$dt1[0];
	$this->load->Model('Model_stock');
	
	if($stinhand>0)
	{
		$stot=$stinhand + $qty;
		
		$stdata = array(
         'item_sta_date' => $dt11,
		  'item_sta_category' => $cat,
		 'item_sta_particulars' => $part,
		 'item_sta_qty' => $qty,
		 'item_sta_units'=>$units,
		 'item_sta_total' => $stot,
		 'item_sta_desc' => $desc,
		  );
		  
		  $this->Model_stock->update_stock($stdata,$part);
		  $this->session->set_flashdata('message', '1#Stock item details updated.!');
	}
	else if($stinhand<=0)
	{
		$stot=$qty;
		$stdata = array(
         'item_sta_date' => $dt11,
		  'item_sta_category' => $cat,
		 'item_sta_particulars' => $part,
		 'item_sta_qty' => $qty,
		 'item_sta_units'=>$units,
		 'item_sta_total' => $stot,
		 'item_sta_desc' => $desc,
		  );
		  $this->Model_stock->insert_stock($stdata);
		  $this->session->set_flashdata('message', '1#Stock item details added.!');
	}
      	  redirect('Stock/Add');  
	}
	
	public function delete_stock()
	{
		$id=$this->uri->segment(3);
		$this->load->Model('Model_stock');
		$this->Model_stock->del_stock($id);
		$this->session->set_flashdata('message', '3# Item removed from stock.!');
      	redirect('Stock/Add');  
	}
	
	public function edit_stock()
	{
		$id=$this->input->post('id');
		$rw=$this->db->select('*')->from('item_stock')->where('item_sta_id',$id)->get()->row();
		
		$dt1=explode("-",$rw->item_sta_date);
	    $dt11=$dt1[1]."/".$dt1[2]."/".$dt1[0];
			
		
		Echo "
		
		<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal'>&times;</button>
					<h4 class='modal-title' > <font color='blue'>Edit Stock</font></h4>
					</div>
		 <form class='form-horizontal' method='POST' action='".base_url('Stock/update_stock')."' enctype='multipart/form-data'>
		<div class='row'>
		<div class='col-md-12'>
 
   
             	
			<input type='text' class='form-control' name='stid'   value='".$rw->item_sta_id ."' required>
			
			<div class='form-group' style='margin-top:15px;'>
			<div class='row'>
			<label class='col-md-4' style='padding-top:5px;' align='right'>Date : </label>
			<div class='col-md-4'>
			<input type='date' class='form-control' name='edate'  value='".$rw->item_sta_date ."' required>
			</div> 
			</div> 
			</div>
			<div class='form-group'>
			<div class='row'>
			<label class='col-md-4' style='padding-top:5px;' align='right'>Category : </label>
			
			<div class='col-md-7'>
			  <select class='form-control' name='category' id='category'>
				<option value=''>--------</option>";
				
				$sttype=$this->db->select('*')->from('stock_category')->get()->result();
				foreach($sttype as $st)
				{				
					echo "<option value='".$st->stock_cat_name."'"; if($rw->item_sta_category==$st->stock_cat_name) echo "selected"; echo " >".$st->stock_cat_name."</option>";
				}
				
				echo "
				</select>
			</div>
			</div>
			</div>
			
			
			<div class='form-group' >
			<div class='row'>
			<label class='col-md-4' style='padding-top:5px;' align='right'>Particulars : </label>
			
			<div class='col-md-7'>
                <select class='form-control' name='particulars' id='particulars'>
				<option value=''>--------</option>";
				
				$sttype=$this->db->select('*')->from('stock_items')->get()->result();
				foreach($sttype as $st)
				{				
				echo "<option value='".$st->stock_itemname."'"; if($rw->item_sta_particulars==$st->stock_itemname) echo "selected"; echo ">".$st->stock_itemname."</option>";
				}
			echo "
				</select>
			</div>
			</div>
			</div>
			
						
			<div class='form-group'>
			<div class='row'>
			<label class='col-md-4' style='padding-top:5px;' align='right'>Units : </label>
			<div class='col-md-3'>
                <select class='form-control' name='units' id='units'>
				<option value=''>--------</option>
				<option value='NOS'"; if ($rw->item_sta_units=='NOS') echo 'selected'; echo ">NOS</option>
				<option value='PACKETS'"; if ($rw->item_sta_units=='PACKETS') echo 'selected';echo ">PACKETS</option>
				<option value='BOX'"; if ($rw->item_sta_units=='BOX') echo 'selected'; echo ">BOX</option>
				<option value='KG'"; if ($rw->item_sta_units=='KG') echo 'selected'; echo ">KG</option>
				<option value='ML'"; if ($rw->item_sta_units=='ML') echo 'selected'; echo ">ML</option>
				<option value='LTR'"; if ($rw->item_sta_units=='LTR') echo 'selected'; echo ">LTR</option>
				</select>
			</div>
			</div>
			</div>
			
			
			<div class='form-group'>
			<div class='row'>
			<label class='col-md-4' style='padding-top:5px;' align='right'>Quantity : </label>
			<div class='col-md-3'>
			<input type='number' name='qty' id='qty' class='form-control' value='".$rw->item_sta_qty."' required>
			</div> 
			</div> 
			</div>
			
			<div class='form-group'>
			<div class='row'>
			<label class='col-md-4' style='padding-top:5px;' align='right'>Description : </label>
			<div class='col-md-7'>
			<textarea rows=3 name='desc' id='desc' class='form-control' required >". $rw->item_sta_desc ."</textarea>
			</div> 
			</div> 
			</div>

</div>
</div>
<div class='modal-footer'>
	<input type='submit' class='btn btn-primary' value='Update' style='padding:3px 20px 3px 20px;' />
	<button type='button' class='btn btn-default ' data-dismiss='modal'>Close</button>
</div>

</form>
";

}
public function update_stock()
	{
	
	$stid=$this->input->post('stid');
	$edate=$this->input->post('edate');
	$cat=$this->input->post('category');
    $part=$this->input->post('particulars');
	$qty=$this->input->post('qty');
	$units=$this->input->post('units');
	$desc=$this->input->post('desc');
		
	$this->load->Model('Model_stock');
		$stdata = array(
         'item_sta_date' => $edate,
		 'item_sta_category' => $cat,
		 'item_sta_particulars' => $part,
		 'item_sta_qty' => $qty,
		 'item_sta_units'=>$units,
		 'item_sta_total' => $qty,
		 'item_sta_desc' => $desc,
		  );

		  $this->Model_stock->update_stock_dt($stdata,$stid);
		  $this->session->set_flashdata('message', '2#Stock item details updated.!');
      	  redirect('Stock/Add/2');  
	}
	
//-------DELIVERY --------------------------------------------------


public function Delivery()
{
$op=$this->uri->segment(3);
$dt11="";$dt12="";
		$this->load->Model('Model_stock');
		if($op==null || $op=="")
		{
			$this->load->Model('Model_stock');
			$delidata['delidata']=$this->Model_stock->view_delivery();
			$delidata['stotab']=1;
		}
		else if($op==1)
		{
			$d1=$this->input->post('sdate');
			$d2=$this->input->post('edate');
			
			$dt1=explode("-",$d1);
			$dt11=$dt1[2]."-".$dt1[1]."-".$dt1[0];
			
			$dt2=explode("-",$d2);
			$dt12=$dt2[2]."-".$dt2[1]."-".$dt2[0];
						
			$this->load->Model('Model_stock');
			$delidata['delidata']=$this->Model_stock->view_delivery1($dt11,$dt12);
			$delidata['stotab']=2;
		}
		else if($op==2)  //for updation
		{
			$this->load->Model('Model_stock');
			$delidata['delidata']=$this->Model_stock->view_delivery();
			$delidata['stotab']=2;
		}
		
		 //for report
		$this->session->set_userdata('delidt1',$dt11);
		$this->session->set_userdata('delidt2',$dt12);
		//--------
	    $this->load->view('admin/delivery_stock_items',$delidata);	        	
}

public function delivery_get_item_qty()
	{
		$na=trim(strtoupper($this->input->post('iname')));
		
		$where="item_sta_particulars='".$na."'";
		$rw=$this->db->select('item_sta_total,item_sta_units')->from('item_stock')->where($where)->get()->row();
		if(is_object($rw))
		{
		echo $rw->item_sta_total."#".$rw->item_sta_units;
		}
		else
		{
			echo 0;
		}
	}	
		
public function delivery_stock()
	{
	
	$edate=$this->input->post('edate');
	$staff=$this->input->post('staffna');
	$stinhand=$this->input->post('stinhand');
	$vregno=$this->input->post('vregno');
    $part=$this->input->post('particulars');
	$qty=$this->input->post('qty');
	$units=$this->input->post('units');
	$desc=$this->input->post('desc');
		
	    $dt1=explode("-",$edate);
	    $dt11=$dt1[2]."-".$dt1[1]."-".$dt1[0];	
				
	
		$stdata = array(
         'delivery_date' => $dt11,
		 'delivery_staff' => $staff,
		 'delivery_vehicle' => $vregno,
		 'delivery_particulars' => $part,
		 'delivery_qty' => $qty,
		 'delivery_units'=>$units,
		 'delivery_desc' => $desc,
		  );
		  
		  $this->load->Model('Model_stock');
		  $this->Model_stock->insert_delivery($stdata);
		  
		  //update stock ----------------------
		  $sqty=($stinhand-$qty);
		  $ddat=array('item_sta_qty'=>$sqty,'item_sta_total'=>$sqty);
		  $this->db->where('item_sta_particulars',$part)->update('item_stock',$ddat);		  
		  //------------------
		  
		  $this->session->set_flashdata('message', '1#Delivery item details added.!');
      	  redirect('Stock/Delivery');  
	}
	
	public function delete_delivery()
	{
		$id=$this->uri->segment(3);
			
		$rw=$this->db->select('delivery_particulars,delivery_qty')->from('delivery_stock')->where('delivery_id',$id)->get()->row();
		$sqty=$rw->delivery_qty;
		$part=$rw->delivery_particulars;
		
		
		$this->load->Model('Model_stock');
		$this->Model_stock->del_delivery($id);
			
		
		// stock updation ------------------
		$ddt="UPDATE item_stock SET item_sta_qty=item_sta_qty+".$sqty.",item_sta_total=item_sta_total+".$sqty." WHERE item_sta_particulars='".$part."'";
		$this->db->query($ddt);	
						
		//-------------------------------------
		$this->session->set_flashdata('message', '3#Delivery details removed.!');
      	redirect('Stock/Delivery/2');  
	}	
	
}	

