<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
<title>Travels</title>

 <style type="text/css">
 td{padding:2px 5px 3px 5px; }
 table{font-size:14px;}
 </style>
  
</head>

<body>
<!-- heading ----------------------------------------------------->
<br>
<table style="border:none;width:100%;margin-top:20px;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
<td width="150px" style="padding: 0px" align='right'>
<?php $date = date('d/m/Y');?>
Date: <?=$date;?>
  </td>
</tr>

<tr>
<td width="150px" style="padding: 0px">
<Strong>Advance World Holidays</strong><br>
PK & Sons Complex, East Moozhikkal<br>
NH 212, Calicut, Kerala, India-673571<br>
Tel: 0091 495 2732 500, 0091 495 3100 700<br>
Fax: 0091 495 2732 400
</td>
<td width="150px" style="padding: 0px" align='right'>

</td>
</tr>
</table>
<!-- heading end----------------------------------------------------->
<hr>

<div class="box-body" style='min-height:450px;'>

			  <div class="row">
	            <div class="col-md-10">
				<table class="table table-hover table-bordered" id="example"  width='100%' border=0 style="font-size:14px">
             <thead>
			 <tr>
			 <td colspan=4 height='35px' align='center'><h3>Profit & Loss Account</h3></td>
			 </tr>

			 <tr>
			 <td colspan=4> <?php echo $this->session->userdata('acperiod1');?></td>
			 </tr>
			 <tr>
			 <td colspan=4><hr></td>
			 </tr>
			 
                <tr>
				<th width='20px' height='30px'></th> 
				<th align='left'>Particulars</th> 
				<th style='width:120px;text-align:right;'>Debit</th>
                <th style='width:120px;text-align:right;'>Credit</th> 
                </tr>
                </thead>
				<tbody>
				
				<?php
				$drtot=0;
				$crtot=0;

				foreach ($incomes as $pr1)
				{
					$drtot+=$pr1->debit;
					$crtot+=$pr1->credit;
				?>
				
				<tr>
				<td width='20px'></td> 
				<td><?=ucwords(strtolower($pr1->ledg_name));?></th> 
				<td style='width:120px;text-align:right;'><?=number_format($pr1->debit,2,".","");?></td>
                <td style='width:120px;text-align:right;'><?=number_format($pr1->credit,2,".","");?></td> 
                </tr>
				<?php
				}
				// direct expenses details

				foreach ($expenses as $pr2)
				{
				if($pr2->debit==0 or $pr2->credit==0)
				{
					$drtot+=$pr2->debit;
					$crtot+=$pr2->credit;
				?>
				<tr>
				<td width='20px'></td> 
				<td><?=ucwords(strtolower($pr2->ledg_name));?></th> 
				<td style='width:120px;text-align:right;'><?=number_format($pr2->debit,2,".","");?></td>
                <td style='width:120px;text-align:right;'><?=number_format($pr2->credit,2,".","");?></td> 
                </tr>
				<?php
				}
				}
				
				$gpl=$crtot-$drtot;
				if($gpl>0){	$gpl_text="GROSS PROFT";}else{$gpl_text="GROSS LOSS";}
				
				?>
				<!----1st- totals----------------------------------->
				<tr >
				<td width='20px'></td> 
				<td style='font-weight:700;font-size:13px;color:#000;'><b>TOTAL</b></td> 
				<td style='width:120px;text-align:right;border-top:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><b><?=number_format($drtot,2,".","");?></b></td>
                <td style='width:120px;text-align:right;border-top:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><b><?=number_format($crtot,2,".","");?></b></td> 
                </tr>
				
				<tr >
				<td width='20px'></td> 
				<td style='font-weight:700;font-size:13px;color:#000;'><b><?=$gpl_text;?></b></td> 
				<td style='width:120px;text-align:right;border-bottom:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'></td>
                <td style='width:120px;text-align:right;border-bottom:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><b><?=number_format($gpl,2,".","");?></b></td> 
                </tr>
				
				<tr ><td></td> 	<td></td> <td></td> <td></td> </tr>
				
				<!---- totals---ends----------------------------------->
				<?php
				$drtot1=0;
				// indirect expenses details
				foreach ($expenses1 as $pr3)
				{
					$drtot1+=$pr3->debit;
				?>
				<tr>
				<td width='20px'></td> 
				<td><?=ucwords(strtolower($pr3->ledg_name));?></td> 
				<td style='width:120px;text-align:right;'><?=number_format($pr3->debit,2,".","");?></td>
                <td style='width:120px;text-align:right;'><?=number_format($pr3->credit,2,".","");?></td> 
                </tr>
				<?php
				}
				
				$gpl1=$crtot-($drtot+$drtot1);
				if($gpl1>0){	$gpl_text1="NET PROFT";}else{$gpl_text1="NET LOSS";}

				?>
				<!----net proft----------------------------------->
				
				<tr >
				<td width='20px'></td> 
				<td style='font-weight:700;font-size:13px;color:#000;'><b>TOTAL</b></td> 
				<td style='width:120px;text-align:right;border-top:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><b><?=number_format($drtot1,2,".","");?></b></td>
                <td style='width:120px;text-align:right;border-top:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><b><?=number_format($gpl,2,".","");?></b></td> 
                </tr>
				<tr >
				<td width='20px'></td> 
				<td style='font-weight:700;font-size:13px;color:#000;'><b><?=$gpl_text1;?></b></td> 
				<td style='width:120px;text-align:right;border-bottom:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'></td>
                <td style='width:120px;text-align:right;border-bottom:2px solid #b7b7b7;font-weight:700;font-size:15px;color:#000;'><b><?=number_format($gpl1,2,".","");?></b></td> 
                </tr>
								
				
				</tbody> 
				</table>
			  </div>
        </div>

       </div>
</body>
</html>