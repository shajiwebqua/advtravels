<!DOCTYPE html>
<html>
<style  type="text/css">
 .fset
 {
   height:30px;
   width:100%;
   background-color:#d2d3d4; 
   border-radius:10px;
   padding-top:5px;
   margin-bottom:10px;
   text-align:center;
 }

</style>

<head>
  <meta charset="UTF-8">
  <title>Advance Travels</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" /> 
		<!-- Font Awesome Icons -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		  <!-- Ionicons -->
		  <!--<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" /> -->
		  <!-- Theme style -->
  <link href="<?php echo base_url('assets/dist/css/AdminLTE.min.css');?>" rel="stylesheet" type="text/css" />
		<!-- AdminLTE Skins. Choose a skin from the css/skins 
		folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/dist/css/jasny-bootstrap.min.css');?>" rel="stylesheet" media="screen">
		<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css')?>">
		<!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css')?>">
	  <!-- AdminLTE Skins. Choose a skin from the css/skins
	  folder instead of downloading all of them to reduce the load. -->
	  <!--<link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/additional.css')?>">-->


	  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	  <!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	  <![endif]-->
  
  
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/dist/css/jquery-modal.css');?>" rel="stylesheet" type="text/css" />
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css');?>" />
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.css');?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css')?>" rel="stylesheet"  type="text/css" /> 
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/font-awesome/css/font-awesome.min.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/search_tabs/selectize.css');?>">
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dist/css/sweetalert.css');?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.css');?>">
  
		  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		  <!--[if lt IE 9]>
		   <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		  <![endif]-->
		  
		
      </head>
      <body class="skin-green sidebar-mini">
	  <div class="wrapper">
		
		<!-- logo --------------------->
				<!--<div style='position:absolute;width:150px;height:80px;background-color:#fff;z-index:5000;margin:10px 0px 0px 43px;'>
				<img src="<?php echo base_url('upload/images/logo_dark.png');?>"  height='45px' alt="logo">
				</div> -->
		<!--------------------------->
			
          <header class="main-header">
		  
            <!-- Logo -->
            <a href="#" class="logo">
              <!-- mini logo for sidebar mini 50x50 pixels -->
              <span class="logo-mini" style='color:#5ab1e2;'><b>A<font color='red'>W</font>H</b></span>
              <!-- logo for regular state and mobile devices -->
              <span class="logo-lg">
			  <h4 style='color:#5ab1e2;'>Advance<font color='red'> World</font> Holidays</h4>
			  <!-- <img src='<?php echo base_url('upload/images/atravels_logo.png');?>' width='95%' ></span> -->
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
				<label id='nts' style='margin:10px 0px 0px 5px;font-size:18px;font-weight:bold;color:#000;'>ACCOUNTS</label>
				
              <!-- Sidebar toggle button-->
              <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
			             
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav ">
					<li class="dropdown user user-menu"> 
						<a href="<?php echo base_url('Login/home');?>" class="dropdown-toggle" style='height:50px !important;'> 
						<span class="hidden-xs"style='color:#585858;' ><button class='btn btn-warning'><i class="fa fa-backward" aria-hidden="true"></i>&nbsp;&nbsp;Travels App</button></span> </a>
					</li>
				
                  <li class="dropdown user user-menu"> 
                    <!-- Menu Toggle Button --> 
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                      <!-- The user image in the navbar--> 
                      <!-- <img src="<?php echo base_url('/dist/img/user.jpg')?>" class="user-image" alt="User Image"> -->
                      <img src="<?php echo $this->session->userdata('userimage');?>" class="user-image" alt="User Image"> 
                      <!-- hidden-xs hides the username on small devices so only the image appears. --> 
                      <span class="hidden-xs"><?php echo $this->session->userdata('name');?></span> </a>
                      <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header" >
                         <img src="<?php echo $this->session->userdata('userimage');?>" class="img-circle" alt="User Image">
                         <p style="color:#000;"><?php echo $this->session->userdata('name');?> </p>
                       </li>
                       
                       <li class="user-footer">
                        <div class="pull-left"> <!--<a href="<?php echo site_url('admin/profile') ?>" class="btn btn-default btn-flat">Profile</a> --> </div>
                        <div class="pull-right"> <a href=" <?php echo site_url('admin/logout') ?>" class="btn btn-default btn-flat">Sign out</a> </div>
                      </li>
                    </ul>
                  </li>
                  
                  <!-- Control Sidebar Toggle Button -->
                  <li style="margin-right:20px;"> </li>
                </ul>
              </div>
			  
              
            </nav>
			
          </header>
          <!-- Left side column. contains the logo and sidebar -->
          <div class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
              
              <!-- Sidebar user panel -->
              
        <?php
              $lvl="";
              $ul=$this->session->userdata('userlevel');
              if($ul=='1')
              {
               $lvl="Super Admin";
             }
             if($ul=='2')
             {
               $lvl="Administrator";
             }
             if($ul=='3')
             {
               $lvl="User";
             }
        ?>
             
             
            <div class="user-panel">
			 <!--<div class="pull-left image"> <img src="<?php echo $this->session->userdata('userimage');?>" class="img-circle" alt="User Image"> </div>
               <div class="pull-left info">
                 <p><?php echo $this->session->userdata('name');?></p>
                 <small style="color:#3c8dbc;"><?php echo $lvl;?></small>
               </div> -->
			   <div style='width:50px;background-color:#fff;z-index:5000;margin:5px 0px 0px 43px;'>
				<img src="<?php echo base_url('upload/images/logo_dark.png');?>"  height='50px' alt="logo">
				</div>
            </div> 
            
            <ul class="sidebar-menu" style='margin-top:5px;'>
             <!-- <li class="header">MAIN NAVIGATION</li> -->
              <li class="treeview">
                <a href="<?php echo site_url('Account/index') ?>">
                 <i class="fa fa-group"></i> <span>Dashboard</span>
               </a>
            </li>
			
			<li class="treeview">
				<a href="">
					<i class="fa fa-list"></i>
					<span>Revenue</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
				  	<li><a href="<?php echo site_url('Account/revenue') ?>"><i class="fa fa-caret-right"></i>Revenue Entry</a></li>
					<li><a href="<?php echo site_url('Account/revenue_list') ?>"><i class="fa fa-caret-right"></i>Revenue List</a></li>
				</ul>
             </li> 
			 
			<li class="treeview">
				<a href="">
					<i class="fa fa-list"></i>
					<span>Voucher Creation</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
				  	<li><a href="<?php echo site_url('Account/payment') ?>"><i class="fa fa-caret-right"></i>Payments</a></li>
					<li><a href="<?php echo site_url('Account/receipt') ?>"><i class="fa fa-caret-right"></i>Corporate Receipts</a></li>
				</ul>
             </li> 
   
            <li class="treeview">
				<a href="">
					<i class="fa fa-list"></i>
					<span>Account List</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
				  	<li><a href="<?php echo site_url('Account/lpayments') ?>"><i class="fa fa-caret-right"></i>Payments List</a></li>
					<li><a href="<?php echo site_url('Account/receipts') ?>"><i class="fa fa-caret-right"></i>Corporate Receipts List</a></li>
					<li><a href="<?php echo site_url('Account/DAccount') ?>"><i class="fa fa-caret-right"></i>Driver Accounts</a></li>
					<li><a href="<?php echo site_url('Account/SAccount') ?>"><i class="fa fa-caret-right"></i>Staff Accounts</a></li>
					<li><a href="<?php echo site_url('Account/vgroup') ?>"><i class="fa fa-caret-right"></i>Group Wise Payments</a></li>
				</ul>
             </li> 
			 
			 <li class="treeview">
				<a href="">
					<i class="fa fa-list"></i>
					<span>Reports</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
				  	<li><a href="<?php echo site_url('Account/payments') ?>"><i class="fa fa-caret-right"></i>Balance Sheet</a></li>
					<li><a href="<?php echo site_url('Account/receipts') ?>"><i class="fa fa-caret-right"></i>Profit & Loss A/c</a></li>
					<li><a href="<?php echo site_url('Account/journal') ?>"><i class="fa fa-caret-right"></i>Drivers Accounts</a></li>
					<li><a href="<?php echo site_url('Account/contra') ?>"><i class="fa fa-caret-right"></i>Corporates Account</a></li>
				</ul>
             </li> 

          <li>&nbsp;</li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </div>
 