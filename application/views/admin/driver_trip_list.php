<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}

.numericCol{
  text-align:right;
}
</style>

 <!-- Content Wrapper. Contains page content -->
 
<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Driver Trip Details</b></h1>

 </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header  end -->	  
	  
  <section class="content"> 
    <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>  
  <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">
    <div style="background-color:#fff;border-radius:3px;padding:15px; ">
					
	<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
<!--- search ---------------------------------------- -->
				
		<div class="row">
         <div class="col-md-5" >
              		<label class="col-md-5  control-label" style="padding-top:3px;text-align:right;"><b>Select Driver Name:</b> </label>
					<div class="col-md-7">
					<select class="form-control" id="drivers" name="drivers">
						<option value="">--Select Driver--</option>
						<?php
							foreach($drivers as $driver)
							{
								echo "<option value='".$driver->driver_id."'>".$driver->driver_name."</option>";
							}
						?>
					</select>
					</div>
          </div>
		  
          <div class="col-md-4" style='border-right:1px solid #e4e4e4;'>
            <div class="form-group">
              <label class="col-md-4 control-label" style="padding-top:3px;" >From Date : </label>
              <div class="col-md-5">
                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                  <input type="text" class="form-control date-pick" readonly id="datepicker1" value="<?php echo date('d-m-Y'); ?>">
                </div> 
              </div>
			   <div class="col-md-2">
            <button class="btn btn-success" id="btnget1">Get</button>
			</div>
            </div>
          </div>
			<div class="col-md-3">
            <div class="form-group">
              <label class="col-md-4 control-label" style="padding-top:3px;" >Month : </label>
              <div class="col-md-8">
			  <select name='drmonth' id='drmonth' class='form-control'>
			  <option value="0">----------</option>
			  <option value="1">January</option>
			  <option value="2">February</option>
			  <option value="3">March</option>
			  <option value="4">April</option>
			  <option value="5">May</option>
			  <option value="6">June</option>
			  <option value="7">July</option>
			  <option value="8">August</option>
			  <option value="9">September</option>
			  <option value="10">October</option>
			  <option value="11">November</option>
			  <option value="12">December</option>
			  </select>

              </div>

            </div>
          </div>

		  
          </div> 
	</div>

              <label style="background-color:#cecece;width:100%;height:1px;margin:20px 0px 20px 0px;"></label>
				</div>
			
				</form>
<!-- ***********************************************  search end  ******************************************** -->

				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                 <div class="row">
                   <div class="col-md-12" >
                            <!-- BEGIN BORDERED TABLE PORTLET-->

									<table class="table table-striped table-hover table-bordered" width='100%' id="example">
									  <thead>
										<tr style='color:#4b88ed'>
										 <th>Trip_ID</th>
										 <th>Trip_Date</th>
										 <th>From-To</th>
										 <th>Days</th>
										 <th>Trip_Charge</th>
										 <th>Driver_Batha</th>
										 <th>Total</th>
									   </tr>
									 </thead>
									<!-- <tfoot>
									   <td>Total</td>
									   <td></td>
									   <td></td>
									   <td></td>
									   <td></td>
									   <td id="driver_charge"></td>
									   <td id="driver_bata"></td>
									   <td id="driver_total"></td>
									 </tfoot>  -->
									 <tfoot>
									<tr style='font-size:17px;font-weight:700;'>
										<th></th>
										 <th></th>
										 <th></th>
										 <th>Totals : </th>
										 <th></th>
										 <th></th>
										 <th></th>
									</tr>
									</tfoot>
								   </table>
                  
						<label style="background-color:#cecece;width:100%;height:1px;"></label>
                        <div class="row">
						</div>
									
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>

</section>
</div>
<!-- /.content-wrapper --> 
<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------
 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
    endDate: 'now'
   });
   
       
  $("#drivers").change(function(){
	var drid = $('#drivers').val();
	driver_trips(drid,"0","0");
	$("#drmonth").val("0");
  });
  
  $("#btnget1").click(function(){
	var drid = $('#drivers').val();
	if(drid=="")
	{
		alert("Please select Driver name.");
		return;
	}
	
	var dt=$('#datepicker1').val();
 	var dt1=dt.split('-');
	var dt2=dt1[2]+"-"+dt1[1]+"-"+dt[0];
    
	driver_trips(drid,dt2,"0");
	  });
    
  $("#drmonth").change(function(){
	var drid = $('#drivers').val();
	var dm = $('#drmonth').val();
	if(drid=="")
	{
		alert("Please select Driver name.");
		return;
	}
	if(dm!="0"){
		driver_trips(drid,"0",dm);
	}
	
	  });
  
  function driver_trips(did,ddt,dmon)
  {
	    $('#example').DataTable( {
           "ordering": false,
           destroy: true,
           "processing": true,
		
			
			/*   var api = this.api();
			   
              length = api.rows().data().length;
              if(length > 0){
                driverCharge  = 0
                driverBata    = 0
                var i         = 0
                for(i;i<length;i++){
                  driverCharge  += parseInt(api.row(i).data().tcharge)
                  driverBata  += parseInt(api.row(i).data().dbata)
                }
                $("#driver_charge").text(driverCharge);
                $("#driver_bata").text(driverBata);
                $("#driver_total").text(driverCharge + driverBata);
                $("#driver_id").val(driver_id);
              }*/
			  
			  "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            tcharge = api
                .column(4)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
			
			tbata = api
                .column(5)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
				
			gtotal = api
                .column(6)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
				
 
            // Total over this page
            /*pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );*/
				
 
            // Update footer
            $( api.column(4).footer()).html(
                '₹ '+tcharge );
			 
			 $( api.column(5).footer()).html(
                '₹ '+tbata );
			  
			 $( api.column(6).footer()).html(
                '₹ '+gtotal );
        },
                  
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Driver/driver_triplist_ajax/" + did +"/"+ ddt +"/"+ dmon,
               },

         
	"aoColumnDefs": [
	{"width":"10%","targets":0},
    {"width":"12%","targets":4},
    {"width":"12%","targets":5},
	
      { "sClass": "numericCol", "aTargets":[4,5,6]},
    ],
	
        "columns": [
      { "data": "tripid" },
      { "data": "sedate"},
      { "data": "tfromto"},
      { "data": "days"},
      { "data": "tcharge"},
      { "data": "dbata" },
      { "data": "total" },
       ]	   
    });
    }
	

var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });	

 	
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

      //$.fn.dataTable.ext.errMode = 'throw';
</script>



