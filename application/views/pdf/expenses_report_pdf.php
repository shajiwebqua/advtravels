<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<title>Travels</title>
	<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" /> 
	<style type="text/css">
		
	table{
		font-size: 14px;
	}
		td{
			padding:7px 5px 7px 5px;

		}

		th{
			padding:7px 5px 7px 5px;
			
		}

		.center {
    		margin: auto;
    		width: 25%;
		}
		
	</style>
</head>
<body>
<table style="border:none;width:100%;">
		<tr>
			<td colspan=2 style="padding: 0px">
				<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" height='35px'>
			</td>
		</tr>
		<tr>
			<td style="padding:0px">
				<span style="font-size:15px;">Advance World Holidays</span><br>
				<span>PK & Sons Complex, East Moozhikkal</span><br>
				<span>NH 212, Calicut, Kerala, India - 673571</span><br>
				<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
				<span>Fax: 0091 495 2732 400</span>
			</td>
			<td width='150px' valign='top' align='right'>Date: <?php echo date('d-m-Y');?></td>
		</tr>
	</table>
	<hr style='margin-bottom:2px;'>
	 <table width="100%"><tr><td width='220px;'><h5>Vehicle Expense Report</h5></td><td><h5><b><?php echo $this->session->flashdata('exphead');?></b></h5></td></tr>
 </table>
 <hr style='margin-top:2px;'>
	<table class="table table-bordered table-sm">
		<thead>
			<tr>
				<th width="70px">ID</th>
				<th width="120px">Bill_Date</th>
				<th width="150px">Vehicle</th>
				<th width="130px">Exp_Type</th>
				<th >Narration</th>
				<th width="100px" >Amount</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$i = 1;
			$tot=0;
			foreach($results as $res)
			{
			?>
				<tr>
					<td><?=$res->expense_id;?></td>
					<td><?=$res->expense_billdate;?></td>
					<td><?=$res->vehicle_regno;?></td>
					<td><?=$res->etype_name;?></td>
					<td><?=$res->expense_narration;?></td>
					<td align='right'><?=number_format($res->expense_amount,"2",".","");?></td>
				</tr>
			<?php
			$tot+=$res->expense_amount;
			}
			?>
		</tbody>
	</table>
	<table width='100%' >
	<tr> <td></td>	<td width='150px' align='right' style='padding:0px;'>-------------------------</td>	</tr>
		<tr > <td align='right' style='font-size:16px;'>Total</td>	<td width='150px' align='right' style='font-size:16px;'><b><?=number_format($tot,"2",".","");?></b></td> </tr>
		<tr> <td></td> <td width='150px' align='right' style='padding:0px;'>===========</td>	</tr>
	</table>

</body>                             

</html>