<?php
 include('application/views/common/header.php');
 ?>
<!-- <link rel="stylesheet" href="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.css');?>" rel="stylesheet" type="text/css" />
 <script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
 <script src="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.js');?>"></script> -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/bootstrap/bootstrap.min.css');?>">-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/font-awesome/css/font-awesome.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/search_tabs/selectize.css');?>">
  
 
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id='bdy'> 
  
  <!-- Main content -->
    
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Service Trip Sheet</b></h1>
    <ol class="breadcrumb">
	</ol> 
    </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
  <!-- Content Header end -->
  <section class="content"> 
 
  <div class="row" >
  <div class="col-md-12">

  <!--Add new user details -->
<div style="background-color:#fff;border-radius:3px;padding:15px; ">

		   <div id='idmsg'style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id="msg"><?php echo $this->session->flashdata('message'); ?></div>
				 <div id="msgerr"><?php echo $this->session->flashdata('message1'); ?></div></center>
				 </div>
				 
 <div class="portlet-body form" >
  

    <div class="row">
    <div class="col-md-6" style='border-right:1px solid #e4e4e4;'> 
	<form id="#myForm" class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Services/add_service_trip')?>" enctype="multipart/form-data">
      <div class='form-group' id='custna'>
          <label class='col-md-4 control-label' style='padding-top:2px;'>Select Vehicle :</label>
           <div class='col-md-8' style='padding-left:0px'>
			  <section class="form-control " style='padding:0px;'>
				<select id="id2" class="form-controll_prop" placeholder="Choose Vehicle" name="vehicleid" required>
				  <option value=""></option>
				  <?php
					$cquery=$this->db->select("*")->from('vehicle_registration')->where('vehicle_available','0')->get()->result();
					foreach( $cquery as $r)
					{
						echo "<option value='".$r->vehicle_id."'>".$r->vehicle_regno."</option>";
					}
				  ?>
									  
				</select>
				</section>
				
				<!--<div class="select_btn1">                              
				<a href="<?php echo base_url('Customer/add_tripcustomer/2');?>" data-target="#myModal2" data-toggle='modal'><button id='custadd' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span></button></a>
				</div>	-->
				</div>
        </div>
		  
		<div class='form-group'>
			  <label class='col-md-4 control-label' style='padding-top:5px;'>Date : </label>
			  <div class='col-md-5' style='padding-left:0px'>
			  <input type='date' class='form-control' name='spsdate' id='spsdate' value="<?php echo date('Y-m-d');?>" required>
			  </div>
       </div>
	   
   	   
	    <div class='form-group'>
          <label class='col-md-4 control-label' style='padding-top:5px;'>Time : </label>
          <div class='col-md-5' style='padding-left:0px'>
          <input type='time' class='form-control' name='spstime' id="spstime" required>
          </div>
        </div>
	      
				
		<div class='form-group' style='padding-top:20px;'>
          <label class='col-md-4 control-label ' style='padding-top:5px;'>Driver Name : </label>
          <div class='col-md-8' style='padding-left:0px'>
				<select class="form-control" name="driverid" required>
				  <option value="">----------</option>
				  <?php
					$cquery=$this->db->select("*")->from('driverregistration')->where("driver_available","0")->get()->result();
					foreach( $cquery as $r)
					{
						echo "<option value='".$r->driver_id."'>".strtoupper($r->driver_name)."</option>";
					}
				  ?>
									  
				</select>
		  
          </div>
        </div>
			  
        <div class='form-group' >
          <label class='col-md-4 control-label' style='padding-top:5px;'>Vehicle From :</label>
          <div class='col-md-8' style='padding-left:0px'>
          <input type='text' class='form-control' name='vfrom' id='vfrom' required>
          </div>
        </div>
      
        <div class='form-group'>
          <label class='col-md-4 control-label ' style='padding-top:5px;'>Service Centre : </label>
          <div class='col-md-8' style='padding-left:0px'>
          <!-- <input type='text' class='form-control' name='srcentre' id="srcentre" required>-->
		  
		     <div class="input-group">
				<select class="form-control" name="srcentreid" aria-describedby="basic-addon2" required>
				  <option value="">----------</option>
				  <?php
					$cquery=$this->db->select("*")->from('service_centre')->get()->result();
					foreach( $cquery as $r)
					{
						echo "<option value='".$r->srcentre_id."'>".strtoupper($r->srcentre_name)."</option>";
					}
				  ?>
				</select>
				<!--<span class="input-group-btn"><button style='padding:0px;'><i class="fa fa-plus" aria-hidden="true"></i></button></span> -->
				<span class="input-group-btn">
        <a href='#myModalSC' data-toggle='modal' data-target='#myModalSC' ><button class="scentre btn btn-default" style='padding:4px 10px 4px 10px;' type="button"><i class="fa fa-plus" aria-hidden="true"></i></button></a>
      </span>
			</div>
          </div>
        </div>
		
	
		<div class='form-group'>
          <label class='col-md-4 control-label ' style='padding-top:5px;'>Required Services : </label>
          <div class='col-md-8' style='padding-left:0px'>
          <textarea rows='3' class='form-control' name='rservice'>Nil</textarea>
          </div>
        </div>
				

			<div class='row' style='padding-top:5px;'>
				<label class='col-md-4 control-label' style='padding-top:5px;'>Current KM : </label>
				<div class='col-md-4' style='padding-left:0px'>
				<input type='number' class='form-control' name='startkm' id='startkm' required>
				</div>
				<div class='col-md-1' style='padding-top:3px;'>
				<!--<a href="#"  data-toggle="modal" class="btn btn-default btn-xs" name="btncheck"  id="btncheck">Check</a> -->
				<input type='button' class="btn btn-default " name="btncheck"  id="btncheck" value="Check Extra KM" >
				</div>
			</div>
						
			<div class='row' style='padding-right:15px;margin-top:5px;'>
			<label class='col-md-4 control-label' style='padding-top:5px;'> </label>
			<div class='col-md-8' id='idextrakm' style="background-color:#e4e4e4;height:50px;border-radius:3px;padding-left:0px;">
				<div class='col-md-8 control-label' style='padding-top:5px; border-right:1px solid #fff;'>
				  <span id='extrakm'></span>
				</div>
				<div class='col-md-4 control-label' style='padding-top:15px;'>
				<input type='button' id='btnreason' data-toggle='modal' class='btn btn-info btn-xs' value='Set Reason'>
				</div>
			</div>
			</div>
	
		<label style="margin-top:20px;background-color:#cecece;width:100%;height:1px;"></label>
 		 <div class='form-group'>
          <div class='col-md-12' style='padding-left:0px'>
          <center><input type="submit" class="btn btn-success" style="padding:10px 50px 10px 50px;" id="idsavebtn" value='Save Service Trip'></center>
          </div>
        </div> 
		<label style="background-color:#cecece;width:100%;height:1px;"></label>
	
	</form>
	</div>
	
	
 <!-- second column --------------------------->

 
 <div class="col-md-6" style='border-right:1px solid #e4e4e4;'> 
 <div class='row'>
 <div class='col-md-10'>
 <label style='margin-left:15px;'><b>Service Trip List</b> </label>
 </div>
 <div class='col-md-2'>
 <a href="<?php echo base_url('Services/trip_list');?>"><button class='btn btn-warning' >More</button></a>
 </div>
 </div>
 <hr style='margin:5px 0px 10px 0px;'>
	 
 
 <table class="table table-striped table-hover table-bordered" id="example" style='color:#000;font-size:13px;'>
					<thead>
					<tr>
					<!-- <th>Edit</th> -->
					 <th>Action</th>
					 <th>ID</th>
					 <th>Vehicle</th>
					 <th>Service Centre</th>
					 <th>Driver</th>
					 <th>Date</th>
					</tr>
					</thead>
                </table>
  </div>
  </div>

	  
</div>

</div>
 </div>
 
  <!-- /.content --> 

</section>
</div>
<!-- /.content-wrapper --> 

<!-- Modal Dialog boxe-------------------->
<div class="modal fade draggable-modal" id="myModalSC" tabindex="-1" role="basic" aria- hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times </button>
				<h4 class="modal-title">Add New Service Centre</h4>
			</div>
			
			<!-- content -------------------------------------------->
			
			<div style="background-color:#fff;padding:10px; ">
   
				<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Services/add_service_centre')?>"> 
                  
						<div class="form-group">
							 <div class="row" style="margin-top:0px;">
									<label class="col-md-3 control-label" style="padding-top:0px">Centre Name</label>
								   <div class="col-md-9">
								   <input type="text" class="form-control"  name="srcentre" id='srcentre' required>
								 </div>
							 </div>
                        </div>
  
						<div class="form-group" >
							 <div class="row" style="margin-top:0px;">
								<label class="col-md-3 control-label" style="padding-top:0px">Address</label>
								 <div class="col-md-9">
								   <textarea rows=2 class="form-control"  name="sraddress"  id='sraddress' required></textarea>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
								<label class="col-md-3 control-label" style="padding-top:0px">Phone</label>
								 <div class="col-md-9">
								   
								   <input type="text" class="form-control"  name="srmobile"  id='srmobile' required>
								 </div>
							 </div>
                        </div>
 
                        <hr>

                    <div class="form-group">
                     <div class="row"><center>
                            <button class="btn btn-primary" id='submitagent'>Submit</button>
								   &nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
								   </center>
                           </div>
                       </div>
					   
                      </form>
                       </div>

				<!------------------------------------------------------------>
		</div>
		</div>
	<!-- /.modal-dialog -->
	</div>
</div>

<!-- end Dialog box ---------------------->


<?php include('application/views/common/footer.php');?>


<script type="text/javascript">

$("#example").DataTable();

$("#idextrakm").hide();


$("#idmsg").hide();
$("#msg").hide();
$("#msgerr").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------

//search customer

$('#id2').selectize({
     create: false
     , sortField: {
     //field: 'text',
     direction: 'asc'
   }
   , dropdownParent: 'body'
   });

   
       $('#example').dataTable( {
         destroy: true,
        "processing": true,
		"scrollX":true,
		"ordering":false,
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Services/service_trip_ajax",// json datasource
               },
	"columnDefs":[
	{"width":"7%","targets":0},
	{"width":"12%","targets":1},
	{"width":"15%","targets":4},
	],
        
        "columns": [
            { "data": "edit" },
            { "data": "id" },
            { "data": "vehicle"},
			{ "data": "scentre"},
			{ "data": "driver"},
            { "data": "sdate" },
      
       ]
  } );
   
  
 
 
 
$('#btncheck').click(function () {
	
	//var Result=$("#myModal3 .modal-body");
	
	var km=$("#startkm").val();
	var vid=$("#id2").val();
	
	if(km=="" && vid=="")
	{
		swal("Try Again","Select Vehicle and Strting KM.","error");
	}
	else
	{
		//alert(km+"-"+vid);
		if (vid=='')
		{
			swal("Try Again","Please select vehicle..!","error");
			//alert("Please select vehicle...");	
			
		}
		else if(km=='')
		{
			//alert("Please type stating kilometer...");	
			swal("Try Again","Please type starting kilometer.","error");
		}
		else
		{
			
			$("#idextrakm").show();
			$("#btncheck").attr("data-target","#myModal3");
			jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "Trip/check_kilometer",
			dataType: 'html',
			data: {skm:km,vid:vid},
			success: function(res) {
				
				if(res=="0")
				{
					$("#extrakm").html("Ending Km= <b>" + km + "</b><br>No diffrence, Continue.");
					$("#btnreason").hide();
				}
				else
				{
				$("#btnreason").show();	
				$("#extrakm").html(res);
				}
			//Result.html(res);
			}
			});
		}
	}
});


$("#btnreason").click(function()
{
	var Result=$("#myModal3 .modal-body");
	var km=$("#startkm").val();
	var vid=$("#id2").val();

		$("#btnreason").attr("data-target","#myModal3");
		
	    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/set_extra_km_reason",
        dataType: 'html',
        data: {skm:km,vid:vid},
        success: function(res) {
		Result.html(res);
        }
		});
});

//-----------------------------------------

 $(document).on("click", "#del_conf1", function () {
    return confirm('Are you sure you want to delete this entry?');
  });

</script>

<script>
$('#example1 tbody').on('click', '.select', function () {
 var Result=$("#myModals .modal-body");
 var id =  $(this).attr('id');
 $('#regno').val( id );
 $('#myModals').modal('hide')
});
</script>
