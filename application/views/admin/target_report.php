<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
 <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <b>Monthly Targets Report</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px;'>
		<li><a id='view-pdf' href="<?php echo base_url('Reports/Monthly_Target');?>" style='color:#4b88ed;'><button style='padding:2px 20px 2px 20px;'><i class="fa fa-print" aria-hidden="true"></i> Print</button></a></li>
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">
  <!-- Add new user details -->
              
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
   <div class="page-content">
   <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>

<?php
		$dtab=$this->session->userdata('doctab');
?>
	
		<div style="background-color:#fff;padding:3px 10px 3px 10px;">
			<div class='row' style='padding:10px;'>
			  <div class='col-md-12' style='background-color:#e4e4e4;padding:10px;'>
			  			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/Target_Report/0');?>" enctype="multipart/form-data">
                      <label class="col-md-2 control-label" style='padding-top:5px;text-align:right;'>Select Month</label>
                           <div class="col-md-3">
						   <select name='month' class='form-control' required>
								<option value='' >---month----</option>
								<option value='1'>JANUARY</option>
								<option value='2'>FERUARY</option>
								<option value='3'>MARCH</option>
								<option value='4'>APRIL</option>
								<option value='5'>MAY</option>
								<option value='6'>JUNE</option>
								<option value='7'>JULY</option>
								<option value='8'>AUGUST</option>
								<option value='9'>SEPTEMBER</option>
								<option value='10'>OCTOBER</option>
								<option value='11'>NOVEMBER</option>
								<option value='12'>DECEMBER</option>
						   </select>
						   </div>
						   
						   <div class="col-md-1" style='padding-left:0px;padding-right:0px;'>
						   <select name='year' class='form-control' required>
								<option value='' >-year-</option>
								<?php
								$y=date('Y')+1;
								for($x=$y;$x>=2017;$x--)
								{
								?>
								<option value='<?=$x;?>'><?=$x;?></option>
								<?php
								}
								?>
								
						   </select>
						   </div>
						   
						   <div class="col-md-2" >
						   <button type='submit' class='btn btn-primary' style='margin-top:1px;padding:3px 15px 3px 15px;'>Get Details</button>
						   </div>
						   </form>
						   
						   
            </div>
			
			 <div class='row'>
			<hr style='margin:0px 0px 15px 0px;'>
			</div>
			
				<div class='row' style='padding:5px 15px 5px 15px;' >
				<div class='col-md-8'>
			<table class="table table-striped table-hover table-bordered" id="example" width='100%'>
                <tr>
								 
				 <?php
				 $month=["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVENMBER","DECEMBER"];
				 $tamount="0";
				 $camt="0";
				 $diff="0";
				 if (isset($result))
				 {
				 foreach($result as $mt)
				 {
					 foreach($result1 as $ct)
					 {
						 if($ct->trip_vehicle_id==$mt->tar_vehicle)
						 {
							 $camt=$ct->camount;
							 $tamount=$mt->tar_amount;
							 $diff=number_format(($ct->camount-$mt->tar_amount),"2",".","");
							 if($diff<=0)
							 {
								 $diff="<font color=red>".$diff."</font>";
							 }
							 else
							 {
								 $diff="<font color=green>".$diff."</font>";
							 }
				 ?>
				 <tr>
				 <td>
					<table width='100%' style='font-size:16px;'>
					<tr style='height:35px;'><td width='400px'>Vehicle Registration No</td><td width='20px'> : </td><td><b><?=$mt->vehicle_regno;?></b></td></tr>
					<tr style='height:35px;'><td >Month</td><td width='20px'> : </td><td><?=$month[$mt->tar_month-1]."&nbsp;&nbsp;-&nbsp;&nbsp;". $mt->tar_year ;?></td></tr>
					<tr style='height:35px;'><td>Target Amount of this month</td><td width='20px'> : </td><td align='right'><?="<b>&#8377;&nbsp;&nbsp;".number_format($mt->tar_amount,"2",".","")."</b>";?></td></tr>
					<tr style='height:35px;'><td>Total Collected Amount</td><td width='20px'> : </td><td align='right'><?="<b>&#8377;&nbsp;&nbsp;".number_format($ct->camount,"2",".","")."</b>";?></td></tr>
					<tr ><td></td><td width='20px'> : </td><td align='right'>===============</td></tr>
					<tr style='height:35px;font-size:18px;'><td>Difference</td><td width='20px'> : </td><td align='right'><?="<b>&#8377;&nbsp;&nbsp;".$diff."</b>";?></td></tr>
					<tr height='50px;'><td></td><td width='20px'> </td><td align='right' valign='top'>===============</td></tr>
					
					</table>
					
				 </td>
				 </tr>
				<?php
						}
					 }
				 }
				 }
				 ?>
				
				 
            </table>
			</div>
		</div>
                <!-- END BORDERED TABLE PORTLET-->
	</div>						

       
</div>
  </div>
</div>
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<!-- modal windows --->

		<div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >

              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
		 
<!--- End -------------------------->


<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
  {
    swal($("#msg").html(),"","success")
    $("#msg").html("");
  }

 $('#view-pdf').on('click',function(){
     
		var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        $.createModal({
        title:'Monthly Targets',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
	   return false; 

    });  
	
	    
    
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

	

 //});
  


</script>



