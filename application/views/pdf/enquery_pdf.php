<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<title>Travels</title>
	<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" /> 
	<style type="text/css">
		
	table{
		font-size: 10px;
	}
		td{
			padding:7px 5px 7px 5px;

		}

		th{
			padding:7px 5px 7px 5px;
			
		}

		.center {
    		margin: auto;
    		width: 25%;
		}
		
	</style>
</head>
<body>

	<table style="border:none;width:100%;">
		<tr>
			<td width="150px" style="padding: 0px">
				<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
			</td>
		</tr>
		<tr>
			<td style="padding: 0px">
				<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
			</td>
		</tr>
	</table>
	<hr>
	 <table width="100%"><tr><td><h4>View All Enquiries </h4></td></tr>
 
 </table>
 <hr>
	<table class="table table-bordered table-sm">
		<thead>
			<tr>
				<th width="10px">Sl No</th>
				<th width="50px">Starting Date & Time</th>
				<th width="105px">Party Name</th>
				<th width="50px">Phone No</th>
				<th width="100px">Address</th>
				<th width="200px" >Details</th>
			    <th width="50px">Returning Date & Time</th>
			    <th width="50px">Returning Place & Time</th>
			    <th width="50px">Amount</th>
			    <th width="50px">Upto(per km)</th>
			     <th width="50px">(per km)</th>
			     <th width="50px">Start From</th>
			       <th width="50px">Permit Charge</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$i = 1;
			foreach($results as $result){
				echo "
				<tr>
					<td>$result->sl_no</td>
					<td>$result->starting_date - $result->starting_time</td>
					<td>$result->party_name</td>
					<td>$result->phone_no , $result->mobile_no</td>
					<td>$result->postal_address</td>
					<td> 
						Journey Details : $result->journey_details <br />
						Nature : $result->nature_of_journey <br />
						Preferred Vehicle : $result->prefered_vehicle <br/>
						Passengers : $result->no_of_passengers <br/>
						Days : $result->no_of_days <br/>
					</td>
						<td>$result->returning_date - $result->returning_time</td>
					   <td>$result->returning_place_time</td>
					   <td>$result->amount</td>
					   <td>$result->upto_km</td>
					   <td>$result->running_charge</td>
					   <td>$result->from</td>
					   <td>$result->permit_charges</td>


				</tr>
				";
			}

			?>
		</tbody>
	</table>


</body>                             

</html>