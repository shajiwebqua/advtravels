<?php
class Model_customer extends CI_Model{
	
    public function form_insert($userdt)
    {
       $this->db->insert('customers', $userdt);  
    }
		
	 public function insert_gst($gst)
    {
       $this->db->insert('customer_gst', $gst);  
    }

public function view_customer()
	{
    $this->db->select('*');
    $this->db->from('customers');
	$this->db->join('country','country.country_id=customers.customer_country','left');
	$this->db->join('area','area.area_id=customers.customer_area','left');
    $this->db->join('agents','agents.agent_id=customers.customer_coagent','left');
	$this->db->order_by("customer_id","desc");
	$query = $this->db->get();
    return $query->result();
}


public function view_corporates()
	{
    $this->db->select('*');
    $this->db->from('customers');
	$this->db->where('customers.customer_type',"2");
	$query = $this->db->get();
    return $query->result();
}

public function view_customer1($id)
{
   $this->db->select('*');
    $this->db->from('customers');
	$this->db->join('country','country.country_id=customers.customer_country','inner');
	$this->db->join('area','area.area_id=customers.customer_area','inner');
	$this->db->join('category','category.category_id=customers.customer_category','inner');
    $this->db->join('agents','agents.agent_id=customers.customer_coagent','inner');
	$this->db->where('customers.customer_id',$id);
	$this->db->order_by("customer_id","desc");
    $query = $this->db->get();
    return $query->row();
}

public function view_country(){

    $this->db->select('*');
    $this->db->from('country');
    $query = $this->db->get();
    return $query->result();
}

public function view_area($id)
{
    $this->db->select('*');
    $this->db->from('area');
	$this->db->where('area_countryid',$id);
    $query = $this->db->get();
    return $query->result();
}

public function view_category(){

    $this->db->select('*');
    $this->db->from('category');
    $query = $this->db->get();
    return $query->result();
}

public function view_agents(){
    $query = $this->db->get('agents');
    return $query->result();
}


 public function update_customer($id,$userdt)
  {
    $this->db->where('customer_id', $id);
    $this->db->update('customers',$userdt); 
  } 
  
 public function update_corporate($id,$contacts)
  {
    $this->db->where('customer_id', $id);
    $this->db->update('customers',$contacts); 
  } 
   
  
  
 public function form_insertlog($userdt1)
    {
       $this->db->insert('log', $userdt1);  
    }



  public function delete_customer($id)
{
    $this->db->where('customer_id', $id);
    $this->db->delete('customers');
    return $this->db->affected_rows();
}

}
?>