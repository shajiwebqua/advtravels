<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class vehicle extends CI_Controller {

function __construct(){
parent::__construct();
$user = $this->session->userdata('userid');
     if (!isset($user))
     { 
       redirect('/');
     } 
     else
     { 
        return true;
     }
$this->load->helper(array('form', 'url'));
$this->load->model('Model_vehicle');
}

public function index()
{
   $this->load->view('admin/vehicle');
}
  
public function addvehicle()
{
 //  $data['lastid']=$this->uri->segment(3);	
   $this->load->view('admin/add_vehicle');
   }

public function vehicle_expense()
{
  $this->load->view('admin/vehicle_expense');
}

public function add_exp_type()
{
	$type=$this->input->post('exptype');
	$egroup=$this->input->post('expgroup');
	$expdt = array(
         'etype_name' => strtoupper($type),
		 'etype_group' => $egroup,
		);
	$exp1=$this->db->insert('expense_type',$expdt);
	echo $this->db->insert_id();
	
}
  
public function add_new_vehicle()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('regno', 'no', 'required');
$this->form_validation->set_rules('vtype', 'vehicle type', 'required');
$this->form_validation->set_rules('vamount', 'vehicle amount', 'required');


 if($this->form_validation->run())
     {
    $date=date('Y-m-d');
    $regno=$this->input->post('regno');
    $type=$this->input->post('vtype');
    $seats=$this->input->post('seats');
	$amount=$this->input->post('vamount');
	
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
    
       $userdt = array(
         'vehicle_date' => $date,
         'vehicle_regno' => $regno,
         'vehicle_type' =>$type,
         'vehicle_noofseats' =>$seats,
         'vehicle_amount' =>$rentperiod,
         'vehicle_status'=>"1",
		 'vehicle_available'=>"0",
		);
	
        $this->load->Model('Model_vehicle');
		$ins_id="";
        $ins_id=$this->Model_vehicle->form_insert($userdt);
		
       
          $uid = $this->db->insert_id();
        $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Vehicle Details Added'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
		   $this->session->set_flashdata('message', 'Vehicle details successfully added..!');
      	redirect('Vehicle/addvehicle');  
    }
 
 else{
         $this->session->set_flashdata('message1', 'Data Not Inserted Successfully');
        //$data['message']="Data Inserted Successfully";

      redirect('Vehicle/addvehicle');
       }  
}

public function show_report()  // call report from vehicle list
{
  $this->load->helper("data");
  $this->load->model('Model_vehicle');
  $regid=urldecode($this->uri->segment(3));
  
  if($regid=="0"|| $regid=="")
  {
	  redirect("Vehicle/index");
  }
  else
  {
  
  $regno=$this->db->select('vehicle_regno')->from('vehicle_registration')->where("vehicle_id",$regid)->get()->row();
  
  $data["regid"] = $regid;
  $data["vehicleno"] = $regno->vehicle_regno;
  $data["tripcharge"] =$this->Model_vehicle->tripcharge($regid);
  $data["expense"] =$this->Model_vehicle->get_expenses($regid);
  $data["service"] =$this->Model_vehicle->get_service_expense($regid);
  $data["loan"] =$this->Model_vehicle->get_vehicle_loan($regno->vehicle_regno);
  $this->load->view("admin/vehicle_report",$data);
  }
}



public function vreport()  // call  vehicle-report from report menu
{
  $this->load->helper("data");
  
  $this->load->model('Model_vehicle');
  $regid=$this->input->post('vregid');
  
  if($regid=="0")
  {
	  $this->session->set_flashdata('message1', 'Vehicle Reg.No Missing.');
	  redirect("Reports/vehicle");
  }
  else
  {
  $regno=$this->db->select('vehicle_regno')->from('vehicle_registration')->where("vehicle_id",$regid)->get()->row();
  
  $data["regid"] = $regid;
  $data["vehicleno"] = $regno->vehicle_regno;
  $data["tripcharge"] =$this->Model_vehicle->tripcharge($regid);
  $data["expense"] =$this->Model_vehicle->get_expenses($regid);
  $data["service"] =$this->Model_vehicle->get_service_expense($regid);
  $data["loan"] =$this->Model_vehicle->get_vehicle_loan($regno->vehicle_regno);
    
  $this->load->view("admin/vehicle_report",$data);
  }
}

public function vallreport()  // all vehicle-report from report menu
{
  $this->load->model('Model_vehicle');
  $data["regid"] = $this->Model_vehicle->getAll_vehicle_No();
  $this->load->view("admin/view_allvehicle_report",$data);
}

public function vall_report()  
{
  $this->load->model('Model_vehicle');
  $regid=$this->input->post('vregno');
  $tedate1=$this->input->post('sdate1');
  $tedate2=$this->input->post('edate1');

  if($tedate1=="" || $tedate2=="")
  {
	  redirect("Reports/vehicle");
  }
  else
  {
	$ted1 =explode("-",$tedate1);
	$tedat1=$ted1[2]."-".$ted1[1]."-".$ted1[0];

	$ted2 =explode("-",$tedate2);
	$tedat2=$ted2[2]."-".$ted2[1]."-".$ted2[0];
	$data['sdate']=$tedat1;
	$data['edate']=$tedat2;
	$data['period']="Period : ".$tedate1."&nbsp;&nbsp;&nbsp; to &nbsp;&nbsp;&nbsp;". $tedate2;
	$data["regid"] = $this->Model_vehicle->getAll_vehicle_No();
	$this->load->view("admin/view_all_vehicle_report",$data);
  }
}


public function vdreport()   //------------date wise vehicle report
{
  $this->load->helper("data");
  
  $this->load->model('Model_vehicle');
  $regid=$this->input->post('vregno');
  $tedate1=$this->input->post('sdate');
  $tedate2=$this->input->post('edate');

	$ted1 =explode("-",$tedate1);
	$tedat1=$ted1[2]."-".$ted1[1]."-".$ted1[0];

	$ted2 =explode("-",$tedate2);
	$tedat2=$ted2[2]."-".$ted2[1]."-".$ted2[0];

   
  if($regid=="0")
  {
	  $this->session->set_flashdata('message1', 'Vehicle Reg.No and Dates Missing.');
	  redirect("Reports/vehicle");
  }
  else
  {
  $regno=$this->db->select('vehicle_regno')->from('vehicle_registration')->where("vehicle_id",$regid)->get()->row();
  
  $data["regid"] = $regid;
  $data["vehicleno"] = $regno->vehicle_regno;
  $data["sedates"]=$tedate1." -- ".$tedate2;
  $data["tripcharge"] =$this->Model_vehicle->tripcharge_date($regid,$tedat1,$tedat2);
  $data["expense"] =$this->Model_vehicle->get_expenses_date($regid,$tedat1,$tedat2);
  $data["service"] =$this->Model_vehicle->get_service_expense_date($regid,$tedat1,$tedat2);
  $data["loan"] =$this->Model_vehicle->get_vehicle_loan($regno->vehicle_regno);
    
  $this->load->view("admin/vehicle_report_date",$data);
  }
}





             
public function vehicle_ajax()
{
        $this->load->model('Model_vehicle');
        $results=$this->Model_vehicle->view_vehicle();
		
        $data1 = array();
        foreach($results  as $r) 
               {
                $edit="<a href='#myModal2' id='$r->vehicle_id' data-toggle='modal' class='edit'><button class='btn btn-default btn-xs' data-title='Edit' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></a>";
			    $mm="<button class='btn btn-default btn-xs' style='margin-left:5px' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></button>";
				$del=anchor('Vehicle/del_entry/'.$r->vehicle_id, $mm, array('id' =>'del_conf'));
				
				// vehicle availablility button
				$mm1="<button class='btn btn-success btn-xs' style='margin-left:5px' >Available</button>";
				$ava1=anchor('Vehicle/change_availability1/'.$r->vehicle_id, $mm1, array('id' =>'change_conf'));
				$mm2="<button class='btn btn-warning btn-xs' style='margin-left:5px' >Not Available</button>";
				$ava2=anchor('Vehicle/change_availability2/'.$r->vehicle_id, $mm2, array('id' =>'change_conf'));
				//--------------------------
				
            // $del=anchor('vehicle/del_entry/'.$r->vehicle_id, 'Delete', array('id' =>'del_conf'));
        
		if($r->vehicle_status==1)
		{
			$st="<font color='green'>Active</font>";
		}
		else
		{
			$st="<font color='red'>Inactive</font>";
		}
		
		if($r->vehicle_available==0)
		{
			$available=$ava1;
		}
		else
		{
			$available=$ava2;
		}

		array_push($data1, array(

					"action"=>$edit,
					"action1"=>$edit.$del,
					"vid"=>"$r->vehicle_id",
					"date"=>"$r->vehicle_date",
					"regno"=>"$r->vehicle_regno",
					"vtype"=>"$r->veh_type_name",
					"seats"=>"$r->vehicle_noofseats",
					"vamt"=>"$r->vehicle_amount",
					"status"=>$st,
					"available"=>$available,
					"report" => "<a href='" . base_url('Vehicle/show_report/'.$r->vehicle_id) . "' class='btn btn-info btn-xs'>Get Report</a>"
			  ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

public function change_availability1()
  {
     $vid=$this->uri->segment(3);
    
	$this->db->where("vehicle_id",$vid);
	 $updt = array(
            'vehicle_available' =>"1",
            );
	 $this->db->update("vehicle_registration",$updt);
     redirect('Vehicle/index');
  }
  
  
  public function change_availability2()
  {
     $vid=$this->uri->segment(3);
    
	$this->db->where("vehicle_id",$vid);
	 $updt = array(
            'vehicle_available' =>"0",
            );
	 $this->db->update("vehicle_registration",$updt);
     redirect('Vehicle/index');
  }
  


 public function edit_vehicle() 
	{
	$m="";
    $f="";
    $vop=$this->uri->segment(3);
	
    $data=$this->session->all_userdata();
    $id=$this->input->post('uid');
    $query = $this->db->select('*')->from('vehicle_registration')->where('vehicle_id',$id)->get();
    $row = $query->row();
     
    
    echo "           
    <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>    
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit vehicle</font></h4>
    </div>
    <br>

           <form  class='form-horizontal' role='form' method='post' action='". site_url('vehicle/update_vehicle'). "' enctype='multipart/form-data'>
                    <!-- text input -->
                    
   <!--              <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Date</label>
                    <div class='col-md-8'>  
         
          <input type='text' name='date' class='form-control' value=''/>
          </div>                   
          </div> -->

		  
         <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Vehicle Reg. No </label>
                    <div class='col-md-8'>  
          
          <input type='text' name='regno' class='form-control'  id='mask_date2' value='$row->vehicle_regno'  required/>
          </div>                   
          </div>

          <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Type </label>
                    <div class='col-md-8'> ";
          
						$section = $this->db->select("*")->from("vehicle_types")->get();
						$sectn1=$section->result();
						echo"<select type='text' name='vtype' class='form-control' >";
						foreach ($sectn1 as $ro):
                               echo "<option value='".$ro->veh_type_id."'";
                             if($row->vehicle_type==$ro->veh_type_id) echo "selected";
                                echo" > ".$ro->veh_type_name." </option>";
                                endforeach;
                             echo"</select >
                               </div>                     
                       </div> ";
                       echo"
          </div>                   
          </div>
       
        <div class='form-group'>
          <label class='col-md-3  control-label'>Seats </label>
          <div class='col-md-8'>
          <input type='number' class='form-control' name='seats' value='$row->vehicle_noofseats' required>
            </div>
          </div>
		  
		<div class='form-group'>
          <label class='col-md-3  control-label'>Vehcicle Price </label>
          <div class='col-md-8'>
          <input type='number' class='form-control' name='vamount' value='$row->vehicle_amount' required>
            </div>
          </div>
		  
				  <div class='form-group'>
                   <label class='col-md-3  control-label'>Status </label>
				  <div class='col-md-8'>
                      <select type='text' name='status' class='form-control'>";
 
					   echo '<option value="0" ';if($row->vehicle_status=='0')echo "selected"; echo'>Inactive</option>
							<option value="1"';if($row->vehicle_status=='1')echo "selected"; echo ">Active</option>
							</select>
                               </div>                     
                       </div>
					   					   
					   
	   
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-8'>
                <input type='hidden' name='uid'  value='$row->vehicle_id'/>
				<input type='hidden' name='vop'  value='$vop'/>
                <input type='submit' class='btn btn-primary' value='Update'/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>
<!--		   <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div> -->  ";
    }
		
public function update_vehicle()
{

$this->form_validation->set_rules('regno', 'no', 'required');
$this->form_validation->set_rules('vtype', 'vehicle type', 'required');

 if($this->form_validation->run())
    {
   
    $vop=$this->input->post('vop');
   
    $regno=$this->input->post('regno');
    $type=$this->input->post('vtype');
    $seats=$this->input->post('seats');
    $vamt=$this->input->post('vamount');
    $status=$this->input->post('status');
        
    $this->load->model('Model_vehicle');
	
    $uid=$this->input->post('uid');
	
      $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
	  
      $date=date("Y/m/d");
	  date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa'); 
    
     $newdt = array(
           'vehicle_regno' =>$regno,
		   'vehicle_type' =>$type,
           'vehicle_noofseats' =>$seats,
		   'vehicle_amount' =>$vamt,
           'vehicle_status'=>"1"
           );
  
        $this->Model_vehicle->update_vehicle($uid,$newdt);
       
         $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Vehicle Details Updated'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
		  
		  $this->session->set_flashdata('message',  'Vehicle details updated..!');
		  if($vop==1)
		  {
		  	  redirect('Vehicle/index');
		  }
		  else
		  {
			  redirect('Vehicle/addvehicle');
		  }

       }
        else 

        {
			$this->session->set_flashdata('message1',  'Details missing.!');
            if($vop==1)
		  {
		  	  redirect('Vehicle/index');
		  }
		  else
		  {
			  redirect('Vehicle/addvehicle');
		  }
        }                   
}



  

  public function del_entry()
  {
    
            $id=$this->uri->segment(3, 0);
            $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
       $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
            $this->load->model('Model_vehicle');
            $result=$this->Model_vehicle->delete_vehicle($id);
            if($result)
            {
                    $this->session->set_flashdata('message', 'Vehicle details has been deleted..!');
                    $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$id,
            'log_desc'=>'Vehicle Details Deleted'

            );
                     $this->load->model('Model_customer');
                    $this->Model_customer->form_insertlog($userdt1);
                   redirect('vehicle/index');
            }
           else
           {
            $this->session->set_flashdata('message', 'Please try again.');
            redirect('vehicle/index');
           }
  }
  
  
  public function acc_del_entry()
  {
			$vid=$this->input->post('alid');
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_vehicle');
            $result=$this->Model_vehicle->delete_accessory($id);
            if($result)
            {
                   $this->session->set_flashdata('message', 'Accessory details has been deleted..!');
                   redirect('vehicle/accessory/'.$vid);
            }
           else
           {
            $this->session->set_flashdata('message', 'Please try again.');
            redirect('vehicle/accessory/'.$vid);
           }
  }
  
  
  //Services --------------------------------------------------------------------------------
  
  public function service_vehicles()
  {
	$this->load->view('admin/service_requiredlist');
  }
    
  
  public function periodic_service_ajax($op)
{
        $this->load->model('Model_vehicle');
		if($op==1)
		$results=$this->Model_vehicle->view_dash_PSvehicles();
		else
		$results=$this->Model_vehicle->view_PSvehicles();
		
        $data1 = array();
        foreach($results  as $r) 
        {
        $edit1="<a href='#myModal2' id='$r->ser_vehicle_id' data-toggle='modal' class='edit1 open-AddBookDialog2'><center><button data-toggle='modal' >Set to Service</button></center></a>";
		$edit2="<a href='#myModal2' id='$r->ser_vehicle_id' data-toggle='modal'  class='edit2 open-AddBookDialog2'><center><button >Return Vehicle</button></center></a>";
		//$mm="<center><button class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></button></center>";
		$mm="<center><span class='glyphicon glyphicon-trash'></span></center>";
		$del=anchor('vehicle/acc_del_entry/'.$r->ser_id, $mm, array('id' =>'del_conf'));
				
        // $del=anchor('vehicle/del_entry/'.$r->vehicle_id, 'Delete', array('id' =>'del_conf'));
 		
		$st="";		
		if($r->ser_status=='1')
		{
			$st="<font color='red'>Service Needed.</font>";
			$ed="";
			$ed1="$edit1";
		}
		else 
		{
			$st="<font color='green'>Servicing.</font>";
			$ed1="";
			$ed="$edit2";
		}
		                 array_push($data1, array(
                            "sid"=>"$r->ser_vehicle_id",
							"regno"=>"$r->vehicle_regno",
							"vtype"=>"$r->veh_type_name",
                            "kilometer"=>"$r->ser_kilometer2",
							"status"=>"$st",
							"service"=>"$ed1",
							"vreturn"=>"$ed",
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}
  

public function periodic_servicing_ajax()
{
        $this->load->model('Model_vehicle');
        $results=$this->Model_vehicle->view_PServicing();
		
        $data1 = array();
        foreach($results  as $r) 
               {
        //$edit="<a href='#myModal2' id='$r->accessory_id' data-toggle='modal' class='edit open-AddBookDialog2'><center><button class='btn btn-primary btn-xs' data-title='Edit' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
		//$mm="<center><button class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></button></center>";
		$mm="<center><span class='glyphicon glyphicon-trash'></span></center>";
		$del=anchor('vehicle/acc_del_entry/'.$r->ser_id, $mm, array('id' =>'del_conf'));
        // $del=anchor('vehicle/del_entry/'.$r->vehicle_id, 'Delete', array('id' =>'del_conf'));
 		
		$st="";		
		if($r->ser_status=='2')
		{
			$st="<font color='red'>Servicing.</font>";
		}
                      array_push($data1, array(
                            "sid"=>"$r->ser_vehicle_id",
							"regno"=>"$r->vehicle_regno",
							"vtype"=>"$r->veh_type_name",
                            "kilometer"=>"$r->ser_kilometer2",
							"status"=>"$st",
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

 public function servicing_vehicles()
  {
  $this->load->view('admin/servicing_vehicle');
  }
  
 public function set_toService() 
	{
    $id=$this->input->post('vid');
    $query = $this->db->select('*')->from('vehicle_registration')->where('vehicle_id',$id)->get();
    $row = $query->row();
    
	$query1 = $this->db->select('*')->from('services')->where('ser_vehicle_id',$id)->get();
    $row1 = $query1->row();
	
    echo "           
    <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>    
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit vehicle</font></h4>
    </div>
    <br>

   <form  class='form-horizontal' role='form' method='post' action='". site_url('Vehicle/settoservice/'.$id). "' enctype='multipart/form-data'>
     
	     <div class='form-group'>
             <label class='col-md-3 control-label' style='padding-top:10px;'>Registration No </label>
             <div class='col-md-8'>  
             <label class='control-label' style='padding-top:10px;'>$row->vehicle_regno</label>
          </div>                   
          </div>

          <div class='form-group'>
            <label class='col-md-3 control-label' style='padding-top:10px;'>Status </label>
          <div class='col-md-8'>  
            <label class=' control-label' style='padding-top:10px;'><font color='red'>Service Needed.</font></label>
          </div>                   
          </div>
		  <div class='form-group'>
            <label class='col-md-3 control-label' style='padding-top:10px;'>Current Kilometer </label>
          <div class='col-md-8'>  
            <label class=' control-label' style='padding-top:10px;'>$row1->ser_kilometer2</label>
          </div>                   
          </div>
		  <div class='form-group'>
            <label class='col-md-3 control-label' style='padding-top:10px;'>Previous Service KM </label>
          <div class='col-md-8'>  
            <label class='control-label' style='padding-top:10px;'>$row1->ser_kilometer1</label>
          </div>                   
          </div>
		<hr style='width:100%'>		  
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-8'>
                <input type='hidden' name='uid'  value='$row->vehicle_id'/>
                <input type='submit' class='btn btn-primary' value='Set this vehicle to service'/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>
<!--		   <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div> -->  ";
    
    }
  
   public function returnvehicle() 
	{
    $id=$this->input->post('vid');
    $query = $this->db->select('*')->from('vehicle_registration')->where('vehicle_id',$id)->get();
    $row = $query->row();
    
	//$query1 = $this->db->select('*')->from('services')->where('ser_vehicle_id',$id)->get();
    //$row1 = $query1->row();
	
    echo "           
    <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>    
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit vehicle</font></h4>
    </div>
    <br>

   <form  onsubmit='return vehiclesubmit();' class='form-horizontal' role='form' method='post' action='". site_url('Vehicle/return_vehicle/'.$id). "' enctype='multipart/form-data'>
     
	     <div class='form-group'>
             <label class='col-md-3 control-label' style='padding-top:10px;'>Registration No </label>
             <div class='col-md-8'>  
             <label class='control-label' style='padding-top:10px;'>$row->vehicle_regno</label>
          </div>                   
          </div>

          <div class='form-group'>
            <label class='col-md-3 control-label' style='padding-top:10px;'>Service KM </label>
          <div class='col-md-8'>  
            <input type='text' id='skm1' class='form-control' name='skm'>
          </div>                   
          </div>
		  
		<hr style='width:100%'>		  
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-8'>
                <input type='hidden' name='uid'  value='$row->vehicle_id'/>
                <input type='submit' class='btn btn-primary' value='Submit'/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>
<!--		   <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div> -->
<script>
function vehiclesubmit()
{
	var res=true;
var skm1=$('#skm1').val();
if(skm1=='')
{
	res=false;
}
else
{
	res=true;
}
	return res;
}

</script>
	  ";

    }
  
  
  public function settoservice()
  {
	$id=$this->uri->segment(3);
	$dt1=array("ser_status"=>'2');
	$dt2=array("vehicle_status"=>'2');
	
	$this->db->where('ser_vehicle_id',$id);
	$this->db->update('services',$dt1);
	
	$this->db->where('vehicle_id',$id);
	$this->db->update('vehicle_registration',$dt2);
	
	$this->session->set_flashdata('message', 'Vehicle Set To Service..!');
    redirect('vehicle/service_vehicles');
  }

 public function return_vehicle()
  {
	$id=$this->uri->segment(3);
	
	$skm=$this->input->post('skm');
	
	$dt1=array(
	"ser_kilometer1"=>$skm,
	"ser_kilometer2"=>$skm,
	"ser_status"=>'1'
	);
	
	$dt2=array("vehicle_status"=>'1');
	
	$this->db->where('ser_vehicle_id',$id);
	$this->db->update('services',$dt1);
	
	$this->db->where('vehicle_id',$id);
	$this->db->update('vehicle_registration',$dt2);
	
	$this->session->set_flashdata('message', 'After service,Vehicle successfully returned.');
    redirect('vehicle/servicing_vehicles');
  }
	
	
public function add_expense()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('regno', 'regno', 'required');
$this->form_validation->set_rules('etype', 'etype', 'required');
$this->form_validation->set_rules('edate', 'edate', 'required');
$this->form_validation->set_rules('billno', 'billno', 'required');
$this->form_validation->set_rules('bdate', 'bdate', 'required');
$this->form_validation->set_rules('amount', 'amount', 'required');

 if($this->form_validation->run())
        {
		
if (!empty($_FILES['files']['name']))
		{
			$filesCount = count($_FILES['files']['name']);
			
			$fname="";	
			for($i = 0; $i < $filesCount; $i++)
			{
                $_FILES['ivf']['name'] = $_FILES['files']['name'][$i];
                $_FILES['ivf']['type'] = $_FILES['files']['type'][$i];
                $_FILES['ivf']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['ivf']['error'] = $_FILES['files']['error'][$i];
                $_FILES['ivf']['size'] = $_FILES['files']['size'][$i];
				
			    $uploadPath = 'upload/expense/';

                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|jpe';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				
                if($this->upload->do_upload('ivf'))
				{
                 $fileData=array('upload_data'=> $this->upload->data());
				 
						if($i==$filesCount-1)
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'];
						 }
						 else
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'].",";
						 }
				}
				else
				{
					echo $this->upload->display_errors();
					
				}
			}
		}		
			
	$texop=$this->input->post('texop');
	
	$trid1=$this->input->post('tripid1');
	
    $regno=$this->input->post('regno');
    $etype=$this->input->post('etype');
    $edate=$this->input->post('edate');
    $billno=$this->input->post('billno');
    $bdate=$this->input->post('bdate');
	$amount=$this->input->post('amount');
	$narra=$this->input->post('narra');
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
    	
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
    
       $userdt = array(
         'expense_vehicleno'=>$regno,
         'expense_date' => $edate,
         'expense_type' =>$etype,
         'expense_billno' =>$billno,
         'expense_billdate' =>$bdate,
		 'expense_amount' =>$amount,
		 'expense_narration'=>$narra,
         'expense_proof' =>$fname,
          );
		  
        $this->load->Model('Model_vehicle');
		$ins_id="";
        $ins_id=$this->Model_vehicle->expense_insert($userdt);
		$this->session->set_flashdata('message', 'Expense added..!');
          $uid = $this->db->insert_id();
        $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Vehicle Details Added'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
		  
		  if($texop=='0')
		  {
      	  redirect('Vehicle/vehicle_expense');  
		  }
		  else
		  {
			 redirect('Trip/completetrip/'.$trid1);  
		  }
    }
 
 else{
         $this->session->set_flashdata('message', 'Error: Data Not Inserted..');
        //$data['message']="Data Inserted Successfully";

		  if(texop=='0')
		  {
      	  redirect('Vehicle/vehicle_expense');  
		  }
		  else
		  {
			 redirect('Trip/completetrip/'.$trid1);  
		  }
       }  
}	

public function add_expense1()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('regno', 'regno', 'required');
$this->form_validation->set_rules('etype', 'etype', 'required');
$this->form_validation->set_rules('edate', 'edate', 'required');
$this->form_validation->set_rules('billno', 'billno', 'required');
$this->form_validation->set_rules('bdate', 'bdate', 'required');
$this->form_validation->set_rules('amount', 'amount', 'required');


 if($this->form_validation->run())
        {
		
if (!empty($_FILES['files']['name']))
		{
			$filesCount = count($_FILES['files']['name']);
			
			$fname="";	
			for($i = 0; $i < $filesCount; $i++)
			{
                $_FILES['ivf']['name'] = $_FILES['files']['name'][$i];
                $_FILES['ivf']['type'] = $_FILES['files']['type'][$i];
                $_FILES['ivf']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['ivf']['error'] = $_FILES['files']['error'][$i];
                $_FILES['ivf']['size'] = $_FILES['files']['size'][$i];
				
			    $uploadPath = 'upload/expense/';

                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|jpe';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				
                if($this->upload->do_upload('ivf'))
				{
                 $fileData=array('upload_data'=> $this->upload->data());
				 
						if($i==$filesCount-1)
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'];
						 }
						 else
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'].",";
						 }
				}
				else
				{
					echo $this->upload->display_errors();
					
				}
			}
		}		
			

	$trid1=$this->input->post('tripid1');
	
    $regno=$this->input->post('regno');
    $etype=$this->input->post('etype');
    $edate=$this->input->post('edate');
    $billno=$this->input->post('billno');
    $bdate=$this->input->post('bdate');
	$amount=$this->input->post('amount');
	$narra=$this->input->post('narra');
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
    	
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
    
       $userdt = array(
         'expense_vehicleno'=>$regno,
         'expense_date' => $edate,
         'expense_type' =>$etype,
         'expense_billno' =>$billno,
         'expense_billdate' =>$bdate,
		 'expense_amount' =>$amount,
		 'expense_narration'=>$narra,
         'expense_proof' =>$fname,
          );
		  
        $this->load->Model('Model_vehicle');
		$ins_id="";
        $ins_id=$this->Model_vehicle->expense_insert($userdt);
		
        $this->session->set_flashdata('message', '1#Expense added..1');
          $uid = $this->db->insert_id();
        $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Vehicle Details Added'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);

      	  redirect('Vehicle/vehicle_expense');  
    }
 
 else {
         $this->session->set_flashdata('message', '4#Data missing.');
      	  redirect('Vehicle/vehicle_expense');  
       }  
}


public function Expenses()
{
	    $op=$this->uri->segment(3);
        $this->load->model('Model_vehicle');
		if($op=='1')
		{
				$dt1=$this->input->post('fdate');
				$dt2=$this->input->post('tdate');
				$vid=$this->input->post('vehicleid1');
				
						$dt11=$dt1; //form message
						$dt21=$dt2;
				
				$d1=explode("-",$dt1);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$dt2);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			$exdata['exresults']=$this->Model_vehicle->view_expense1($dt1,$dt2,$vid);
			$this->session->set_flashdata('exphead', 'Expense List of : <b>'.$dt11." => ".$dt21.'</b>');
		}
		else if($op=='2')
		{
			$mon=$this->input->post('mon');
			$vid=$this->input->post('year');
			$m1=date('Y')."-".$mon."-"."01";
			
			$exdata['exresults']=$this->Model_vehicle->view_expense2($mon,$vid);
			$this->session->set_flashdata('exphead', 'Expense List of : <b>'. date('F',strtotime($m1)).'</b>');
		}
		else
		{
			//current  month list
		$this->session->set_flashdata('exphead', 'Expense List of : <b>'.date('F').'</b>');
		$this->load->model('Model_vehicle');
		$exdata['exresults']=$this->Model_vehicle->view_expense();
		}
		
		$this->load->view('admin/expense_list',$exdata);
}

public function allexpenses()
{
		$this->session->set_flashdata('exphead', 'Expense List of : <b>'.date('F').'</b>');
		$this->load->model('Model_vehicle');
		$exdata['exresults']=$this->Model_vehicle->view_expense();
		$this->load->view('admin/expense_list',$exdata);
}

public function del_expense()
 {
	$id=$this->uri->segment(3, 0);
	$this->load->model('Model_vehicle');
	$result=$this->Model_vehicle->delete_expense($id);
	if($result)
	{
		  $this->session->set_flashdata('message','Expense Removed..!');
		   redirect('vehicle/expenses');
	}
	else
	{
	$this->session->set_flashdata('message', 'Please try again.');
	redirect('vehicle/expenses');
	}
 }
 
  public function edit_expense()   //vehicle expense
  {
	  $id=$this->input->post('eid');
	  $erw=$this->db->select('*')->from('vehicle_expense')->where('expense_id',$id)->get()->row();
	  	  
	  echo '
	   <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"> <font color="blue">Edit Expense</font></h4>
		</div>
		<br>
	  <form class="form-horizontal" role="form" method="POST" action="'. base_url('Vehicle/update_vehicle_expense').'" enctype="multipart/form-data">
            <input type="hidden" name="expid" value="'.$erw->expense_id.'">
		<div class="form-group">
		<label class="col-md-3" style="padding-top:5px;" align="right">Vehicle RegNo : </label>
        <div class="col-md-8">
		 <div id="regno1">
			<select name="regno" id="regno" class="form-control" required>
             <option value="">------------</option>';
            
			$vehicleid=$this->db->select('*')->from("vehicle_registration")->get()->result();
			foreach($vehicleid as $values6)
			{
			   echo '<option value="'.$values6->vehicle_id.'"'; if($erw->expense_vehicleno==$values6->vehicle_id) echo "selected"; echo '>'.$values6->vehicle_regno.'</option>';
            }
			
			echo '
               </select>
            </div>
		  </div> 
		</div> 
		
		<div class="form-group">
		<label class="col-md-3" style="padding-top:5px;" align="right">Expense Type : </label>
			<div class="col-md-8">
				<select name="etype" id="etype" class="form-control" required>
				 <option value="">------------</option>';
				 
				   $etype=$this->db->select('*')->from("expense_type")->where("etype_group","1")->get()->result();	
					foreach($etype as $et)
					{
						
						echo '<option value="'.$et->etype_id.'"'; if($erw->expense_type==$et->etype_id) echo "selected"; echo '>'.$et->etype_name.'</option>';
						
					} 
				echo '
				</select>
			</div> 
			</div> 
			
		<div class="form-group">
		<label class="col-md-3" style="padding-top:5px;" align="right">Date : </label>
        <div class="col-md-4">
		<input type="date" name="edate" class="form-control" value="'.$erw->expense_date.'" required>
		</div> 
		</div> 
		
		<div class="form-group">
		<label class="col-md-3" style="padding-top:5px;" align="right">Bill No : </label>
        <div class="col-md-4">
		<input type="number" name="billno" class="form-control" value="'.$erw->expense_billno.'"  required>
		</div> 
		</div> 
		
		<div class="form-group">
		<label class="col-md-3" style="padding-top:5px;" align="right">Bill Date : </label>
        <div class="col-md-4">
		<input type="date" name="bdate" class="form-control" value="'.$erw->expense_billdate.'" required>
		</div> 
		</div> 
		
		<div class="form-group">
		<label class="col-md-3" style="padding-top:5px;" align="right">Amount : </label>
        <div class="col-md-4">
		<input type="text" name="amount" class="form-control" value="'.$erw->expense_amount.'" required>
		</div> 
		</div> 
	
	
	<div class="form-group">
		<label class="col-md-3" style="padding-top:5px;" align="right">Narration : </label>
        <div class="col-md-8">
		<textarea rows=4 name="narra" class="form-control"  required>'.$erw->expense_narration.'</textarea>
		</div> 
		</div> 
		
				<label style="background-color:#cecece;width:100%;height:1px;margin:0px padding:0px;"></label> 
					<div class="form-group"> 
					<label class="col-md-3 control-label" style="padding-top:3px;"></label>
						<div class="col-md-7">
							<input type="submit" class="btn btn-primary" value="Update Details" />
							 <input type="submit" class="btn btn-default" style="margin-left:15px;" data-dismiss="modal" value="Close" />
							</center>
						</div>
					</div> 

</form>';
  }
  
  public function update_vehicle_expense()
  {
	
	$expid=$this->input->post('expid');
    $regno=$this->input->post('regno');
    $etype=$this->input->post('etype');
    $edate=$this->input->post('edate');
    $billno=$this->input->post('billno');
    $bdate=$this->input->post('bdate');
	$amount=$this->input->post('amount');
	$narra=$this->input->post('narra');
  
	$staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
    	
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
    
       $expdt = array(
         'expense_vehicleno'=>$regno,
         'expense_date' => $edate,
         'expense_type' =>$etype,
         'expense_billno' =>$billno,
         'expense_billdate' =>$bdate,
		 'expense_amount' =>$amount,
		 'expense_narration'=>$narra,
         );
		  
        $this->load->Model('Model_vehicle');
		$ins_id="";
        $ins_id=$this->Model_vehicle->update_expense($expid,$expdt);
		$this->session->set_flashdata('message', 'Expense added..!');
          $uid = $this->db->insert_id();
        $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>"Vehicle Expense edited-".$expid
            );
			$this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
			$this->session->set_flashdata('message','Expense Updated.!');
      	  redirect('Vehicle/expenses');  		
  }
  
  
}