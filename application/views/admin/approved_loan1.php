<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}


</style>
 
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Approved Loan</b></h1>
   <ol class="breadcrumb" style='font-size:15px;'>
		<!-- <li><a href="<?php echo base_url('Loan/loan_master');?>"style='color:#4b88ed;'><i class="fa fa-bus" aria-hidden="true"></i>New Loan</a></li>
		<li><a href="<?php echo base_url('Loan/long_term_loan');?>"style='color:#4b88ed;'><i class="fa fa-bus" aria-hidden="true"></i>Approve Loan</a></li> -->
     </ol> 
 </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header  end -->	  

	  
  <section class="content"> 
  <div style="color:blue;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Print,Share,Save,View,Edit,Delete)  </div>
        <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
		</div>  
  <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details-->
	
	 <div style="background-color:#fff;border-radius:10px;padding:15px; ">
					
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">

				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                   <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                       <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="loan_master_list">
                        <thead>
								<tr style='color:#4b88ed'>
								<th>SL/No</th>
								 <th>LTL Referene</th>
								 <th>Client Name</th>
								 <th>Client Address</th>
								 <th>Moble No</th>
								 <th>Phone No</th>
								 <th>Anual Intrest</th>
								 <th>Loan Duration</th>

								 <th>Bank Name</th>
								 <th>Bank Account</th>
								<th>Shedules</th>
								 </tr>
								</thead>
                        </table>
                        </div>
						
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
						<div class='col-md-4'>
						</div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>

 <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
  
          <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Loan Schedules</h4>
	              </div>
               <div class="modal-body" id="model-body1">
			   
               </div>
              </div>
            </div>
          </div>			

           <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content" id="content1">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
			  
		
            <!-- /.modal-dialog -->
            </div>
            </div>  

            <div class="modal fade draggable-modal" id="myModals" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Share</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
            </div>     
            </div>       

				
	</div>
	<!-- End user details -->

    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------

 setTimeout(function(){ $("#idh5").html(""); }, 3000);
 
$('#loan_master_list').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "loan/loanapproved_ajax",// json datasource
               },
			   "columnDefs":[
         {"width":"1%","targets":0},
			   {"width":"5%","targets":0},
			   {"width":"8%","targets":1},
			   {"width":"10%","targets":3},
			   {"width":"10%","targets":5},
			   {"width":"5%","targets":6},
			   {"width":"5%","targets":7},
			   {"width":"7%","targets":8},
			   {"width":"7%","targets":9},
         {"width":"7%","targets":9},
			   ],
			   
        "columns": [
      { "data": "slno" },
			{ "data": "ltl_ref" },
      { "data": "acc_name"},
			{ "data": "acc_addr"},
			{ "data": "mobile_no"},
      { "data": "phone_no" },
			{ "data": "anual_intrest_rt" },
			{ "data": "loan_duration" },
			{ "data": "bank_name" },
			{ "data": "bank_acc_no" },
      { "data": "shedule"},
       ]
  } );

var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });			   

function loadshedule(obj){
  ltl_ref = $(obj).attr("data-value");
  jQuery.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "Loan/approved_loan",
          dataType: 'html',
          data: {ltl_ref: ltl_ref},
          success: function(res) {
            $("#model-body1").empty();
            $("#model-body1").append(res);
            $("#myModal2").modal();
         }
  });
}

  
  $('#view-pdf').on('click',function(){
       var id=$('#example').find('tr.selected').find('td').eq(1).text();
     if(id=="")
     {
       alert ("please Select customer details..");
     }
     else
     {
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
		var pdf_link = $(this).attr('href')+'/'+id;
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Customer Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
       } return false; 

    });    

  
      $('#idedit').click(function () {
        var id=$('#example').find('tr.selected').find('td').eq(1).text();
		alert(id);
		 if(id=="")
		 {
			 alert ("please Select staff details..");
		 }
		 else
		 {
	
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/edit_trip/"+id,
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
			
                   }
            });
		 }
        }); 

		
  $('#idview').click(function ()
  {
        var Result=$("#myModal1 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(id=="")
		 {
			 alert ("please Select staff details..");
		 }
		 else
		 {
			$("#idview").attr('data-target','#myModal1') 
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/trip_details",
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
		 }
        }); 		


       $('#idshare').click(function () {
        var Result=$("#myModals .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
        var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(id=="")
		 {
			 alert ("please Select staff details..");
		 }
		 else
		 {
			$("#idshare").attr('data-target','#myModals') 
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/share_trip_details",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
			 Result.html(res);
                    }
            });
		 }
    
        });  

  $('#idsave').click(function ()
    {   
         var id=$('#example').find('tr.selected').find('td').eq(1).text();
         if(id=="")
         {
             alert ("please Select trip details..");
         }
         else
         {
            jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "Trip/save_trip",
            dataType: 'html',
            data:{tid:id},
            success: function(res) {
				alert("Trip sheet saved...!");
             }
            });
         }
        }); 
		
		
		
		
		
		
	$('#iddelete').click(function () 
    {
    var res=confirm("Delete this coustomer details?");
    if(res)
    {
    var id=$('#example').find('tr.selected').find('td').eq(1).text();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Customer/del_entry",
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    }
        });     
	
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });
 //});
  


</script>



