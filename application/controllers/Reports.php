<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

private $msr_month;
private $msr_year;
private $msdata=array();

function __construct(){
parent::__construct();
$user = $this->session->userdata('userid');
if (!isset($user))
{ 
redirect('/');
} 
else
{ 
return true;
}
$this->load->helper(array('form', 'url'));
$this->load->model('Model_reports');
}

//Trip Reports
public function index()
{
  $this->load->model('Model_reports');
  $this->load->view('admin/view_tripreports');
}

public function pdf_cotrip()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
      $dt1=$this->uri->segment(3);
		$dt2=$this->uri->segment(4);
		$d1=explode("-",$dt1);
		$dte1=$d1[2]."-".$d1[1]."-".$d1[0];
	    $d2=explode("-",$dt2);
		$dte2=$d2[2]."-".$d2[1]."-".$d2[0];
		$data['dte2']=$d2[2]."-".$d2[1]."-".$d2[0];
		$data['dte1']=$d1[2]."-".$d1[1]."-".$d1[0];
	$this->load->model('Model_reports');
	$data['results']=$this->Model_reports->view_cotrip_date($dte1,$dte2);

ini_set('memory_limit','256M');
$html=  $this->load->view('pdf/reports_pdf',$data,true);

$pdf->WriteHtml($html);
$output='allreports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
exit();
}


public function pdf_cltrip()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
      $dt1=$this->uri->segment(3);
		$dt2=$this->uri->segment(4);
		$d1=explode("-",$dt1);
		$dte1=$d1[2]."-".$d1[1]."-".$d1[0];
	    $d2=explode("-",$dt2);
		$dte2=$d2[2]."-".$d2[1]."-".$d2[0];
		$data['dte2']=$d2[2]."-".$d2[1]."-".$d2[0];
		$data['dte1']=$d1[2]."-".$d1[1]."-".$d1[0];
	$this->load->model('Model_reports');
	$data['results']=$this->Model_reports->view_cltrip_date($dte1,$dte2);

ini_set('memory_limit','256M');

//$path   = 'http://localhost/advtravels/uploads/pdf/';  
//echo realpath($path);   
$html=  $this->load->view('pdf/reports_pdf',$data,true);

$pdf->WriteHtml($html);
$output='allreports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}


public function pdf_rtrip()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
      $dt1=$this->uri->segment(3);
		$dt2=$this->uri->segment(4);
		
	$this->load->model('Model_reports');
		if($dt1!='r1')
		{
		$d1=explode("-",$dt1);
		$dte1=$d1[2]."-".$d1[1]."-".$d1[0];
	    $d2=explode("-",$dt2);
		$dte2=$d2[2]."-".$d2[1]."-".$d2[0];
		$data['dte2']=$d2[2]."-".$d2[1]."-".$d2[0];
		$data['dte1']=$d1[2]."-".$d1[1]."-".$d1[0];
		$data['results']=$this->Model_reports->view_rtrip_date($dte1,$dte2);
		}
else
{
	$dte1="";
	$dte2="";
	$data['results']=$this->Model_reports->view_rtrip_date($dte1,$dte2);
}

ini_set('memory_limit','256M');

$html=  $this->load->view('pdf/reports_pdf',$data,true);
$pdf->WriteHtml($html);
$output='allreports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function pdf_utrip()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
      $dt1=$this->uri->segment(3);
		$dt2=$this->uri->segment(4);

		$this->load->model('Model_reports');
		if($dt1!=="r2")
		{
		$d1=explode("-",$dt1);
		$dte1=$d1[2]."-".$d1[1]."-".$d1[0];
	    $d2=explode("-",$dt2);
		$dte2=$d2[2]."-".$d2[1]."-".$d2[0];
		$data['dte2']=$d2[2]."-".$d2[1]."-".$d2[0];
		$data['dte1']=$d1[2]."-".$d1[1]."-".$d1[0];
		$data['results']=$this->Model_reports->view_utrip_date($dte1,$dte2);
		}
		else
		{
			$dte1="";
			$data['dte2']="r2";
			$data['results']=$this->Model_reports->view_utrip_date($dte1,$dte2);
		}

ini_set('memory_limit','256M');

//$path   = 'http://localhost/advtravels/uploads/pdf/';  
//echo realpath($path);   
$html=  $this->load->view('pdf/reports_pdf',$data,true);

$pdf->WriteHtml($html);
$output='allreports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

//vehicle reports
public function vehiclereport()
{
  $this->load->view('admin/view_vehiclereports');
}

public function vehicle()
{
  $this->load->view('admin/view_vehiclereports');
}

public function expenses()
{
	$this->load->view('admin/expense_listRep');
}

public function Gen_Expenses()
{
	$this->load->view('admin/general_expense_rep');
}

public function pdf_allvehicles()
{
$this->load->library('pdf1');
$pdf=$this->pdf1->load();
    $radio=$this->uri->segment(3);
    if($radio=="vehicles")
    {
	$this->load->model('Model_reports');
	$data['results']=$this->Model_reports->view_allvehicles();
	
ini_set('memory_limit','256M');
$html=  $this->load->view('pdf/vehicle_pdf',$data,true);

$pdf->WriteHtml($html);
$output='allvehiclereports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
exit();
}

else
{
$this->load->model('Model_reports');
	$data['results']=$this->Model_reports->view_servicing();
ini_set('memory_limit','256M');
$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/vehicle_pdf1',$data,true);
$pdf->WriteHtml($html);
$output='allvehiclereports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');
exit();
}
}

public function pdf_month_vehicles()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $month=$this->uri->segment(3);
    $this->load->model('Model_reports');
    $data['month']=$this->uri->segment(3);
    //$data['results']=$this->Model_reports->view_servicing($month);
	$data['results']=$this->Model_reports->view_month_service($month);
ini_set('memory_limit','256M');
$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/vehicle_pdf1',$data,true);
$pdf->WriteHtml($html);
$output='allvehiclereports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}




//Staff Reports
public function staffreports()
{
  $this->load->model('Model_reports');
  $this->load->view('admin/view_staffreports');
}


public function pdf_allvendors()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
  $this->load->model('Model_reports');
	$data['results']=$this->Model_reports->view_vendors();
ini_set('memory_limit','256M');
$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/owner_pdf',$data,true);
$pdf->WriteHtml($html);
$output='allvehiclereports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}


//Log Reports
public function logreports()
{
  $this->load->model('Model_reports');
   $this->load->view('admin/view_logreports');
	
}


public function pdf_alllogs_date()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $data['date']=$this->uri->segment(3);
        $d1=$this->uri->segment(3);
		$d2=explode("-",$d1);
		$date=$d2[2]."/".$d2[1]."/".$d2[0];
  $this->load->model('Model_reports');
	$data['results']=$this->Model_reports->view_logs_date($date);
ini_set('memory_limit','256M');
$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/logs_pdf_date',$data,true);
$pdf->WriteHtml($html);
$output='alllogreports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function pdf_alllogs_month()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $data['month']=$this->uri->segment(3);
    $month=$this->uri->segment(3);
	$this->load->model('Model_reports');
	$data['results']=$this->Model_reports->view_logs_month($month);
ini_set('memory_limit','256M');
$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/logs_pdf_month',$data,true);
$pdf->WriteHtml($html);
$output='alllogreports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function expense_report()  //vehicle expense report
{
	$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
	    $op=$this->uri->segment(3);
        $this->load->model('Model_vehicle');
		if($op=='1')
		{
				$vid=$this->uri->segment(4);
				$dt1=$this->uri->segment(5);
				$dt2=$this->uri->segment(6);
								
						$dt11=$dt1; //form message
						$dt21=$dt2;
				
				$d1=explode("-",$dt1);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$dt2);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			$data['results']=$this->Model_vehicle->view_expense1($dt1,$dt2,$vid);
			$this->session->set_flashdata('exphead', ': <b>'.$dt11." => ".$dt21.'</b>');
		}
		else if($op=='2')
		{
			//$mon=$this->input->post('mon');
			//$vid=$this->input->post('vehicleid2');
			
			$vid=$this->uri->segment(4);
			$mon=$this->uri->segment(5);
			
			$m1=date('Y')."-".$mon."-"."01";
			
			var_dump($mon);
			var_dump($vid);
			
			$data['results']=$this->Model_vehicle->view_expense2($mon,$vid);
			$this->session->set_flashdata('exphead', ': <b>'. date('F',strtotime($m1)).'</b>');
		}
		else
		{
			//current  month list
		$this->session->set_flashdata('exphead', ': <b>'.date('F').'</b>');
		$this->load->model('Model_vehicle');
		$data['results']=$this->Model_vehicle->view_expense();
		}
		
		$html=  $this->load->view('pdf/expenses_report_pdf',$data,true);
		$pdf->WriteHtml($html);
		$output='alllogreports'.date('Y_m_d_H').'_.pdf';
		$pdf->Output("$output",'I');
	
}


public function General_expenses()
{
	    $op=$this->uri->segment(3);
		
		$this->load->library('pdf1');
		$pdf=$this->pdf1->load();
		
        $this->load->model('Model_general');
		
		if($op=='1')
		{
				$dt1=$this->uri->segment(4);
				$dt2=$this->uri->segment(5);
				
						$dt11=$dt1; //form message
						$dt21=$dt2;
				
				$d1=explode("-",$dt1);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$dt2);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];

			$exdata['genexp']=$this->Model_general->view_general_expense($dt1,$dt2,"","");
			$this->session->set_flashdata('Gexphead', 'General Expense List of : <b>'.$dt11." => ".$dt21.'</b>');
		}
		else if($op=='2')
		{
			$mon=$this->uri->segment(4);
			$m1=date('Y')."-".$mon."-"."01";
			$exdata['genexp']=$this->Model_general->view_general_expense("","",$mon,"");
			$this->session->set_flashdata('Gexphead', 'General Expense List of : <b>'. date('F',strtotime($m1)).'</b>');
		}
		else
		{
		$this->session->set_flashdata('Gexphead', 'General Expense List of : <b>'.date('F')." - ".date('Y').'</b>');
		$exdata['genexp']=$this->Model_general->view_general_expense("","","","0");
		}
		
		$html=  $this->load->view('pdf/gen_expenses_report_pdf',$exdata,true);
		$pdf->WriteHtml($html);
		$output='alllogreports'.date('Y_m_d_H').'_.pdf';
		$pdf->Output("$output",'I');
	
}

public function Booking()
{
$this->load->view('admin/view_booking_Rep');	
}


/*public function booking_report()    //new booking report
{
	$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
	//$op=$this->uri->segment(3);
	$this->load->Model('Model_trip');
	
	$sdate=$this->input->post('sdate');
	$edate=$this->input->post('edate');
	$cltype=$this->input->post('cltype');
	$client=$this->input->post('client');
		
	$btnget=$this->input->post('btnget');
	$btncancel=$this->input->post('btncancel');
	$btnclose=$this->input->post('btnclose');
	
	$dt1="";
	$dt2="";

	$d1=explode("-",$sdate);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	
	$d2=explode("-",$edate);
	$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
	if($cltype=="")
	{
	$where="(trip_booking.book_tripdate>='".$dt1. "' and trip_booking.book_tripdate<='".$dt2."')";		
	}
	else
	{
	$where="(trip_booking.book_tripdate>='".$dt1. "' and trip_booking.book_tripdate<='".$dt2."') and trip_booking.book_clienttype='" .$cltype."' ";			
	}
	
	
	if($cltype=="2")
	{
		$where.=" and trip_booking.book_corpid='" .$client."'";
	}
	else if($cltype=="3")
	{
		$where.=" and trip_booking.book_agentid='" .$client."'";
	}
	else 
	{
		
	}	
	
		
	if(isset($btnget))
	{
	$where.=" and trip_booking.book_status='1' order by  book_id desc";
	}
	else if(isset($btncancel))
	{
		$where.=" and trip_booking.book_status='2' order by  book_id desc";
	}	
	else if(isset($btnclose))
	{
		$where.=" and trip_booking.book_status='3' order by  book_id desc";
	}	
		
	$ses_cap="[ ".$sdate." => ". $edate." ]";
	$brepdata['booking']= $this->Model_trip->view_booking_trip1("1",$where);
	
	$html=  $this->load->view('pdf/booking_list',$data,true);
	$pdf->WriteHtml($html);
	$output='alllogreports'.date('Y_m_d_H').'_.pdf';
	$pdf->Output("$output",'I');	
	
	exit();
}*/

public function booking_report()    //new booking report
{
	$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
	$sdate=$this->uri->segment(3);
	$edate=$this->uri->segment(4);
	$cltype=$this->uri->segment(5);
	$client=$this->uri->segment(6);
	$btn=$this->uri->segment(7);
	
	$this->load->Model('Model_trip');
		
	$dt1="";
	$dt2="";

	$d1=explode("-",$sdate);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	
	$d2=explode("-",$edate);
	$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
	if($cltype=="0")
	{
	$where="(trip_booking.book_tripdate>='".$dt1. "' and trip_booking.book_tripdate<='".$dt2."')";
	}
	else
	{
	$where="(trip_booking.book_tripdate>='".$dt1. "' and trip_booking.book_tripdate<='".$dt2."') and trip_booking.book_clienttype='" .$cltype."' ";			
	}
		
	if($cltype=="2")
	{
		$where.=" and trip_booking.book_corpid='" .$client."'";
		
	}
	else if($cltype=="3")
	{
		$where.=" and trip_booking.book_agentid='" .$client."'";
		
	}
	
	if($btn=="1")
	{
	$where.=" and trip_booking.book_status='1' order by  book_id desc";
	$ses_cap="Booking List : [ ".$sdate." => ". $edate." ]";
	}
	else if($btn=="2")
	{
		$where.=" and trip_booking.book_status='2' order by  book_id desc";
		$ses_cap="Cancelled Booking List : [ ".$sdate." => ". $edate." ]";
	}	
	else if($btn=="3")
	{
		$where.=" and trip_booking.book_status='3' order by  book_id desc";
		$ses_cap="Closed Booking List : [ ".$sdate." => ". $edate." ]";
	}	
	
	$brepdata['booking']= $this->Model_trip->view_booking_trip1("1",$where);
	$brepdata['bcap']=$ses_cap;
	$html=  $this->load->view('pdf/booking_list',$brepdata,true);
	$pdf->WriteHtml($html);
	$output='alllogreports'.date('Y_m_d_H').'_.pdf';
	$pdf->Output("$output",'I');	
	
	exit();
}


  function get_corporate_names()
  {
	$ctype = $this->input->post('cltype');
   	$rw=$this->db->select('customer_id,customer_name')->from('customers')->where('customer_type',$ctype)->get()->result();
		$opt="<option value='0' selected>---------</option>";
		foreach($rw as $rw1)
		{
			$opt.="<option value='".$rw1->customer_id."'>".$rw1->customer_name."</option>";
		}
		
	echo  $opt;
  }


/*public function booking_report()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
	
   	$this->load->model('Model_trip');
	$where=$this->session->userdata('booking_where');
	$data['results']=$this->Model_trip->booking_trip_report($where);
	$data['bcap']=$this->session->userdata('booking_cap');
	
	ini_set('memory_limit','256M');
	$path   = 'http://localhost/advtravels/uploads/pdf/';  
	echo realpath($path);   
	$html=  $this->load->view('pdf/booking_list',$data,true);
	$pdf->WriteHtml($html);
	$output='alllogreports'.date('Y_m_d_H').'_.pdf';
	$pdf->Output("$output",'I');
	// $pdf->Output('uploads/pdf/'.$output,'F');


}
*/

public function Documents() 		 //expiry documents report
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
   	$this->load->model('Model_general');
	//$where=$this->session->userdata('booking_where');
	$data['docs']=$this->Model_general->renewal_documents();
	$data['dcap']="Renewal Documents List";
	
ini_set('memory_limit','256M');
$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/exp_documents',$data,true);
$pdf->WriteHtml($html);
$output='alllogreports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function Monthly_Target() 		 //expiry documents report
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
	$mon=$this->session->userdata('mtmonth');
	$yr=$this->session->userdata('mtyear');
	
   	$this->load->model('Model_general');
    $data['result']=$this->Model_general->report_doc($mon,$yr);
	$data['result1']=$this->Model_general->get_monthly_collection($mon,$yr);
	
	$data['dcap']="Monthly Target Achievement Report";
		
ini_set('memory_limit','256M');
$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/target_report_pdf',$data,true);
$pdf->WriteHtml($html);
$output='alllogreports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function Evaluation() 		 //expiry documents report
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
	$op=$this->session->userdata('se_op');
	$md=$this->session->userdata('se_opt');
	
   	$this->load->model('Model_general');
    $this->load->model('Model_services');
	$sedata['eva_result'] = $this->Model_services->service_evo_list($op,$md);
	
	$sedata['dcap']=$this->session->userdata('se_head');
		
ini_set('memory_limit','256M');
$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/evaluation_report',$sedata,true);
$pdf->WriteHtml($html);
$output='alllogreports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}



// marketing report

public function marketing() 		 //expiry documents report
{
$this->load->library('pdf1');
$pdf=$this->pdf1->load();

	$yr=$this->session->userdata('mbyear');
	$mon=$this->session->userdata('mbmonth');
	$dt1=$this->session->userdata('mbdate1');
	$dt2=$this->session->userdata('mbdate2');
	$op=$this->session->userdata('mbop');
	//------------------- 
	$this->load->model('Model_general');
    $mbdata['marres']=$this->Model_general->view_marketing($dt1,$dt2,$mon,$yr,$op);

    $mbdata['dcap']="MarketingBusiness : ".$this->session->userdata('se_head');

ini_set('memory_limit','256M');
$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/mar_business_report',$mbdata,true);
$pdf->WriteHtml($html);
$output='alllogreports'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function request_report()
	{
		$this->load->library('pdf1');
		$pdf=$this->pdf1->load();

		$srid=$this->uri->segment(3);
		$this->load->model('Model_services');
    	$srdata['srresult1'] = $this->Model_services->view_service_request("R",$srid,"");
		
		$html=  $this->load->view('pdf/service_request_pdf',$srdata,true);
		$pdf->WriteHtml($html);
		$output='alllogreports'.date('Y_m_d_H').'_.pdf';
		$pdf->Output("$output",'I');
		
		exit();
	}

public function Summary()
{
	  $month = date('m');
      $year = date('Y');
	  
	if($this->input->post('month'))
    {
      $month = $this->input->post('month');
      $year = $this->input->post('year');
    }
    else
    {
      $month = date('m');
      $year = date('Y');
    }
	
	$this->session->set_userdata('msr_month',$month);
	$this->session->set_userdata('msr_year',$year);

	$this->get_summary_details($month,$year);
	
	$this->load->view('admin/monthly_summary_report',$this->msdata);
}

/*public function get_summary_details($month,$year)
{

	$this->msdata['salary'] = $this->db->select('SUM(salary_paidamount) as SALARY')->where('month(salary_date)', $month)->where('year(salary_date)', $year)->get('staff_salary')->row();
	
	$this->msdata['advance'] = $this->db->select('SUM(sal_adv_amount) as ADVANCE')->where('month(sal_adv_vdate)', $month)->where('year(sal_adv_vdate)', $year)->get('salary_advance')->row();

    $this->msdata['vehicle_expense'] = $this->db->select('SUM(expense_amount) as EXPENSE')->where('month(expense_billdate)', $month)->where('year(expense_billdate)', $year)->get('vehicle_expense')->row();

    $this->msdata['general_expense'] = $this->db->select('SUM(gen_expense_amount) as GEN_EXPENSE')->where('month(gen_expense_billdate)', $month)->where('year(gen_expense_billdate)', $year)->get('general_expense')->row();

    $this->msdata['vehicle_service'] = $this->db->select('SUM(service_invoiceamt) as SERVICE_EXPENSE')->where('month(service_invoicedate)', $month)->where('year(service_invoicedate)', $year)->get('vehicle_service')->row();

    $this->msdata['vehicle_loan'] = $this->db->select('SUM(loan_instalment) as LOAN_EXPENSE')->where('month(loan_date)', $month)->where('year(loan_date)', $year)->where('status','1')->get('loan_shedule')->row();

    //$msdata['trip_vehicles'] = $this->db->distinct()->select('trip_vehicle_id,trip_vehicle_regno')->where('month(trip_enddate)', $month)->where('year(trip_enddate)', $year)->get('trip_management')->result();
	$this->msdata['trip_total'] = $this->db->select('SUM(trip_gtotal) as TRIP_AMOUNT')->where('month(trip_enddate)', $month)->where('year(trip_enddate)', $year)->where('trip_status',"2")->get('trip_management')->row();
    $d=$year."-".$month."-01";
	$mon=date('F',strtotime($d));
    $this->msdata['msmonth'] = $mon;
    $this->msdata['msyear'] = $year;
}
*/

public function get_summary_details($month,$year)
{
	$this->msdata['salary'] = $this->db->select('SUM(salary_paidamount) as SALARY')->where('month(salary_date)', $month)->where('year(salary_date)', $year)->get('staff_salary')->row();
	
	$this->msdata['advance'] = $this->db->select('SUM(sal_adv_amount) as ADVANCE')->where('month(sal_adv_vdate)', $month)->where('year(sal_adv_vdate)', $year)->get('salary_advance')->row();

    $this->msdata['vehicle_expense'] = $this->db->select('SUM(expense_amount) as EXPENSE')->where('month(expense_billdate)', $month)->where('year(expense_billdate)', $year)->get('vehicle_expense')->row();

    $this->msdata['general_expense'] = $this->db->select('SUM(gen_expense_amount) as GEN_EXPENSE')->where('month(gen_expense_billdate)', $month)->where('year(gen_expense_billdate)', $year)->get('general_expense')->row();

    //$this->msdata['vehicle_service'] = $this->db->select('SUM(service_invoiceamt) as SERVICE_EXPENSE')->where('month(service_invoicedate)', $month)->where('year(service_invoicedate)', $year)->get('vehicle_service')->row();

    $this->msdata['vehicle_loan'] = $this->db->select('SUM(expense_amount) as LOAN_EXPENSE')->where('month(expense_billdate)', $month)->where('year(expense_billdate)', $year)->where('expense_type','10')->get('vehicle_expense')->row();
	
	$this->msdata['additional_income'] = $this->db->select('SUM(addi_inc_amount) as ADDI_INCOME')->where('month(addi_inc_date)', $month)->where('year(addi_inc_date)', $year)->get('additional_incomes')->row();
	
    //$msdata['trip_vehicles'] = $this->db->distinct()->select('trip_vehicle_id,trip_vehicle_regno')->where('month(trip_enddate)', $month)->where('year(trip_enddate)', $year)->get('trip_management')->result();
	$this->msdata['trip_total'] = $this->db->select('SUM(trip_gtotal) as TRIP_AMOUNT')->where('month(trip_enddate)', $month)->where('year(trip_enddate)', $year)->where('trip_status',"2")->get('trip_management')->row();
    $d=$year."-".$month."-01";
	$mon=date('F',strtotime($d));
    $this->msdata['msmonth'] = $mon;
    $this->msdata['msyear'] = $year;
}

public function summary_report()
{
	$this->load->library('pdf1');
	$pdf=$this->pdf1->load();
	$month=$this->session->userdata('msr_month');
	$year=$this->session->userdata('msr_year');

	$this->get_summary_details($month,$year);
	//var_dump($this->msdata);
	
	$html=  $this->load->view('pdf/monthly_summary_pdf',$this->msdata,true);
	$pdf->WriteHtml($html);
	$output='alllogreports'.date('Y_m_d_H').'_.pdf';
	$pdf->Output("$output",'I');
	exit();
}


  public function assets()
  {
    $data['result'] = $this->db->order_by('asset_id','desc')->get('assets')->result();

    $html=  $this->load->view('pdf/asset_pdf',$data,true); 

    $this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
    $pdf->WriteHtml($html);
    $output='assets_'.date('Y_m_d').'_.pdf';
    $pdf->Output("$output",'I');
    exit();
  }
 
 
 public function Stock()
  {
	$this->load->Model('Model_stock');
	
    $stodata['stock']=$this->Model_stock->view_stock();
	
    $html=  $this->load->view('pdf/stock_items_pdf',$stodata,true); 

    $this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
    $pdf->WriteHtml($html);
    $output='assets_'.date('Y_m_d').'_.pdf';
    $pdf->Output("$output",'I');
    exit();
  }
 
 
 
 
 public function Delivery()
  {
	$dt11=$this->session->userdata('delidt1');
	$dt12=$this->session->userdata('delidt2');
	
	$this->load->Model('Model_stock');
	if($dt11=="" and $dt12=="")
	{
	$delidata['delidata']=$this->Model_stock->view_delivery();
	}
	else
	{
    $delidata['delidata']=$this->Model_stock->view_delivery1($dt11,$dt12);
	}
	
    $html=  $this->load->view('pdf/delivery_items_pdf',$delidata,true); 

    $this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
    $pdf->WriteHtml($html);
    $output='assets_'.date('Y_m_d').'_.pdf';
    $pdf->Output("$output",'I');
    exit();
  }
 
 public function Incomes()
{
	$this->load->view('admin/additional_income_rep');
}
 
 
 public function Additional_income()
{
	    $op=$this->uri->segment(3);
		
		$this->load->library('pdf1');
		$pdf=$this->pdf1->load();
		
        $this->load->model('Model_general');
		
		if($op=='1')
		{
				$dt1=$this->uri->segment(4);
				$dt2=$this->uri->segment(5);
				
						$dt11=$dt1; //form message
						$dt21=$dt2;
				
				$d1=explode("-",$dt1);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$dt2);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];

			$incdata['addiinc']=$this->Model_general->view_additional_incomes($dt1,$dt2,"","");
			$this->session->set_flashdata('inchead', 'Additional Income List of : <b>'.$dt11." => ".$dt21.'</b>');
		}
		else if($op=='2')
		{
			$mon=$this->uri->segment(4);
			$m1=date('Y')."-".$mon."-"."01";
			$incdata['addiinc']=$this->Model_general->view_additional_incomes("","",$mon,"");
			$this->session->set_flashdata('inchead', 'Additional Income List of : <b>'. date('F',strtotime($m1)).'</b>');
		}
		else
		{
		$this->session->set_flashdata('inchead', 'Additional Income List of : <b>'.date('F')." - ".date('Y').'</b>');
		$exdata['addiinc']=$this->Model_general->view_additional_incomes("","","","0");
		}
		
		$html=  $this->load->view('pdf/addi_income_report_pdf',$exdata,true);
		$pdf->WriteHtml($html);
		$output='alllogreports'.date('Y_m_d_H').'_.pdf';
		$pdf->Output("$output",'I');
	
}
	
	
}
