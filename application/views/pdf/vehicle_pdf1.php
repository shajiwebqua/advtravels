<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>
<?php
if($month==1)
{
$mon="-[ January ]";
}
else if($month==2)
{
  $mon="-[ February ]";
}
else if($month==3)
{
  $mon="-[ March ]";
}
else if($month==4)
{
  $mon="-[ April ]";
}
else if($month==5)
{
  $mon="-[ May ]";
}
else if($month==6)
{
  $mon="-[ June ]";
}
else if($month==7)
{
  $mon="-[ July ]";
}
else if($month==8)
{
  $mon="-[ August ]";
}
else if($month==9)
{
  $mon="-[ September ]";
}
else if($month==10)
{
  $mon="-[ October ]";
}
else if($month==11)
{
  $mon="-[ November ]";
}
else if($month==12)
{
  $mon="-[ December ]";
}

else
{
$mon=="";
}
?>

 <table width="100%"><tr><td><h4>Servicing Vehicles List <?=$mon;?> </h4></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
		<table style="width:100%;border:1px solid #e4e4e4;" cellpadding=5 cellspacing=0>
		   <thead>
			<tr>
			   <tr style='color:#4b88ed;border:1px solid #e4e4e4;'>
			    <th class='th1' width='70px'>Slno</th>
				<th class='th1' width='120px'>Date</th>
				<th class='th1' width='200px'>Vehicle Reg.No</th>
				<th class='th1' width='200px'>Vehicle Type</th>
				<th class='th1'>Start Date</th>
				<th class='th1'>End Date</th>
				<th class='th1'>Status</th>
				</tr>
			</tr>
		</thead>
		<tbody>
			<?php
			$slno=1;
			
				foreach ($results as $key => $value) {
					
					if($value->srtrip_staus=='1')
					{
						$st="<font color=red>Servicing</font>";
					}
					else
					{
						$st="<font color=green>Completed</font>";
					}
				?>
				<tr >
				<td class='td1'><?=$slno;?> </td> 
				<td class='td1'><?=$value->vehicle_date;?> </td> 
				<td class='td1'><?=$value->vehicle_regno;?> </td>
				<td class='td1'><?=$value->veh_type_name;?> </td>
				<td class='td1'><?=$value->srtrip_sdate;?> </td>
				<td class='td1'><?=$value->srtrip_edate;?> </td>
				<td class='td1'><?=$st;?> </td>
				
			<!-- 	<td class='td1'><?=$value->vehicle_status;?> </td> -->
			</tr>
				<?php
				
				$slno++;
				}
				?>
		  
		</tbody>
		</table>
</body>
</html>