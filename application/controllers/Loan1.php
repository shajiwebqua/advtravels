<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan1 extends CI_Controller {

	function __construct(){
		parent::__construct();
		$user = $this->session->userdata('userid');
		if (!isset($user))
		{ 
			redirect('/');
		} 
		else
		{ 
			return true;
		}
	}
	
	public function loan_ajax()
	{
		$this->load->model('Model_loan');

		$results = $this->Model_loan->select("loan_vehicle_master",array("loan_vehiclestatus" => "1"));

		$data1 = array();
		
		foreach($results  as $r) 
		{
			$edit="<a href='".base_url()."Loan/edit_loan_master/".$r->loan_vmid."' id='$r->loan_vmid' data-toggle='modal' class='edit open-AddBookDialog2'>
			<center><button class='btn btn-primary btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
			$approve="<a href='#myModal1' id='{$r->loan_vmid}' data-toggle='modal' class='approve open-AddBookDialog2'>
			<center><button class='btn btn-primary btn-xs' data-title='Delete' data-toggle='modal' >Approve</button></center></a>";
			$print="<a href='pdf_trip/{$r->loan_vmid}'  class='print open-AddBookDialog2'>
			<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-print'></span></button></center></a>";
			$share="<a href='#myModals' id='{$r->loan_vmid}' data-toggle='modal' class='share open-AddBookDialog2'><center><button class='btn btn-success btn-xs' data-title='Share' data-toggle='modal' ><span class='glyphicon glyphicon-share'></span></button></center></a>";
			
			$del=anchor('Loan/del_entry/'.$r->loan_vmid, 'Delete', array('id' =>'del_conf'));
			$st="";
			if($r->loan_vehiclestatus=='1')
			{
				$st="<font color=red>New</font>";
			}
					
			array_push($data1, array(
				"loanid"=>$r->loan_vmid,
				"ltlrefno"=>	'<span class="ltlrefno">' .$r->loan_ltlrefno .'</span>',
				"vehiclename"	=>	$r->loan_vehiclename,
				"vehicleamount"	=>	$r->loan_vehicleamount,
				"loanamount"=>$r->loan_amount,
				"loanterm"	=> $r->loan_interestrate ."%" .", ". $r->loan_term. " Months",
				"interest"=>$r->loan_interest,
				"loantotal"=>$r->loan_amount+$r->loan_interest,
				"status"=>$st,
				));
		}
		echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
	}
	
	public function loanshedule_ajax(){
		$this->load->model('Model_loan');
		$ltl_refrence = $this->uri->segment(3);
    	$results = $this->Model_loan->select("loan_shedule",array("ltl_ref_no" => $ltl_refrence));
    	$i=0;
    	$data1 = array();
    	foreach($results  as $r) 
		{			
			array_push($data1, array(
				"slno"		=> ++$i,
				"loan_date"	=> $r->loan_date,
				"loan_amt"	=>$r->loan_amt,
				"loan_instalment"=>	$r->loan_instalment,
				"loan_intrest"=> $r->loan_intrest,
				"loan_principal"=> $r->loan_principal,
				"check_no"=> $r->check_no,
				"voucher_no"=> $r->voucher_no,
			));
		}
		echo json_encode(array('data' => $data1));
	}
	
public function get_details()
{
	$ltl= $this->input->post("lrno");
	$this->load->model("Model_loan");
	$loanDT = $this->Model_loan->select("loan_vehicle_master",array("loan_ltlrefno" => $ltl));
	$dt=array();

	foreach($loanDT as $r1)
	{
		$dt=array('ltlref'=>$r1->loan_ltlrefno,
		'vname'=>$r1->loan_vehiclename,
		'vregno'=>$r1->loan_vehicleno,
		'lamount'=>$r1->loan_amount,
		'linterest'=>$r1->loan_interest,
		'ltotoal'=>$r1->loan_totalamt,
		'period'=>$r1->loan_term."months",
		);
	}
	echo json_encode($dt);
}

/// loan - re-payment 

public function repayment()
{
	$this->load->model('Model_loan');
	$data['results'] = $this->Model_loan->select("loan_vehicle_master",array("loan_vehiclestatus"=>"1"));
	//$data['results1'] = $this->Model_loan->select("loan_vehicle_master",array("ltl_refno" => $lrno));
	$this->load->view('admin/loan_repayment',$data);
}


public function repayment_ajax(){
	$ltlref=$this->uri->segment(3);
	$ltlref=str_replace("%20", " ", $ltlref);
	
		$this->load->model('Model_loan');
		$results = $this->Model_loan->select("loan_shedule",array("ltl_ref_no" => $ltlref));

		$data1 = array();
        $i=0;
		foreach($results  as $r) 
		{
			$dt=date_create($r->loan_date);
			$repayment_btn = '<button data-toggle="modal" data-target="#repayment_box" class="repay" id="'. $r->loan_shedule_id.'" onclick="setpayment(this)">Repayment</button>';
			array_push($data1, array(
				"lslno"	=>++$i,
				"lsid"	=> "<span class='id'>" . $r->loan_shedule_id ."</span>",
				"ldate"	=>"<span class='date'>" . date_format($dt, 'd-M-Y') . '</span>',
				"lamount"=>$r->loan_amt,
				"linstall"=>$r->loan_instalment,
				"linter"=>	$r->loan_intrest,
				"lprin"=>	$r->loan_principal,
				"lcheque"=>	$r->cheque_no,
				"lvoucher"=>$r->voucher_no,
				"repayment"=>$repayment_btn,
				));
		}
		echo json_encode(array('data' => $data1));
	}

	
	public function add_repayment()
	{
		$lrno=$this->input->post('ltlrefno');

		$this->load->library('form_validation');

		$this->form_validation->set_rules('ltlrefno', 'ltlrefno', 'required');
		$this->form_validation->set_rules('insmonth', 'insmonth', 'required');
		$this->form_validation->set_rules('pmode', 'pmode', 'required');
		$this->form_validation->set_rules('voucherno', 'voucherno', 'required');

		if ($this->form_validation->run())
		{	
			$lrno=$this->input->post('ltlrefno');
			$mon=$this->input->post('insmonth');
			$cheque=$this->input->post('chequeno');
			$pmode=$this->input->post('pmode');
			$voucher=$this->input->post('voucherno');

			if($pmode!=='Cheque')
			{
				$cheque=$pmode;
			}			
			$newdt=array('check_no'=>$cheque,
				"voucher_no"=>$voucher,
				"status"=>"1",
				);

			$this->load->model("Model_loan");

			$where=array('ltl_ref_no'=>$lrno,
				'MONTH(loan_date)'=>$mon
				);

			$this->Model_loan->update("loan_shedule", $newdt, $where);
			$this->session->set_flashdata('message', 'Payment Accepted.');
			redirect('Loan/repayment/'.$lrno);
		}
		else
		{
			$this->session->set_flashdata('message', '');
			redirect('Loan/repayment/'.$lrno);
		}
	}

public function update_shedule(){
		$sid1=$this->input->post("sid");
		$chno1=$this->input->post("chno");
		$vno1=$this->input->post("vno");
		echo  $sid1.",". $chno1.",". $vno1;
		
		$shedule = array(
			"cheque_no" => $chno1,
			"voucher_no" => $vno1,
			"status" => "1"
		);
		$this->load->model('Model_loan1');
		$this->Model_loan1->update("loan_shedule", $shedule,array("loan_shedule_id"=> $sid1));
		echo "true";
	}


public function loanpayment()
{
	$this->load->view('admin/loan_payments');
}

public function loanpayment_ajax(){
	
		$this->load->model('Model_loan');
		$results = $this->Model_loan->select("loan_shedule",array("MONTH(loan_date)" =>date('m'),"status"=>'1'));

		$data1 = array();
$i=0;
		foreach($results  as $r) 
		{
			array_push($data1, array(
				"ltlno"	=>$r->ltl_ref_no,
				"lsid"	=>$r->loan_shedule_id,
				"ldate"	=>$r->loan_date,
				"lamount"=>$r->loan_amt,
				"linstall"=>$r->loan_instalment,
				"linter"=>	$r->loan_intrest,
				"lprin"=>	$r->loan_principal,
				"lcheque"=>	$r->check_no,
				"lvoucher"=>$r->voucher_no,
				));
		}
		echo json_encode(array('data' => $data1));
	}

	public function setpayment(){
		echo '

		<input type="hidden" value="" name="shedule_id" id="shedule_id">
		<!--*****************************************************-->
		<div class="form-group  pad">
		<div class="row">
			<label class="control-label col-sm-3"  style="margin-top:3px;">Date : </label>
			<div class="col-sm-9"> 
				<input name="date" class="form-control" id="date" onfocus="$(this).blur()">
			</div>
		</div>
		</div>
		<div class="form-group  pad">
		<div class="row">
			<label class="control-label col-sm-3"  style="margin-top:3px;">Payment Mode</label>
			<div class="col-sm-9"> 
				<select class="form-control" name="paymentmode" id="paymentmode">
					<option>--Select Payment Mode--</option>
					<option value="Transfer">Transfer</option>
					<option  value="Cheque">Cheque</option>
					<option  value="Cash">Cash</option>
				</select>
			</div>
		</div>
		</div>
		<div class="form-group  pad">
		<div class="row">
			<label class="control-label col-sm-3"  style="margin-top:3px;">Cheque No</label>
			<div class="col-sm-9"> 
				<input name="cheque" class="form-control" id="cheque" required>
			</div>
		</div>
		</div>
		<div class="form-group  pad">
		<div class="row">
			<label class="control-label col-sm-3"  style="margin-top:3px;">Voucher No</label>
			<div class="col-sm-9"> 
				<input name="voucher" class="form-control" id="voucher" >
			</div>
		</div>
		</div>
		<div class="row">
			<div class="col-sm-4 col-sm-offset-3"> 
				<button type="button" class="btn btn-success btn-md" id="repayment_add" >Add</button>
			</div>
		</div>
		</div>

		';
	}
}

