<?php
 include('application/views/common/header.php');
 ?>

 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>New Vehicle Entry</b></h1>
	
    <ol class="breadcrumb" style='font-size:15px;'>
	<li><a href="<?php echo base_url('Vehicle/index');?>" style='color:#4b88ed;'><i class="fa fa-backward" aria-hidden="true"></i>Back to List</a></li>
	
	<!-- <li><a href="<?php echo base_url('owner/index');?>"style='color:#4b88ed;'><i class="fa fa-address-card" aria-hidden="true"></i>Vendors</a></li>
	<li><a href="<?php echo base_url('Driver/index');?>"style='color:#4b88ed;'><i class="fa fa-user-plus" aria-hidden="true"></i>Drivers</a></li>
	<li><a href="<?php echo base_url('Services/index');?>" style='color:#4b88ed;'><i class="fa fa-file-text-o" aria-hidden="true"></i>Services</a></li> -->

	<!--<li><a href="<?php echo base_url('owner/index');?>" style="color:blue;"><h5><b>Owners List</b></h5></a></li>
	<li><a href="<?php echo base_url('Driver/index');?>" style="color:blue;" ><h5><b>Drivers List</b></h5></a></li>
	<li><a href="<?php echo base_url('Vehicle/add_vehicle');?>"  data-target="#myModal1" data-toggle='modal'  style="color:blue;"><h5><b>Add Vehicle</h5></b></a></li>
	<li><a href="<?php echo base_url('Services/index');?>" style="color:blue;"><h5><b>Service Details</h5></b></a></li>-->
	
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 	  
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
	<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
	
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details -->
	
<div style="background-color:#fff;padding:15px; ">
			
<div class="page-content-wrapper">
 	
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
			
        <div class="row">
        <div class="col-md-5">
		<div class='rows'>
		 <label style="padding-bottom:5px;color:blue; border-bottom:1px solid #00adee;margin:0px; width:80%;">Vehicle Details</label>
		 <label style="padding-bottom:5px;color:blue; border-bottom:1px solid #00adee;margin:0px;">
		 <a href='<?php echo site_url('vehicle/accessory/0');?>' style='color:green;'><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;New Data</a>
		 </label>
		 </div>
			<div style="padding-top:10px;">
					<form  class='form-horizontal' role='form' method='post' action="<?php echo site_url('vehicle/add_new_vehicle');?>"' enctype='multipart/form-data'>
                    <!-- text input -->
          
							<!--  <input list='vendors' name='vendor' /></label>
								<datalist id='vendors'>
									  <option value='Shaji'>
									  <option value='Renny'>
									  <option value='Shinil'>
									  <option value='Raju'>
									  <option value='Suresh'>
									  <option value='Subheesh'>
								</datalist>  -->
		  
					<div class='form-group'>
						  <input type='hidden' name='date' class='form-control' value="date('Y-m-d')"/>
						  <label class='col-md-4 control-label' style='padding-top:5px;'>Vendor Name</label>
						   <div class='col-md-7'>
							<select name='owner'  class='form-control id11' required>
							 <option value="">----</option>
							 <?php
									$section = $this->db->select('*')->from('vehicle_ownerreg')->get();
									$sectn1=$section->result();
									foreach ($sectn1 as $ro):
									   echo "<option value='$ro->owner_id'>".$ro->owner_name."</option>";
									endforeach;
									?>
							   echo "                     
									</select>
						   </div>
					</div>

                            
					 <div class='form-group'>
						   <label class='col-md-4 control-label' style='padding-top:5px;'>Registration No </label>
						   <div class='col-md-7'>  
								<input type='text'  name='regno' class='form-control id11' required  />
						   </div>                   
					  </div>

					  <div class='form-group'>
								 <label class='col-md-4 control-label' style='padding-top:5px;'>Vehicle Type </label>
								<div class='col-md-7'>  
								
								<select name='type' class='form-control id11' required >
								 <option value="">----</option>
							 <?php
									$section = $this->db->select('*')->from('vehicle_types')->get();
									$sectn1=$section->result();
									foreach ($sectn1 as $ro):
									   echo "<option value='$ro->veh_type_id'>".$ro->veh_type_name."</option>";
									endforeach;
									?>
							   echo "                     
									</select>
						 </div>                   
					  </div>
       
					  <div class='form-group'>
					  <label class='col-md-4  control-label' style='padding-top:5px;'>No Of Seats </label>
					  <div class='col-md-7'>
					  <input type='number' class='form-control id11' placeholder='' name='seats' required>
						</div>
					  </div>
					  
					   <div class='form-group'>
					 <!-- <label class='col-md-4  control-label'>Rent Period</label>
					  <div class='col-md-7'>
					  <select name='rentperiod' class='form-control id11' required>
								<option value=''>---------</option>
								<option value='1'>DAILY</option>
								<option value='2'>WEEKLY</option>
								<option value='3'>MONTHLY</option>
								</select>
					  </div> -->
					  <input type='hidden' class='form-control id11' placeholder='' name='rentperiod'  value='2' required/>
					  </div>
								
					  <div class='form-group'>
					 <!-- <label class='col-md-4  control-label'>Rent</label> -->
					  <div class='col-md-7'>
					  <input type='hidden' class='form-control id11' placeholder='' name='rent' value='0' required/>
					  </div>
					  </div>
		  
		  
					  <label style='height:1px;width:100%;background-color:#e4e4e4;'></label>		  
					  <div class='form-group'> 
							<div class='col-md-4'>
							</div>
							<div class='col-md-7'>
							<input type='submit' class='btn btn-primary' id='saveD' value='Save Details'/>
							</div>
					  </div>  
					 <label style='height:1px;width:100%;background-color:#e4e4e4;'></label>					  
					  </form>
					  </div>
					<label style="border-bottom:1px solid blue;color:blue">Added Vehicle Details</label>
					
					<div style="padding-top:3px;">
					<fieldset style="border:1px solid #e4e4e4;padding:10px;border-radius:10px;min-height:150px;" >
					<?php
					    $lid=$this->uri->segment(3);
						$section = $this->db->select('*')->from('vehicle_registration')->join('vehicle_ownerreg', 'vehicle_ownerreg.owner_id=vehicle_registration.vehicle_owner','inner')->
		               join('vehicle_types', 'vehicle_types.veh_type_id=vehicle_registration.vehicle_type','inner')->where('vehicle_registration.vehicle_id',$lid)->get();
						$sectn1=$section->row();
						if(is_object($sectn1))
						{
							if($sectn1->vehicle_rentperiod==1)
							{
								$rentperiod="Daily";
							}
							else if($sectn1->vehicle_rentperiod==2)
							{
							$rentperiod="Weekly";	
							}
							else
							{
								$rentperiod="Monthly";
							}
						echo "<table>";
						   echo "<tr><td width='120px'>ID</td><td>:</td><td>".$sectn1->vehicle_id."</td></tr>";
						   echo "<tr><td>Date</td><td>:</td><td>".$sectn1->vehicle_date."</td></tr>";
						   echo "<tr><td>Vendor Name</td><td>:</td><td>".$sectn1->owner_name."</td></tr>";
						   echo "<tr><td>Registration No</td><td>:</td><td>".$sectn1->vehicle_regno."</td></tr>";
						   echo "<tr><td>Type</td><td>:</td><td>".$sectn1->veh_type_name."</td></tr>";
						   echo "<tr><td>No of Seats</td><td>:</td><td>".$sectn1->vehicle_noofseats."</td></tr>";
						   echo "<tr><td>Rent</td><td>:</td><td>".$sectn1->vehicle_rent."</td></tr>";
						   echo "<tr><td>Rent Period</td><td>:</td><td>".$rentperiod."</td></tr>";
					echo "</table>";
						}
						else
						{
							echo '<center><p style="margin-top:50px;font-size:12px;">Data Not found...!</p></center>';
						}
					?>
					</fieldset>
					</div>
	  
			</div>
			<div class="col-md-7">
			<label style="padding-bottom:5px;color:blue; border-bottom:1px solid #00adee;margin:0px; width:100%;">Vehicle Accessory Entry</label>
			
				<div style="padding-top:10px;">
				<form  class='form-horizontal' role='form' method='post' action="<?php echo site_url('Vehicle/add_new_accessory');?>" enctype='multipart/form-data'>
				<div class='form-group'>
						   <label class='col-md-4 control-label' style='padding-top:5px;'>Vehicle Id</label>
						   <div class='col-md-7'>  
								<input type='text' name='alid' id='alid' class='form-control' value="<?php echo $lid;?>" readonly/>
						   </div>                   
				</div>
   
				<div class='form-group'>
						   <label class='col-md-4 control-label' style='padding-top:5px;'>Accessory Name </label>
						   <div class='col-md-7'>  
								<input type='text' name='acname' class='form-control id11' required  />
						   </div>                   
				</div>
				
				<div class='form-group'>
						   <label class='col-md-4 control-label' style='padding-top:5px;'>No of iems</label>
						   <div class='col-md-7'>  
								<input type='text' name='acnos' class='form-control id11' required  />
						   </div>                   
				</div>
				
				<div class='form-group'> 
							<div class='col-md-4'>
							</div>
							<div class='col-md-7'>
							<input type='submit' class='btn btn-primary' id='addbtn' value='Add'/>
							</div>
							</div>               
						
				<div class='rows'>
				<label style="padding-bottom:5px;color:blue; border-bottom:1px solid #00adee;margin:0px; width:100%;">Accessory List</label>
					<table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
								<tr>
								 <th>Accessory Name</th>
								 <th>Nos</th>
								 <th>Del</th>
								 </tr>
								</thead>
                        </table>
				</div>
				</form>
				</div>
			
			</div>
		<!-- tab pages  end---------------------------------- -->	
			
		</div>
		</div>
			   
				   
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                  <!--          <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
								<tr>
								 <th>Edit</th>
								
								 <th>Delete</th>
								 <th>Date</th>
								 <th>Owner</th>
								 <th>Reg No</th>
								 <th>Type</th>
								 <th>Seat</th>
								 <th>Rent</th>
								 <th>Rent Period</th>
								 <th>Status</th>
								</tr>
								</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                       <!-- </div> -->
                 <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
		
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Edit</h4>
								</div>
							</div>
							</div>
						<!-- /.modal-dialog -->
						</div>
                   </div>	

                    <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>       						
				
	</div>
	<!-- End user details -->
	</div>
	
	</div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

$("#idmsg").hide();

//sweet alert ----------------------------
	if($("#idh5").html()!="")
	{
		swal( $("#idh5").html(),"","success")
		$("#idh5").html("");
	}
// sweet alert box -------------------------- 



  $(document).ready(function(){
	   //$('.id11').prop('disabled',true);
		//$('#saveD').prop('disabled',true);
		//$('#addbtn').prop('disabled',true);
  });

 
	var lid=$('#alid').val();
 
      $('#example').dataTable( {
		 "ordering":false,
		 "bInfo" : false,
		 "bPaginate": false,
         "bFilter": false,
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "vehicle/accessory_ajax/"+lid,// json datasource
                },
			   "columnDefs":[
					{"width":"5%","targets":1},
					{"width":"7%","targets":2},
				],
			   
        "columns": [
            { "data": "accessory"},
            { "data": "anos" },
			{ "data": "delete" },
      ]
  } );

$('#newbtn').click(function()
{
	 $('.id11').prop('disabled',false);
	 $('#saveD').prop('disabled',false);
});
  
$('#saveD').click(function()
{
	 $('#addbtn').prop('disabled',false);
});  
 /* 
  
$('#example tbody').on('click', '.add', function () {
        var Result=$("#myModal1 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         // var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "vehicle/add_vehicle",
        dataType: 'html',
        // data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 

      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "vehicle/edit_vehicle",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 


  $('#example tbody').on('click', '.change', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "userprofile/change_password",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 		

		*/
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

 //});
  


</script>



