<?php
include('application/views/common/header.php');
?>
  <style>
  .dashbtn
  {
	  width:75px;
	  height:75px;
	  background-color:#f4f4f4;
	  border:1px solid #fff;
	  /*border-radius:10px;*/
	  padding:5px;
	  text-align:center;
  }
  
.sh4
{
 border-bottom:1px solid #e4e4e4;
 width:100%;
 color:#00adee;
 padding-bottom:5px;
 margin-top:0px;"
}

.chart1
{
	float:left;
	position:relative;
	width:195px;
	text-align:left;
}

.my-btn{
	padding:0px 5px 0px 5px;
	font-size:12px;
}
</style>
 
 <link rel="stylesheet" href="<?php echo base_url('assets/chart/css/material-charts.css');?>">
 <script src="<?php echo base_url('assets/chart/js/material-charts.js');?>"></script>
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#203856'><b>Dashboard </b>
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
     <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>

        <!-- Main content -->
    <section class="content">
		
		
	<!-- for chart data------------------->
		<input type='hidden' name='tkm' id='tkm' value='<?php echo $this->session->userdata('tkm');?>'>
		<input type='hidden' name='drna' id='drna' value='<?php echo $this->session->userdata('drna');?>'>
		<input type='hidden' name='tkm1' id='tkm1' value='<?php echo $this->session->userdata('tkm1');?>'>
		<input type='hidden' name='dr_id' id='dr_id' value='<?php echo $this->session->userdata('drvid');?>'>
		<input type='hidden' name='dr_id1' id='dr_id1' value='<?php echo $this->session->userdata('drvid1');?>'>
		
	<!-- for chart data------------------->	

			<div class='row'>

			<div class='col-md-3'>
			<div class="info-box">
				    <div >
					<center><span class="info-box-text" style='padding:3px;background-color:#864141;color:#fff;'>Document Renewals</span></center>
					<?php 
					$hcust=$this->db->select('COUNT(*) as hccount')->from('customers')->get()->row();
					?>
					<span style='font-size:20px;font-weight:600;padding:3px 10px 3px 25px;'><?php echo $cnt;?></span>
					<span style='float:right;margin-right:20px;padding-top:3px;'> <a href="<?php echo site_url('General/renewals');?>"><button class='btn  btn-danger btn-xs my-btn;' >More</button></a></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>


			<div class='col-md-3'>
			<div class="info-box">
				    <div >
					<center><span class="info-box-text" style='padding:3px;background-color:#494949;color:#fff;'>Running Trips</span></center>
					<?php 
					$rveh=$this->db->select('COUNT(*) as runveh')->from('trip_management')->where('trip_status=1')->where('trip_startdate<=',date('Y-m-d'))->get()->row();
					?>
					<span style='font-size:20px;font-weight:600;padding:3px 10px 3px 25px;'><?php echo $rveh->runveh;?></span>
					<span style='float:right;margin-right:20px;padding-top:3px;'><a href="<?php echo site_url('Trip/view_trip');?>"><button class='btn  btn-info btn-xs my-btn;' >More</button></a></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>


			<div class='col-md-3'>
			<div class="info-box">
				    <div >
					<center><span class="info-box-text" style='padding:3px;background-color:#404e6d;color:#fff;'>Upcoming Trips</span></center>
					<?php 
					//$sta=$this->db->select('COUNT(*) as stano')->from('staffregistration')->where('staff_status=1')->get()->row();
					$upveh=$this->db->select('COUNT(*) as upcveh')->from('trip_management')->where('trip_status=1')->where('trip_startdate>=',date('Y-m-d'))->get()->row();
					?>
					<span style='font-size:20px;font-weight:600;padding:3px 10px 3px 25px;'><?php echo $upveh->upcveh;?></span>
					<span style='float:right;margin-right:20px;padding-top:3px;'><a href="<?php echo site_url('Trip/view_trip');?>"><button class='btn  btn-success btn-xs my-btn;' > More</button></a></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>

			<div class='col-md-3'>
			<div class="info-box">
				    <div >
					<center><span class="info-box-text" style='padding:3px;background-color:#144400;color:#fff;'>Service Required Vehicles</span></center>
					<?php 
					$hcust=$this->db->select('COUNT(*) as hccount')->from('customers')->get()->row();
					?>
					<span style='font-size:20px;font-weight:600;padding:3px 10px 3px 25px;'><?php echo $servehicle;?></span>
					<span style='float:right;margin-right:20px;padding-top:3px;'><a href="<?php echo site_url('Vehicle/service_vehicles');?>"><button class='btn  btn-warning btn-xs my-btn;' >More</button></a></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>

			</div>


	<!--------CHART END ---------------------------------------------------------------------->
<style>
.btn-wl
{
	width:210px;
	text-align:left;
	padding-left:5px;
}
</style>
	
	
		<div class='row' style="padding:5px 15px 0px 15px;">
		
		<div class='col-md-9'>
		
	    <div class="box box-info1 box-solid " style="border:1px solid #c4c4c4;margin-bottom:12px;">
	    <!--<div class="box-header">
          <h3 class="box-title"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Quick Task</h3>
        </div>-->
		<label style='margin-left:10px;font-size:18px;color:#000;'><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Quick Tasks </label>
		<hr style='margin:0px;'>
        <div class="box-body" style="margin-top:0px;padding-top:0px;">
		 <div class='row' style='padding:0 15px;'>
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip/index');?>"> <img src="<?php echo base_url('assets/dist/img/01.png');?>" ></a>
			 </div>
			 
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip/view_trip');?>"> <img src="<?php echo base_url('assets/dist/img/02.png');?>" ></a>
			 </div>
			 
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Vehicle/vehicle_expense');?>"> <img src="<?php echo base_url('assets/dist/img/03.png');?>" ></a>
			 </div>
			 
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('General/Expenses');?>"> <img src="<?php echo base_url('assets/dist/img/04.png');?>" ></a>
			 </div>
		 </div>
        </div>
		
	</div>
	<!--  second row --->
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4;margin-bottom:12px;">
	<label style='margin-left:10px;font-size:18px;color:#000;'><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Booking/Lists </label>
	<hr style='margin:0px;'>
        <div class="box-body" style="margin-top:0px;padding-top:0px;">
		 <div class='row' style='padding:0 15px;'>
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip/booking_trip');?>"> <img src="<?php echo base_url('assets/dist/img/r4.png');?>" ></a>
			 </div>

			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip/booking_list');?>"> <img src="<?php echo base_url('assets/dist/img/r3.png');?>" ></a>
			 </div>
			 
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip_reports/view_vehicleTrip');?>"> <img src="<?php echo base_url('assets/dist/img/r1.png');?>" ></a>
			 </div>
			 
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip_reports/index');?>"> <img src="<?php echo base_url('assets/dist/img/r2.png');?>" ></a>
			 </div>
		 </div>
        </div>
	</div>
	
	<!--  third row --->
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4;margin-bottom:12px;">
	
        <div class="box-body" style='padding-bottom:15px;'>
		 <div class='row'>
			 <div class='col-md-3'>
			  <a href="<?php echo base_url('Driver/performance');?>"><button class='btn btn-default' style='margin-top:10px;margin-left:15px;'>
			   Driver's Performance
		 </button></a>
			 </div>
		 </div>

        </div>
		
	</div>
	
	</div>
	
	<div class='col-md-3' style='padding-left:15px;'>
		
	    <div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	   <!-- <div class="box-header">
          <h3 class="box-title"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Accounts</h3>
        </div> -->
		<label style='margin-left:10px;font-size:18px;color:#000;'><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Accounts </label>
		<hr style='margin:0px;'>
				
		<div class="box-body" style='padding-bottom:15px;margin-bottom:5px;'>
		 <div class='row' style='padding:0 15px;'>
		<center>
		 <a href="<?php echo base_url('Investment/Add');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/011.png');?>" width="40px" height="40px">
		   &nbsp;&nbsp;Investments
		 </button></a>
		 
		 <a href="<?php echo base_url('Ledger/Add');?>"> <button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/066.png');?>"  width="42px" height="42px">
		 &nbsp;&nbsp;Create Ledger
		 </button></a>
		 
		 <a href="<?php echo base_url('Ledger/View');?>"> <button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/022.png');?>"  width="42px" height="42px">
		 &nbsp;&nbsp;View Ledgers
		 </button></a>
		 		 
		 <a  href="<?php echo base_url('Account/profit_loss');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/033.png');?>"  width="42px" height="42px">
		 &nbsp;&nbsp;Profit & Loass Accounts
		 </button></a>
		 
		 <a href="<?php echo base_url('Account/balance_sheet');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/044.png');?>"  width="42px" height="42px">
		 &nbsp;&nbsp;Balance Sheet
		 </button></a>
		 		 
		 <a href="<?php echo base_url('Account/trial_balance');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/055.png');?>"  width="42px" height="42px">
		 &nbsp;&nbsp;Trial Balance
		 </button></a>
		 
       </center>

		</div>
        </div>
		
	</div>
	</div>
	</div>
	
</section>
    <!-- content wrapper -->
</div>
     <?php
  include('application/views/common/footer.php');
  ?>
 
  
</body>
</html>

  <script type="text/javascript">
  $(document).ready(function()
  {
	  
	  $(".fset").mouseover(function()
	  {
	  $(this).css("background-color","#aeaeae");
	  });
	  	  
	  $(".fset").mouseleave(function()
	  {
	  $(this).css("background-color","#d2d3d4");
	  });
	  
	  
	  
	/*  $('#example').dataTable( {
		 "ordering":false,
		 "bInfo" : false,
		 "destroy": true,
        "processing": true,
		"pageLength": 5,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Vehicle/periodic_service_ajax/1",// json datasource
                },
			 			   
        "columns": [
            { "data": "sid"},
            { "data": "regno" },
			{ "data": "vtype" },
			{ "data": "kilometer" },
			{ "data": "status" },
      ]
  } );*/
	  

 /* $('#example2').dataTable( {
		 "ordering":false,
		 "bInfo" : false,
		 "destroy": true,
        "processing": true,
		"pageLength": 5,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Vehicle/periodic_servicing_ajax",// json datasource
                },
			 			   
        "columns": [
            { "data": "sid"},
            { "data": "regno" },
			{ "data": "vtype" },
			{ "data": "kilometer" },
			{ "data": "status" },
      ]
  } );*/
  
  
	  
$('#example1').dataTable( {
		"ordering":false,
		"bInfo" : false,
        "destroy": true,
        "processing": true,
		"pageLength": 5,
		  // "ajax": "<?php echo site_url('report/course_fee');?>",
      
        "ajax": {
                url :"<?php echo base_url(); ?>" + "trip/starter_trip_ajax",// json datasource
                },
"columnDefs":[
{"width":"20%","targets":4},
],			 			   
        "columns": [
            { "data": "tid"},
            { "data": "customer" },
			{ "data": "driver" },
			//{ "data": "dmobile" },
			{ "data": "date" },
			{ "data": "fromto" },
			{ "data": "status" },
			
      ]
  } );
	  
	  
	$('#upcoming').dataTable( {
		"ordering":false,
		"bInfo" : false,
        "destroy": true,
        "processing": true,
		"pageLength": 5,
		  // "ajax": "<?php echo site_url('report/course_fee');?>",
      
        "ajax": {
                url :"<?php echo base_url(); ?>" + "trip/upcoming_trip_ajax",// json datasource
                },
"columnDefs":[
{"width":"20%","targets":4},
],			 			   
        "columns": [
            { "data": "tid"},
            { "data": "customer" },
			{ "data": "driver" },
			//{ "data": "dmobile" },
			{ "data": "date" },
			{ "data": "fromto" },
			{ "data": "status" },
			
      ]
  } );
	    
  });
  </script>
 <script>
 $(document).ready(function()
  {


  });
  
</script>
<script type='text/javascript'>


  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- BAR CHART -
    //--------------
 
   var d11=$("#tkm").val();
   var d2=d11.split(",");
   
   var d12=$("#tkm1").val();
   var d21=d12.split(",");
      
   var dr_na=$("#drna").val();
   var lbl=dr_na.split(",");

   var a1=["January", "February", "March", "April", "May", "June", "July"];

    var areaChartData = {
		
      labels: lbl,
	  
      datasets: [
        {
          label: "<?php echo date('F',strtotime("-1 month"));?>",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: d2
		  },
		  
        {
          label: "<?php echo date('F');?>",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          //data: [28, 48, 40, 19, 36, 27, 20]
		  data: d21
        }
      ]
    };
	
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

	
    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
  });
  
  
  
     //-------------------------------------------------------------------------------------------------
    //- PIE CHART -
    //---------------------------------------------------------------------------------------------------
    // Get context with jQuery - using jQuery's .get() method.
	
	
	var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
	
	<?php
	$j=1;
	$tc1=0;
	$fc1=0;
	$sc1=0;
	$oc1=0;
	$nc1=0;
	foreach($vresult as $v1)
	{
		for($l=0;$l<count($charges);$l++)
		{
			if($charges[$l]['regno']==$v1->vehicle_id)
			{
				$tc1=$charges[$l]['trip'];
				$fc1=$charges[$l]['fuel'];
				$sc1=$charges[$l]['service'];
				$oc1=$charges[$l]['others'];
				break;
			}
		}
		
		if($tc1==0 && $fc1==0 && $sc1==0 && $oc1==0)
		{
			$nc1=1;
		}
		
		
	?>
	var pieChartCanvas = $("#pieChart<?=$j;?>").get(0).getContext("2d");
    var pieChart<?=$j;?> = new Chart(pieChartCanvas);

    var PieData<?=$j;?> = [
      {
        value: <?=$tc1;?>,
        color: "#f56954",
        highlight: "#f56954",
        label: "Trip "
      },
      {
        value: <?=$fc1;?>,
        color: "#00a65a",
        highlight: "#00a65a",
        label: "Fuel "
      },
      {
        value: <?=$sc1;?>,
        color: "#f39c12",
        highlight: "#f39c12",
        label: "Service "
      },
      {
        value: <?=$oc1;?>,
        color: "#00c0ef",
        highlight: "#00c0ef",
        label: "Others "
      },
	  
	  {
        value: <?=$nc1;?>,
        color: "#e0e0df",
        highlight: "#00c0ef",
        label: "Null "
      },
      
    ];
	  
    pieChart<?=$j;?>.Doughnut(PieData<?=$j;?>, pieOptions);
 
 <?php
 $j++;
 }?>
  
  
</script>
</body>
</html>

  
  