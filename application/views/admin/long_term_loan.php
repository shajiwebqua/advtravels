<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');

?>

<style type="text/css">
	.wrap {
	    width: 352px;
	}

	.wrap table {
	    width: 300px;
	    table-layout: fixed;
	}

	table tr td {
	    padding: 5px;
	    border: 1px solid #eee;
	    width: 100px;
	    word-wrap: break-word;
	}

	table.head tr td {
	    background: #eee;
	}

	.inner_table {
	    height: 100px;
	    overflow-y: auto;
	}
	
	.search-btn
{
	margin-top:1px;
	z-index:1000;
	right:-40px;
	position:absolute;
	height:30px;
}
</style>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1 class='heading' style='color:#00adee'>Set Loan Schedule
		</h1>
		<ol class="breadcrumb">
			<!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
		</ol>
	</section>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
	<!-- Content Header end -->
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>

		<div class="row">
			<div class="col-md-12">
	<div style="background-color:#fff;border-radius:10px;padding:2px 15px 5px 15px;">
	  <div class="portlet-body form" style="margin:0px;">
	  		<div class="box-body">

	  			<div class="row well">
	  				<div class="container-fluid">
	  					<div class="row">
	  						<div class="col-md-12">
	  							<form class="form-horizontal" role="loan_form" id="loan_form" method="post" action="<?php echo base_url('loan/add_long_term_loan'); ?>">
	  							<div class="col-md-8 container">
								
								 <div class='form-group'>
						<!-- <div class='row'> -->
									<label class="control-label col-sm-2" for="ltf_ref_no">LTL Ref No</label>
										<div class='col-md-10'>
									  <section class="form-control " style='padding:0px;'>
										<select class="form-controll_prop" id="ltf_ref_no" placeholder="Choose ltl ref. no" name="ltf_ref_no">
										  <option value=""></option>
										  <?php
											$cquery=$this->db->select("ltl_refrence")->from('account_master')->where('acc_status','0')->get()->result();
											foreach( $cquery as $r)
											{
												echo "<option value='".$r->ltl_refrence."'>".$r->ltl_refrence."</option>";
											}
										  ?>
															  
										</select>
						
									</section>
								
								  </div>
						<!---  -->
						 </div>
												
									
	  								<!--	<div class="form-group">
	  										<label class="control-label col-sm-2" for="ltf_ref_no">LTL Ref No</label>
	  										<div class="col-sm-10">
	  										<div class="input-group"> 
	  											<input type="text" name="ltf_ref_no" class="form-control" id="ltf_ref_no" >
	  											<span class="input-group-addon glyphicon glyphicon-search" style="" id="ltf_ref_btn"></span>
	  										</div>
	  										</div>
	  									</div> -->
										
										
	  									<div class="form-group">
	  										<label class="control-label col-sm-2" for="bank_name">Bank Name</label>
	  										<div class="col-sm-10">
	  											<select name="bank_name" class="form-control" id="bank_name">
	  											</select>
	  										</div>
	  									</div>
	  									<div class="form-group">
	  										<label class="control-label col-sm-2" for="description">Description:</label>
	  										<div class="col-sm-10"> 
	  											<input type="text" name="description" onfocus="this.blur();" class="form-control" id="description" >
	  										</div>
	  									</div>
	  									<div class="form-group">
	  										<label class="control-label col-sm-2" for="loan_amt">Loan Amount</label>
	  										<div class="col-sm-4">
	  										<div class="input-group"> 
	  											<input type="text" name="loan_amt" onfocus="this.blur();" class="form-control" style='margin-top:1px;' id="loan_amt" >
	  										<!--	<input type='button'  id="loan_amt_btn" class ="btn btn-default search-btn" style='flot:left:margin-right:0px;' value='Calc'>-->
												<!--<span class="input-group-addon glyphicon glyphicon-arrow-right"  id="loan_amt_btn"></span> -->
	  										</div>
	  										</div>
											
	  										<label class="control-label col-sm-3" for="profit_amt">Profit Amount</label>
	  										<div class="col-sm-3"> 
	  											<input type="text" class="form-control" onfocus="this.blur();" name="profit_amt" id="profit_amt" >
	  										</div>
	  									</div>
	  									<div class="form-group">
	  										<label class="control-label col-sm-2" for="loan_ac">Loan A/C </label>
	  										<div class="col-sm-10"> 
	  											<input type="text" class="form-control" onfocus="this.blur();" name="loan_ac" id="loan_ac" >
	  										</div>
	  									</div>
	  									<div class="form-group">
	  										<label class="control-label col-sm-2" for="intrest_ac">Inerest A/C</label>
	  										<div class="col-sm-10"> 
	  											<input type="text" class="form-control" onfocus="this.blur();" name="intrest_ac" id="intrest_ac" >
	  										</div>
	  									</div>
	  									<div class="radio col-md-12" style="padding:0px;padding-bottom:10px;">
	  										<div class="col-sm-2"> 
	  											<div class="radio">
	  												<!--<label><input type="radio" name="loan_type" value="o" disabled='disabled'>Opening Loan</label>-->
	  											</div>
	  										</div>
											
	  										<div class="col-sm-2" style="padding:0px"> 
	  											<div class="radio">
	  												<label><input type="radio" name="loan_type" value="n" checked="checked">New Loan</label>
	  											</div>
	  										</div>
	  										<label class="control-label col-sm-2" for="debit_ac">Debit A/C</label>
	  										<div class="col-sm-6" style="padding:0px"> 
	  											<input type="text" class="form-control" onfocus="this.blur();" name="debit_ac" id="debit_ac" >
	  										</div>
	  									</div>
	  							</div>
	  							<div class="col-md-4" style="padding-right: 0px;">
	  								<div class="form-group" >
	  									<label class="control-label col-sm-4" for="loan_date"> Date:</label>
	  									<div class="col-sm-8"> 
	  										<input type="text" class="form-control datepicker" onfocus="this.blur();" id="datepicker2" value="<?php echo date('d-m-Y'); ?>">
	  									</div>
	  								</div>
	  								<div class="form-group" >
	  									<label class="control-label col-sm-4" for="intrest_p">Intrest % </label>
	  									<div class="col-sm-8"> 
	  										<input type="text" class="form-control" onfocus="this.blur();" name="intrest_p" id="intrest_p" >
	  									</div>
	  								</div>
	  							</div>
	  							</form>
	  						</div>
	  					</div>
	  					




	  					<div class="row well" style="border:1px solid #ADADAD">
	  						<div class="col-md-10" >
	  							<div class="form-group row">
	  								<label class="control-label col-sm-2" >Repay. Mode</label>
	  								<div class="col-sm-3"> 
	  									<select type="email" class="form-control" id="email">
	  												<option>Transfer</option>
	  												<option>Check</option>
	  												<option>Cash</option>
	  									</select>
	  								</div>
	  							</div>
								
	  							<div class="form-group row" >
	  								<label class="control-label col-sm-2" style="padding:5px 0px 0px 15px;" for="installment_amt">Installment</label>
	  								<div class="col-sm-2"> 
	  									<input type="text" class="form-control" onfocus="this.blur();" id="installment_amt" name="installment_amt">
	  								</div>
	  								<label class="control-label col-sm-2" style="padding:5px 5px 0px 0px;text-align:right" >Start Date</label>
	  								<div class="col-sm-3" style="padding-left: 0px;"> 
	  									<input type="date" class="form-control" placeholder="MM/DD/YYYY" id="start_date" >
	  								</div>
	  								<div class="col-sm-2"> 
	  									<button class="btn btn-primary " style="padding:5px;" id="get_shedule">Get Schedule</button>
	  								</div>
	  							</div>
	  						</div>

  							<div class="col-sm-12 row" style="padding:0px;">
  								<div class="col-md-4" style="padding:0px;">
  									<h5 style="margin:3px;">Repayment Shedule :</h5>
  									<hr style="border-color: black;margin:0px;margin-bottom:5px;" />
  								</div>

  							<div class="col-md-10 panel panel-default" style="height:250px;overflow-y:auto">
	       					  <table class="table table-hover table-bordered" id="example">
	                            <thead>
	       								<tr style="color:#5068f8;">
	       								 <th>SL</th>
	       								 <th>Date</th>
	       								 <th>Loan</th>
	       								 <th>Instaltment</th>
	       								 <th>Intrest</th>
	       								 <th>Principle</th>
	       								 <th>Check No</th>
	       								 <th>Voucher No</th>
	       								 </tr>
	       								</thead>
	       						<tbody id="loan_shedule">			
	       						</tbody>		
	                               </table>
  							 </div>
  							<!--<div class="col-md-2" style="float:right;padding-right: 0px;">
	  						</div> -->

  							</div>

	  						<div class="col-sm-12 row" style="padding:0px;">
	  								<label class="control-label col-sm-2" style="width:10%;" for="pwd"> Total :</label>
	  								<div class="col-sm-2" style="padding:0px;">
	  									<label class="control-label" style="width:10%;" for="instalment">Installment</label>
	  									<input type="text" class="form-control" onfocus="this.blur();" id="instalment" >
	  								</div>
	  								<div class="col-sm-2" style="padding:0px;"> 
	  									<label class="control-label col-sm-2" style="width:10%;" for="intrest_total">Intrest</label>
	  									<input type="text" class="form-control" id="intrest_total" onfocus="this.blur();">
	  								</div>
	  								<div class="col-sm-2" style="padding:0px;"> 
	  									<label class="control-label col-sm-2" style="width:10%;" for="principal"> Principal</label>
	  									<input type="text" class="form-control" id="principal" onfocus="this.blur();">
	  								</div>
	  								

	  								<div class="col-md-4">
		  								
		  								<div class="col-sm-6">
		  									<label class="control-label" style="text-align:left;" for="setteled_amt"> Settled Amount :</label>
		  									<input type="text" class="form-control" id="setteled_amt" name="setteled_amt" onfocus="this.blur();">
		  								</div>

										<div class="col-sm-6">
											<label class="control-label" style="text-align:left;" for="balance"> Balance :</label>
											<input type="text" class="form-control" name="balance" id="balance" onfocus="this.blur();">
										</div>
									</div>	  								
	  							
	  						</div>
	  					</div>
	  				</div>
	  				<center>
	  					<button class="btn btn-success" id="save_details" disabled="disabled">Save Details</button>
	  				</center>
	  			</div>
	  			
	  		</div>
	  </div>	
	</div>   
</div>
</div>
</section>

</div>

<?php include('application/views/common/footer.php');?>
<script type="text/javascript">

	$('#ltf_ref_no').change(function(e){
    		get_loan_details(this);
	});

	$("#ltf_ref_btn").click(function(){
		alert("hai")
		get_loan_details("#ltf_ref_no");
		$("#ltf_ref_no").blur();
	});

	$("#loan_amt_btn").click(function(){
		loan_amt = $("#loan_amt").val();
		intrest_rt = $("#intrest_p").val();
		
		profit_amt = loan_amt * (intrest_rt/100);
		$("#profit_amt").val(profit_amt);
	});

	function get_loan_details(selector){
		ltl_ref = $(selector).val();

	    jQuery.ajax({
	        type: "POST",
	        url: "<?php echo base_url(); ?>" + "Loan/get_loan_details",
	        dataType: 'html',
	        data: {ltl_ref: ltl_ref},
	        success: function(res) {
	        	res = JSON.parse(res)
	        	$("#bank_name").empty();
				if(res){
					$("#bank_name").append("<option>" + res[0].bank_name + "</option>");
					$("#intrest_p").val(res[0].anual_intrest_rt);
					$("#loan_amt").val(res[0].loan_amount);
					$("#description").val(res[0].loan_description);
					$("#loan_ac").val(res[0].loan_ac);
					$("#intrest_ac").val(res[0].intrest_ac);
					$("#loan_date").val(res[0].approved_date);
					$("#debit_ac").val(res[0].debit_ac);

					term = res[0].loan_term;
					loan_amt = $("#loan_amt").val();
					intrest_rt = $("#intrest_p").val();
					profit_amt = loan_amt * (intrest_rt/100) * (term/12);
					$("#profit_amt").val(profit_amt);

					intrest = profit_amt/term;
					principal = loan_amt/term;
					instalment = intrest + principal;
					$("#installment_amt").val(instalment);					
				}
	        }
	    });
	}

	$("#get_shedule").click(function(){
		ltl_ref = $("#ltf_ref_no").val();
		loan_amt = $("#loan_amt").val();
		intrest = $("#intrest_p").val();
		start_date = $("#start_date").val();

		if(ltl_ref == "" || loan_amt == "" || intrest == "" || start_date == ""){
			alert("LTL Reference , Loan amount and Intrest Persentage required");
			return;
		}
		var sendData = new FormData();
		sendData.append("ltl_ref", ltl_ref);
		sendData.append("loan_amount", loan_amt);
		sendData.append("intrest", intrest);
		sendData.append("start_date", start_date);

		    jQuery.ajax({
		        type: "POST",
		        url: "<?php echo base_url(); ?>" + "Loan/get_shedule",
		        dataType: 'json',
		        data: sendData,
		        contentType: false,
    			processData: false,
		        success: function(res) {
		        	$("#loan_shedule").empty();
		        	console.log(res)
		        	//res = JSON.parse(res);
		        	
		        	if(res["loan_shedule"] != null){
		        		console.log(res)
		        		res = res["loan_shedule"]
		        		intrest_total = 0
			        		principal = 0
			        		instalment = 0
		        		for(i=0;i<(res.length);i++){

			        		$("#loan_shedule").append("<tr>");
			        		$("#loan_shedule").append("<td>"+ (i+1) +"</td>");
			        		$("#loan_shedule").append("<td>"+ res[i].loan_date +"</td>");
			        		$("#loan_shedule").append("<td>"+ res[i].loan_amt +"</td>");
			        		$("#loan_shedule").append("<td>"+ res[i].loan_instalment +"</td>");
			        		$("#loan_shedule").append("<td>"+ res[i].loan_intrest +"</td>");
			        		$("#loan_shedule").append("<td>"+ res[i].loan_principal +"</td>");
			        		$("#loan_shedule").append("<td>"+ res[i].check_no +"</td>");
			        		$("#loan_shedule").append("<td>"+ res[i].vounher_no +"</td>");
			        		$("#loan_shedule").append("</tr>");
			        		
			        		intrest_total += Number(res[i].loan_intrest)
			        		principal += Number(res[i].loan_principal)
			        		instalment += Number(res[i].loan_instalment)
		        		}
		        	$("#intrest_total").val(intrest_total);
		        	$("#principal").val(principal);
		        	$("#instalment").val(instalment);	
		        	}
		        	else{
		        		loan_amount = res[0].loan_given;
		        	for(i=0;i<(res.length-1);i++){

		        		$("#loan_shedule").append("<tr>");
		        		$("#loan_shedule").append("<td>"+ (i+1) +"</td>");
		        		$("#loan_shedule").append("<td>"+ res[i].loan_date +"</td>");
		        		$("#loan_shedule").append("<td>"+ loan_amount +"</td>");
		        		$("#loan_shedule").append("<td>"+ (Number(res[i+1].intrest_received) + Number(res[i+1].loan_received))+"</td>");
		        		$("#loan_shedule").append("<td>"+ res[i+1].intrest_received +"</td>");
		        		$("#loan_shedule").append("<td>"+ res[i+1].loan_received +"</td>");
		        		$("#loan_shedule").append("<td></td>");
		        		$("#loan_shedule").append("<td></td>");
		        		$("#loan_shedule").append("</tr>");

		        		loan_amount = loan_amount - res[i+1].loan_received;
		        	}
		        	$("#intrest_total").val(res[0].intrest_given);
		        	$("#principal").val(res[0].loan_given);
		        	$("#instalment").val(Number(res[0].loan_given) + Number(res[0].intrest_given));
		        	}
		        	$("#save_details").prop('disabled', false);
		        }

		        
		    });
	});


	$("#save_details").click(function(){
		ltl_ref = $("#ltf_ref_no").val();
		location.href = "<?php echo base_url() ?>" + "/Loan/save_details/" + ltl_ref;
	});

	$("#loan_amt, #profit_amt, #intrest_p").on("keypress", function(e){
		if(!isNumber(e)){
			e.preventDefault();
		}
	});
	 $('.datepicker').click(function(){
	 	$(this).datepicker({
		format: 'dd-mm-yyyy',
	    autoclose: true,
	    todayHighlight: true
	});
	   });  
	  

///dropdown list  customer name
   $('#ltf_ref_no').selectize({
     create: false
     , sortField: {
     //field: 'text',
     direction: 'asc'
   }
   , dropdownParent: 'body'
   });
	   
	   
</script>