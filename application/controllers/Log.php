<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {

function __construct(){
parent::__construct();
$user = $this->session->userdata('userid');
if (!isset($user))
{ 
redirect('/');
} 
else
{ 
return true;
}
$this->load->helper(array('form', 'url'));
$this->load->model('Model_log');
}


public function view_log()
{
  $this->load->model('Model_log');
  //$data['result1']=$this->Model_customer->view_country();
  //$data['result2']=$this->Model_customer->view_area();
  $this->load->view('admin/view_log');
	
}

public function log_ajax()
{
	    $op=$this->uri->segment(3);
	    $this->load->Model('Model_log');
		if($op==1)  //normal
		{
		  $results=$this->Model_log->view_log($op,"");
		}
		else if($op==2)   //date wise
		{
		$d1=$this->uri->segment(4);
		$d2=explode("-",$d1);
		$dt=$d2[2]."/".$d2[1]."/".$d2[0];
        $results=$this->Model_log->view_log($op,$dt);
		}
		else if($op==3)  //month wise
		{
		$m=$this->uri->segment(4);
	    $results=$this->Model_log->view_log($op,$m);
		}
    
        $data1 = array();
        foreach($results  as $r) 
        {
             array_push($data1, array(
              "id"=>"$r->log_id",
              "staffid"=>"$r->log_staffid",	
              "name"=>"$r->log_staffname",
              "date"=>"$r->log_date",
              "time"=>"$r->log_time",
              "entry"=>"$r->log_entryid",
              "description"=>"$r->log_desc",
              ));
        }
        echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

public function share_log()
{
   $data=$this->session->all_userdata();
        $dt1=$this->input->post('dt1');
         $m=$this->input->post('m');
		 
  echo " <style> .tx{text-align:right;} .fnt{text-align:right;font-size:18px;}</style>            
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Share</font></h4>
    </div>
    <br>
  
<form  class='form-horizontal' role='form' method='post' action='". site_url('Pdf/share_log'). "' enctype='multipart/form-data'>

  <div class='row'>
    <div class='col-md-12'>
   
         <div class='form-group'>
          <label class='col-md-2 control-label' style='padding-top:10px;'>Email to </label>
          <div class='col-md-8'>
          <input type='email' class='form-control' name='email' value='' placeholder='Enter email'> 
           <input type='text' class='form-control' name='dt1' value='$dt1' placeholder='Enter email'> 
           <input type='text' class='form-control' name='m' value='$m' placeholder='Enter email'> 
          </div>
          </div>
    </div>
    </div> 
    <div class='modal-footer'>
      <input type='submit' class='btn btn-primary' value='Share'/>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
     </div>     
  
</form>   
    ";
}
}
  