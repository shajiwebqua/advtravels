<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

 <table width="100%" style='font-size:15px;'><tr><td><h4>Stock Items List </h4></td><td style='text-align:right;font-size:14px;'>Date: <?php echo date('d-m-Y');?></td></tr>
 <tr><td></td><td></td></tr>
 <tr><td></td><td></td></tr>
 </table>
  
 <style>
#example tr, #example td , #example th
{
	 border:1px solid #e4e4e4;
 }
 </style>
  
  
 		<table class="table table-striped table-hover table-bordered" id="example" width='100%' style='font-size:14px;' cellpadding=0 cellspacing=0>
		<thead>
			<tr style="color:#5068f8;">
			 <th width='70px'>Del</th>
			 <th width='100px'>Date</th>
			 <th >Category</th>
			 <th >Particulars</th>
			 <th width='100px'>Stock in Hand</th>
			 <th width='70px'>Units</th>
			 <th width='300px'>Description</th>
			 </tr>
		</thead>
		<tbody>
		<?php
		
		if(isset($stock))
		{
		foreach($stock as $r1)
		{
		?>
		<tr>
			 <td align='center'><?=$r1->item_sta_id;?></td>
			 <td><?=date_format(date_create($r1->item_sta_date),"d-m-Y");?></td>
			 <td><?=$r1->item_sta_category;?></td>
			 <td><?=$r1->item_sta_particulars;?></td>
			 <td><?=$r1->item_sta_total;?></td>
			 <td><?=$r1->item_sta_units;?></td>
			 <td><?=$r1->item_sta_desc;?></td>
			 </tr>
		<?php 
		} 
		}?>
		</tbody>
		<thead>

		</thead>
		
	</table>
</body>
</html>