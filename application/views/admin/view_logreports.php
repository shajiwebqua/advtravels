<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}
</style>

 <!-- Content Wrapper. Contains page content -->
 
<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Logs Reports</b></h1>
  <label style="background-color:#cecece;width:100%;height:1px;margin-top:0px;"></label>
 </section>
  
<!-- Content Header  end -->	  
	  
  <section class="content"> 
 <!--  <div style="color:blue;font-size:12px;"> Select Trip details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Print,Share,Save,View,Delete)  </div> -->
  <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>  
  <!-- Small boxes (Stat box) -->
   <div class="row">
   <div class="col-md-12">

	<!-- Add new user details-->
	
	 <div style="background-color:#fff;border-radius:10px;padding:15px; ">
					
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                  
                <div class="page-content">
<!--- search all ---------------------------------------- -->

	<form class="form-horizontal" role="form" method="POST" id="myformd" action="<?php echo base_url('Reports/pdf_alllogs_date')?>" >
	<div style='margin-top:20px;'>
 	 <label><b>To get Logs Reports (day wise)</b></label>
	</div>
	<label style="background-color:#cecece;width:100%;height:1px;"></label><br>
  			<div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
        	<label class="col-md-2 control-label" style="padding-top:3px;" >Select Date : </label>
						<div class="col-md-3">
						<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" readonly id="datepicker" value="<?php echo date('d-m-Y');?>">
						</div> 
						</div>
				<div class="col-md-2">
							<input class="form-control btn btn-primary" style="text-align:center;" type="submit" value="Get Reports" id="btnget">
						</div>
					</div>
				</div> 
			</div>
		</form>
		
		
		
		
<!-- search all ------------------------------------- -->
<form class="form-horizontal" role="form" method="POST" id="myformm" action="<?php echo base_url('Reports/pdf_alllogs_month')?>" >

<div style='margin-top:20px;'>
 	 <label><b>To get Logs Reports(month wise)</b></label>
</div>
<label style="background-color:#cecece;width:100%;height:1px;"></label><br>
			<div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                    	<br><label class="col-md-2 control-label" style="padding-top:3px;" >Select Month : </label>
						<div class="col-md-3">
						<select class="form-control" id="month" name="pmonth1">
					<option value="0">-----</option>
					<option value="1">January</option>
					<option value="2">February</option>
					<option value="3">March</option>
					<option value="4">April</option>
					<option value="5">May</option>
					<option value="6">June</option>
					<option value="7">July</option>
					<option value="8">August</option>
					<option value="9">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
					</select> 
						</div>
						
						
						<div class="col-md-2">
							<input class="form-control btn btn-primary" type="submit" value="Get Reports" id="btngetm">
						</div>
					</div>
				</div> 
			</div>
		</form>

		</div>       

				
	</div>
	
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}


	 $('#datepicker').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
//-------------------------------------
 $("#btnget").click(function(){
      var date=$('#datepicker').val();
	 var pdf_link = $('#myformd').attr('action')+'/'+date;  
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Log Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;

});



 $("#btngetm").click(function(){
         var month=$('#month').val();
         if(month=="0")
         {
  alert('Please Select Month');
         }
         else
         {
	 var pdf_link = $('#myformm').attr('action')+'/'+month;  
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Log Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;


} return false;
      

  });

    
  
</script>



