<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<title>Travels</title>
	<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" /> 
	<style type="text/css">
		
	table{
		font-size: 10px;
	}
		td{
			padding:7px 5px 7px 5px;

		}

		th{
			padding:7px 5px 7px 5px;
			font-size:16px;
			
		}

		.center {
    		margin: auto;
    		width: 25%;
		}
		.tbl tr
		{
			height:40px;
		}
		
.td_style
{
 text-align:right;
 padding-right:5px; 
 }
 table td
 {
	 font-size:16px;
 }
  p{ margin:0px;padding:0px;font-size:17px;font-weight:bold;}

  .tblresult td, .tbresult tr, .tbresult th{
	  border:1px solid #e4e4e4;
  }
  
	</style>
</head>
 <body>
  <table style="border:none;width:100%;">
		<tr>
			<td colspan=2 style="padding: 0px">
				<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" height='35px'>
			</td>
		</tr>
		<tr>
			<td style="padding:0px">
				<span style="font-size:15px;">Advance World Holidays</span><br>
				<span>PK & Sons Complex, East Moozhikkal</span><br>
				<span>NH 212, Calicut, Kerala, India - 673571</span><br>
				<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
				<span>Fax: 0091 495 2732 400</span>
			</td>
			<td width='150px' valign='top' align='right'>Date: <?php echo date('d-m-Y');?></td>
		</tr>
	</table>
	<hr style='margin-bottom:2px;'>
	
		 <div class='row' style='margin-bottom:15px;'>
		 <div class='col-md-12'>
		 <?php
		 $dcap=$msmonth. " -" .$msyear;
		 ?>
		 <h4><b>Summary Report of : &nbsp;&nbsp; <?=$dcap;?></b></h4>
		 </div>
		 </div>
 <hr style='margin-top:2px;'>
 
<table  style="width:100%;border:1px solid #e4e4e4;" cellpadding=5 cellspacing=0 class="tblresult table table-stripped" >
		   <thead>
			<tr>
			   <tr style='color:#4b88ed;border:1px solid #e4e4e4;font-size:16px;'>
			     <th width='10%'>Sl No</th>
				 <th>Particulars</th>
				 <th width='18%' style='text-align:right;'>Income</th>
				 <th width='18%' style='text-align:right;'>Expense</th>
				</tr>
			</tr>
		</thead>
		<tbody>
			    <tr>
					<td>1</td>
					<td>Vehicle Loan Paid</td>
					<td class="td_style">-</td>
					<td class="td_style"><?php $total_expense = 0;$total_income=0;
						$loan = number_format((float)$vehicle_loan->LOAN_EXPENSE,2,'.','');
						echo $loan;
						$total_expense = $total_expense + $loan;?></td>
				</tr>
				<tr>
					
					<td>2</td>
					<td>Trip Collections </td>
              	    <td class="td_style"><?php
					 $total_income=number_format($trip_total->TRIP_AMOUNT,2,'.','');
					 echo $total_income;
					 ?>
					 </td>
          		    <td class="td_style">0.00</td>
				</tr>
				<!--<tr>
					<td>3</td>
					<td>Vehicle Service Charges</td>
					<td class="td_style">-</td>
					<td class="td_style"><?php //$service = number_format((float)$vehicle_service->SERVICE_EXPENSE,2,'.','');
						//echo $service;
						//$total_expense = $total_expense + $service;?></td>
				</tr> -->
				
			    <tr>
					<td>3</td>
					<td>General Expense </td>
					<td class="td_style">-</td>
					<td class="td_style"><?php $general = number_format((float)$general_expense->GEN_EXPENSE,2,'.','');
						echo $general;
						$total_expense = $total_expense + $general;?></td>
				</tr>
				<tr>
					<td>4</td>
					<td>Vehicle Expense </td>
					<td class="td_style">-</td>
					<td class="td_style"><?php $vehicle = number_format((float)$vehicle_expense->EXPENSE,2,'.','');
						echo $vehicle;
						$total_expense = $total_expense + $vehicle;?></td>
				</tr>
				<tr>
					<td>5</td>
					<td>Salary Advance</td>
					<td class="td_style">-</td>
					<td class="td_style"><?php $advance = number_format((float)$advance->ADVANCE,2,'.','');
						echo $advance;
						$total_expense = $total_expense + $advance;?></td>
				</tr>
				<tr>
					<td>6</td>
					<td>Staff Salary</td>
					<td class="td_style">-</td>
					<td class="td_style"><?php $salary = number_format((float)$salary->SALARY,2,'.','');
						echo $salary;
						$total_expense = $total_expense + $salary;?></td>
				</tr>
				 
			    <tr>
			    	<td></td>
			    	<td style="font-size:18px;font-weight:bold;"><b>Total<b></td>
			    	<td class="td_style" style="font-size:18px;font-weight:bold;"><b><?= number_format($total_income,2,'.','');?></b></td>
			    	<td class="td_style" style="font-size:18px;font-weight:bold;"><b><?= number_format($total_expense,2,'.','');?></b></td>
			    </tr>
			    
				<?php
				
				$lp=$total_income-$total_expense;
				if($lp<0)
				{
					$lptext="Net Loss  :"; 
					$lpamt=number_format($lp,2,'.','');
				}
				else
				{
					$lptext="Net Profit:"; 
					$lpamt=number_format($lp,2,'.','');
				}
				?>
						
				<tr> <td colspan="4" height='20px'></td> </tr>
				<tr>
			    	<td colspan="3" class="td_style" valign='top'><p>Total Income :</p><p>Total Expense :</p><p>&nbsp;</p><p>Net Amount :</p></td>
			    	<td class="td_style" ><p><?=number_format($total_income,2,'.','');?></p>
					<p><?=number_format($total_expense,2,'.','');?></p>
					<p>------------------</p>
					<p><?=number_format($lp,2,'.','');?></p>
					<p>------------------</p></td>
			    </tr>
				<tr height='50px'>
			    	<td colspan="4" style='font-size:20px;text-align:right;font-weight:bold;'><span style='padding-right:20px;'><b><?=$lptext;?></span> &nbsp;&nbsp;&nbsp;<?=$lpamt;?></b><br>===================</td>
			    	
			    </tr>

		</tbody>
		</table>
        </tbody>
    </table> 
 
</body>
</html>
