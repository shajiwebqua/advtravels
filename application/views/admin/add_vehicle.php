<?php
 include('application/views/common/header.php');
 ?>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>New Vehicle Entry</b></h1>
	
    <ol class="breadcrumb" style='font-size:15px;'>
	<!--<li><a href="<?php echo base_url('Vehicle/index');?>" style='color:#4b88ed;'><i class="fa fa-backward" aria-hidden="true"></i>Back to List</a></li> -->
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 	  
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
	<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div>
				 <div id='msgerr'><?php echo $this->session->flashdata('message1'); ?></div>
				 </center>
				 </div>
			</div>
	
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details -->
	
<div style="background-color:#fff;padding:15px; ">
			
<div class="page-content-wrapper">
 	
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
			
        <div class="row">
        <div class="col-md-5">
		
		<label style="padding-top:5px;padding-bottom:3px; color:blue; border-bottom:1px solid #00adee;margin:0px; width:100%;">Add Vehicle </label>
		 
			<div class='row' style="margin-top:15px;">
					<form  class='form-horizontal' role='form' method='post' action="<?php echo site_url('vehicle/add_new_vehicle');?>"' enctype='multipart/form-data'>
                    <!-- text input -->
          
							<!--  <input list='vendors' name='vendor' /></label>
								<datalist id='vendors'>
									  <option value='Shaji'>
									  <option value='Renny'>
									  <option value='Shinil'>
									  <option value='Raju'>
									  <option value='Suresh'>
									  <option value='Subheesh'>
								</datalist>  -->
                            
					 <div class='form-group' >
						   <label class='col-md-4 control-label' style='padding-top:5px;'>Vehicle Reg. No </label>
						   <div class='col-md-7'>  
								<input type='text'  name='regno' class='form-control id11' required  />
						   </div>                   
					  </div>

					  <div class='form-group'>
								 <label class='col-md-4 control-label' style='padding-top:5px;'>Vehicle Type </label>
								<div class='col-md-7'>  
								
								<select name='vtype' class='form-control id11' required >
								 <option value="">----</option>
							 <?php
									$section = $this->db->select('*')->from('vehicle_types')->get();
									$sectn1=$section->result();
									foreach ($sectn1 as $ro):
									   echo "<option value='$ro->veh_type_id'>".$ro->veh_type_name."</option>";
									endforeach;
									?>
							   echo "                     
									</select>
						 </div>                   
					</div>
					
			  
					  <div class='form-group'>
					  <label class='col-md-4  control-label' style='padding-top:5px;'>No Of Seats </label>
					  <div class='col-md-3 col-sm-12'>
					  <input type='number' class='form-control id11' placeholder='' name='seats' required>
						</div>
					  </div>
					  					  
					  <div class='form-group'>
							<label class='col-md-4 control-label' style='padding-top:5px;'>Vehicle Price</label>
							<div class='col-md-5 col-sm-12'> 
								<input type='text'  name='vamount' class='form-control id11' required  />
							</div>                   
					  </div>
					  
					<hr>					  
					
					<div class='form-group'>
							<div class='col-md-12'> 
								<center><input type='submit'  name='vsave' class='btn btn-primary' value ="Add Vehicle" /></center>
							</div>                   
					  </div>
				  
				  
				  
				  
					  </form>
					  </div>
	  
			</div>
			<div class="col-md-7">
			<div class='row' style='border-bottom:1px solid #00adee;'>
			<label class='col-md-8' style='text-align:left'>Vehicle List</label>
			<label class='col-md-4' style='text-align:right;'><a href="<?php echo base_url('Vehicle/index');?> "><button class='btn btn-warning btn-xs'>Vehicle List</button></a></label>
			</div>
	
				<div style="padding-top:10px;">
						<table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
						 	<tr>
							  <th></th>
							  <th>ID</th>
							  <th>Date</th>
							  <th>Reg No</th>
							  <th>Modal</th>
							  <th>Seats</th>
							  <th>Price</th>
							  <th>Status</th>
							</tr>
						</thead>
                        </table>
				
				</div>
			
			</div>
		<!-- tab pages  end---------------------------------- -->	
			
		</div>
		</div>

        </div>
		
  
					<div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Edit</h4>
								</div>
							</div>
							</div>
						<!-- /.modal-dialog -->
						</div>
                   </div>	

                    <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>       						
				
	</div>
	<!-- End user details -->
	</div>
</section>
</div>

<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

$("#idmsg").hide();
$("#msgerr").hide();

//sweet alert ----------------------------

	if($("#idh5").html()!="")
	{
		swal("Saved",$("#idh5").html(),"success")
		$("#idh5").html("");
	}
	if($("#msgerr").html()!="")
	{
		swal("Try Again",$("#msgerr").html(),"error")
		$("#idh5").html("");
	}
	
// sweet alert box -------------------------- 

$('#example').DataTable({
		"ordering":false,
        "destroy": true,
        "processing": true,
        
        "ajax": {
                url :"<?php echo base_url(); ?>" + "vehicle/vehicle_ajax",// json datasource
                },
			   "columnDefs":[
					{"width":"5%","targets":1},
					{"width":"7%","targets":2},
				],
			   
        "columns": [
            { "data": "action"},
            { "data": "vid" },
			{ "data": "date" },
			{ "data": "regno" },
			{ "data": "vtype" },
			{ "data": "seats" },
			{ "data": "vamt" },
			{ "data": "status" },
      ]	
	
});


  $(document).ready(function(){
	   //$('.id11').prop('disabled',true);
		//$('#saveD').prop('disabled',true);
		//$('#addbtn').prop('disabled',true);
  });

	var lid=$('#alid').val();
 
  

$('#newbtn').click(function()
{
	 $('.id11').prop('disabled',false);
	 $('#saveD').prop('disabled',false);
});
  
$('#saveD').click(function()
{
	 $('#addbtn').prop('disabled',false);
});  


      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
           var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "vehicle/edit_vehicle/2",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 

		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

 //});
  


</script>



