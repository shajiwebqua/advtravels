<?php
include('application/views/common/header.php');
?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1><b> Office Stationary Pruchase Request </b></h1>
		<ol class="breadcrumb" style='font-size:15px;'>
			<li><a href="<?php echo base_url('Services/request_list');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Service Request List</button></a></li>
			
	    </ol> 
         <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
  
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
<div class="row">
<div class="col-md-12">

<div style="background-color:#fff;border-radius:2px;padding:15px; ">

<!--<div class="portlet-title">
		 <div class="caption" >
				<h4 style='margin-top:0px;'><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Parts Details</h4>
		 </div>
</div> -->
<!--<hr style="background-color:#cecece,border:1px;width:100%;top:10px;margin:0px;padding:3px 0px 10px 0px;">-->
             
 <div class="portlet-body form">
 <div class='row'>
<div class="col-md-12">
<form enctype="multipart/form-data" method="post" id="addpurchaserequest" name="addpurchaserequest" action="">
        <!--<form>-->
		<div class='row'>
		<label style='margin-left:15px;'><b>Add Items here</b></label>
		</div>
		<div class='row' style='margin-bottom:15px;'>
		<hr style='margin:0px;'>
		</div>
		
		 <div class="row">
        <div class="col-md-4">
        <label class="control-label">Particulars</label>
        </div>
		
		<div class="col-md-2">
        <label class="control-label">Quantity</label>
        </div>
		
        <div class="col-md-2">
        <label class="control-label">Approx.Price</label>
        </div>
        <div class="col-md-1"></div>
        
        </div>
		
		
        <div class="row">
        <div class="col-md-4">
		
		 <div class="input-group">
                <select class="form-control" name='parts' id='parts'>
				<option value="">--------</option>
				<option value="PEN">PEN</option>
				<option value="BOOKS">BOOKS</option>
				<option value="A4 SHEETS">A4 SHEETS</option>
				</select>
				<span class="input-group-addon" style='padding:2px;margin:0px;'><button class='btn btn-primary btn-xs' ><i class="fa fa-plus" aria-hidden="true"></i></button></span>
            </div>
	     </div>
		
		<div class="col-md-2">
        <input type="text" id="qty" name="qty" placeholder="Quantity" class="form-control">
        </div>
		
        <div class="col-md-2" >
        <input type="text" id="price" placeholder="Price" class="form-control">
        </div>
        <div class="col-md-1" style='margin-bottom:15px;'>
        <input type="button" class="add-row btn btn-default btn-sm" value="Add Row" >
        </div>
		</div>
		
		<div class='row'>
		<label style='margin-left:15px;'><b>Requested Items</b></label>
		</div>
		
<!--
<div class='col-md-9' style='border-left:solid 1px red;' >
<style>
#data_table1 td, th 
{
	padding:3px;
}

#data_table2 td, th
{
	padding:3px;
}
</style>
<table align='center' cellspacing=2 cellpadding=5 id="data_table1" border=1 width='100%'>
<tr>
<th width='5%'>To</th>
<th width='25%'>Particulars</th>
<th>qty</th>
<th>Price</th>
<th></th>
</tr>

<tr>
<td ><input type="text" id="pslno" size="5" value='1'  readonly></td>
<td >
<select id="parts"  style='width:300px;padding:2px;'>
<option value=''>----------</option>
</select>
</td>
<td ><input type="text" id="qty" size="10" style='text-align:right;'></td>
<td ><input type="text" id="price" size="10" style='text-align:right;'></td>
<td ><button type="button" class="add"  style='color:green;' onclick="add_row();" > <i class="fa fa-save" aria-hidden="true"></button></td>
</tr>
</table>
</div>
		-->	

		
		
	<div class='row'>
	<div class='col-md-9'>
        <table id="example" class="table table-striped table-bordered display" cellspacing="0" width="100%">
        <thead>

                <th width="5%">No</th>
                <th width="40%">Parts</th>
				<th width="8%">Qty</th>
                <th width="8%">Price</th>
				<th width="3%" style='padding:0px;margin-right:0px;'></th>
                <!--<td width="10%">Action</td>-->

        </thead>
         <tbody class='ptbody'>
           
        </tbody>
	</table> 
	<table cellspacing="0" width="100%" >
      	<th style='text-align:right;font-size:17px;'> Total</th> <th id='total' style='text-align:right;width:150px;font-size:17px;'>0.00</th><th  width='7%'>&nbsp;</th>
	 </table> 
	
	
	</div>
	<div class='row col-md-9' style='margin-top:20px;text-align:right;'>
	<button type="submit" class="add btn btn-primary" >Add</button>
	    <button type="submit" id='tsubmit' class="btn btn-primary" style='padding:5px 20px 5px 15px;' > <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
		</div>
    </div>
  </form>
	</div> <!--portlet-body-->
 
	</div>    
	</div>  
	</div>
	</div>
  </div>
  </div>
</section>

<?php include('application/views/common/footer.php');?>
  </div>

  <script type="text/javascript">
/*$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
*/
  
  $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
 
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
	//endDate:'now'
   }); 
    
	
var i=1;
$(".add-row").click(function(){
    var parts = $("#parts").val();
	var qty = $("#qty").val();
    var price = $("#price").val();
    $("#parts").val('');
	$("#qty").val('');
    $("#price").val('');
    var markup = "<tr id='"+i+"'><td>"+i+"</td><td>" + parts + "</td>" +
	"<td>" + qty + "</td><td class='price' style='text-align:right;'>" + parseFloat(price).toFixed(2)+ 
	"</td><td style='text-align:center;'><button id='row_remove' style='padding:0px 5px 0px 5px;color:#ff5722;'>"+
	"<i class='fa fa-trash' aria-hidden='true'></i></button></td><tr>";
	
    $("table .ptbody").append(markup);
    i++;
	var tot=0;
	
  	  $('#example').find("td.price").each(function(index){
	   var myData = $(this).html();
	   tot=tot+parseFloat(myData);
	  });
	  //alert(tot);
	  $("#total").html(parseFloat(tot).toFixed(2));
	
});

$(".add").click(function()
{
	$("#example tr:gt(0)").each(function (row,tr) {
        var this_row = $(this);
        var p1 = $(tr).find('td:eq(0)').html();//td:eq(0) means first td of this row
        var p2 = $.trim(this_row.find('td:eq(1)').html());
        var p3 = $.trim(this_row.find('td:eq(2)').html());
		var p4 = $.trim(this_row.find('td:eq(3)').html());
		if(p1!="" && p2!="" && p3!="" && p4!="")
		{			
		alert(p1);
			alert(p2);
			alert(p3);
			alert(p4);
		}	
    });
});
  
function storeTblValues()
{
    var TableData = new Array();

    /*$('#example tr').each(function(row, tr){
        TableData[row]={
            "No" : $(tr).find('td:eq(0)').text()
            , "Parts" :$(tr).find('td:eq(1)').text()
            , "Price" : $(tr).find('td:eq(2)').text()
        }    
    }); 
   TableData.shift();  // first row will be empty - so remove
    return TableData;*/
	var str="";
	$("#example tr:gt(0)").each(function (row,tr) {
		
		  var p1=$(tr).find('td:eq(0)').html();//td:eq(0) means first td of this row
          var p2=$(tr).find('td:eq(1)').html();
          var p3=$(tr).find('td:eq(2)').html();
		  var p4=$(tr).find('td:eq(3)').html();
		  if(p1!=void(0))
		  {
		  str+=p1+"|"+p2+"|"+p3+"|"+p4+"#";
		  }
		});
		return str;
}

$(document).on('click','#row_remove',function(event) {
    $(this).parent().parent('tr').remove();
	var tot=0;
  	  $('#example').find("td.price").each(function(index){
	   var myData = $(this).html();
	   tot=tot+parseFloat(myData);
	  });
	  if(tot<=0)
	  {i=1;}
	  $("#total").html(parseFloat(tot).toFixed(2));
});


$(document).on("click", "#tsubmit", function (event) {
          
    event.preventDefault();
    var TData =storeTblValues();
    $.ajax({
		url: "<?php echo base_url();?>Stationary/add_purchase_request",
		type: 'POST',
		data: {pTableData:TData},
		success: function (data, status) {
			alert(data);
		  if(data=="success")
		  {
			  swal({
				  title: "Saved",
				  text: "Service parts request details added.!",
				  type: "success",
				  showCancelButton: false,
				  confirmButtonClass: "btn-success",
				  confirmButtonText: " Ok ",
				  closeOnConfirm: false
				},
				function(){
				  window.location.reload(); 
				});
		  }
		  else
		  {
			swal("Error", "Something went wrong", "error");    
		  }  
		}
	  });
});
  
  
/*$("#btnexp1").click(function()
{
	var etype1=$("#exptype1").val();
	var egroup=$("#expgroup").val();
	jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Vehicle/add_exp_type",
        dataType: 'html',
        data: {exptype:etype1,expgroup:egroup},
        success: function(res) {
		$("#etype").append("<option value='"+res+"'>"+etype1+"</option>")
                    }
            });
});
*/

</script>


  </body>
</html>
