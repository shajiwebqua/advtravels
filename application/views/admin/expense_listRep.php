<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
 <!-- Main content -->
 <!-- Content Header (Page header) -->

  <section class="content-header">
    <h1><b>Vehicle Expenses Report</b> </h1>
    <ol class="breadcrumb" style='font-size:14px;'>
	 <li> <a href="<?php echo base_url('Vehicle/vehicle_expense');?>" style="color:#fff;"><button class='btn btn-primary'> <i class="fa fa-cab" aria-hidden="true"></i> Add New Expenses</a></button></li>
	 <li> <a href="<?php echo base_url('Vehicle/Expenses');?>" style="color:#4b88ed;"><button class='btn btn-info'><i class="fa fa-list" aria-hidden="true"></i> Expenses List</button> </a></li>
    </ol> 
  </section>
  
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 	  
  <section class="content"> 
<!--   <div style="color:blue;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Print,Share,Save,View,Edit,Delate)  </div> -->
			<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
   
    <!-- Small boxes (Stat box) -->
    <div class="row">
	
 <div class='col-md-7' style='padding-right:0px;'>
  <div style="background-color:#fff;padding:20px;">
  
  <div class='row'>
  	<label class='col-md-4 control-form'><b>Current Month</b></label>
	<div class='col-md-6' style='padding-left:20px;'>
	 <form method='POST' action='<?php echo base_url('Reports/expense_report/0');?>' id='myform1'>
		<input type='submit' class='btn btn-primary' name='allsubmit' value='Submit' id='allsubmit'>
	</form>
	</div>
  </div>
<label style="background-color:#000;width:100%;height:1px;margin-top:15px;margin-bottom:10px;"></label>
  <label class='control-form'><b>Date Wise Report</b></label><br>
  <div style='width:100%;background-color:#e4e4e4;padding:5px;'>
  
  <form onsubmit='return checkdata1();'  method='POST' id='myform2' action='<?php echo base_url('Reports/expense_report/1');?>'>
	  <div class='row'>
		  <label class='col-md-4 control-form' style='text-align:right;'>From Date</label>
		  <div class='col-md-6'>
		  <input class=' form-control' data-provide="datepicker" id="datepicker1" value="<?php echo date('d-m-Y');?>" name='fdate'>
		  </div>
	  </div>
	  
	  <div class='row'>
		  <label class='col-md-4 control-form' style='text-align:right;'>To Date</label>
		  <div class='col-md-6'>
		  <input class='form-control' data-provide="datepicker" id="datepicker2" value="<?php echo date('d-m-Y');?>" name='tdate'>
		  </div>
	  </div>
  
	  <div class='row'>
	      <label class='col-md-4 control-form' style='text-align:right;'>Vehicle</label>
		  <div class='col-md-6'>
		  <select name='vehicleid1' class='form-control' id='vehicleid1'>
		  <option value=''> ------------</option>
		  <?php
		  $vresult1=$this->db->select('*')->from('vehicle_registration')->get()->result();
		  foreach($vresult1 as $vr1)
		  {
		  ?>
		  <option value='<?=$vr1->vehicle_id;?>'><?=$vr1->vehicle_regno;?></option>
		  <?php } ?>
		  </select>
	  </div>
	  </div>
	   
	  <div class='row'>
			<label class='col-md-4 control-form'></label>
			<div class='col-md-6'>
			<input type='submit' class='btn btn-primary' style='margin-top:3px;' name='submit1' id='submit1'>
			</div>
	  </div>
</form> 
  </div>
   <label style="background-color:#000;width:100%;height:1px;margin-top:15px;margin-bottom:10px;"></label>
   <label class='control-form'><b>Month & Year Wise Report</b></label><br>
   
  <div style='width:100%;background-color:#e4e4e4;padding:5px;'>
  
  <form onsubmit='return checkdata2();' method='POST' id='myform3' action='<?php echo base_url('Reports/expense_report/2');?>'>
  <div class='row'>
  <label class='col-md-4 control-form' style='text-align:right;margin-top:5px;'>Select Month</label>
  <div class='col-md-6'>
	  <select name='mon' class='form-control' id='mon'>
	  <option value=''> ------------</option>
	  <option value='1'> January</option>
	  <option value='2'> February</option>
	  <option value='3'> March</option>
	  <option value='4'> April</option>
	  <option value='5'> May</option>
	  <option value='6'> June</option>
	  <option value='7'> July</option>
	  <option value='8'> August</option>
	  <option value='9'> September</option>
	  <option value='10'> October</option>
	  <option value='11'> Novenber</option>
	  <option value='12'> December</option>
	  </select>
  </div>
  </div>
  
  <div class='row'>
	  <label class='col-md-4 control-form' style='text-align:right;margin-top:5px;'>Year</label>
	  <div class='col-md-3'>
	 <select name='year' class='form-control' id='year'>
		  <option value=''> ------------</option>
		  <?php
		  for($x=date('Y');$x>=2016;$x--)
		  {
		  ?>
		  <option value='<?=$x;?>'><?=$x;?></option>
		  <?php } ?>
		  </select>
	  </div>
  </div>
  <div class='row'>
	   <label class='col-md-4 control-form'></label>
	  <div class='col-md-6'>
	  <input type='submit' class='btn btn-primary' style='margin-top:3px;' name='submit2' id='submit2'>
	  </div>
	  </div>
	  </form>
	  </div>
  </div>
  </div>
   
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 
 $('#example').DataTable({
	 "ordering":false,
 });

//sweet alert ----------------------------
	$("#idmsg").hide();
	if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
// sweet alert box -------------------------- 
 
 $('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});

$('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});

function checkdata1()
{
	var dt1=$('#datepicker1').val();
	var dt2=$('#datepicker2').val();
	var vid=$('#vehicleid1').val();
	
	if(dt1=="" || dt2=="" || vid=="")
	{
		alert("Select date and vehicle.");
		return false;
	}
	else
	{
	return true;
	}
}

function checkdata2()
{
	var mon=$('#mon').val();
	var vid=$('#vehicleid2').val();
	
	if(mon=="" || vid=="")
	{
		alert("Select month and vehicle.");
		return false;
	}
	else
	{
	return true;
	}
}
			   

 
    
$("#allsubmit").click(function(){
      	  
	 var pdf_link = $('#myform1').attr('action'); 
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><div id="overlay"><div id="loading"></div><iframe  id="myDiv" class="animate-bottom" src="'+pdf_link+'"></iframe></div></div>';
        //alert(iframe);
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;
  });

$("#submit1").click(function(){
    
	var dt1=$('#datepicker1').val();
	var dt2=$('#datepicker2').val();
	var vid=$('#vehicleid1').val();
	
	 var pdf_link = $('#myform2').attr('action'); 
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><div id="overlay"><div id="loading"></div><iframe id="myDiv" class="animate-bottom" src="'+pdf_link+'/'+vid+'/'+dt1+'/'+dt2+'"></iframe></div></div>';
        //alert(iframe);
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;
  });

$("#submit2").click(function(){
    
	
	var mon=$('#mon').val();
	var yr=$('#year').val();
		  
	 var pdf_link = $('#myform3').attr('action');

        var iframe = '<div class="iframe-container"><div id="overlay"><div id="loading"></div><iframe id="myDiv" class="animate-bottom" src="'+pdf_link+'/'+yr+'/'+mon+'"></iframe></div></div>';
        //alert(iframe);
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;
  });
  
 
 
  
  

</script>



