<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller 
{
     public function index()
       {
		   $acper=$this->session->userdata('accpermission');
		   if($acper=="1")
		   {
           $this->load->view('account/starter');
		   }
		   else
		   {
			   $this->session->set_flashdata('mesacc', 'You have no permission.!'); 
			   redirect('Login/home');
		   }
       }  
	 
	 
	public function edit_company()
	{
		$this->load->model("Model_account");
		$data['result']=$this->Model_account->select_all('acc_company');
		$this->load->view('account/edit_company',$data);
	}
	
	
	public function ledger()
	{
		$this->load->view('account/add_ledger');
	}

	
	public function Groups()
	{
		$this->load->model("Model_account");
		$data['result']=$this->Model_account->select_all('acc_groups');
		$this->load->view('account/add_groups',$data);
	}

	
	public function voucher_type()
	{
		$this->load->view('account/add_voucher_types');
	}
		
	public function payment()
	{   
		$data['tabstatus']='1';
		$this->session->set_flashdata('ptitle', 'Driver Payment Details.');
		$this->load->view('account/payments',$data);
	}
	
	
	public function payments()
	{
		$sdate=$this->input->post('startdate');
		$edate=$this->input->post('enddate');
		
		$dbtn=$this->input->post('drbtn');
		$sbtn=$this->input->post('stbtn');
		$obtn=$this->input->post('otbtn');
		
		//echo "driver".$dbtn;
		//echo "staff".$sbtn;
		//echo "others".$obtn;
		$data=array();
		
		$d1=explode("-",$sdate);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$edate);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
				
		$this->load->model("Model_account");
		if(isset($dbtn))
		{
		$data['results']=$this->Model_account->select_payments('1',$dt1,$dt2);
		$data['tabstatus']='2';
		$data['namestatus']='1';
		$this->session->set_flashdata('ptitle', 'Driver Payment Details.');
		}
		
		else if(isset($sbtn))
		{
		$data['results']=$this->Model_account->select_payments('2',$dt1,$dt2);
		$data['tabstatus']='2';
		$data['namestatus']='2';
		$this->session->set_flashdata('ptitle', 'Staff Payment Details.');
		}
		
		else if(isset($obtn))
		{
		$data['results']=$this->Model_account->select_payments('3',$dt1,$dt2);
		$data['tabstatus']='2';
		$data['namestatus']='3';
		$this->session->set_flashdata('ptitle', 'Others Payment Details.');
		}
		
		$this->load->view('account/payments',$data);
	}
		
	public function lpayments()
	{
		$data['tabstatus']='2';
		$this->session->set_flashdata('ptitle', 'Driver Payment Details.');
		$this->load->view('account/payments',$data);
	}
		
	public function receipt()
	{	
		$this->load->model("Model_account");
		$data['tabstatus']='1';
		$data['results']=$this->Model_account->corporate_receipts();
		$this->session->set_flashdata('ptitle', "RECEIPTS : Month of - ".date('F'));
		$this->load->view('account/corporate_receipts',$data);
	}
		
	public function receipts()
	{	
		$this->load->model("Model_account");
		$data['tabstatus']='2';
		$data['results']=$this->Model_account->corporate_receipts();
		$this->session->set_flashdata('ptitle', "RECEIPTS : Month of - ".date('F'));
		$this->load->view('account/corporate_receipts',$data);
	}
		
	public function DAccount()
	{   
		$data['tabstatus']='1';
		$this->session->set_flashdata('ptitle', 'Driver Payment Details.');
		$this->load->view('account/driver_account',$data);
	}
		
	public function SAccount()
	{   
		
		$this->session->set_flashdata('ptitle', 'Staff Payment Details.');
		$this->load->view('account/staff_account');
	}
	
	public function revenue()
	{   
		$this->load->model("Model_account");
		$data['tabstatus']='1';
		//$data['results']=$this->Model_account->get_revenue_list();
		$this->session->set_flashdata('ptitle', 'Invetment Details.');
		$this->load->view('account/revenue',$data);
	}
	
	public function vgroup()
	{   
		
		$this->session->set_flashdata('ptitle', 'Group wise Payment Details.');
		$this->load->view('account/voucher_group_account');
	}
	
		
	public function journal()
	{
		$this->load->view('account/journal');
	}
	
	
	public function contra()
	{
		$this->load->view('account/contra');
	}
	
	
	public function update_company()
	{
		$cname=$this->input->post('cmpname');
		$cadd=$this->input->post('cmpadd');
		$cctry=$this->input->post('cmpcountry');
		$cphone=$this->input->post('cmpphone');
		$cmob=$this->input->post('cmpmobile');
		$cemail=$this->input->post('cmpemail');
		$cweb=$this->input->post('cmpweb');
		
		$cdt=array("acc_comp_name"=>strtoupper($cname),
		"acc_comp_address"=>$cadd,
		"acc_comp_country"=>$cctry,
		"acc_comp_phone"=>$cphone,
		"acc_comp_mobile"=>$cmob,
		"acc_comp_email"=>$cemail,
		"acc_comp_website"=>$cweb,
		);
				
		$this->load->model("Model_account");
		$data['result']=$this->Model_account->update('acc_company',$cdt,array("acc_comp_id"=>"1"));
		
		$this->session->set_flashdata('message', '2#Company Details Updated.'); 
        redirect('Account/edit_company');
	}

	
public function add_group()
	{
		$gname=$this->input->post('grpname');
		$gunder=$this->input->post('grpunder');
		
		$gdt=array("acc_grp_name"=>strtoupper($gname),
		"acc_grp_under"=>$gunder,
		);
				
		$this->load->model("Model_account");
		$data['result']=$this->Model_account->insert('acc_groups',$gdt);
		
		$this->session->set_flashdata('message', '1#Group Successfully added.'); 
        redirect('Account/Groups');
	}

	
public function add_ledger()
{
  	   	$lname=$this->input->post('lname');
		$lunder=$this->input->post('grpunder');
		$lmname=$this->input->post('mname');
		$ladd=$this->input->post('address');
		$lopbal=$this->input->post('opbal');
		
		
		$cdt=array("acc_ledg_lname"=>strtoupper($lname),
		"acc_ledg_under"=>$lunder,
		"acc_ledg_name"=>$lmname,
		"acc_ledg_address"=>$ladd,
		"acc_ledg_opbalance"=>$lopbal,
		);
				
		$this->load->model("Model_account");
		$this->Model_account->insert('acc_ledgers',$cdt);
		
		$this->session->set_flashdata('message', '1#Ledger details added.'); 
        redirect('Account/ledger');
	
}
	
	
public function edit_ledger()
{
        $id=$this->input->post('lid');
        $query = $this->db->select('*')->from('acc_ledgers')->where('acc_ledg_id',$id)->get();
        $row = $query->row();
 
 echo "       
     <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>         
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Ledger</font></h4>
    </div>
    <br>

           <form  class='form-horizontal' role='form' method='post' action='". site_url('Account/update_ledger'). "' enctype='multipart/form-data' onsubmit='return checkdata();'>
                    <!-- text input -->
                    
          <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-3 control-label'>Ledger Name :</label>
		  <div class='col-md-7'> 
          <input type='hidden' name='ledgerid'  value='$row->acc_ledg_id'/>
          <input type='text' name='lname' class='form-control' value='$row->acc_ledg_lname' required/>
          </div>                   
          </div>
		  </div>
		  
		  
   
						<div class='form-group'>
						   <div class='row'>
							  <label class='col-md-3 control-label'>Under :</label>
								<div class='col-md-7'>
									<select name='grpunder' class='form-control' required>
									<option value=''>----------</option>";
									
									$agrps=$this->db->select('*')->from('acc_groups')->get()->result();
									foreach($agrps as $ag)
									{
									echo "<option value='".$ag->acc_grp_name,"'"; if($row->acc_ledg_under==$ag->acc_grp_name) echo 'selected'; echo " >".$ag->acc_grp_name."</option>";
									}
									
								echo "</select>
								</div>
							</div>
						</div>	
  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-3 control-label'>Name :</label>
		  <div class='col-md-7'> 
             <input type='text' name='mname' class='form-control' value='$row->acc_ledg_name' />
          </div>                   
          </div>
		  </div>
		  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-3 control-label'>Address :</label>
		  <div class='col-md-7'> 
          <textarea rows=2 name='address' class='form-control'>$row->acc_ledg_address</textarea>
          </div>                   
          </div>
		  </div>
		  
		  <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-3 control-label'>Opening Balance :</label>
		  <div class='col-md-4'> 
          <input type='text' name='opbal' class='form-control' value='$row->acc_ledg_opbalance' />
          </div>                   
          </div>
		  </div>
  <hr style='margin:5px;'>
  
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-7'>
                <input type='submit' class='btn btn-primary' value='Update'/>
				<button type='button' class='btn btn-default' data-dismiss='modal' style='margin-left:20px;'>Close</button>
                </div>
                </div>                                
				<br>
           </form>
<!--		   <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div>  -->
";

}

public function edit_vouchertypes()
{
        $id=$this->input->post('vid');
        $query = $this->db->select('*')->from('acc_voucher_types')->where('acc_vou_id',$id)->get();
        $row = $query->row();
 
 echo "       
     <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>         
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Ledger</font></h4>
    </div>
    <br>

           <form  class='form-horizontal' role='form' method='post' action='". site_url('Account/update_ledger'). "' enctype='multipart/form-data' onsubmit='return checkdata();'>
                    <!-- text input -->
                    
          <div class='form-group'>
		  <div class='row'>
		  <label class='col-md-3 control-label'>Ledger Name :</label>
		  <div class='col-md-7'> 
          <input type='hidden' name='voucherid'  value='$row->acc_vou_id'/>
          <input type='text' name='lname' class='form-control' value='$row->acc_vou_name' required/>
          </div>                   
          </div>
		  </div>
		  
		  
   
						<div class='form-group'>
						   <div class='row'>
							  <label class='col-md-3 control-label'>Type of Voucher :</label>
								<div class='col-md-7'>
									<select name='grpunder' class='form-control' required>
									<option value=''>----------</option>";
									
									$this->db->select('*')->from('acc_voucher_types');
									$avou==$this->db->limit(4)->get()->result();
									
									foreach($avou as $av)
									{
									echo "<option value='".$av->acc_vou_name,"'"; if($row->acc_vou_name==$av->acc_vou_name) echo 'selected'; echo " >".$ag->acc_vou_name."</option>";
									}
									
								echo "</select>
								</div>
							</div>
						</div>	
   <hr style='margin:5px;'>
  
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-7'>
                <input type='submit' class='btn btn-primary' value='Update'/>
				<button type='button' class='btn btn-default' data-dismiss='modal' style='margin-left:20px;'>Close</button>
                </div>
                </div>                                
				<br>
           </form>
<!--		   <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div>  -->
";

}


public function update_ledger()
{
  	    $lid=$this->input->post('ledgerid');
		$lname=$this->input->post('lname');
		$lunder=$this->input->post('grpunder');
		$lmname=$this->input->post('mname');
		$ladd=$this->input->post('address');
		$lopbal=$this->input->post('opbal');
		
		
		$cdt=array("acc_ledg_lname"=>strtoupper($lname),
		"acc_ledg_under"=>$lunder,
		"acc_ledg_name"=>$lmname,
		"acc_ledg_address"=>$ladd,
		"acc_ledg_opbalance"=>$lopbal,
		);
				
		$this->load->model("Model_account");
		$this->Model_account->update('acc_ledgers',$cdt,array("acc_ledg_id"=>$lid));
		
		$this->session->set_flashdata('message', '2#Ledger details updated.'); 
        redirect('Account/ledger');
	
}

public function delete_ledger()
{
  	    $lid=$this->uri->segment(3);
				
		$this->load->model("Model_account");
		$this->Model_account->delete('acc_ledgers',array("acc_ledg_id"=>$lid));
		$this->session->set_flashdata('message', '3#Ledger details removed.'); 
        redirect('Account/ledger');
}

    public function logout()
    {
            $this->session->sess_destroy();
            redirect('login/index');
    }

/*---------------------------------------------------------------------------------

public function category_names()
{
	$vcat=$this->input->post('vcatid');
	if($vcat==1)
	{
	$drresult=$this->db->select('*')->from('driverregistration')->get()->result();
	$res="<option value=''>---------</option>";
	
	foreach($drresult as $drr)
	{	if($drr->driver_id!='1')
		{
		$res.="<option value='".$drr->driver_id."'>".$drr->driver_name."</option>";
		}
	}
	}
	
	else if($vcat==2)
	{
	$drresult=$this->db->select('*')->from('driverregistration')->get()->result();
	$res="<option value=''>---------</option>";
	
	foreach($drresult as $drr)
	{	if($drr->driver_id!='1')
		{
		$res.="<option value='".$drr->driver_id."'>".$drr->driver_name."</option>";
		}
	}
	}
	else if($vcat==3)
	{
	$drresult=$this->db->select('*')->from('customers')->where('customer_type=2')->get()->result();
	$res="<option value=''>---------</option>";
	foreach($drresult as $drr)
	{
		$res.="<option value='".$drr->customer_id."'>".$drr->customer_name."</option>";
	}
	}
	echo $res;
}


public function save_voucher()
{
	$this->load->model("Model_account");
	
	$vtype=$this->input->post('vtype');
	$vledger=$this->input->post('vledger');
	$vledgerto=$this->input->post('vledgerto');
	$vcategory=$this->input->post('vcategory');
	$vname=$this->input->post('vname');
	$vdesc=$this->input->post('vdesc');
	$vamount=$this->input->post('vamount');
	
	$vdate=date('Y-m-d');
	
	if($vcategory==1)
	{
	
	$where=array('driver_id'=>$vname);
	$drow=$this->Model_account->driver_balance("driver_account",$where);
		if(isset($drow))
		{
			$drbal=($vamount+$drow->balance);
		}
		else
		{
			$drbal=$vamount;
		}
	
	$data=array('driver_id'=>$vname,'date'=>$vdate,'description'=>$vdesc,
	'debit'=>"0",'credit'=>$vamount,'balance'=>$drbal,
	);
	$this->Model_account->insert("driver_account",$data);
	
	$drbal=$drbal-$vamount;
	$data=array('driver_id'=>$vname,'date'=>$vdate,'description'=>$vdesc,
	'debit'=>$vamount,'credit'=>"0",'balance'=>$drbal,
	);
	$this->Model_account->insert("driver_account",$data);
	}
	redirect('Account/new_voucher');
}
*/

public function get_driver_staff_names()
{
	$cid=$this->input->post('catid');
	$opt="<option value=''>----------</option>";
	if ($cid=="1")
	{
		$result=$this->db->select('*')->from('driverregistration')->get()->result();
		
		foreach($result as $r)
		{
			$opt.="<option value='".$r->driver_id."'>".$r->driver_name."</option>";
		}
	}
	else if ($cid=="2")
	{
		$result=$this->db->select('*')->from('staffregistration')->get()->result();
		foreach($result as $r)
		{
			$opt.="<option value='".$r->staff_id."'>".$r->staff_name."</option>";
		}
	}
	echo $opt;
}

public function get_customer_names()
{
	$cid=$this->input->post('catid');
	
	$opt="<option value=''>----------</option>";
	
	if ($cid=="1")
	{
		$result=$this->db->select('*')->from('customers')->where('customer_type','1')->get()->result();
		
		foreach($result as $r)
		{
			$opt.="<option value='".$r->customer_id."'>".$r->customer_name."</option>";
		}
	}
	else if ($cid=="2")
	{
		$result=$this->db->select('*')->from('customers')->where('customer_type','2')->get()->result();
		foreach($result as $r)
		{
			$opt.="<option value='".$r->customer_id."'>".$r->customer_name."</option>";
		}
	}
	else if ($cid=="3")
	{
		$result=$this->db->select('*')->from('customers')->where('customer_type','3')->get()->result();
		foreach($result as $r)
		{
			$opt.="<option value='".$r->customer_id."'>".$r->customer_name."</option>";
		}
	}
	
	echo $opt;
}


public function save_payments()
{
	$this->load->model("Model_account");
	
	$vtype=$this->input->post('vtype');
	$vgroup=$this->input->post('vgroup');
	$vcat=$this->input->post('vcat');
	$vname=$this->input->post('sdnames');
	$vamt=$this->input->post('vamt');
	$vnarr=$this->input->post('vnarr');
	$vname1=$this->input->post('particular');
	$vdate=date('Y-m-d');
			
	if($vname1!="")
	{
		$vname=$vname1;
	}
	
	$data1=array(
			'acc_vdate'=>$vdate,
			'acc_vtype'=>$vtype,
			'acc_vgroup'=>$vgroup,
			'acc_vcategory'=>$vcat,
			'acc_vname'=>$vname,
			'acc_vexpense'=>$vamt,
			'acc_vincome'=>"0",
			'acc_vnarration'=>$vnarr,
			);

	$this->Model_account->insert("acc_payments",$data1);
	$this->session->set_flashdata('message', '1#Voucher sccessfully added.'); 
	$data['tabstatus']='1';
	redirect('Account/payment');
}
public function save_receipts()
{
	$this->load->model("Model_account");
	
	$rtype=$this->input->post('rtype');
	$rgroup=$this->input->post('rgroup');
	$rcat=$this->input->post('rcat');
	$rname=$this->input->post('rclient');
	$ramt=$this->input->post('ramt');
	$rnarr=$this->input->post('rnarr');
	$sdate=$this->input->post('startdate');
	$edate=$this->input->post('enddate');
	
	$dt1=explode("-",$sdate);
	$dt2=$dt1[2]."-".$dt1[1]."-".$dt1[0];
	
	$dt3=explode("-",$edate);
	$dt4=$dt3[2]."-".$dt3[1]."-".$dt3[0];
		
	$rdate=date('Y-m-d');
		
	$data1=array(
			'acc_redate'=>$rdate,
			'acc_retype'=>$rtype,
			'acc_regroup'=>$rgroup,
			'acc_rename'=>$rname,
			'acc_redatefrom'=>$dt2,
			'acc_redateto'=>$dt4,
			'acc_reexpense'=>"0",
			'acc_reincome'=>$ramt,
			'acc_renarration'=>$rnarr,
			);

	$this->Model_account->insert("acc_receipts",$data1);
	$this->session->set_flashdata('message', '1#Receipts sccessfully added.'); 
	$data['tabstatus']='1';
	redirect('Account/receipt');
}

public function save_revenue()
{
	$this->load->model("Model_account");
	
	$revtype=$this->input->post('revtype');
	$revgroup=$this->input->post('revgroup');
	$revdate=$this->input->post('revdate');
	$revamt=$this->input->post('revamt');
	$revnarr=$this->input->post('revnarr');
		
	$dt1=explode("-",$revdate);
	$dt2=$dt1[2]."-".$dt1[1]."-".$dt1[0];
			
	$data1=array(
			'acc_revdate'=>$dt2,
			'acc_revtype'=>$revtype,
			'acc_revgroup'=>$revgroup,
			'acc_revamount'=>$revamt,
			'acc_revnarration'=>$revnarr,
			);

	$this->Model_account->insert("acc_revenue",$data1);
	$this->session->set_flashdata('message', '1#Revenue details sccessfully added.'); 
	$data['tabstatus']='1';
	redirect('Account/revenue');
}








public function search_receipts()
{
	$this->load->model("Model_account");
	
	$rclient=$this->input->post('rclient');
	$sdate=$this->input->post('startdate3');
	$edate=$this->input->post('enddate4');
	
	$dt1=explode("-",$sdate);
	$dt2=$dt1[2]."-".$dt1[1]."-".$dt1[0];
	
	$dt3=explode("-",$edate);
	$dt4=$dt3[2]."-".$dt3[1]."-".$dt3[0];

	$rw=$this->db->select('*')->from('customers')->where('customer_id',$rclient)->get()->row();
	$title="RECEIPTS :".$rw->customer_name.", &nbsp;&nbsp;Period : [".$sdate." => ". $edate."]";

	$data['results']=$this->Model_account->search_receipts($dt2,$dt4,$rclient);
	
	$this->session->set_flashdata('ptitle', $title);
	
	$data['tabstatus']='2';
	$this->load->view('account/corporate_receipts',$data);
}

public function dr_accounts()
{
	$this->load->model("Model_account");
	
	$dname=$this->input->post('drname');
	$sdate=$this->input->post('startdate');
	$edate=$this->input->post('enddate');
	
	$d1=explode("-",$sdate);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	
	$d2=explode("-",$edate);
	$dt2=$d2[2]."-".$d2[1]."-".$d2[0];

	$rw=$this->db->select('*')->from('driverregistration')->where('driver_id',$dname)->get()->row();
	$title=  $rw->driver_name. " - Account details, &nbsp;&nbsp;Period : [".$sdate." => ". $edate."]";

	$data['results']=$this->Model_account->driver_account($dt1,$dt2,$dname);
	$this->session->set_flashdata('ptitle', $title);
	$this->load->view('account/driver_account',$data);
}


public function staff_accounts()
{
	$this->load->model("Model_account");
	
	$stid=$this->input->post('stname');
	$sdate=$this->input->post('startdate');
	$edate=$this->input->post('enddate');
	
	$d1=explode("-",$sdate);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	
	$d2=explode("-",$edate);
	$dt2=$d2[2]."-".$d2[1]."-".$d2[0];

	$rw=$this->db->select('*')->from('staffregistration')->where('staff_id',$stid)->get()->row();
	$title= $rw->staff_name." - Account details, &nbsp;&nbsp;Period : [".$sdate." => ". $edate."]";

	$data['results']=$this->Model_account->staff_account($dt1,$dt2,$stid);
	$this->session->set_flashdata('ptitle', $title);
	$this->load->view('account/staff_account',$data);
}


public function vgroup_accounts()
{
	$this->load->model("Model_account");
	$vgroup=$this->input->post('vgroup');
	$sdate=$this->input->post('startdate');
	$edate=$this->input->post('enddate');
	
	$d1=explode("-",$sdate);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	
	$d2=explode("-",$edate);
	$dt2=$d2[2]."-".$d2[1]."-".$d2[0];

	if($vgroup=='0')
	{
	$title="All Group - Account details, &nbsp;&nbsp;Period : [".$sdate." => ". $edate."]";
	$data['results']=$this->Model_account->vgroup_account($dt1,$dt2,$vgroup,'1');
	}
	else
	{
	$rw=$this->db->select('*')->from('voucher_group')->where('vg_id',$vgroup)->get()->row();
	$title= $rw->vg_name." - Account details, &nbsp;&nbsp;Period : [".$sdate." => ". $edate."]";
	$data['results']=$this->Model_account->vgroup_account($dt1,$dt2,$vgroup,'2');
	}
	$this->session->set_flashdata('ptitle', $title);
	$this->load->view('account/voucher_group_account',$data);
}


}
?>