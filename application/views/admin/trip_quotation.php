<?php include('application/views/common/header.php'); ?>
<!-- <link rel="stylesheet" href="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.css');?>" rel="stylesheet" type="text/css" /><script src="

<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script><script src="

<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.js');?>"></script> -->
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/bootstrap/bootstrap.min.css');?>">-->
<link rel="stylesheet" type="text/css" href="

<?php echo base_url('assets/common/font-awesome/css/font-awesome.min.css');?>">
<link rel="stylesheet" type="text/css" href="

<?php echo base_url('assets/common/search_tabs/selectize.css');?>">
<style> /* hide number  spinner*/ input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {-webkit-appearance: none; margin: 0; } .ttext {background-color:#fff; font-weight:bold; color:Green; } .search-btn {padding-top:3px; z-index:1000; right:17px; position:absolute; } .select_btn1 ,.select_btn2{position: absolute; top: 3px; z-index: 999; } .select_btn1{right: 19px; } .select_btn2 {right: 19px; } legend{margin-left:10px; width:20%; } </style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Main content -->
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		<b>New Trip Coat</b>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="
				
				<?php echo base_url('Trip/trip_qutationlist');?>" style="color:#4b88ed;font-size:15px;">
				<i class="fa fa-backward" aria-hidden="true"></i>Back to Quotation List
			
			</a>
		</li>
	</ol>
</section>
<label for="" style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->
<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-md-12">
			<!--Add new user details -->
			<div style="background-color:#fff;border-radius:10px;padding:15px; ">
				<!--   <div class="portlet-title"><div class="caption"><div style="color:blue;" > Trip Details </div></div></div> -->
				<div class="portlet-body form">
					<form class="form-horizontal" role="form" method="POST" action="
						
						<?php echo base_url('Trip/add_tripquatation')?>">
						
						<div class="row">
							<fieldset>
								<legend>Customer :</legend>
								<div class='form-group'>
									<label for="cust_name" class='col-md-3 control-label' style='padding-top:5px;'>Customer Name : </label>
									<div class='col-md-8'>
										<input type='text' class='form-control' name='cust_name' id='cust_name' required>
										</div>
									</div>
									<div class='form-group'>
										<label for="cust_addr" class='col-md-3  control-label' style='padding-top:5px;'>Address : </label>
										<div class='col-md-8'>
											<textarea class="form-control" name="cust_addr" id="cust_addr" style="height:100px" required></textarea>
											</div>
										</div>
										<div class='form-group'>
											<label for="phone" class='col-md-3 control-label' style='padding-top:5px;'>Phone : </label>
											<div class='col-md-8'>
												<input type='number' class='form-control' name='phone' id='phone' required>
												</div>
											</div>
											<div class='form-group'>
											<label for="description" class='col-md-3 control-label' style='padding-top:5px;'>Description : </label>
											<div class='col-md-8'>
												<textarea class="form-control" name="description" id="description" style="height:100px" required></textarea>
												</div>
											</div>
										</fieldset>
									</div>

						<div class="row">
							<fieldset>
								<legend>Trip Charges:</legend>
								<div class='form-group'>
									<label for="mincharge" class='col-md-3 control-label' style='padding-top:5px;'>Mini(Km) Charge : </label>
									<div class='col-md-8'>
										<input type='number' class='form-control' name='mincharge' id='mincharge' required>
										</div>
									</div>
									<div class='form-group'>
										<label for="percharge" class='col-md-3  control-label' style='padding-top:5px;'>Per(Km) Charge : </label>
										<div class='col-md-8'>
											<input type='number' class='form-control' name='percharge' id='percharge' required>
											</div>
										</div>
										<div class='form-group'>
											<label for="othercharge" class='col-md-3 control-label' style='padding-top:5px;'>Other Charges : </label>
											<div class='col-md-8'>
												<input type='number' class='form-control' name='othercharge' id='othercharge' required>
												</div>
											</div>
										</fieldset>
									</div>
									<div class="row" style="background-color: #f9f9f9;">
										<fieldset>
											<legend>Accommodation :</legend>
											<div class='form-group'>
												<!-- <div class='row'> -->
												<label for="acm_type" class='col-md-3 control-label' style='padding-top:2px;'>Accommodation  Type :</label>
												<div class='col-md-8'>
													<section class="form-control" style='padding:0px;'>
														<select id="acm_type" class="form-controll_prop" name="acm_type" required>
															<option value="0"></option>
															<option value="5">5 Star</option>
															<option value="4">4 Star</option>
															<option value="3">3 Star</option>
															<option value="2">2 Star</option>
															<option value="1">1 Star</option>
															<option value="0">Ordinary</option>
														</select>
													</section>
												</div>
											</div>
											<div class='form-group'>
												<label for="acm_cost" class='col-md-3 control-label' style='padding-top:5px;'>Accommodation  Totalcost : </label>
												<div class='col-md-8'>
													<input type='number' class='form-control' name='acm_cost' id='acm_cost' required >
													</div>
												</div>
												<div class='form-group'>
													<label for="acm_menu" class='col-md-3  control-label' style='padding-top:5px;'>Accommodation  Menu : </label>
													<div class='col-md-8'>
														<textarea class="form-control" name="acm_menu" id="acm_menu" style="height:150px"> Date :         Day : 
Hotel Name 
														</textarea>
													</div>
												</div>
											</fieldset>
										</div>
										<div class="row" style="">
											<fieldset>
												<legend>Food :</legend>
												<div class='form-group'>
													<!-- <div class='row'> -->
													<label for="food_type" class='col-md-3 control-label' style='padding-top:2px;'>Food Type :</label>
													<div class='col-md-8'>
														<section class="form-control" style='padding:0px;'>
															<select id="food_type" class="form-controll_prop" name="food_type" required>
																<option></option>
																<option value='vg'>Vegetarian</option>
																<option value='nv'>Non-Vegetarian</option>
															</select>
														</section>
													</div>
												</div>
												<div class='form-group'>
													<label for="food_cost" class='col-md-3 control-label' style='padding-top:5px;'>Food Totalcost : </label>
													<div class='col-md-8'>
														<input type='number' class='form-control' name='food_cost' id='food_cost' required>
														</div>
													</div>
													<div class='form-group'>
														<label for="food_menu" class='col-md-3  control-label' style='padding-top:5px;'>Food Menu : </label>
														<div class='col-md-8'>
															<textarea class="form-control" name="food_menu" id="food_menu" style="height:150px" required>
Date :         Day :
Morning : 
Noon    : 
Evening : 
Night   : 
															</textarea>
														</div>
													</div>
												</fieldset>
											</div>
												<div class="row" style="">
											<fieldset>
												<legend>Place Visiting :</legend>
													<div class='form-group'>
														<label for="place_visit" class='col-md-3  control-label' style='padding-top:5px;'>Visiting Places : </label>
														<div class='col-md-8'>
															<textarea class="form-control" name="place_visit" id="place_visit" style="height:150px" required></textarea>
														</div>
													</div>
												</fieldset>
											</div>
										</div>
									</div>
									<label for="" style="width:100%;height:1px;"></label>
									<div class="row"></div>
									<div class='form-group'>
										<div class='row'>
											<center>
												<button type="submit" class="btn btn-success" style="padding:10px 50px 10px 50px;" id="idsavebtn" >
													<b>Make Quotation</b>
												</button>
											</center>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php include('application/views/common/footer.php');?>
<script type="text/javascript">
$('#acm_type, #food_type').selectize({
     create: false
     , sortField: {
     //field: 'text',
     direction: 'asc'
   }
   , dropdownParent: 'body'
   });
</script>
</body>
</html>

