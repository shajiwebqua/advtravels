<?php
 include('application/views/common/header.php');?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Vehicle Service History</b></h1>
    <ol class="breadcrumb" style='font-size:15px;'>
	<li><a href="<?php echo base_url('Services/Index');?>" style='color:#4b88ed;'><i class="fa fa-backward" aria-hidden="true"></i>Back</a></li>
	<li><a  id='view-pdf' href="<?php echo base_url('Pdf/service_invoice');?>" style='color:#4b88ed;'><i class="fa fa-print" aria-hidden="true"></i>Print</a></li>
	<li><a href="" id='idsave' style='color:#4b88ed;'><i class="fa fa-save" aria-hidden="true"></i>Save</a></li>
	<li><a href=""  id='idshare' style='color:#4b88ed;' data-target="" data-toggle='modal'><i class="fa fa-share-alt" aria-hidden="true" ></i>Share</a></li>
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 	  
  <section class="content"> 
			
  <div class="row">
                 <div class="col-md-1" style="border-right:1px solid #cecece">
				 <h4  style="color:blue;">Search </h4> 
				 </div>

			<div class="col-md-8">
					<div class="form-group" >
					<label class="col-md-3 control-label" style="padding-top:7px;text-align:right;">Vehicle Reg No: </label>
					<div class="col-md-3">
					<select class="form-control" id="idregno" name="regno" required>
					<option value="">---------</option>
					<?php
						foreach($regno as $values1)
						{
						?>
						<option value='<?=$values1->vehicle_id;?>'> <?=$values1->vehicle_regno;?> </option>
						<?php } ?>
					</select>

					</div> 
					
					</div>
				    </div>
										
					<!--  <div class="col-md-3">
                   	<div class="form-group" >
					<label class="col-md-4 control-label" style="padding-top:7px;" >Month:</label>
					<div class="col-md-8">
					<select class="form-control" id="idmonth" name="month">
					<option value="0">---------</option>
					<option value="0">January</option>
					<option value="0">February</option>
					<option value="0">March</option>
					<option value="0">April</option>
					<option value="0">May</option>
					<option value="0">June</option>
					<option value="0">July</option>
					<option value="0">August</option>
					<option value="0">September</option>
					<option value="0">October</option>
					<option value="0">November</option>
					<option value="0">December</option>
					 </select>
					</div> 
					</div> 

				</div>  -->
					

					<label style="background-color:#cecece;width:100%;height:1px;"></label>
				    </div>   
  <!-- <div style="color:blue;font-size:12px;"> Select Customer details first (click on the table row) , Then Choose the option (Print,Share,Save)  </div> -->
  
   <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
  
  
   <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details -->
	
<div style="background-color:#fff;padding:15px; ">
		
							
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
 				
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
								<tr>
								 

								 <th>Edit</th>
				                 <th>Delete</th>
								 <th>ID</th>
				                 <th>Service Date</th>
				                 <th>Vehicle RegNo</th>
				                 <th>Description</th>
				                 <th>Invoice Date</th>
				                 <th>Invoice No</th>
				                 <th>Amount</th>
								</tr>
								</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Edit</h4>
								</div>
							</div>
							</div>
						<!-- /.modal-dialog -->
						</div>
                   </div>		




                      <div class="modal fade draggable-modal" id="myModalsh" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
                   </div> 					
				
	</div>
	<!-- End user details -->
	</div>
	
	</div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 //sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------
  $('#example').dataTable( {
	  "ordering":false,
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Services/view",// json datasource
               },
"columnDefs":[
{"width":"9%","targets":8},
],
			   
        "columns": [
		     { "data": "edit"},
            { "data": "delete" },
			{ "data": "sid"},
            { "data": "date"},
            { "data": "regno" },
            { "data": "description" },
            { "data": "invoicedate" },
            { "data": "invoiceno" },
            { "data": "invoiceamt" }
       ]
  } ); 

  
  $("#idregno").change(function(){
    var uaid=$('#idregno').val();
    if(uaid!=='0')
	{
	  $('#example').dataTable( {
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Services/regno_ajax/"+uaid,// json datasource
               },
           "columns": [
		    { "data": "edit"},
            { "data": "delete" },
			{ "data": "sid"},
            { "data": "date"},
            { "data": "regno" },
            { "data": "description" },
            { "data": "invoicedate" },
            { "data": "invoiceno" },
            { "data": "invoiceamt" }
       ]
       });
	}
  });
  
  
   // var table = $('#example').DataTable();
   //   $('#example tbody').on( 'click', 'tr', function () {
   //      if ( $(this).hasClass('selected') ) {
   //          $(this).removeClass('selected');
   //      }
   //      else {
	  //       table.$('tr.selected').removeClass('selected');
   //          $(this).addClass('selected');
   //      }
   //  } );


     

       $('#view-pdf').on('click',function(){
       var uaid=$('#idregno').val();
       //alert(uaid);
        if(uaid=="")
         {
       alert ("Please Select Vehicle Regno..");
         }
       else
        {
        var pdf_link = $(this).attr('href')+'/'+uaid;
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(pdf_link);
        $.createModal({
        title:'Service Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });

       
       } return false; 

    }); 
       // });   


  
  
  
  

      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Services/edit_services",
        dataType: 'html',
        data: {service_id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 


         $('#idshare').click(function ()
  { 
    var Result=$("#myModalsh .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         //var id =  $(this).attr('id');
     //var id=$('#example').find('tr.selected').find('td').eq(0).text();
       var id=$('#idregno').val();
     //alert(id);
     if(id=="")
     {
       alert ("Please Select Vehicle Reg No..");
     }
     else
     {
    $('#idshare').attr('data-target','#myModalsh'); 
      jQuery.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "Services/share_servicehistory",
      dataType: 'html',
      data:{id:id},
      success: function(res) {
      Result.html(res);
       }
      });
     }
        });


         $('#idsave').click(function ()
    {   
        //var Result=$("#myModalsa .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         //var id =  $(this).attr('id');
         //var id=$('#example').find('tr.selected').find('td').eq(0).text();
     //alert(id);
      var id=$('#idregno').val();
         if(id=="")
         {
             alert ("Please Select Vehicle Reg No..");
         }
         else
         {
            jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "Services/save_servicehistory",
            dataType: 'html',
            data:{uid:id},
            success: function(res) {
            Result.html(res);
             }
            });
         }
        }); 
   
		
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });



 //});
  


</script>



