<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

 <table width="100%"><tr><td><h4>Staff Logs List </h4></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
		<table style="width:100%;border:1px solid #e4e4e4;" cellpadding=5 cellspacing=0>
		   <thead>
			<tr>
			   <tr style='color:#4b88ed;border:1px solid #e4e4e4;'>
			     <th class='th1'>Slno</th>
			       <th width='70px'>Staff ID</th>
			      <th class='th1'>Staff Name</th>
				  <th class='th1'>Date</th>
                  <th class='th1'>Time</th>
				  <th class='th1'>Entry ID</th>
				 <th class='th1'>Description</th>
				</tr>
			</tr>
		</thead>
		<tbody>
			<?php
			$slno=1;
				foreach ($results as $key => $value) {
					
			
				?>
				<tr >
				<td class='td1'><?=$slno;?> </td> 
				<td class='td1'><?=$value->log_staffid;?></td>
				<td class='td1'><?=$value->log_staffname;?> </td>
				<td class='td1'><?=$value->log_date;?> </td>
				<td class='td1'><?=$value->log_time;?> </td>
				<td class='td1'><?=$value->log_entryid;?> </td>
				<td class='td1'><?=$value->log_desc;?> </td>
				
			</tr>
				<?php
				
				$slno++;
				}
				?>
		  
		</tbody>
		</table>
</body>
</html>