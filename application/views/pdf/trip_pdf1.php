<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8" />
  <title>Travels</title>
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

<div id="title">Trip Details</div>
</span>
<hr>

<label id="name"><h3> Customer Name</label></h3>
 <?php
 foreach ($results as $key => $value) {
  ?>
  <div id="name1">:<?=$value->customer_name;?></div>

<label id="name"><h3>Mobile</label></h3>
<div id="name1"><?=':'.$value->customer_mobile;?></div>

<label id="name"><h3>Purpose</label></h3>
<div id="name1"><?=':'.$value->trip_purpose;?></div>

<label id="name"><h3>Vehicle</label></h3>
<div id="name1"><?=':'.$value->vehicle_regno;?></div>


<label id="name"><h3>Driver Name</label></h3>
<div id="name1"><?=':'.$value->driver_name;?></div>

<label id="name"><h3>Trip From-To</label></h3>
<div id="name1"><?=':'.$value->trip_from.' '.'-'.$value->trip_to;?></div>


<label id="name"><h3>Date Start-End</label></h3>
<div id="name1"><?=':'.$value->trip_startdate.' '.'-'.$value->trip_enddate;?></div>

<label id="name"><h3>Time Start-End</label></h3>
<div id="name1">
<?=':'.$value->trip_starttime.' '.'-'.$value->trip_endtime;?></div>

<label id="name"><h3>Km Start-End</label></h3>
<div id="name1">
<?=':'.$value->trip_startkm.' '.'=>'.$value->trip_endkm;?></div>
<hr>

<label id="name"><h3>Running Km</label></h3>
<div id="name1">
<?php 
$end=$value->trip_endkm;
$start=$value->trip_startkm; ?>
<?=':'.abs($end-$start);?></div>

<div id="mkm">
<label id="name"><h3>Minimum Charge Km</h3></label></div>
<div id="mckm1">
<?=':'.$value->trip_minchargekm;?></div>

<hr>
<div id="title"><b>Charges</b></div>
<hr>
Minimum Charges:
<label id="name"><h3>Min.Days</label></h3>
<div id="days1">
<?=':'.$value->trip_mindays;?></div>


<div id="mc">
<label id="name"><h3>Min.Charge</h3></label></div>
<div id="mc1">
<?=':'.$value->trip_mincharge;?></div>

<div id="tot">
<label id="name"><h3>Total</h3></label></div>
<div id="tot1">
<?=':'.$value->trip_mintotal;?></div>

<div style="margin-top: 10px;">Additional Charges:</div>
<label id="name"><h3>Addi.Km</label></h3>
<div id="days1">
<?=':'.$value->trip_addikm;?></div>


<div id="mc">
<label id="name"><h3>Km.Charge</h3></label></div>
<div id="mc1">
<?=':'.$value->trip_addikmcharge;?></div>

<div id="tot">
<label id="name"><h3>Total</h3></label></div>
<div id="tot1">
<?=':'.$value->trip_additotal;?></div>
<div id="totc">
<label id="name"><h3>Total Trip Charge</h3></label></div>
<div id="totc1">
<?=':'.$value->trip_charge;?></div>


<div style="margin-top: 10px;">Other Charges:</div>
<label id="name"><h3>Toll/Parking</label></h3>
<div id="days1">
<?=':'.$value->trip_tollparking;?></div>


<div id="is">
<label id="name"><h3>Interstate Permit</h3></label></div>
<div id="mc1">
<?=':'.$value->trip_interstate;?></div>

<div id="oc">
<label id="name"><h3>Other Charges</h3></label></div>
<div id="tot1">
<?=':'.$value->trip_othercharge;?></div>

<label id="name"><h3>Other Charges Description</label></h3>
<div id="desc1">
<?=':'.$value->trip_otherdesc;?></div>

<label id="name"><h3>Addition Information</label></h3>
<div id="desc1">
<?=':'.$value->trip_addiinfo;?></div>

<div id="grand">
<label id="name"><h3><b>Grand Total</b></label></h3></label></div>
<div id="grand1">
<b><?='Rs:'.$value->trip_gtotal;?></b></div>

<?php } ?>
                
 

 <style type="text/css">
   #name1
   {
   margin-left: 145px;
   margin-top: -31px;
   font-size: 16px;
   float: left;
   }
   #mckm
   {
      margin-left: 725px;
      float: left;
        margin-top: -29px;
        float: left;
   }
   #mckm1
   {
    margin-left: 420px;
    margin-top: -29px;
     font-size: 16px;

   }
   #mkm
   {
    margin-left: 250px;
      margin-top: -29px;
   }
   #mc1
   {
    margin-left: 280px;
    margin-top: -29px;
     font-size: 16px;

   }
   #mc
   {
    margin-left: 180px;
      margin-top: -29px;
   }
   #title
   {
    font-size: 12px;
   }
    #days1
   {
   margin-left: 100px;
   margin-top: -31px;
   font-size: 16px;
   float: left;
   }
    #tot1
   {
    margin-left: 420px;
    margin-top: -29px;
     font-size: 16px;

   }
   #tot
   {
    margin-left: 350px;
      margin-top: -29px;
   }
    #totc1
   {
    margin-left: 420px;
    margin-top: -30px;
     font-size: 16px;
     float: left;

   }
   #totc
   {
    margin-left: 290px;
      margin-top: 25px;
   }
    #is
   {
    margin-left: 160px;
      margin-top: -29px;
   }
    #oc
   {
    margin-left: 320px;
      margin-top: -29px;
   }
     #desc1
   {
   margin-left: 245px;
   margin-top: -31px;
   font-size: 16px;
   float: left;
   }
   #grand
   {
    margin-left: 310px;
    margin-top: 11px;
     font-size: 13px;
     float: left;
   }
   #grand1
   {
    margin-left: 430px;
    margin-top: -32px;
    font-size: 16px;
   }
        
 </style>
                               
                       
</html>