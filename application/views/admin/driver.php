<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b> Driver's Details</b></h1>
   <ol class="breadcrumb" style='font-size:15px;'>
			<li> <a href="<?php echo base_url("Driver/add_driver");?>" style="color:#4b88ed;font-size:15px;"> <button class='btn btn-primary'><i class="fa fa-user" aria-hidden="true"></i> New Driver</button></a></li>
		<li><a id='view-pdf' href="<?php echo site_url('Pdf/pdf_driver') ?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-print" aria-hidden="true"></i> Print </button></a></li>
		<!-- data-target="#myModal1" --> <li><a id='idsave' href="<?php echo site_url('Pdf/pdf_driver') ?>" data-target="" data-toggle='modal'style='color:#4b88ed;' target="_blank"> <button class='btn btn-primary'><i class="fa fa-save" aria-hidden="true"></i> Save </button></a></li>

		<?php 
		//if(checkisAdmin()){
			//echo "<li><a id='iddelete' href='' style='color:#4b88ed;'><i class='fa fa-trash' aria-hidden='true'></i>Delete</a></li>";
		//}
		?>
		
    </ol> 
  </section>

  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 
	  
  <section class="content">
  <!-- <div style="color:#4b88ed;font-size:12px;"> Select Driver details first (<b>CLICK ON THE TABLE ROW</b>) , And click on the button (Delete)  </div> -->
  <div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div>
				 <div id='idh6'><?php echo $this->session->flashdata('message1'); ?></div>
				 <div id='idh7'><?php echo $this->session->flashdata('message2'); ?></div></center>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details -->
	

	 <div style="background-color:#fff;padding:15px; ">
		
							
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
 			
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="example"  width='100%'>
                        <thead>
								<tr>
								<th>Edit</th>
								<th>ID</th>
								<th>Image</th>
								<th>Name</th>
								<th>Address</th>
								<th>Licence No</th>
								<th>Status</th>
								</tr>
								</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Edit</h4>
								</div>
							</div>
							</div>
						<!-- /.modal-dialog -->
						</div>
                   </div>		

           <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
                   </div>					
				
	</div>
	<!-- End user details -->
	</div>
	
	</div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 
 $("#idmsg").hide();
//sweet alert ----------------------------
	if($("#idh5").html()!="")
	{
		swal("Saved",$("#idh5").html(),"success")
		$("#idh5").html("");
	}
	if($("#idh6").html()!="")
	{
		swal("Updated",$("#idh6").html(),"success")
		$("#idh6").html("");
	}
	
	if($("#idh7").html()!="")
	{
		swal("Deleted",$("#idh7").html(),"success")
		$("#idh7").html("");
	}
	
// sweet alert box -------------------------- 
 
      $('#example').dataTable( {
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Driver/driver_ajax",// json datasource
               
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"6%","targets":1},
		{"width":"8%","targets":2},
		{"width":"16%","targets":3},
		{"width":"16%","targets":5},
		],
			   
        "columns": [
           
            { "data": "edit"},
            { "data": "id" },
			{ "data": "image" },
            { "data": "name"},
            { "data": "address"},
 			{"data": "licence" },
            { "data": "status" }
       ]
  } );

  /*var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });*/
  
        $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Driver/edit_driver",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 

    $('#iddelete').click(function () 
    {
    var id=$('#example').find('tr.selected').find('td').eq(1).text();
	if(id=='')
	{
		alert ("Please select driver details..!");
	}
	else
	{
	var res=confirm("Delete this driver details?");
		if(res)
		{
		//var id=$('#example').find('tr.selected').find('td').eq(1).text();
			jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "Driver/del_entry",
			dataType: 'html',
			data: {id: id},
			success: function(res) {
			Result.html(res);
						}
				});
		}
	}
        });     


  $('#example tbody').on('click', '.change', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "userprofile/change_password",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 



        $('#view-pdf').on('click',function(){
       var id=$('#example').find('tr.selected').find('td').eq(1).text();
    
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
    var pdf_link = $(this).attr('href');
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Driver Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
    return false;   

    });    
		
		
		

		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });



 //});
  


</script>



