<?php
include('application/views/common/header.php');
?>
<style>
input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
	
</style>


<!--<link href="vendor/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="vendor/select2/dist/js/select2.min.js"></script> -->



        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#00adee;margin-bottom:10px;font-weight:bold;'>All Account Transactions 
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
          </section>
	
        <!-- Main content -->
    <section class="content">
	<?php
//var_dump($alltrans);
?>
	<div class='row' style="padding:5px 15px 5px 15px;">
	<!--<div class="box box-info1 box-solid " style="border:1px solid #f5f3f3;"> -->
	<div class="box " style="border:1px solid #f5f3f3;">
	    <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp; Transactions</h3>
		  <label id='mes'><?php echo $this->session->flashdata('message');?></label>
        </div>
		
        <div class="box-body">
			<div class='row' style='margin:0px 5px 0px 5px;background-color:#e4e4e4;padding:5px;'>
					
			<form role="form" method="POST" action="<?php echo base_url('Account/ledgerwise_list/1')?>" enctype="multipart/form-data">
			<div class='col-md-2' style='padding-top:5px;text-align:right;'>Select Ledger : </div>
			<div class='col-md-3'>
			<select class='form-control' name="sledger" >
				<option value="">------</option>
				<?php
					$res1=$this->db->select('')->from('acc_ledgers')->get()->result();
					foreach($res1 as $r2)
					{
						echo '<option value="'. $r2->acc_led_id.'">'.$r2->acc_led_description.'</option>';
					}
				?>
			</select>
			</div>
			<div class='col-md-1' style='padding-top:2px;'><input type='submit' name='btnledgerwise' class='btn btn-primary' value='Get'></div>
			</form>
			
			<form role="form" method="POST" action="<?php echo base_url('Account/ledgerwise_list')?>" enctype="multipart/form-data">
			<div class='col-md-6' style='padding-top:2px;text-align:right;'><input type='submit' name='btnledgerwise1' class='btn btn-warning' value='All'></div>
			</form>
			</div>

			
			<div class='row' style='margin-left:10px;'>
			<label style='padding-bottom:3px; border-bottom:1px solid #e4e4e4;font-size:15px;'><b><?php echo $this->session->flashdata('ptitle');?></b></label>
			</div>
					
			<div class='row' style='padding:10px;'>
			<div class='col-md-12'>
			 <table class="table table-striped table-hover table-bordered" id="example" border=0 STYLE='font-size:14px;width:100%'>
			 
			 <thead>
                <tr>
				 <th width='5%'  style='text-align:center !important;'></th>
                 <th>ID</th>
				 <th width='10%'>Date</th>
                 <th width='8%'>Type</th> 
                 <th width='20%'>Ledger</th>
				 <th >Dedit</th>
				 <th>Credit</th>
				 <th >Narration</th>
				 <th >Cheque_Details</th>
			 
                </tr>
                </thead>
				<tbody>
					<?php
					$crtot=0;
					$drtot=0;
					if(isset($alltrans))
					{
					foreach($alltrans as $r)
					{
						$del=anchor('Account/delete_transactions2/'.$r->acc_trans_id,'<span class="glyphicon glyphicon-trash" aria-hidden="true" style="font-size:14px;color:red;"></span>', array('id' =>'del_conf'));
					?>
						<tr height='33px'>
						 <td align='center'>
						 <?=$del;?>
						 </td>
						 <td><?=$r->acc_trans_id?></td>
					     <td><?=date_format(date_create($r->acc_trans_date),'d-m-Y');?></td>
						 <td><?=$r->acc_vtypename;?></td>
						 <td><?=$r->acc_led_description;?></td> 
						  <td style='text-align:right;'><?=number_format($r->acc_tra_debit,2,".","");?></td> 
						  <td style='text-align:right;'><?=number_format($r->acc_tra_credit,2,".","");?></td> 
						  <td><?=$r->acc_trans_narration;?></td> 
						  <td>No&nbsp;&nbsp;&nbsp;&nbsp;: <?=$r->acc_trans_chequeno;?><br>Date: <?php if($r->acc_trans_chequedate!=null) echo date_format(date_create($r->acc_trans_chequedate),'d-m-Y');?></td> 
						 
						</tr>
					<?php
					$crtot+= $r->acc_tra_credit;
					$drtot+= $r->acc_tra_debit;
					
					}
					
					}
					?>	
				</tbody>
					<tr>
					 <td width='5%'  style='text-align:center !important;'></td>
					 <td></td>
					 <td width='10%'></td>
					 <td width='8%'></td> 
					 <td width='20%'><b>Totals</b></td>
					 <td style='text-align:right;'><b><?=number_format($drtot,2,".","");?></b></td>
					 <td style='text-align:right;' ><b><?=number_format($crtot,2,".","");?></b></td>
					 <td ></td>
					 <td ></td>
				 
					</tr>
				</table>
			
				</div>
			</div>	
			</div> <!-- second tab end --->
			</div> <!-- tab content end -->
        </div>
	</div>

</section>

<?php
  include('application/views/common/footer.php');
  ?>
</body>
</html>
  <script type="text/javascript">
  
  $("#example").dataTable({
	  "ordering":false,
  });
  
   $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
   
   $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
       
  
  $("#mes").hide();
    
  var msg=$("#mes").html().split("#");
  
  if($("#mes").html()!="")
  {
	  var msg=$("#mes").html().split("#");
	if(msg[0]=='1')
	  swal("",msg[1],"success"); //Saved.!
	else if(msg[0]=='2')
	  swal("",msg[1],"success");  //Updated.!
    else if(msg[0]=='3')
	  swal("",msg[1],"success"); //Removed.!
	else if(msg[0]=='4')
	  swal("",msg[1],"error"); //Try Again.
     $("#mes").html("");
  }
  
  $(document).on("click", "#del_conf", function () {
        return confirm('Are you sure you want to delete this entry?');
    });
 </script>
</body>
</html>

  
  