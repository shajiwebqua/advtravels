<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trip_reports extends CI_Controller {

function __construct(){
parent::__construct();
$this->load->helper(array('form', 'url'));
}

public function index()
{
	$arctype=array('sdate1'=>"----",'edate1'=>"----",'corpid'=>"-1",'ctype'=>"0",'ctype1'=>"0");
	$this->session->set_userdata($arctype);
	$this->load->view('admin/view_tripctype_report');
}

public function view_vehicleTrip()
{
   $arveh=array('sdate1'=>"----",'edate1'=>"----",'repvehicle'=>"-1",'repvehicle1'=>"-1",'repavehicle'=>"-1");
   $this->session->set_userdata($arveh);
   $this->load->view('admin/view_tripvehicle_report');
}
               

public function view_report()
{
	//$arctype=array('sdate1'=>"----",'edate1'=>"----",'corpid'=>"0",'ctype'=>"0",'ctype1'=>"0");
	//$this->session->set_userdata($arctype);
	
	$ctype=$this->input->post('ctype');
	$ctype1=$this->input->post('ctype1');
	$corp=$this->input->post('corpclient');
	
		$d1=$this->input->post('sdate');
		$d2=$this->input->post('edate');
		
		if(isset($d1) and isset($d2))
		{
		$dt1=explode("-",$d1);
		$d11=$dt1[2]."-".$dt1[1]."-".$dt1[0];
		
		$dt2=explode("-",$d2);
		$d22=$dt2[2]."-".$dt2[1]."-".$dt2[0];
		
		$this->load->model('Model_tripreports');
		
		$arctype=array('sdate1'=>$d1,'edate1'=>$d2,'corpid'=>$corp,'ctype'=>$ctype,'ctype1'=>$ctype1);
		$this->session->set_userdata($arctype);
		}
		
	$data=array();
		
	if($ctype!="0" && ($ctype1=="0" && $corp=="-1"))
	{
		$data['results']=$this->Model_tripreports->view_tripDT($ctype,"1",$d11,$d22);
	}
	else if($ctype1!="0" && ($ctype=="0" && $corp=="-1"))
	{
		$data['results']=$this->Model_tripreports->view_tripDT($ctype1,"2",$d11,$d22);
	}
	else if($corp!="-1" && ($ctype=="0" && $ctype1=="0"))
	{
        $data['results']=$this->Model_tripreports->view_Corp_TripDT($corp,$d11,$d22);
	}
	
	$this->load->view('admin/view_tripctype_report',$data);	
}




public function vehicle_report()
{
	//$arctype=array('sdate1'=>"",'edate1'=>"",'vehicle'=>"0",'vehicle1'=>"0",'avehicle'=>"0");
	//$this->session->set_userdata($arctype);
	$data['results']=array();
	$vehicle=$this->input->post('vehicle');
	$vehicle1=$this->input->post('vehicle1');
	$avehicle=$this->input->post('avehicle');
	
		$d1=$this->input->post('sdate');
		$d2=$this->input->post('edate');
		
		$data=array();
		
		if(isset($d1) and isset($d2))
		{
		$dt1=explode("-",$d1);
		$d11=$dt1[2]."-".$dt1[1]."-".$dt1[0];
		
		$dt2=explode("-",$d2);
		$d22=$dt2[2]."-".$dt2[1]."-".$dt2[0];
		
		$this->load->model('Model_tripreports');
		
		$arveh=array('sdate1'=>$d1,'edate1'=>$d2,'repvehicle'=>$vehicle,'repvehicle1'=>$vehicle1,'repavehicle'=>$avehicle);
		$this->session->set_userdata($arveh);
		
		
		
			if($vehicle!="-1" && ($vehicle1=="-1" && $avehicle=="-1"))
			{
				$sec=1;
				$data['vehresults']=$this->Model_tripreports->view_tripVehicleDT($vehicle,$sec,$d11,$d22);
				$data['sec']=1;
			}
			else if($vehicle1!="-1" && ($vehicle=="-1" && $avehicle=="-1"))
			{
				$sec=2;
				$data['vehresults']=$this->Model_tripreports->view_tripVehicleDT($vehicle1,$sec,$d11,$d22);
				$data['sec']=2;
			}
			else if($avehicle!="-1" && ($vehicle=="-1" && $vehicle1=="-1"))
			{
				$data['vehresults']=$this->Model_tripreports->view_tripVehicleDT1($avehicle,$d11,$d22);
				$data['sec']=3;
			}
		}
		var_dump($data['vehresults']);
	//$this->load->view('admin/view_tripvehicle_report',$data);	
	
}

}