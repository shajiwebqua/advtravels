<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
td{
	padding:5px 10px 5px 10px;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

 <table width="100%"><tr><td><h3><u><?php echo $dcap;?></u></h3></td><td align='right' >Date : <?php echo date('d-m-Y');?></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
 
 		<div class='row' style='padding:5px 15px 5px 15px;' >
				<div class='col-md-8'>
			<table class="table table-striped table-hover table-bordered" id="example" width='100%'>
                <tr>
								 
				 <?php
				 $month=["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVENMBER","DECEMBER"];
				 $tamount="0";
				 $camt="0";
				 $diff="0";
				 if (isset($result))
				 {
				 foreach($result as $mt)
				 {
					 foreach($result1 as $ct)
					 {
						 if($ct->trip_vehicle_id==$mt->tar_vehicle)
						 {
							 $camt=$ct->camount;
							 $tamount=$mt->tar_amount;
							 $diff=number_format(($ct->camount-$mt->tar_amount),"2",".","");
							 if($diff<=0)
							 {
								 $diff="<font color=red>".$diff."</font>";
							 }
							 else
							 {
								 $diff="<font color=green>".$diff."</font>";
							 }
				 ?>
				 <tr>
				 <td>
					<table width='100%' style='font-size:16px;'>
					<tr style='height:35px;'><td width='400px'>Vehicle Registration No</td><td width='20px'> : </td><td><b><?=$mt->vehicle_regno;?></b></td></tr>
					<tr style='height:35px;'><td >Month</td><td width='20px'> : </td><td><?=$month[$mt->tar_month-1]."&nbsp;&nbsp;-&nbsp;&nbsp;". $mt->tar_year ;?></td></tr>
					<tr style='height:35px;'><td>Target Amount of this month</td><td width='20px'> : </td><td align='right'><?="<b>&#8377;&nbsp;&nbsp;".number_format($mt->tar_amount,"2",".","")."</b>";?></td></tr>
					<tr style='height:35px;'><td>Total Collected Amount</td><td width='20px'> : </td><td align='right'><?="<b>&#8377;&nbsp;&nbsp;".number_format($ct->camount,"2",".","")."</b>";?></td></tr>
					<tr ><td></td><td width='20px'> : </td><td align='right'>===============</td></tr>
					<tr style='height:35px;font-size:18px;'><td>Difference</td><td width='20px'> : </td><td align='right'><?="<b>&#8377;&nbsp;&nbsp;".$diff."</b>";?></td></tr>
					<tr height='50px;'><td></td><td width='20px'> </td><td align='right' valign='top'>===============</td></tr>
					
					</table>
					
				 </td>
				 </tr>
				<?php
						}
					 }
				 }
				 }
				 ?>
				
				 
            </table>
			</div>
		</div>
</body>
</html>