<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>View Users</b></h1>
    <ol class="breadcrumb">

	<li><a href="<?php echo base_url('Userprofile/add_users');?>" data-target="#myModal1" data-toggle='modal'><input type='button' class='btn btn-primary  add' value='New User Details'></a></li>
  <li><a href="<?php echo site_url('Pdf/user') ?>" ><input type='button' class='btn btn-primary add' value='Print'></a></li>
</ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 
	  
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!------------------------------------ Add new user details ----------------------------------->
	

	 <div style="background-color:#fff;padding:15px; ">
		
							
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                 <CENTER>
                    <h5 id="idh5" style="color:green;">
                       <?php echo $this->session->flashdata('usermsg'); ?>
                    </h5>
                </CENTER>
				
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
								<tr>
								 <th>Edit</th>
								 <th>Password</th>
								 <th>Delete</th>
								 <th>Name</th>
								 <th>Image</th>
								 <th>Username</th>
								 <th>Mobile</th>
								 <th>Status</th>
								</tr>
								</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
                   </div> 

                    <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>                   
        
  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
       $('#example').dataTable( {
         "ordering": false,
		 destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "userprofile/user_ajax",// json datasource
               
               },
		"columnDefs":[
		{"width":"8%","targets":0},
		{"width":"10%","targets":1},
		{"width":"8%","targets":2},
		{"width":"10%","targets":4},
		],
			   
        "columns": [
           
            { "data": "edit"},
			{ "data": "change"},
            { "data": "delete" },
            { "data": "name"},
            { "data": "image" },
			{ "data": "username" },
            { "data": "mobile" },
            { "data": "status" }
       ]
  } );



      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "userprofile/edit_user",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 


  $('#example tbody').on('click', '.change', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "userprofile/change_password",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 		
		
		
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#idh5').html(''); }, 3000);

 //});
  


</script>



