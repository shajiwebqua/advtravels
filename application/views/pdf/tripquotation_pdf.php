<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Travels</title>

  <style type="text/css">
 td
   {
   padding:5px;
   }

  .brd1
  {
	  border:1px solid #e4e4e4;
	  
  }  
   </style>
  
  
  </head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
<td width="150px" style="padding: 0px">
<?php $date = date('Y/m/d');?>
Date: <?=$date;?>
  </td>
</tr>
</table>
<hr>

<table style="border:none;width:100%;">
  <tr>
      <td>From
          <address>
            <strong>Advance World Holidays</strong><br>
            PK & Sons Complex, East Moozhikkal<br>
            NH 212, Calicut, Kerala, India-673571<br>
		    Tel: 0091 495 2732 500, 0091 495 3100 700<br>
            Fax: 0091 495 2732 400
          </address></td>
          <td> To
          <address>
            <strong><?=$results->cust_name;?></strong><br>
           <?=$results->cust_address;?><br>
           Phone:<?=$results->cust_phone;?><br>
            
          </address></td>
          <th>Trip Quotation No: <?=$id;?>
          </th>
  </tr>
</table>
<hr>
 <h4>Food Menu :</h4>
  <table width="100%">
    <tr>
      <!-- <th align='left'></th> -->
      <td><?= $results->food_menu;?></td>
    </tr>
  </table>
  <hr>
  <h4>Accomodation Menu :</h4>
  <table width="100%">
    <tr>
      <!-- <th align='left'></th> -->
      <td><?=$results->acm_menu; ?></td>
    </tr>
  </table>
  <hr>
  <h4>Places Visiting :</h4>
  <table width="100%">
    <tr>
      <!-- <th align='left'></th> -->
      <td><?= $results->place_visit; ?></td>
    </tr>
  </table>
<hr>
  <h4>Charges :</h4>
  <table width="25%">
    <tr>
      <th align='left'>Minimum Charge KM</th>
      <td><?= $results->trip_mincharge;?></td>
    </tr>
    <tr>
      <th align='left'>Per Km Charge</th>
      <td><?=$results->trip_perkmcharge;?></td>
    </tr>
    
    <tr>
      <th align='left'>Accomadation Charge</th>
      <td><?= $results->acm_cost; ?></td>
    </tr>
    <tr>
      <th align='left'>Food Charge</th>
      <td><?=$results->food_cost; ?></td>
    </tr>
	<tr>
      <th align='left'>Other Charges</th>
      <td><?= $results->trip_othercharge; ?></td>
    </tr>
  </table>
 <table width="100%">
 <tr><td colspan='3' height='30px'></td> </tr>
             <tr>
           <?php    
           $total=$results->trip_mincharge+$results->trip_othercharge+$results->acm_cost+$results->food_cost
           ?>
                <th width='86%' style='text-align:right;padding-right:25px;font-size:25px;'>RS : <?= $total; ?></th>
                <td width='10%' style='text-align:right;font-size:25px;'><b></b></td>
        <td width='3%'></td>
              </tr>
        <tr>
                <td width='10%' style='text-align:right;font-size:25px;'><b>===========</b></td>
        <td width='3%'></td>
              </tr>
 </table>


</html>