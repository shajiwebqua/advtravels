<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<title>Travels</title>
	<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" /> 
	<style type="text/css">
		
	table{
		font-size: 14px;
	}
		td{
			padding:7px 5px 7px 5px;

		}

		th{
			padding:7px 5px 7px 5px;
			
		}

		.center {
    		margin: auto;
    		width: 25%;
		}
		
	</style>
</head>
<body>
<table style="border:none;width:100%;">
		<tr>
			<td colspan=2 style="padding: 0px">
				<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" height='35px'>
			</td>
		</tr>
		<tr>
			<td style="padding:0px">
				<span style="font-size:15px;">Advance World Holidays</span><br>
				<span>PK & Sons Complex, East Moozhikkal</span><br>
				<span>NH 212, Calicut, Kerala, India - 673571</span><br>
				<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
				<span>Fax: 0091 495 2732 400</span>
			</td>
			<td width='150px' valign='top' align='right'>Date: <?php echo date('d-m-Y');?></td>
		</tr>
	</table>
	<hr style='margin-bottom:2px;'>
	 <table width="100%"><tr><td width='220px;'><h5><b><?php echo $this->session->flashdata('inchead');?></b></h5></td></tr>
 </table>
 <hr style='margin-top:2px;'>
	<table class="table table-bordered table-sm" width='100%'>
		<thead>
			<tr>
				<th width="70px">ID</th>
				<th width="120px">Date</th>
				<th>Income_Type</th>
				<th>From</th>
				<th>Narration</th>
				<th width="100px" >Amount</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$i = 1;
			$tot=0;
			foreach($addiinc as $res)
			{
			?>
				<tr>
					<td><?=$res->addi_inc_id;?></td>
					<td><?=$res->addi_inc_date;?></td>
					<td><?=$res->inc_type_name;?></td>
					<td><?=$res->addi_inc_from;?></td>
					<td><?=$res->addi_inc_narration;?></td>
					<td align='right'><?=number_format($res->addi_inc_amount,"2",".","");?></td>
				</tr>
			<?php
			$tot+=$res->addi_inc_amount;
			}
			?>
		</tbody>
	</table>
	<table width='100%' >
	<tr> <td></td>	<td width='150px' align='right' style='padding:0px;'>-------------------------</td>	</tr>
		<tr > <td align='right' style='font-size:16px;'>Total</td>	<td width='150px' align='right' style='font-size:16px;'><b><?=number_format($tot,"2",".","");?></b></td></tr>
		<tr> <td></td> <td width='150px' align='right' style='padding:0px;'>===========</td>	</tr>
	</table>

</body>                             

</html>