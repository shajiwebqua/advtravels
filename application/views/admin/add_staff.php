<?php
 include("application/views/common/header.php");?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.ttext
{
	background-color:#fff;
	font-weight:bold;
	color:Green;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
  
   <?php 
    //include("application/views/common/top_menu.php");
    ?>
    
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>New Staff Details</b> </h1>
  <ol class="breadcrumb">
   <li> <a href="<?php echo base_url("Staff/view_staff");?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'> <i class="fa fa-list" aria-hidden="true"></i> Staff List</button></a></li>
    </ol>  
   </section>
<label style="background-color:#cecece;width:100%;height:1px;"></label>
  <!-- Content Header end -->
<section class="content"> 
<div class="row">
<div class="col-md-12">

  <!--Add new user details -->
	<div style="background-color:#fff;border-radius:3px;padding:15px; ">
      <div class="portlet-title">
		<div class='row'>
		<div class='col-md-5'>
		<h4 style='padding:0px; margin:0px;'><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Details</h4>
		</div>
		<div class='col-md-7'>		
		<center><h5 style="color:green;" id="mes" ><?php echo $this->session->flashdata("message"); ?></h5></center>
		</div>
     </div>
 <label style="background-color:#e4e4e4;width:100%;height:1px"></label>
 <div class="portlet-body form">
 <form  class="form-horizontal" role="form" method="post" action="<?php echo site_url("Staff/add_new_staff");?>" enctype="multipart/form-data">
                    <!-- text input -->
<div class="box-body">

<div class='row'>
<div class='col-md-12'>

<div class='col-md-8'>
		  
		  <div class="form-group">
		   <label class="col-md-4 control-label">Start Date</label>
				<div class="col-md-8">
					<input type="text" class="form-control" name="sdate" id='datepicker1' value='<?php echo date('d-m-Y');?>' required>
		  </div>
		  </div> 
		  <div class="form-group">
		   <label class="col-md-4 control-label"> Name</label>
				<div class="col-md-8">
					<input type="text" class="form-control" name="name" required>
			  </div>
			  </div>
			  
		<div class="form-group">
		 <label class="col-md-4 control-label">Address</label>
			<div class="col-md-8">
			   <textarea rows=2 cols=50 class="form-control"  name="address"  required></textarea>
			   </div>
		  </div>
		  
		 <div class="form-group">
		 <label class="col-md-4 control-label">Mobile</label>
			<div class="col-md-8">
			    <input type="number" class="form-control" name="mobile" id="mobile" required>
			   </div>
		  </div>

				
				  	 
		  
				<div class="form-group" style="padding-top:10px;">
					 <label class="col-md-4 control-label" style='color:blue;padding-top:5px;'>Staff Role</label>
					 <div class="col-md-8">
					 <select name='srole' class="form-control" id='srole' required>
					 <option value=''>-----</option>
					 <?php
				foreach($result as $r1)
						   {
				echo "<option value='". $r1->role_id."'>".$r1->role_name."</option>";
						   }
						   ?>
						   </select>
					 </div>
				  </div>
				  
				  <div class="form-group">
					 <label class="col-md-4 control-label" style='padding-top:5px;'>Profession</label>
					 <div class="col-md-8">
				    <select name='position' class="form-control" required>
					 <option value=''>-----</option>
					  <?php
				foreach($result1 as $r1)
						   {
				echo "<option value='". $r1->profession_id."'>".$r1->profession_name."</option>";
						   }
						   ?>
					 </select>
					 </div>
				  </div>
					 
				  <div class="form-group">
					 <label class="col-md-4 control-label" style='padding-top:5px;'>Date of birth</label>
					 <div class="col-md-8">
						 <input type="date" class="form-control" name="bdate" required>
					 </div>
				  </div>
					 
					 <div class="form-group" style='padding-top:10px;'>
					 <label class="col-md-4 control-label" style='padding-top:5px;' >Gender</label>
					 <div class="col-md-8">
						 <input type="radio" name="gender" value='Male' checked>Male
						 <input type="radio" name="gender" value='Female'  style='margin-left:15px;'>Female
					 </div>
				     </div> 
								 
				 <div class="form-group" style='margin-bottom:8px;'>
				  <label class='col-md-4 la-right' style='margin-top:0xp;'>Email</label>	
					 <div class="col-md-8">
						 <input type="email" class="form-control" name="email" required>
					 </div>
				  </div>
				  
				   	 
				  <div class="form-group" style='margin-bottom:8px;' id='pword'>
				   <label class='col-md-4 la-right' style='margin-top:0xp;'>Password</label>
					 <div class="col-md-8">
						 <input type="password" class="form-control" name="password" id='password'  required>
					 </div>
				</div>
					 
					 <div class="form-group" style='padding-top:10px;'>
					 <label class="col-md-4 control-label" style='padding-top:5px;'>Blood Group</label>
					 <div class="col-md-8">
						 <select name='bgroup' class="form-control" required>
						 <option value=''>-----</option>
						 <option value='A+'>A+</option>
						 <option value='B+'>B+</option>
						 <option value='A-'>A-</option>
						 <option value='B-'>B-</option>
						 <option value='AB+'>AB+</option>
						 <option value='AB-'>AB-</option>
						 <option value='O+'>O+</option>
						 <option value='O-'>O-</option>
						 </select>
					 </div>
				     </div> 
		  
		  	
					<div class="form-group" style='margin-bottom:8px;'>
					<label class='col-md-4 la-right' style='margin-top:0xp;'>Guardian Name</label>
					 <div class="col-md-8" >
						 <input type="text" class="form-control"  name="guardian" id="guardian" required>
					 </div>
				   </div>
		  
				
					<div class="form-group" style='margin-bottom:8px;'>
					<label class='col-md-4 la-right' style='margin-top:0xp;'>Contact Number</label>
					 <div class="col-md-8">
						 <input type="number" class="form-control" name="contactno" id="contactno" required>
					 </div>
				  </div>
				  
		  
					
					 <div class="form-group">
					 <label class="col-md-4 control-label">Aadhar Card No</label>
					 <div class="col-md-8">
														   
						 <input type="text" class="form-control" name="aadhar" id="aadhar" required>
						<!--  <label id="mobmessage" style="color:red;"></label> -->                              
					 </div>
					 </div>
                        
                        <div class="form-group">
                      <label class="col-md-4 control-label">Pan Card No</label>
                        <div class="col-md-8">
                                                   
						<input type="text" class="form-control" name="pancard" id="pancard" required>
                        <!-- <label id="landmessage" style="color:red;"></label> -->                            
                        </div>
                      </div>

					  <div class="form-group" style='margin-top:20px;' >
                      <label class="col-md-4 control-label">Bank Name</label>
                        <div class="col-md-8">
							<input type="text" class="form-control" name="bankname" required>
                        </div>
                      </div>
                    
                     <div class="form-group">
                      <label class="col-md-4 control-label">Bank Account No</label>
                        <div class="col-md-8">
							<input type="text" class="form-control" name="account" required>
                        </div>
                     </div>
					 		 
					 
					 <div class="form-group">
                      <label class="col-md-4 control-label">Bank IFSC Code</label>
                        <div class="col-md-8">
							<input type="text" class="form-control" name="ifsc" required>
                        </div>
                     </div>
	    
				  
				  <div class="form-group" style='margin-bottom:10px;'>
				  <label class='col-md-4 la-right' style='margin-top:0xp;'>Basic Salary</label>
					 <div class="col-md-8">
						 <input type="number" class="form-control" name="bsalary" required>
					 </div>
				  </div>
					 
					 
					 
<!--<label style="color:blue;font-size:13px;margin-left:50px;"> Scan Your (Certificates,Licence,Id Card) and create a Zip file.<br> Then select file and upload. </label>-->

<label style="color:blue;font-size:13px;margin-left:50px;"> Upload Your scanned copy of Certificates,Licence and Id Card.</label>

<!---  attache your proof ----------------------------------->
					
					<div class='form-group'> 			
					
					  <div class="row">
					  <label class='col-md-3 control-label' style='padding-top:3px;'> </label>
					  
					  <div class="control-group col-md-6" id="fields" >
						  <div class="controls" >
						   	  <div class="entry input-group col-xs-3" style='margin-top:5px;'>
								<input class="btn btn-primary" name="files[]" type="file">
								<span class="input-group-btn">
							  <button class="btn btn-success btn-add" type="button">
									<span class="glyphicon glyphicon-plus"></span>
								</button>
								</span>
							  </div>
						 
						</div>
					  </div>
					</div>
					</div>

		  
</div>
		<div class="col-md-3">
			<div class="form-group" >
			  <!-- <label class="col-md-3 control-label">Photo</label>-->
			   <div class="col-md-12">
			   <center>
				  <div class="fileinput fileinput-new" data-provides="fileinput">
					 <div class="fileinput-new thumbnail" style="width: 130px;height:135px;">
						 <img src="<?php echo base_url("assets/dist/img/user.jpg");?>" alt=""/> </div>
						 <div class="fileinput-preview fileinput-exists thumbnail" style="width:130px; height:135px;"> </div>
					 <div>
					  <span class="btn btn-default btn-file">
					  <span class="fileinput-new"> Select </span>
					  <span class="fileinput-exists"> Change </span>
					  <input type="file" name="photo" required> </span>
					  <a href="javascript:;" class="btn btn-default fileinput-exists" data-dismiss="fileinput"> Remove </a>
				 </div>
				</div>
				</center>
			</div>
		</div>
		</div>
</div>
</div>

  

<label style="width:100%;height:1px;background-color:#e4e4e4;"></label>

	  <div class="form-group">
	  <label class='col-md-3'></label>
		   <div class="col-md-8">
				<button type="submit" class="btn btn-success" style="padding:5px 20px 5px 20px;" >Save Details</button>
		   </div>
	  </div> 
</div>
 
</form>
  </div>	
</div>   
</div>
</section>
</div>

<?php include("application/views/common/footer.php");?>
 

 <script type="text/javascript">
	 
	 //sweet alert box ----------------------
//$("#mes").hide();
 var msg=$("#mes").html().split("#");
  
  if($("#mes").html()!="")
  {
	  var msg=$("#mes").html().split("#");
	if(msg[0]=='1')
	  swal("Saved.!",msg[1],"success");
	else if(msg[0]=='2')
	  swal("Updated.!",msg[1],"success");
    else if(msg[0]=='3')
	  swal("Removed.!",msg[1],"success");
	else if(msg[0]=='4')
	  swal("Try Again.",msg[1],"error");
     $("#mes").html("");
  }
  
  
//------------------------------------
 
 $("#srole").change(function()
 {
	 if($(this).val()=='4')
	 {
		 $("#pword").hide();
		 $("#password").val("12345");
	 }
	 else
	 {
		 $("#pword").show();
		 $("#password").val("");
	 }
	 
 });
 
 $('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});


$('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});
 
 
 
	 
 $("#mobmessage").hide();
 $("#landmessage").hide(); 
	 
  function checkdata()
  {
    
    var mob=$("#mobile").val();
    var landline=$("#landline").val();
      
  
    if(mob.length<10 || mob.length>10)
    {
	  $("#mobmessage").show();
      $("#mobmessage").html("Invalid mobile no,10 digits only.");
      return false;
    }
	
    /*else if(landline.length<11 || landline.length>11)
    {
	  $("#mobmessage").hide();
      $("#landmessage").show();
      $("#landmessage").html("Invalid mobile no,11 digits only.");
      return false;
    }*/
    
    else
    {
	  $("#mobmessage").hide();
      $("#landmessage").hide();
      $("#mobmessage").html("");
      $("#landmessage").html("");
      return true;
    }
  }

</script> 

<script>
$(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});
</script>

 
 