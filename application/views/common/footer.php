
   <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-body" >
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Add</h4>
            </div>
          </div>
          </div>
        <!-- /.modal-dialog -->
        </div>
	</div>
  
    <footer class="main-footer">
	<div class="pull-right">
		<!--<label style="font-size:11px;">Powered By : <a href="www.webqua.com">Webqua</a></label> -->
    </div>
    <!-- Default to the left -->
		<label style="font-size:12px;color:#3289e2;">&copy; Copyright  2016 -&nbsp;Advance World Holidays</label>
		<label style="font-size:12px;float:right;color:#3289e2;padding-right:15px;"><a href="http://www.advanceworldholidays.com/">www.advanceworldholidays.com</a></label>
		
	</footer>

  <script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
  <script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js')?>" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js')?>" type="text/javascript"></script>
  <!-- Bootstrap 3.3.2 JS -->
  <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script> 
     <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url('assets/dist/js/demo.js');?>" type="text/javascript"></script> 
  <script src="<?php echo base_url('assets/dist/js/jasny-bootstrap.min.js');?>"></script> 
  <script src="<?php echo base_url('assets/dist/js/holder.js');?>"></script>
       <!-- FastClick -->
  <script src="<?php echo base_url('assets/plugins/fastclick/fastclick.min.js');?>"></script>
       <!-- AdminLTE App -->
  <script src="<?php echo base_url('assets/dist/js/app.min.js');?>" type="text/javascript"></script>  
  <script src="<?php echo base_url('assets/dist/js/jquery.modal.js');?>"></script> 
<!--select dropdown control ------>
  <script src="<?php echo base_url('assets/common/search_tabs/selectize.js');?>" type="text/javascript"></script> 
  <script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/plugins/chartjs/Chart.min.js');?>" type="text/javascript" ></script>
  <script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js');?>" type="text/javascript"></script>
  <script src="<?php echo base_url('js/jquery.json-2.4.min.js');?>" type="text/javascript"></script> 
 <!-- <script src="<?php echo base_url('js/bootstable.js');?>" type="text/javascript"></script> -->
  <script src="<?php echo base_url('assets/dist/js/sweetalert.min.js');?>" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/dist/js/select2.min.js');?>" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/datatables/datatables.checkboxes.js');?>"></script>

  <script type="text/javascript">
  
  $("#hdr_ifr").hide();  //to hide extra running script/iframe on server.
  $("#chk_frame").hide();
  
  
   function isNumber(evt) {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      //alert(charCode)
      if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105))
       return false;

     return true;
   }
   
   var ames=$("#accmes").val();
   if(ames!="")
   {
	   swal("Thank You.!",$("#accmes").val(),'success');
	   $("#accmes").val("");
   }

   
  //pre-loading----------------------
     
 var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 1000);
}

function showPage() {
    document.getElementById("loading").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}
  //----------------------------
</script>
</body>
</html>