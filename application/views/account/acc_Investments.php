<?php include('application/views/common/header.php'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1 class='heading' style='color:#00adee; padding-bottom:15px; font-weight:bold;'>Investments </h1>
      
      <ol class="breadcrumb" style="margin-right:20px;font-size:13px;">
        <li><a href="#"></a></li>
      </ol>
	   
    </section>

    <!-- Main content -->
    <section class="content">
   
      <div class="row">
        <div class="col-md-5">
          <div class="box" style="border:1px solid #f5f3f3;">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Add Details</h3> 
				<center><input type='hidden' id="msg" value='<?php echo $this->session->flashdata('message'); ?>'></CENTER>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
	            <div class="col-md-12">
				 <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Investment/save_investment')?>" enctype="multipart/form-data">
						
						<div class="form-group" style='margin-top:15px;'>
						   <div class="row">
							  <label class="col-md-4 control-label">Date :</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="datepicker1" name="idate" required>
								</div>
							</div>
						</div>
						
						<div class="form-group" style='margin-top:15px;'>
						   <div class="row">
							  <label class="col-md-4 control-label">From :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="iname" required>
								</div>
							</div>
						</div>
										
						<div class="form-group" style='margin-top:15px;'>
						   <div class="row">
							  <label class="col-md-4 control-label">Amount :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="iamount" required>
								</div>
							</div>
						</div>
						
						<div class="form-group" style='margin-top:20px;margin-bottom:20px;'>
						   <div class="row">
							  <label class="col-md-4 control-label"></label>
								<div class="col-md-7">
									<input type="submit" class="btn btn-primary "  name="btnsubmit" value="Save Data">
								</div>
							</div>
						</div>
				</form>
				
			    </div>
        </div>

       </div>
    </div>
   </div>
   <!-- end create group---->
   <!-- group list ---->
    <div class="col-md-7">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-arrow-circle-right " aria-hidden="true"></i>&nbsp; Investments</h3> 
				
            </div>
            <!-- /.box-header -->
            <div class="box-body" style='min-height:450px;'>
              <div class="row">
	            <div class="col-md-12">
				<table class="table table-hover table-bordered" id="example" width='100%' border=0 style="font-size:14px" >
                <thead>
                <tr>
				<th width='8%'>Action</th>
                 <th width='10%'>ID</th>
				  <th>Date</th> 
                 <th>Ledger_Name</th> 
				 <th style='text-align:right;'>Amount</th> 
                 </tr>
                 </thead>
				 <tbody>
					<?php
					$ulevel=$this->session->userdata('userlevel');
					
					foreach($cresult as $r)
					{
						//$edit="<a href='#' id='".$r->entry_id."' class='abtn abtn-edit edit' data-toggle='modal' data-target='#editModal'><span class='glyphicon glyphicon-edit' aria-hidden='true' style='font-size:16px;'></span></a>";
						$del=anchor('Investment/delete_investment/'.$r->entry_id,'<span class="glyphicon glyphicon-trash" aria-hidden="true" style="font-size:14px;color:red;"></span>', array('id' =>'del_conf'));
						
						if($r->entry_debit!=0)
							$amt=$r->entry_debit;
						else
							$amt=$r->entry_credit;
						
						if($ulevel==1)
						{
							$edit1=$del;
						}
						else
						{
							$edit1="";
						}
						
						
					?>
						<tr>
						 <td align='center' class='dtable'> <?= $edit1 ;?>	 </td>
						 <td><?=$r->entry_id;?></td>
						  <td><?=date_format(date_create($r->accu_date),'d-m-Y');?></td> 
						 <td>Capital Amount</td>
						 <td style='text-align:right;'><?=number_format($amt,2,'.',"");?></td> 
						 </tr>
					<?php
					}
					?>	
				</tbody>
				</table>
			  </div>
        </div>
       </div>
    </div>
   </div>
  
</div>
</section>
</div>
<!---modal dialog start ---->

<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document" >
    <div class="modal-content" style='top:50px;border:1px solid #b7b7b7;'>
      <div class="modal-body"  style='padding:0px;' >
	  </div>
	    
	  
    </div>
  </div>
</div>
          
  <!-- /.content-wrapper -->
  
<?php include('application/views/common/footer.php'); ?>
   
<script type="text/javascript">

$("#example").dataTable({"ordering":false});

//sweet alert start-----------------------
 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
   
var msg=$("#msg").val().split('#');
if(msg[0]=='1')
{
	swal('',msg[1],'success'); //Saved
}

else if(msg[0]=='2')
{
	swal('',msg[1],'success'); //Updated
}
else if(msg[0]=='3')
{
	swal('',msg[1],'success'); //Removed.
}
else if(msg[0]=='4')
{
	swal('',msg[1],'error'); //Try Again.
}

//sweet alert end -----------------------
	$('#example tbody').on('click', '.edit', function () {
        var Result=$("#editModal .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
        var id =  $(this).attr('id');
		
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Account/edit_group",
        dataType: 'html',
        data: {gid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 
		

      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

</script>  
