<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

 <table width="100%"><tr><td><h4>View All Staffs </h4></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
		<table style="width:100%;border:1px solid #e4e4e4;" cellpadding=5 cellspacing=0>
		   <thead>
			<tr>
			   <tr style='color:#4b88ed;border:1px solid #e4e4e4;'>
			     <th class='th1'>Slno</th>
				<th class='th1'>Staff Code</th>
				 <th class='th1'>Joining Date</th>
				 <th class='th1'>Staff Name</th>
				  <th class='th1'>Staff Image</th>
                  <th class='th1'>Address</th>
				 <th class='th1'>Profession</th>
				 <th class='th1'>Date Of Birth</th>
				 <th class='th1'>Gender</th>
				 <th class='th1'>Blood Group</th>
				 <th class='th1'>Mobile</th>
				 <th class='th1'>Email</th>
				 <th class='th1'>Basic Salary</th>
				 <th class='th1'>Aadhar Card No</th>
				 <th class='th1'>Pan Card No</th>
				 <th class='th1'>Bank Name</th>
				 <th class='th1'>Bank Account</th>
				 <th class='th1'>Bank IFSC No</th>

				</tr>
			</tr>
		</thead>
		<tbody>
			<?php
			$slno=1;
				foreach ($results as $key => $value) {
					
				?>
				<tr >
				<td class='td1'><?=$slno;?> </td> 
				<td class='td1'><?=$value->staff_code;?> </td> 
				<td class='td1'><?=$value->staff_startdate;?></td>
				<td class='td1'><?=$value->staff_name;?></td>
				<td class='td1'><img src='<?=$value->staff_image;?>' width='85px' height='90px'>   </td>
				<td class='td1'><?=$value->staff_address;?> </td>
				<td class='td1'><?=$value->profession_name;?> </td>
				<td class='td1'><?=$value->staff_dob;?> </td>
				<td class='td1'><?=$value->staff_gender;?> </td>
				<td class='td1'><?=$value->staff_bloodgroup;?> </td>
				<td class='td1'><?=$value->staff_mobile;?> </td>
				<td class='td1'><?=$value->staff_email;?> </td>
				<td class='td1'><?=$value->staff_basicsalary;?> </td>
				<td class='td1'><?=$value->staff_aadharcard;?> </td>
				<td class='td1'><?=$value->staff_pancard;?> </td>
				<td class='td1'><?=$value->staff_bankname;?> </td>
				<td class='td1'><?=$value->staff_bankaccount;?> </td>
				<td class='td1'><?=$value->staff_ifsc;?> </td>

			</tr>
				<?php
				
				$slno++;
				}
				?>
		  
		</tbody>
		</table>
</body>
</html>