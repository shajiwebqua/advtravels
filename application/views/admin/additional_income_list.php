<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
 <!-- Main content -->
 <!-- Content Header (Page header) -->

  <section class="content-header">
    <h1><b>Additional Income list</b> </h1>
    <ol class="breadcrumb" style='font-size:14px;'>
	<li><a  href="<?php echo site_url('General/Incomes') ?>" style='color:#4b88ed;'><button class='btn btn-primary' style='margin-bottom:2px;'><i class="fa fa-plus" aria-hidden="true"></i> Add Incomes</button></a></li>
	<li><a href="<?php echo base_url('Reports/Incomes');?>" style='color:#4b88ed;'><button ><i class="fa fa-print" aria-hidden="true"></i> Print </button></a></li>
    </ol> 
  </section>
  
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 	  
  <section class="content"> 
<!--   <div style="color:blue;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Print,Share,Save,View,Edit,Delate)  </div> -->
		<div style="padding:2px 0px 2px 0px;">
		    <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
			 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
			</div>
		</div>
		
	
<div class="row">
<div class="col-md-12">

<!------------------------------------ Add new user details ----------------------------------->

<div style="background-color:#fff;padding:15px; ">
							
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" >
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
                      <div class="row">
                      <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered" >
                               
                                <div class="portlet-body" >
								
								
								
	<div class='row' style=' background-color:#e4e4e4;margin:0px;padding:5px;'>
	<form onsubmit='return checkdata1();' method="POST" action="<?php echo base_url('General/View_Income/1')?>" enctype="multipart/form-data">
	<label class='col-md-2 la-right'>Select Date:</label>
	<div class='col-md-3' style='padding:0px;'>
			<div class='row'>
				<div class='col-md-6' >
				<input class='form-control' data-provide="datepicker" id="datepicker1" name='fdate'>
				</div><div class='col-md-6' >
				<input class='form-control' data-provide="datepicker" id="datepicker2" name='sdate'>
				</div>
			</div>
	</div>
	<div class='col-md-1' style='padding:2px;margin-left:10px;'>
	<button type='submit' name='btnget1' >Get</button>
	</div>
	</form>
	<form onsubmit='return checkdata2();' method="POST" action="<?php echo base_url('General/View_Income/2')?>" enctype="multipart/form-data">
	<label class='col-md-2 la-right' >Select Month:</label>
	<div class='col-md-2'>
		<select name='mon' id='mon' class='form-control'>
		  <option value=''> ------------</option>
		  <option value='1'> January</option>
		  <option value='2'> February</option>
		  <option value='3'> March</option>
		  <option value='4'> April</option>
		  <option value='5'> May</option>
		  <option value='6'> June</option>
		  <option value='7'> July</option>
		  <option value='8'> August</option>
		  <option value='9'> September</option>
		  <option value='10'> October</option>
		  <option value='11'> Novenber</option>
		  <option value='12'> December</option>
		  </select>
	</div>
	<div class='col-md-1' style='padding:2px;margin-left:10px;'>
	<button type='submit' name='btnget2' >Get</button>
	</div>
	</form>
	</div>
					
								
           <div class="row" style='margin-top:15px;'>
              <div class="col-md-12">
									<h4 style='margin-top:0px;margin-bottom:3px;'><?php echo $this->session->flashdata('inchead'); ?></h4>
									</div>
									<label style="background-color:#e4e4e4;width:100%;height:1px;margin-top:0px;margin-bottom:10px;"></label>
								</div>
					  <table class="table table-striped table-hover table-bordered" id="example" width='100%' style='font-size:14px;'>
							<thead>
								<tr >
								 <th>Del</th>
								 <th>ID</th>
								 <th>Date</th>
								 <th>Income_Type</th>
								 <th>From</th>
								 <th style='text-align:right;'>Amount</th>
								 <th>Narration</th>
								 </tr>
							</thead>
							
							<tbody>
							<?php
							if(isset($addiinc))
							{
								$gtotal=0;
							foreach($addiinc as $r1)
							{
							?>
							<tr>
								 <td width='50px' align='center'><a href="<?php echo base_url('General/del_income/'.$r1->addi_inc_id);?>" id='del_conf'><button class='btn btn-danger btn-xs'><i class="fa fa-trash-o" aria-hidden="true"></i></button></a></td>
								 <td width='80px'><?=$r1->addi_inc_id;?></td>
								 <td width='100px'><?=date_format(date_create($r1->addi_inc_date),"d-m-Y");?></td>
															 
								 <td width='150px'><?=$r1->inc_type_name;?></td>
								  <td width='150px'><?=$r1->addi_inc_from;?></td>
								 <td align='right' width='120px'><b><?=number_format($r1->addi_inc_amount,"2",".","");?></b></td>
								 <td ><?=$r1->addi_inc_narration;?></td>
								 </tr>
							<?php 
							$gtotal+=$r1->addi_inc_amount;
							} 
							}?>
							
							</tbody>
							<footer>
								<tr style="color:#5068f8;font-size:18px;height:35px;">
								  <th colspan='6' style='text-align:right'>Total : &nbsp;&nbsp;&#8377;&nbsp;&nbsp <b><?=number_format($gtotal,"2",".","");?></b></th>
								 <th></th>
								 </tr>
							</footer>
							
                        </table>
						
						<!--<div class='row' >
							<div class='col-md-9'>
						    <table width='100%'><tr style='font-size:24px;height:35px'>
								 <td align='right'>Total : &nbsp;&nbsp;&nbsp;&#8377;&nbsp;&nbsp</td>
								 <td width='25%'><b><?=number_format($gtotal,"2",".","");?></b></td>
							</tr>
							</table>
							</div>
						</div> -->
						
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>


  <!-- End user details -->
  </div>
  
   </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

 <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            </div>
		 </div>


<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 
 $('#example').DataTable({
	 "ordering":false,
 });

//sweet alert ----------------------------
	$("#idmsg").hide();
	if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
	
// sweet alert box -------------------------- 
 
 $('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});

$('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});

function checkdata1()
{
	var dt1=$('#datepicker1').val();
	var dt2=$('#datepicker2').val();
	
	if(dt1=="" || dt2=="")
	{
		alert("Please select dates.");
		return false;
	}
	else
	{
	return true;
	}
}

function checkdata2()
{
	var mon=$('#mon').val();
	if(mon=="")
	{
		alert("please select month.");
		return false;
	}
	else
	{
	return true;
	}
}

//pre-loading----------------------
var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 1000);
}

function showPage() {
    document.getElementById("loading").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}
//------------------------
  /*   $('#view-pdf').on('click',function(){
       var slno=$('#example').find('tr.selected').find('span.sln').text();
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
    var pdf_link = $(this).attr('href')+'/'+slno;
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Given Enquiry',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
        return false
    });*/

	
    $(document).on("click", "#del_conf", function () 
	{
            return confirm('Are you sure you want to delete this entry?');
        });

 //});
    


</script>



