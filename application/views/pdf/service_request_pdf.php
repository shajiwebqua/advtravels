<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<title>Travels</title>
	<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" /> 
	<style type="text/css">
		
	table{
		font-size: 14px;
	}
		td{
			padding:7px 5px 7px 5px;

		}

		th{
			padding:7px 5px 7px 5px;
			
		}

		.center {
    		margin: auto;
    		width: 25%;
		}
		.tbl tr
		{
			height:40px;
		}
	</style>
</head>
 <body>
  <table style="border:none;width:100%;">
		<tr>
			<td colspan=2 style="padding: 0px">
				<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" height='35px'>
			</td>
		</tr>
		<tr>
			<td style="padding:0px">
				<span style="font-size:15px;">Advance World Holidays</span><br>
				<span>PK & Sons Complex, East Moozhikkal</span><br>
				<span>NH 212, Calicut, Kerala, India - 673571</span><br>
				<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
				<span>Fax: 0091 495 2732 400</span>
			</td>
			<td width='150px' valign='top' align='right'>Date: <?php echo date('d-m-Y');?></td>
		</tr>
	</table>
	<hr style='margin-bottom:2px;'>
	 <table width="100%"><tr><td width='220px;'><h4>Vehicle Service Request Report</h4></td></tr>
 </table>
 <hr style='margin-top:2px;'>
 

 <table width="100%" class='tbl'>
       
        <tbody>
        <?php $i=1;
		foreach($srresult1 as $row){
			$d=$row->srequest_date;
			$d1=explode("-",$d);
			$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
			
			$d=$row->srequest_datetime;
			$d1=explode("-",$d);
			$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
			
			?>
            <tr ><td width='25%'>Request ID</td><td>:&nbsp;&nbsp;<?=$row->srequest_id;?></td></tr>
			<tr ><td>Request Date</td><td>:&nbsp;&nbsp;<?=$dt1;?></td></tr>
			<tr ><td>Vehicle</td><td>:&nbsp;&nbsp;<b><?=$row->vehicle_regno?> (<?=$row->veh_type_name?>)</b></td></tr>
			<tr ><td>Servie </td><td>:&nbsp;&nbsp;<?php if($row->srequest_category==1)echo "Toyota Servicing"; else echo "General";?></td></tr>
			<tr ><td>Expected Date</td><td>:&nbsp;&nbsp;<?=$dt1?></td></tr>
			<tr><td>Expected Duration</td><td>:&nbsp;&nbsp;<?= $row->srequest_duration?></td></tr>
			<tr><td>Service Centre</td><td>:&nbsp;&nbsp;<?= $row->srequest_workshop?></td></tr>
			
			<tr><td colspan="2"  style='padding:15px;'><b>Service Parts details</b></td></tr>
			<tr><td colspan="2">
			
				<table width='80%' border=1>
				<tr><th width='10%' style='padding:3px 3px 3px 5px;'>Slno</th>
				<th style='padding:3px 3px 3px 5px;'>Particulers</th>
				<th width='20%' style='padding:3px 3px 3px 5px;'>Price (Approximate)</th></tr>
				<?php $res = $this->db->where('srequest_id',$row->srequest_id)->get('service_requirements')->result();
				$slno=1;
				$tot=0;
				 foreach($res as $row1)
				{
					
                  echo "<tr height='30px'><td style='padding:3px 3px 3px 5px;'>".$slno."</td>";
				  echo"<td style='padding:3px 3px 3px 5px;'>".$row1->parts."</td>";
				  echo "<td style='padding:3px 5px 3px 5px;text-align:right;'> &#8377;&nbsp;".number_format($row1->price,"2",".","")."</td></tr>";
				  $slno++;
				  $tot+=$row1->price;
                }
				?>
				<tr height='50px'><td colspan="3" style='padding:3px 5px 3px 5px;text-align:right;font-size:18px;'><b>Total :&nbsp;&nbsp;&nbsp;&#8377;&nbsp;<?=number_format($tot,"2",".","");?></b></td>
				</table>
			
			</td></tr>
				
			</table>			
         <?php $i++;} ?>   
		 
			<table width='80%'>
				<tr height="60px;"><td style='padding:3px 3px 3px 5px;'> Approved By :</td></tr>
				<tr height="50px;"><td style='padding:3px 15px 3px 5px;text-align:right;'> For Advanced Travels</td></tr>
				<tr ><td></td></tr>
				<tr><td></td></tr>
				<tr height="50px;"><td style='padding:3px 15px 3px 5px;text-align:right;'> (Signature)</td></tr>
			</table>
        </tbody>
    </table> 
 
</body>
</html>
