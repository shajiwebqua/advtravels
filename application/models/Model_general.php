<?php
class Model_general extends CI_Model{
	
    public function insert_country($log)
    {
       $this->db->insert('country', $log);  
    }
	
    public function insert_general_expense($log)
    {
       $this->db->insert('general_expense', $log);  
    }

	public function insert_additional_incomes($log)
    {
       $this->db->insert('additional_incomes', $log);  
    }
	
    public function view_country()
    {
    	$this->db->select('*');
		$this->db->from('country');
		$query = $this->db->get();
		return $query->result();
    }

    public function update_country($id,$newdt)
    {
    	$this->db->where('country_id', $id);
		$this->db->update('country', $newdt); 
    }
	
	public function delete_country($id)
    {
		$this->db->where('country_id', $id);
		$this->db->delete('country');
		return $this->db->affected_rows();
    }

    /*Area*/

    public function insert_area($log)
    {
       $this->db->insert('area', $log);  
    }

    public function view_area()
    {
		$this->db->select('*');
		$this->db->from('area');
		$this->db->join('country',"country.country_id=area.area_countryid","inner");
		$query = $this->db->get();
		return $query->result();
    }
    
	public function view_areas1($mid)
    {
		$this->db->select('*');
		$this->db->from('area');
		$this->db->where('area_countryid', $mid);
		$query = $this->db->get();
		return $query->result();
    }
 
    public function update_area($id,$usr_newdt)
    {
		$this->db->where('area_id', $id);
		$this->db->update('area', $usr_newdt); 
    }
	
    public function delete_area($id)
    {
		$this->db->where('area_id', $id);
		$this->db->delete('area');
		return $this->db->affected_rows();
    }
	
	/*category*/

    public function insert_category($log)
    {
		$this->db->insert('category', $log);  
    }

    public function view_category()
    {
		$this->db->select('*');
		$this->db->from('category');
		$query = $this->db->get();
		return $query->result();
    }

    public function update_category($id,$usr_newdt)
    {
		$this->db->where('category_id', $id);
		$this->db->update('category', $usr_newdt); 
    } 
	
    public function delete_category($id)
    {
		$this->db->where('category_id', $id);
		$this->db->delete('category');
		return $this->db->affected_rows();
    }

	
	function view_farelist()
	{
		$this->db->select('*')->from("fare_list");
		$this->db->join("vehicle_types","vehicle_types.veh_type_id=fare_list.fare_vehicle","inner");
		$this->db->join("customers","customers.customer_id=fare_list.fare_corporate","inner");
		$this->db->join("category","category.category_id=fare_list.fare_trip","left");
		$this->db->order_by("fare_id","desc");
		$query=$this->db->get();
		return $query->result();
	}

    //profession
     public function insert_profession($log)
    {
       $this->db->insert('profession', $log);  
    }

    public function view_profession()
    {
    $this->db->select('*');
    $this->db->from('profession');
    $query = $this->db->get();
    return $query->result();
    }

    public function update_profession($id,$usr_newdt)
    {
    $this->db->where('profession_id', $id);
    $this->db->update('profession', $usr_newdt); 
    } 
    
    public function delete_profession($id)
    {
        $this->db->where('profession_id', $id);
    $this->db->delete('profession');
    return $this->db->affected_rows();
    }

//role
 public function insert_role($log)
    {
       $this->db->insert('role', $log);  
    }

    public function view_role()
    {
    $this->db->select('*');
    $this->db->from('role');
    $query = $this->db->get();
    return $query->result();
    }

   public function update_role($id,$usr_newdt)
    {
    $this->db->where('role_id', $id);
    $this->db->update('role', $usr_newdt); 
    } 
    
    public function delete_role($id)
    {
        $this->db->where('role_id', $id);
    $this->db->delete('role');
    return $this->db->affected_rows();
    }

//agents
 public function insert_agent($values)
    {
       $this->db->insert('agents', $values);  
    }

    public function view_agents()
    {
    $this->db->select('*');
    $this->db->from('agents');
    $query = $this->db->get();
    return $query->result();
    }

   public function update_agent($id,$agent)
    {
    $this->db->where('agent_id', $id);
    $this->db->update('agents', $agent); 
    } 
    
    public function delete_agent($id)
    {
        $this->db->where('agent_id', $id);
		$this->db->delete('agents');
		return $this->db->affected_rows();
    }
	
//Barnches

public function insert_branch($log)
    {
       $this->db->insert('branches', $log);  
    }

 public function view_branches()
    {
    $this->db->select('*');
    $this->db->from('branches');
    $query = $this->db->get();
    return $query->result();
    }

 public function update_branch($id,$log)
    {
    $this->db->where('branch_id', $id);
    $this->db->update('branches', $log); 
    } 
    
 public function delete_branch($id)
    {
    $this->db->where('branch_id', $id);
    $this->db->delete('branches');
    return $this->db->affected_rows();
    }
	
public function view_agentsDT($id)
{
    $this->db->select('*');
    $this->db->from('agents');
    $this->db->where('agent_id',$id);
	$query = $this->db->get();
    return $query->result();
}


 public function view_vehicletypes()
    {
    $this->db->select('*');
    $this->db->from('vehicle_types');
	$this->db->order_by("veh_type_id","desc");
	$query = $this->db->get();
    return $query->result();
    }
	
	public function delete_vtype($id)
    {
        $this->db->where('veh_type_id', $id);
		$this->db->delete('vehicle_types');
		return $this->db->affected_rows();
    }
	
	public function update_vtype($id,$log)
    {
    $this->db->where('veh_type_id', $id);
    $this->db->update('vehicle_types', $log); 
    } 
		
	public function delete_drv_duty($id)
    {
        $this->db->where('drv_id', $id);
		$this->db->delete('driver_schedule');
		return $this->db->affected_rows();
    }
	
	public function delete_fare_list($id)
    {
        $this->db->where('fare_id', $id);
		$this->db->delete('fare_list');
		return $this->db->affected_rows();
    }
	
	//vehicle expense ---------------------
	
	 public function view_expensetypes()
    {
    $this->db->select('*');
    $this->db->from('expense_type');
	$this->db->order_by("etype_id","desc");
	$query = $this->db->get();
    return $query->result();
    }
	
	public function delete_exptype($id)
    {
        $this->db->where('etype_id', $id);
		$this->db->delete('expense_type');
		return $this->db->affected_rows();
    }
	
	public function update_exptype($id,$log)
    {
    $this->db->where('etype_id', $id);
    $this->db->update('expense_type', $log); 
    } 
	
	public function get_driver_perform($d1,$y1)
    {
	//$qry="SELECT driver_id,trip_driver_id,SUM(trip_startkm),SUM(trip_endkm) FROM driverregistration inner join trip_management on trip_management.trip_driver_id=driver_id group by driver_id, trip_driver_id";
    $this->db->select('trip_driver_id,trip_drivername,SUM(trip_endkm) as endkm, SUM(trip_startkm) as startkm,SUM(trip_gtotal) as gtotal');
    $this->db->from('trip_management');
	$this->db->join('driverregistration',"driverregistration.driver_id=trip_management.trip_driver_id","inner");
	//$this->db->where('MONTH(trip_startdate)',date('m'));
	//$this->db->where('MONTH(trip_enddate)',7);
	$this->db->where('MONTH(trip_enddate)',$d1);
	$this->db->where('YEAR(trip_enddate)',$y1);
	$this->db->group_by("trip_driver_id,trip_drivername");
	$this->db->order_by("trip_driver_id","asc");
	$query = $this->db->get();
    return $query->result();
    }
	
	public function get_perform_driver_names()
    {
    $this->db->select('*');
    $this->db->from('driverregistration');
	$this->db->order_by("driver_id","asc");
	$this->db->group_by("driver_id");
	$query = $this->db->get();
    return $query->result();
    }
	
	public function get_driver_perform_numrows($d1,$y1)
    {
    $this->db->select('trip_driver_id,trip_drivername,SUM(trip_endkm) as endkm, SUM(trip_startkm) as startkm,SUM(trip_gtotal) as gtotal');
    $this->db->from('trip_management');
	$this->db->join('driverregistration',"driverregistration.driver_id=trip_management.trip_driver_id","inner");
	$this->db->where('MONTH(trip_enddate)',$d1);
	$this->db->where('YEAR(trip_enddate)',$y1);
	$this->db->group_by("trip_driver_id,trip_drivername");
	$this->db->order_by("trip_driver_id","asc");
	$query = $this->db->get();
    return $query->num_rows();
    }

	public function get_driver_perform_yearly($d1,$drid)
    {
    $this->db->select('trip_driver_id,SUM(trip_endkm) as endkm, SUM(trip_startkm) as startkm,SUM(trip_gtotal) as gtotal');
    $this->db->from('trip_management');
	$this->db->where('trip_driver_id',$drid);
	$this->db->where('MONTH(trip_enddate)',$d1);
	$this->db->group_by("trip_driver_id");
	$this->db->order_by("trip_driver_id","asc");
	$query = $this->db->get();
    return $query->row();
    }
	
	public function get_driver_yearly_totalkm()  // get the total km running this year
    {
    $this->db->select('trip_driver_id,SUM(trip_endkm) as endkm, SUM(trip_startkm) as startkm,SUM(trip_gtotal) as gtotal');
    $this->db->from('trip_management');
	$this->db->where('YEAR(trip_enddate)',date('Y'));
	$this->db->group_by("trip_driver_id");
	$this->db->order_by("trip_driver_id","asc");
	$query = $this->db->get();
    return $query->result();
    }
	
	
	public function get_driver_monthly_totalkm($m)  // get the total km running this year
    {
    $this->db->select('trip_driver_id,SUM(trip_endkm) as endkm, SUM(trip_startkm) as startkm,SUM(trip_gtotal) as gtotal');
    $this->db->from('trip_management');
	$this->db->where('MONTH(trip_enddate)',$m);
	$this->db->group_by("trip_driver_id");
	$this->db->order_by("trip_driver_id","asc");
	$query = $this->db->get();
    return $query->result();
    }
	
	
	public function update_GST($id,$gst)
    {
    	$this->db->where('gen_id', $id);
		$this->db->update('general', $gst); 
    }
	
	
	public function view_driver_duty()
    {
    $this->db->select('*');
    $this->db->from('driver_schedule');
	$this->db->join('driverregistration',"driverregistration.driver_id=driver_schedule.drv_name","inner");
	$this->db->where('MONTH(drv_date)',date('m'));
	$this->db->where('YEAR(drv_date)',date('Y'));
	$this->db->order_by("driver_schedule.drv_id","desc");
    $query = $this->db->get();
    return $query->result();
    }
	
	public function view_driver_duty1($op,$md,$yr)
    {
    $this->db->select('*');
    $this->db->from('driver_schedule');
	$this->db->join('driverregistration',"driverregistration.driver_id=driver_schedule.drv_name","inner");
	if($op==1)
	{
	$this->db->where('driver_schedule.drv_date',$md);
	}
	else if($op==2)
	{
		$this->db->where('MONTH(drv_date)',$md);
		$this->db->where('YEAR(drv_date)',$yr);
	}
	else
	{
		$this->db->where('MONTH(drv_date)',date('m'));
		$this->db->where('YEAR(drv_date)',date('Y'));
	}
	$this->db->order_by("driver_schedule.drv_id","desc");
    $query = $this->db->get();
    return $query->result();
    }
	
	// registration documents details  -------------------------------------------------
	
	public function get_vehicle()
	{
	$this->db->select('*');
    $this->db->from('vehicle_registration');
	$this->db->join('vehicle_types',"vehicle_types.veh_type_id=vehicle_registration.vehicle_type","inner");
    $query = $this->db->get();
    return $query->result();
    }

	public function get_documents($vid)
	{
	$this->db->select('*');
	$this->db->from('vehicle_documents');
	$this->db->join('vehicle_registration',"vehicle_registration.vehicle_id=vehicle_documents.doc_vehicle_id","inner");
	$this->db->join('vehicle_types',"vehicle_types.veh_type_id=vehicle_documents.doc_vehicle_type","inner");
	if($vid!=="0")
	{
		$this->db->where('vehicle_documents.doc_vehicle_id',$vid);
	}
    $query = $this->db->get();
    return $query->result();
    }
	
	public function edit_doc($id)
	{
	$this->db->select('*');
	$this->db->from('vehicle_documents');
	$this->db->join('vehicle_registration',"vehicle_registration.vehicle_id=vehicle_documents.doc_vehicle_id","inner");
	$this->db->join('vehicle_types',"vehicle_types.veh_type_id=vehicle_documents.doc_vehicle_type","inner");
	$this->db->where('vehicle_documents.doc_id',$id);
    $query = $this->db->get();
    return $query->row();
    }
	
	public function delete_doc($id)
    {
        $this->db->where('doc_id', $id);
		$this->db->delete('vehicle_documents');
		return $this->db->affected_rows();
    }
	
	
	public function renewal_documents()
	{
	$this->db->select('*');
	$this->db->from('vehicle_documents');
	$this->db->join('vehicle_registration',"vehicle_registration.vehicle_id=vehicle_documents.doc_vehicle_id","inner");
	$this->db->join('vehicle_types',"vehicle_types.veh_type_id=vehicle_documents.doc_vehicle_type","inner");
	$this->db->order_by("vehicle_documents.doc_vehicle_id","asc");
    $query = $this->db->get();
    return $query->result();
    }
		
	// monthly targets --------------------------------------->
	
	public function get_monthly_targets($mon)
	{
	$this->db->select('*');
	$this->db->from('monthly_targets');
	$this->db->join('vehicle_registration',"vehicle_registration.vehicle_id=monthly_targets.tar_vehicle","inner");
	if($mon!="0")
	{
		$this->db->where("monthly_targets.tar_month",$mon);
	}
	$this->db->order_by("monthly_targets.tar_id","desc");
    $query = $this->db->get();
    return $query->result();
    }
	
	public function check_monthly_entry($mon,$yr,$veh)
	{
	$this->db->select("count(*) as cnt");
	$this->db->from('monthly_targets');
	$where="(tar_vehicle=".$veh." and tar_month=". $mon. " and tar_year=".$yr.")";
	$this->db->where($where);
	$query = $this->db->get();
    return $query->row();
    }
	
	public function delete_mon_targets($id)
    {
        $this->db->where('tar_id', $id);
		$this->db->delete('monthly_targets');
		return $this->db->affected_rows();
    }
		
	public function edit_mon_targets($id)
	{
	$this->db->select('*');
	$this->db->from('monthly_targets');
	$this->db->join('vehicle_registration',"vehicle_registration.vehicle_id=monthly_targets.tar_vehicle","inner");
	$this->db->where('monthly_targets.tar_id',$id);
    $query = $this->db->get();
    return $query->row();
    }
	
	public function report_doc($mon,$yr)
	{
	$this->db->select('*');
	$this->db->from('monthly_targets');
	$this->db->join('vehicle_registration',"vehicle_registration.vehicle_id=monthly_targets.tar_vehicle","inner");
	$this->db->where("monthly_targets.tar_month",$mon);
	$this->db->where("monthly_targets.tar_year",$yr);
	$this->db->order_by("monthly_targets.tar_month","asc");
    $query = $this->db->get();
    return $query->result();
    }
	
	public function get_monthly_collection($mon,$yr)
	{
	$this->db->select('SUM(trip_gtotal) as camount,trip_vehicle_id');
	$this->db->from('trip_management');
	$this->db->join('vehicle_registration',"vehicle_registration.vehicle_id=trip_management.trip_vehicle_id","inner");
	$this->db->where("MONTH(trip_enddate)",$mon);
	$this->db->where("YEAR(trip_enddate)",$yr);
	$this->db->group_by("trip_management.trip_vehicle_id");
    $query = $this->db->get();
    return $query->result();
    }
		
	public function view_general_expense($dt1,$dt2,$mon,$op)
	{
		$this->db->select('*');
		$this->db->from('general_expense');
		$this->db->join('expense_type', 'expense_type.etype_id=general_expense.gen_expense_type','inner');
		if($mon=="" && $op=="")
		{
		$where="(general_expense.gen_expense_billdate>='".$dt1."' and  general_expense.gen_expense_billdate<='".$dt2."')";	
		}
		else if($mon!="" && $op=="")
		{
			$where="(MONTH(general_expense.gen_expense_billdate)='".$mon."')";	
		}
		
		if($op=='0')
		{
			$where="(MONTH(general_expense.gen_expense_billdate)='".date('m')."')";	
		}
		$this->db->where($where);
		$this->db->order_by('general_expense.gen_expense_id','desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function view_marketing($dt1,$dt2,$mon,$yr,$op)
	{
	$this->db->select('*');
	$this->db->from('marketing');
	$this->db->join('staffregistration',"staffregistration.staff_id=marketing.mar_staff","inner");
	$this->db->join('profession',"profession.profession_id=marketing.mar_profession","inner");
	$where="";
	if($op=="1")
		{
		$where="(marketing.mar_edate>='".$dt1."' and  marketing.mar_edate<='".$dt2."')";	
		}
		else if($op=="2")
		{
			$where="(MONTH(marketing.mar_edate)='".$mon."' and YEAR(marketing.mar_edate)='".$yr."')";	
		}
		
		if($op=="")
		{
			$where="(MONTH(marketing.mar_edate)='".date('m')."')";	
		}
		
		$this->db->where($where);
	
	$this->db->order_by("marketing.mar_id","asc");
	$query = $this->db->get();
	return $query->result();
	}
	
	public function delete_marketing($id)
    {
        $this->db->where('mar_id', $id);
		$this->db->delete('marketing');
		return $this->db->affected_rows();
    }
	
	
	public function view_additional_incomes($dt1,$dt2,$mon,$op)
	{
		$this->db->select('*');
		$this->db->from('additional_incomes');
		$this->db->join('income_types', 'income_types.inc_type_id=additional_incomes.addi_inc_type','inner');
		if($mon=="" && $op=="")
		{
		$where="(additional_incomes.addi_inc_date>='".$dt1."' and  additional_incomes.addi_inc_date<='".$dt2."')";	
		}
		else if($mon!="" && $op=="")
		{
			$where="(MONTH(additional_incomes.addi_inc_date)='".$mon."')";	
		}
		
		if($op=='0')
		{
			$where="(MONTH(additional_incomes.addi_inc_date)='".date('m')."')";	
		}
		$this->db->where($where);
		$this->db->order_by('additional_incomes.addi_inc_id','desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function delete_income($id)
    {
        $this->db->where('addi_inc_id', $id);
		$this->db->delete('additional_incomes');
		return $this->db->affected_rows();
    }
	
	
	
}