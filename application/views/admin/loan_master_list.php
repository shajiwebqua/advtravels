<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}


</style>
 
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  <h1><b>Vehicle loans list</b></h1>
<ol class="breadcrumb" style='font-size:15px;'>

<li><a id='showshedule' href="#" data-toggle="modal"  style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Show Shedule</button></a></li>


<li><a id='view-pdf' href="<?php echo site_url('Pdf/pdf_loan') ?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-print" aria-hidden="true"></i> Print</button></a></li>
<!-- data-target="#myModal1" --> <li><a id='idsave' href="<?php echo site_url('Pdf/pdf_loan') ?>" data-target="" data-toggle='modal'style='color:#4b88ed;' target="_blank"><button class='btn btn-primary'><i class="fa fa-save" aria-hidden="true"></i> Save</button></a></li>
<li><a id='iddelete' href="" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-trash" aria-hidden="true"></i> Delete</button></a></li>
       </ol> 
 </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header  end -->	  
	  
<section class="content"> 
  <div style="color:blue;font-size:12px;"> Select loan details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Show Schedule,Delete)  </div>
            <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('messages'); ?></div></center>
		    </div>
		</div>  
    <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">
	<!-- Add new user details-->
	 <div style="background-color:#fff;border-radius:2px;padding:15px; ">
					
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
					<div class="row">
					<div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
					
								</div>
							</div>
					</div>
					<table class="table table-striped table-hover table-bordered" id="loan_master_list">
                            <thead>
								<tr style='color:#4b88ed'>
								 <th>Id</th>
								 <th>LTL Ref.No</th>
								 <th>Vehicle</th>
								 <th>Amount</th>
								 <th>Loan Amount</th>
								 <th>Interest/Period</th>
								 <th>Interest</th>
								 <th>Loan Total</th>
								 <th>Status</th>
								</tr>
						    </thead>
                    </table>
                    </div>
                        <!-- END BORDERED TABLE PORTLET-->
                    </div>
                    </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>

        <div class="modal fade draggable-modal" id="myModals3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>Loan Shedules</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style='padding:0px 10px 0px 10px;'>
                    <div class=" max-width">
                        <table id="loan_shedule" class="table table-striped table-hover table-bordered" cellspacing="0">
                        <thead>
                    <tr>
                        <th>#</th>
                        <th> Date </th>
                        <th> Loan </th>
                        <th> Instalment </th>
                        <th> Intrest </th>
                        <th> Principal </th>
                        <th> Check No </th>
                        <th> Voucher No </th>
                    </tr>
                </thead>
            </table>
                    </div>
                </div>


            </div>
        </div>
        </div>
        </div>
  
      
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Add</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
                   </div> 

           <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
			  
		
            <!-- /.modal-dialog -->
            </div>
                   </div>  

           <div class="modal fade draggable-modal" id="myModals" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Share</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
            </div>     
           </div>       
			
	</div>
  <!-- /.content --> 
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------

 setTimeout(function(){ $("#idh5").html(""); }, 3000);
 
$('#loan_master_list').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "loan1/loan_ajax",// json datasource
               },
        "columns": [
			{ "data": "loanid" },
			{ "data": "ltlrefno" },
			{ "data": "vehiclename"},
			{ "data": "vehicleamount"},
			{ "data": "loanamount" },
			{ "data": "loanterm" },
			{ "data": "interest" },
			{ "data": "loantotal" },
			{ "data": "status" },
			]
  } );

 var table = $('#loan_master_list').DataTable();
     $('#loan_master_list tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });			   


$('#showshedule').click(function ()
{   
    ltlrefno = $('#loan_master_list').find('tr.selected').find('span.ltlrefno').text();
     //alert(ltlrefno);
     if(ltlrefno=="")
     {
      alert('Please Select Loan List');
     }
     else
     {
		 $(this).attr("data-target","#myModals3");
		 
      $('#loan_shedule').dataTable({

       destroy: true,
       "processing": true,
       "ajax": {
                url :"<?php echo base_url(); ?>" + "Loan/showshedule/" + ltlrefno,// json datasource
                
            },
            "columnDefs":[
            {"width":"6%","targets":0},
            ],

            "columns": [
            { "data": "slno"},
            { "data": "ln_date" },
            { "data": "ln_amount" },
            { "data": "ln_instalment" },
            { "data": "ln_intrest" },
            { "data": "ln_principal" },
            { "data": "ln_check" },
            { "data": "ln_voucher" },
            ]
        } ); 
} //return false;
});
		
		
		
		
	$('#iddelete').click(function () 
    {
     var id=$('#loan_master_list').find('tr.selected').find('td').eq(1).text();
     //alert(id);
     if(id=="")
     {
      alert("Please Select Loan Details")

     }
     else
     {
    var res=confirm("Delete this loan details?");
    if(res)
    {
    var id=$('#loan_master_list').find('tr.selected').find('td').eq(1).text();
       jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Loan/del_entry",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    }
  }
        });     
	
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });
 //});
  

    $('#view-pdf').on('click',function(){
      
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
    var pdf_link = $(this).attr('href');
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Loan Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    }); return false;
       
      
    });    


</script>



