<?php
 include("application/views/common/header.php");?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.ttext
{
	background-color:#fff;
	font-weight:bold;
	color:Green;
	
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Enquiry Form</b> </h1>
    
    <ol class="breadcrumb">
   <li> <a href="<?php echo base_url("Enquiry/enquiry_list");?>" style="color:#4b88ed;font-size:15px;"> <i class="fa fa-backward" aria-hidden="true"></i>Back to List</a></li>
    </ol> 
    </section>
<label style="background-color:#cecece;width:100%;height:1px;"></label>
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
  <!-- Content Header end -->
<section class="content"> 
    <!-- Small boxes (Stat box) -->

<div class="row">
<div class="col-md-12">

  <!--Add new user details -->
<div style="background-color:#fff;border-radius:10px;padding:15px; ">
      <div class="portlet-title">
      
 </div>
  <div class="portlet-body form">
 <form  onsubmit="return checkdata();" class="form-horizontal" role="form" method="post" action="<?php echo site_url("Enquiry/add_order");?>">
 <div class="box-body">

 <?php
 
 $this->db->select('MAX(sl_no) as maxslno');
 $this->db->from('enquiry');
 $row=$this->db->get()->row();
  ?>
 
	<div class="form-group">
	
		<div class='row' style='margin-top:5px;'>
			<label class="col-md-3 control-label" for="sl_no">Enquiry No : </label>
			<div class="col-md-4">
			<input type="number" class="form-control" name="sl_no" id="sl_no" value="<?=($row->maxslno+1);?>" required>
			</div> 
		</div>
	
	<div class='row' style='margin-top:5px;'> 
		   <label class="col-md-3 control-label" for="party_name">Client Category : </label>
			<div class="col-md-4">
				<select name="category" id='category' class='form-control'>
					<option value="">---------</option>
					<option value="1">Individual</option>
					<option value="2">Corporate</option>
					<option value="3">Agent</option>
				</select>
			</div>
		</div>
		<div id='corp'>
			<div class='row' style='margin-top:5px;'> 
			   <label class="col-md-3 control-label" id='corplbl' style='color:#669ef9' >Corporate-Name : </label>
				<div class="col-md-4">
				<input type="text" class="form-control" name="corporate" id="corporate">
				</div>
			</div>
			
			<div class='row' style='margin-top:5px;'> 
			   <label class="col-md-3 control-label" id='corplbl' style='color:#669ef9'>Phone/Mobile No : </label>
				<div class="col-md-4">
				<input type="text" class="form-control" name="corpmobile" id="corpmobile">
				</div>
			</div>
		</div>
		
		<div class='row' style='margin-top:5px;'> 
		<div id='agents'>
		   <label class="col-md-3 control-label" id='agentlbl'>Agent Name : </label>
			<div class="col-md-5">
			<input type="text" class="form-control" name="agent" id="agent">
			<input type="hidden" class="form-control" name="agentid" id="agentid">
		</div>
		<a href="#" id='agentselect' data-target="#myModalSA" data-toggle='modal'><input type='button' value='Select' class="btn btn-default btn-xs" name='agentselect'  style='margin-top:5px;'> </a>
			&nbsp;&nbsp;<a href="#" data-target="#myModal1" data-toggle='modal'><input type='button' value='Add' class="btn btn-default btn-xs" name='agentadd' id='agentadd' style='margin-top:5px;'> </a>
		</div>
		</div>
		
		
		<div class='row' style='margin-top:10px;'> 
		   <label class="col-md-3 control-label" for="party_name">Client Name : </label>
			<div class="col-md-5">
			<input type="text" class="form-control" name="party_name" id="party_name" required>
		</div>
		</div>
	
		<div class='row' style='margin-top:5px;'>
		
		   <label class="col-md-3 control-label" for="phone_no">Phone No : </label>
				<div class="col-md-3" >
					<input type="text" pattern="[0-9]*" class="form-control" name="phone_no" id="phone_no"   required>
					<label id="landmessage" style="color:red;"></label>
				</div>
		
		
	     </div>
		 
		 <div class='row' style='margin-top:5px;'>
			<label class="col-md-3 control-label" for="mobile_no">Mobile : </label>
				<div class="col-md-3">
					<input type="text"pattern="[0-9]*" class="form-control" name="mobile_no" id="mobile_no" required>
					<label id="mobmessage" style="color:red;"></label>
			  </div>
	     </div>
		 
		 
		 
		 <div class='row' style='margin-top:5px;'>
				<label class="col-md-3 control-label" for="address">Postal Address : </label>
				<div class="col-md-5">
			   <textarea rows=2 cols=50 class="form-control"  name="address"  id="address" required></textarea>
			   </div>
			</div>
			
			
			<div class="row" style="margin-top:10px;">
					<label class="col-md-3 control-label" for="nature_journey">Nature of Journey : </label>
			<div class="col-md-4">
			<select class='form-control' name="njourney" id="njourney" required>
			<option value=''>---------</option>
			<?php
			$vt=$this->db->select('*')->from("category") ->get()->result();
			
			foreach($vt as $v)
			{
			 echo"<option value='". $v->category_name ."'>".$v->category_name ."</option>";
			}
			?>
			</select>
		  </div>
	    </div>		

			 <div class='row' style='margin-top:10px;'>
			<label class="col-md-3 control-label" for="passenger">No. of Passengers : </label>
				<div class="col-md-2" style="padding:0px;">
					<input type="text" pattern="[0-9]*" class="form-control" name="passenger" id="passenger" style='margin-left:15px;' required>
			  </div>
			</div>
			
			<div class='row' style='text-align:left;'>
			<label class="col-md-2 control-label" style='text-align:right;'>No of : </label>
			<label class="col-md-1 control-label" style='text-align:left;'>Male</label>
			<label class="col-md-1 control-label" style='text-align:left;'>Female </label>
			<label class="col-md-1 control-label" style='text-align:left;'>Childrens</label>
			<label class="col-md-1 control-label" style='text-align:left;'>Infants</label>
			</div>
			
		<div class="row" style="margin-top:5px;">
			<div class="col-md-3"> </div> 
			<div class="col-md-1">
     			<input type="text" class="form-control" name="nmale" id="nmale" value="" required>
			</div> 
			<div class="col-md-1">
     			<input type="text" class="form-control" name="nfemale" id="nfemale" value="" required>
			</div>
			<div class="col-md-1">
     			<input type="text" class="form-control" name="nchildren" id="nchildren" value="" required>
			</div>
			<div class="col-md-1">
     			<input type="text" class="form-control" name="ninfant" id="ninfant" value="" required>
			</div>
		</div>		
			
			
		<div class="row" style="margin-top:5px;">
			<label class="col-md-3 control-label" for="prefered_vehicle">No of Days : </label>
			<div class="col-md-2">
     			<input type="text" pattern="[0-9]*" class="form-control" name="no_days" id="no_days" required>
			</div> 
		</div>		
			
		
			
		<div class="row" style="margin-top:15px;">
			<label class="col-md-3 control-label" for="prefered_vehicle">Prefered Vehicle : </label>
			<div class="col-md-4">
			<select class='form-control' name="prefered_vehicle" id="prefered_vehicle" required>
			<option value=''>---------</option>
			<?php
			$vt=$this->db->select('*')->from("vehicle_types") ->get()->result();
			
			foreach($vt as $v)
			{
			 echo"<option value='". $v->veh_type_name ."'>".$v->veh_type_name ."</option>";
			}
			?>
			</select>
			
			</div> 
		</div>		
			
			
			
		  <div class="row " style="margin-top:5px;">
				   <label class="col-md-3 control-label" for="start_date">Starting Date & Time : </label>
				<div class="col-md-3">
					<input type="date" class="form-control" name="start_date" id="start_date"  value='<?php echo date('Y-m-d');?>' required>
			  </div>
				<div class="input-group bootstrap-timepicker timepicker">
					<input type="text" class="form-control" name="start_time" id="start_time" required>
			  </div>
			</div>

		<div class="row" style="margin-top:5px;">
		
			<label class="col-md-3 control-label" for="return_date">Returning Date & Time : </label>
				<div class="col-md-3">
					<input type="date" class="form-control" name="return_date" id="return_date"  value='<?php echo date('Y-m-d');?>' required>
			  	</div>
				
			  	<div class="input-group bootstrap-timepicker timepicker">
					<input type="text" class="form-control" name="return_time" id="return_time" required>
			  	</div>
		</div>
			
		<div class="row" style="margin-top:15px;">
		<label class="col-md-3 control-label" for="return_place">Returning Place : </label>
			<div class="col-md-5">
			   <input type="text" class="form-control" name="return_place" id="return_place" required>
			</div>
		</div>	

		
		<div class="row" style="margin-top:5px;">
		<label class="col-md-3 control-label" for="journy_details">Vehicle arriving place & details of journy : </label>
			<div class="col-md-5">
			<textarea rows=3 cols=50 class="form-control"  name="journy_details" id="journy_details" required></textarea>
		</div> 
		</div>	
		
		 <div class="row" style="margin-top:5px;">
		   <label class="col-md-3 control-label" for="from">Starting Location : </label>
			<div class="col-md-4">
				<input type="text" class="form-control" name="from" id="from" required>
		    </div>
		  </div>
		

		<label style='width:100%;height:1px;background-color:#e4e4e4;margin-top:0px;'></label>
		<div class="row" style="margin-top:5px;">
		<label class="col-md-3 control-label" for="amount">Amount : </label>
			<div class="col-md-2">
			<input type="text" pattern="[0-9]*" class="form-control" name="amount" id="amount" value="" required>
		</div> 
		  </div>
		  
		 <div class="row" style="margin-top:5px;">
		   <label class="col-md-3 control-label" for="upto">Upto ( per Km ) : </label>
			<div class="col-md-2">
				<input type="text" pattern="[0-9]*" class="form-control" name="upto" id="upto" required>
			</div>
		  </div>
		  <div class="row" style="margin-top:5px;">
			<label class="col-md-3 control-label" for="running">Running (per RKm) : </label>
			<div class="col-md-2">
				<input type="text" class="form-control" pattern="[0-9]*" name="running" id="running" value="" required>
			</div> 
		  </div>
		  
		 
				  
				<div class="row" style="margin-top:5px;">
				   <label class="col-md-3 control-label" for="permit_charge">Permit Charge : </label>
					<div class="col-md-2">
					<input type="text" pattern="[0-9]*" class="form-control"  name="permit_charge" id="permit_charge" required>
				  </div>
				</div>
				
	

			<label style="width:100%;height:1px;background-color:#e4e4e4;"></label>

			<div class="row" style="margin-top:5px;">
			   <div class="col-md-6">
					<center><button type="submit" class="btn btn-success" style="padding:5px 20px 5px 20px" >Save Details</button>
					</center>
			   </div>
			</div> 
</div>
 </div>
</form>

</div>
</div>
</div>
</div>
</section>
</div>

<!-- first Model for add agents -------------------------------------------------------->

 <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
				<h4 class="modal-title">Add New Agent Details</h4>
			</div>
			
			<!-- content -------------------------------------------->
			
			<div style="background-color:#fff;padding:10px; ">
   
				<!--<form onsubmit="return checkdata();" class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/add_agent')?>"> -->
                  
						<div class="form-group">
							 <div class="row" style="margin-top:0px;">
									<label class="col-md-3 control-label" style="padding-top:0px">Agent Code</label>
								   <div class="col-md-4">
								   <input type="text" class="form-control"  name="agent_code" id='acode' required>
								 </div>
							 </div>
                        </div>
  
						<div class="form-group" >
							 <div class="row" style="margin-top:0px;">
								<label class="col-md-3 control-label" style="padding-top:0px">Agent Name</label>
								 <div class="col-md-7">
								   <input type="text" class="form-control"  name="agent_name"  id='aname' required>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
								<label class="col-md-3 control-label" style="padding-top:0px">Address</label>
								 <div class="col-md-7">
								   <textarea rows=2 cols=30 class="form-control"  name="agent_address"  id='aaddress' required></textarea>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
							 	<label class="col-md-3 control-label" style="padding-top:0px">Mobile</label>
								 <div class="col-md-7">
								   <input type="number" pattern="[0-9]*" class="form-control"  name="agent_mobile" id="amobile" required>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
									<label class="col-md-3 control-label" style="padding-top:0px">Email</label>
							 	 <div class="col-md-7">
								   <input type="email" class="form-control"  name="agent_email" id='aemail' required>
								 </div>
							 </div>
                        </div>
                                 <hr>

                    <div class="form-group">
                     <div class="row"><center>
                            <button class="btn btn-primary" id='submitagent'>Submit</button>
								   &nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
								   </center>
                           </div>
                       </div>
					   
                       <!-- </form> -->
                       </div>

				<!------------------------------------------------------------>

		</div>
		</div>
	<!-- /.modal-dialog -->
	</div>
</div>

<!--- MODEL 2 DISPLAY AGENT LIST -------------------->

<div class="modal fade" id="myModalSA" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Agents List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class="col-md-12">
            <table id="tblagents" class="table table-striped table-hover table-bordered" cellspacing="0">
            <thead>
            <tr>
                <th>Select</th>
				<th>ID</th>
                <th>Agent Name</th>
                <th>Mobile</th>
            </tr>
        </thead>
    </table>
        </div>
      </div>
 	 </div>
 	</div>
   </div>
  </div>

<!--- AGENTS END -------------------->




<?php include("application/views/common/footer.php");?>
 
<script type="text/javascript">
	 
//sweet alert box ----------------------
$("#corp").hide();
$("#agents").hide();
		
$("#msg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"success")
	if(mg[0]==2)
		swal("Updated",mg[1],"success")
	if(mg[0]==3)
		swal("Deleted",mg[1],"success")
	if(mg[0]==4)
		swal("Try Again",mg[1],"error")
    $("#msg").html("");
  }
//---------	
	
	
	
	
	
	
	
	
	
$("#category").change(function()
{
	var cat=$("#category").val();
	if(cat==1)
	{
		$("#corp").hide();
		$("#agents").hide();
		$("#agentid").val('0');
	}
	else if(cat==2)
	{
		$("#corp").show();
		$("#agents").hide();
		$("#agentid").val('0');
	}
	
	else if(cat==3)
	{
		$("#corp").hide();
		$("#agents").show();
	}
	
});	

$("#acode").blur(function(){
	if($("#acode").val()==""){$("#acode").css('border','1px solid red');}
	else {$("#acode").css('border','1px solid #c6c6c6');}
	
});

$("#aname").blur(function(){
	if($("#aname").val()==""){$("#aname").css('border','1px solid red');}
	else {$("#aname").css('border','1px solid #c6c6c6');}
});

$("#aaddress").blur(function(){
	if($("#aaddress").val()==""){$("#aaddress").css('border','1px solid red');}
	else {$("#aaddress").css('border','1px solid #c6c6c6');}
});
	
$("#amobile").blur(function(){
	if($("#amobile").val()==""){$("#amobile").css('border','1px solid red');}
	else {$("#amobile").css('border','1px solid #c6c6c6');}
});

$("#aemail").blur(function(){
	if($("#aemail").val()==""){$("#aemail").css('border','1px solid red');}
	else {$("#aemail").css('border','1px solid #c6c6c6');}
});


function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test($email );
}



$("#submitagent").click(function()
		{

		var acod=$("#acode").val();
		var ana=$("#aname").val();
		var aadd=$("#aaddress").val();
		var amob=$("#amobile").val();
		var aema=$("#aemail").val();

		var res1=true;
		var res2=true;
		var res3=true;
		var res4=true;
		var res5=true;
		var res6=true;
		

if(acod==""){res1=false;$("#acode").css('border','1px solid red'); }else{res1=true;$("#acode").css('border','1px solid #c6c6c6');}
if(ana==""){res2=false;$("#aname").css('border','1px solid red');}else{res2=true;$("#aname").css('border','1px solid #c6c6c6');}
if(aadd==""){res3=false;$("#aaddress").css('border','1px solid red');}else{res3=true;$("#aaddress").css('border','1px solid #c6c6c6');}
if(amob==""){res4=false;$("#amobile").css('border','1px solid red');}else{res4=true;$("#amobile").css('border','1px solid #c6c6c6');}
if(aema==""){res5=false;$("#aemail").css('border','1px solid red');}else{res5=true;$("#aemail").css('border','1px solid #c6c6c6');}
	
		if(res1!=false && res2!=false && res3!=false && res4!=false && res5!=false )
			{
	
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "General/add_agent1",
				dataType: 'html',
				data: {acode:acod,aname:ana,aaddress:aadd,amobile:amob,aemail:aema},
				success: function(data) 
				 {
					 
				  if(parseInt(data)>0)
				  {
					  $("#agentid").val(data);
					  $("#agent").val(ana);
					  swal("Saved","Agent details saved..","success");
					  
				  }
				  else
				  {
					  swal("Cancelled","Agent details missing, Try again..","error");
				  }
				}
				});
			}
			else
			{
				swal("Missing","Agent details missing, Try again..","error");
			}
		});
	
//-------------------------------------
	 
     $("#mobmessage").hide();
     $("#landmessage").hide();  
	 
	 
  function checkdata()
  {
    var mob=$("#mobile_no").val();
    var landline=$("#phone_no").val();
  
    if(mob.length<10 || mob.length>10)
    {
	  $("#mobmessage").show();
      $("#mobmessage").html("Invalid mobile no,10 digits only.");
      return false;
    }
    /*else if(landline.length<11 || landline.length>11)
    {
	  $("#mobmessage").hide();
      $("#landmessage").show();
      $("#landmessage").html("Invalid land phone no,11 digits only.");
      return false;
    }*/
        
    else
    {
	  $("#mobmessage").hide();
      $("#landmessage").hide();
      $("#mobmessage").html("");
      $("#landmessage").html("");
      return true;
    }
  }
   
   	$("#start_time, #return_time").timepicker()
  
    
	
 var table= $('#tblagents').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/agentlist_ajax",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"7%","targets":1},
		],
			   
        "columns": [
            { "data": "select"},
			{ "data": "agid"},
		    { "data": "agname"},
            { "data": "agmobile"},
       ]
  });
  
  /*var table = $('#tblagents').DataTable();
     $('#tblagents tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });	*/
  
    
  $('#tblagents tbody').on('click', '.agselect', function ()
  {
	    var id =  $(this).attr('id');
		
      jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/get_agentsDT",
        dataType: 'html',
        data: {aid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#agent").val(obj.agname);
				$("#agentid").val(obj.agid);
		       }
            });
        }); 
   
 /* 
$("#agentselect").click(function()
{
	$("#agentselect").attr("data-target","#myModald");
});
  */
</script> 
 
 