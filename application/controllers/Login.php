<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{
	function __construct(){
		parent::__construct();
		if (!isset($user))
		{ 
		//redirect('/');
		} 
		else
		{ 
		return true;
		}
		$this->load->helper(array('form', 'url'));
		}


	public function index()
	{
    //if($this->session->userdata("logged_in"))
	//{
      //$this->home();
    //}

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|md5|alpha_dash');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/login');
		}
		else
		{
			$email= $this->security->xss_clean($this->input->post('email'));
			$password = $this->security->xss_clean($this->input->post('password'));
			$this->load->model('Model_login');
            
            $database = array(
            	'users' => array(
            		'username' => array('user_email','user_mobile'),
            		'password' => "password"
            	),
            	'staffregistration' => array(
            		'username' =>array('staff_email', 'staff_mobile'),
            		'password' => "staff_password"
            	)
            );
			
            $flag_out_of_loop = false;
            $flag_admin = true;
            $result = null;
            foreach($database as $tableKey => $table){
            	$where[$table['password']] = $password;
            	foreach($table['username'] as $column){
            		$where[$column] = $email;          			
            		$result = $this->Model_login->login($tableKey, $where);
            		if($tableKey != "users"){
            			$flag_admin = false;
            		}
            		if($result != null){
            			$flag_out_of_loop = true;
            			break;
            		}
            		array_pop($where);
            	}
            	if($flag_out_of_loop){
            		break;
            	}
            	$where = array();
            }

			if($result == null){
			 	$msg = '<font color=red>Invalid username and/or password.</font><br />';
				$data['msg'] = $msg;	
				$this->load->view('admin/login',$data);
				return;
			}
			
		
			if($flag_admin){
				$newdata = array(
    			   'name'=>$result[0]->user_name,
				   'userimage'=>$result[0]->user_image,
				   'email'=> $result[0]->user_email,
      			   'userid'  =>  $result[0]->user_id,
				   'userlevel'=>$result[0]->user_level,
      			   'usercode'   => "admin",
				   
       			   'logged_in' => TRUE,
       			   'user_role' => "0",
				   'accpermission'=>$result[0]->account_permission,
               	);
			}
			else{ 
				$newdata = array(
    			   'name'=>$result[0]->staff_name,
				   'userimage'=>$result[0]->staff_image,
				   'email'=> $result[0]->staff_email,
      			   'userid'  =>  $result[0]->staff_id,
      			   'usercode'   =>$result[0]->staff_code,
				   'userlevel'=>"0",
       			   'logged_in' => TRUE,
       			   'user_role' => $result[0]->staff_role,
				   'accpermission'=>$result[0]->account_permission,
               	);
			}
			$this->session->set_userdata($newdata);
			redirect('Login/home');
		}
    }
	
   	public function home()
	{
	/*	$this->session->set_userdata('tkm',"");
		$this->session->set_userdata('drna',"");
		$this->session->set_userdata('tkm1',"");
		$dy=date('Y');
		$dy1=$dy;
		
		if(date('m')==1)
		{
		$d1=12;
		$d2=date('m');
		$dy1=(date('Y')-1);
		}
		else
		{
		$d1=date('m')-1;
		$d2=date('m');
		$dy1=date('Y');
		}
		
		$this->performer_driver1($d1,$dy1); //previous month
		$this->performer_driver2($d2,$dy);  //current month
		*/
		
		$vresults=$this->db->select("*")->from("vehicle_registration")->get()->result();
		$data1['vresult']=$vresults;
		$this->load->model('Model_vehicle');
		$serv=$this->Model_vehicle->view_Dash_PSvehicles_nos();
		
		//PIE CHART-----------------------------
		/*$data2=array();
		foreach($vresults as $v1)
		{
		$vid=$v1->vehicle_id;
		$t=$this->Model_vehicle->trip_charge($vid);
		$f=$this->Model_vehicle->get_fuel_amount($vid);
		$s=$this->Model_vehicle->get_service_expense($vid);
		$o=$this->Model_vehicle->get_other_amount($vid);;
		$dt1=array("regno"=>$v1->vehicle_id,"trip"=>$t,"fuel"=>$f,"service"=>$s,"others"=>$o);
		array_push($data2,$dt1);	
		}
		$data1['charges']=$data2;*/
		
		$data1['servehicle']=$serv;
		//-----------------------------------------
		$this->load->view("admin/starter",$data1);
    }
 
//--------------the following functions written into the Login page for starter dashboard --------->
	
public function performer_driver1($dt1,$y1)
{
$this->load->Model('Model_general');
$result=$this->Model_general->get_driver_perform($dt1,$y1);
$dresult=$this->Model_general->get_perform_driver_names();

$nrows=$this->Model_general->get_driver_perform_numrows($dt1,$y1);

$data1=array();
$i=0;
$tkm="";
$dna="";
$dtop=array();
$drid=0;
$dkmid="1,0";
if($nrows>0)
{

$m=array();	
	$x1=0;
	foreach($dresult as $r11)
	{
		if($x1<count($result))
		{
			if($result[$x1]->trip_driver_id==$r11->driver_id)
			{
			
				$rkm=($result[$x1]->endkm - $result[$x1]->startkm);
				$v1=round(($rkm/100),0,1);
				
				$d=array("0"=>$v1,"1"=> $result[$x1]->trip_driver_id);
				array_push($m,$d);	
				$x1++;
			}	
			else
			{
				$d=array("0"=>"1","1"=> $r11->driver_id);
				array_push($m,$d);
			}	
		}
		else
		{
		$d=array("0"=>"1","1"=> $r11->driver_id);
		array_push($m,$d);	
		}
	}	

	//var_dump($m1);
		
	for($x2=0;$x2<count($m);$x2++)
	{
		if($m[$x2][0]==0)
		{
			$m[$x2][0]=5;
			$tkm.=$m[$x2][0].",";
		}
		else
		{
		$tkm.=$m[$x2][0].",";
		}
	}

	array_multisort(array_column($m, 0), SORT_DESC, $m);
	$dkmid=$m[0][0].",".$m[0][1];

	//var_dump($m);
	
	foreach($dresult as $dr)
	{
		$dna.=$dr->driver_name. ',';	
	}
	
	
}
else
{
	//foreach($dresult as $d22)
	//{
	//$dkmid="1,".$d22->driver_id;
	//break;
	//}
	
		foreach($dresult as $dr)
		{
		$dna.=$dr->driver_name . ',';	
		$tkm.='1' .',';
		}	
}	

$tkm=substr($tkm,0,strlen($tkm)-1);
$dna=substr($dna,0,strlen($dna)-1);

$dd1=explode(",",$dkmid);

$this->session->set_userdata('tkm',$tkm);
$this->session->set_userdata('drna',$dna);
$this->session->set_userdata('drvid',$dd1[1]);
}
 
 
 
public function performer_driver2($dt2,$y2)
{
$this->load->Model('Model_general');
$result=$this->Model_general->get_driver_perform($dt2,$y2);
$dresult=$this->Model_general->get_perform_driver_names();

$nrows1=$this->Model_general->get_driver_perform_numrows($dt2,$y2);

$data1=array();
$i=0;
$tkm1="";
$dna1="";
$dtop1=array();
$drid1=0;
$dkmid1="1,0";

if($nrows1>0)
{

		
	$m1=array();	
	$x1=0;
	foreach($dresult as $r11)
	{
		if($x1<count($result))
		{
			if($result[$x1]->trip_driver_id==$r11->driver_id)
			{
			
				$rkm=($result[$x1]->endkm - $result[$x1]->startkm);
				$v1=round(($rkm/100),0,1);
							
				$d=array("0"=>$v1,"1"=> $result[$x1]->trip_driver_id);
				array_push($m1,$d);	
				$x1++;
			}	
			else
			{
				$d=array("0"=>"1","1"=> $r11->driver_id);
				array_push($m1,$d);
			}	
		}
		else
		{
		$d=array("0"=>"1","1"=> $r11->driver_id);
		array_push($m1,$d);	
		}
	}	

	
	for($x2=0;$x2<count($m1);$x2++)
	{
		if($m1[$x2][0]==0)
		{
			$m1[$x2][0]=5;
			$tkm1.=$m1[$x2][0].",";
		}
		else
		{
		$tkm1.=$m1[$x2][0].",";
		}
	}

	array_multisort(array_column($m1, 0), SORT_DESC, $m1);
	$dkmid1=$m1[0][0].",".$m1[0][1];
//-----------------------
		
	foreach($dresult as $dr)
	{
		$dna1.=$dr->driver_name. ',';	
	}
}
else
{
//	foreach($dresult as $d22)
//	{
//	$dkmid1="1,".$d22->driver_id;
//	break;
//	}	
$dkmid1="1,0";

	foreach($dresult as $dr)
		{
		$dna1.=$dr->driver_name. ',';	
		$tkm1.='1' .',';
		}
		
}	

$dd2=explode(",",$dkmid1);

$tkm1=substr($tkm1,0,strlen($tkm1)-1);
$this->session->set_userdata('tkm1',$tkm1);
$this->session->set_userdata('drvid1',$dd2[1]);
}
//----------------------------------------------------------------------------------------------->	
}
?>
