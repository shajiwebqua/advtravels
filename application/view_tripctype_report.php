<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<style>
.numericCol{
  text-align:right;
}
</style>

<?php

	$sdate1=$this->session->userdata('sdate1');
	$edate1=$this->session->userdata('edate1');
	$corpid=$this->session->userdata('corpid');
	$ctype=$this->session->userdata('ctype');
	$ctype1=$this->session->userdata('ctype1');
	
	if(!isset($sdate1)){$sdate1="----";}
	if(!isset($edate1)){$edate1="----";}
	if(!isset($corpid)){$corpid='-1';}
	if(!isset($ctype)){$ctype='0';}
	if(!isset($ctype1)){$ctype1='0';}
	
	?>


<div class="content-wrapper"> 
  <!-- Main content -->
  <!-- Content Header (Page header) -->
  <section class="content-header">
  <h1><b>Client type wise trip details</b></h1>
  <ol class="breadcrumb" style="font-size:15px;">
<!--  <li><a id='view-pdf' href="<?php echo site_url('Pdf/pdf_log') ?>" style='color:#4b88ed;' ><i class="fa fa-print" aria-hidden="true"></i>Print</a></li> 
  <li><a id='idsave' href="<?php echo site_url('Pdf/pdf_log') ?>" style='color:#4b88ed;' target="_blank"><i class="fa fa-save" aria-hidden="true"></i>Save</a></li>
  <li><a id='idshare' href="" data-toggle='modal' style='color:#4b88ed;' ><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a></li>
  -->
     <li> <a id="view-pdf" href="<?php echo site_url('Pdf/Trip_CT_Report') ?>" style="color:#4b88ed;font-size:15px;"><button> <i class="fa fa-print" aria-hidden="true"></i>Print Report</button></a></li>
  </ol>
  
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->   
    
  <section class="content"> 
  <!-- <div style="color:#4b88ed;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , And click on the button (Print,Share,Edit,View,Delete)  </div> -->
    <div style="padding:2px 0px 2px 0px;">
		 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
		 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div>
		 <div id='mserr'><?php echo $this->session->flashdata('message1'); ?></div>
		 </center>
		 </div>
	</div>
	
	<!-- search options -------------------------------------------->
	
	<form  onsubmit="return check_date();" method='post' action="<?php echo base_url('Trip_reports/view_report');?>">
				<div class="row">
				
				<div class="col-md-3" style="border-right:2px solid #e4e4e4; border-left:2px solid #e4e4e4;">
					<div class='row'>
					<label class="col-md-7 control-label " style="font-size:16px;font-weight:700;color:#a85506; " >Select Period :</label>
					</div>
					
					
					<div class='row' style="margin-top:5px;">
					<label class="col-md-4  control-label" style="text-align:right;">Start Date </label>
					<div class="col-md-7" >
					<div class='row'>
					<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick"  name='sdate' readonly id="datepicker1" value="<?php echo $sdate1;?>">
					</div> 
					</div>
					</div>
					</div>
					
					<div class='row' style="margin-top:5px;">
					<label class="col-md-4  control-label" style="text-align:right;">End Date </label>
					<div class="col-md-7" >
					<div class='row'>
					<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" name='edate' readonly id="datepicker2" value="<?php echo $edate1;?>">
						</div> 
					</div>
					</div>
					</div>
				    </div>
			                 
				   <div class="col-md-2 " style="border-right:2px solid #e4e4e4; ">
					<div class='row'>
					<label class="col-md-12 control-label " style="font-size:16px;font-weight:700;color:purple; " >Running Trips:</label>
					</div>
					
					<div class='row' style="margin-top:5px;">
					<label class="col-md-12 control-label" >Select Option : </label>
					</div>
					
					<div class='row' >
					<div class="col-md-12" >
					<select class="form-control" id="ctype" name="ctype" >
					<option value="0" <?php if($ctype=="0") echo "selected";?> >-----</option>
					<option value="1" <?php if($ctype=="1") echo "selected";?>>INDIVIDUAL</option>
					<option value="2" <?php if($ctype=="2") echo "selected";?>>CORPORATE</option>
					<option value="3" <?php if($ctype=="3") echo "selected";?>>AGENTS</option>
					<option value="4" <?php if($ctype=="4") echo "selected";?>>ALL</option>
					</select>
					</div>
					</div>
				    </div>
										
					<div class="col-md-2" style="border-left:2px solid #e4e4e4; ">
					<div class='row'>
					<label class="col-md-12 control-label " style="font-size:16px;font-weight:700;color:#4d8c24; " >Completed Trips:</label>
					</div>
					
					<div class='row' style="margin-top:5px;">
				   
					<label class="col-md-12  control-label" >Select Option : </label>
					</div>
					<div class='row'>
					<div class="col-md-12" >
					<select class="form-control" id="ctype1" name="ctype1" >
					<option value="0" <?php if($ctype1=="0") echo "selected";?> >-----</option>
					<option value="1" <?php if($ctype1=="1") echo "selected";?>>INDIVIDUAL</option>
					<option value="2" <?php if($ctype1=="2") echo "selected";?>>CORPORATE</option>
					<option value="3" <?php if($ctype1=="3") echo "selected";?>>AGENTS</option>
					<option value="4" <?php if($ctype1=="4") echo "selected";?>>ALL</option>
					</select>
					</div>
					</div>
					</div>
										
					<div class="col-md-3" style="border-left:2px solid #e4e4e4;">
					<div class='row'>
					<label class="col-md-12 control-label " style="font-size:16px;font-weight:700;color:#4d8c24; " >Corporate Clients :</label>
					</div>
					
					<div class='row' style="margin-top:5px;">
				   
					<label class="col-md-12  control-label" >Select Option : </label>
					</div>
					<div class='row'>
					<div class="col-md-12" >
					<select class="form-control" id="corpclient" name="corpclient" >
					<option value="-1">-----</option>
					<option value="0" <?php if($corpid=="0") echo "selected";?> >ALL</option>
					
					<?php
					$qry=$this->db->select('*')->from('customers')->where('customer_type','2')->get()->result();
					foreach($qry as $q)
					{
						echo "<option value='". $q->customer_id ."'"; if($q->customer_id==$corpid) echo "selected"; echo ">". strtoupper($q->customer_name) ."</option>";
					}
					
					?>
					
					</select>
					</div>
					</div>
					
				    </div>
					
					<div class="col-md-2" style="border-left:2px solid #e4e4e4;">
						<input type='submit' class='btn btn-primary' style='margin-top:20px;height:50px;' name='btn_getDT' value='Get Details' id='btn_get'>
					</div>
					
					</form>
		           <label style="background-color:#cecece;width:100%;height:1px;"></label>
				</div>

  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->

<div class="portlet-body">
<div style="background-color:#fff;padding:15px; ">
              
		<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                        
        <!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
        
		<div class="row">
	   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
         <div class="portlet light portlet-fit bordered">
    
            <table class="table table-striped table-hover table-bordered" id="example"  width="100%">
              <thead>
                <tr>
                 <th>Trip_ID</th>
				 <th>Ref_No</th>
                 <th width='170px'>Client</th>
                 <th width='120px'>Vehicle</th>
				 <th width='120px'>Driver</th>
				 <th>Room/Guest</th>
                 <th width='80px'>Trip_Dates</th>
                 <th>Days</th>
                 <th>KM</th>
				 <th>Payment</th>
				 <th>Amount</th>
				 </tr>
                </thead>
				<tbody>
				<?php
				$gtot=0.00;
				if(isset($results))
				{
					
					foreach($results as $r)
					{
						$d1=$r->trip_startdate;
						  $dt1=date("d-m-Y", strtotime($d1));
						  $d2=$r->trip_enddate;
						  $dt2=date("d-m-Y", strtotime($d2));
								
						  $date1=date_create($r->trip_startdate);
								$date2=date_create($r->trip_enddate);
								$diff=date_diff($date1,$date2);
						  //$diff1=$diff->format("%R%a days");
						  $diff1=$diff->format("%a");
						  $km=$r->trip_endkm - $r->trip_startkm ;
						
						$gtot+=$r->trip_gtotal;
					?>				
					<tr>
					 <td><?=$r->trip_id;?></td>
					 <td><?=$r->trip_refno;?></td>
					 <td><?=$r->customer_name;?></td>
					 <td><?=$r->trip_vehicle_regno;?></td>
					 <td><?=$r->driver_name;?></td>
					 <td><?=$r->trip_roomno;?></td>
					 <td><?=$dt1."<font color='red'> =></font> ".$dt2;?></td>
					 <td><?=$diff1;?></td>
					 <td><?=$km;?></td>
					 <td><?=$r->trip_paymentmode;?></td>
					 <td><?=number_format($r->trip_gtotal,"2");?></td>
					 </tr>
				   <?php
					}
				}
				?>
				</tbody>
			
            </table>
	
        </div>
	
                       <!-- END BORDERED TABLE PORTLET-->
		</div>
		</div>
		<div class='row'>
		<hr style='margin-top:2px;'>
		</div>
		<div class='row'>
		<div class='col-md-9' style='text-align:right;'>
		<label style='font-size:20px;font-weight:700;padding-top:5px;'>Grand Total</label>
		</div>
		<div class='col-md-3'>
		<input type='text' class='form-control' style='font-size:25px;font-weight:700;text-align:right;' id='tgtotal' value='<?php echo number_format($gtot,"2",".","");?>' width='150px' readonly>
		</div>
		</div>
				
                <!-- END CONTENT BODY -->
	</div>
	</div>
            <!-- END QUICK SIDEBAR -->
</div>
  
                     <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div> 

 
      
  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
<!-- /.content --> 
</section>
  <!-- /.content-wrapper -->
</div>
 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

//setInterval(function(){ $('#idh5').html(''); }, 3000);
$('#example').dataTable();

$("#idmsg").hide();
$("#msgerr").hide();

//sweet alert ----------------------------
	if($.trim($("#idh5").html())!="")
	{
		swal( $("#idh5").html(),"","success")
		$("#idh5").html("");
	}
	
	if($.trim($("#msgerr").html())!="")
	{
		swal($("#idh5").html(),"","error")
		$("#idh5").html("");
	}
	
// sweet alert box -------------------------- 

 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
    
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });

  
$("#ctype").change(function(){
      $('#ctype1').val("0");
	  $('#corpclient').val("-1");
 });
 
 $("#ctype1").change(function(){
      $('#ctype').val("0");
	  $('#corpclient').val("-1");
 });
 
 $("#corpclient").change(function(){
      $('#ctype').val("0");
	  $('#ctype1').val("0");
 });
 
 function check_date()
 {
	 var res=true;
	 var d1=$("#datepicker1").val();
     var d2=$("#datepicker2").val();
	if(d1=="----" || d2=="----")
	{
		alert("Please select dates. Thank you.");
		res=false;
	}
	return res;
 }
      
   
$('#view-pdf').on('click',function(){
  
   //var id=$('#example').find('tr.selected').find('td').eq(2).text();
  
   var cty=$('#ctype').val();
   var cty1=$('#ctype1').val();		  
   var cty2=$('#corpclient').val();

   var dt1=$("#datepicker1").val();
   var dt2=$("#datepicker2").val();
     
	
	if(cty!='0')
	{
		var pdf_link = $(this).attr('href')+'/'+cty+'/1/'+dt1+'/'+dt2;
	}
	else if(cty1!='0')
	{
		var pdf_link = $(this).attr('href')+'/'+cty1+'/2/'+dt1+'/'+dt2;
	}
    else if(cty2!='-1')
	{
		var pdf_link = $(this).attr('href')+'/'+cty2+'/4/'+dt1+'/'+dt2;
	}

      // alert ("please Select staff details..");
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Logs Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
     return false; 
});    
   
   
   
  
/*

 $("#ctype").change(function(){

	var Table=  $('#example').dataTable();
	
	var dt1=$("#datepicker1").val();
	var dt2=$("#datepicker2").val();

	  
	
	if(dt1=="----" || dt1=="" || dt2=="----" || dt2=="" )
	{
	  swal("Try Again","Please select start & end Dates","error");
	  $('#ctype').val("0");
	  $('#ctype1').val("0");
	  $('#corpclient').val("0");
	}
	else
	{
		
	  $('#ctype1').val("0");
	  $('#corpclient').val("0");

      var cty=$('#ctype').val();
	  var sec=1;
	  $("#viewpdf").prop('disabled',false);
	  
	  $("#ctype1").val("0");
	  $("#tgtotal").val("0");
	  
	if(cty!='0')
	{		
    $('#example').DataTable({
        "ordering":false,
        "destroy": true,
        "processing": true,
		
		    // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip_reports/trip_report_ajax/"+cty+"/"+sec+"/"+dt1+"/"+dt2,// json datasource
               },
			   
	      "initComplete": function (settings, json) {//here is the tricky part 
			       //var count = $('#example tr').length-1;
 					//alert(count);
					
					
					
					
					get_total(cty,sec,dt1,dt2);
					
              },
			   
	"columnDefs":[
	{"width":"20%","tragets":2},
	{"width":"30%","tragets":4},
	],
		
 "aoColumnDefs": [
      { "sClass": "numericCol", "aTargets": [7,9] }
    ],	
  
  
    "columns": [
      { "data": "tripid" },
      { "data": "trip_refno" },
	  { "data": "customer" },
      { "data": "vehicle" },
      { "data": "driver" },
	  { "data": "roomno" },
	  { "data": "date" },
      { "data": "days"},
      { "data": "totalkm"},
	  { "data": "paymode" },
      { "data": "gtotal"},
        ]
	});
	}
	
	}
  });
  
    
  $("#ctype1").change(function(){
	   
	   
      var cty=$('#ctype1').val();
	  var sec="2";
	  
	  var dt1=$("#datepicker1").val();
	  var dt2=$("#datepicker2").val();

	  if(dt1=="----" || dt1=="" || dt2=="----" || dt2=="" )
	  {
		  swal("Try Again","Please select start & end Dates","error");
		  $('#ctype1').val("0");
		  $('#ctype').val("0");
		  $('#corpclient').val("0");
		  
	  }
	  else
	  {
	  $('#ctype').val("0");
	  $('#corpclient').val("0");
	  
	  $("#viewpdf").prop('disabled',false);
	  
	  $("#tgtotal").val("0");
	  
	  if(cty!='0')
	  {
	  
	  table=$('#example').dataTable({
         "ordering":false,
         "destroy": true,
        "processing": true,
		
		    // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip_reports/trip_report_ajax/"+cty+"/"+sec+"/"+dt1+"/"+dt2,// json datasource
               },
		   "initComplete": function (settings, json) {//here is the tricky part 
			   //var count = $('#example tr').length-1;
				//alert(count);
				get_total(cty,sec,dt1,dt2);
				
		  },
			   
	"columnDefs":[
	{"width":"25%","tragets":2},
	{"width":"25%","tragets":4},
	],
	
	"aoColumnDefs": [
      { "sClass": "numericCol", "aTargets": [7,9] }
    ],	
	
    "columns": [
      { "data": "tripid" },
	  { "data": "trip_refno" },
      { "data": "customer" },
      { "data": "vehicle" },
      { "data": "driver" },
	  { "data": "roomno" },
	  { "data": "date" },
      { "data": "days"},
      { "data": "totalkm"},
	  { "data": "paymode" },
      { "data": "gtotal"},
        ]
   });
	  }
  }
  
  });


  
   $("#corpclient").change(function(){

	
	var dt1=$("#datepicker1").val();
	var dt2=$("#datepicker2").val();
	
	if(dt1=="----" || dt1=="" || dt2=="----" || dt2=="" )
	{
	  swal("Try Again","Please select start & end Dates","error");
	  $('#ctype').val("0");
	  $('#ctype1').val("0");
	  $('#corpclient').val("0");
	}
	else
	{
      var cty=$('#corpclient').val();
	  var sec=4;
	  
	  $("#viewpdf").prop('disabled',false);
	  
	  $("#ctype1").val("0");
	  $('#ctype').val("0");
	  
	  $("#tgtotal").val("0");
	  
	if(cty!='0')
	{		
    $('#example').DataTable({
        "ordering":false,
        "destroy": true,
        "processing": true,
		
		    // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip_reports/trip_corpclient_ajax/"+cty+"/"+dt1+"/"+dt2,// json datasource
               },
			   
	      "initComplete": function (settings, json) {//here is the tricky part 
			       //var count = $('#example tr').length-1;
 					//alert(count);
					get_corp_total(cty,dt1,dt2);
					
              },
			   
	"columnDefs":[
	{"width":"25%","tragets":2},
	{"width":"30%","tragets":4},
	],
		
 "aoColumnDefs": [
      { "sClass": "numericCol", "aTargets": [7,9] }
    ],	
  
  
    "columns": [
      { "data": "tripid" },
	  { "data": "trip_refno" },
      { "data": "customer" },
      { "data": "vehicle" },
      { "data": "driver" },
	  { "data": "roomno" },
	  { "data": "date" },
      { "data": "days"},
      { "data": "totalkm"},
	  { "data": "paymode" },
      { "data": "gtotal"},
        ]
	});
	}
	
	}
  });
  
  
/*  
  
 
  
    $('#idsave').on('click',function(){
       //var id=$('#example').find('tr.selected').find('td').eq(2).text();
          var dt1=$('#datepicker1').val();
          var m=$('#lmonth').val();
       var pdf_link = $(this).attr('href')+'/'+m+'/'+dt1;
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
		window.open(pdf_link);
      return false; 
    
}); 





 $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
        var id =  $(this).attr('id');

		 jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/edit_booking",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 		 
  
  
    $('#idview').click(function () 
    {
    var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
        //var id =  $(this).attr('id');
    var id=$('#example').find('tr.selected').find('td').eq(0).text();
    if(id=="")
    {
       alert ("please Select customer details..");
    }
    else
    {
		$('#idview').attr('data-target','#myModal2'); 
		
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Customer/view_customer",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
            }
          });
    }
        }); 
    

	
// -----------------------------------------------------------------------------------------------------------------------------------    
    
  $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this booking?');
        });

		
		$(document).on("click", "#del_conf1", function () {
            return confirm('Are you sure you want to Cancel this booking?');
        });

*/
		
</script>



