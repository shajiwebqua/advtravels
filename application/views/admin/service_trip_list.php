<?php
 include('application/views/common/header.php');
 ?>
<!-- <link rel="stylesheet" href="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.css');?>" rel="stylesheet" type="text/css" />
 <script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
 <script src="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.js');?>"></script> -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/bootstrap/bootstrap.min.css');?>">-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/font-awesome/css/font-awesome.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/search_tabs/selectize.css');?>">
  
 
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id='bdy'> 
  
  <!-- Main content -->
    
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Service Trip Sheet</b></h1>
    <ol class="breadcrumb">
	</ol> 
    </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
  <!-- Content Header end -->
  <section class="content"> 
 
  <div class="row" >
  <div class="col-md-12">

  <!--Add new user details -->
<div style="background-color:#fff;border-radius:3px;padding:15px; ">

		   <div id='idmsg'style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id="msg"><?php echo $this->session->flashdata('message'); ?></div>
				 <div id="msgerr"><?php echo $this->session->flashdata('message1'); ?></div></center>
				 </div>
				 
 <div class="portlet-body form" >


 <div class="row">
 <div class="col-md-12" style='border-right:1px solid #e4e4e4;'> 

 <div class='row'>
 <div class='col-md-7'>
 <label style='margin-left:15px;'><b>Service Trip List</b> </label>
 </div>
 <div class='col-md-5' style='text-align:right;'>
  <a href="<?php echo base_url('Services/index');?>"><button class='btn btn-warning'>Add Service Invoice</button></a>
 <a href="<?php echo base_url('Services/trip');?>"><button class='btn btn-warning'>Service Trip</button></a>
 </div>
 </div>
 <hr style='margin:5px 0px 10px 0px;'>
 
 <table class="table table-striped table-hover table-bordered" id="example" style='color:#000;font-size:13px;'>
					<thead>
					<tr>
					<!-- <th>Edit</th> -->
					 <th>Action</th>
					 <th>ID</th>
					 <th>Vehicle</th>
					 <th>Driver</th>
					 <th>Date</th>
					 <th>Time</th>
					 <th>From</th>
					 <th>Service_Centre</th>
					 <th>Required Services</th>
					 <th>Kilometers</th>
					 <th>Status</th>
					</tr>
					</thead>
                </table>
  </div>
  </div>
	  
</div>

</div>
 </div>
 
  <!-- /.content --> 

</section>
</div>
<!-- /.content-wrapper --> 

<!-- Modal Dialog boxe-------------------->
<div class="modal fade draggable-modal" id="myModalSC" tabindex="-1" role="basic" aria- hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times </button>
				<h4 class="modal-title">Vehicle Service Closing</h4>
			</div>
			
			<!-- content -------------------------------------------->
			
			<div style="background-color:#fff;padding:10px; ">
   
				<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Services/close_service_entry')?>"> 
                  
				  <input type='hidden' name='vehid' id='vehid'>
				  
						<div class="form-group">
							 <div class="row" style="margin-top:0px;">
									<label class="col-md-4 control-label" >Select Option</label>
								   <div class="col-md-4 col-sm-12">
								   <select name='srstatus' class='form-control' required>
								   <option value="">----------</option>
								   <option value="2">Completed</option>
								   </select>
								 </div>
							 </div>
                        </div>
  
  
						<div class="form-group" >
							 <div class="row" style="margin-top:0px;">
								<label class="col-md-4 control-label" >Current KM</label>
								 <div class="col-md-4 col-sm-12">
								    <input type="text" class="form-control"  name="currkm"  id='currkm' required>
								 </div>
							 </div>
                        </div>
 
 
						<div class="form-group">
							 <div class="row">
								<label class="col-md-4 control-label" >Return Date</label>
								 <div class="col-md-4 col-sm-12">
								   <input class='form-control' type='text' data-provide='datepicker' name='srdate' id='srdate'>
								 </div>
							 </div>
                        </div>
                    <hr>

                    <div class="form-group">
                     <div class="row"><center>
                            <button class="btn btn-primary" id='submitagent'>Submit</button>
								   &nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
								   </center>
                           </div>
                       </div>
					   
                      </form>
                       </div>

				<!------------------------------------------------------------>
		</div>
		</div>
	<!-- /.modal-dialog -->
	</div>
</div>
<!-- end Dialog box ---------------------->


<?php include('application/views/common/footer.php');?>


<script type="text/javascript">

$('#srdate').datepicker({
	format:'dd-mm-yyyy',
	autoclose:true,
	todayHighlight:true
});


$("#example").DataTable();

$("#idextrakm").hide();

$("#idmsg").hide();
$("#msg").hide();
$("#msgerr").hide();
if($("#msg").html()!="")
	{
		swal("Saved",$("#msg").html(),"success")
		$("#msg").html("");
	}
	if($("#msgerr").html()!="")
	{
		swal("Try Again",$("#msgerr").html(),"error")
		$("#msgerr").html("");
	}
//-------------------------------------

//search customer

$('#id2').selectize({
     create: false
     , sortField: {
     //field: 'text',
     direction: 'asc'
   }
   , dropdownParent: 'body'
   });
   
       $('#example').dataTable({
         destroy: true,
        "processing": true,
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Services/service_trip_ajax",// json datasource
               },
	"columnDefs":[
	//{"width":"7%","targets":0},
	],
        
        "columns": [
            { "data": "stclose" },
            { "data": "id" },
            { "data": "vehicle"},
			{ "data": "driver"},
            { "data": "sdate" },
			{ "data": "stime" },
			{ "data": "vfrom" },
			{ "data": "scentre" },
			{ "data": "rservice" },
			{ "data": "startkm" },
			{ "data": "status" },
       ]
  } );
   
  
 $('#example tbody').on('click', '.stclose', function () 
{
	var vid=$(this).attr('id');
		$(".stclose").attr("data-target","#myModalSC");
		$("#vehid").val(vid);

});
 
 
 
$('#btncheck').click(function () {
	
	//var Result=$("#myModal3 .modal-body");
	
	var km=$("#startkm").val();
	var vid=$("#id2").val();
	
	if(km=="" && vid=="")
	{
		swal("Try Again","Select Vehicle and Strting KM.","error");
	}
	else
	{
		//alert(km+"-"+vid);
		if (vid=='')
		{
			swal("Try Again","Please select vehicle..!","error");
			//alert("Please select vehicle...");	
			
		}
		else if(km=='')
		{
			//alert("Please type stating kilometer...");	
			swal("Try Again","Please type starting kilometer.","error");
		}
		else
		{
			
			$("#idextrakm").show();
			$("#btncheck").attr("data-target","#myModal3");
			jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "Trip/check_kilometer",
			dataType: 'html',
			data: {skm:km,vid:vid},
			success: function(res) {
				
				if(res=="0")
				{
					$("#extrakm").html("Ending Km= <b>" + km + "</b><br>No diffrence, Continue.");
					$("#btnreason").hide();
				}
				else
				{
				$("#btnreason").show();	
				$("#extrakm").html(res);
				}
			//Result.html(res);
			}
			});
		}
	}
});


$("#btnreason").click(function()
{
	var Result=$("#myModal3 .modal-body");
	var km=$("#startkm").val();
	var vid=$("#id2").val();

		$("#btnreason").attr("data-target","#myModal3");
		
	    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/set_extra_km_reason",
        dataType: 'html',
        data: {skm:km,vid:vid},
        success: function(res) {
		Result.html(res);
        }
		});
});

//-----------------------------------------

 $(document).on("click", "#del_conf1", function () {
    return confirm('Are you sure you want to delete this entry?');
  });

</script>

<script>
$('#example1 tbody').on('click', '.select', function () {
 var Result=$("#myModals .modal-body");
 var id =  $(this).attr('id');
 $('#regno').val( id );
 $('#myModals').modal('hide')
});
</script>
