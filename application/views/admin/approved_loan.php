<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}
.mtop
{
	margin-top:5px;
}

</style>
 
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Approved Loan Details</b></h1>
<ol class="breadcrumb" style='font-size:15px;'>
<!--<li><a id='view-pdf' href="<?php echo site_url('Pdf/loan') ?>" style='color:#4b88ed;'><i class="fa fa-print" aria-hidden="true"></i>Print</a></li>
<!-- data-target="#myModal1" --><!--<li><a id='idsave' href="" style='color:#4b88ed;'><i class="fa fa-save" aria-hidden="true"></i>Save</a></li>
<li><a id='iddelete' href="" style='color:#4b88ed;'><i class="fa fa-trash" aria-hidden="true"></i>Delete</a></li> -->
</ol> 
 </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header  end -->	  

	  
  <section class="content"> 
  <div style="color:blue;font-size:12px;"> Click on the row to display details </div>
        <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
		</div>  
  <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details-->
	
	 <div style="background-color:#fff;border-radius:10px;padding:15px; ">
					
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">

				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                   <div class="row">
                   <div class="col-md-8">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                       <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="loan_master_list">
                        <thead>
								<tr style='color:#4b88ed'>
								 <th>SL/No</th>
								 <th>LTL Ref.No</th>
								 <th>Amount</th>
								 <th>Intrest%</th>
								 <th>Period</th>
								<th>Shedules</th>
								<th>Re-Payment</th>
								 </tr>
								</thead>
                        </table>
                        </div>
						
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
						<div class='col-md-4' style='border-left:1px solid #c4c4c4;'>
						
							<div class='row mtop'>
							<div class='col-md-12'><label style='font-size:18px;font-weight:bold;'>Details</label></div>
							</div>
							<div class='row '>
							<label style='background-color:#c4c4c4;width:100%;height:1px;padding-top:0px;'></label>
							</div>
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>LTL Ref. No.</label>
							<div class='col-md-8'><input type='text' class='form-control' id='ltlrf' name='ltlrf'> </div>
							</div>
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Client Name</label>
							<div class='col-md-8'><input type='text' class='form-control' id='cname' name='cname'> </div>
							</div>
						
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Address</label>
							<div class='col-md-8'><textarea rows='2' class='form-control' id='add' name='add'></textarea> </div>
							</div>
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Mobile No.</label>
							<div class='col-md-8'><input type='text' class='form-control' id='mob' name='mob'> </div>
							</div>
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Land Line.</label>
							<div class='col-md-8'><input type='text' class='form-control' id='phone' name='phone'> </div>
							</div>
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Loan Amount</label>
							<div class='col-md-8'><input type='text' class='form-control' id='lamt' name='lamt'> </div>
							</div>
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Interest Rate</label>
							<div class='col-md-8'><input type='text' class='form-control' id='irate' name='irate'> </div>
							</div>
						
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Period</label>
							<div class='col-md-8'><input type='text' class='form-control' id='period' name='period'> </div>
							</div>
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Interest Amt.</label>
							<div class='col-md-8'><input type='text' class='form-control' id='intamt' name='intamt'> </div>
							</div>
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Installment</label>
							<div class='col-md-8'><input type='text' class='form-control' id='install' name='install'> </div>
							</div>
							
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Bank Name</label>
							<div class='col-md-8'><input type='text' class='form-control' id='bname' name='bname'> </div>
							</div>
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>A/C Number</label>
							<div class='col-md-8'><input type='text' class='form-control' id='accno' name='accno'> </div>
							</div>
							
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Debit A/C</label>
							<div class='col-md-8'><input type='text' class='form-control' id='debitac' name='debitac'> </div>
							</div>
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Interest A/C</label>
							<div class='col-md-8'><input type='text' class='form-control' id='intrestac' name='intrestac'> </div>
							</div>
							
							<div class='row mtop'>
							<label class='col-md-4 control-label'>Loan A/C</label>
							<div class='col-md-8'><input type='text' class='form-control' id='loanac' name='loanac'> </div>
							</div>
							
							
						
						</div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>

 <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
  
          <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Loan Schedules</h4>
	              </div>
               <div class="modal-body" id="model-body1">
			   
               </div>
              </div>
            </div>
          </div>			

           <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content" id="content1">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
			  
		
            <!-- /.modal-dialog -->
            </div>
            </div>  

            <div class="modal fade draggable-modal" id="myModals" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Share</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
            </div>     
            </div>       

				
	</div>
	<!-- End user details -->

    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------

 setTimeout(function(){ $("#idh5").html(""); }, 3000);
 
  
 
$('#loan_master_list').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "loan/loanapproved_ajax",// json datasource
               },
			   "columnDefs":[
				{"width":"5%","targets":0},
				{"width":"12%","targets":2},
				{"width":"10%","targets":3},
				{"width":"10%","targets":4},
			     {"width":"5%","targets":5},
			   ],
			   
        "columns": [
			{ "data": "slno" },
			{ "data": "ltl_ref" },
			{ "data": "amount"},
			{ "data": "anual_intrest_rt" },
			{ "data": "loan_duration" },
		    { "data": "shedule"},
			{ "data": "repayment"},
       ]
  } );

var table = $('#loan_master_list').DataTable();
     $('#loan_master_list tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });		


/*$('#loan_master_list tbody').on('click', '.rpay', function () 
{
		var ltlrno =  $(this).attr('id');
		jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Loan/repayment",
        dataType: 'html',
        data: {lrno:ltlrno},
        success: function(res) {
          }
            });
    
        });*/
   

function loadshedule(obj){
  ltl_ref = $(obj).attr("data-value");
  jQuery.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "Loan/approved_loan",
          dataType: 'html',
          data: {ltl_ref: ltl_ref},
          success: function(res)
		  {
            $("#model-body1").empty();
            $("#model-body1").append(res);
            $("#myModal2").modal();
          }
  });
}


$('#loan_master_list tbody').on('click','tr',function () 
    {
        var ltlref=$('#loan_master_list').find('tr.selected').find('td').eq(1).text();
		//alert(ltlref);
		
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Loan/get_details",
        dataType: 'html',
        data: {ltl:ltlref},
        success: function(res)
			{
				var obj = jQuery.parseJSON(res);
				$("#ltlrf").val(obj.ltlref);
				$("#cname").val(obj.name);
				$("#add").val(obj.address);
				$("#mob").val(obj.mobile);
				$("#phone").val(obj.phone);
				$("#lamt").val(obj.amount);
				$("#irate").val(obj.intrest);
				$("#period").val(obj.period);
				$("#bname").val(obj.bankname);
				$("#accno").val(obj.bankacc);
				$("#debitac").val(obj.debitac);
				$("#intrestac").val(obj.intrestac);
				$("#loanac").val(obj.loanac);
				$("#intamt").val(obj.intamt);
				$("#install").val(obj.installment);
		
			/*$.each(res, function(idx, obj) {
				alert(obj.name);
			});*/
			}
        });
        });     
	



  
  $('#view-pdf').on('click',function(){
       var id=$('#example').find('tr.selected').find('td').eq(1).text();
     if(id=="")
     {
       alert ("please Select customer details..");
     }
     else
     {
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
		var pdf_link = $(this).attr('href')+'/'+id;
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Customer Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
       } return false; 

    });    

  
      $('#idedit').click(function () {
        var id=$('#example').find('tr.selected').find('td').eq(1).text();
		alert(id);
		 if(id=="")
		 {
			 alert ("please Select staff details..");
		 }
		 else
		 {
	
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/edit_trip/"+id,
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
			
                   }
            });
		 }
        }); 

		
  $('#idview').click(function ()
  {
        var Result=$("#myModal1 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(id=="")
		 {
			 alert ("please Select staff details..");
		 }
		 else
		 {
			$("#idview").attr('data-target','#myModal1') 
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/trip_details",
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
		 }
        }); 		


       $('#idshare').click(function () {
        var Result=$("#myModals .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
        var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(id=="")
		 {
			 alert ("please Select staff details..");
		 }
		 else
		 {
			$("#idshare").attr('data-target','#myModals') 
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/share_trip_details",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
			 Result.html(res);
                    }
            });
		 }
    
        });  

		
  $('#idsave').click(function ()
    {   
         var id=$('#example').find('tr.selected').find('td').eq(1).text();
         if(id=="")
         {
             alert ("please Select trip details..");
         }
         else
         {
            jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "Trip/save_trip",
            dataType: 'html',
            data:{tid:id},
            success: function(res) {
				alert("Trip sheet saved...!");
             }
            });
         }
        }); 
		
		
		
		
		
		
	$('#iddelete').click(function () 
    {
    var res=confirm("Delete this coustomer details?");
    if(res)
    {
    var id=$('#example').find('tr.selected').find('td').eq(1).text();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Customer/del_entry",
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    }
        });     
	
	
	
	
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });
 //});
  


</script>



