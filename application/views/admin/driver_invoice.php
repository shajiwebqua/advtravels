<?php
 include("application/views/common/header.php");
 ?>
<div class="wrapper">
  <div class="content-wrapper">
  <section class="content-header">
    <h1><b>Invoice</b></h1>
    <ol class="breadcrumb">
   <li> <a href="<?php echo base_url('Driver/driver_payment');?>" style="color:#4b88ed;font-size:15px;"> <i class="fa fa-backward" aria-hidden="true"></i>Back to Driver List</a></li>
    </ol> 
    </section>
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            Drivers Bill Payment
            <small class="pull-right">
              <div class='form-group'>
                <label class='col-md-3 control-label'>Date : </label>
                <div class='col-md-9'>
                  <input type='date' class='form-control' style="text-align:right;">
                </div>
              </div>
            </small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>Advance World Group</strong><br>
             Place , City<br>
            State, pin - 94107<br>
            Phone: (804) 123-5432<br>
            Email: info@almasaeedstudio.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-md-4"></div>
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?php echo $driver[0]->driver_name ?></strong><br>
            <!-- <?php
              $str = $driver[0]->driver_address;
              $str = explode(" ",$str);
              $length = 0;
              for($i=0; $i < sizeof($str); $i++){
                $length += strlen($str[$i]);
                if($i + 1 == sizeof($str)){
                  echo $str[$i];
                  echo "<br>";
                  $length++;
                }
                else if($length < 50){
                  echo $str[$i];
                  echo " ";
                  $length++;
                }
                else{
                  echo " <br />" ;
                 echo $str[$i];
                 echo " ";
                 $length = strlen($str[$i]) + 1;
                }
              }
            ?>
            Phone: <?php echo $driver[0]->driver_mobile; ?><br>
            Email: <?php echo $driver[0]->driver_email; ?> -->
          </address>
        </div>
        <!-- /.col -->
      </div>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>#</th>
              <th>Place</th>
              <th>Date</th>
              <th>Days</th>
              <th>Trip Charge</th>
              <th>DriverCharge</th>
              <th>Batha</th>
              <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <?php 
              /*$i = 1;
              $subtotal = 0;
              foreach($driver_payments as $driver_payment){
                $startdate    = date_create($driver_payment->start_date);
                $enddate      = date_create($driver_payment->end_date);
                $no_of_days   = date_diff($startdate,$enddate);

                $driver_charge = $driver_payment->driver_trip_charge;
                $driver_bata = ($driver_payment->driver_bata * (int)$no_of_days->days);
                $total = $driver_charge + $driver_bata;
                  echo "
                        <tr>
                          <td>". $i++ ."</td>
                          <td>". $driver_payment->trip_to ."</td>
                          <td>". $driver_payment->start_date  ."<span style='color:red'><i>  to  </i></span>". $driver_payment->end_date ."</td>
                          <td>". $no_of_days->format("%a") ."</td>
                          <td>". $driver_payment->trip_charge ."</td>
                          <td>". $driver_charge ."</td>
                          <td>". $driver_bata ."</td>
                          <td>". $total ."</td>
                        </tr>
                  ";

                  $subtotal += $total;
              }

*/
             ?>
            
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6"></div>
        <!-- /.col -->
        <div class="col-xs-5">
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td style="text-align: right;"><?php //echo $subtotal; ?></td>
              </tr>
                <th>Payment</th>
                <td><input type='numbers' class='form-control' style="text-align:right;"></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <?php include("application/views/common/footer.php");?>