<?php
 include('application/views/common/header.php');
 ?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/font-awesome/css/font-awesome.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/search_tabs/selectize.css');?>">
  
 
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.ttext
{
	background-color:#fff;
	font-weight:bold;
	color:Green;
}

.search-btn
{
	padding-top:3px;
	z-index:1000;
	right:17px;
	position:absolute;
}

.select_btn1 ,.select_btn2{
  
    position: absolute;
    top: 3px;
    z-index: 999;
   
}
.select_btn1{
	 right: 19px;
}
.select_btn2 {
    right: 19px;
}

.lblmap
{
	background-color:#e6e6e6;
	padding-top:12px;
	padding-bottom:12px;
	height:45px;
}

/* radio button ----------------*/

#m-radio {
	margin: 0 0px;
}

#m-radio label {
  width: 200px;
  border-radius: 3px;
  border: 1px solid #D1D3D4
  	color:#888 ;
}

/* hide input */
#m-radio input.radio:empty {
	margin-left: -999px;
}

/* style label */
#m-radio input.radio:empty ~ label {
	position: relative;
	float: left;
	line-height: 2.5em;
	text-indent: 3.25em;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

#m-radio input.radio:empty ~ label:before {
	position: absolute;
	display: block;
	top: 0;
	bottom: 0;
	left: 0;
	content: '';
	width: 2.5em;
	background: #D1D3D4;
	border-radius: 3px 0 0 3px;

}

/* toggle hover */
#m-radio input.radio:hover:not(:checked) ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #C2C2C2;

}

#m-radio input.radio:hover:not(:checked) ~ label {
	color:#777 ;
}

/* toggle on */
#m-radio input.radio:checked ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #9CE2AE;
	background-color: #4DCB6D;
}

#m-radio input.radio:checked ~ label {
	/*color: #888;*/
}

/* radio focus */
.form-horizontal .checkbox, .form-horizontal .radio {
    min-height: 0px;
}
.no-pad {
	padding-left: 0px;
	padding-right: 0px;
}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id='bdy'> 
  
  <!-- Main content -->
    
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Trip Booking</b></h1>
    <ol class="breadcrumb">
	<!--<li> <a href="#" data-toggle='modal' data-target='#myModalduty' style="color:#4b88ed;font-size:15px;"> <i class="fa fa-user" aria-hidden="true"></i>Duty List</a></li>
	<li> <a href="#" data-toggle='modal' data-target='#myModalfare' style="color:#4b88ed;font-size:15px;"> <i class="fa fa-cab" aria-hidden="true"></i>Fare List</a></li>
	-->
   <li> <a href="<?php echo base_url('Trip/booking_list');?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'> <i class="fa fa-backward" aria-hidden="true"></i> Booking List</button></a></li>
    
    </ol> 
    </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
  <!-- Content Header end -->
  <section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg'style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id="msg"><?php echo $this->session->flashdata('message'); ?></div>
				 <div id="msgerr"><?php echo $this->session->flashdata('message1'); ?></div>
				 </center>
				 </div>
			</div>
  
    <!-- Small boxes (Stat box) -->
    <div class="row">
  <div class="col-md-12">

  <!--Add new user details -->
<div style="background-color:#fff;border-radius:3px;padding:15px; ">
    
<!--  <div class="portlet-title">
      <div class="caption">
        <div style="color:blue;" >
            Trip Details                   
        </div> 
        </div>
       </div> -->
              
			  
 <div class="portlet-body form">
    <form id="#myForm" class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Trip/add_new_booking')?>" enctype="multipart/form-data"  onsubmit='return checkdata();'>

        <div class="row">
        <div class="col-md-12" > 
		<div class='form-group' >
                    <!-- <div class='row'> -->
          <label class='col-md-3 control-label' style='padding-top:2px;'>Client Category :</label>
            <div class='col-md-4'>
				<select id="category" class="form-control" placeholder="Client type" name="clienttype" required>
				  <option value="">----------</option>
				  <option value="1">INDIVIDUAL</option>
				  <option value="2">CORPORATE</option>
				  <option value="3">AGENT</option>
				</select>
			</div>
        </div>
								
		  <!-- <div class='form-group clientname'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Corporate Name : </label>
			  <div class='col-md-8'>
			  <input type='text' class='form-control' name='clientna' id='clientna'  value="" >
			  </div>
		  </div> -->

		  <div class='form-group' id='corp_na'>
          <label class='col-md-3 control-label' style='padding-top:2px;'>Corporate Name :</label>
           <div class='col-md-4'>
			  <section class="form-control " style='padding:0px;'>
				<select id="id3" class="form-controll_prop" placeholder="Choose Your Name" name='corpna' id='corpna'>
				  <option value=""></option>
				  <?php
					$cquery=$this->db->select("customer_name,customer_id")->from('customers')->where("customer_type","2")->get()->result();
					foreach( $cquery as $r)
					{
						echo "<option value='".$r->customer_id."'>".$r->customer_name."</option>";
					}
				  ?>
									  
				</select>
				
				</section>
				<div class="select_btn1">                              
				<a href="<?php echo base_url('Customer/add_tripcustomer/2');?>" data-target="#myModal2" data-toggle='modal'><button id='custadd' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span></button></a>
				</div>	
				</div>
         </div>
		 		  
 		<div class='form-group' id='agents'> 
		   <label class="col-md-3 control-label" id='agentlbl'>Agent Name</label>
			<div class="col-md-4">
			<input type="text" class="form-control" name="agent" id="agent">
			<input type="hidden" class="form-control" name="agentid" id="agentid">
		</div>
		
		<a href="#" id='agentselect' data-target="#myModalSA" data-toggle='modal'><input type='button' value='Select' class="btn btn-default btn-xs" name='agentselect'  style='margin-top:5px;'> </a>
			&nbsp;&nbsp;<a href="#" data-target="#myModalA" data-toggle='modal'><input type='button' value='Add' class="btn btn-default btn-xs" name='agentadd' id='agentadd' style='margin-top:5px;'> </a>
		</div>
					
		 <div class='form-group'  id='custna1'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Room No : </label>
			  <div class='col-md-4'>
			  <input type='text' class='form-control' name='roomno' id='roomno' >
			  </div>
         </div>
		 
		 
		  <div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Guest Name: </label>
			  <div class='col-md-4'>
			  <input type='text' class='form-control' name='guestna' id='guestna' >
			  </div>
        </div>
		 
		<div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Contact No: </label>
			  <div class='col-md-4'>
			  <input type='text' class='form-control' name='contactno' id='contactno' >
			  </div>
        </div>
				 
		<div class='form-group' style='margin-top:20px;'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Trip Date : </label>
			  <div class='col-md-3'>
			   <input type='date' class='form-control' name='tripdate' id='tripdate' required>
			  </div>
        </div>
		
		<div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Reporting Time: </label>
			  <div class='col-md-3'>
			  <input type='time' class='form-control' name='reptime' id='reptime' >
			  </div>
        </div>
		 
		 <!-- <div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Location: </label>
			  <div class='col-md-4'>
			  <input type='text' class='form-control' name='location' id='location' >
			  </div>
        </div> -->
		  
		<div class='form-group' style='margin-top:20px; background-color:#eff0f2;padding:5px'>
          <label class='col-md-3 control-label' style='padding-top:20px;'><b>Select Trip Option :</b> </label>
		  <div class='col-md-9'>
		  <div class='row' style='margin-left:5px;'>
		  <div id="m-radio" class="col-md-5 no-pad">
		  <input type="radio" name="sltrip" id="radio1" class="radio" value='ST' checked/>
			<label for="radio1" style='float:left;'><b>Short Trip (<100km)</b></label>
			</div>

			<div id="m-radio" class="col-md-5 no-pad">
			<input type="radio" name="sltrip" id="radio2" class="radio" value='LT'/>
			<label for="radio2" ><b>Out Station(>100km)</b></label>
			</div>
          </div>
		  
		  <div class='row' style='margin-left:5px;'>
		  <div id="m-radio" class="col-md-5 no-pad">
		  <input type="radio" name="sltrip" id="radio3" class="radio" value='AP'/>
			<label for="radio3" style='float:left;'><b>Airport Pick Up</b></label>
			</div>

			<div id="m-radio" class="col-md-5 no-pad">
			<input type="radio" name="sltrip" id="radio4" class="radio" value='AD'/>
			<label for="radio4" ><b>Airport Drop</b></label>
			</div>
          </div>
		  
		  <div class='row' style='margin-left:5px;'>
		  <div id="m-radio" class="col-md-5 no-pad">
		  <input type="radio" name="sltrip" id="radio5" class="radio" value='RP' />
			<label for="radio5" style='float:left;'><b>Railway Pick Up</b></label>
			</div>

			<div id="m-radio" class="col-md-5 no-pad">
			<input type="radio" name="sltrip" id="radio6" class="radio" value='RD'/>
			<label for="radio6" ><b>Railway Drop</b></label>
			</div>
          </div>
		  <div class='row' style='margin-left:5px;'>
		  <div id="m-radio" class="col-md-5 no-pad">
		  <input type="radio" name="sltrip" id="radio7" class="radio" value='FP' />
			<label for="radio7" style='float:left;'><b>Fixed Pacakge</b></label>
			</div>

			<div id="m-radio" class="col-md-4 no-pad">
			<input type="radio" name="sltrip" id="radio8" class="radio" value='CM'/>
			<label for="radio8" ><b>Complimentary</b></label>
			</div> 
          </div>
		  </div>
        </div>  
  		  		  
        <!--<div class='form-group' style='padding-top:20px;'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Trip From :</label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='tripfrom' id='tripfrom' required>
          </div>
        </div> -->
		
		 <div class='form-group' style='margin-top:15px;'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Location(From): </label>
			  <div class='col-md-4'>
			  <input type='text' class='form-control' name='location' id='location' >
			  </div>
        </div>
      
        <div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:5px;'>Trip To : </label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='tripto' id="tripto" required>
          </div>
        </div>
		
		<div class='form-group' style='margin-top:15px;'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Vehicle Category : </label>
			  <div class='col-md-4'>
			  <select class="form-control" name='vehiclecat' id='vehiclecat' required>
				  <option value="">------</option>
				  <option value="OWN">OWN</option>
				  <option value="ATTACHED">ATTACHED</option>
				</select>
			  </div>
        </div> 
	
				
		<div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:5px;'>Preffered Vehicle : </label>
          <div class='col-md-4'>
		 <select class="form-control" name='vehicle' id='vehicle'>
				  <option value="">----------</option>
				  <?php
					$cquery=$this->db->select("*")->from('vehicle_types')->get()->result();
					foreach( $cquery as $r)
					{
						echo "<option value='".$r->veh_type_id."'>".$r->veh_type_name."</option>";
					}
				  ?>
									  
				</select>
          </div>
        </div>
		
		 <div class='form-group' style='margin-top:15px;'>
          <label class='col-md-3 control-label ' style='padding-top:5px;'>Vehicle Reg.No: </label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='vehno' id="vehno" required>
          </div>
        </div>
		
		 <div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:5px;'>Driver Name : </label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='drname' id="drname" required>
          </div>
        </div>
		
		 <div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:5px;'>Mobile No : </label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='drmobile' id="drmobile" required>
          </div>
        </div>
		
		<div class='form-group' style='margin-top:15px;'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Payment: </label>
			  <div class='col-md-4'>
			  <select class="form-control" name='payment' id='payment' required>
				  <option value="">------</option>
				  <option value="CASH">CASH</option>
				  <option value="CREDIT">CREDIT</option>
				</select>
			  </div>
        </div> 
		
		 <div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Description: </label>
			  <div class='col-md-4'>
			  <textarea rows=4 class='form-control' name='desc' id='desc' ></textarea>
			  </div>
        </div>
		
	
		</div>

		
 <!-- second column --------------------------->
	  </div>
 


<label style="background-color:#cecece;width:100%;height:1px;"></label>

<div class='form-group'>
<label class='col-md-3 control-label '></label>
<div class='col-md-3' >
	<button type="submit" class="btn btn-success" style="padding:10px 50px 10px 50px;" id="idsavebtn" > <b>Save Booking</b></button>
</div>
</div>

</form>
</div>

 <label style="background-color:#cecece;width:100%;height:1px;"></label>
 
 </div>
 
   <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content" style='height:600px;'>
				<div class="modal-body" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit</h4>
				</div>
			</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
   </div>  
   
 
	<div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit</h4>
				</div>
			</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
    </div>  

 </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 

</section>
</div>

<!-- first Model for add agents -------------------------------------------------------->

 <div class="modal fade draggable-modal" id="myModalA" tabindex="-1" role="basic" aria- hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
				<h4 class="modal-title">Add New Agent Details</h4>
			</div>
			
			<!-- content -------------------------------------------->
			
			<div style="background-color:#fff;padding:10px; ">
   
				<!--<form onsubmit="return checkdata();" class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/add_agent')?>"> -->
                  
						<div class="form-group">
							 <div class="row" style="margin-top:0px;">
									<label class="col-md-3 control-label" style="padding-top:0px">Agent Code</label>
								   <div class="col-md-4">
								   <input type="text" class="form-control"  name="agent_code" id='acode' required>
								 </div>
							 </div>
                        </div>
  
						<div class="form-group" >
							 <div class="row" style="margin-top:0px;">
								<label class="col-md-3 control-label" style="padding-top:0px">Agent Name</label>
								 <div class="col-md-7">
								   <input type="text" class="form-control"  name="agent_name"  id='aname' required>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
								<label class="col-md-3 control-label" style="padding-top:0px">Address</label>
								 <div class="col-md-7">
								   <textarea rows=2 cols=30 class="form-control"  name="agent_address"  id='aaddress' required></textarea>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
							 	<label class="col-md-3 control-label" style="padding-top:0px">Mobile</label>
								 <div class="col-md-7">
								   <input type="number" pattern="[0-9]*" class="form-control"  name="agent_mobile" id="amobile" required>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
									<label class="col-md-3 control-label" style="padding-top:0px">Email</label>
							 	 <div class="col-md-7">
								   <input type="email" class="form-control"  name="agent_email" id='aemail' required>
								 </div>
							 </div>
                        </div>
                                 <hr>

                    <div class="form-group">
                     <div class="row"><center>
                            <button class="btn btn-primary" id='submitagent'>Submit</button>
								   &nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
								   </center>
                           </div>
                       </div>
					   
                       <!-- </form> -->
                       </div>

				<!------------------------------------------------------------>
		</div>
		</div>
	<!-- /.modal-dialog -->
	</div>
</div>

<!--- MODEL 2 DISPLAY AGENT LIST -------------------->

<div class="modal fade" id="myModalSA" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Agents List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class="col-md-12">
            <table id="tblagents" class="table table-striped table-hover table-bordered" cellspacing="0">
            <thead>
            <tr>
                <th>Select</th>
				<th>ID</th>
                <th>Agent Name</th>
                <th>Mobile</th>
            </tr>
        </thead>
    </table>
        </div>
      </div>
 	 </div>
 	</div>
   </div>
  </div>
  

<?php include('application/views/common/footer.php');?>


<script type="text/javascript">

$("#corp_na").hide();
$("#agents").hide();


$("#idmsg").hide();
if($.trim($("#msg").html())!="")
	{
		swal("Saved",$("#msg").html(),"success")
		$("#msg").html("");
	}

$("#msgerr").hide();
if($.trim($("#msgerr").html())!="")
	{
		swal("Try Again",$("#msgerr").html(),"error")
		$("#msgerr").html("");
	}	

	
 /*$("input[type='radio']").click(function(){
            var radioValue = $("input[name='sltrip']:checked").val();
			//alert(radioValue);
			if(radioValue=='LT')
			{	
				$("#hplace1").show();
				$("#hplace2").show();
				$("#hplace3").show();
				$("#pay_mode").show();
				$("#paymode").val("");
				$("#paymode1").val("");
			}
			if(radioValue=='CM')
			{	
				$("#pay_mode").hide();
				$("#paymode1").val('NONE');
			}
			else
			{
				$("#hplace1").hide();
				$("#hplace2").hide();
				$("#hplace3").hide();
				$("#pay_mode").show();
				$("#paymode").val("");
				$("#paymode1").val("");
			}
});*/


//-- driver list
//sweet alert box ----------------------


//-------------------------------------


//search customer

$('#id3').selectize({
     create: false
     , sortField: {
     //field: 'text',
     direction: 'asc'
   }
   , dropdownParent: 'body'
   });

  
 $("#category").change(function()
 {
	 var op=$("#category").val();
		
		if (op==1)
		{
		 $("#corp_na").hide();
		 $("#agents").hide();
		}
		else if(op==2)
		{	
		 $("#corp_na").show();
		 $("#agents").hide();
		}
		else if(op==3)
		{	
		$("#corp_na").hide();
		$("#agents").show();
		}
 });
  

//-----------------------------------------

$("#id2").change(function()
{
	    var id =  $("#id2").val();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_cust_mobile",
        dataType: 'html',
        data: {cid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#idcmobile").val(obj.mobile);
		       }
            });
});



//drop down script .................
//..................................

 
  $('#TblCustomer tbody').on('click', '#custadd', function () 
  {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         // var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Customer/add_customer",
        dataType: 'html',
        // data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
        }); 
 //---------------------------------------------------
  
 
$(document).ready(function () {
	//$("#idsavebtn").prop('disabled', true);
	$("#idbtnget").prop('disabled', true);
	//$("#idbtncalc").prop('disabled', true);
        setTimeout(function(){ $("#msg").html(""); }, 3000);
    }); 
	
</script>

<script>
$('#example1 tbody').on('click', '.select', function () {
 var Result=$("#myModals .modal-body");
 var id =  $(this).attr('id');
 $('#regno').val( id );
 $('#myModals').modal('hide')
});
</script>


<script>

/* -------- button desable/enanble ------------------*/


 function checkDate() {
            var stDate =new Date($("#startdate").val()) //for javascript
            var enDate =new Date($("#enddate").val()); // For JQuery
		
			var g=stDate-enDate;
            if (g>0) {
                return false;
            }
            else {
               return true;
            }
        }




  
  var table= $('#tblagents').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/agentlist_ajax",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"7%","targets":1},
		],
			   
        "columns": [
            { "data": "select"},
			{ "data": "agid"},
		    { "data": "agname"},
            { "data": "agmobile"},
       ]
  });
  
  
  
  
  $("#acode").blur(function(){
	if($("#acode").val()==""){$("#acode").css('border','1px solid red');}
	else {$("#acode").css('border','1px solid #c6c6c6');}
	
});

$("#aname").blur(function(){
	if($("#aname").val()==""){$("#aname").css('border','1px solid red');}
	else {$("#aname").css('border','1px solid #c6c6c6');}
});

$("#aaddress").blur(function(){
	if($("#aaddress").val()==""){$("#aaddress").css('border','1px solid red');}
	else {$("#aaddress").css('border','1px solid #c6c6c6');}
});
	
$("#amobile").blur(function(){
	if($("#amobile").val()==""){$("#amobile").css('border','1px solid red');}
	else {$("#amobile").css('border','1px solid #c6c6c6');}
});

$("#aemail").blur(function(){
	if($("#aemail").val()==""){$("#aemail").css('border','1px solid red');}
	else {$("#aemail").css('border','1px solid #c6c6c6');}
});


function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test($email );
}


$("#submitagent").click(function()
		{

		var acod=$("#acode").val();
		var ana=$("#aname").val();
		var aadd=$("#aaddress").val();
		var amob=$("#amobile").val();
		var aema=$("#aemail").val();

		var res1=true;
		var res2=true;
		var res3=true;
		var res4=true;
		var res5=true;
		var res6=true;
		

if(acod==""){res1=false;$("#acode").css('border','1px solid red'); }else{res1=true;$("#acode").css('border','1px solid #c6c6c6');}
if(ana==""){res2=false;$("#aname").css('border','1px solid red');}else{res2=true;$("#aname").css('border','1px solid #c6c6c6');}
if(aadd==""){res3=false;$("#aaddress").css('border','1px solid red');}else{res3=true;$("#aaddress").css('border','1px solid #c6c6c6');}
if(amob==""){res4=false;$("#amobile").css('border','1px solid red');}else{res4=true;$("#amobile").css('border','1px solid #c6c6c6');}
if(aema==""){res5=false;$("#aemail").css('border','1px solid red');}else{res5=true;$("#aemail").css('border','1px solid #c6c6c6');}
	
		if(res1!=false && res2!=false && res3!=false && res4!=false && res5!=false )
			{
	
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "General/add_agent1",
				dataType: 'html',
				data: {acode:acod,aname:ana,aaddress:aadd,amobile:amob,aemail:aema},
				success: function(data) 
				 {
					 
				  if(parseInt(data)>0)
				  {
					  $("#agentid").val(data);
					  $("#agent").val(ana);
					  swal("Saved","Agent details saved..","success");
					  
				  }
				  else
				  {
					  swal("Cancelled","Agent details missing, Try again..","error");
				  }
				}
				});
			}
			else
			{
				swal("Missing","Agent details missing, Try again..","error");
			}
		});
	
	
	var table= $('#tblagents').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/agentlist_ajax",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"7%","targets":1},
		],
			   
        "columns": [
            { "data": "select"},
			{ "data": "agid"},
		    { "data": "agname"},
            { "data": "agmobile"},
       ]
  });
  
  
  $('#tblagents tbody').on('click', '.agselect', function ()
  {
	    var id =  $(this).attr('id');
		
		jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/get_agentsDT",
        dataType: 'html',
        data: {aid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#agent").val(obj.agname);
				$("#agentid").val(obj.agid);
		       }
            });
   }); 
  
  
   
  
  
  
</script>



 