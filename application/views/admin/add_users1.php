<style>
.left1
{
	padding-left:30px;
}
</style>
        <section class="content" style="padding-bottom:10px;">
		<div style="background-color:#fff;padding:15px;">
		   <div class="row">
		    <div class="col-md-12">
		           <h4>New user Details </h4>
		    </div>
		            <label style="background-color:#cecece;height:1px;width:100%"></label>
            <div id='response'>	<?php echo $this->session->flashdata('message'); ?></div>
                </div><!-- /.box-header -->
                <!-- form start -->
                 <?php echo form_open_multipart('userprofile/add_new_user');?>
				 
				 
                 <div class="box-body">
	            <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('userprofile/add_new_user')?>" enctype="multipart/form-data">
	
				  <div class="row">
				  <div class="col-md-9">
				  
					   
											<div class="form-group">
											<div class="row">
											<label class="col-md-4  control-label left1">Name</label>
											<div class="col-md-8">
                                                    <input type="text" class="form-control"  name="name" required>
                                                </div>
										    </div>
                                            </div>
										
										<div class="form-group">
										<div class="row">
                                                <label class="col-md-4  control-label left1">Phone</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"  name="phone" required>
                                                </div>
                                        </div>
										</div>

										
                                        <div class="form-group">
										<div class="row">
                                                <label class="col-md-4  control-label left1">Email</label>
                                                <div class="col-md-8">
                                                    <input type="email" class="form-control"  name="email" required>
                                                </div>
                                        </div>
										</div>
                                           

                                         <div class="form-group">
										 <div class="row">
                                                    <label class="col-md-4  control-label left1">Gender </label>
                                                    <div class="icheck-inline">
                                                    <label style="padding-left:10px;">
                                                     &nbsp;<input type="radio" name="gender" class="icheck" data-radio="iradio_square-grey" value="Male">&nbsp;Male</label> &nbsp;&nbsp;&nbsp;
                                                    <label>
                                                    <input type="radio" name="gender" class="icheck" data-radio="iradio_square-grey" value="Female">&nbsp;Female</label>
                                                    </div>
                                                    </div>
                                         </div>
										 <div class="form-group">
										 <div class="row">
                                        <label class="col-md-4   control-label left1" >Username</label>
                                        <div class="col-md-8">
                                           <input type="text" class="form-control"  name="username" required>
                                        </div>
                                        </div>
										</div>
										<div class="form-group">
										<div class="row">
                                        <label class="col-md-4 control-label left1" >Password</label>
                                        <div class="col-md-8">
                                           <input type="password" class="form-control"  name="userpass" required>
                                        </div>
                                        </div>
										</div>
                    
                                        <div class="form-group">
										<div class="row">
                                                <label class="col-md-4 control-label left1">User level</label>
                                                <div class="col-md-8">
                                                    <select name='userlevel' class='form-control'>";
                                                <option value='0'>----</option>
												<option value='1'>SuperAdmin</option>
												<option value='2'>Admin</option>
												<option value='3'>User</option>
												
                                                </select>
                                                </div>
                                        </div>
										</div>

                                        
							  </div>
							  <div class="col-md-3">
							  <div class="form-group">
											<div class="row">
                                                 <!--<label class="col-md-3  control-label left1">Photo</label>-->
												 </div>
												 <div class="row">
                                                 <div class="col-md-3">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="max-width:105px; max-height:110px;">
                                                                        <img src="<?php echo base_url('assets/dist/img/user.jpg')?>" alt="" /> </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width:105px; max-height:110px;"> </div>
                                                                    <div>
                                                                        <span class="btn btn-default btn-file">
                                                                            <span class="fileinput-new "> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="image"  > </span>
                                                                        <a href="javascript:;" class="btn btn-default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                                 </div>
                                                            </div>
								</div> <!-- image end -->
				</div>  <!-- col end -->
			</div> <!-- row end-->
</div><!-- /.box -->

							<label style="background-color:#cecece;height:1px;width:100%"></label>
										<div class="form-group">
                                            <div class="row">
											<div class="col-md-7">
											</div>
                                                <div class="col-md-4" >
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                    <button type="submit" class="btn btn-primary"  style="margin-left:10px;" data-dismiss="modal" >Close</button>
                                                </div>
                                            </div>
                                        </div>
			
			</form>
			
			
</div>   <!-- /.row -->
</section><!-- /.content -->
 
<script>
$("#ufile").change(function()
{
$("#response").html("");
});
</script>


  </body>
</html>