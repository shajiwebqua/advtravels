<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class userprofile extends CI_Controller {

function __construct(){
parent::__construct();
$user = $this->session->userdata('userid');
        if (!isset($user))
          { 
             redirect('/');
         } 
         else
         { 
            return true;
        }
$this->load->helper(array('form', 'url'));
$this->load->model('Model_userprofile');
}


public function index()
{
   $this->load->view('admin/add_users');
}
  
public function add_new_user()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('name', 'name', 'required');
$this->form_validation->set_rules('email', 'email', 'required');
$this->form_validation->set_rules('phone', 'phone', 'required');
$this->form_validation->set_rules('gender', 'gender', 'required');
$this->form_validation->set_rules('username', 'username', 'required');

$this->form_validation->set_rules('userpass', 'userpass', 'required|md5');
$this->form_validation->set_rules('userlevel', 'userlevel', 'required');

 if($this->form_validation->run())
        {
			if ($this->input->post('userlevel')<=0)
			{
				 $this->session->set_flashdata('message', '<font color="red">Some data is missing, try again..!</font>');
				 redirect('userprofile/index');  
			}
       if(isset($_FILES['image']) && $_FILES['image']['size']!=0){
            
        $config['upload_path'] ='./upload/images';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        //$image_info = getimagesize($_FILES['userfile']['tmp_name']);
        //$image_width=$image_info[0];
        //$image_height=$image_info[1];
        $this->load->library('upload', $config);
        // echo $image_width;

            if (  !$this->upload->do_upload('image'))
            {
                $error['error'] = array('error' => $this->upload->display_errors());
                 $this->load->view("admin/add_users",$error);
            }
            else
            {
    $name=$this->input->post('name');
    $email=$this->input->post('email');
    $phone=$this->input->post('phone');
    $gender=$this->input->post('gender');
    $username=$this->input->post('username');
    $userpass=$this->input->post('userpass');
    $userlevel=$this->input->post('userlevel');
	
	
	$upload_data = $this->upload->data();
    $file_name = $upload_data['file_name'];
    $fname=base_url('/upload/images')."/".$file_name;

       $userdt = array(
                'user_name' => $name,
                'user_image'=>$fname,
                'user_gender' => $gender,
				'user_mobile' =>$phone,
                'username' =>$username,
                'password' =>$userpass,
                'user_email' =>$email,
				'user_level' => $userlevel,
				'status'=>"1"
               );
         $this->load->Model('Model_userprofile');
        $this->Model_userprofile->form_insert($userdt);
        $this->session->set_flashdata('message', '<font color="green">User details successfully added</font>');
        redirect('userprofile/view_users');  
        }
    }
  }
 else{
         $this->session->set_flashdata('message', 'Data Not Inserted Successfully');
        //$data['message']="Data Inserted Successfully";

      redirect('userprofile/index');
       }  
}
 
 public function add_users()
  {
   echo " 
     <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>             
    <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Add User Details</font></h4>
    </div>
    <br>
      <form  class='form-horizontal' role='form' method='post' action='". site_url('Userprofile/add_new_user'). "' enctype='multipart/form-data' onsubmit='return checkdata();'>

 <!-- text input -->
              <div class='row'>
          <div class='col-md-8'>
          
             
                      <div class='form-group'>
                      <div class='row'>
                      <label class='col-md-4  control-label left1' style='padding-top:10px;'>Name</label>
                      <div class='col-md-8'>
                           <input type='text' class='form-control'  name='name' required>
                         </div>
                        </div>
                      </div>
                   
                    <div class='form-group'>
                    <div class='row'>
						<label class='col-md-4  control-label left1' style='padding-top:10px;'>Phone</label>
						<div class='col-md-8'>
							<input type='number' class='form-control'  name='phone' id='phone' required>
							 <label id='mobmessage' style='color:red;'></label>   
						</div>
					</div>
					</div>

                    
					<div class='form-group'>
                    <div class='row'>
						<label class='col-md-4  control-label left1' style='padding-top:10px;'>Email</label>
						<div class='col-md-8'>
							<input type='email' class='form-control'  name='email' required>
						</div>
					</div>
                    </div>
                                           

					 <div class='form-group'>
                     <div class='row'>
						<label class='col-md-4  control-label left1' style='padding-top:10px;'>Gender </label>
						<div class='icheck-inline'>
						<label style='padding-left:10px;'>
						 &nbsp;<input type='radio' name='gender' class='icheck' data-radio='iradio_square-grey' value='Male'>&nbsp;Male</label> &nbsp;&nbsp;&nbsp;
						<label>
						<input type='radio' name='gender' class='icheck' data-radio='iradio_square-grey' value='Female'>&nbsp;Female</label>
						</div>
						</div>
					</div>
                     <div class='form-group'>
                     <div class='row'>
						<label class='col-md-4   control-label left1' style='padding-top:10px;'>Username</label>
						<div class='col-md-8'>
						   <input type='text' class='form-control'  name='username' required>
						</div>
						</div>
                    </div>
                    <div class='form-group'>
                    <div class='row'>
						<label class='col-md-4 control-label left1' style='padding-top:10px;'>Password</label>
						<div class='col-md-8'>
						   <input type='password' class='form-control'  name='userpass' required>
						</div>
						</div>
                    </div>
                    
					<div class='form-group'>
                    <div class='row'>
						<label class='col-md-4 control-label left1' style='padding-top:10px;'>User level</label>
						<div class='col-md-8'>
							<select name='userlevel' class='form-control'>';
						<option value='0'>----</option>
                        <option value='1'>SuperAdmin</option>
                        <option value='2'>Admin</option>
                        <option value='3'>User</option>
                        
						</select>
						</div>
						</div>
                    </div>

                                        
                </div>
                <div class='col-md-3'>
                <div class='form-group'>
                      <div class='row'>
                                                 <!--<label class='col-md-3  control-label left1'>Photo</label>-->
                         </div>
                    <div class='col-md-4'>  
                     <div class='form-group' >
                       <div class='col-md-12'>
                          <div class='fileinput fileinput-new' data-provides='fileinput'>
                             <div class='fileinput-new thumbnail' style='max-width: 145px; max-height: 152px;''>
                              <img src='".base_url('assets/dist/img/user.jpg')."' alt='' /> </div>
                               <div class='fileinput-preview fileinput-exists thumbnail' style='max-width: 135px; max-height: 140px;'> </div>
                             <div>
                              <span class='btn btn-default btn-file'>
                              <span class='fileinput-new'> Select Photo </span>
                              <span class='fileinput-exists'> Change </span>
                              <input type='file' name='image' > </span>
                              <a href='javascript:;' class='btn btn-default fileinput-exists' data-dismiss='fileinput'> Remove </a>
                         </div>
                        </div>
                      </div>
                 </div> 
             </div>
 </div>
 </div>

              <label style='width:100%;background-color:#cecece;'></label>
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-8'>
                <input type='submit' class='btn btn-primary' value='Save Details'/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form> 
       <br>
      
       
         <script type='text/javascript'>
  
  function checkdata()
  {
    
    var mob=$('#phone').val();
     if(mob.length > 11 || mob.length < 10)
    {
      $('#mobmessage').html('Invalid phone number  only.');
      return false;
    }
    else
    {
      $('#mobmessage').html('');
      return true;
    }
  }

</script> ";
  }		
			
       
 public function view_users()
  {
   $this->load->view('admin/view_userprofiles');
  }
        
                
public function user_ajax()
{
        $this->load->model('Model_userprofile');
        $results=$this->Model_userprofile->view_userprofiles();
		
        $data1 = array();
        foreach($results  as $r) 
               {
             $edit="<a href='#myModal2' id='$r->user_id' data-toggle='modal' class='edit open-AddBookDialog2'><center><button class='btn btn-primary btn-xs' data-title='Edit' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
             
			 $change="<a href='#myModal2' id='$r->user_id' data-toggle='modal' class='change' open-AddBookDialog2'><center><button class='btn btn-info btn-xs' data-title='Change' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
             //$del=anchor('userprofile/del_entry/'.$r->user_id, 'Delete', array('id' =>'del_conf'));
            $mm= "<center><button class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></button></center>";
            $del=anchor('userprofile/del_entry/'.$r->user_id, $mm, array('id' =>'del_conf'));
             $img="<img src='$r->user_image' class='img-thumbnail' class='img-responsive' alt='Responsive image' style='width:80px; height:82px;' />";
    
		if($r->status==1)
		{
			$st="<font color='green'>Active</font>";
		}
		else
		{
			$st="<font color='red'>Inactive</font>";
		}
                      array_push($data1, array(
                            "edit"=>"$edit",
							"change"=>"$change",
                            "delete"=>"$del",
                            "name"=>"$r->user_name",
                            "image"=>$img,
							"username"=>"$r->username",
                            "mobile"=>"$r->user_mobile",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

    public function edit_user() 
	{
	$m="";
    $f="";
    
        $data=$this->session->all_userdata();
        $id=$this->input->post('uid');
        $query = $this->db->select('*')->from('users')->where('users.user_id',$id)->get();
        $row = $query->row();
     
     if($row->user_gender=="Male")
     {
       $m="checked";
       $f="";
     }
     else
     {
       $m="";
       $f="checked";
     }
         
    echo "       
     <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>         
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit User Profile</font></h4>
    </div>
    <br>

           <form  class='form-horizontal' role='form' method='post' action='". site_url('userprofile/update_user'). "' enctype='multipart/form-data' onsubmit='return checkdata();'>
                    <!-- text input -->
                    
                    <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Name : </label>
                    <div class='col-md-8'>  
          <input type='hidden' name='usrid'  value='$row->user_id'/>
          <input type='text' name='usrname' class='form-control'  id='mask_date2' value='$row->user_name' required/>
          </div>                   
          </div>
                    

                 <div class='form-group' >
                         <label class='col-md-3 control-label'>Image : </label>
                             <div class='col-md-8'>
                             <div class='fileinput fileinput-new' data-provides='fileinput'>
                             <div class='fileinput-new thumbnail' style='max-width: 145px; max-height: 152px;''>
                              <img src='$row->user_image' alt='' /> </div>
                               <div class='fileinput-preview fileinput-exists thumbnail' style='max-width: 145px; max-height: 152px;'> </div>
                             <div>
                              <span class='btn default btn-file'>
                              <span class='fileinput-new'> Select image </span>
                              <span class='fileinput-exists'> Change </span>
                              <input type='file' name='usrimage'> </span>
                              <a href='javascript:;' class='btn default fileinput-exists' data-dismiss='fileinput'> Remove </a>
                         </div>
                        </div>
                      </div>
                    <input type='hidden' name='tmpimage' value='$row->user_image'>
                   </div> 
            
          <div class='form-group'>
             <label class='col-md-3 control-label'>Gender </label>
          <div class='col-md-8'>
            <div class='icheck-inline'>
            <label style='padding-left:10px;'>
             <input type='radio' name='usrgender' ".$m."  class='icheck' data-radio='iradio_square-grey' value='Male'>Male</label>
            <label>
            <input type='radio' name='usrgender' ".$f." class='icheck' data-radio='iradio_square-grey' value='Female'>Female</label>
            </div>
          </div>
          </div>
       
	      <div class='form-group'>
          <label class='col-md-3  control-label'>Mobile : </label>
          <div class='col-md-8'>
          <input type='number' class='form-control' placeholder='Enter mobile no' name='usrmobile' value='$row->user_mobile' required id='phone'/>
           <label id='mobmessage' style='color:red;'></label>   
            </div>
          </div>
          
          <div class='form-group'>
          <label class='col-md-3  control-label'>Email : </label>
          <div class='col-md-8'>
          <input type='email' class='form-control' placeholder='Enter email' name='usremail' value='$row->user_email' required/>
          </div>
          </div>
	   
					</div>
                      <div class='form-group'>
                      <label class='col-md-3  control-label'>Status </label>
					<div class='col-md-8'>
                      <select type='text' name='status' class='form-control'>";
 
					   echo '<option value="0" ';if($row->status=='0')echo "selected"; echo'>Inactive</option>
							<option value="1"';if($row->status=='1')echo "selected"; echo ">Active</option>
							</select>
                               </div>                     
                       </div>
	   

	   
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-8'>
                <input type='submit' class='btn btn-primary' value='Update'/>
				<button type='button' class='btn btn-default' data-dismiss='modal' style='margin-left:20px;'>Close</button>
                </div>
                </div>                                
				<br>
           </form>
<!--		   <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div>  -->
	  
      
         <script type='text/javascript'>
  
  function checkdata()
  {
    
    var mob=$('#phone').val();
      
  
    if(mob.length > 11 || mob.length < 10)
    {
      $('#mobmessage').html('Invalid phone number  only.');
      return false;
    }
    
    else
    {
      $('#mobmessage').html('');
     
      return true;
    }
  }

</script> ";
  
  
    }
public function update_user()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('usrname', 'usrname', 'required');
$this->form_validation->set_rules('usremail', 'usremail', 'required');
$this->form_validation->set_rules('usrmobile', 'usrmobile', 'required');
$this->form_validation->set_rules('usrgender', 'usrgender', 'required');
 
       if($this->form_validation->run())
        {
        $upload1 = $_FILES['usrimage']['name'];
        $fname=$this->input->post('tmpimage');
         
    if(isset($_FILES['usrimage']) && $_FILES['usrimage']['size']!=0)
    {

        $config['upload_path'] = 'upload/images';                        
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $this->load->library('upload', $config);
        $this->upload->do_upload('usrimage');
        $upload_data = $this->upload->data();
        $file_name = $upload_data['file_name'];
        $fname=base_url('/upload/images')."/".$_FILES['usrimage']['name'];
    }
        
    $this->load->model('Model_userprofile');
	
    $usrname=$this->input->post('usrname');
    $usremail=$this->input->post('usremail');
    $usrmobile=$this->input->post('usrmobile');
    $usrgender=$this->input->post('usrgender');
	$status=$this->input->post('status');
    $uid=$this->input->post('usrid');
    
    $usr_newdt = array(
                'user_image'=>$fname,
                'user_name' => $usrname,
                'user_mobile' =>$usrmobile,
                'user_email' =>$usremail,
                'user_gender' => $usrgender,
				'status'=>$status
               );
  
        $this->Model_userprofile->update_user_profile($uid,$usr_newdt);
        $this->session->set_flashdata('usermsg',  '<font color="green">User details Updated..!</font>');
        redirect('Userprofile/view_users');

       }
        else 

        {
           redirect('Userprofile/edit_user');
        }                   
}
	
public function change_password() 
	{
	$m="";
    $f="";
    
    $data=$this->session->all_userdata();
    $id=$this->input->post('uid');
    $query = $this->db->select('*')->from('users')->where('users.user_id',$id)->get();
    $row = $query->row();
     
    echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Change User Password</font></h4>
    </div>
    <br>

           <form  class='form-horizontal' role='form' method='post' action='". site_url('userprofile/update_password'). "' enctype='multipart/form-data'>
                    <!-- text input -->
                    
                    <div class='form-group'>
                     <label class='col-md-4 control-label' style='padding-top:10px;'>Name : </label>
                    <div class='col-md-7'>  
          <input type='hidden' name='usrid'  value='$row->user_id'/>
          <input type='text' name='uname' class='form-control'  id='mask_date2' value='$row->user_name'/ readonly>
          </div>                   
          </div>
                    
            
	      <div class='form-group'>
          <label class='col-md-4  control-label'>Username : </label>
          <div class='col-md-7'>
          <input type='text' class='form-control' placeholder='Enter mobile no' name='usrname' value='$row->username' readonly>
            </div>
          </div>
          
          <div class='form-group'>
          <label class='col-md-4  control-label'>Enter New Password : </label>
          <div class='col-md-7'>
          <input type='password' class='form-control'  name='upass' required/>
          </div>
          </div>
	   
	      <div class='form-group'>
          <label class='col-md-4  control-label'>Re-enter Password : </label>
          <div class='col-md-7'>
          <input type='password' class='form-control' name='rupass' required/>
          </div>
          </div>
                <div class='form-group'> 
                <div class='col-md-4'>
                </div>
                <div class='col-md-7'>
                <input type='submit' class='btn btn-primary' value='Change Password'/>
				<button type='button' class='btn btn-default' data-dismiss='modal' style='margin-left:20px;'>Close</button>
                </div>
                </div>  
<br>				
           </form> 
		   <!--<div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div>-->
	  ";
    }	


public function update_password()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('upass', 'upass', 'required');
$this->form_validation->set_rules('rupass', 'rupass', 'required');

  if($this->form_validation->run())
    {
    $upass1=$this->input->post('upass');
    $upass2=$this->input->post('rupass');
    $upass3=md5($this->input->post('rupass'));
    $uid=$this->input->post('usrid');
    echo $upass1."-".$upass2;
	
	if($upass1!==$upass2)
	{
		$this->session->set_flashdata('usermsg','<font color="green">New password  not match..!</font>');
        redirect('Userprofile/view_users');
	}
	else
	{
    $usr_dt = array(
                'password'=>$upass3
                 );
		$this->load->model('Model_userprofile');
        $this->Model_userprofile->update_user_pass($uid,$usr_dt);
        $this->session->set_flashdata('usermsg',  '<font color="green">Password Changed..!</font>');
        redirect('Userprofile/view_users');
       }
                   
}
}

  public function del_entry()
  {
    
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_userprofile');
            $result=$this->Model_userprofile->delete_userprofile($id);
            if($result)
            {
                    $this->session->set_flashdata('usermsg', '<font color="green"> User details has been deleted..!</font>');
                    redirect('Userprofile/view_users');
            }
           else
           {
            $this->session->set_flashdata('usermsg', '<font color="red">Please try again</font>');
            redirect('Userprofile/view_users');
           }
  }
}