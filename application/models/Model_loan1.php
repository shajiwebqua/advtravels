<?php

defined("BASEPATH") OR exit("'No direct script access allowed'");


class Model_loan1 extends CI_Model{


	public function __construct(){
		parent::__construct();
	}

	public function insert($table,$data){
		return $this->db->insert($table,$data);
	}

	public function select_all($table)
	{
		$this->db->select("*");
		$this->db->from($table);
		$query = $this->db->get();
		return $query->result();
	}

	public function select($table,$where, $column = "*")
	{
		$this->db->select($column);
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function update($table, $values, $where){
		$this->db->where($where);
		$this->db->update($table, $values);
	}

	public function delete($table, $where){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function get_ltlreference($value){
		$this->db->where("loan_ltlrefeno", $value);
		$query = $this->db->get("loan_vehicle_master");
		if($query->num_rows() > 0){ return TRUE; }
		else{ return FALSE; }
	}

	
	
}