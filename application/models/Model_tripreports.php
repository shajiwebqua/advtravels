<?php
class Model_tripreports extends CI_Model{
	
    public function form_insert($userdt)
    {
       $this->db->insert('trip_management', $userdt);  
    }
	
	public function view_tripDT($op,$sec,$d11,$d22)
	{
    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','left');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','left');
	//$this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=trip_management.trip_driver_id','left');
	if($sec==1)
	{
		$this->db->where("trip_management.trip_status=1");
	}
	else if($sec==2)
	{
		$w="(trip_management.trip_status=2 or trip_management.trip_status=3)";
		$this->db->where($w);
	}
	
	if($op!=4)
	{
		$this->db->where("trip_management.trip_clienttype",$op);	
	}
	
	
	$w1="(trip_management.trip_enddate>='".$d11."' and trip_management.trip_enddate<='".$d22."')";
	$this->db->where($w1);
	
	$this->db->order_by("trip_management.trip_id","desc");
    $query = $this->db->get();
    return $query->result();
	}
		
	
	public function view_tripVehicleDT($op,$sec,$d11,$d22)
	{
    $this->db->select('*');
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','left');
	
	if($sec!=="3")
	{
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','left');
	}
	//$this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=trip_management.trip_driver_id','left');
    if($sec==1)
	{
		$this->db->where("trip_management.trip_status=1");
	}
	else if($sec==2)
	{
		$w="(trip_management.trip_status=2 or trip_management.trip_status=3)";
		$this->db->where($w);
	}
	if($op>0)
	{
		$this->db->where("trip_management.trip_vehicle_id",$op);
	}
    $w1="(trip_management.trip_startdate>='".$d11."' and trip_management.trip_startdate<='".$d22."')";
	$this->db->where($w1);
	
	$this->db->order_by("trip_management.trip_id","desc");
    $query = $this->db->get();
    return $query->result();
	}	
		
		
	
	public function view_tripVehicleDT1($op,$d11,$d22)
	{
		$op=str_replace("%20"," ",$op);
				
		$this->db->select('*');
		$this->db->from('trip_management');
		$this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
		
		$this->db->where('trip_management.trip_driver_id','0');
		$w="(trip_management.trip_status=2 or trip_management.trip_status=3)";
		$this->db->where($w);
			
		$w1="(trip_management.trip_startdate>='".$d11."' and trip_management.trip_startdate<='".$d22."')";
		$this->db->where($w1);
		
		if($op>0)
		{
			$w1="trip_management.trip_vehicle_regno='".trim($op)."' ";
		}
		
		$this->db->where($w1);
		
		$this->db->order_by("trip_management.trip_id","desc");
		$query = $this->db->get();
		return $query->result();
	}	
		
		
	public function view_Corp_TripDT($op,$d11,$d22)
	{
		
		$this->db->select('*');
		$this->db->from('trip_management');
		$this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','left');
		$this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','left');
		//$this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=trip_management.trip_driver_id','left');
		
		//if($sec==1)
		//{
			//$this->db->where("trip_management.trip_status=1");
	//	}
		//else if($sec==2)
		//{
	//		$w="(trip_management.trip_status=2 or trip_management.trip_status=3)";
	//		$this->db->where($w);
	//	}
		$this->db->where("trip_management.trip_clienttype","2");
		if($op!="0")
		{
		$this->db->where("trip_management.trip_clientcustomer",$op);
		}
		$w1="(trip_management.trip_startdate>='".$d11."' and trip_management.trip_startdate<='".$d22."')";
		$this->db->where($w1);
		$this->db->order_by("trip_management.trip_id","desc");
		$query = $this->db->get();
		return $query->result();
	}
	
}
?>