<?php
 include('application/views/common/header.php');
 ?>
<!-- <link rel="stylesheet" href="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.css');?>" rel="stylesheet" type="text/css" />
 <script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
 <script src="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.js');?>"></script> -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/bootstrap/bootstrap.min.css');?>">-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/font-awesome/css/font-awesome.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/search_tabs/selectize.css');?>">
  
 <style>
 .nav-tabs {
    margin: 0;
    padding: 0;
    border: 0;    
}
.nav-tabs > li > a {
    background: #f2f2f2;
    border-radius: 0;
    //box-shadow: inset 0 -8px 7px -9px rgba(0,0,0,.4),-2px -2px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li.active > a:hover {
    background: #fff;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li > a:hover {
    background: #e4e4e4;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}
/* Tab Content */
.tab-pane {
    background: #fff;
    //box-shadow: 0 0 4px rgba(0,0,0,.4);
    border-radius: 0;
    border:1px solid #e4e4e4;
    text-align: left;
    padding: 10px;
}

.cli
{
padding:5px 0px 5px 0px;
margin-top:3px;

}

.selclass > div:hover
{
background-color:#f4f4f4;
}

</style>
  
 
 
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

input[type=number] {
    -moz-appearance:textfield;
}

.ttext
{
	background-color:#fff;
	font-weight:bold;
	color:Green;
}

.search-btn
{
	padding-top:3px;
	z-index:1000;
	right:17px;
	position:absolute;
}

.select_btn1 ,.select_btn2{
  
    position: absolute;
    top: 3px;
    z-index: 999;
   
}
.select_btn1{
	 right: 19px;
}
.select_btn2 {
    right: 19px;
}

.lblmap
{
	background-color:#e6e6e6;
	padding-top:12px;
	padding-bottom:12px;
	height:45px;
}

/* radio button ----------------*/

#m-radio {
	margin: 0 0px;
}

#m-radio label {
  width: 200px;
  border-radius: 3px;
  border: 1px solid #D1D3D4
  	color:#888 ;
}

/* hide input */
#m-radio input.radio:empty {
	margin-left: -999px;
}

/* style label */
#m-radio input.radio:empty ~ label {
	position: relative;
	float: left;
	line-height: 2.5em;
	text-indent: 3.25em;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

#m-radio input.radio:empty ~ label:before {
	position: absolute;
	display: block;
	top: 0;
	bottom: 0;
	left: 0;
	content: '';
	width: 2.5em;
	background: #D1D3D4;
	border-radius: 3px 0 0 3px;

}

/* toggle hover */
#m-radio input.radio:hover:not(:checked) ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #C2C2C2;

}

#m-radio input.radio:hover:not(:checked) ~ label {
	color:#777 ;
}

/* toggle on */
#m-radio input.radio:checked ~ label:before {
	content:'\2714';
	text-indent: .9em;
	color: #9CE2AE;
	background-color: #4DCB6D;
}

#m-radio input.radio:checked ~ label {
	/*color: #888;*/
}

/* radio focus */
.form-horizontal .checkbox, .form-horizontal .radio {
    min-height: 0px;
}
.no-pad {
	padding-left: 0px;
	padding-right: 0px;
}


/* fare list -----------*/
table#fare tbody tr:hover {
    background-color: #afc0db;
    cursor: pointer;
	color:#fff;
}

/* fixedfare list -----------*/
table#fixedfare tbody tr:hover {
    background-color: #afc0db;
    cursor: pointer;
	color:#fff;
}


/* clubfare list -----------*/
table#clubfare tbody tr:hover {
    background-color: #afc0db;
    cursor: pointer;
	color:#fff;
}



</style>
<!-- Content Wrapper. Contains page content -->


<div class="content-wrapper" id='bdy'> 
  
  <!-- Main content -->
    
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 ><b>New Trip Sheet</b></h1>
    <ol class="breadcrumb">
	<li> <a href="#" data-toggle='modal' data-target='#myModalduty' style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'><i class="fa fa-user" aria-hidden="true"></i> Duty List</button></a></li>
	<li> <a href="#" data-toggle='modal' data-target='#myModalfare' style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'> <i class="fa fa-cab" aria-hidden="true"></i> Fare List</button></a></li>
   <li> <a href="<?php echo base_url('Trip/view_trip');?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'> <i class="fa fa-backward" aria-hidden="true"></i> Trip List</button></a></li>
    </ol> 
    </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
  <!-- Content Header end -->
 <section class="content"> 
			<div style="padding:2px 0px 2px 0px;" >
				 <div id='idmsg'>
				 <center><div id="msg"><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
  
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!--Add new user details -->
<div style="background-color:#fff;padding:5px; ">
   	
<!-- <div class="portlet-title">
      <div class="caption">
        <div style="color:blue;" >
            Trip Details                   
        </div> 
        </div>
 </div> -->
		  
		<div style='padding-left:3px;'> 
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item active" id='tb1' >
						<a class="nav-link tp1" data-toggle="tab" href="#bdetails"  id="tab1" role="tab"><i class="fa fa-user"></i> Basic Details</a>
					</li>
					<li class="nav-item " id='tb2'>
						<a class="nav-link tp1"  id="tab2"  role="tab"> <i class="fa fa-cab"></i> Trip Vehicle & Location</a>
					</li>
					<li class="nav-item " id='tb3'>
						<a class="nav-link tp1 " id="tab3" role="tab"> <i class="fa fa-compass"></i> Kilometer & Others</a>
					</li>
                 </ul>
        <div class="tab-content">
		<div class="tab-pane active" id="bdetails" role="tabpanel">  <!-- class list -->
        <div style='padding:10px 5px;width:100%'> 	  
				  
		  
	<div class="portlet-body form">
    <form  onsubmit='return check_tab3_data();' id="#myForm" class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Trip/add_new_trip')?>" enctype="multipart/form-data" >

        <div class="row">
        <div class="col-md-7" style='border-right:1px solid #e4e4e4;'> 

		<!-- <div class='form-group' style="background-color:#e6e6e6;padding:8px 0px 8px 0px;">
          <label class='col-md-3 control-label' style='padding-top:2px;'><b>Get from Enquiry :</b></label>
           <div class='col-md-8'>
				<select id="enquiry" class="form-control" placeholder="Client type" name="enquiry">
				<option value="">----------</option>

					   <?php 
					   $qry1=$this->db->select('*')->from('enquiry')->get()->result();
					   
						foreach($qry1 as $r1)
						{	   	   
					   $op1=$r1->enquiry_id."-".$r1->party_name."-".$r1->nature_of_journey;
					   ?>
						  <option value="<?=$r1->enquiry_id;?>"><?=$op1;?></option>
						  
						<?php } ?>
						</select>
 				  </div>
         </div> 
		
	 <label style="background-color:#cecece;width:100%;height:1px;"></label>  -->
		
		<div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Select Booking : </label>
          <div class='col-md-9'>
		        <select class="form-control"  name="bookingid" id='bookingid'>
				  <option value="">----------</option>
				  <option value="0" selected>0</option>
						<?php
						$bk=$this->db->select('*')->from("trip_booking")->where("book_status","1")->order_by('book_id','desc')->get()->result();
						
						foreach($bk as $b)
						{
						 echo"<option value='". $b->book_id."'>".$b->book_id."-".$b->book_guestname."</option>";
						}
						?>
	
				</select>
		  
          </div>
        </div> 
		  
		<div class='form-group' style='padding-bottom:10px;'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Reference: No :</label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='refno' id='refno'  value='0' required>
          </div>
		  <label class='col-md-2 control-label' style='padding-top:5px;'>Room No :</label>
          <div class='col-md-3'>
          <input type='text' class='form-control' name='roomno' id='roomno' value='0' required>
          </div>
	  
        </div>

		
		<div class='form-group' >
           <!-- <div class='row'> -->
        <label class='col-md-3 control-label' style='padding-top:2px;'>Client Category :</label>
            <div class='col-md-9'>
				<select id="category" class="form-control" placeholder="Client type" name="clienttype" required>
				  <option value="">----------</option>
				  <option value="1">INDIVIDUAL</option>
				  <option value="2">CORPORATE</option>
				  <option value="3">AGENT</option>
				</select>
			</div>
        </div>
								
		  <!--<div class='form-group clientname'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Corporate Name : </label>
			  <div class='col-md-8'>
			  <input type='text' class='form-control' name='clientna' id='clientna'  value="" >
			  </div>
		  </div> -->

		<div class='form-group' id='client_na'>
          <label class='col-md-3 control-label' style='padding-top:2px;'>Corporate Name :</label>
           <div class='col-md-9'>
			  <section class="form-control " style='padding:0px;'>
				<select id="id3" class="form-controll_prop" placeholder="Choose Your Name" name='clientna'>
				  <option value=""></option>
				  <?php
					$cquery=$this->db->select("customer_name,customer_id")->from('customers')->where("customer_type","2")->get()->result();
					foreach( $cquery as $r)
					{
						echo "<option value='".$r->customer_id."'>".$r->customer_name."</option>";
					}
				  ?>
									  
				</select>
				
				</section>
				<div class="select_btn1">                              
				<a href="<?php echo base_url('Customer/add_tripcustomer/2');?>" data-target="#myModal2" data-toggle='modal'><button id='custadd' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span></button></a>
				</div>	
				</div>
         </div>
		  
		  
          <div class='form-group cpurpose'>
			  <label class='col-md-3  control-label' style='padding-top:5px;'>Purpose for Trip: </label>
			  <div class='col-md-9'>
			  <textarea rows=2 class='form-control' name='purposetrip' id="purposetrip" ></textarea>
			  </div>
		  </div>
						
		<div class='form-group' id='agents'> 
		   <label class="col-md-3 control-label" id='agentlbl'>Agent Name</label>
			<div class="col-md-4">
			<input type="text" class="form-control" name="agent" id="agent">
			<input type="hidden" class="form-control" name="agentid" id="agentid">
		</div>
		
		<a href="#" id='agentselect' data-target="#myModalSA" data-toggle='modal'><input type='button' value='Select' class="btn btn-default btn-xs" name='agentselect'  style='margin-top:5px;'> </a>
			&nbsp;&nbsp;<a href="#" data-target="#myModalA" data-toggle='modal'><input type='button' value='Add' class="btn btn-default btn-xs" name='agentadd' id='agentadd' style='margin-top:5px;'> </a>
		</div>
						
					
      <div class='form-group' id='custna'>
          <label class='col-md-3 control-label' style='padding-top:2px;'>Customer Name :</label>
           <div class='col-md-9'>
			  <section class="form-control " style='padding:0px;'>
				<select id="id2" class="form-controll_prop" placeholder="Choose Your Name" name="custid" >
				  <option value=""></option>
				  <?php
					$cquery=$this->db->select("customer_name,customer_id")->from('customers')->get()->result();
					foreach( $cquery as $r)
					{
						echo "<option value='".$r->customer_id."'>".$r->customer_name."</option>";
					}
				  ?>
				</select>
				
				</section>
				<div class="select_btn1">                              
				<a href="<?php echo base_url('Customer/add_tripcustomer/2');?>" data-target="#myModal2" data-toggle='modal'><button id='custadd' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span></button></a>
				</div>	
				</div>
         </div>
		 
		 <div class='form-group'  id='custna1'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Customer Name : </label>
			  <div class='col-md-9'>
			  <input type='text' class='form-control' name='cust_name' id='cust_name' >
			  </div>
         </div>
		 
		  <div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Customer Mobile : </label>
			  <div class='col-md-9'>
			  <input type='text' class='form-control' name='cmobile' id='idcmobile' required >
			  </div>
          </div>
		 
          <div class='form-group'>
			  <label class='col-md-3  control-label' style='padding-top:5px;'>Nature of Jurney : </label>
			  <div class='col-md-9'>
			 <!-- <input type='text' class='form-control' name='nojourney' id='nojourney' required> -->
			 
			 
			 <select class="form-control" placeholder="Choose Purpose" name="nojourney" id='nojourney'>
				  <option value="">----------</option>
						<?php
						$vt=$this->db->select('*')->from("category") ->get()->result();
						
						foreach($vt as $v)
						{
						 echo"<option value='". $v->category_name ."'>".$v->category_name ."</option>";
						}
						?>
	
				</select>
			  </div>
          </div>
		  
		<div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Trip Start Date : </label>
			  <div class='col-md-4'>
			  <input type='date' class='form-control' name='startdate' id='startdate' value="<?php echo date('Y-m-d');?>" required>
			  </div>
			  <label class='col-md-1 control-label'  > End:</label>
          <div class='col-md-4'>
          <input type='date' class='form-control' name='enddate' id='enddate' required>
          </div>
       </div>
	   
	    <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Trip Start Time : </label>
          <div class='col-md-4'>
          <input type='time' class='form-control' name='starttime' id="starttime" required>
          </div>
		  <label class='col-md-1 control-label'> End: </label>
			<div class='col-md-4'>
          <input type='time' class='form-control' name='endtime' id="endtime" required>
          </div>
        </div>
	
	<!--	<div class='row'>
			<label style="background-color:#cecece;width:100%;height:1px;"></label>
		</div> -->
		</div>
		 		  
 <!-- second column --------------------------->
		  
		  <div class='col-md-5' style='padding-left:20px;' >
		  
		 <div class='row fare' style='padding:0px 0px 5px 10px;margin-right:0px;'>
		  <div class='col-md-12'>
		  
		  <div class="row" style='background-color:#e4e4e4;padding:3px;'>
		  <label class='col-md-12' style='font-size:15px;color:#000;'><b>Select Trip</b></label>
		  </div>
		  	 
		  
		  <div class='row' style='margin-left:5px; margin-top:15px;'>
		  <div id="m-radio" class="col-md-6 no-pad">
		  <input type="radio" name="sltrip" id="radio1" class="radio" value='ST' checked/>
		  <label for="radio1" style='float:left;'><b>Short Trip (<100km)</b></label>
		  </div>

		  <div id="m-radio" class="col-md-6 no-pad">
		  <input type="radio" name="sltrip" id="radio2" class="radio" value='LT'/>
		  <label for="radio2" ><b>Out Station(>100km)</b></label>
		  </div>
          </div>
		  
		  <div class='row' style='margin-left:5px;'>
		  <div id="m-radio" class="col-md-6 no-pad">
		  <input type="radio" name="sltrip" id="radio3" class="radio" value='AP'/>
			<label for="radio3" style='float:left;'><b>Airport Pick Up</b></label>
			</div>

			<div id="m-radio" class="col-md-6 no-pad">
			<input type="radio" name="sltrip" id="radio4" class="radio" value='AD'/>
			<label for="radio4" ><b>Airport Drop</b></label>
			</div>
          </div>
		  
		  <div class='row' style='margin-left:5px;'>
		  <div id="m-radio" class="col-md-6 no-pad">
		  <input type="radio" name="sltrip" id="radio5" class="radio" value='RP' />
			<label for="radio5" style='float:left;'><b>Railway Pick Up</b></label>
			</div>

			<div id="m-radio" class="col-md-6 no-pad">
			<input type="radio" name="sltrip" id="radio6" class="radio" value='RD'/>
			<label for="radio6" ><b>Railway Drop</b></label>
			</div>
          </div>
		  <div class='row' style='margin-left:5px;'>
		  <div id="m-radio" class="col-md-6 no-pad">
		  <input type="radio" name="sltrip" id="radio7" class="radio" value='FP' />
			<label for="radio7" style='float:left;'><b>Fixed Package</b></label>
			</div>

			<div id="m-radio" class="col-md-6 no-pad">
			<input type="radio" name="sltrip" id="radio8" class="radio" value='CM'/>
			<label for="radio8" ><b>Complimentary</b></label>
			</div> 
          </div>
		  </div>

		</div>
		 		  
        </div>
	  </div>


</div>

 <label style="background-color:#cecece;width:100%;height:1px;"></label>

<div class='row' style='margin-bottom:5px;'>
<div class='form-group'>
<div class='col-md-11' style='text-align:right;' >
	<button type="button" class="btn btn-primary" style="padding:5px 20px 5px 20px;" id="btnnext1" > <b>Next <span style='padding-left:10px;color:#000;'><i class="fa fa-chevron-right" aria-hidden="true"></i></span></b></button>
</div>
</div>
</div>
 
  
 </div>

</div> <!-- tab container------------------------------------------------------------------------------------------------->

<div class="tab-pane" id="tpv" role="tabpanel" >  <!-- class list -->
          <!-- datatable list here ------------>

	<div class='row'>
	<div class='col-md-7'  style='border-right:1px solid #e4e4e4;'>

		<div class='row' style='padding-top:20px;'>
          <label class='col-md-3 control-label' style='padding-top:5px;text-align:right;'>Trip From :</label>
          <div class='col-md-9'>
          <input type='text' class='form-control' name='tripfrom' id='tripfrom' required>
          </div>
        </div>
      
        <div class='row' style='padding-top:5px;'>
          <label class='col-md-3 control-label'style='padding-top:5px;text-align:right;'>Trip To : </label>
          <div class='col-md-9' style='margin-top:5px;'>
          <input type='text' class='form-control' name='tripto' id="tripto" required>
          </div>
        </div>
		
		<div class='row' id="hplace1" >
          <label class='col-md-3 control-label ' style='padding-top:5px;font-weight:600;'> </label>
		  <label class='col-md-6' style='margin-top:0px;padding-left:15px;font-size:12px; color:blue'> Enter visiting places bellow separate with COMMA (<font style='color:red'>Eg : Kozhikode,Palakad,Ooty...etc</font>)</label>
        </div>
		<div class='row' id="hplace2">
		  <label class='col-md-3 control-label ' style='padding-top:5px;font-weight:600;text-align:right;'>Visiting Places : </label>
          <div class='col-md-6'>
          <textarea rows='2' class='form-control' name='vplace' id="vplace" >Nil</textarea>
          </div>
		  <div class='col-md-3'>
          <button type='button' class='btn btn-warning' name='btndistance' id="btndistance" style='margin-top:15px;'>Distance</button>
          </div>
		</div>
		
		<div class='row' id="hplace3" style='padding-top:5px;'>
          <label class='col-md-3 control-label ' style='padding-top:5px;text-align:right;'>Total KM  : </label>
          <div class='col-md-2'>
          <input type='text' class='form-control' name='total_km' id="total_km" value="0">
          </div>
		  <label class='col-md-1 control-label ' style='padding-top:5px;'> </label>
		  <label class='col-md-4 control-label' style='padding-top:5px;text-align:right;'>Running KM (Up&Down) : </label>
		  <div class='col-md-2'>
		  <input type='text' class='form-control' name='totalkm' id="totalkm" value="0">
		  </div>
		 <!-- <input type='hidden' name='totalkm' id="totalkm" >  -->
		</div>
		
		<div class='row' style='padding-top:10px;padding-bottom:5px;'>
        <label class='col-md-3 control-label' style='padding-top:5px;'></label>
          <div class='col-md-9' style='text-align:right;'>
           <a href="" data-target="#myModal_Atta" data-toggle='modal' ><button id='attavehiclesearch' class='btn btn-default btn-xs'>Attached Vehicle</button></a>
		  <a id='addattached' data-target="#myModaladd" data-toggle='modal' ><button class='btn btn-primary btn-xs' name='addattached'>Add</button></a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
		  
		  <a id='vehiclesearch' data-target="#myModals" data-toggle='modal' ><button  class='btn btn-default btn-xs'>Our Vehicle</button></a>
					<!--	<button class='btn btn-primary btn-xs' name='addvehicle' id="addvehicle">Add</button></a> -->
		  </div>
		</div>
			
			
		  <div class='row'> 
          <label class='col-md-3 control-label ' style='text-align:right;'>Vehicle : </label>
          <div class='col-md-9' id="regs1">
		   <div class='search-btn'> 
			<!--<a href="" data-target="#myModals" data-toggle='modal' ><button id='vehiclesearch' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button></a>-->
		  </div>
		  <input type='text' class='form-control' name='vehicle' id='vregno' required >
		  <input type='hidden' class='form-control' name='vehicleid' id='vehicleid' >
          </div>
		  </div>
 
           <div class='row' style='padding-top:5px;'> 
			<label class='col-md-3 control-label' style='text-align:right;' >Driver Name : </label>
			<div class='col-md-9'>
			 <div class='search-btn'> 
			   <a id='driversearch' data-target="" data-toggle='modal' ><button  class='btn btn-primary btn-xs'>Select</button></a>
			   </div>
			<input type='text' class='form-control' name='driver' id='driver' required >
			<input type='hidden' class='form-control' name='driverid' id='driverid' > 
			</div>
        </div>
	
		
		<div class='row' style='padding-top:5px;'>
          <label class='col-md-3 control-label ' style='padding-top:5px;text-align:right;'>Addi. Information : </label>
          <div class='col-md-9'>
          <textarea rows='3' class='form-control' name='addiinfo' id='addiinfo'>Nil</textarea>
          </div>
        </div>
</div>
<div class='col-md-5'></div>
</div>

<label style="background-color:#cecece;width:100%;height:1px;"></label>

<div class='row' style='margin-bottom:5px;'>
<div class='form-group'>
<div class='col-md-11' style='text-align:right;' >
	<button type="button" class="btn btn-primary" style="padding:5px 20px 5px 20px;" id="btnnext2" > <b>Next <span style='padding-left:10px;color:#000;'><i class="fa fa-chevron-right" aria-hidden="true"></i></span></b></button>
</div>
</div>
</div>
</div>


<!-- tab 3 start ------------------------------>
<div class="tab-pane" id="kmothers" role="tabpanel">  
          <!-- datatable list here ------------>

	<div class='row'>
	<div class='col-md-7'  style='border-right:1px solid #e4e4e4;'>

	<div class='row'>
		<div class='col-md-12'>
			<div class='row' style='padding-top:3px;' id='pay_mode'>
				<label class='col-md-4 control-label' style='padding-top:5px;color:blue;'>Payment Mode : </label>
				<div class='col-md-8'>
				<select name="paymode" id='paymode' class='form-control'>
				 <option value=''>----------</option>
				 <option value='CREDIT'>CREDIT</option>
				 <option value='CASH'>CASH</option>
			   </select>	
					<input type='hidden' class='form-control' name='paymode1' id='paymode1' value='NONE'>
				</div>
			</div>
		
			<div class='row' style='padding-top:15px;'>
				<label class='col-md-4 control-label' style='padding-top:5px;'>Minimum Kilometers : </label>
				<div class='col-md-3'>
				 <input type='number' class='form-control' name='minckm' id='idminckm' style='text-align:right;' >
				</div>
				
				<label class='col-md-2 control-label' style='padding-top:5px;'>Charge : </label>
				<div class='col-md-3'>
				 <input type='number' class='form-control' name='mincharge' id='mincharge' style='text-align:right;'>
				</div>

			</div>
			<!--<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Minimum Kilometer Charge : </label>
				<div class='col-md-5'>
				 <input type='number' class='form-control' name='mincharge' id='mincharge' >
				</div>
			</div> -->
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-4 control-label' style='padding-top:5px;'>Trip Starting Kilometer : </label>
				<div class='col-md-3'>
				<input type='number' class='form-control' name='startkm' id='startkm' style='text-align:right;'>
				</div>
				<div class='col-md-5' style='padding-top:3px;padding-left:5px;text-align:right;'>
				<!--<a href="#"  data-toggle="modal" class="btn btn-default btn-xs" name="btncheck"  id="btncheck">Check</a> -->
				<input type='button' class="btn btn-default " name="btncheck"  id="btncheck" value="Click to check the Extra KM" >
				</div>
			</div>
			
			<div class='row' style='margin-top:5px; padding-top:10px;'>
			<div class='col-md-3'></div>
			<div class='col-md-9' id='idextrakm' style="background-color:#e4e4e4;height:50px;border-radius:10px;">
				<div class='col-md-8 control-label' style='padding-top:5px; border-right:1px solid #fff;'>
				  <span id='extrakm'></span>
				</div>
				<div class='col-md-4 control-label' style='padding-top:15px;'>
				<input type='button' id='btnreason' data-toggle='modal' class='btn btn-info btn-xs' value='Set Reason'>
				</div>
			</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'> </label>
				<div class='col-md-5' style='text-align:right;'> 
				 <input type='button' class='btn btn-primary' name='btnSet' id='btnSet' value='Get Values'>
				</div>
			</div>
			 
			<div class='row' style='padding-top:5px;margin-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Trip Ending Kilometer (Vehicle) : </label>
				<div class='col-md-5'>
				<input type='number' class='form-control' name='endkm' id='endkm'  style='text-align:right;'>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Minimum Days : </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='mindays' id='mindays' style='text-align:right;' required>
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Minimum Days Total : </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' style='font-weight:bold;text-align:right;' name='mintotal' id='mintotal' readonly>
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Additional Kilometers : </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='addikm' id='addikm' style='text-align:right;' required>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Additional Kilometers Charge: </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='addikmcharge' id='addikmcharge' style='text-align:right;' required>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Additional KM Total Charge : </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' style='font-weight:bold;text-align:right;'' name='additotal' id='additotal' readonly>
				</div>
			</div>
		
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Toll and Parking Charges: </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='tollparking' id='tollparking' style='text-align:right;'  required>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Interstate Permit Charge: </label>
				<div class='col-md-5'>
				 <input type='text' class='form-control' name='ispermit' id='ispermit' style='text-align:right;' required>
				</div>
			</div>
			
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'> </label>
				<div class='col-md-5' style='text-align:right;'>
				 <input type='button' class='btn btn-primary' name='btncalc' id='idbtncalc' value='Calculate'>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-7 control-label' style='padding-top:5px;'>Trip Estimated Cost: </label>
				<div class='col-md-5'>
				 <input type='number' class='form-control ttext' style="background-color:#fff;text-align:right;'" name='tripcost' id='tripcost' value="0" readonly>
				</div>
			</div>
			
			
		<div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:5px;'></label>
          <label class="col-md-4 control-label" style="font-size:25px; text-align:right;color:blue;">RS : </label> 
		  <label id="lblgtotal" class="col-md-5 control-label" style="font-size:25px; text-align:right;color:blue;">0.00</label>
        </div>
		</div>
		</div>
	
	
<label style="background-color:#cecece;width:100%;height:1px;"></label>

<div class='form-group'>
<div class='col-md-11' style='text-align:right;'>
	<button type="submit" class="btn btn-success" style="padding:10px 50px 10px 50px;" id="idsavebtn" > <b>Save Trip Details</b></button>
</div>
</div>
		
</div>

<!-- second column ------- FARE LIST------------------------------------------------>

	<div class='col-md-5'>
	
	<div style='padding-left:3px;'> 
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item active" id='tb1' >
						<a class="nav-link tp1" data-toggle="tab" href="#fnormal"  role="tab"><i class="fa fa-compass"></i> Normal Fare</a>
					</li>
					<li class="nav-item " id='tb2'>
						<a class="nav-link tp1 " data-toggle="tab" href="#ffixed"  role="tab"> <i class="fa fa-compass"></i> Fixed Fare</a>
					</li>
					<li class="nav-item " id='tb3'>
						<a class="nav-link tp1 " data-toggle="tab" href="#fclub"  role="tab"> <i class="fa fa-compass"></i> Club Fare</a>
					</li>
                 </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="fnormal" role="tabpanel"> 
		<label style='padding:3px 5px 3px 5px;font-size:15px;' id='corpname'>Fare List of : </label>
		<label style='padding:2px 0px 2px 5px;font-size:12px;color:blue;'> Click on the below table to get fare details</label>
	     <div style='margin-top:5px;'>
		 	<table id='fare' width='100%' style='border:1px solid #e4e4e4;font-size:13px;' >
			<thead>
			<th style='padding:3px;'>Sl.No</th>
			<th style='padding:5px;'>Vehicle</th>
			<th style='padding:5px;'>Hrs</th>
			<th style='padding:5px;'>KM</th>
			<th style='text-align:right;padding:5px;'>Rate</th>
			</thead>
			<tbody id='farelist'>
			
			</tbody>
			</table> 
		</div>
		</div>
		<div class="tab-pane " id="ffixed" role="tabpanel"> 
			<label style='padding:3px 5px 3px 5px;font-size:15px;' id='corpname'>Fare List of : </label>
				<label style='padding:2px 0px 2px 5px;font-size:12px;color:blue;'> Click on the below table to get fare details</label>
				 <div style='margin-top:5px;'>
					<table id='fixedfare' width='100%' style='border:1px solid #e4e4e4;font-size:13px;' >
					<thead>
					<th style='padding:3px;'>Sl.No</th>
					<th style='padding:5px;'>Vehicle</th>
					<th style='padding:5px;'>Trip</th>
					<th style='text-align:right;padding:5px;'>Rate</th>
					</thead>
					<tbody id='fixedlist'>
					
					</tbody>
					</table> 
			</div>

		</div>
		<div class="tab-pane " id="fclub" role="tabpanel"> 
			<label style='padding:3px 5px 3px 5px;font-size:15px;' id='corpname'>Fare List of : </label>
				<label style='padding:2px 0px 2px 5px;font-size:12px;color:blue;'> Click on the below table to get fare details</label>
				 <div style='margin-top:5px;'>
					<table id='clubfare' width='100%' style='border:1px solid #e4e4e4;font-size:13px;' >
					<thead>
					<th style='padding:3px;'>Sl.No</th>
					<th style='padding:5px;'>Vehicle</th>
					<th style='padding:5px;'>Trip</th>
					<th style='text-align:right;padding:5px;'>Rate</th>
					</thead>
					<tbody id='clublist'>
					
					</tbody>
					</table> 
			</div>
		
		
		</div>
	</div>
		

	
	</div>
	</div>
	
	<!-- ------- END FARE LIST------------------------------------------------>
	
</div>

<!-- tab 3 end ------------------------------>
</form>
</div>
</div>
</div>
</div>
</div>



<!-- end ---------------------------------------------------------------------------------------------------------------------->
</section>

<!-- </div> -->

<div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content" style='height:600px;'>
				<div class="modal-body" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit</h4>
				</div>
			</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
   </div>  
   
   <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit</h4>
				</div>
			</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
    </div> 
	
	
	
	<div class="modal fade draggable-modal" id="myModaladd" tabindex="-1" role="basic" aria- hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" style='height:500px;'>
				<div class="modal-header">
				<button type='button' class='close' data-dismiss='modal'>&times;</button>
				<h4 class='modal-title' > <font color='blue'>Attached Vehicle Details</font></h4>
					
				</div>
				
				<div class='col-md-12'>
  			<!--<form  class='form-horizontal' role='form' method='post' action=".base_url('Attachvehicle/modal_addvehicle')." enctype='multipart/form-data' onsubmit='return checkdata();' >  -->
                    <!-- text input -->
			<div class='row'>
			<div class='col-md-11'>
	   
            <div class='form-group'>
            <div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label'>Vehicle Type : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<select name='mvtype' class='form-control' id='mvtype' required>
				<option value=''>---------</option>
				
				<?php
				$qry=$this->db->select('*')->from('vehicle_types')->get()->result();
				foreach($qry as $r1)
				{
					echo "<option value='".$r1->veh_type_id."'>".$r1->veh_type_name."</option>";
					
				}
				?>
				</select>	</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Register No : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='mregno' class='form-control'  id='mregno' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >No os seats : </label>
				<div class='col-md-2' style='padding-left:19px;'>
				<input type='text' name='mnseats' class='form-control' id='mnseats'  required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' >R/C Owner Name : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='mrcname' class='form-control'  id='mrcname' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Mobile : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='number' name='mrcmobile' class='form-control' id='mrcmobile' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' >Driver Name : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='mdrname' class='form-control' id='mdrname' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Mobile No : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='number' name='mdrmobile' class='form-control'  id='mdrmobile' required />
				</div>           
			</div>
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' style='color:blue'>Kilometers : </label>	
			</div>	
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Starting : </label>
				<div class='col-md-3' style='padding-left:19px;'>
				<input type='text' name='mstartkm' class='form-control'  id='mstartkm' required />
				</div>           
				<label class='col-md-2 control-label' >Ending : </label>
				<div class='col-md-3' style='padding-left:19px;'>
				<input type='text' name='mendkm' class='form-control'  id='mendkm' required />
				</div>           
			</div>
        </div>
      
 </div>
 </div>
     <label style='height:1px;width:100%;background-color:#cecece;'></label>
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-8'>
                <button class='btn btn-primary' id='idaddvehicle' >Add Vehicle</button>
                </div>
                </div>                                
          <!-- </form> -->
           </div>
				
			</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
    </div>  
   
   

<div class="modal fade" id="myModal_Atta" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Attached Vehicles List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class=" max-width">
                <table id="TblAttaVehicle" class="table table-striped table-hover table-bordered" cellspacing="0">
				<thead>
            <tr>
                <th>Select</th>
                <th>Registration No</th>
                <th>Vehicle Type</th>
				<th>Driver Name</th>
				<th>Driver Mobile</th>
                <th>No Of Seats</th>
            </tr>
        </thead>
    </table>
    </div>
   </div>

    </div>
</div>
</div>
</div>


<div class="modal fade" id="myModals" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Vehicles List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class=" max-width">
                <table id="TblVehicle" class="table table-striped table-hover table-bordered" cellspacing="0">
				<thead>
            <tr>
                <th>Select</th>
                <th>Registration No</th>
                <th>Vehicle Type</th>
                <th>No Of Seats</th>
				<th>Status</th>
            </tr>
        </thead>
    </table>
    </div>
   </div>


    </div>
</div>
</div>
</div>

<div class="modal fade" id="myModald" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Drivers List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class="col-md-12">
            <table id="TblDriver" class="table table-striped table-hover table-bordered" cellspacing="0">
            <thead>
            <tr>
                <th>Select</th>
				<th>ID</th>
                <th>Driver Name</th>
                <th>Mobile</th>
				<th>Status</th>
            </tr>
        </thead>
      
		<!--	  <tbody>
        <?php  foreach($results2  as $r) 
               {   ?>

              <?php $edit="<a href='#myModald' id='$r->driver_id'  class='select open-AddBookDialog2'>Select</a>";?>
            <tr>
                <td><?=$edit;?></td>
                <td><?=$r->driver_name;?></td>
                <td><?=$r->driver_mobile;?></td>
                
                <?php if($r->driver_status==1)
                {
                 $st="<font color='green'>Available</font>";
                }
                else
                {
                 $st="<font color='red'>Not Available</font>";
                } ?>
                <td><?=$st;?></td>
                
            </tr>
            <?php } ?>
           
        </tbody> -->
		
    </table>
        </div>
      </div>
 	 </div>
 	</div>
   </div>
    
  </div>
  
  <div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria- hidden="true">
	    <div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Check Kilometer</h4>
				</div>
			</div>
			</div>
		<!-- /.modal-dialog -->
	   </div>
   </div>  
</div>

<div class="modal fade draggable-modal" id="myModalA" tabindex="-1" role="basic" aria- hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
				<h4 class="modal-title">Add New Agent Details</h4>
			</div>
			
			<!-- content -------------------------------------------->
			
			<div style="background-color:#fff;padding:10px; ">
   
				<!--<form onsubmit="return checkdata();" class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/add_agent')?>"> -->
                  
						<div class="form-group">
							 <div class="row" style="margin-top:0px;">
									<label class="col-md-3 control-label" style="padding-top:0px">Agent Code</label>
								   <div class="col-md-4">
								   <input type="text" class="form-control"  name="agent_code" id='acode' required>
								 </div>
							 </div>
                        </div>
  
						<div class="form-group" >
							 <div class="row" style="margin-top:0px;">
								<label class="col-md-3 control-label" style="padding-top:0px">Agent Name</label>
								 <div class="col-md-7">
								   <input type="text" class="form-control"  name="agent_name"  id='aname' required>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
								<label class="col-md-3 control-label" style="padding-top:0px">Address</label>
								 <div class="col-md-7">
								   <textarea rows=2 cols=30 class="form-control"  name="agent_address"  id='aaddress' required></textarea>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
							 	<label class="col-md-3 control-label" style="padding-top:0px">Mobile</label>
								 <div class="col-md-7">
								   <input type="number" pattern="[0-9]*" class="form-control"  name="agent_mobile" id="amobile" required>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
									<label class="col-md-3 control-label" style="padding-top:0px">Email</label>
							 	 <div class="col-md-7">
								   <input type="email" class="form-control"  name="agent_email" id='aemail' required>
								 </div>
							 </div>
                        </div>
                                 <hr>

                    <div class="form-group">
                     <div class="row"><center>
                            <button class="btn btn-primary" id='submitagent'>Submit</button>
								   &nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
								   </center>
                           </div>
                       </div>
					   
                       <!-- </form> -->
                       </div>

				<!------------------------------------------------------------>
		</div>
		</div>
	<!-- /.modal-dialog -->
	</div>
</div>

<!--- MODEL 2 DISPLAY AGENT LIST -------------------->

<div class="modal fade" id="myModalSA" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Agents List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class="col-md-12">
            <table id="tblagents" class="table table-striped table-hover table-bordered" cellspacing="0">
            <thead>
            <tr>
                <th>Select</th>
				<th>ID</th>
                <th>Agent Name</th>
                <th>Mobile</th>
            </tr>
        </thead>
    </table>
        </div>
      </div>
 	 </div>
 	</div>
   </div>
  </div>
  

<div class="modal fade" id="myModalfare" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Fare List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class="col-md-12">
            <table id="Tblfarelist" class="table table-striped table-hover table-bordered" cellspacing="0">
            <thead>
            <tr>
                <th>ID</th>
				<th>Corporate Name</th>
                <th>Vehicle</th>
				<th>Hrs</th>
				<th>KM</th>
                <th>Amount</th>
            </tr>
        </thead>
    </table>
        </div>
      </div>
 	 </div>
 	</div>
   </div>
  </div>
  
  
  <div class="modal fade" id="myModalduty" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Drivers Duty List</h4>
        </div>
        <div class="modal-body">
            <div class="row" style='padding:0px 10px 0px 10px;'>
            <div class="col-md-12">
            <table id="Tbldrvduty" class="table table-striped table-hover table-bordered" cellspacing="0">
            <thead>
            <tr>
                <th>ID</th>
				<th>Date</th>
				<th>Driver Name</th>
                <th>Mobile</th>
				<th>Shift</th>
                <th>Time</th>
            </tr>
        </thead>
    </table>
        </div>
      </div>
 	 </div>
 	</div>
   </div>
   </div>
<!--- AGENTS END -------------------->



<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>  -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBx1BJxGlTRJyF2W_AdaCfRf-U0FLRxif4&libraries=places">
</script>
<script type="text/javascript">

$("#idmsg").hide();
$("#radio1").hide();
$("#radio2").hide();
$("#radio3").hide();
$("#radio4").hide();
$("#radio5").hide();
$("#radio6").hide();
$("#radio7").hide();
$("#radio8").hide();
$("#idcmobile").prop("disabled",true);
$("#idcmobile").css("background-color","#fff");
$("#purposetrip").val("Nil");

var checkData1=0;
var checkData2=0;
var checkData3=0;

if($("#msg").html()!="")
	{
		var msg1=$("#msg").html().split("#");
		swal(msg1[0],msg1[1],"success")
		$("#msg").html("");
	}
	
	
$("#hplace1").hide();
$("#hplace2").hide();
$("#hplace3").hide();
$("#idextrakm").hide();
$("#tab2").prop("disabled",true);


$("#btnnext1").click(function()
{
	checkData1=check_tab1_data();
if (checkData1==1)
{
	$("#bdetails").removeClass('active');
	$("#tpv").addClass('active');
	
	$("#tb1").removeClass('active');
	$("#tb2").addClass('active');
}
});


$("#tab1").click(function()
{
	$("#tvp").removeClass('active');
	$("#kmothers").removeClass('active');
	$("#bdetails").addClass('active');
});


$("#tab2").click(function()
{
checkData1=check_tab1_data();
if (checkData1==1)
{
	$("#tab2").prop("disabled",false);
	$("#tab2").attr("href","#tpv");
	$("#tab2").attr("data-toggle","tab");
	$("#bdetails").removeClass('active');
	$("#kmothers").removeClass('active');
	$("#tpv").addClass('active');
	
	$("#tb1").removeClass('active');
	$("#tb3").removeClass('active');
	$("#tb2").addClass('active');
}
else
{
	$("#tb2").removeClass('active');
	$("#tb3").removeClass('active');
	$("#tb1").addClass('active');
}
});
   
$("#tab3").click(function()
{
	checkData1=check_tab1_data();
	if (checkData1!=1)
	{
		$("#tb2").removeClass('active');
		$("#tb3").removeClass('active');
		$("#tb1").addClass('active');
	}
	else
	{
		checkData2=check_tab2_data();
		
		if (checkData2==1)
		{
			$("#tab3").attr("href","#kmothers");
			$("#tab3").attr("data-toggle","tab");
			$("#tpv").removeClass('active');
			$("#bdetails").removeClass('active');
			$("#kmothers").addClass('active');
			
			$("#tb2").removeClass('active');
			$("#tb1").removeClass('active');
			$("#tb3").addClass('active');
		}
		else
		{
			$("#tab2").attr("href","#tpv");
			$("#tab2").attr("data-toggle","tab");
			$("#bdetails").removeClass('active');
			$("#kmothers").removeClass('active');
			$("#tpv").addClass('active');
			
			$("#tb3").removeClass('active');
			$("#tb1").removeClass('active');
			$("#tb2").addClass('active');
		}
	}
});

function check_tab1_data()
{
	var a1=$("#category").val();
	var a2=$(".item").html();
	var a3=$("#purposetrip").val();
	var a4=$("#idcmobile").val();
	var a5=$("#nojourney").val();
	var a6=$("#startdate").val();
	var a7=$("#enddate").val();
	var a8=$("#starttime").val();
	var a9=$("#endtime").val();
	var a10=$("#refno").val();
	var a11=$("#roomno").val();
		
	
	if(!a1 || !a2 || !a3 || !a4 || !a5 || !a6 || !a7 || !a8 || !a9 || !a10 || !a11)
	{
		alert("Basic details missing.!");
	}
	
	else
	{
	return 1;
	}
}


function check_tab2_data()
{
	var a1=$("#tripfrom").val();
	var a2=$("#tripto").val();
	var a3=$("#vregno").val();
	var a4=$("#driver").val();
	var a5=$("#addiinfo").val();

	if(!a1 || !a2  || !a4 || !a5)
	{
		alert("Missing starting location and vehicle details");
	}
	else
	{
	return 1;
	}
}

function check_tab3_data()
{

	var a1=$("#paymode").val();
	var a2=$("#idminckm").val();
	var a3=$("#mincharge").val();
	var a4=$("#startkm").val();
	var a5=$("#endkm").val();
	var a6=$("#mintotal").val();
	var a7=$("#addikm").val();
	var a8=$("#additotal").val();
	var a9=$("#tollparking").val();
	var a10=$("#ispermit").val();
	
	var trop=$("#radio8").val();
	
	
	if(!a2 || !a3 || !a4 || !a5 || !a6 || !a7 || !a8 || !a9 || !a10)
	{
		alert("Kilometers and other details are missing");
		return false;	
	}
	else
	{
		if(trop!="CM")
		{
			if(!a1)
			{
				alert("Payment Mode missing.!");
				return false;
			}
			else
			{
			   return true;
			}
		}
		else
		{
			return true;
		}
	}
}


$("#btnnext2").click(function()
{
	checkData2=check_tab2_data();
if (checkData2==1)
{
	$("#bdetails").removeClass('active');
	$("#tpv").removeClass('active');
	$("#kmothers").addClass('active');
	
	$("#tb1").removeClass('active');
	$("#tb2").removeClass('active');
	$("#tb3").addClass('active');
}

});


 $("input[type='radio']").click(function(){
            var radioValue = $("input[name='sltrip']:checked").val();
			//alert(radioValue);
			if(radioValue=='LT')
			{	
				$("#hplace1").show();
				$("#hplace2").show();
				$("#hplace3").show();
				$("#pay_mode").show();
				$("#paymode").val("");
				$("#paymode1").val("");
			}
			if(radioValue=='CM')
			{	
				$("#pay_mode").hide();
				$("#paymode1").val('NONE');
			}
			else
			{
				$("#hplace1").hide();
				$("#hplace2").hide();
				$("#hplace3").hide();
				$("#pay_mode").show();
				$("#paymode").val("");
				$("#paymode1").val("");
			}
});


$("#paymode").change(function()
{
	$("#paymode1").val($(this).val());
});


//-- driver list
$("#custna1").hide();
$("#custname").hide();
$("#custmobile").hide();
$("#viewMap").prop('disabled',true);
$("#agents").hide();
$("#client_na").hide();

//-------------------------------------


//search customer

$('#id2').selectize({
     create: false
     , sortField: {
     //field: 'text',
     direction: 'asc'
   }
   , dropdownParent: 'body'
   });

  $('#id3').selectize({
     create: false
     , sortField: {
     //field: 'text',
     direction: 'asc'
   }
   , dropdownParent: 'body'
 }); 
   
   
 $(".clientname").hide();
 $(".cpurpose").hide();
 
 
 $("#category").change(function()
 {
	 var op=$("#category").val();
		
		if (op==1)
		{
		 $(".clientname").val("");
		 $(".cpurpose").val(""); 
		 $(".clientname").hide();
		 $(".cpurpose").hide(); 
		 $("#custna").show();
		 $("#client_na").hide();
		 $("#agents").hide();
		}
		else if(op==2)
		{	
		 $(".clientname").show();
		 $(".cpurpose").show(); 
		 $("#client_na").show();
		 //$("#custna1").hide();
		$("#agents").hide();
		}
		else if(op==3)
		{	
		$(".clientname").val("");
		$(".cpurpose").val(""); 
		$(".clientname").hide();
		$(".cpurpose").hide(); 
		// $("#custna").hide();
		$("#client_na").hide();
		$("#agents").show();
		}
 });
   
 $("#enquiry").change(function()
 {
	 var op=$("#enquiry").val();
	 jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/GetEnquiry",
        dataType: 'html',
        data: {opt:op},
        success: function(res) {
			//alert(res);
			var obj = jQuery.parseJSON(res);
			
			//alert(obj[0].enquiry_id);
			
			$("#custname").show();
			$("#idd2").hide();
			//$(".select_btn1").hide();
			
					
			$("#cust_name").val(obj[0].party_name);
			$("#idcmobile").val(obj[0].mobile_no);
			
			$("#nojourney").val(obj[0].nature_of_journey);
			$("#startdate").val(obj[0].starting_date);
			$("#enddate").val(obj[0].returning_date);
			$("#starttime").val(obj[0].starting_time);
			$("#endtime").val(obj[0].returning_time);
			$("#tripfrom").val(obj[0].starting_place);
			$("#tripto").val(obj[0].to_place);
        }
		});
 });
 
 
$('#btncheck').click(function () {
	
	//var Result=$("#myModal3 .modal-body");
	
	var km=$("#startkm").val();
	var vid=$("#vehicleid").val();
	
	if(km=="" && vid=="")
	{
		swal("Try Again","Select Vehicle and Strting KM.","error");
	}
	else
	{
			
		//alert(km+"-"+vid);
		if (vid=='')
		{
			swal("Try Again","Please select vehicle..!","error");
			//alert("Please select vehicle...");	
			
		}
		else if(km=='')
		{
			//alert("Please type stating kilometer...");	
			swal("Try Again","Please type starting kilometer.","error");
		}
		else
		{
			$("#idextrakm").show();
			$("#btncheck").attr("data-target","#myModal3");
			jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "Trip/check_kilometer",
			dataType: 'html',
			data: {skm:km,vid:vid},
			success: function(res) {
				
				if(res=="0")
				{
					$("#extrakm").html("Ending Km= <b>" + km + "</b><br>No diffrence, Continue.");
					$("#btnreason").hide();
				}
				else
				{
				$("#btnreason").show();	
				$("#extrakm").html(res);
				}
			//Result.html(res);
			}
			});
		}
	}
});

$("#btnreason").click(function()
{
	var Result=$("#myModal3 .modal-body");
	var km=$("#startkm").val();
	var vid=$("#vehicleid").val();
	
		$("#btnreason").attr("data-target","#myModal3");
		
	    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/set_extra_km_reason",
        dataType: 'html',
        data: {skm:km,vid:vid},
        success: function(res) {
		Result.html(res);
        }
		});
});

//-----------------------------------------

$("#id2").change(function()
{
	    var id =  $("#id2").val();
			
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_cust_mobile",
        dataType: 'html',
        data: {cid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#idcmobile").val(obj.mobile);
		       }
            });
});

$("#id3").change(function()
{
	    var id =  $("#id3").val();
		var cna=  $("#id3").html();
		
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_corporate_farelist",
        dataType: 'html',
        data: {coid: id},
        success: function(data) {
			$("#farelist").empty();
			$("#farelist").append(data);
			}
		});
	
	//fixed fare list
		jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "Trip/get_fixed_farelist",
			dataType: 'html',
			data: {coid: id},
			success: function(data) {
				$("#fixedlist").empty();
				$("#fixedlist").append(data);
				}
		});
		 
	//club fare list		 
		 jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "Trip/get_club_farelist",
			dataType: 'html',
			data: {coid: id},
			success: function(data) {
				$("#clublist").empty();
				$("#clublist").append(data);
				}
		}); 
	  
	  
	  jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_corporate_name",
        dataType: 'html',
        data: {coid: id},
        success: function(data) {
			$("#corpname").html("Fare List of : <b>"+ data+"</b>");
			 }
		});
	  
});


//get fare details -----------------------------

$('#fare tbody').on('click','tr', function(){
  var rt1=$(this).find('td').eq(3).text();
  var rt2=$(this).find('td').eq(4).text();
 // alert(rt1+ "---"+rt2);
  $("#idminckm").val(rt1);
  $("#mincharge").val(rt2);
});


$('#fixedfare tbody').on('click','tr', function(){
  var rt1=$(this).find('td').eq(2).text();
  var rt2=$(this).find('td').eq(3).text();
 // alert(rt1+ "---"+rt2);
  $("#idminckm").val(rt1);
  $("#mincharge").val(rt2);
});


$('#clubfare tbody').on('click','tr', function(){
  var rt1=$(this).find('td').eq(2).text();
  var rt2=$(this).find('td').eq(3).text();
 // alert(rt1+ "---"+rt2);
  $("#idminckm").val(rt1);
  $("#mincharge").val(rt2);
});






//drop down script .................
//..................................

$('#Tblfarelist').dataTable({
         destroy: true,
		 "ordering":false,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/farelist_ajax",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"10%","targets":3},
		{"width":"9%","targets":4},
		/*{ className : "dt-right", "targets":4},*/
		],
			   
        "columns": [
            { "data": "ID"},
			{ "data": "corp"},
		    { "data": "vehicle"},
			{ "data": "desc"},
            { "data": "farekm"},
			{ "data": "amount"},
       ]
  } );
   
   
$('#Tbldrvduty').dataTable({
         destroy: true,
		 "ordering":false,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/drv_dutylist_ajax",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"100px","targets":1},
		{"width":"100px%","targets":3},
		{"width":"100px%","targets":4},
		/*{ className : "dt-right", "targets":4},*/
		],
			   
        "columns": [
            { "data": "ID"},
			{ "data": "date"},
			{ "data": "dname"},
		    { "data": "dmobile"},
            { "data": "dshift"},
			{ "data": "dtime"},
       ]
  } );  
    
$('#TblDriver').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/driver_ajax/0",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"7%","targets":1},
		],
			   
        "columns": [
            { "data": "select"},
			{ "data": "drid"},
		    { "data": "name"},
            { "data": "mobile"},
			{ "data": "status"},
       ]
  } );
  

$("#driversearch").click(function()
{
	var sdate=$("#startdate").val();
	var edate=$("#enddate").val();
  //alert(sdate);
	
	/*if(sdate=="" || edate=="")
	{
		swal ("try Again","Please select trip starting and Ending dates.","error")
	}
	else
	{*/
		$("#driversearch").attr("data-target","#myModald");
		$('#TblDriver').dataTable({
			 destroy: true,
			"processing": true,
			"ajax": {
					/*url :"<?php echo base_url(); ?>" + "Trip/driver_ajax/"+sdate+"/"+edate,*/
					url :"<?php echo base_url(); ?>" + "Trip/driver_ajax",
				   },
			"columnDefs":[
			{"width":"6%","targets":0},
			{"width":"7%","targets":1},
			],
				   
			"columns": [
				{ "data": "select"},
				{ "data": "drid"},
				{ "data": "name"},
				{ "data": "mobile" },
				{ "data": "status"},
		   ]
		} );
	
});
  //-- vehicle List

  
  
  $('#TblVehicle').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/vehicle_ajax",
				
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		],
			   
        "columns": [
            { "data": "select"},
		    { "data": "regno"},
            { "data": "vtype" },
			{ "data": "noofseats" },
			{ "data": "status" },
       ]
  } );
  
  //attached vehile list
    
  getattached_vehicle();  // call vehicle details
  
  function getattached_vehicle()
  {
   $('#TblAttaVehicle').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/atta_vehicle_ajax",
				
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		],
			   
        "columns": [
            { "data": "select"},
		    { "data": "regno"},
            { "data": "vtype" },
			{ "data": "drname" },
			{ "data": "drmobile" },
			{ "data": "noofseats" },
       ]
  } );
  }

  //Customer list

 /* $('#TblCustomer').dataTable({
         destroy: true,
        "processing": true,
        "ajax":{
                url :"<?php echo base_url(); ?>" + "Trip/customer_ajax",// json datasource
				
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		],
			   
        "columns": [
            { "data": "select"},
		    { "data": "cname"},
            { "data": "cmobile"},
       ]
  } ); */
  
  
  
 $('#bookingid').change(function ()
  {
        var id =  $("#bookingid").val();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_booking_ref_roomno",
        dataType: 'html',
        data: {bid: id},
        success: function(data){
			var dat1=data.split("#");
			$("#refno").val(dat1[0]);
			$("#roomno").val(dat1[1]);
		}
            });
  });

     
  // selection ---- vechicle and driver -------------------

  $('#TblDriver tbody').on('click', '.drselect', function () {
  
	    var id =  $(this).attr('id');
         jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_driverDT",
        dataType: 'html',
        data: {did: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#driver").val(obj.name);
				$("#driverid").val(obj.driverid);
		       }
            });
        }); 
  
  
 $('#TblVehicle tbody').on('click', '.vselect', function () {
         var id =  $(this).attr('id');
         jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_vehicleDT",
        dataType: 'html',
        data: {vid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#vregno").val(obj.name);
				$("#vehicleid").val(obj.vehicleid);
				$("#driver").val("");
				$("#driverid").val("");
		       }
            });
        }); 

		
$('#TblAttaVehicle tbody').on('click', '.atta_vselect', function () {
        var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_atta_vehicleDT",
        dataType: 'html',
        data: {avid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#vregno").val(obj.regno);
				$("#vehicleid").val(obj.avehicleid);
				$("#driver").val(obj.drname);
				$("#driverid").val("0");
		       }
            });
    
        });
 
 /*$('#TblCustomer tbody').on('click', '.cselect', function () {
     // $(this).parent().parent().toggleClass('selected');
        var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_customerDT",
        dataType: 'html',
        data: {cid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#custname").val(obj.name);
				$("#idcmobile").val(obj.mobile);
        $("#idcid").val(obj.id);
		       }
            });
    
        }); */
 
  $('#TblCustomer tbody').on('click', '#custadd', function () 
  {
        var Result=$("#myModal2 .modal-body");
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Customer/add_customer",
        dataType: 'html',
        success: function(res) {
        Result.html(res);
                    }
            });
        }); 
 //---------------------------------------------------
  
 
$(document).ready(function () {
	$("#idsavebtn").prop('disabled', true);
	$("#idbtnget").prop('disabled', true);
	//$("#idbtncalc").prop('disabled', true);
        setTimeout(function(){ $("#msg").html(""); }, 3000);
    }); 
	
</script>

<script>
$('#example1 tbody').on('click', '.select', function () {
 var Result=$("#myModals .modal-body");
 var id =  $(this).attr('id');
 $('#regno').val( id );
 $('#myModals').modal('hide')
});
</script>


<script>
$('#example2 tbody').on('click', '.select', function () {
 var Result=$("#myModald .modal-body");
 var id =  $(this).attr('id');
 $('#driver').val(id);
 $('#myModald').modal('hide')
});

/* -------- button desable/enanble ------------------*/

$('#idminckm').focus(function () {
	$("#idbtnget").prop('disabled', false);
});

/* -------- button desable/enanble ------------------*/


        function checkDate() {
            var stDate =new Date($("#startdate").val()) //for javascript
            var enDate =new Date($("#enddate").val()); // For JQuery
		
			var g=stDate-enDate;
            if (g>0) {
                return false;
            }
            else {
               return true;
            }
        }

		
/* get Button  ----------------------*/

$('#btnSet').click(function (){
	var skm=parseInt($.trim($('#startkm').val()));
	if(isNaN(skm))
	{
		alert("Please type starting KM.");
		$("#startkm").focus(); 
	}
	var ekm=(parseInt($.trim(skm))+ parseInt($.trim($('#idminckm').val())));

	$("#endkm").val(ekm);
	
	var sdate=$('#startdate').val();
	var edate=$('#enddate').val();
	
	var mk1=parseInt($.trim($('#idminckm').val()));
	
	
	if(checkDate()==false)
	{
		swal ("Try Again","Please select end date correctly.","error")
	}
	else if(sdate=="" || edate=="")
	{
		swal("Try Again","Please choose Trip Start and End Dates.","error");
	}
	else
	{
	if(mk1<100)
	{
		$("#addikmcharge").val(0);
		var stkm=$("#startkm").val();
		stkm=parseInt(stkm)+parseInt($("#idminckm").val());
		$("#endkm").val(stkm);
		$("#mindays").val(1);
		$("#mintotal").val($("#mincharge").val());
		$("#additotal").val(0);
		$("#tollparking").val(0);
		$("#ispermit").val(0);
		$("#addikm").val(0);
	}
	else
	{
		
	if(isNaN(skm) || isNaN(ekm) || skm=="0" || ekm=="0" || (parseInt(skm) > parseInt(ekm)))
	{
		alert("Please get Total KM and running KM. Click on the Distance Button");
	}
	else if(mk1.length==0)
	{
		swal("Try Again","Please type minimum charge KM..!","error");
	}	
	else
	{		
		
	var rkm=(parseInt(ekm)-parseInt(skm));	
	var edate1=new Date(edate);
	var sdate1=new Date(sdate);
	var days = (parseInt(edate1.getTime() - sdate1.getTime())) / (1000 * 60 * 60 * 24);
	
	if(parseInt(days)==0)
	{
		days = 1;
	}
			
	//var adkm=parseInt(rkm)-(parseInt(days)*parseInt(mk1))
		
		
	if((skm+mk1)>ekm)	
	{
		var adkm=(ekm-skm+mk1);
	}
	else
	{
		var adkm="0";
	}
		
		
	//var days = (edate1.getTime() - sdate1.getTime())

	$("#addikm").val(adkm);
	$("#runningkm").val(rkm);
	$("#mindays").val(days);
	var mtot=(parseInt($("#mindays").val())*parseInt($("#mincharge").val()))
	$("#mintotal").val(mtot);
	$("#addikmcharge").val(0);
	$("#tollparking").val(0);
	$("#ispermit").val(0);
	

	$("#idbtncalc").prop('disabled', false);
	$("#idsavebtn").prop('disabled', true);
	}
	}
	}
});
/*----------------------------------------*/

/* claulate totals ----------------------*/
$('#idbtncalc').click(function () {

var mdays=$('#mindays').val();	
var mcharge=$('#mincharge').val();	

var akm=$('#addikm').val();	
var akmc=$('#addikmcharge').val();
	
var tpc=$('#tollparking').val();	
var isp=$('#ispermit').val();	
var mtot1=$('#mintotal').val();

//var mtot=(parseFloat(mdays)* parseFloat(mcharge)).toFixed(2);
var adtot=(parseFloat(akm)* parseFloat(akmc)).toFixed(2);
var ttot=(parseFloat(mtot1)+parseFloat(adtot)).toFixed(2);
var gtot=(parseFloat(ttot)+parseFloat(tpc)+parseFloat(isp)).toFixed(2);

$("#additotal").val(adtot);
$("#tripcost").val(gtot);
$("#lblgtotal").html(gtot);
$("#idsavebtn").prop('disabled', false);
//$("#idsavebtn").prop('disabled', false);
});
/*----------------------------------------*/

/// Add  new Atached Vehicle details
 
  $('#idaddvehicle').click(function()
	{
		   var vtyp=$('#mvtype').val();
		   var nseat=$('#mnseats').val();
		   var regn=$('#mregno').val();
		   var rcna=$('#mrcname').val();
		   var rcmob=$('#mrcmobile').val();
		   var drna=$('#mdrname').val();
		   var drmob=$('#mdrmobile').val();
		   var skm=$('#mstartkm').val();
		   var ekm=$('#mendkm').val();
		   
			jQuery.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>' + 'Attachvehicle/modal_addvehicle',
			dataType: 'html',
			data: {vtype:vtyp,nseats:nseat,regno:regn,rcname:rcna,rcmobile:rcmob,drname:drna,drmobile:drmob,startkm:skm,endkm:ekm},
			success: function(res) 
			{
				getattached_vehicle();
			}			   
	   });
  });
  
  /*----------------------------------- Agents ------------------------ */
  
  var table= $('#tblagents').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/agentlist_ajax",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"7%","targets":1},
		],
			   
        "columns": [
            { "data": "select"},
			{ "data": "agid"},
		    { "data": "agname"},
            { "data": "agmobile"},
       ]
  });
  
  
  
  
  $("#acode").blur(function(){
	if($("#acode").val()==""){$("#acode").css('border','1px solid red');}
	else {$("#acode").css('border','1px solid #c6c6c6');}
	
});

$("#aname").blur(function(){
	if($("#aname").val()==""){$("#aname").css('border','1px solid red');}
	else {$("#aname").css('border','1px solid #c6c6c6');}
});

$("#aaddress").blur(function(){
	if($("#aaddress").val()==""){$("#aaddress").css('border','1px solid red');}
	else {$("#aaddress").css('border','1px solid #c6c6c6');}
});
	
$("#amobile").blur(function(){
	if($("#amobile").val()==""){$("#amobile").css('border','1px solid red');}
	else {$("#amobile").css('border','1px solid #c6c6c6');}
});

$("#aemail").blur(function(){
	if($("#aemail").val()==""){$("#aemail").css('border','1px solid red');}
	else {$("#aemail").css('border','1px solid #c6c6c6');}
});


function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test($email );
}


$("#submitagent").click(function()
		{

		var acod=$("#acode").val();
		var ana=$("#aname").val();
		var aadd=$("#aaddress").val();
		var amob=$("#amobile").val();
		var aema=$("#aemail").val();

		var res1=true;
		var res2=true;
		var res3=true;
		var res4=true;
		var res5=true;
		var res6=true;
		

if(acod==""){res1=false;$("#acode").css('border','1px solid red'); }else{res1=true;$("#acode").css('border','1px solid #c6c6c6');}
if(ana==""){res2=false;$("#aname").css('border','1px solid red');}else{res2=true;$("#aname").css('border','1px solid #c6c6c6');}
if(aadd==""){res3=false;$("#aaddress").css('border','1px solid red');}else{res3=true;$("#aaddress").css('border','1px solid #c6c6c6');}
if(amob==""){res4=false;$("#amobile").css('border','1px solid red');}else{res4=true;$("#amobile").css('border','1px solid #c6c6c6');}
if(aema==""){res5=false;$("#aemail").css('border','1px solid red');}else{res5=true;$("#aemail").css('border','1px solid #c6c6c6');}
	
		if(res1!=false && res2!=false && res3!=false && res4!=false && res5!=false )
			{
	
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "General/add_agent1",
				dataType: 'html',
				data: {acode:acod,aname:ana,aaddress:aadd,amobile:amob,aemail:aema},
				success: function(data) 
				 {
					 
				  if(parseInt(data)>0)
				  {
					  $("#agentid").val(data);
					  $("#agent").val(ana);
					  swal("Saved","Agent details saved..","success");
					  
				  }
				  else
				  {
					  swal("Cancelled","Agent details missing, Try again..","error");
				  }
				}
				});
			}
			else
			{
				swal("Missing","Agent details missing, Try again..","error");
			}
		});
	
	
	var table= $('#tblagents').dataTable({
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/agentlist_ajax",// json datasource
               },
		"columnDefs":[
		{"width":"6%","targets":0},
		{"width":"7%","targets":1},
		],
			   
        "columns": [
            { "data": "select"},
			{ "data": "agid"},
		    { "data": "agname"},
            { "data": "agmobile"},
       ]
  });
  
  
  $('#tblagents tbody').on('click', '.agselect', function ()
  {
	    var id =  $(this).attr('id');
		
		jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/get_agentsDT",
        dataType: 'html',
        data: {aid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#agent").val(obj.agname);
				$("#agentid").val(obj.agid);
		       }
            });
   }); 
  
 
  
</script>



 