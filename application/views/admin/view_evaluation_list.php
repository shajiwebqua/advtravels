<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 

  <!-- Main content -->
 
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><b>View Evaluation List</b> </h1>
    <ol class="breadcrumb" style='font-size:15px;'>
		<li><a  href="<?php echo site_url('Services/Evaluation') ?>"><button class='btn btn-primary'><i class="fa fa-plus" aria-hidden="true"></i> Add Service Evaluation</button></a></li>
		<li><a id='view-pdf' href="<?php echo site_url('Reports/Evaluation') ?>"><button class='btn btn-success'><i class="fa fa-list" aria-hidden="true"></i> Get Report</button></a></li>
       </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 
	  
  <section class="content"> 
  		<div id='idmsg' style="background-color:#fff;margin-bottom:3px;">
		   <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
		</div>
	
    <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!------------------------------------ Add new user details ----------------------------------->

<div style="background-color:#fff;padding:15px; ">
							
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
 
                      <div class="row" >
                      <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                   
                                <div class="row" style='background-color:#e4e4e4;padding:5px; margin:0px;'>
								<label class='col-md-2 control-label la-right'>Select Date:</label>
								<form enctype="multipart/form-data" method="post"  action="<?php echo site_url('Services/view_evaluation/1') ?>">
								<div class='col-md-2'>
								 <input type='text' class='form-control' name='sdate' id='datepicker1' placeholder='--date--' required>
								</div>
								<div class='col-md-1'>
								<button type='submit' class='btn btn-primary' style='margin-top:2px;'>Get</button>
								</div>
								</form>
								<form enctype="multipart/form-data" method="post"  action="<?php echo site_url('Services/view_evaluation/2') ?>">
								<label class='col-md-2 control-label la-right'>Select Month:</label>
								<div class='col-md-2'>

								<select name='smonth' class='form-control' required>
								<option value=''  >---month----</option>
								<option value='1'>JANUARY</option>
								<option value='2'>FERUARY</option>
								<option value='3'>MARCH</option>
								<option value='4'>APRIL</option>
								<option value='5'>MAY</option>
								<option value='6'>JUNE</option>
								<option value='7'>JULY</option>
								<option value='8'>AUGUST</option>
								<option value='9'>SEPTEMBER</option>
								<option value='10'>OCTOBER</option>
								<option value='11'>NOVEMBER</option>
								<option value='12'>DECEMBER</option>
						        </select>
								</div>
								<div class='col-md-1'>
								<button type='submit' class='btn btn-primary' style='margin-top:2px;'>Get</button>
								</div>
								</form>

								</div>
								
								
	<div class='row' style='margin:15px 0px;'>	
	<label class='control-label'>Service Evaluation List of : <b><?php echo strtoupper($sehead);?></b></label>
	</div>	
	<div class='row' style='margin:10px 0px;'>							
	<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
			    <th width="3%">Action</th>
                <th width="5%">ID</th>
                <th>Client</th>
                <th>Guest Name</th>
                <th>Date</th>
                <th>Vehicle</th>
                <th>Feedback</th>
                <th>Action_Taken</th>
                               
            </tr>
        </thead>
        
        <tbody>
        <?php $i=1;foreach($eva_result as $row){
			?>
            <tr>
                <td>	<a href="#" onclick="edit_service('<?php echo $row->se_id;?>')" data-toggle='modal' class='edit'><button class='btn btn-primary btn-xs'  data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></a>
				<?=anchor('Services/delete_evaluation/'.$row->se_id, "<button style='margin-left:3px;' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>", array('id' =>'del_conf'));?>
				</td>
				<td><?= $row->se_id;?></td>
                <td><?php if($row->se_service_type==1)echo "INDIVIDUAL"; else echo "CORPORATE";?></td>
                <td><b>Guest:&nbsp; </b><?=$row->se_guest_name?><br><b>Mob:&nbsp; </b><?=$row->se_contact_no?> </td>
                <td><?=date_format(date_create($row->se_date),'d-m-Y')?></td>
                <td><b>Vehicle:&nbsp;</b><?=  $row->vehicle_regno?><br><b>Chauffeur:&nbsp; </b><?=$row->driver_name?></td>
                <td><?=$row->se_feedback?></td>
                <td><?=$row->se_actiontaken?></td>
            </tr>
         <?php $i++;} ?>   
        </tbody>
    </table> </div>
	
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
  </div>
  </div>
  
  </div>


</section>
</div>
<!-- /.content-wrapper --> 
<div class="modal fade" id="edit_myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content" id="result">
      
    </div>
  </div>
</div>

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">


$("#msg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"success")
	if(mg[0]==2)
		swal("Updated",mg[1],"success")
	if(mg[0]==3)
		swal("Deleted",mg[1],"success")
	if(mg[0]==4)
		swal("Try Again",mg[1],"error")
    $("#msg").html("");
  }

 $('#example').DataTable({
       "scrollX": "100%",
          
    });

function edit_service(id)
{
  $.ajax({
  type: "post",
  url: "<?php echo base_url();?>Services/edit_evaluation?id="+id,
    success: function(data){ 
      $('#edit_myModal2').modal('show');
      $('#result').html(data);
    }
  });
}
		
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

$('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	/*endDate:'now'*/
   });
   
   
   $('#view-pdf').on('click',function(){
     
		var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        $.createModal({
        title:'Booking List',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
	   return false; 

    });  
</script>



