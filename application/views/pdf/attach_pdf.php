<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

 <table width="100%"><tr><td><h4>Vehicle Vendors List </h4></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
		<table style="width:100%;border:1px solid #e4e4e4;" cellpadding=5 cellspacing=0>
		   <thead>
			<tr>
			   <tr style='color:#4b88ed;border:1px solid #e4e4e4;'>
			     <th class='th1'>Slno</th>
			     <th class='th1'>Reg.No</th>
				 <th class='th1'>Type</th>
                 <th class='th1'>Seats</th>
				 <th class='th1'>RC_Owner</th>
				 <th class='th1'>Mobile</th>
				 <th class='th1'>Driver</th>
				 <th class='th1'>Mobile</th>
				 </tr>
			</tr>
		</thead>
		<tbody>
			<?php
			$slno=1;
				foreach ($results as $key => $value) {
					
				 if($value->attached_status==1)
					{
					  $st="<font color='green'>Active</font>";
					}
					else
					{
					  $st="<font color='red'>Inactive</font>";
					}	
				?>
				<tr >
				<td class='td1'><?=$slno;?> </td> 
				<td class='td1'><?=$value->attached_vehicleno;?></td>
				<td class='td1'><?=$value->attached_vehicletype;?></td>
				<td class='td1'><?=$value->attached_noofseats;?> </td>
				<td class='td1'><?=$value->attached_rcname;?> </td>
				<td class='td1'><?=$value->attached_rcmobile;?> </td>
				<td class='td1'><?=$value->attached_drname;?> </td>
				<td class='td1'><?=$value->attached_drmobile;?> </td>

			</tr>
				<?php
				
				$slno++;
				}
				?>
		  
		</tbody>
		</table>
</body>
</html>