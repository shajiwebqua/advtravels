<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');
?>
<style>
	/* hide number  spinner*/
	input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
	.ttext
	{
		background-color:#fff;
		font-weight:bold;
		color:Green;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 

	<!-- Main content -->

	<?php 
    //include("application/views/common/top_menu.php");
	?>

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><b>Attached Vehicle Details</b> </h1>
		<ol class="breadcrumb" style='font-size:15px;'>
	    <li><a id='attach' href="<?php echo site_url('attachvehicle/index') ?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-cab" aria-hidden="true"></i> Vehicle List</button></a></li>
		</ol>
	</section>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
	<!-- Content Header end -->
	<section class="content"> 
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12">

				<!--Add new user details -->
				<div style="background-color:#fff;border-radius:3px;padding:15px; ">

					<div class="portlet-title">
 <!--   <div class="caption">
        <div style="color:blue;" >
          Details                   
      </div> -->
     <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
         <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div></center>
		  <center><div id='idh5'><?php echo $this->session->flashdata('errmsg'); ?></div></center>
         </div>
      <!--</div> -->
  </div>
  <div class="portlet-body form">
  		<!-- text input -->
  		<div class="box-body">
  			<div class='col-md-12'>
  			<form  class='form-horizontal' role='form' method='post' action="<?php echo base_url('Attachvehicle/addvehicle') ?>" enctype='multipart/form-data' onsubmit='return checkdata();' >
                    <!-- text input -->
			<div class='row'>
			<div class='col-md-8'>
	   
            <div class='form-group'>
            <div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label'>Vehicle Type : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='vtype' class='form-control'  required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >No os seats : </label>
				<div class='col-md-2' style='padding-left:19px;'>
				<input type='text' name='nseats' class='form-control'  required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' >Vehicle Reg. No : </label>
				<div class='col-md-6' style='padding-left:19px;'>
				<input type='text' name='regno' class='form-control'  required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >R/C Owner Name : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='rcname' class='form-control'  required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Mobile : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='number' name='rcmobile' class='form-control'  required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' >Driver Name : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='drname' class='form-control'  required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Mobile No : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='number' name='drmobile' class='form-control'  required />
				</div>           
			</div>
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' style="color:blue">Kilometers : </label>	
			</div>	
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Starting : </label>
				<div class='col-md-3' style='padding-left:19px;'>
				<input type='text' name='startkm' class='form-control'  required />
				</div>           
				</div>           
			</div>

        </div>
			
      
 </div>
 </div>
     <label style='height:1px;width:100%;background-color:#cecece;'></label>
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-8'>
                <input type='submit' class='btn btn-primary'  style='padding:7px 20px 7px 20px' value='Add Vehicle Details'/>
                </div> 
                </div>                                
           </form> 
           </div>

</div>
</div>	
</div>   
</div>
</section>
</div>

<?php include("application/views/common/footer.php");?>


<script type="text/javascript">
	  $("#idmsg").hide();
//sweet alert ----------------------------
  if($("#idh5").html()!="")
  {
    swal( $("#idh5").html(),"","success")
    $("#idh5").html("");
  }
// sweet alert box -------------------------- 
     function checkdata()
     {

      var mob=$('#mobile').val();
      var landline=$('#landline').val();

      
      if(mob.length<10 || mob.length>10)
      {
        $('#mobmessage').show('');
        $('#mobmessage').html('Invalid mobile no,10 digits only.');
        return false;
      }
      else if(landline.length<11 || landline.length>11)
      {
       $('#mobmessage').hide('');
       $('#landmessage').show('');
       $('#landmessage').html('Invalid mobile no,11 digits only.');
       return false;
     }

     else
     {
      $('#mobmessage').hide();
      $('#landmessage').hide();
      $('#mobmessage').html('');
      $('#landmessage').html('');
      return true;
    }
    }


</script> 

