<?php
include('application/views/common/header.php');
?>
<style>
input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
</style>

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#00adee;font-weight:bold;margin-bottom:10px;'>Receipts
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
    
     </section>
	
        <!-- Main content -->
    <section class="content">
	
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box" style="border:1px solid #f5f3f3;">
        <div class="box-header with-border">
			<div class='row'>
			<div class='col-md-4'>
				<h3 class="box-title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>&nbsp; Create Receipts</h3>
			</div>
			<div class='col-md-6'>
				<label id='mes'><?php echo $this->session->flashdata('message');?></label>
			</div>
			<div class='col-md-2' style='text-align:right'>
				<a href="<?php echo base_url('Account/View_trans')?>" ><input type='submit' name='btnledgerwise1' class='btn btn-warning' value='Transactions List'></a>
		    </div>
			</div>
        </div>
		
        <div class="box-body">

        	<div class='row' style='padding:15px;'>
			<div class='col-md-8'>
			<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Voucher/save_receipts')?>" enctype="multipart/form-data">
				  <div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Voucher Type :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="rvtype" value='RECEIPTS' readonly>
								</div>
							</div>
						</div>
						
						
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label"> Voucher Date :</label>
								<div class="col-md-4">
									<input type="text" class="form-control"  name="rvdate" value='<?php echo date('d-m-Y');?>' readonly>
								</div>
							</div>
						</div>
								
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label"> Voucher No :</label>
								<div class="col-md-4">
									<input type="text" class="form-control"  name="rvno" required>
								</div>
							</div>
						</div>
						<!-- <div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Account :</label>
								<div class="col-md-4">
									<select  class="form-control" name='raccount' required>
									  <option value="">---select----</option>
									  <option value="CASH">CASH</option>
									  <?php
										//$gquery=$this->db->select("*")->from('acc_groups')->limit('24')->get()->result();
										//foreach( $gquery as $g)
										//{
											//echo "<option value='".$g->acc_grpid."'>".$g->acc_grpname."</option>";
										//}
									  ?>
														  
									</select>
								</div>
							</div>
						</div>  -->
												
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Ledger :</label>
								<div class="col-md-7">
								<select  class="form-control" name='rvledger' required>
									  <option value="">---select----</option>
									<?php
										$lquery=$this->db->select("*")->from('acc_ledgers')->get()->result();
										foreach($lquery as $l)
										{
											echo "<option value='".$l->acc_led_id."'>".$l->acc_led_description."</option>";
										}
									  ?>
								</select>
								</div>
							</div>
						</div>
			
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Payment Mode :</label>
								<div class="col-md-4">
									<select  class="form-control" name='paymode' id='paymode' required>
									  <option value="">---select----</option>
									  <option value="1">CASH</option>
									  <option value="2">BANK</option>
									  <option value="3">PDC</option>
									</select>
								</div>
							</div>
						</div>
						
						
						<div class="form-group" id='cheque'>
						   <div class="row">
							  <label class="col-md-4 control-label">Cheque No :</label>
								<div class="col-md-4">
									<input type="text" class="form-control"  name="rvchno" >
								</div>
							</div>
							
							<div class="row" style='margin-top:7px;'>
							  <label class="col-md-4 control-label">Cheque Date :</label>
								<div class="col-md-4">
									<input type="text" class="form-control"  name="rvchdate"  id="rvchdate" >
								</div>
							</div>
							
							<div class="row" style='margin-top:7px;'>
							  <label class="col-md-4 control-label">Realise Date :</label>
								<div class="col-md-4">
									<input type="text" class="form-control"  name="rvredate"  id="rvredate">
								</div>
							</div>
						</div>
								
						
			
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Amount :</label>
								<div class="col-md-4">
									<input type="number" class="form-control"  name="rvamount" required>
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Narration :</label>
								<div class="col-md-7">
									<textarea rows=3 class="form-control"  name="rvnarrat" required></textarea>
							  </div>
							</div>
						</div>
						
						<hr>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label"></label>
								<div class="col-md-7">
									<input type="submit" class="btn btn-primary "  name="btnsubmit" value="Save Receipts">
								</div>
							</div>
						</div>
						
			</form>
		
		
			</div>
			</div>

        </div>
	</div>
	</div>
	
</section>
    <!-- content wrapper -->
</div>
     <?php
  include('application/views/common/footer.php');
  ?>

</body>
</html>
  <script type="text/javascript">
  
 $("#cheque").hide();
  
   $(document).ready(function() {
	   //$("#pledger").select2(); 
	   });
	   
   $('#rvdate').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d',
	autoclose:true,
});
  
$('#rvchdate').datepicker({
    format: 'dd-mm-yyyy',
	//startDate: '-3d',
    autoclose:true,
	
});

   $('#rvredate').datepicker({
    format: 'dd-mm-yyyy',
	//startDate: '-3d',
    autoclose:true,
});
  
  
 $("#paymode").change(function()
 {
	 var pmode=$("#paymode").val();
	 if(pmode==2 || pmode==3)
	 {
		 $("#cheque").show();
	 }
	 else
	 {
		 $("#cheque").hide();
	 }
	 
 });
  
  
  $("#example").dataTable({
	  "ordering":false,
  });
  
  $("#mes").hide();
  
  
  var msg=$("#mes").html().split("#");
  
  if($("#mes").html()!="")
  {
	  var msg=$("#mes").html().split("#");
	if(msg[0]=='1')
	  swal("",msg[1],"success"); //Saved.!
	else if(msg[0]=='2')
	  swal("",msg[1],"success"); //Updated.!
    else if(msg[0]=='3')
	  swal("",msg[1],"success"); //Removed.!
	else if(msg[0]=='4')
	  swal("",msg[1],"error"); //Try Again.
     $("#mes").html("");
  }
  
$('#example tbody').on('click','.edit', function()
{
  //var rt1=$(this).parent('tr')
  var rid=$(this).closest('tr').find('td').eq(1).text();
  
  jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Voucher/get_edit_receipts",
        dataType: 'html',
        data: {reid:rid},
        success: function(res) 
		{
			var ldt=res.split(',');
			  $("#mrid").val(ldt[0]);
			  $("#mrledger").val(ldt[1]);
			  $("#mramount").val(ldt[2]);
			  $("#mrnarrat").val(ldt[3]);
        }
		});
 });	
  
$('#example tbody').on('click','.del', function()
{
  var rid=$(this).closest('tr').find('td').eq(1).text();
  
		  swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this receipt.!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "Cancel",
		  closeOnConfirm: true,
		  closeOnCancel: true
		},
		function(isConfirm) 
		{
		  if (isConfirm)
 		  {
			  jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>" + "Voucher/delete_receipt",
					dataType: 'html',
					data: {reid:rid},
					success:function(res)
					{
						window.location.href="<?php echo base_url('Voucher/receipts/3');?>";
					}
					}); 
			
		  }
		});
 });	
    
  </script>
</body>
</html>

  
  