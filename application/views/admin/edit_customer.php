<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');
?>
<style>
	/* hide number  spinner*/
	input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
	.ttext
	{
		background-color:#fff;
		font-weight:bold;
		color:Green;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 

	<!-- Main content -->
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><b>Edit Customer Details</b> </h1>
		<ol class="breadcrumb">
			<li> <a href="<?php echo base_url("Customer");?>" style="color:#4b88ed;font-size:15px;"> <i class="fa fa-backward" aria-hidden="true"></i>Back to Trip List</a></li>
		</ol> 
	</section>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
	<!-- Content Header end -->
	<section class="content"> 
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12">

				<!--Add new user details -->
				<div style="background-color:#fff;border-radius:10px;padding:15px; ">

					<div class="portlet-title">
 <!--   <div class="caption">
        <div style="color:blue;" >
          Details                   
      </div> -->
      <center><h5 style="color:green;" id="h5msg" ><?php echo $this->session->flashdata("message"); ?></h5></center>
      <!--</div> -->
  </div>
  <div class="portlet-body form">
  		<!-- text input -->
  		<div class="box-body">
  			<div class='col-md-12'><form  class='form-horizontal' role='form' method='post' action="<?php echo base_url('customer/update_customer') ?>" enctype='multipart/form-data' onsubmit='return checkdata();' >
                    <!-- text input -->
    <div class='row'> 
    
    <div class='col-md-6'>
        
    
      <div class='form-group'>
           <label class='col-md-4 control-label' style='padding-top:5px;'>Customer Code</label>
          <div class='col-md-7'>  
          <input type='text' name='custcode' class='form-control' value='$row->customer_code' required />
          </div>                   
          </div>
            
       <div class='form-group'>
              <label class='col-md-4 control-label' style='padding-top:5px;'>Date</label>
           <div class='col-md-7'>  
          <input type='date' name='custdate' class='form-control' value='$row->customer_edate' required />
          </div>                   
          </div>
      
          <div class='form-group'>
              <label class='col-md-4 control-label' style='padding-top:5px;'>Category</label>
           <div class='col-md-7'>  
        <select name='category' class='form-control'>
        <option>ONE</option>
        <option>TWO</option>
        <option>THREE</option>
        <option>FOUR</option>
        <option>FIVE</option>
            
        </select>
      
          </div>                   
          </div>

        <div class='form-group'>
              <label class='col-md-4 control-label' style='padding-top:5px;'>C/O Agent</label>
           <div class='col-md-7'>  
          <input type='text' name='coagent' class='form-control' value='$row->customer_coagent' required />
          </div>                   
          </div>    
    
           <div class='form-group'>
                     <label class='col-md-4 control-label' style='padding-top:5px;'>Name</label>
           <div class='col-md-7'>  
          <input type='text' name='name' class='form-control'  value='$row->customer_name' required />
          </div>                   
          </div>
                    
           
        

          <div class='form-group'>
                     <label class='col-md-4 control-label' style='padding-top:5px;'>Address </label>
                    <div class='col-md-7'>  
          
         <textarea name='address' rows='3' class='form-control' required>$row->customer_address</textarea>
          </div>                   
          </div>
  </div>

<div class='col-md-6'>
         
        <div class='form-group'>
          <label class='col-md-3  control-label'>Mobile</label>
          <div class='col-md-8'>
          <input type='number' class='form-control' placeholder='Enter mobile no' name='mobile' id='mobile' value='$row->customer_mobile' required>
          <label id='mobmessage' style='color:red;'></label>
            </div>
          </div>
          
          <div class='form-group'>
          <label class='col-md-3  control-label'>Landline </label>
          <div class='col-md-8'>
          <input type='number' class='form-control' placeholder='Enter landline' name='landline' value='$row->customer_landline' required id='landline' required/>
          <label id='landmessage' style='color:red;'></label>
          </div>
          </div>
      <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:5px;'>Email</label>
                    <div class='col-md-8'>  
          
          <input type='email' name='email' class='form-control'  value='$row->customer_email' required/>
          </div>                   
          </div>
      
      <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:5px;'>Country</label>
                    <div class='col-md-8'> 
           <select name='country' class='form-control' id='country1'>
       <option value=''>----------</option>
       <?php
       $result1=$this->Model_customer->view_country();
       
      foreach($result1 as $ro){
                echo "<option value='".$ro->country_id."'>".$ro->country_name."</option>";
      }
      ?>
      </select>
          </div>                   
          </div>
      
      <div class='form-group'>
              <label class='col-md-3 control-label' style='padding-top:5px;'>Area</label>
              <div class='col-md-8'>  
              <select name='area' class='form-control' id='idarea'>
        </select>
          </div>                   
          </div>
      
       <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:5px;'>Description</label>
                    <div class='col-md-8'>  
          <textarea name='description' rows='3' class='form-control' required></textarea>
          </div>                   
          </div>
      
    <!--  <div class='form-group'>
                      <label class='col-md-3  control-label'>Status </label>
          <div class='col-md-8'>
                      <select type='text' name='status' class='form-control'>";
 
             echo '<option value="0" ';if($row->customer_status=='0')echo "selected"; echo'>Inactive</option>
              <option value="1"';if($row->customer_status=='1')echo "selected"; echo ">Active</option>
              </select>
                           </div>                     
      </div>  -->

</div>  
</div>

<label style='width:100%;height:1px;'></label>  
                <div class='form-group'> 
                <div class='col-md-12'>
                <center><input type='submit' class='btn btn-primary' value='Update Details'/>
        </center>
                </div>
                </div>     
<br>
          </form></div>

</div>
</div>	
</div>   
</div>
</div>
</section>
</div>

<?php include("application/views/common/footer.php");?>
<script type="text/javascript">
	     $('#mobmessage').hide();
	           $('#landmessage').hide();
	       function checkdata()
	       {
	         
	         var mob=$('#mobile').val();
	         var landline=$('#landline').val();
	       
	         if(mob.length<10 || mob.length>10)
	         {
	           $('#mobmessage').show('');
	           $('#mobmessage').html('Invalid mobile no,10 digits only.');
	           return false;
	         }
	         else if(landline.length<11 || landline.length>11)
	         {
	          $('#mobmessage').hide('');
	           $('#landmessage').show('');
	           $('#landmessage').html('Invalid mobile no,11 digits only.');
	           return false;
	         }
	             
	         else
	         {
	         $('#mobmessage').hide();
	           $('#landmessage').hide();
	           $('#mobmessage').html('');
	           $('#landmessage').html('');
	           return true;
	         }
	       }
	          
	     /* get area of selected country  */

	       $('#country1').change(function () {
	         /*alert($('#country1').val());*/
	             var id=$('#country1').val();
	             jQuery.ajax({
	             type: 'POST',
	             url: 'view_areas',
	         data:{cid:id},
	             success: function(data)
	         {
	             $('#idarea').html(data);
	             }
	             });
	            }); 
	       

</script> 

