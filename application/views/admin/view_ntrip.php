<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}


</style>
 
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Closed Trip</b></h1>
   <ol class="breadcrumb" style='font-size:15px;'>
  <li> <a href="<?php echo base_url('Trip/index');?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'>  <i class="fa fa-cab" aria-hidden="true"></i> New Trip Sheet </button></a></li>
     </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header  end -->	  

	  
  <section class="content"> 
          <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
		</div>  
  <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details-->
	
	 <div style="background-color:#fff;border-radius:2px;padding:15px; ">
					
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">

				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
								<tr style='color:#4b88ed'>
              <!--   <th>View</th> -->
								 <th>Trip Id</th>
								 <th>Customer</th>
								 <th>Driver</th>
								 <th>Date</th>
								 <th>Days</th>
								 <th>KM</th>
								 <th>Charge</th>
								 <th>Others</th>
								 <th>Total</th>
								 <th>Status</th>
                 
								 </tr>
								</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Edit</h4>
								</div>
							</div>
							</div>
						<!-- /.modal-dialog -->
		 </div>
         </div>			


   
     </div>       

				
	</div>
	<!-- End user details -->

    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------

 setTimeout(function(){ $("#idh5").html(""); }, 3000);
 
      $('#example').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
          "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/trip_najax",// json datasource
               },
			   "columnDefs":[
			   {"width":"8%","targets":0},
			   {"width":"15%","targets":2},
			   {"width":"10%","targets":3},
			   {"width":"6%","targets":4},
			   {"width":"5%","targets":5},
			   {"width":"5%","targets":6},
			   {"width":"7%","targets":7},
			   {"width":"7%","targets":8},
			   ],
			   
        "columns": [
      // { "data": "view" },
			
			{ "data": "tripid" },
      { "data": "customer"},
			{ "data": "driver"},
      { "data": "date" },
			{ "data": "days" },
			{ "data": "totalkm" },
			{ "data": "charge" },
			{ "data": "ocharge" },
			{ "data": "gtotal" },
			{ "data": "status" },
       ]
  } );

var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });			   
   
  $('#view-pdf').on('click',function(){
       var id=$('#example').find('tr.selected').find('td').eq(1).text();
     if(id=="")
     {
       alert ("please Select customer details..");
     }
     else
     {
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
		var pdf_link = $(this).attr('href')+'/'+id;
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Customer Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
	 });
    } return false; 

    });    
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

       
 //});


 $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/approve_ntrip",
        dataType: 'html',
        data: {trip_id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 
  


</script>



