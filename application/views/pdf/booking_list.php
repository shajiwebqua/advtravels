<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
.th1
{
	border-bottom:1px solid #e4e4e4;
	text-align:left;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>

 <table width="100%" style='font-size:15px;'><tr><td><h4><?php echo $bcap;?></h4></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
 		<table class="table table-striped table-hover table-bordered" id="example" width='100%'>
              <thead>
                <tr>
                 <th width='10%' style='text-align:left;'>ID</th>
				 <th width='10%' style='text-align:left;'>Trip_ID</th>
                 <th width='25%' style='text-align:left;'>Client</th>
                 <th width='22%' style='text-align:left;'>Room/Guest</th>
				 <th width='26%' style='text-align:left;'>Location/Vehicle</th>
				 <th width='12%' style='text-align:left;'>Status</th>
                </tr>
				<tr><td colspan='10'><hr style='margin:0px;'></td></tr>
                </thead>
				
				<tbody>
			<?php
			  $rgname="";
			  $loca="";
			foreach ($booking as $r)
			{
				if($r->book_roomno!=="")
				{
					$rg=$r->book_roomno;
				}

				if($r->book_clienttype=="1"){ 
				$ct="INDIVIDUAL"; 
				$cna =$r->book_guestname;
				}
				else if($r->book_clienttype=="2"){
				$ct="CORPORATE";
				$cna=$r->customer_name;
				}
				else if($r->book_clienttype=="3"){
					$ct="AGENT";
					$cna=$r->customer_name;
				}
					
				if($r->book_tripoption=="ST"){ $trop="SHORT TRIP"; }
				else if($r->book_tripoption=="LT"){ $trop="OUT STATION"; }
				else if($r->book_tripoption=="AD"){ $trop="AIRPORT DROP"; }
				else if($r->book_tripoption=="AP"){ $trop="AIRPORT PICKUP"; }
				else if($r->book_tripoption=="RD"){ $trop="RAILWAY DROP"; }
				else if($r->book_tripoption=="RP"){ $trop="RAILWAY PICKUP"; }
				else if($r->book_tripoption=="FP"){ $trop="FIXED PACKAGE"; }
				else if($r->book_tripoption=="CM"){ $trop="COMPLIMENTARY"; }

				if($r->book_status=="1"){
					$act=$close.$cancel.$del;
					$stat="<font color=green><b>New</b></font>"; 
					}
				else if($r->book_status=="2"){
					$act=$del;
					$stat="<font color=red><b>Cancelled</b></font>"; 
					}
				else if($r->book_status=="3"){
					$act=$del;
					$stat="<font color=purple><b>Closed</b></font>";
					}
				
				$rgname="<b>Room No :</b> ".$rg."<br><b>Contact : </b>".$r->book_contactno."<Br><b>Reporting:</b> <br>&nbsp;&nbsp;&nbsp;Date : ".date_format(date_create($r->book_tripdate),'d-m-Y')."<br>&nbsp;&nbsp;&nbsp;Time : ".$r->book_reporttime;
				$rgname.="<br><b>Vehicle: </b>".$r->book_vcategory;
				
			    $loca="<b>From :</b>".$r->book_location."<br><b>To : </b>".$r->book_tripto."<br><b>Trip :</b>".$trop."<br><b>Vehicle :</b>".$r->veh_type_name."<br><b>Reg.No : </b>".$r->book_vehicleregno."<br><b>Driver : </b>".$r->book_drivername."<br><b>Mobile : </b>".$r->book_drivermobile;
				$loca.="<br><b>Decription: </b>".$r->book_description;
				
				$client="<b>Type: </b>".$ct."<br><b>Client:</b>".$cna."<br><br><b>Payment :</b>".$r->book_payment;
				?>
							
				 <tr style='border-bottom:1px solid #000;'>
                 <td><?=$r->book_id;?></td>
				 <td><?=$r->book_tripno;?></td>
                 <td><?=$client;?></td>
                 <td><?=$rgname;?></td>
				 <td><?=$loca;?></td>
				 <td><?=$stat;?></td>
                </tr>
				<tr><td colspan='10'><hr style='margin:0px;'></td></tr>
				</tr>
				
				<?php
				}
				?>
				</tbody>
            </table>
</body>
</html>