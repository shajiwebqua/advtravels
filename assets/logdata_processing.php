<?php
/*
 * DataTables example server-side processing script.
 *
 * Easy set variables
 */
 
// DB table to use
$table = 'log';
 
// Table's primary key
$primaryKey = 'log_id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => 'log_id',  	   'dt' => 0 ),
    array( 'db' => 'log_staffid',  'dt' => 1 ),
    array( 'db' => 'log_staffname','dt' => 2 ),
   

    array(
        'db'        => 'log_date',
        'dt'        => 3,
        'formatter' => function($d, $row ) {
            return date( 'd-m-Y', strtotime($d));
        }
    ),
	 array( 'db' => 'log_time',     'dt' => 4 ),
	array( 'db' => 'log_entryid',  'dt' => 5 ),
	array( 'db' => 'log_desc',     'dt' => 6 ),
	
   /* array(
        'db'        => 'salary',
        'dt'        => 5,
        'formatter' => function( $d, $row ) {
            return '$'.number_format($d);
        }
    )*/
);
 
// SQL server connection information
$sql_details = array(
    'user' => 'root',
    'pass' => '',
    'db'   => 'advtravels',
    'host' => 'localhost'
);
  
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('ssp.class.php');


		//custom conditions ----------------------------------
		$dt1=$_REQUEST['date1'];
		$dt2=$_REQUEST['date2'];
		if(!empty($dt1) and !empty($dt2))
		{
		$d1=explode("-",$dt1);
		$da1=$d1[2]."-".$d1[1]."-".$d1[0];

		$d2=explode("-",$dt2);
		$da2=$d2[2]."-".$d2[1]."-".$d2[0];

		$where="log.log_date>='".$da1 ."' and log.log_date<='".$da2."'";
		}
		else
		{
		$where=null;		
		}

echo json_encode(
    /*SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )*/
	SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns,$whereResult=null,$whereAll=$where)
);
