<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . "/libraries/Rupees.php";

class Invoice extends CI_Controller {

function __construct(){
parent::__construct();
$user = $this->session->userdata('userid');
   if (!isset($user))
   { 
		   redirect('/');
   } 
   else
   { 
	   return true;
   }
$this->load->helper(array('form', 'url'));
$this->load->model('Model_invoice');
}

public function invoice_form()
{
	$data['trids']=$this->input->post('trids');
	$this->load->view('admin/invoice_form',$data);
}

public function Trips()
{
	$this->load->view('admin/view_unpaidTrips');
}

public function Create()
{
	$tripids=$this->input->post('trip_ids');
	$invno=$this->input->post('invno');
	$invdate=$this->input->post('invdate');
		
	$delinote=$this->input->post('delinote');
	$terms=$this->input->post('terms');
	$suppref=$this->input->post('suppref');
	$otherref=$this->input->post('otherref');
	$bordno=$this->input->post('bordno');
	$borddated=$this->input->post('borddated');
	$desdno=$this->input->post('desdno');
	$delindate=$this->input->post('delindate');
	$despatch=$this->input->post('despatch');
	$destin=$this->input->post('destin');
	$desc=$this->input->post('desc');
	$hsnsac=$this->input->post('hsnsac');
				
	$dt=explode("-",$invdate);
	$dt1=$dt[2]."-".$dt[1]."-".$dt[0];
	
	$sdt1="";
	if(!empty($borddated))
	{
		$sdt=explode("-",$borddated);
		$sdt1=$sdt[2]."-".$sdt[1]."-".$sdt[0];
	}
	
	$ndt1="";
	if(!empty($delindate))
	{
		$ndt=explode("-",$delindate);
		$ndt1=$ndt[2]."-".$ndt[1]."-".$ndt[0];
	}
		
	$ivno=explode("-",$invno);
	$tripid=explode(',',$tripids);
	
	$this->load->model('Model_invoice');
	
	$amount=0;
	
	for($i=1;$i<count($tripid);$i++)
	{
		$amount+=$this->Model_invoice->get_amount_of_trip($tripid[$i]); //get total amount
	}
		
	$invdata=$this->Model_invoice->get_customer_id($tripid[1]);  //get customer id
	
	$gst=($amount*$invdata->up_gstper)/100;
	
	$gst=number_format($gst,"2",".","");
    $total=number_format(($amount+$gst),"2",".","");
	
	$obj= new Rupees();
	
    $gtotwords=$obj->Words($total);
	$taxwords=$obj->Words($gst);	
			
	$invodt=array(
	"invo_date"=>$dt1,
	"invo_tripids"=>$tripids,
	"invo_invoiceid"=>$ivno[1],
	"invo_delinote"=>$delinote,
	"invo_terms"=>$terms,
	"invo_supplierref"=>$suppref,
	"invo_otherref"=>$otherref,
	"invo_buyersordno"=>$bordno,
	"invo_buyersdated"=>$sdt1,
	"invo_desdocno"=>$desdno,
	"invo_delinotedate"=>$ndt1,
	"invo_despatched"=>$despatch,
	"invo_destination"=>$destin,
	"invo_description"=>strtoupper($desc),
	"invo_hsnsac"=>$hsnsac,
	"invo_customer"=>$invdata->up_custid,
	"invo_amount"=>$amount,
	"invo_gstper"=>$invdata->up_gstper,
	"invo_gst"=>$gst,
	"invo_total"=>$total,
	"invo_totalwords"=>$gtotwords,
	"invo_taxwords"=>$taxwords,
	"invo_status"=>"0",
	);	
		
	$this->Model_invoice->insert_invoice($invodt);
	
	//update invoice id in upaid trips table -----------------
	$trids=explode(",",$tripids);
	for($i=1;$i<count($trids);$i++)
	{
		$dat=array(
		"up_invoiceno"=>$ivno[1],
		"up_status"=>"1"
		);
		$this->db->where('up_tripid',$trids[$i]);
		$this->db->update("unpaid_trips",$dat);
	}
	//--------------------------------------------------------
		
	$this->session->set_flashdata('message', 'Invoice successfully created.');
	redirect('Invoice/Trips');	
}

public function unpaid_trip_ajax()
{
	$this->load->model("Model_invoice");
	
	$cltype=$this->uri->segment(3);
	
	$where="";
	if(!empty($cltype))
	{
	$sdate=$this->uri->segment(4);
	$edate=$this->uri->segment(5);

	$dt1="";
	$dt2="";

		$d1=explode("-",$sdate);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$edate);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];

			$where="(unpaid_trips.up_date>='".$dt1. "' and unpaid_trips.up_date<='".$dt2."') and unpaid_trips.up_custid=".$cltype;		
			$ses_where=$where. " and unpaid_trips.up_status='1' ". "order by up_id  desc";
			$results = $this->Model_invoice->view_unpaid_trips($where);
	}
	else
	{
		$results = $this->Model_invoice->view_unpaid_trips("");
	}
  
  $data = array();
  $slno = 1;
  
  foreach($results as $up)
  {

		$tot=$up->up_amount+$up->up_gst;  
		
		if($up->up_invoiceno!=0)
		{
			$view="<center><a href='#myModal2' id='$up->up_invoiceno' data-toggle='modal' class='view'><b>INV-".$up->up_invoiceno."</b></a></center>";
		}
		else
		{
			$view="";
		}
		  	  	  
    array_push($data, array(
	  "check"=>"<input type='checkbox' class='sub_chk'  style='width:17px;height:17px;' data-id='".$up->up_id."'>",
      "upid" => $up->up_id,
      "update" => $up->up_date,
      "uptripid" =>$up->up_tripid,
	  "upinvno"=>$view,
	  "upcust" => nl2br($up->customer_name)."<br>".nl2br($up->customer_address),
      "upamt" => number_format($up->up_amount,'2','.',''),
      "upgstper" => $up->up_gstper,
      "upgst" => number_format($up->up_gst,'2','.',''),
	  "total"=>"<b>".number_format($tot,'2','.','')."</b>"
	  
      //"print" => "<a href='". base_url('Trip/tripquotation_print/' . $result->tripquot_id) ."' class='btn btn-primary'> Print </a>" ,
    ));
  }
  echo json_encode(array("data" => $data));
}

public function Payments()
{
	//$this->load->model("Model_invoice");
    //$results['dt'] = $this->Model_invoice->view_unpaid_invoices("3","");
	$this->load->view('admin/view_record_payments');
}

public function unpaid_invoice_ajax()
{
  $up1=$this->uri->segment(3);
  $cl=$this->uri->segment(4);
  if($up1=="" and $cl=="")
  {
	  $up1=3;
  }
  $this->load->model("Model_invoice");
  $results = $this->Model_invoice->view_unpaid_invoices($up1,$cl);
  
  $data = array();
   
 foreach($results as $iv)
  {
    $stat="";
 	if($iv->invo_status=="1"){
		//$act=$edit.$close.$cancel.$del;
		$pay="";
		$stat="<font color=green><b>Paid</b></font>"; 
		}
		
	else if($iv->invo_status=="0"){
		//$act=$del;
		$pay="<a href='' id='$iv->invo_invoiceid' data-toggle='modal'  class='btnpay'><center><button class='btn btn-warning btn-xs' >Record Payment</button></center></a>"; 
		$stat="<font color=red><b>Unpaid</b></font>"; 
		
		$del=anchor('Invoice/delete_invoice/'.$iv->invo_invoiceid, '<button class="btn btn-danger btn-xs" style="margin-top:3px;">Delete</button>', array('id' =>'del_conf'));
		}
	
	$invview="<center><a  href='".site_url('Pdf/pdf_invoice')."' id='".$iv->invo_invoiceid."' class='view'><b><font color=green><i class='fa fa-eye' aria-hidden='true'></i> INV-".$iv->invo_invoiceid."</font></b></a></center>";
	$invtrips="<center><a href='".site_url('Pdf/pdf_invoice_trips')."' id='$iv->invo_invoiceid'  ref='$iv->invo_customer' class='invtrips'><b><span style='color:#e07c00'><i class='fa fa-eye' aria-hidden='true'></i> Trips</span></b></a></center>";		
    
	array_push($data, array(
	  "pay" => $pay.$del,
	  "invid" => $iv->invo_id,
      "invdate" =>date_format(date_create($iv->invo_date),'d-m-Y'),
	  "invtrips" =>$invtrips,
      "invno" =>$invview,
	  "invcust" => nl2br($iv->customer_name)."<br>".nl2br($iv->customer_address),
	  "invstat" =>$stat,
      "invamt" => number_format($iv->invo_amount,'2','.',''),
      "invgstper" => $iv->invo_gstper,
      "invgst" => number_format($iv->invo_gst,'2','.',''),
	  "invtotal"=>"<b>".number_format($iv->invo_total,'2','.','')."</b>"
    ));
  }
  echo json_encode(array("data" => $data));
}

public function record_payment()
{
  $ivid=$this->input->post('ivid');
  
  $tamt=$this->db->select('invo_total')->from('invoices')->where('invo_invoiceid',$ivid)->get()->row()->invo_total;
  $ledg=$this->db->select('ledg_id,ledg_name')->from('ac_ledgers')->get()->result();
  
 // $this->load->view('admin/record_payment',$data);
  
  echo "            
     <div class='modal-header'>
    <button type='button' class='btn btn-default btn-xs' style='float:right;' data-dismiss='modal'>X</button>
         <h4 class='modal-title' > <font color='blue'>Record payments</font></h4>
    </div>
    <br>
  
	<form  class='form-horizontal' role='form' method='post' action='". site_url('Invoice/save_payments'). "' enctype='multipart/form-data'>
	<input type='hidden' name='invoid'  value='$ivid'/>
    <div class='row'>
    <div class='col-md-12'>
   
          <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:4px;'>Date </label>
          <div class='col-md-4'>
          <input type='text' class='form-control' id='dtpicker1' name='edate' value='".date('d-m-Y')."'> 
          </div>
          </div>
		  
		  <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:4px;'>Amount Received</label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='amount' value='".number_format($tamt,'2','.','')."' required> 
          </div>
          </div>
		  
		  <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:4px;'>Received From</label>
          <div class='col-md-8'>
		  <select class='form-control select2' name='ledgerid' style='width:100%;' required>
		  <option value=''>---select ledger---</option>";
		  foreach($ledg as $lg)
		  {
			echo "<option value='".$lg->ledg_id."'>".$lg->ledg_name."</option>";
		  }
		  echo"
		  </select>
		  </div>
          </div>
		  
		  <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:4px;'>Payment Mode</label>
          <div class='col-md-4'>
		  <select class='form-control' name='paymode' style='width:100%;' required>
		  <option value='' >---select---</option>
		  <option value='1' >CASH</option>
		  <option value='2' >BANK</option>
		  </select>
		  </div>
          </div>
		  
		  <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:4px;'>Narration</label>
          <div class='col-md-8'>
          <textarea rows=3 class='form-control' name='narra' required> </textarea>
          </div>
          </div>
		  		  
    </div>
    </div> 
    <div class='modal-footer'>
      <input type='submit' class='btn btn-primary'  style='padding:5px 15px 5px 15px;'  value='Submit Payment'/>
        <button type='button' class='btn btn-default' style='padding:5px 15px 5px 15px;' data-dismiss='modal'>Close</button>
     </div>     
  
</form>   
<script>
$('.select2').select2();

$('#dtpicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   </script>
   ";
}

public function save_payments()
{
	$invoid=$this->input->post('invoid');
	$edate=$this->input->post('edate');
	$amount=$this->input->post('amount');
	$ledgerid=$this->input->post('ledgerid');
	$paymode=$this->input->post('paymode');
	$narra=$this->input->post('narra');
		
	$d1=explode("-",$edate);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
			
	$invodt=array(
	"invo_ledgerid"=>$ledgerid,
	"invo_paiddate"=>$dt1,
	"invo_paymode"=>$paymode,
	"invo_narration"=>$narra,
	"invo_status"=>"1"
	);
	
	// save data to account ------------------------
	
	//---------------------add data to accounts
		//HEAD ENTRY

		 $this->load->model('Model_accounts');
		 
		 $accdata=array(
		 "accu_date"=>date('Y-m-d'),
		 "accu_description"=>"invoice:" .$invoid.", Amount ".$amount . " Paid on ".$edate, 
		 "accu_voucherno"=>0,
		 "accu_narration"=>$narra,
		  );
		  
		  $insid=$this->Model_accounts->insert_data("ac_accounts",$accdata);

		 //ENTRY 1
		 $accdata2=array(
		 "entry_date"=>date('Y-m-d'),
		 "entry_accuid"=>$insid,
		 "entry_ledgerid"=>"1", 		//FOR TRIP SALES/cash
		 "entry_debit"=>"0",
		 "entry_credit"=>$amount,
		 );
		 
		 $this->Model_accounts->insert("ac_account_entry",$accdata2);
		 
		 //ENTRY 2
		 $accdata3=array(
		 "entry_date"=>date('Y-m-d'),
		 "entry_accuid"=>$insid,
		 "entry_ledgerid"=>$ledgerid,  					//for CUSTOMERS
		 "entry_debit"=>$amount, 
		 "entry_credit"=>"0",
		 );
		 
		 $this->Model_accounts->insert("ac_account_entry",$accdata3);
			
		 $acccash=array(
		 "cashbook_date"=>date('Y-m-d'),
		 "cashbook_headid"=>$insid,
		 "cashbook_debit"=>"0",
		 "cashbook_credit"=>$amount, 
		 "cashbook_narration"=>"Amount : ". $amount ."(Invoice: " .$invoid.") Received on ".$edate, 
		 );
		 $this->Model_accounts->insert("ac_cashbook",$acccash);
		 
			
	//-----------------------------------------------
	
	//update data ----------------------------	
	$this->db->where('invo_invoiceid',$invoid);
	$this->db->update('invoices',$invodt);
	
	$updt=array(
	"up_status"=>"1",
	);
	
	$this->db->where('up_invoiceno',$invoid);
	$this->db->update('unpaid_trips',$updt);
	
	$this->session->set_flashdata('message', 'Payment successfully recorded.');
	redirect('Invoice/Payments');	
}

public function Paid()
{
	$op=$this->uri->segment(3);
	$where="";
	
	$udata=array('invo_custid'=>"",'invo_date'=>"All",'invowhere'=>"");
	$this->session->set_userdata($udata);
	
	if($op==1)
	{
		$sdate=$this->input->post('sdate');
		$edate=$this->input->post('edate');
		$cltype=$this->input->post('cltype');
		
		$dt1="";
		$dt2="";

		$d1=explode("-",$sdate);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$edate);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];

		//for report pdf
		$this->session->set_userdata('invo_custid',$cltype);
		$dt3=$sdate." to ".$edate;
		$this->session->set_userdata('invo_date',$dt3);
		//----
			
		$where="(invoices.invo_date between '".$dt1."' and '".$dt2."') and invoices.invo_customer='".$cltype."' and invoices.invo_status='1'";
		
	}
	else if($op==2 or empty($op))
	{
		$where="invoices.invo_status='1'";
	}
	
	//for report pdf
		$this->session->set_userdata('invowhere',$where);
	
	$this->load->model('Model_invoice');
	$data['invopaid']=$this->Model_invoice->paid_invoices($where);
	$this->load->view('admin/view_paid_invoices',$data);
}


public function Unpaid()
{
	$op=$this->uri->segment(3);
	$where="";
	$udata=array('invo_custid'=>"",'invo_date'=>"All",'invowhere'=>"");
	$this->session->set_userdata($udata);
	if($op==1)
	{
		$sdate=$this->input->post('sdate');
		$edate=$this->input->post('edate');
		$cltype=$this->input->post('cltype');
		
		$dt1="";
		$dt2="";

		$d1=explode("-",$sdate);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$edate);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];

		//for report pdf
		$this->session->set_userdata('invo_custid',$cltype);
		$dt3=$sdate." to ".$edate;
		$this->session->set_userdata('invo_date',$dt3);
		//----
			
		$where="(invoices.invo_date between '".$dt1."' and '".$dt2."') and invoices.invo_customer='".$cltype."' and invoices.invo_status='0'";
		
	}
	else if($op==2 or empty($op))
	{
		$where="invoices.invo_status='0'";
	}
	
	//for report pdf
		$this->session->set_userdata('invowhere',$where);
	
	$this->load->model('Model_invoice');
	$data['invopaid']=$this->Model_invoice->unpaid_invoices($where);
	$this->load->view('admin/view_unpaid_invoices',$data);
}

public function delete_invoice()
{
	$invoid=$this->uri->segment(3);
	
	$updt=array(
	"up_status"=>"0",
	"up_invoiceno"=>"0"
	);
	
	$this->db->where('up_invoiceno',$invoid);
	$this->db->update('unpaid_trips',$updt);
	
	$this->load->model('Model_invoice');
	$res=$this->Model_invoice->del_invoice($invoid);
	if($res)
	{
		$this->session->set_flashdata('message', 'Invoice successfully removed.');
        redirect('Invoice/Payments');
	}
	else
	{
		$this->session->set_flashdata('message1', 'Can\'t remove invoice, try again.');
        redirect('Invoice/Payments');
	}
	
}












public function index()
{
  $this->load->Model('Model_vehicle');
  $this->load->Model('Model_driver');
  $data['results'] = $this->Model_vehicle->view_vehicle();
  $data['results2'] = $this->Model_driver->view_driver();
  //$this->load->view('admin/add_trip',$data);
  $this->load->view('admin/add_trip_new',$data);
}

public function view_trip()
{
   $this->load->view('admin/view_trip');
}

public function trip_quotation()
{
   $this->load->view('admin/trip_quotation');
}

public function tripquotation_print()
{
   $tripquot = $this->uri->segment(3);
   $this->load->model("Model_trip");
   $data["trip_quot"] = $this->Model_trip->get_quotatiionlist(array("tripquot_id" => $tripquot));
   $this->load->view('admin/tripquotation_print',$data);
}

public function trip_qutationlist(){
  $this->load->view("admin/tripquotation_list");
}


public function trip_quatation_ajax()
{
  $this->load->model("Model_trip");
  $results = $this->Model_trip->get_quotationlist();
  $data = array();
  $slno = 1;
  foreach($results as $result){
    array_push($data, array(
      "slno" => $slno++,
      "customer" => $result->cust_name,
      "address" => nl2br($result->cust_address),
      "phone" => $result->cust_phone,
      "description" => nl2br($result->description),
      "total" => $result->total,
      "print" => "<a href='". base_url('Trip/tripquotation_print/' . $result->tripquot_id) ."' class='btn btn-primary'> Print </a>" ,
    ));
  }
  echo json_encode(array("data" => $data));
}

public function completed_trip()
{
   $this->load->view('admin/view_completed_trip');
}

public function add_tripquatation(){
  $trip_coat = array(
    "cust_name" => $this->input->post("cust_name"),
    "cust_address" => $this->input->post("cust_addr"),
    "cust_phone" => $this->input->post("phone"),
    "description" => $this->input->post("description"),
    "trip_mincharge" => $this->input->post("mincharge"),
    "trip_perkmcharge" => $this->input->post("percharge"),
    "trip_othercharge" => $this->input->post("othercharge"),
    "acm_type" => $this->input->post("acm_type"),
    "acm_cost" => $this->input->post("acm_cost"),
    "acm_menu" => $this->input->post("acm_menu"),
    "food_type" => $this->input->post("food_type"),
    "food_cost" => $this->input->post("food_cost"),
    "food_menu" => $this->input->post("food_menu"),
    "place_visit" => $this->input->post("place_visit")

  );

  $this->db->insert("quotation", $trip_coat);
     $uid = $this->db->insert_id();
    $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
      $date=date("Y/m/d");
      date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa'); 
      $userdt1 = array(
        'log_staffid' => $staffid,
        'log_staffname'=>$staffname,
        'log_time'=>$time,
        'log_date'=>$date,
        'log_entryid'=>$uid,
        'log_desc'=>'Quotation Added'

        );
      $this->load->model('Model_customer');
      $this->Model_customer->form_insertlog($userdt1);
  redirect("Trip/trip_qutationlist");
}

 
public function add_new_trip()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('clienttype', 'clienttype', 'required');

 if($this->form_validation->run())
    {
    $custid=$this->input->post('custid');
	$clienttype=$this->input->post('clienttype');
	
	if($clienttype=='1')
	{
		$clientname='Nil';
		$clientpurpose='Nil';
		$customermobile=$this->input->post('cmobile');
		$agentid='0';
	}
	
	if($clienttype=='2')
	{
		//$clientname=$this->input->post('clientna');
		$clientpurpose=$this->input->post('purposetrip');
		$custid=$this->input->post('custid');
		
		$clientname1=$this->db->select('customer_name')->from('customers')->where('customer_id',$custid)->get()->row();
		$clientname=$clientname1->customer_name;
			
		
		$customermobile=$this->input->post('cmobile');
		$agentid='0';
	}
	
	if($clienttype=='3')
	{
		$clientname='Nil';
		$clientpurpose='Nil';
		$custid=$this->input->post('custid');
		$customermobile=$this->input->post('cmobile');
		$agentid=$this->input->post('agentid');
	}
		
	
	$purpose=$this->input->post('nojourney');
    $vehicle=$this->input->post('vehicleid');
	$regno=$this->input->post('vehicle');
    $driver=$this->input->post('driverid');
	$drname=$this->input->post('driver');
	
    $from=$this->input->post('tripfrom');
    $to=$this->input->post('tripto');
	$vplace=$this->input->post('vplace');
    $startdate=$this->input->post('startdate');
    $enddate=$this->input->post('enddate');
    $starttime=$this->input->post('starttime');
    $endtime=$this->input->post('endtime');
    $startkm=$this->input->post('startkm');
    $endkm=$this->input->post('endkm');
	
	$refno=$this->input->post('refno');
    $roomno=$this->input->post('roomno');
	
	
	//$cbehavior=$this->input->post('cbehavior');

	$mdays=$this->input->post('mindays');
    $mcharge=$this->input->post('mincharge');
    $mtotal=$this->input->post('mintotal');
    
	$adkm=$this->input->post('addikm');
    $adcharge=$this->input->post('addikmcharge');
    $adtot=$this->input->post('additotal');
		
    $tcharge=$this->input->post('tripcost');
	
    $tollparking=$this->input->post('tollparking');
    $ispermit=$this->input->post('ispermit');
	$paymode=$this->input->post('paymode1');
		
	//$ocharge=$this->input->post('ocharge');
   // $otherdesc=$this->input->post('otherdesc');
   
    $ocharge=0;
    $otherdesc="";

    $adiinfo=$this->input->post('addiinfo');
	$minchargekm=$this->input->post('minckm');
	
	$tripop=$this->input->post('sltrip');
		
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
				
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
   
       $userdt = array(
                'trip_cust_id' => $custid,
				'trip_clienttype'=>$clienttype,
				'trip_clientname'=>$clientname,
				'trip_clientpurpose'=>$clientpurpose,
				'trip_clientcustomer'=>$custid,
				'trip_clientmobile'=>$customermobile,
				
                'trip_purpose' => $purpose,
				'trip_vehicle_id' =>$vehicle,
				'trip_vehicle_regno'=>$regno,
                'trip_from' =>$from,
                'trip_driver_id' =>$driver,
				'trip_drivername'=>$drname,
                'trip_to' =>$to,
				'trip_visitingplace'=>$vplace,
                'trip_startdate' => $startdate,
                'trip_enddate' => $enddate,
				'trip_starttime' =>$starttime,
                'trip_endtime' =>$endtime,
				'trip_paymentmode'=>$paymode,
                'trip_startkm' =>$startkm,
                'trip_endkm' =>$endkm,
				'trip_refno' =>$refno,
                'trip_roomno' =>$roomno,
											
				'trip_mindays' => $mdays,
                'trip_mincharge' => $mcharge,
				'trip_mintotal' =>$mtotal,
				
                'trip_addikm' =>$adkm,
                'trip_addikmcharge' =>$adcharge,
                'trip_additotal' =>$adtot,
				
				'trip_othercharge' =>$ocharge,
                'trip_otherdesc' =>$otherdesc,
                
                'trip_charge' => $tcharge,
                'trip_tollparking' => $tollparking,
				'trip_interstate' =>$ispermit,
                'trip_gtotal' =>$tcharge,
                'trip_addiinfo' =>$adiinfo,
				'trip_minchargekm'=>$minchargekm,
				'trip_agentid'=>$agentid,
				'trip_option'=>$tripop,
				'trip_status'=>"1",
               );
			   
	$this->load->Model('Model_trip');
        $this->Model_trip->form_insert($userdt);
		
			
	// vehicle available status update -------------
		$vudt = array(
                'vehicle_available' =>'1',
		);
		$this->Model_trip->vehicle_update($vehicle,$vudt);
		
	// driver trip date update-----------------

		/*$drudt = array(
                'driver_startdate' =>$startdate,
				'driver_enddate' =>$enddate,
		);*/
		
		$drudt = array(
                'driver_available' =>"1",
		);
				
		$this->Model_trip->driver_update($driver,$drudt);
		
//update srevice KM ------------------

	$vs1=$this->db->select('count(*) as cn1')->from('services')->where('ser_vehicle_id=',$vehicle)->get()->row();

	if($vs1->cn1>0)
	{
		$vser1 = array(
                'ser_kilometer2' =>$endkm,
			);
		$this->Model_trip->update_service_km($vehicle,$vser1);
	}
	else
	{
		$vser1 = array(
                'ser_vehicle_id' =>$vehicle,
				'ser_kilometer1' =>$startkm,
				'ser_kilometer2' =>$endkm,
				'ser_status' =>'1',
			);
		$this->Model_trip->insert_service_km($vser1);
	}
	//-------------------------
	  $this->session->set_flashdata('message', 'Saved # Trip details successfully added.');
      $this->load->Model('Model_trip');
      $id1=$this->Model_trip->getLastInserted();
      $id=$id1->trip_id;
       
		   
	   
     $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$id,
            'log_desc'=>'Trip Details Added'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
        redirect('Trip/index');  
    }
    else
	{
      $this->session->set_flashdata('message', 'Details missing, try again');
      //$data['message']="Data Inserted Successfully";
      redirect('Trip/index');
    }		
			
}
				 
public function trip_ajax()
{
	$op=$this->uri->segment(3);
	$this->load->model('Model_trip');
	$ctmes="";
	if ($op==1)
	{
		$results=$this->Model_trip->view_trip();
	}
	else if($op==2)
	{
		$dt1=date('Y-m-d',strtotime("-3 months"));
				
		$results=$this->Model_trip->view_completed($op,$dt1,"");
		$ctmes="Period of trip list [ ".date_format(date_create($dt1),'d-m-Y') ." => ". date('d-m-Y')." ]";
		
	}
	else if($op==3)
	{
		$dt1=$this->uri->segment(4);
		$dt2=$this->uri->segment(5);
		
		$ctmes="Period of trip list [ ".$dt1." => ". $dt2." ]";
		
		$d1=explode("-",$dt1);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	
		$d2=explode("-",$dt2);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
		//$dt2=date_create($dt2);
		
		
	$results=$this->Model_trip->view_completed($op,$dt1,$dt2);
	}
	else if($op==4)
	{
	 $mon=$this->uri->segment(4);
	 $mdt="2017-".$mon."-01";
	 $ctmes="Period of trip list [ ".date("F",strtotime($mdt))."-".date('Y')." ]";
	 $results=$this->Model_trip->view_completed($op,$mon,"");
	 }	
	else if($op==5)  //upcoming trips
	{
	$opst=5;
	$results=$this->Model_trip->view_UPCtrip();
	}
		$this->session->set_userdata("comtrip",$ctmes);
       $data1 = array();
        foreach($results  as $r) 
        {
        $complete="<a href='".base_url()."Trip/completetrip/".$r->trip_id."' id='$r->trip_id' data-toggle='modal' class='complete open-AddBookDialog2'>
		<center><button class='btn btn-primary btn-xs' data-title='complete' data-toggle='modal' >Complete</button></center></a>";
		$edit="<a href='".base_url()."Trip/edit_trip/".$r->trip_id."' id='$r->trip_id' data-toggle='modal' class='edit open-AddBookDialog2'>
		<center><button class='btn btn-warning btn-xs' data-title='edit' >Edit</button></center></a>";
		
		$view="<a href='#myModal1' id='$r->trip_id' data-toggle='modal' class='view open-AddBookDialog2'>
		<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-list-alt'></span></button></center></a>";
		$print="<a href='pdf_trip/$r->trip_id'  class='print open-AddBookDialog2'>
		<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-print'></span></button></center></a>";
		$share="<a href='#myModals' id='$r->trip_id' data-toggle='modal' class='share open-AddBookDialog2'><center><button class='btn btn-success btn-xs' data-title='Share' data-toggle='modal' ><span class='glyphicon glyphicon-share'></span></button></center></a>";
			
		$del=anchor('Trip/del_entry/'.$r->trip_id, 'Delete', array('id' =>'del_conf'));
		$invoice=anchor('Trip/views_trip/'.$r->trip_id, 'Invoice', array('class' =>'btn btn-warning btn-xs'));
		$tclose="<a href='#myModals' id='$r->trip_id' data-toggle='modal' class='tclose open-AddBookDialog2'><center><button class='btn btn-primary btn-xs' data-title='Delete' data-toggle='close' >Close Trip</button></center></a>";
			  
		  $d1=$r->trip_startdate;
		  $dt1=date("d-m-Y", strtotime($d1));
		  $d2=$r->trip_enddate;
		  $dt2=date("d-m-Y", strtotime($d2));
		  
		  //$date1=date_create($r->trip_startdate);
		  //$date2=date_create($r->trip_enddate);
		  //$diff=date_diff($date1,$date2);
		  
		  $diff1= $r->trip_mindays;
		  
		  //$diff1=$diff->format("%R%a days");
		  
		  //$diff1=$diff->format("%a");
		  
		  $km=$r->trip_endkm - $r->trip_startkm ;
		  
		if($r->trip_status==2)
		{
			$st="<font color='red'>Completed</font>";
		}
		else if($r->trip_status==1)
		{
			$st="<font color='green'>Running</font>";
		}
		else
		{
			$st="<font color='green'>New</font>"; 
		}
		
		$oamt=($r->trip_tollparking + $r->trip_interstate + $r->trip_othercharge);
		$charge1=$r->trip_charge-$oamt;
		if($charge1<0)
		{
			$charge1=0;
		}
		
                      array_push($data1, array(
                            "invoice"=>$invoice.$tclose,
							"edit"=>$edit,
							"tclose"=>$tclose,
                            "complete"=>$complete,
                            "tripid"=>"$r->trip_id",
							"refno"=>"$r->trip_refno",
                            "customer"=>"$r->customer_name",
							"comp_customer"=>$r->customer_name."<br>Ph:".$r->customer_mobile,
							"custphone"=>"$r->customer_mobile",
                            //"purpose"=>"$r->trip_purpose",
                            "driver"=>"$r->driver_name",
                            "date"=>"$dt1"."<font color='red'> =></font> "."$dt2" ,
							"comp_date"=>$dt1."<font color='red'> =></font>".$dt2."<br>Days :<b>".$diff1."</b><br>KM :<b>".$km."</b>",
                            "days"=>$diff1,
                            "totalkm"=>$km,
                            "charge"=>"<b>".number_format($charge1,2,".","")."</b>",
							"comp_charge"=>"Charge: <b>".number_format($charge1,2,".","")."</b><br>Others :<b>".($r->trip_tollparking + $r->trip_interstate + $r->trip_othercharge)."</b>",
							"ocharge"=>$oamt,
							"gtotal"=>"<b>$r->trip_gtotal</b>",
							
                            // "vehicle"=>"$r->trip_vehicle_id",
                            // "from"=>"$r->trip_from",
                            // "startdate"=>"$r->trip_startdate",
                            // "enddate"=>"$r->trip_enddate",
                            // "starttime"=>"$r->trip_starttime",
                            // "endtime"=>"$r->trip_endtime",
                            // "startkm"=>"$r->trip_startkm",
                            //   "endkm"=>"$r->trip_endkm",
                            // "enddate"=>"$r->trip_enddate",
                            // "charge"=>"$r->trip_charge",
                            // "tollparking"=>"$r->trip_tollparking",
                            // "interstate"=>"$r->trip_interstate",
                            //  "gtotal"=>"$r->trip_gtotal",
                            // "description"=>"$r->trip_description",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}



public function trip_UPCajax()
{
	$this->load->model('Model_trip');
	$results=$this->Model_trip->view_UPCtrip();
		
       $data1 = array();
        foreach($results  as $r) 
        {
 	  
			  $d1=$r->trip_startdate;
			  $dt1=date("d-m-Y", strtotime($d1));
			  $d2=$r->trip_enddate;
			  $dt2=date("d-m-Y", strtotime($d2));

			  $date1=date_create($r->trip_startdate);
			  $date2=date_create($r->trip_enddate);
			  $diff=date_diff($date1,$date2);
			  //$diff1=$diff->format("%R%a days");
			  
			  $diff1=$diff->format("%a");

		if($r->trip_status==1)
		{
			$st="<font color='magenta'>Coming</font>";
		}
		              array_push($data1, array(
                            "tripid"=>"$r->trip_id",
                            "customer"=>"$r->customer_name",
							"custphone"=>"$r->customer_mobile",
                            "driver"=>"$r->trip_drivername",
                            "date"=>"$dt1"."<font color='red'> =></font> "."$dt2" ,
                            "days"=>$diff1,
                            "purpose"=>"$r->trip_purpose",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
}



public function pdf_trip()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id = $this->uri->segment(3);
    $this->load->model('Model_pdf');
    //$data['results']=$this->Model_pdf->trip($id);
    $results=$this->Model_pdf->trip($id);
					 // $ids=$this->model_staff->projectdet($sess);
					 //$i=array('id'=>$ids->user_id, 'name'=>$ids->name, );
					 //$data['resul']=$this->buku_model4->projectdet1($sess);
	ini_set('memory_limit','256M');

	$path   = 'http://localhost/advtravels/uploads/pdf/';  
	echo realpath($path);   
	$html=  $this->load->view('pdf/trip_pdf',$results,true);

	$pdf->WriteHtml($html);
	$output='report'.date('Y_m_d_H').'_.pdf';
	$pdf->Output("$output",'I');
	// $pdf->Output('uploads/pdf/'.$output,'F');
exit();
}

public function starter_trip_ajax()
{
        $this->load->model('Model_trip');
        $results=$this->Model_trip->view_trip_starter("0");
		$data1 = array();
        foreach($results  as $r) 
        {

      $d1=$r->trip_startdate;
      $dt1=date("d-m-Y", strtotime($d1));
      $d2=$r->trip_enddate;
      $dt2=date("d-m-Y", strtotime($d2));
      
      
      $date1=date_create($r->trip_startdate);
      $date2=date_create($r->trip_enddate);
      $diff=date_diff($date1,$date2);

			
      $diff1=$diff->format("%a");
      $km=$r->trip_endkm - $r->trip_startkm ;
		if($r->trip_status==1)
		{
			$st="<font color='green'>Running</font>";
		}
		
				array_push($data1, array(
                            "tid"=>"$r->trip_id",
                            "customer"=>"$r->customer_name",
							"driver"=>"$r->trip_drivername",
							//"dmobile"=>"$r->driver_mobile",
                            "date"=>"$dt1"."<font color='red'> =></font> "."$dt2" ,
							"fromto"=>"$r->trip_from"."<font color='red'> =></font> "."$r->trip_to" ,
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

public function upcoming_trip_ajax()
{
        $this->load->model('Model_trip');
		$results=$this->Model_trip->view_trip_starter("1");
		
        $data1 = array();
        foreach($results  as $r) 
        {

      $d1=$r->trip_startdate;
      $dt1=date("d-m-Y", strtotime($d1));
      $d2=$r->trip_enddate;
      $dt2=date("d-m-Y", strtotime($d2));
      
      
      $date1=date_create($r->trip_startdate);
      $date2=date_create($r->trip_enddate);
      $diff=date_diff($date1,$date2);

			
      $diff1=$diff->format("%a");
      $km=$r->trip_endkm - $r->trip_startkm ;
		if($r->trip_status==1)
		{
			$st="<font color='green'>Coming</font>";
		}
		
				array_push($data1, array(
                            "tid"=>"$r->trip_id",
                            "customer"=>"$r->customer_name",
							"driver"=>"$r->trip_drivername",
							//"dmobile"=>"$r->driver_mobile",
                            "date"=>"$dt1"."<font color='red'> =></font> "."$dt2" ,
							"fromto"=>"$r->trip_from"."<font color='red'> =></font> "."$r->trip_to" ,
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

public function share_trip_details()
{
   $data=$this->session->all_userdata();
        $id=$this->input->post('id');
  echo " <style> .tx{text-align:right;} .fnt{text-align:right;font-size:18px;}</style>            
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Share</font></h4>
    </div>
    <br>
  
<form  class='form-horizontal' role='form' method='post' action='". site_url('Pdf/share_trip'). "' enctype='multipart/form-data'>

  <div class='row'>
    <div class='col-md-12'>
   
         <div class='form-group'>
          <label class='col-md-2 control-label' style='padding-top:10px;'>Email to </label>
          <div class='col-md-8'>
            <input type='hidden' name='id'  value='$id'/>
          <input type='text' class='form-control' name='email' value='' placeholder='Enter email'> 
          </div>
          </div>
    </div>
    </div> 
    <div class='modal-footer'>
      <input type='submit' class='btn btn-primary' value='Share'/>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
     </div>     
  
</form>   
    ";
}

public function trip_details()
  {
        $data=$this->session->all_userdata();
        $id=$this->input->post('tid');
		
        //$query = $this->db->select('*')->from('trips')->where('table_customer.customer_id',$id)->get();
        //$row = $query->row();
 
        $this->load->model('Model_trip');
        $results=$this->Model_trip->view_trip_details($id);
    
   foreach($results  as $row) 
    {
      $d1=$row->trip_startdate;
      $dt1=date("d-m-Y", strtotime($d1));
      $d2=$row->trip_enddate;
      $dt2=date("d-m-Y", strtotime($d2));
    echo "   
	<div class='modal-header'>
		<button type='button' class='close' data-dismiss='modal'>&times;</button>
			 <h4 class='modal-title' > <font color='blue'>Trip Details</font></h4>
		</div>  
    
        <form  class='form-horizontal' role='form' method='post' action='". site_url('customer/add_new_customer'). "' enctype='multipart/form-data'>
                    <!-- text input -->
		<div class='row' style='margin-top:10px;'>

         <div class='col-md-6'>  
                     <div class='form-group'>
          <label class='col-md-4 control-label' style='padding-top:5px;'>Customer Name </label>
          <div class='col-md-8'>
          <input type='text' class='form-control' style='background-color:#f9f8f8;' name='name' value='".$row->customer_name."' readonly> 
          </div>
          </div>
		  
		  <div class='form-group'>
          <label class='col-md-4 control-label' style='padding-top:5px;'>Contact Number </label>
          <div class='col-md-8'>
          <input type='text' class='form-control' style='background-color:#f9f8f8;' name='name' value='".$row->customer_mobile.", ".$row->customer_landline."' readonly> 
          </div>
          </div>
   
          <div class='form-group'>
          <label class='col-md-4 control-label' style='padding-top:5px;'>Purpose</label>
          <div class='col-md-8'>  
          
          <input type='text' name='purpose' style='background-color:#f9f8f8;' class='form-control' value='".$row->trip_purpose."' readonly/>
          </div>                   
          </div>
 		  
	 <div class='row'  style='padding-left:20px;'>
		 <div class='col-md-6'>Driver Name</div>
		 <div class='col-md-6'>Vehicle Reg. No </div>
	  </div>
		  
	 <div class='row' style='padding-left:20px;'>
	 <div class='col-md-6'>
		<input type='text' class='form-control' style='background-color:#f9f8f8;'name='driver' value='".$row->driver_name."' readonly>
	  </div>
	  <div class='col-md-6'>	
	     <input type='text' class='form-control' style='background-color:#f9f8f8;' name='vehicle' value='".$row->vehicle_regno."' readonly>
	  </div>
	</div>  
		  
	 <div class='row'  style='padding:10px 0px 0px 20px;'>
		 <div class='col-md-6'>From</div>
		 <div class='col-md-6'>To</div>
	  </div>
		  
	 <div class='row' style='padding-left:20px;'>
	 <div class='col-md-6'>
		<input type='text' class='form-control'  style='background-color:#f9f8f8;' name='from' value='".$row->trip_from."' readonly>
	  </div>
	  <div class='col-md-6'>	
	     <input type='text' class='form-control' style='background-color:#f9f8f8;' name='to' value='".$row->trip_to."' readonly>
	  </div>
	</div>  	  

	<div class='row'  style='padding:10px 0px 0px 20px;'>
		 <div class='col-md-6'>Date Start</div>
		 <div class='col-md-6'>Date end</div>
	  </div>
		  
	 <div class='row' style='padding-left:20px;'>
	 <div class='col-md-6'>
		<input type='text' class='form-control'  style='background-color:#f9f8f8;' name='dstart' value='".$dt1."' readonly>
	  </div>
	  <div class='col-md-6'>	
	     <input type='text' class='form-control' style='background-color:#f9f8f8;' name='dend' value='".$dt2."' readonly>
	  </div>
	</div>  	  
    </div>     
			  
	<!-- second column --------------------------------------- -->
     <div class='col-md-6'> 
     <div class='row'>
	 
	  <label style='padding:5px 0px 0px 20px; color:#5a99e6;'> Kilometers : </label>
	 <div class='row'>
	 <div class='col-md-3 col-md-offset-1'>Start KM</div>
			<div class='col-md-3'>End KM</div>
			<div class='col-md-4'>Trip KM </div>
	 </div>
	 	
	 <div class='row'>
	      <div class='col-md-3 col-md-offset-1'> <input type='text' class='form-control' style='background-color:#f9f8f8;' name='kmstart' value='".$row->trip_startkm."' readonly></div>
	      <div class='col-md-3'> <input type='text' class='form-control' name='kmstart' style='background-color:#f9f8f8;' value='".$row->trip_endkm."' readonly></div>
	      <div class='col-md-4'><input type='text' class='form-control tx' name='kmend'  style='background-color:#f9f8f8;' value='".($row->trip_endkm-$row->trip_startkm)."' readonly> </div>
	 </div>

	 <label style='padding:5px 0px 0px 20px;color:#5a99e6;'>Minimum Charges : </label>
	 <div class='row'>
	 <div class='col-md-3 col-md-offset-1'>Days</div>
			<div class='col-md-3'>Min. Charge</div>
			<div class='col-md-4'>Total </div>
	 </div>
	 
 		 
	 <div class='row'>
	      <div class='col-md-3 col-md-offset-1'> <input type='text' class='form-control' style='background-color:#f9f8f8;' name='kmstart' value='".$row->trip_mindays."' readonly></div>
	      <div class='col-md-3'> <input type='text' class='form-control' name='kmstart'  style='background-color:#f9f8f8;' value='".$row->trip_mincharge."' readonly></div>
	      <div class='col-md-4'><input type='text' class='form-control tx' name='kmend'  style='background-color:#f9f8f8;' value='".($row->trip_mindays*$row->trip_mincharge)."' readonly> </div>
	 </div>

   <label style='padding:5px 0px 0px 20px; color:#5a99e6;'>Additional Charges : </label>
     <div class='row'>
	 <div class='col-md-3 col-md-offset-1'>Additional KM</div>
			<div class='col-md-3'>KM Charge</div>
			<div class='col-md-4'>Total </div>
	 </div>
	  		 
	 <div class='row'>
	      <div class='col-md-3 col-md-offset-1'> <input type='text' class='form-control' style='background-color:#f9f8f8;'  name='kmstart' value='".$row->trip_addikm."' readonly></div>
	      <div class='col-md-3'> <input type='text' class='form-control' style='background-color:#f9f8f8;' name='kmstart' value='".$row->trip_addikmcharge."' readonly></div>
	      <div class='col-md-4'><input type='text' class='form-control tx' style='background-color:#f9f8f8;' name='kmend' value='".($row->trip_addikm*-$row->trip_addikmcharge)."' readonly> </div>
	 </div>

</div>

<div class='row'> 
<label class='col-md-12 control-label' style='padding-top:10px;'><hr style='margin:0px;'> </label>  
<br>

   <div class='form-group'>
          <label class='col-md-4 control-label col-sm-offset-3' style='padding-top:5px;color:blue;'>Trip Charge</label>
          <div class='col-md-4'>
          <input type='text' class='form-control tx' style='font-weight:bold;color:green;' name='toll' value='".($row->trip_charge-($row->trip_tollparking+$row->trip_interstate+$row->trip_othercharge))."' readonly>
          </div>
          </div>

 </div>   
 </div>
</div>
<!-- row end ---- -->

<label style='width:100%;height:1px;background-color:#e4e4e4;'></label>	

<div class='row' style='padding-left:10px;color:#5a99e6;'>
<label class='col-md-3' >Toll/Parking </label>
<label class='col-md-3' >Inter State Permit: </label>
<label class='col-md-2' >Other Charges: </label>
<label class='col-md-4' >Other Charges Description </label>
</div>

<div class='row' style='padding-left:10px;'>
<div class='col-md-3' >
      <input type='text' class='form-control' style='background-color:#f9f8f8;' name='toll' value='".$row->trip_tollparking."' readonly>
 </div>
 <div class='col-md-3'>
     <input type='text' class='form-control ' style='background-color:#f9f8f8;' name='inter' value='".$row->trip_interstate."' readonly>
  </div>
 <div class='col-md-2'>
     <input type='text' class='form-control ' style='background-color:#f9f8f8;' name='inter' value='".$row->trip_othercharge."' readonly>
  </div>
   <div class='col-md-4'>
     <textarea rows='2' cols='30' class='form-control' style='background-color:#f9f8f8;' name='otherdesc' readonly>".$row->trip_otherdesc."</textarea>
  </div>
</div>  

<div class='row' style='padding-top:10px;'>
<label class='col-md-2' >Additional Info.</label>
<div class='col-md-6'>
     <textarea rows='2' cols='30' class='form-control' style='background-color:#f9f8f8;' name='otherdesc' readonly>".$row->trip_addiinfo."</textarea>
  </div>
   <label class='col-md-2' style='padding-top:5px;color:blue;' >Grand Total.</label>
  <div class='col-md-2'>
      <input type='text' class='form-control fnt' style='font-weight:bold;color:green;'  name='gtotal' value='".$row->trip_gtotal."' readonly>
   </div>
  </div>  
 
</div>   

<label style='width:100%;height:1px;background-color:#e4e4e4;'></label>	
</div>
           </form> 
           <div class='row'>
                <button type='button' class='btn btn-primary col-md-offset-10' data-dismiss='modal'>Close</button>
           </div> ";
   }
}


public function edit_trip() 
	{
       $this->load->Model('Model_vehicle');
       $this->load->Model('Model_driver');
       $id = $this->uri->segment(3);
	   
	   $qry=$this->db->select('trip_driver_id')->from('trip_management')->where('trip_id',$id)->get()->row();
	   
       $this->db->select('*')->from('trip_management');
	   $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
	   
	   //$this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=trip_management.trip_vehicle_id','inner');
	   
	   if($qry->trip_driver_id!=0 || $qry->trip_driver_id!="")
	   {
	   //$this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
	   }
	   $query = $this->db->where('trip_id',$id)->get();
	   
       $data['result'] = $query->result();
       $data['results'] = $this->Model_vehicle->view_vehicle();
       $data['results2'] = $this->Model_driver->view_driver();
       //$this->load->view('admin/edit_trip_new',$data);
	   $this->load->view('admin/edit_trip_details',$data);	 
    }

 public function trip_edit()  //completed trip editing
	{
       $id = $this->uri->segment(3);
	   
		$query=$this->db->select('*')->from('trip_management')->where('trip_id',$id)->get();
		$data['result'] = $query->result();
	    $this->load->view('admin/edit_completed_trip',$data);	 
    }

	
 public function completetrip() 
	{
       $this->load->Model('Model_vehicle');
       $this->load->Model('Model_driver');
	   $this->load->Model('Model_trip');
       $id = $this->uri->segment(3);
	   
       $this->db->select('*')->from('trip_management');
	   $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
	   $query = $this->db->where('trip_id',$id)->get();
       $data['result'] = $query->result();
	   
       $data['results'] = $this->Model_vehicle->view_vehicle();
       $data['results2'] = $this->Model_driver->view_driver();
	   $data['gst'] = $this->Model_trip->get_gst();
	   $this->load->view('admin/edit_trip_new',$data);
    }	

	
public function update_trip()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('clienttype', 'clienttype', 'required');

$tid=$this->input->post('tripid');

 if($this->form_validation->run())
    {
    $custid=$this->input->post('custid');
	$clienttype=$this->input->post('clienttype');
	
	if($clienttype=='1')
	{
		$clientname='Nil';
		$clientpurpose='Nil';
		$customermobile=$this->input->post('cmobile');
		$agentid='0';
	}
	
	if($clienttype=='2')
	{
		$clientname=$this->input->post('clientna');
		$clientpurpose=$this->input->post('purposetrip');
		$custid=$this->input->post('custid');
		$customermobile=$this->input->post('cmobile');
		$agentid='0';
	}
	
	if($clienttype=='3')
	{
		$clientname='Nil';
		$clientpurpose='Nil';
		$custid=$this->input->post('custid');
		$customermobile=$this->input->post('cmobile');
		$agentid=$this->input->post('agentid');
	}
		
	$purpose=$this->input->post('nojourney');
    $vehicle=$this->input->post('vehicleid');
	$regno=$this->input->post('vehicle');
    $driver=$this->input->post('driverid');
	$drname=$this->input->post('driver');
	
    $from=$this->input->post('tripfrom');
    $to=$this->input->post('tripto');
	$vplace=$this->input->post('vplace');
    $startdate=$this->input->post('startdate');
    $enddate=$this->input->post('enddate');
    $starttime=$this->input->post('starttime');
    $endtime=$this->input->post('endtime');
    $startkm=$this->input->post('startkm');
    $endkm=$this->input->post('endkm');
	
	$refno=$this->input->post('refno');
    $roomno=$this->input->post('roomno');
	
	
	//$cbehavior=$this->input->post('cbehavior');

	$mdays=$this->input->post('mindays');
    $mcharge=$this->input->post('mincharge');
    $mtotal=$this->input->post('mintotal');
    
	$adkm=$this->input->post('addikm');
    $adcharge=$this->input->post('addikmcharge');
    $adtot=$this->input->post('additotal');
		
    $tcharge=$this->input->post('tripcost');
	
    $tollparking=$this->input->post('tollparking');
    $ispermit=$this->input->post('ispermit');
	$paymode=$this->input->post('paymode1');
	
	
	
	
	//$ocharge=$this->input->post('ocharge');
   // $otherdesc=$this->input->post('otherdesc');
   
    $ocharge=0;
    $otherdesc="";

    $adiinfo=$this->input->post('addiinfo');
	$minchargekm=$this->input->post('minckm');
	$tripop=$this->input->post('sltrip');
		
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
	
				
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
   
       $userdt = array(
                'trip_cust_id' => $custid,
				'trip_clienttype'=>$clienttype,
				'trip_clientname'=>$clientname,
				'trip_clientpurpose'=>$clientpurpose,
				'trip_clientcustomer'=>$custid,
				'trip_clientmobile'=>$customermobile,
				
                'trip_purpose' => $purpose,
				'trip_vehicle_id' =>$vehicle,
				'trip_vehicle_regno'=>$regno,
                'trip_from' =>$from,
                'trip_driver_id' =>$driver,
				'trip_drivername'=>$drname,
                'trip_to' =>$to,
				'trip_visitingplace'=>$vplace,
                'trip_startdate' => $startdate,
                'trip_enddate' => $enddate,
				'trip_starttime' =>$starttime,
                'trip_endtime' =>$endtime,
				'trip_paymentmode'=>$paymode,
                'trip_startkm' =>$startkm,
                'trip_endkm' =>$endkm,
				'trip_refno' =>$refno,
                'trip_roomno' =>$roomno,
											
				'trip_mindays' => $mdays,
                'trip_mincharge' => $mcharge,
				'trip_mintotal' =>$mtotal,
				
                'trip_addikm' =>$adkm,
                'trip_addikmcharge' =>$adcharge,
                'trip_additotal' =>$adtot,
				
				'trip_othercharge' =>$ocharge,
                'trip_otherdesc' =>$otherdesc,
                
                'trip_charge' => $tcharge,
                'trip_tollparking' => $tollparking,
				'trip_interstate' =>$ispermit,
                'trip_gtotal' =>$tcharge,
                'trip_addiinfo' =>$adiinfo,
				'trip_minchargekm'=>$minchargekm,
				'trip_agentid'=>$agentid,
				'trip_option'=>$tripop,
				'trip_status'=>"1",
               );
			   
        $this->load->Model('Model_trip');
        $this->Model_trip->update_trip($tid,$userdt);
		
		//var_dump($tripop);
				
	// vehicle available status update -------------
		$vudt = array(
                'vehicle_available' =>'1',
		);
		$this->Model_trip->vehicle_update($vehicle,$vudt);
		
	// driver trip date update-----------------

		/*$drudt = array(
                'driver_startdate' =>$startdate,
				'driver_enddate' =>$enddate,
		);*/
		
		$drudt = array(
                'driver_available' =>"1",
		);
				
		$this->Model_trip->driver_update($driver,$drudt);
		
//update srevice KM ------------------

	$vs1=$this->db->select('count(*) as cn1')->from('services')->where('ser_vehicle_id=',$vehicle)->get()->row();
	
	//echo $vs1->cn1;
	
	if($vs1->cn1>0)
	{
		$vser1 = array(
                'ser_kilometer2' =>$endkm,
			);
		$this->Model_trip->update_service_km($vehicle,$vser1);
	}
	else
	{
		$vser1 = array(
                'ser_vehicle_id' =>$vehicle,
				'ser_kilometer1' =>$startkm,
				'ser_kilometer2' =>$endkm,
				'ser_status' =>'1',
			);
		$this->Model_trip->insert_service_km($vser1);
	}
	//-------------------------
	
	 // $this->session->set_flashdata('message', $userdt);
     	   
	   
     $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$tid,
            'log_desc'=>'Trip Details Added'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);

		  $this->session->set_flashdata('message', 'Updated#Trip details successfully Updated.');
        redirect('Trip/view_trip');  
    }
    else
	{
      $this->session->set_flashdata('message', 'Try Again#Details missing, try again');
      //$data['message']="Data Inserted Successfully";
      redirect('Trip/view_trip');
    }
}


public function complete_the_trip()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('custid', 'custid', 'required');
$this->form_validation->set_rules('nojourney', 'purpose', 'required');

 if($this->form_validation->run())
    {
		
    $custid=$this->input->post('custid');
	$custname=$this->input->post('custname');
	$cmobile=$this->input->post('cmobile');
	
    $purpose=$this->input->post('nojourney');
	$vehicle=$this->input->post('vehicleid');
	$driver=$this->input->post('driverid');
	
	$tripfrom=$this->input->post('tripfrom');
    $tripto=$this->input->post('tripto');
		
    $startdate=$this->input->post('startdate');
    $enddate=$this->input->post('enddate');
    $starttime=$this->input->post('starttime');
    $endtime=$this->input->post('endtime');
    $startkm=$this->input->post('startkm');
    $endkm=$this->input->post('end_km');
    $mdays=$this->input->post('mindays');
    $mcharge=$this->input->post('mincharge');
    $mtotal=$this->input->post('mintotal');
    $adkm=$this->input->post('addikm');
    $adcharge=$this->input->post('addikmcharge');
    $adtot=$this->input->post('additotal');
    $tcharge=$this->input->post('tripcost');
    $tollparking=$this->input->post('tollparking');
    $ispermit=$this->input->post('ispermit');
    $ocharge=$this->input->post('ocharge');
    $otherdesc=$this->input->post('otherdesc');
    $gtotal=$this->input->post('gtotal');
    $adiinfo=$this->input->post('addiinfo');
    $minckm=$this->input->post('minckm');
	
	$tripid=$this->input->post('tripid');
	$paymode=$this->input->post('paymode');
	
	$trop=$this->input->post('trop'); //for redirect different views
	
    $date=date("Y/m/d");
	
	$dbatha=$this->input->post('dbtha');
	$acomm=$this->input->post('agentcom');
	$disc=$this->input->post('discount');
			
    
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa');
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
 
       $userdt = array(
                'trip_cust_id' => $custid,
				'trip_clientcustomer'=>$custid,
				'trip_clientmobile'=>$cmobile,
				'trip_clientname' => $custname,
                'trip_purpose' => $purpose,
				
                'trip_from' =>$tripfrom,
                'trip_to' =>$tripto,
				
                'trip_startdate' => $startdate,
                'trip_enddate' => $enddate,
				'trip_starttime' =>$starttime,
                'trip_endtime' =>$endtime,
				'trip_paymentmode'=>$paymode,
                'trip_startkm' =>$startkm,
                'trip_endkm' =>$endkm,
       
				'trip_mindays' => $mdays,
				'trip_mincharge' => $mcharge,
				'trip_mintotal' =>$mtotal,
        
                'trip_addikm' =>$adkm,
                'trip_addikmcharge' =>$adcharge,
                'trip_additotal' =>$adtot,
        
				'trip_othercharge' =>$ocharge,
                'trip_otherdesc' =>$otherdesc,
                
                'trip_charge' => $tcharge,
                'trip_tollparking' => $tollparking,
				'trip_interstate' =>$ispermit,
				
                'trip_gtotal' =>$tcharge,
                'trip_addiinfo' =>$adiinfo,
                'trip_minchargekm' =>$minckm,
				'trip_drbatha'=>$dbatha,
				'trip_agentcom'=>$acomm,
				'trip_discount'=>$disc,
                'trip_status'=>"2"
               );

        $this->load->model('Model_trip');
        $this->Model_trip->update_trip($tripid,$userdt);
		
		//update vehicle ------
		$vudt = array(
            'vehicle_available' =>'0',
		);
				
		$this->Model_trip->vehicle_update($vehicle,$vudt);
				
		//update driver ------
		/*$dudt = array(
            'driver_startdate' =>NULL,
			'driver_enddate' =>NULL,
		);*/
		
		$dudt = array(
            'driver_available' =>"0",
		);
		
	$this->Model_trip->driver_update($driver,$dudt);
	//------------
	
	
	
	/*$dqry=$this->db->select('*')->from('driver_charges')->get()->row();
		
    $start_date  = date_create($startdate);
    $end_date    = date_create($enddate);
    $no_of_days  = date_diff($start_date,$end_date);
    $days = $no_of_days->format("%a");
    $bata = ($days * $dqry->drbatha);

    $driver_trip_list = array(
                'trip_id' => $tripid,
                'start_date' => $startdate,
                'end_date' => $enddate,
                'cust_id' => $custid,
                'trip_charge' => $tcharge,
                'driver_bata' => $bata,
                'driver_trip_charge' => ($tcharge * ($dqry->drcharge/100)),
                'driver_id' => $driver,
                'trip_to' => $tripto
	);

    $this->Model_trip->insert("driver_trip_list",$driver_trip_list);
    $insert_id = $this->db->insert_id();
    $this->load->model("Model_driver");
    $bls = $this->Model_driver->select("driver_account",array("driver_id"  => $driver , "bl_status" => '1'), "balance");
    $credit = ($tcharge * ($dqry->drcharge/100)) + $bata;
    $balance = 0;
    if($bls != null){
      $this->Model_driver->update("driver_account",array("bl_status" => '0'), array("driver_id" => $driver));  
      $balance += $bls[0]->balance;
    }
    $balance += $credit;

    $driver_acc = array(
                'driver_id' => $driver,
                'credit' => $credit,
                'date' => $enddate,
                "description" => "Trip from " . $from . " to " . $to ." (Trip id : ". $insert_id .")",
                "balance" => "0",
                "balance" => $balance,
                "bl_status" => '1'
    );

    $this->Model_trip->insert("driver_account",$driver_acc);*/
	
	

//update srevice KM ------------------

$vs1=$this->db->select('count(*) as cn1')->from('services')->where('ser_vehicle_id=',$vehicle)->get()->row();
	
	echo $vs1->cn1;
	
	if($vs1->cn1>0)
	{
			
		$vser1 = array(
                'ser_kilometer2' =>$endkm,
			);
		$this->Model_trip->update_service_km($vehicle,$vser1);
	}
	else
	{
		$vser1 = array(
                'ser_vehicle_id' =>$vehicle,
				'ser_kilometer1' =>$startkm,
				'ser_kilometer2' =>$endkm,
				'ser_status' =>'1',
			);
		$this->Model_trip->insert_service_km($vser1);
	}

	$gstper=$this->db->select('cg_gst')->from('customer_gst')->where('cg_custid',$custid)->get()->row()->cg_gst;
	if(empty($gstper))
	{
		$gstper="0";
	}
	
	$gst=($tcharge*$gstper)/100;
	
	//unpaid trips adding--------------------
	$unpaid = array(
                'up_date' =>date('Y-m-d'),
				'up_tripid' =>$tripid,
				'up_custid' =>$custid,
				'up_amount' =>$tcharge,
				'up_gstper' =>$gstper,
				'up_gst' =>$gst,
				'up_status' =>"0",
			);
	
	$this->Model_trip->insert_unpaid_trip($unpaid);
			
	
//---------------------add data to accounts
//HEAD ENTRY

 $this->load->model('Model_accounts');
 
 $accdata=array(
 "accu_date"=>date('Y-m-d'),
 "accu_description"=>$custname, 
 "accu_voucherno"=>0,
 "accu_narration"=>"ID=".$tripid.",Customer : ".$custname. ",(".$tripfrom."-".$tripto."), Dated: (". date_format(date_create($startdate),'d-m-Y')." => ".  date_format(date_create($enddate),'d-m-Y').")", 
  );
  
  $insid=$this->Model_accounts->insert_data("ac_accounts",$accdata);

 //ENTRY 1
 $accdata2=array(
 "entry_date"=>date('Y-m-d'),
 "entry_accuid"=>$insid,
 "entry_ledgerid"=>1, 					//FOR TRIP SALES
 "entry_debit"=>0, 
 "entry_credit"=>$tcharge,
 );
 $this->Model_accounts->insert("ac_account_entry",$accdata2);
 
 //ENTRY 2
 $accdata3=array(
 "entry_date"=>date('Y-m-d'),
 "entry_accuid"=>$insid,
 "entry_ledgerid"=>2,  					//for CUSTOMERS
 "entry_debit"=>$tcharge, 
 "entry_credit"=>0,
 );
 
 $this->Model_accounts->insert("ac_account_entry",$accdata3);
    
 $acccash=array(
 "cashbook_date"=>date('Y-m-d'),
 "cashbook_headid"=>$insid,
 "cashbook_debit"=>$tcharge,
 "cashbook_credit"=>0, 
 "cashbook_narration"=>"Trip charges(Trip id=".$tripid.")", 
 );
 $this->Model_accounts->insert("ac_cashbook",$acccash);
 
 //-----------------------------------
 $this->session->set_flashdata('message',  'Trip Successfully Closed.!');

			$userdt1 = array(
				'log_staffid' => $staffid,
				'log_staffname'=>$staffname,
				'log_time'=>$time,
				'log_date'=>$date,
				'log_entryid'=>$tripid,
				'log_desc'=>'Trip Completed'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
		  if($trop=='0'){
				redirect('Trip/view_trip');
		  }
		  else
		  {
			 redirect('Trip/completed_trip'); 
		  }

       }
        else 
        {
		   $this->session->set_flashdata('message',  'Some data missing, try again.!');
            if($trop=='0'){
				redirect('Trip/view_trip');
		  }
		  else
		  {
			 redirect('Trip/completed_trip'); 
		  }
        }                   
}

  public function del_entry()
  {
			//$id=$this->uri->segment(3, 0);
			$id=$this->input->post('tid');
			$date=date("Y/m/d");
			date_default_timezone_set('Asia/Kolkata');
			$time=date('h:i:sa');
			$staffid=$this->session->userdata('userid');
			$staffname=$this->session->userdata('name');
			$this->load->model('Model_trip');
            $result=$this->Model_trip->delete_trip($id);
            if($result)
            {
             $this->session->set_flashdata('message', 'Trip details has been deleted..!');
               $userdt1 = array(
				'log_staffid' => $staffid,
				'log_staffname'=>$staffname,
				'log_time'=>$time,
				'log_date'=>$date,
				'log_entryid'=>$id,
				'log_desc'=>'Trip Details Deleted'
            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
                   redirect('Trip/view_trip');
            }
           else
           {
            $this->session->set_flashdata('message', 'Please try again.');
            redirect('Trip/view_trip');
           }
  }
 
public function save_trip()
{
$this->load->model('Model_trip');
$this->load->library('pdf1');
     
  $pdf=$this->pdf1->load();
   $id=$this->input->post('tid');
   $this->load->model('Model_trip');
   $data['results1']=$this->Model_trip->view_trip_details($id);
   ini_set('memory_limit','256M');
   $html=  $this->load->view('pdf/trip_pdf',$data,true);
	$pdf->WriteHtml($html);
	$output='Trip_'.$id.'_'.date('Y_m_d').'_.pdf';
	// $pdf->Output("$output",'D');
	$pdf->Output('upload/savedpdf/'.$output,'F');
	$this->session->set_flashdata('message',  'Trip Details Saved Successfully..!');
  redirect('Trip/view_trip');
 exit();
    
}


public function save_utrip()
{

$this->load->model('Model_pdf');
$this->load->library('pdf1');
     
  $pdf=$this->pdf1->load();
   //$id=$this->input->post('tid');
   $this->load->model('Model_pdf');
  $data['results']=$this->Model_pdf->UPCtrip();
   ini_set('memory_limit','256M');
   $html=  $this->load->view('pdf/UPCtrip_pdf',$data,true);
  $pdf->WriteHtml($html);
  $output='Trip_'.date('Y_m_d').'_.pdf';
  // $pdf->Output("$output",'D');
  $pdf->Output('upload/savedpdf/'.$output,'F');
  $this->session->set_flashdata('message','Trip Details Saved Successfully..!');
  redirect('Trip/upcoming_trip');
 exit();
    
}

   
public function add_new_customer()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('name', 'name', 'required');
$this->form_validation->set_rules('email', 'email', 'required');


 if($this->form_validation->run())
    {
    $name=$this->input->post('name');
    $email=$this->input->post('email');
    $mobile=$this->input->post('mobile');
    $address=$this->input->post('address');
    $landline=$this->input->post('landline');
    $status=$this->input->post('status');
   
       $userdt = array(
                'customer_name' => $name,
                'customer_email' => $email,
				'customer_mobile' =>$mobile,
                'customer_landline' =>$landline,
                'customer_address' =>$address,
				'customer_status'=>"1"
               );
         $this->load->Model('Model_customer');
        $this->Model_customer->form_insert($userdt);
        $this->session->set_flashdata('message', 'New Customer added successfully added..!');
        redirect('Trip/index');  
  }
 else{
         $this->session->set_flashdata('message', 'Data Not Inserted Successfully');
        //$data['message']="Data Inserted Successfully";

      redirect('Trip/index');
       }  
}

public function driver_ajax()
{
	/*$sdt=$this->uri->segment(3);*/
	/*if($sdt=='0')
	{*/

       $this->load->model('Model_trip');
       $results=$this->Model_trip->view_driver();
	/*}
	else
	{
		// ajax driver filter -----------------------
	    $sdt=$this->uri->segment(3);
		$edt=$this->uri->segment(4);
	
		$this->load->model('Model_trip');
		$results=$this->Model_trip->view_driver1($sdt,$edt);
	   //-----------------------------------------
	}	*/
       $data1 = array();
       foreach($results  as $r) 
        {
       // $select="<a href='#myModald' id='$r->driver_id' data-toggle='modal' class='drselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' ><span class='glyphicon glyphicon-list-alt'></span></button></center></a>";
	      $select="<a href='#myModald' id='$r->driver_id' data-toggle='modal' class='drselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' >Select</button></center></a>";
                      array_push($data1, array(
							"select"=>"$select",
							"drid"=>"$r->driver_id",
                            "name"=>"$r->driver_name",
                            "mobile"=>"$r->driver_mobile",
							"status"=>"<font color=green>Available</font>",
                            //"status"=>$st
          ));
        }
        echo json_encode(array('data' => $data1));
        //$this->load->view('admin/view_business_category',$data1);
}

public function get_driverDT()
{
	$id=$this->input->post('did');
	$this->load->model('Model_trip');
    $result=$this->Model_trip->view_driverDT($id);
	foreach($result as $r)
	{
	     $data1=array(
					"name"=>$r->driver_name,
                    "mobile"=>$r->driver_mobile,
					"driverid"=>$r->driver_id,
          );
	}
         //echo json_encode(array('data' => $data1));
		 echo json_encode($data1);
}


public function get_cust_mobile()
{
	$id=$this->input->post('cid');
	
	$row=$this->db->select('customer_mobile')->from("customers")->where('customer_id',$id)->get()->row();
         $data1=array(
					"mobile"=>$row->customer_mobile,
         );
         //echo json_encode(array('data' => $data1));
		 echo json_encode($data1);
}

public function get_cust_name_mobile()
{
	$id=$this->input->post('cid');
	
	$row=$this->db->select('customer_name,customer_mobile')->from("customers")->where('customer_id',$id)->get()->row();
         $data1=array(
					"customer"=>$row->customer_name,
					"mobile"=>$row->customer_mobile,
         );
         //echo json_encode(array('data' => $data1));
		 echo json_encode($data1);
}


public function get_vehicleDT()
{
	$id=$this->input->post('vid');
	$this->load->model('Model_trip');
    $result=$this->Model_trip->view_vehicleDT($id);
	foreach($result as $r)
	{
	     $data1=array(
					"name"=>$r->vehicle_regno,
					"vehicleid"=>$r->vehicle_id,
          );
	}
         //echo json_encode(array('data' => $data1));
		 echo json_encode($data1);
}


public function vehicle_ajax()
{
       $this->load->model('Model_trip');
       $results=$this->Model_trip->view_vehicles();
		
       $data1 = array();
       foreach($results  as $r) 
        {
        //$select="<a href='#myModals' id='$r->vehicle_id' data-toggle='modal' class='vselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' ><span class='glyphicon glyphicon-list-alt'></span></button></center></a>";
		$select="<a href='#myModals' id='$r->vehicle_id' data-toggle='modal' class='vselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' >Select</button></center></a>";
                 array_push($data1, array(
					"select"=>"$select",
                    "regno"=>"$r->vehicle_regno",
                    "vtype"=>"$r->veh_type_name",
					"noofseats"=>"$r->vehicle_noofseats",
                    "status"=>"<font color=green>Available</font>",
          ));
        }
        echo json_encode(array('data' => $data1));
        //$this->load->view('admin/view_business_category',$data1);
}



public function atta_vehicle_ajax()
{
       $this->load->model('Model_trip');
       $results=$this->Model_trip->view_atta_vehicles();
		
       $data1 = array();
       foreach($results  as $r) 
        {
        //$select="<a href='#myModals' id='$r->vehicle_id' data-toggle='modal' class='vselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' ><span class='glyphicon glyphicon-list-alt'></span></button></center></a>";
		$select="<a href='#myModal_Atta' id='$r->attached_vehicleid' data-toggle='modal' class='atta_vselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' >Select</button></center></a>";
                 array_push($data1, array(
					"select"=>"$select",
                    "regno"=>"$r->attached_vehicleno",
					"vtype"=>"$r->attached_vehicletype",
                    "drname"=>"$r->attached_drname",
					"drmobile"=>"$r->attached_drmobile",
					"noofseats"=>"$r->attached_noofseats",
          ));
        }
        echo json_encode(array('data' => $data1));
        //$this->load->view('admin/view_business_category',$data1);
}


public function get_atta_vehicleDT()
{
	$id=$this->input->post('avid');
	$this->load->model('Model_trip');
    $result=$this->Model_trip->view_atta_vehicleDT($id);
	foreach($result as $r)
	{
	     $data1=array(
					"regno"=>$r->attached_vehicleno,
					"avehicleid"=>$r->attached_vehicleid,
					"drname"=>$r->attached_drname,
          );
	}
         //echo json_encode(array('data' => $data1));
		 echo json_encode($data1);
}



public function customer_ajax()
{
       $this->load->model('Model_trip');
       $results=$this->Model_trip->view_customers();
		
       $data1 = array();
       foreach($results  as $r) 
        {
        //$select="<a href='#myModalc' id='$r->customer_id' data-toggle='modal' class='cselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' ><span class='glyphicon glyphicon-list-alt'></span></button></center></a>";
		$select="<a href='#myModalc' id='$r->customer_id' data-toggle='modal' class='cselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' >Select</button></center></a>";
                 array_push($data1, array(
					"select"=>"$select",
                    "cname"=>"$r->customer_name",
                    "cmobile"=>"$r->customer_mobile",
                    //"status"=>$st
          ));
        }
        echo json_encode(array('data' => $data1));
        //$this->load->view('admin/view_business_category',$data1);
}

public function get_customerDT()
{
	$id=$this->input->post('cid');
	$this->load->model('Model_trip');
    $result=$this->Model_trip->view_customerDT($id);
	foreach($result as $r)
	{
	     $data1=array(
					"name"=>"$r->customer_name",
                    "mobile"=>"$r->customer_mobile",
                    "id"=>"$r->customer_id"
          );
	}
         //echo json_encode(array('data' => $data1));
		 echo json_encode($data1);
}

  public function view_areas()
  {
    //$id=$this->uri->segment(3);
    $id=$this->input->post('cid');
    $op="<option value=''>----------</option>";
    $this->load->model('Model_customer');
    $result=$this->Model_customer->view_area($id);
    foreach ($result as $r)
    {
      $op.="<option value='".$r->area_id."'>". $r->area_name."</option>";
    }
    echo $op;
  }
    
  
public function set_extra_km_reason()
{

$vid=$this->input->post('vid');
$skm=$this->input->post('skm');

$this->db->select("trip_endkm,trip_drivername,trip_vehicle_regno");
$this->db->from("trip_management");
$this->db->where("trip_vehicle_id",$vid);
$this->db->order_by('trip_id','desc');
$this->db->LIMIT(1);

$qrykm=$this->db->get()->row();

if(count($qrykm)<=0)
{
	$diff=0;
}
else
{
	$diff=$skm-$qrykm->trip_endkm;
	
	$drna=$qrykm->trip_drivername;
    $vregno=$qrykm->trip_vehicle_regno;
}

if($diff>0)
{
	$mes=$diff;
}
else
{
	$mes=$deff ;  //"Kilometer status is OK, Continue ";
}

  echo "         
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Extra KM Details </font></h4>
    </div>
    <br>
   
	<div style='padding-left:30px;'>
		  <div class='row'>
          <label class='col-md-4 control-label' style='text-align:right;'>Previous Ending Km :</label> 
		  <div class='col-md-3'>
		  <input type='text' class='control-form' name='pendkm' id='pendkm' value='&nbsp;$qrykm->trip_endkm' >
		   <input type='hidden' class='control-form' name='vid' id='vid' value='$vid' >
          </div>
		  </div>
		  
		  <div class='row' style='margin-top:5px;'>
          <label class='col-md-4 control-label' style='text-align:right;'>Current starting Km :</label> 
		  <div class='col-md-3'>
		  <input type='text' class='control-form' name='skm' id='skm' value='&nbsp;$skm' >
		   <input type='hidden' class='control-form' name='vid' id='vid' value='$vid' >
          </div>
		  </div>
		  
		  <div class='row' style='margin-top:5px;'>
          <label class='col-md-4 control-label' style='text-align:right;'>Diffrence(Km)&nbsp;&nbsp;:</label>
			<div class='col-md-3'> 
		  <input type='text' class='control-form' name='diffkm'  id='diffkm' value='&nbsp;$mes' >
		  <input type='hidden' name='startkm'  id='startkm' value='$skm' >
          </div>
		  </div>
		  		  
		  <div class='row'  style='margin-top:5px;'>
          <label class='col-md-4 control-label' style='text-align:right;'>Type Reason :</label>
          <div class='col-md-7'>
		   <textarea rows=4  name='reason'  id='reason' class='form-control' required></textarea>
		  </div>
          </div>
		  
		  
		  <div class='modal-footer'>
		  <div class='col-md-11'>
		  <input type='button' id='idsubmit' class='btn btn-primary' aria-hidden='true'data-dismiss='modal'  value='Submit'>&nbsp;&nbsp;&nbsp;
          <input type='button' class='btn btn-default' data-dismiss='modal' aria-hidden='true' value='Close'>
		  </div>
          </div>
		  
	</div>
<!--</form>   -->

<script type='text/javascript'>
$('#idsubmit').click(function()
{
	var km=$('#startkm').val();
	var vid=$('#vid').val();
	var pkm=$('#pendkm').val();
	var dif=$('#diffkm').val();
	var rson=$('#reason').val();
		alert(km);
	    jQuery.ajax({
        type: 'POST',
        url: ' ". base_url()."' + 'Trip/add_extrakm_reason',
        dataType: 'html',
        data: {skm:km,vid:vid,pendkm:pkm,reason:rson,diff:dif},
        success: function(res) {
			if(res==1)
			{
				swal('Saved','Extra KM Reason added..','success')
			}
			else
			{
				swal('Try Again','Details missing..','error')
			}
		
        }
		});
});
</script>

    ";
}

public function add_extrakm_reason()
{
	
	$vid=$this->input->post('vid');
	
	$this->db->select("trip_drivername,trip_vehicle_regno");
	$this->db->from("trip_management");
	$this->db->where("trip_vehicle_id",$vid);
	$this->db->order_by('trip_id','desc');
	$this->db->LIMIT(1);
	$qkm=$this->db->get()->row();
	
	
	$pendkm=$this->input->post('pendkm');
    $diffkm=$this->input->post('diff');
    $reason=$this->input->post('reason');
	$skm=$this->input->post('skm');
	
      $ndt = array(
                'exkm_vregno' =>$qkm->trip_vehicle_regno,
                'exkm_drname' =>$qkm->trip_drivername,
				'exkm_prekm' =>$pendkm,
                'exkm_startkm' =>$skm,
                'exkm_extrakm' =>$diffkm,
				'exkm_reason'=>$reason,
				'exkm_date'=>date('Y-m-d'),
               );
			   
			   
	//var_dump($ndt);
	
	if($this->db->insert('extrakm',$ndt))
	{
		echo  "1";
	}
	else
	{
		echo "0";
	}
	
}

  
public function check_kilometer()
{
$vid=$this->input->post('vid');
$skm=$this->input->post('skm');

$this->db->select("trip_endkm");
$this->db->from("trip_management");
$this->db->where("trip_vehicle_id",$vid);
$this->db->order_by('trip_id','desc');
$this->db->LIMIT(1);
$qrykm=$this->db->get()->row();

if(count($qrykm)<=0)
{
 $diff=0;
}
else
{
 $diff=$skm-$qrykm->trip_endkm;
}

if($diff>0)
{
	$mes="Previous Ending KM - <font color='green'><b>".$qrykm->trip_endkm ."</font><br> Extra running KM - <font color='red'><b>". $diff . "</b></font>";
}
else
{
	$mes="0";
}
echo $mes;
} 
 

  
//Approval Running Trips
public function view_ptrip()
{
   $this->load->view('admin/view_ptrip');
}  

public function trip_pajax()
{
 $this->load->model('Model_trip');
 $results=$this->Model_trip->view_trip();
  $data1 = array();
        foreach($results  as $r) 
        {
        $edit="<a href='".base_url()."Trip/edit_trip/".$r->trip_id."' id='$r->trip_id' data-toggle='modal' class='edit open-AddBookDialog2'>
      <center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
    $view="<a href='#myModal1' id='$r->trip_id' data-toggle='modal' class='view open-AddBookDialog2'>
    <center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-list-alt'></span></button></center></a>";
    $print="<a href='pdf_trip/$r->trip_id'  class='print open-AddBookDialog2'>
    <center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-print'></span></button></center></a>";
    $share="<a href='#myModals' id='$r->trip_id' data-toggle='modal' class='share open-AddBookDialog2'><center><button class='btn btn-success btn-xs' data-title='Share' data-toggle='modal' ><span class='glyphicon glyphicon-share'></span></button></center></a>";
    $app=anchor('Trip/approve_trip/'.$r->trip_id, 'Approve', array('id' =>'approve1','class' =>'btn btn-primary btn-xs')); 
    //$views="<a href='".base_url()."Trip/views_trip/".$r->trip_id."' id='$r->trip_id'  class='btn btn-success btn-xs'>";
	
	
      $del=anchor('Trip/del_entry/'.$r->trip_id, 'Delete', array('id' =>'del_conf'));
      $views=anchor('Trip/views_trip/'.$r->trip_id, 'View', array('class' =>'btn btn-primary btn-xs'));
	  
	  
      $d1=$r->trip_startdate;
      $dt1=date("d-m-Y", strtotime($d1));
      $d2=$r->trip_enddate;
      $dt2=date("d-m-Y", strtotime($d2));
      
      
      $date1=date_create($r->trip_startdate);
            $date2=date_create($r->trip_enddate);
            $diff=date_diff($date1,$date2);
      //$diff1=$diff->format("%R%a days");
      $diff1=$diff->format("%a");
      $km=$r->trip_endkm - $r->trip_startkm ;
    if($r->trip_status==2)
    {
      $st="<font color='red'>Completed</font>";
    }
    else if($r->trip_status==1)
    {
      $st="<font color='green'>Running</font>";
    }
    else
  {
     $st="<font color='green'>New</font>";
    }
  
                      array_push($data1, array(
                            //"view"=>$views,
                            //"app"=>$app,
                            "tripid"=>"$r->trip_id",
                            "customer"=>"$r->customer_name",
              "custphone"=>"$r->customer_mobile",
                            // "purpose"=>"$r->trip_purpose",
                            "driver"=>"$r->driver_name",
                            "date"=>"$dt1"."<font color='red'> =></font> "."$dt2" ,
                            "days"=>$diff1,
                            "totalkm"=>$km,
                            "charge"=>"<b>$r->trip_charge</b>",
              "ocharge"=>($r->trip_tollparking + $r->trip_interstate + $r->trip_othercharge),
              "gtotal"=>"<b>$r->trip_gtotal</b>",
              
                            // "vehicle"=>"$r->trip_vehicle_id",
                            // "from"=>"$r->trip_from",
                            // "startdate"=>"$r->trip_startdate",
                            // "enddate"=>"$r->trip_enddate",
                            // "starttime"=>"$r->trip_starttime",
                            // "endtime"=>"$r->trip_endtime",
                            // "startkm"=>"$r->trip_startkm",
                            //   "endkm"=>"$r->trip_endkm",
                            // "enddate"=>"$r->trip_enddate",
                            // "charge"=>"$r->trip_charge",
                            // "tollparking"=>"$r->trip_tollparking",
                            // "interstate"=>"$r->trip_interstate",
                            //  "gtotal"=>"$r->trip_gtotal",
                            // "description"=>"$r->trip_description",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

public function approve_trip()
    {
 $id=$this->uri->segment(3, 0);
 $this->load->model('Model_trip');
 $result=$this->Model_trip->approve_trip($id);
    if($result=1)
  {
     $this->session->set_flashdata('message', 'Trip Approved');
       redirect('Trip/view_ptrip');          
     }
    else
  { 
  $this->session->set_flashdata('message', 'Please try again');
   redirect('Trip/view_ptrip');   
  }
}

public function views_trip()
{
    $id=$this->uri->segment(3);
    $this->load->model('Model_pdf');
    $data['inv_results']=$this->Model_pdf->trip($id);
//var_dump($data);
    $this->load->view('admin/views_trip',$data);
}

//Approve New Trips
public function view_ntrip()
{
   $this->load->view('admin/view_ntrip');
}

public function trip_najax()
{
 $this->load->model('Model_trip');
 $results=$this->Model_trip->view_ntrip();
  $data1 = array();
        foreach($results  as $r) 
        {
  	  
      $app="<a href='#myModal2' id='$r->trip_id' data-toggle='modal' class='edit open-AddBookDialog2'>
        <center><button class='btn btn-info btn-xs' data-title='approve' data-toggle='modal' >Approve</button></center></a>";
	  $del=anchor('Trip/del_entry/'.$r->trip_id, 'Delete', array('id' =>'del_conf'));
	  
	  $d1=$r->trip_startdate;
      $dt1=date("d-m-Y", strtotime($d1));
      $d2=$r->trip_enddate;
      $dt2=date("d-m-Y", strtotime($d2));
            
      $date1=date_create($r->trip_startdate);
      $date2=date_create($r->trip_enddate);
      $diff=date_diff($date1,$date2);
      //$diff1=$diff->format("%R%a days");
      $diff1=$diff->format("%a");
	  
      $km=$r->trip_endkm - $r->trip_startkm ;
		if($r->trip_status==2)
		{
		  $st="<font color='red'>Completed</font>";
		}
		else if($r->trip_status==1)
		{
		  $st="<font color='green'>Running</font>";
		}
    else if($r->trip_status==3)
    {
      $st="<font color='red'>Closed</font>";
    }
		else
		{
		 $st="<font color='green'>New</font>";
		}
                      array_push($data1, array(
                            "app"=>$app,
                            "tripid"=>"$r->trip_id",
                            "customer"=>"$r->customer_name<br><b>Ph: </b>$r->customer_mobile",
							"custphone"=>"$r->customer_mobile",
                            // "purpose"=>"$r->trip_purpose",
                            "driver"=>"$r->trip_drivername",
                            "date"=>"$dt1"."<font color='red'> =></font> "."$dt2" ,
                            "days"=>$diff1,
                            "totalkm"=>$km,
                            "charge"=>"<b>$r->trip_charge</b>",
							"ocharge"=>($r->trip_tollparking + $r->trip_interstate + $r->trip_othercharge),
							"gtotal"=>"<b>$r->trip_gtotal</b>",
              
                            // "vehicle"=>"$r->trip_vehicle_id",
                            // "from"=>"$r->trip_from",
                            // "startdate"=>"$r->trip_startdate",
                            // "enddate"=>"$r->trip_enddate",
                            // "starttime"=>"$r->trip_starttime",
                            // "endtime"=>"$r->trip_endtime",
                            // "startkm"=>"$r->trip_startkm",
                            //   "endkm"=>"$r->trip_endkm",
                            // "enddate"=>"$r->trip_enddate",
                            // "charge"=>"$r->trip_charge",
                            // "tollparking"=>"$r->trip_tollparking",
                            // "interstate"=>"$r->trip_interstate",
                            //  "gtotal"=>"$r->trip_gtotal",
                            // "description"=>"$r->trip_description",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}


public function approve_ntrip() 
  {
	$data=$this->session->all_userdata();
	$id=$this->input->post('trip_id');
	$query = $this->db->select('*')->from('trip_management')->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner')->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner')->join('vehicle_registration', 'vehicle_registration.vehicle_id=trip_management.trip_vehicle_id','inner')->where('trip_id',$id)->get();
    $row = $query->row();
     
	 
	  $d1=$row->trip_startdate;
      $dt1=date("d-m-Y", strtotime($d1));
      $d2=$row->trip_enddate;
      $dt2=date("d-m-Y", strtotime($d2));
            
     
     echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Change Status</font></h4>
    </div>
    <br>

    <form  class='form-horizontal' role='form' method='post' action='". site_url('Trip/update_new'). "' enctype='multipart/form-data'>
                    <!-- text input -->
          <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:3px;'>Trip ID: </label>
                    <div class='col-md-8'>  
          <input type='text' name='id' class='form-control'  id='mask_date2' value='$row->trip_id' readonly/>
          </div>                   
          </div>

          <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:3px;'>Name: </label>
                    <div class='col-md-8'>  
          <input type='text' name='ss' class='form-control'  id='mask_date2' value='$row->customer_name' readonly/>
          </div>                   
          </div>

           <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:3px;'>Mobile No: </label>
                    <div class='col-md-8'>  
          <input type='text' name='gg' class='form-control'  value='$row->customer_mobile' readonly/>
          </div>                   
          </div>
                           <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:3px;'>Driver Name: </label>
                    <div class='col-md-8'>  
          <input type='text' name='gg' class='form-control'  id='mask_date2' value='$row->driver_name' readonly/>
          </div>                   
          </div>
          <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:3px;'>Purpose: </label>
                     <div class='col-md-8'>  
          <textarea rows='3' class='form-control' name='addiinfo' readonly>$row->trip_purpose</textarea>
          </div>
          </div>

          <div class='form-group'>
                    <label class='col-md-3 control-label' style='padding-top:3px;'>Vehicle: </label>
                    <div class='col-md-8'>  
          <input type='text' name='gg' class='form-control'  id='mask_date2' value='$row->vehicle_regno' readonly/>
          </div>                   
          </div>
		  
	                <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:3px;'>Start(Date-End): </label>
                    <div class='col-md-8'> 
					
          <input type='text' name='gg' class='form-control'  id='mask_date2' value='$dt1 => $dt2' readonly/>
          </div>                   
          </div>
           
                    
            <label style='background-color:#cecece ;width:100%;height:1px;''></label>
                <div class='form-group'> 
                <div class='row'>
                <div class='col-md-5'>
                </div>
                <div class='col-md-6'>
                <input type='submit' name='appsubmit' class='btn btn-primary' value='Approve'/>
				<input type='submit' name='appsubmit' class='btn btn-primary'  style='margin-left:10px;' value='Reject'/>
				
                   <button type='button' class='btn btn-default'  style='margin-left:10px;' data-dismiss='modal'>Cancel</button>
                </div>
                </div>                                
           </form>  ";
  }

public function update_new()
    {
    $id=$this->input->post('id');
    $op=$this->input->post('appsubmit');

    $this->load->model('Model_trip');
	if($op='Approve')
	{		
		$result=$this->Model_trip->approve_ntrip($id);
		if($result=1)
		{
		 $this->session->set_flashdata('message', 'Trip Approved');
		   redirect('Trip/view_ntrip');          
		}
    }
	else
	{
		$result=$this->Model_trip->approve_rejecttrip($id);
		if($result=1)
		{
		 $this->session->set_flashdata('message', 'Trip Rejected');
		   redirect('Trip/view_ntrip');          
		}
	}
}
  
 public function upcoming_trip()
 {
	 $this->load->view('admin/view_upcomingtrip');
 }
 
 
 public function share_utrip_details()

{
   $data=$this->session->all_userdata();
       // $id=$this->input->post('id');
  echo " <style> .tx{text-align:right;} .fnt{text-align:right;font-size:18px;}</style>            
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Share</font></h4>
    </div>
    <br>
  
<form  class='form-horizontal' role='form' method='post' action='". site_url('Pdf/share_utrip'). "' enctype='multipart/form-data'>

  <div class='row'>
    <div class='col-md-12'>
   
         <div class='form-group'>
          <label class='col-md-2 control-label' style='padding-top:10px;'>Email to </label>
          <div class='col-md-8'>
            <input type='hidden' name='id'  value=''/>
          <input type='text' class='form-control' name='email' value='' placeholder='Enter email'> 
          </div>
          </div>
    </div>
    </div> 
    <div class='modal-footer'>
      <input type='submit' class='btn btn-primary' value='Share'/>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
     </div>     
  
</form>   
    ";
}



 public function share_rtrip_details()

{
   $data=$this->session->all_userdata();
       // $id=$this->input->post('id');
  echo " <style> .tx{text-align:right;} .fnt{text-align:right;font-size:18px;}</style>            
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Share</font></h4>
    </div>
    <br>
  
<form  class='form-horizontal' role='form' method='post' action='". site_url('Pdf/share_rtrip'). "' enctype='multipart/form-data'>

  <div class='row'>
    <div class='col-md-12'>
   
         <div class='form-group'>
          <label class='col-md-2 control-label' style='padding-top:10px;'>Email to </label>
          <div class='col-md-8'>
            <input type='hidden' name='id'  value=''/>
          <input type='text' class='form-control' name='email' value='' placeholder='Enter email'> 
          </div>
          </div>
    </div>
    </div> 
    <div class='modal-footer'>
      <input type='submit' class='btn btn-primary' value='Share'/>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
     </div>     
  
</form>   
    ";
}


public function save_rtrip()
{

$this->load->model('Model_pdf');
$this->load->library('pdf1');
     
  $pdf=$this->pdf1->load();
   //$id=$this->input->post('tid');
   $this->load->model('Model_pdf');
   $data['results']=$this->Model_pdf->rtrip();
   ini_set('memory_limit','256M');
   $html=  $this->load->view('pdf/rtrip_pdf',$data,true);
  $pdf->WriteHtml($html);
  $output='Trip_running'.date('Y_m_d').'_.pdf';
  // $pdf->Output("$output",'D');
  $pdf->Output('upload/savedpdf/'.$output,'F');
  $this->session->set_flashdata('message','Trip Details Saved Successfully..!');
  redirect('Trip/view_trip');
 exit();
    
}

public function close_trip()
{
   $id=$this->input->post("tid");
       // $id=$this->input->post('id');
  echo " <style> .tx{text-align:right;} .fnt{text-align:right;font-size:18px;}</style>            
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Close Trip</font></h4>
    </div>
    <br>
  
<form  class='form-horizontal' role='form' method='post' action='". site_url('Trip/Close_tripDT'). "' enctype='multipart/form-data'>

  <div class='row'>
    <div class='col-md-12'>
   
         <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:10px;'>Closing Message </label>
          <div class='col-md-8'>
          <input type='hidden' name='id'  value='". $id ."'/>
          <textarea rows='2' class='form-control' name='comment'>Comments</textarea> 
          </div>
          </div>
    </div>
    </div> 
    <div class='modal-footer'>
		<div class='col-md-8' style='text-align:right;'>
	    <input type='submit' class='btn btn-primary' value='Close Trip'/>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
		</div>
     </div>     
  
</form>   
    ";
}

 public function close_tripDT()
 {
	$id=$this->input->post('id');
  $comment=$this->input->post('comment');
    $this->load->model('Model_trip');
		$result=$this->Model_trip->close_trip($id,$comment);
		if($result=1)
		{
      $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
      $date=date("Y/m/d");
      date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa');
      $userdt1 = array(
          'log_staffid' => $staffid,
          'log_staffname'=>$staffname,
          'log_time'=>$time,
          'log_date'=>$date,
          'log_entryid'=>$id,
          'log_desc'=>'Trip is closed'

          );
        $this->load->model('Model_customer');
        $this->Model_customer->form_insertlog($userdt1);
		 $this->session->set_flashdata('message', 'Trip Closed successfully');
		   redirect('Trip/completed_trip');          
		}
	
	}
	
public function getEnquiry()
{
	$id = $this->input->post('opt');
	$id1=explode("-",$id);

    $query = $this->db->select('*')->from('enquiry')->where('enquiry_id',$id1[0])->get();
    $results = $query->result();
	echo json_encode($results);
	//return $results;
}

public function farelist_ajax()
{
       $this->load->model('Model_trip');
       $results=$this->Model_trip->view_farelist();
		
       $data1 = array();
       foreach($results  as $r) 
        {
        //$select="<a href='#myModalc' id='$r->customer_id' data-toggle='modal' class='cselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' ><span class='glyphicon glyphicon-list-alt'></span></button></center></a>";
		$select="<a href='#myModalc' id='$r->fare_id' data-toggle='modal' class='cselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' >Select</button></center></a>";
                 array_push($data1, array(
					"ID"=>"$r->fare_id",
                    "corp"=>"$r->customer_name",
                    "vehicle"=>"$r->veh_type_name",
					"desc"=>"$r->fare_description",
					"farekm"=>"$r->fare_km",
					"amount"=> number_format($r->fare_amount,2),
                    //"status"=>$st
          ));
        }
        echo json_encode(array('data' => $data1));
        //$this->load->view('admin/view_business_category',$data1);
}

public function drv_dutylist_ajax()
{
       $this->load->model('Model_trip');
       $results=$this->Model_trip->view_dutylist();
		
       $data1 = array();
       foreach($results  as $r) 
        {
			
		if($r->drv_shift=='1')
			$ds='DAY';
		else if($r->drv_shift=='2')
			$ds='NIGHT';
		else
			$ds='<font color=red>OFF</font>';
		
		
		if($r->drv_date==date('Y-m-d'))
			$ddt="<b>".date_format(date_create($r->drv_date),"d-m-Y")."</b>";
		else
			$ddt=date_format(date_create($r->drv_date),"d-m-Y");
		
      //$select="<a href='#myModalc' id='$r->customer_id' data-toggle='modal' class='cselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' ><span class='glyphicon glyphicon-list-alt'></span></button></center></a>";
	  $select="<a href='#myModalc' id='$r->drv_id' data-toggle='modal' class='cselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' >Select</button></center></a>";
                 array_push($data1, array(
					"ID"=>"$r->drv_id",
					"date"=>$ddt,
                    "dname"=>"$r->driver_name",
                    "dmobile"=>"$r->drv_mobile",
					"dshift"=>"$ds",
					"dtime"=> "$r->drv_time",
                    //"status"=>$st
          ));
      }
      echo json_encode(array('data' => $data1));
      //$this->load->view('admin/view_business_category',$data1);
}


//------- trip booking --------------------------------------------------------------

public function booking_trip()
{
	$this->load->view('admin/booking_trip');
}

public function edit_booking_trip()
{
	$bid=$this->uri->segment(3);
	$this->load->Model('Model_trip');
	$data['result']=$this->Model_trip->edit_booking_trip($bid);
	$this->load->view('admin/edit_booking_trip',$data);
}


public function booking_list()
{
	$op=$this->uri->segment(3);
	$this->load->Model('Model_trip');
	
	$sdate=$this->input->post('startdate');
	$edate=$this->input->post('enddate');
	$cltype=$this->input->post('cltype');
	
	$btnget=$this->input->post('btnget');
	$btncancel=$this->input->post('btncancel');
	$btnclose=$this->input->post('btnclose');
	
	$dt1="";
	$dt2="";
	
	if($op=="")
	{
		$ses_cap="";
	$ses_where="trip_booking.book_status=1";
	$data['booking']= $this->Model_trip->view_booking_trip("0","");
	}
	else
	{
	$d1=explode("-",$sdate);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	
	$d2=explode("-",$edate);
	$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
	
	if($cltype=="")
	$where="(trip_booking.book_tripdate>='".$dt1. "' and trip_booking.book_tripdate<='".$dt2."')";
	else
	$where="(trip_booking.book_tripdate>='".$dt1. "' and trip_booking.book_tripdate<='".$dt2."') and trip_booking.book_clienttype=".$cltype;		

	if(isset($btnget))
	{
		$ses_cap="[ ".$sdate." => ". $edate." ]";
		$ses_where=$where. " and trip_booking.book_status='1' ". "order by book_id  desc";
		$data['booking']= $this->Model_trip->view_booking_trip("1",$where);
	}
	else if(isset($btncancel))
	{
		$ses_cap=" CANCELLED "; 
		$ses_where=$where. " and trip_booking.book_status='2' "."order by book_id desc";
		$data['booking']= $this->Model_trip->view_booking_trip("2",$where);
	}
	else if(isset($btnclose))
	{
		$ses_cap=" CLOSED "; 
		$ses_where=$where. " and trip_booking.book_status='3' "."order by book_id desc";
		$data['booking']= $this->Model_trip->view_booking_trip("3",$where);
	}
	
	} 
		
	$this->session->set_userdata('booking_where',$ses_where);
	$this->session->set_userdata('booking_cap',$ses_cap);
	$this->load->view('admin/view_booking',$data);
}



public function add_new_booking()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('clienttype', 'clienttype', 'required');

 if($this->form_validation->run())
    {
		
	$clienttype=$this->input->post('clienttype');
	
	if($clienttype=='1')
	{
		$agentid='0';
		$corp_na="0";
	}
	
	if($clienttype=='2')
	{
		$corp_na=$this->input->post('corpna');
		$agentid='0';
	}
	
	if($clienttype=='3')
	{
		$corp_na='0';
		$agentid=$this->input->post('agentid');
	}
		
	$roomno=$this->input->post('roomno');
	$guestna=$this->input->post('guestna');
	$vehiclecat=$this->input->post('vehiclecat');
	$vehicle=$this->input->post('vehicle');
	$to=$this->input->post('tripto');
	$tdate=$this->input->post('tripdate');
    $reptime=$this->input->post('reptime');
    $loca=$this->input->post('location');
	$contactno=$this->input->post('contactno');
	$payment=$this->input->post('payment');
	$desc=$this->input->post('desc');
	
	$vehno=strtoupper($this->input->post('vehno'));
	$drname=strtoupper($this->input->post('drname'));
	$drmobile=$this->input->post('drmobile');
	

	$tripop=$this->input->post('sltrip');
		
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
				
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
   
       $userdt = array(
                'book_clienttype'=>$clienttype,
				'book_agentid'=>$agentid,
				'book_corpid'=>$corp_na,
				'book_roomno'=>$roomno,
				'book_guestname'=>$guestna,
				'book_contactno'=>$contactno,
				'book_tripdate' => $tdate,
				'book_reporttime' =>$reptime,
				'book_location'=>$loca,
				'book_vcategory' =>$vehiclecat,
				'book_tripto' =>$to,
				'book_vehicle'=>$vehicle,
				'book_vehicleregno'=>$vehno,
				'book_drivername'=>$drname,
				'book_drivermobile'=>$drmobile,
				'book_payment'=>$payment,
				'book_tripoption'=>$tripop,
				'book_description'=>$desc,
                'book_status'=>"1"
               );
			   
        $this->load->Model('Model_trip');
        $this->Model_trip->book_insert($userdt);

		//-------------------------
	  $this->session->set_flashdata('message', 'Trip booking details added.');
      redirect('Trip/booking_trip');  
    }
    else
	{
      $this->session->set_flashdata('message1', 'Details missing, try again');
      //$data['message']="Data Inserted Successfully";
      redirect('Trip/booking_trip');
    }
}

public function update_booking()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('clienttype', 'clienttype', 'required');

 if($this->form_validation->run())
    {
		
	$bkid=$this->input->post('bkid');
	$clienttype=$this->input->post('clienttype');
	
	if($clienttype=='1')
	{
		$agentid='0';
		$corp_na="0";
	}
	
	if($clienttype=='2')
	{
		$corp_na=$this->input->post('corpna');
		$agentid='0';
	}
	
	if($clienttype=='3')
	{
		$corp_na='0';
		$agentid=$this->input->post('agentid');
	}
		
	$roomno=$this->input->post('roomno');
	$guestna=$this->input->post('guestna');
	$vehiclecat=$this->input->post('vehiclecat');
	$vehicle=$this->input->post('vehicle');
	$to=$this->input->post('tripto');
	$tdate=$this->input->post('tripdate');
    $reptime=$this->input->post('reptime');
    $loca=$this->input->post('location');
	$contactno=$this->input->post('contactno');
	$payment=$this->input->post('payment');
	$desc=$this->input->post('desc');

	$vehno=$this->input->post('vehno');
	$drname=$this->input->post('drname');
	$drmobile=$this->input->post('drmobile');
	
	$tripop=$this->input->post('sltrip');
		
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
				
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
   
       $userdt = array(
                'book_clienttype'=>$clienttype,
				'book_agentid'=>$agentid,
				'book_corpid'=>$corp_na,
				'book_roomno'=>$roomno,
				'book_guestname'=>$guestna,
				'book_contactno'=>$contactno,
				'book_tripdate' => $tdate,
				'book_reporttime' =>$reptime,
				'book_location'=>$loca,
	            'book_tripto' =>$to,
				'book_vcategory' =>$vehiclecat,
				'book_vehicle'=>$vehicle,
				'book_vehicleregno'=>$vehno,
				'book_drivername'=>$drname,
				'book_drivermobile'=>$drmobile,
				'book_payment'=>$payment,
				'book_tripoption'=>$tripop,
				'book_description'=>$desc,
                'book_status'=>"1"
               );
			   
        $this->load->Model('Model_trip');
        $this->Model_trip->booking_update($bkid,$userdt);

		//-------------------------
	  $this->session->set_flashdata('message', 'Booking details updated.');
      redirect('Trip/booking_list');  
    }
    else
	{
      $this->session->set_flashdata('message1', 'Details missing, try again');
      //$data['message']="Data Inserted Successfully";
      redirect('Trip/edit_booking_trip');
    }
}



public function booking_ajax()
{
	    $op=$this->uri->segment(3);
	    $this->load->Model('Model_trip');
		if($op==1)  //normal
		{
		  $results=$this->Model_trip->view_booking_trip($op,"");
		}
		else if($op==2)   //date wise
		{
		$d1=$this->uri->segment(4);
		$d2=explode("-",$d1);
		$dt=$d2[2]."-".$d2[1]."-".$d2[0];
        $results=$this->Model_trip->view_booking_trip($op,$dt);
		}
		else if($op==3)  //month wise
		{
		$m=$this->uri->segment(4);
	    $results=$this->Model_trip->view_booking_trip($op,$m);
		}
        else if($op==4)  //cancelled
		{
	    $results=$this->Model_trip->view_booking_trip($op,"");
		}
        $data1 = array();
        foreach($results  as $r) 
        {
	
		 // $del=anchor('Trip/delete_booking_trip/'.$r->book_id,'<i class="fa fa-trash-o" aria-hidden="true"></i>Delete', array('id' =>'del_conf'));
		  //$cancel=anchor('Trip/cancel_booking_trip/'.$r->book_id,'<i class="fa fa-ban" aria-hidden="true"></i>Cancel', array('id' =>'del_conf1'));

  		  /*$action='<div class="btn-group">
            <button type="button" class="btn btn-mini dropdown-toggle" style="font-size:13px; padding:3px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Action&nbsp;&nbsp;&nbsp; <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" style="padding:5px;font-size:13px;position:absolute;z-index:2500;">
           <!--<li><a href="" id="'.$r->book_id.'" data-toggle="modal" class="edit open-AddBookDialog2">&nbsp;<i class="fa fa-pencil" aria-hidden="true"></i> &nbsp;Edit</a></li> -->
           <li><hr style="margin:3px 0px ;padding:0px 3px;"></li>
           <li>'.$cancel.'</li>
		    <li><hr style="margin:3px 0px ;padding:0px 3px;"></li>
		   <li>'.$del. '</li>
          </ul>
        </div>';*/
				
			
		  $del=anchor('Trip/delete_booking_trip/'.$r->book_id,'<button class="btn btn-danger btn-xs">Delete&nbsp;</button>', array('id' =>'del_conf'));
		  //$cancel=anchor('Trip/cancel_booking_trip/'.$r->book_id,'<button class="btn btn-warning btn-xs">Cancel</button>', array('id' =>'del_conf1'));
		  $cancel='<a href="" id="'.$r->book_id.'" data-toggle="modal"  data-target="#myModal2" class="edit open-AddBookDialog2"><button class="btn btn-warning btn-xs">Cancel</button></a>';

			$action="<div>".$cancel.$del."</div>";
			
			if($op==4)	{	$action=$del;}  //cancel booking list action button
			
				
			if($r->book_roomno!=="")
			{
				$rg=$r->book_roomno;
			}
			
			if($r->book_clienttype=="1"){ 
			$ct="INDIVIDUAL"; 
			$cna =$r->book_guestname;
			}
			else if($r->book_clienttype=="2"){
			$ct="CORPORATE";
			$cna=$r->customer_name;
			}
			else if($r->book_clienttype=="3"){
				$ct="AGENT";
				$cna=$r->customer_name;
				}
			
			
			if($r->book_tripoption=="ST"){ $trop="SHORT TRIP"; }
			else if($r->book_tripoption=="LT"){ $trop="OUT STATION"; }
			else if($r->book_tripoption=="AD"){ $trop="AIRPORT DROP"; }
			else if($r->book_tripoption=="AP"){ $trop="AIRPORT PICKUP"; }
			else if($r->book_tripoption=="RD"){ $trop="RAILWAY DROP"; }
			else if($r->book_tripoption=="RP"){ $trop="RAILWAY PICKUP"; }
			else if($r->book_tripoption=="FP"){ $trop="FIXED PACKAGE"; }
			else if($r->book_tripoption=="CM"){ $trop="COMPLIMENTARY"; }
				
			if($r->book_status=="1"){ $stat="<font color=green>New</font>"; }
			else if($r->book_status=="2"){	$stat="<font color=red>Cancelled</font>"; }
			else if($r->book_status=="3"){	$stat="<font color=purple>Closed</font>"; }
			
			
             array_push($data1, array(
              "id"=>"$r->book_id",
              "ctype"=>$ct,	
			  "client"=>$cna,	
              "rgname"=>"Room No : ".$rg."<br>Contact : ".$r->book_contactno."<Br><b>Report:</b> <br>Date : ".$r->book_tripdate."<br> Time : ".$r->book_reporttime,
			  "loca"=>"<b>From :</b>".$r->book_location."<br><b>To : </b>".$r->book_tripto."<br><b>Vehicle :</b>".$r->veh_type_name."<br><b>Trip :</b>".$trop,
			  "desc"=>$r->book_description,
			  "reason"=>$r->book_reason,
			  "action"=>$action,
			  "status"=>$stat
			  ));
        }
        echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}


public function delete_booking_trip()
  {
			$bid=$this->uri->segment(3, 0);
			//$id=$this->input->post('tid');
			$date=date("Y/m/d");
			date_default_timezone_set('Asia/Kolkata');
			$time=date('h:i:sa');
			$staffid=$this->session->userdata('userid');
			$staffname=$this->session->userdata('name');
			
			$this->load->model('Model_trip');
            $result=$this->Model_trip->delete_bookingtrip($bid);
            if($result)
            {
            
			$userdt1 = array(
				'log_staffid' => $staffid,
				'log_staffname'=>$staffname,
				'log_time'=>$time,
				'log_date'=>$date,
				'log_entryid'=>$bid,
				'log_desc'=>'Trip Details Deleted'
            );
			
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
		  $this->session->set_flashdata('message', 'Trip booking details deleted..!');
          redirect('Trip/booking_list');
            }
           else
           {
            $this->session->set_flashdata('message1', 'Please try again.');
            redirect('Trip/booking_list');
           }
  }

  public function close_booking()
  {
	  $bid=$this->input->post('bid');
						echo ' <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Close Trip</h4>
                                </div>
								<form Method="POST" action="'.base_url('Trip/close_booking_trip').'" >
								
								<div class="form-group" style="margin-top:20px;"> 
								<div class="row">
								   <label class="col-md-4 control-label la-right">Booking Id</label>
									<div class="col-md-6">
									 <input type="text" class="form-control" name="cbookid" id="cbookid" value="'.$bid.'" readonly>
								</div>
								</div>
								
								<div class="row" style="margin-top:15px;">
								   <label class="col-md-4 control-label la-right">Enter Trip ID</label>
									<div class="col-md-6">
									<input type="text" class="form-control" name="tripno" id="tripno" required>
								</div>
								</div>
								<div class="row"> </div>
								
								<div class="modal-footer" style="margin-top:20px;">
                                    <input type="submit" class="btn btn-primary"  value="Close Trip">
									<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                </div>
                            </div>
							</form>';
  }
  
    
 public function close_booking_trip()
  {
			//$bid=$this->uri->segment(3, 0);
			$tripid=$this->input->post('tripno');
			$bid=$this->input->post('cbookid');
				
			$this->load->model('Model_trip');
			$ndt=array("book_tripno"=>$tripid,"book_status"=>"3");
			
            $result=$this->Model_trip->close_bookingtrip($bid,$ndt);
            if($result)
            {
				$this->session->set_flashdata('message', 'Booking trip closed.!');
				redirect('Trip/booking_list');
            }
           else
           {
            $this->session->set_flashdata('message1', 'Please try again.');
            redirect('Trip/booking_list');
           }
  }
    
  
public function cancel_booking()
{

 $id=$this->input->post("bid");
       // $id=$this->input->post('id');
  echo " <style> .tx{text-align:right;} .fnt{text-align:right;font-size:18px;}</style>            
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Cancel Booking</font></h4>
    </div>
    <br>
  
<form  class='form-horizontal' role='form' method='post' action='". site_url('Trip/cancel_booking_trip'). "' enctype='multipart/form-data'>

  <div class='row'>
    <div class='col-md-12'>
   
         <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:10px;'>Enter Reason : </label>
          <div class='col-md-8'>
          <input type='hidden' name='id'  value='". $id ."'/>
          <textarea rows='4' class='form-control' name='reason' required></textarea> 
          </div>
          </div>
    </div>
    </div> 
    <div class='modal-footer'>
		<div class='col-md-8' style='text-align:right;'>
	    <input type='submit' class='btn btn-primary' value='Cancel Trip'/>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
		</div>
     </div>     
  
</form>   
    ";
}
  
  
public function cancel_booking_trip()
  {
	  
			//$bid=$this->uri->segment(3, 0);
			
			$bid=$this->input->post('id');
			$reason=$this->input->post('reason');
			
			$ndt=array("book_reason"=>$reason,
						"book_status"=>"2");
						
			$this->load->model('Model_trip');
			$result=$this->Model_trip->cancel_bookingtrip($bid,$ndt);
            if($result)
            {
				$this->session->set_flashdata('message', 'Trip booking cancelled.!');
				redirect('Trip/booking_list');
			}
			else
			{
			$this->session->set_flashdata('message1', 'Please try again.');
            redirect('Trip/booking_list');	
			}
  }
  
  
  function get_corporate_farelist()
  {
	$id = $this->input->post('coid');
   	
	$this->load->model("Model_trip");
	$results=$this->Model_trip->get_corp_fare_list($id,"1");
	
	//$tb="<table width='100%' style='border:1px solid #e4e4e4;' id='fare'>
	//<thead><th style='padding:3px;'>Sl.No</th><th style='padding:3px;'>Vehicle</th><th style='padding:3px;'>Description</th>
	//<th style='padding:3px;'>KM</th><th style='text-align:right;padding:3px;'>Rate</th></thead><tbody>";
	$tb="";
	$n=1;
	foreach($results as $r)
	{
		$tb.="<tr class='ftr1' style='border:1px solid #e4e4e4;padding:5px;'>
		<td style='border:1px solid #e4e4e4;padding:3px;'>".$n."</td>
		<td style='border:1px solid #e4e4e4;padding:3px;'>".$r->veh_type_name."</td>
		<td style='border:1px solid #e4e4e4;padding:3px;'>".$r->fare_description."</td>
		<td style='border:1px solid #e4e4e4;padding:3px;'>".$r->fare_km."</td>
		<td style='border:1px solid #e4e4e4;text-align:right;padding:3px;'>".number_format($r->fare_amount,'2',".","")."</td></tr>";
		$n++;
	}

	//$tb.="</tbody></table>";
	echo  $tb;
  }
  
  
  function get_fixed_farelist()
  {
	$id = $this->input->post('coid');
   	
	$this->load->model("Model_trip");
	$results=$this->Model_trip->get_corp_fare_list($id,"2");

	$tb1="";
	$n=1;
	foreach($results as $r)
	{
		$tb1.="<tr class='ftr1' style='border:1px solid #e4e4e4;padding:5px;'>
		<td style='border:1px solid #e4e4e4;padding:3px;'>".$n."</td>
		<td style='border:1px solid #e4e4e4;padding:3px;'>".$r->veh_type_name."</td>
		<td style='border:1px solid #e4e4e4;padding:3px;'>".$r->category_name."</td>
		<td style='border:1px solid #e4e4e4;text-align:right;padding:3px;'>".number_format($r->fare_amount,'2',".","")."</td></tr>";
		$n++;
	}

	//$tb.="</tbody></table>";
	echo  $tb1;
  }
  
  function get_club_farelist()
  {
	$id = $this->input->post('coid');
   	
	$this->load->model("Model_trip");
	$results=$this->Model_trip->get_corp_fare_list($id,"3");

	$tb3="";
	$n=1;
	foreach($results as $r)
	{
		$tb3.="<tr class='ftr1' style='border:1px solid #e4e4e4;padding:5px;'>
		<td style='border:1px solid #e4e4e4;padding:3px;'>".$n."</td>
		<td style='border:1px solid #e4e4e4;padding:3px;'>".$r->veh_type_name."</td>
		<td style='border:1px solid #e4e4e4;padding:3px;'>".$r->category_name."</td>
		<td style='border:1px solid #e4e4e4;text-align:right;padding:3px;'>".number_format($r->fare_amount,'2',".","")."</td></tr>";
		$n++;
	}

	//$tb.="</tbody></table>";
	echo  $tb3;
  }
    
  
  function get_corporate_name()
  {
	$id = $this->input->post('coid');
   	$rw=$this->db->select('customer_name')->from('customers')->where('customer_id',$id)->get()->row();
	if(is_object($rw))
	{
	echo  $rw->customer_name;
	}
  }

  
  public function get_booking_ref_roomno()
  {
	  $bid=$this->input->post('bid');
	  if($bid=='0')
	  {
		  $bdt="0#0";
	  }
	  else
	  {
	  $brow=$this->db->select('*')->from('trip_booking')->where('book_id',$bid)->get()->row();
	  $bdt=$brow->book_id."#".$brow->book_roomno;
	  }
	  echo $bdt;
  }
  
}