<?php
class Model_pdf extends CI_Model{
	
public function service_invoice($id){
   //date_default_timezone_set('Asia/Calcutta');
 //$date = date("Y");
    $this->db->select('*');
    $this->db->from('vehicle_registration');
    $this->db->join('vehicle_service ','vehicle_service.service_vehicleid=vehicle_registration.vehicle_id','inner');
    $this->db->join('vehicle_ownerreg ','vehicle_ownerreg.owner_id=vehicle_registration.vehicle_owner','inner');
      $this->db->where('vehicle_service.service_vehicleid',$id);
        $query = $this->db->get();
          return $query->row();
	}
public function service_invoice1($id){
   //date_default_timezone_set('Asia/Calcutta');
 //$date = date("Y");
    $this->db->select('*');
    $this->db->from('vehicle_registration');
    $this->db->join('vehicle_service ','vehicle_service.service_vehicleid=vehicle_registration.vehicle_id','inner');
    $this->db->join('vehicle_ownerreg ','vehicle_ownerreg.owner_id=vehicle_registration.vehicle_owner','inner');
      $this->db->where('vehicle_service.service_vehicleid',$id);
        $query = $this->db->get();
          return $query->result();
    }


public function trip($id){

    $this->db->select("*");
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    //$this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
    //$this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=trip_management.trip_vehicle_id','inner');
    $this->db->where("trip_management.trip_id",$id);
    $query = $this->db->get();
    return $query->row();

}


public function tripquotation($id){

    $this->db->select("*");
    $this->db->from('quotation');
    $this->db->where("tripquot_id",$id);
     $query = $this->db->get();
    return $query->row();

}


public function UPCtrip(){

    $this->db->select("*");
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
    $this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=trip_management.trip_vehicle_id','inner');
    $this->db->where("trip_management.trip_status",'1');
    $this->db->where("trip_management.trip_startdate>",date('Y-m-d'));
    $query = $this->db->get();
    return $query->result();

}
public function rtrip(){

    $this->db->select("*");
    $this->db->from('trip_management');
    $this->db->join('customers', 'customers.customer_id=trip_management.trip_cust_id','inner');
    $this->db->join('driverregistration', 'driverregistration.driver_id=trip_management.trip_driver_id','inner');
    $this->db->join('vehicle_registration', 'vehicle_registration.vehicle_id=trip_management.trip_vehicle_id','inner');
    $this->db->where("trip_management.trip_status",'1');
    //$this->db->where("trip_management.trip_startdate>",date('Y-m-d'));
    $query = $this->db->get();
    return $query->result();

}



public function customer(){

    $this->db->select('*');
    $this->db->from('customers');
    $this->db->where('customer_status', 1);
    $query = $this->db->get();
    return $query->result();
}

public function staff(){

    $this->db->select('*');
    $this->db->from('staffregistration');
    $this->db->where('staff_status', 1);
    $query = $this->db->get();
    return $query->result();
}
public function user(){

    $this->db->select('*');
    $this->db->from('users');
    $this->db->where('status', 1);
    $query = $this->db->get();
    return $query->result();
}

public function view_invoice($invid)
{
	$this->db->select('*');
    $this->db->from('invoices');
    $this->db->where('invo_invoiceid',$invid);
    $query = $this->db->get();
    return $query->result();
}

public function view_invoice_trips($invid)
{
	$this->db->select('*');
    $this->db->from('unpaid_trips');
    $this->db->where('up_invoiceno',$invid);
    $query = $this->db->get();
    return $query->result();
}

}
?>