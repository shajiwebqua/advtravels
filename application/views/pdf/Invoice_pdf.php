<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Travels</title>

  <style type="text/css">
	td{padding:2px 5px 3px 5px; }
    .tb2{ border:none;}
	.tr1{height:35px;}
	.tr3{height:40px;}
	.tr2{height:200px;}
	.add{padding:15px;}
	
	.row1 td{border:1px solid #999;height:25px;}
     table{font-size:14px;}
	.row2{height:35px;}
	 
	.table-main{
          border-collapse: collapse;
     }

    .table-main td{
    border: 1px solid black;
     }
	
	
	
	.table-inner td{
		border: none !important;
	} 
	 
   </style>
  
  </head>
<body>
<br><br>

<div style='width:1024px;'>
<?php

foreach ($invoice as $iv)
{
$cad=$this->db->select('customer_name,customer_address')->from('customers')->where('customer_id',$iv->invo_customer)->get()->row();	
$gno=$this->db->select('cg_gstno')->from('customer_gst')->where('cg_custid',$iv->invo_customer)->get()->row();	
?>

<table width='100%'><tr><td align='center'><h2>Tax Invoice</h2></td></tr></table>
<br>
<table width='100%' cellpadding=0 cellspacing=0 class='table-main'>
<tr >
<td style='width:40%;border:1px solid #000;' rowspan=3 valign='top'>
<br>
		<address>
            <strong>Advance World Holidays</strong><br>
            PK & Sons Complex, East Moozhikkal<br>
            NH 212, Calicut, Kerala, India-673571<br>
			Tel: 0091 495 2732 500, 0091 495 3100 700<br>
            Fax: 0091 495 2732 400
          </address>

</td>
	<td  style='width:30%;' class='table-main'> <table class='table-inner' width='100%'><tr><td>Invoice No.</td></tr><tr><td style='font-weight:600;'><b>INV-<?=$iv->invo_invoiceid;?></b></td></tr></table> </td>
	<td style='width:30%;' class='table-main'> <table class='table-inner' width='100%'><tr><td>Dated.</td></tr><tr><td style='font-weight:600;'><b><?=date_format(date_create($iv->invo_date),'d-m-Y');?></b></td></tr></table>
	</td>
</tr>


<tr>
    <td style='width:30%;' class='table-main'> <table class='table-inner' width='100%'><tr><td>Delivery Note.</td></tr><tr><td style='font-weight:600;'><b><?=$iv->invo_delinote;?></b></td></tr></table> </td>
	<td style='width:30%;' class='table-main'> <table class='table-inner' width='100%'><tr><td>Mode/Terms of Payment</td></tr><tr><td style='font-weight:600;'><b><?=$iv->invo_terms;?> Day(s)</b></td></tr></table>
	</td>
</tr>

<tr>
    <td style='width:30%;'> <table class='table-inner' width='100%'><tr><td>Supplier's Ref.</td></tr><tr><td style='font-weight:600;'><b>&nbsp;<?=$iv->invo_supplierref;?></b></td></tr></table> </td>
	<td style='width:30%;'> <table class='table-inner' width='100%'><tr><td>Other Reference(s)</td></tr><tr><td style='font-weight:600;'><b>&nbsp;<?=$iv->invo_otherref;?></b></td></tr></table>
	</td>
</tr>

<tr><td style='width:40%;border:1px solid #000;' rowspan=4 valign='top' >
<br>
<span >Buyer:</span><br>
		<address>
            <strong><?=$cad->customer_name;?></strong><br>
            <?=nl2br($cad->customer_address);?><br>
			<?php
			if(!empty($gno))
			{
				echo "GST No: ".$gno->cg_gstno;
			}
			?>
          </address>
</td>
<td style='width:30%;'> <table class='table-inner' width='100%'><tr><td>Buyer's Order No.</td></tr><tr><td style='font-weight:600;'><b>&nbsp;<?=$iv->invo_buyersordno;?></b></td></tr></table> </td>
	<td style='width:30%;'> <table class='table-inner' width='100%'><tr><td>Dated.</td></tr><tr><td style='font-weight:600;'><b>&nbsp;<?=$iv->invo_buyersdated;?></b></td></tr></table>
	</td>
</tr>
	
<tr>
    <td style='width:30%;'><table class='table-inner' width='100%'><tr><td>Despatch Document No.</td></tr><tr><td style='font-weight:600;'><b>&nbsp;<?=$iv->invo_desdocno;?></b></td></tr></table> </td>
	<td style='width:30%;'> <table class='table-inner' width='100%'><tr><td>Delivery Note Date</td></tr><tr><td style='font-weight:600;'><b>&nbsp;<?=$iv->invo_delinotedate;?></b></td></tr></table>
	</td>
</tr>

<tr><td style='width:30%;'><table class='table-inner' width='100%'><tr><td>Despatched through</td></tr><tr><td style='font-weight:600;'><b>&nbsp;<?=$iv->invo_despatched;?></b></td></tr></table> </td>
	<td style='width:30%;'> <table class='table-inner' width='100%'><tr><td>Destination</td></tr><tr><td style='font-weight:600;'><b>&nbsp;<?=$iv->invo_destination;?></b></td></tr></table>
	</td>
</tr>

<tr><td style='width:30%;height:120px;' colspan=2  valign='top' >Terms of Delivery<br><b><?=$iv->invo_deliterms;?></b></td></tr>
</table>

<table class='table-main' width='100%' cellpadding=0 cellspacing=0 >
	<tr style='font-weight:600;'>
		<td style='height:25px;' width='5%'>sl no</td>
		<td width='45%'>Description of Goods</td>
		<td width='12%'>HSN/HAC</td>
		<td width='10%'>Quantity</td>
		<td width='10%'>Rate</td>
		<td width='6%'>Per</td>
		<td width='12%'>Amount</td>
	</tr>
	<?php
	$gst=number_format(($iv->invo_gst/2),'2','.','');
	?>
	<tr valign='top'>
		<td width='5%' style='height:200px;' valign='top'>1<br>2<br>3</td>
		<td width='45%' valign='top' align='right'><b><?=$iv->invo_description;?><br>
		SGST<br>CGST</b>
		</td>
		<td width='12%' valign='top'><?=$iv->invo_hsnsac;?></td>
		<td width='10%' valign='top'>&nbsp;</td>
		<td width='10%' valign='top'>&nbsp;</td>
		<td width='6%' valign='top'>&nbsp;</td>
		<td width='12%' valign='top' align='right'><b><?=number_format($iv->invo_amount,'2','.','');?><br><?=$gst;?><br><?=$gst;?></b></td>
	</tr>
	
	<tr class='tr1'>
		<td width='5%'>&nbsp;</td>
		<td width='42%'>&nbsp;</td>
		<td width='12%'>&nbsp;</td>
		<td width='10%'>&nbsp;</td>
		<td width='10%'>&nbsp;</td>
		<td width='6%'>&nbsp;</td>
		<td width='15%' align='right' style='font-size:18px;' ><b><?=number_format($iv->invo_total,'2','.','');?>&nbsp;</td>
	</tr>
	
	<tr valign='top'>
		<td colspan=7>
		<table class='table-inner' width='100%'>
			<tr><td>Amount chargable (in words): </td><td align='right'>E. & O.E</td></tr>
		<tr><td colspan=2 style='font-weight:600;padding-left:50px;'><b>
		<?=$iv->invo_totalwords;?></b></td></tr></table>
		</td>
	</tr>

</table>

<table  class='table-main' width='100%' cellpadding=0 cellspacing=0 >
	<tr  style=' font-weight:600;'>
		<td  width='25%' rowspan=2 valign='top' align='center' >HSN/SAC</td>
		<td width='20%' rowspan=2 valign='top' align='center'>Taxable<br> Value</td>
		<td width='20%' align='center' colspan=2>Central Tax</td>
		<td width='20%' align='center' colspan=2>State Tax</td>
		<td width='15%' rowspan=2 valign='middle' align='center'>Total <br>Tax Amount</td>
	</tr>
	<tr style='font-weight:600;'>
		<td >Rate</td>
		<td >Amount</td>
		<td >Rate</td>
		<td >Amount</td>
	</tr>
	<tr >
	<?php
	$gper=number_format($iv->invo_gstper/2,'2','.','');
	?>
		<td  style='height:40px;'>&nbsp;<br><?=$iv->invo_hsnsac;?></td>
		<td align='right'>&nbsp;<br><?=number_format($iv->invo_amount,'2','.','');?></td>
		<td align='right'><?=$iv->invo_gstper;?>%<br><?=$gper;?></td>
		<td align='right'>&nbsp;<br><?=$gst;?></td>
		<td align='right'><?=$iv->invo_gstper;?>%<br><?=$gper;?></td>
		<td align='right'>&nbsp;<br><?=$gst;?></td>
		<td align='right'>&nbsp;<br><?=number_format($iv->invo_gst,'2','.','');?></td>
	</tr>
	
	<tr >
		<td style='height:30px;'><b>Total</b></td>
		<td align='right'><b><?=number_format($iv->invo_amount,'2','.','');?></b></td>
		<td align='right'>&nbsp;</td>
		<td align='right'><b><?=$gst;?></b></td>
		<td align='right'>&nbsp;</td>
		<td align='right'><b><?=$gst;?></b></td>
		<td align='right'><b><?=number_format($iv->invo_gst,'2','.','');?></b></td>
	</tr>

<tr>
<td colspan=7 height='120px' valign='top'>
<table class='table-inner' width='100%' cellpadding=0 cellspacing=0 >
	<tr style='border:none;'><td>Taxable Amount (in words): </td></tr>
	<tr style='border:none;'><td style='padding-left:50px;'><b><?=$iv->invo_taxwords;?><b></td></tr>
</table>
</td>
</tr>
</table>
	
<table class='table-main' width='100%' cellpadding=0 cellspacing=0 >
	<tr height='80px'>
		<td width='65%' valign='top'><b>Declaration:</b><br>
		We declare that this invoice shows the actual price of the goods described and  that all particulars  are true and correct.
		</td>
		<td  valign='top' align='right'>
		<b>For Advance World</b><br><br><br>
		Authorized Signatory
		
		</td>
	</tr>
	
</table>	
<table width='100%'><tr><td align='center'><p>This is a computer generated invoice</p></td></tr></table>


<?php
}
?>

</div>


</html>