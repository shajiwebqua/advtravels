<!DOCTYPE html>

<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>Travels</title>
<style>
.td1 
{
  border-bottom:1px solid #e4e4e4;
  text-align:left;
}
.th1
{
  border-bottom:1px solid #e4e4e4;
  text-align:left;
}
</style>    
</head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India - 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>
<?php
if($month==1)
{
$mon="-[ January ]";
}
else if($month==2)
{
  $mon="-[ February ]";
}
else if($month==3)
{
  $mon="-[ March ]";
}
else if($month==4)
{
  $mon="-[ April ]";
}
else if($month==5)
{
  $mon="-[ May ]";
}
else if($month==6)
{
  $mon="-[ June ]";
}
else if($month==7)
{
  $mon="-[ July ]";
}
else if($month==8)
{
  $mon="-[ August ]";
}
else if($month==9)
{
  $mon="-[ September ]";
}
else if($month==10)
{
  $mon="-[ October ]";
}
else if($month==11)
{
  $mon="-[ November ]";
}
else if($month==12)
{
  $mon="-[ December ]";
}

else
{
$mon=="";
}



        ?>
 <table width="100%"><tr><td><h4>Vehicle Service List <?=$mon;?> </h4></td></tr>
 <tr><td></td></tr>
 <tr><td></td></tr>
 </table>
    <table style="width:100%;border:1px solid #e4e4e4;" cellpadding=5 cellspacing=0>
       <thead>
      <tr>
         <tr style='color:#4b88ed;border:1px solid #e4e4e4;'>
           <th class='th1'>Slno</th>
            <th class='th1'>Vendor Name</th>
             <th class='th1'>Mobile</th>
            <th class='th1'>Service Date</th>
          <th class='th1'>Reg No</th>
          <th class='th1'>Description</th>
          <th class='th1'>Invoice Date</th>
         <th class='th1'>Invoice No</th>
         <th class='th1'>Invoice Amount</th>
        
         

        </tr>
      </tr>
    </thead>
    <tbody>
      <?php
      $slno=1;
      $gtot=0;
        foreach ($results as $key => $value) {
        ?>
        <tr >
        <td class='td1'><?=$slno;?> </td>
        <td class='td1'><?=$value->owner_name;?></td>
        <td class='td1'><?=$value->owner_mobile;?></td> 
        <td class='td1'><?=$value->service_date;?></td>
        <td class='td1'><?=$value->vehicle_regno;?>  </td>
        <td class='td1'><?=$value->service_description;?> </td>
        <td class='td1'><?=$value->service_invoicedate;?> </td>
        <td class='td1'><?=$value->service_invoiceno;?> </td>
        <td class='td1'><?=$value->service_invoiceamt;?> </td>
      </tr>
        <?php
        
        $slno++;

          $gtot=$gtot+$value->service_invoiceamt;
        }
        ?>
      
    </tbody>
    </table>
      <hr>

                      
<table width='100%' >
<tr><td width='100px'></td><td width='70px'></td><td width='100px'></td><td style='color:blue;'><b>Grand Total</b> </td>
<td width='100px' style='font-size:18px;' colspan='2'>:&nbsp;&nbsp;<b><b>Rs:<?=$gtot;?> </b></td>
</tr>
</table>
</body>
</html>