<?php
 include('application/views/common/header.php');?>

 <style>
 .tabbable-panel {
  border:1px solid #eee;
  padding: 3px 10px 3px 10px; 
  height:400px;
}

/* Default mode */
.tabbable-line > .nav-tabs {
  border: none;
  margin: 0px;
}
.tabbable-line > .nav-tabs > li {
  margin-right: 2px;
}
.tabbable-line > .nav-tabs > li > a {
  border: 0;
  margin-right: 0;
  color: #737373;
}
.tabbable-line > .nav-tabs > li > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
  border-bottom: 4px solid #fbcdcf;
}
.tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
  border: 0;
  background: none !important;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
  margin-top: 0px;
}
.tabbable-line > .nav-tabs > li.active {
  border-bottom: 4px solid #f3565d;
  position: relative;
}
.tabbable-line > .nav-tabs > li.active > a {
  border: 0;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.active > a > i {
  color: #404040;
}
.tabbable-line > .tab-content {
  margin-top: -3px;
  background-color: #fff;
  border: 0;
  border-top: 1px solid #eee;
  padding: 15px 0;
}
.portlet .tabbable-line > .tab-content {
  padding-bottom: 0;
}

/* Below tabs mode */

.tabbable-line.tabs-below > .nav-tabs > li {
  border-top: 4px solid transparent;
}
.tabbable-line.tabs-below > .nav-tabs > li > a {
  margin-top: 0;
}
.tabbable-line.tabs-below > .nav-tabs > li:hover {
  border-bottom: 0;
  border-top: 4px solid #fbcdcf;
}
.tabbable-line.tabs-below > .nav-tabs > li.active {
  margin-bottom: -2px;
  border-bottom: 0;
  border-top: 4px solid #f3565d;
}
.tabbable-line.tabs-below > .tab-content {
  margin-top: -10px;
  border-top: 0;
  border-bottom: 1px solid #eee;
  padding-bottom: 15px;
}
</style>
 
 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>New Vehicle Entry</b></h1>
	
    <ol class="breadcrumb" style='font-size:15px;'>
	<li><a href="<?php echo base_url('Vehicle/add_vehicle');?>" data-target="#myModal1" data-toggle='modal' style='color:#4b88ed;'><i class="fa fa-cab" aria-hidden="true"></i>New Vehicle</a></li>
	<li><a href="<?php echo base_url('Vehicle/add_vehicle');?>" data-target="#myModal1" data-toggle='modal' style='color:#4b88ed;'><i class="fa fa-cab" aria-hidden="true"></i>Vehicle Accessories</a></li>
	
	<li><a href="<?php echo base_url('owner/index');?>"style='color:#4b88ed;'><i class="fa fa-address-card" aria-hidden="true"></i>Vendors</a></li>
	<li><a href="<?php echo base_url('Driver/index');?>"style='color:#4b88ed;'><i class="fa fa-user-plus" aria-hidden="true"></i>Drivers</a></li>
	<li><a href="<?php echo base_url('Services/index');?>" style='color:#4b88ed;'><i class="fa fa-file-text-o" aria-hidden="true"></i>Services</a></li>

	<!--<li><a href="<?php echo base_url('owner/index');?>" style="color:blue;"><h5><b>Owners List</b></h5></a></li>
	<li><a href="<?php echo base_url('Driver/index');?>" style="color:blue;" ><h5><b>Drivers List</b></h5></a></li>
	<li><a href="<?php echo base_url('Vehicle/add_vehicle');?>"  data-target="#myModal1" data-toggle='modal'  style="color:blue;"><h5><b>Add Vehicle</h5></b></a></li>
	<li><a href="<?php echo base_url('Services/index');?>" style="color:blue;"><h5><b>Service Details</h5></b></a></li>-->
	
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 	  
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
	<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
	
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details -->
	

	 <div style="background-color:#fff;padding:15px; ">
		
							
<div class="page-content-wrapper">
 	
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
			
        <div class="row">
        <div class="col-md-12">
				
		<!-- tab pages ---------------------------------- -->	
		
			<div class="tabbable-panel">
				<div class="tabbable-line">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#tab_default_1" data-toggle="tab">
							Vehicle Details </a>
						</li>
						<li>
							<a href="#tab_default_2" data-toggle="tab">
							Accessories Details </a>
						</li>
						<li>
							<a href="#tab_default_3" data-toggle="tab">
							Tab 3 </a>
						</li>
					</ul>
			<div class="tab-content">
			<div class="tab-pane active" id="tab_default_1">

			<!-- 1st tab page content -------------------------------- -->
	
			<div class="rows">
			<!-- 1st column ---------------- -->
			<div class="col-md-5">
	
				<form  class='form-horizontal' role='form' method='post' action='". site_url('vehicle/add_new_vehicle'). "' enctype='multipart/form-data'>
                    <!-- text input -->
          
							<!--  <input list='vendors' name='vendor' /></label>
								<datalist id='vendors'>
									  <option value='Shaji'>
									  <option value='Renny'>
									  <option value='Shinil'>
									  <option value='Raju'>
									  <option value='Suresh'>
									  <option value='Subheesh'>
								</datalist>  -->
		  
		  
					<div class='form-group'>
						  <input type='hidden' name='date' class='form-control' value="date('Y-m-d')"/>
						  <label class='col-md-4 control-label' style='padding-top:5px;'>Vendor Name</label>
						   <div class='col-md-7'>
							<select name='owner' class='form-control'>';
							 <option value=''>----</option>";
								$section = $this->db->select('*')->from('vehicle_ownerreg')->get();
									$sectn1=$section->result();
									foreach ($sectn1 as $ro):
									   echo "<option value='$ro->owner_id'>$ro->owner_name</option>";
									endforeach;
							   echo "                     
									</select>
						   </div>
					</div>

                            
					 <div class='form-group'>
						   <label class='col-md-4 control-label' style='padding-top:5px;'>Registration No </label>
						   <div class='col-md-7'>  
								<input type='text' name='regno' class='form-control' required  />
						   </div>                   
					  </div>

					  <div class='form-group'>
								 <label class='col-md-4 control-label' style='padding-top:5px;'>Vehicle Type </label>
								<div class='col-md-7'>  
								
								<select name='type' class='form-control' >
								<option value='0'>---------</option>
								<option value='1'>INNOVA</option>
								<option value='2'>TAVERA</option>
								<option value='3'>CAR</option>
								<option value='4'>BUS</option>
								<option value='5'>TATA SUMO</option>
								<option value='6'>TRAVALLER</option>
								<option value='6'>ENJOY</option>
								</select>
						 </div>                   
					  </div>
       
					  <div class='form-group'>
					  <label class='col-md-4  control-label' style='padding-top:5px;'>No Of Seats </label>
					  <div class='col-md-7'>
					  <input type='number' class='form-control' placeholder='' name='seats' required>
						</div>
					  </div>
					  
					  <div class='form-group'>
					  <label class='col-md-4  control-label'>Rent Period</label>
					  <div class='col-md-7'>
					  <select name='rentperiod' class='form-control' >
								<option value='0'>---------</option>
								<option value='1'>DAILY</option>
								<option value='2'>WEEKLY</option>
								<option value='3'>MONTHLY</option>
								</select>
					  </div>
					  </div>
								
					  <div class='form-group'>
					  <label class='col-md-4  control-label'>Rent</label>
					  <div class='col-md-7'>
					  <input type='number' class='form-control' placeholder='' name='rent' required/>
					  </div>
					  </div>
		  
					<label style='height:1px;width:100%;background-color:#e4e4e4;'></label>		  
							  
					  <div class='form-group'> 
							<div class='col-md-4'>
							</div>
							<div class='col-md-7'>
							<input type='submit' class='btn btn-primary' value='Save Vehicle Details'/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
							</div>
							</div>                                
					   </form>
			</div>
			<!-- 1st column end ---------------- -->
			
			<!-- second column--------- -->
			<div class="col-md-7">
			</div>
			<!---  second column end ---- -->
			</div>
			</div>			
		
			   
							   
   <!--  <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div> -->

<!-- ----------1st tab page content end--------------------------------------------------- -->
						<div class="tab-pane" id="tab_default_2">
							<p>
								Howdy, I'm in Tab 2.
							</p>
							<p>
								Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.
							</p>
							<p>
								<a class="btn btn-warning" href="http://j.mp/metronictheme" target="_blank">
									Click for more features...
								</a>
							</p>
						</div>
						<div class="tab-pane" id="tab_default_3">
							<p>
								Howdy, I'm in Tab 3.
							</p>
							<p>
								Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat
							</p>
							<p>
								<a class="btn btn-info" href="http://j.mp/metronictheme" target="_blank">
									Learn more...
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!-- tab pages  end---------------------------------- -->	
			
		</div>
		</div>

			
							   
				   
				   
				   
				   
				   
				   
				   
				   
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                  <!--          <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
								<tr>
								 <th>Edit</th>
								
								 <th>Delete</th>
								 <th>Date</th>
								 <th>Owner</th>
								 <th>Reg No</th>
								 <th>Type</th>
								 <th>Seat</th>
								 <th>Rent</th>
								 <th>Rent Period</th>
								 <th>Status</th>
								</tr>
								</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                       <!-- </div> -->
                 <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
		
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Edit</h4>
								</div>
							</div>
							</div>
						<!-- /.modal-dialog -->
						</div>
                   </div>	

                    <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div>       						
				
	</div>
	<!-- End user details -->
	</div>
	
	</div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>



<script type="text/javascript">

//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------
 
      $('#example').dataTable( {
		  "ordering":false,
         destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "vehicle/vehicle_ajax",// json datasource
               
               },
		"columnDefs":[
		{"width":"8%","targets":0},
		{"width":"8%","targets":1},
		{"width":"8%","targets":2},
		],
			   
        "columns": [
           
            { "data": "edit"},
		
            { "data": "delete" },
            { "data": "date"},
             { "data": "owner"},
              { "data": "regno"},
              { "data": "type"},
              { "data": "seats"},
            { "data": "rent" },
			{ "data": "period" },
            { "data": "status" }
       ]
  } );


$('#example tbody').on('click', '.add', function () {
        var Result=$("#myModal1 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         // var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "vehicle/add_vehicle",
        dataType: 'html',
        // data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 

      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "vehicle/edit_vehicle",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 


  $('#example tbody').on('click', '.change', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "userprofile/change_password",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 		

		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

 //});
  


</script>



