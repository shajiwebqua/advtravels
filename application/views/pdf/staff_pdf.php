<!DOCTYPE html>
<html lang="en">
    

<head>

  <meta charset="utf-8" />
  <title>Travels</title>
  <style type="text/css">
    td{
		 padding:7px 0px 7px 0px;
	}
		
  </style>
</head>
<body>

<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
</tr>
<tr>
<td style="padding: 0px">
<span style="font-size:15px;">Advance World Holidays</span><br>
<span>PK & Sons Complex, East Moozhikkal</span><br>
<span>NH 212, Calicut, Kerala, India.</span><br>
<span>Pin 673571</span><br>
<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
<span>Fax: 0091 495 2732 400</span>
</td>
</tr>
</table>
<hr>
<br>
<h4><u>Staff Informations</u></h4>

<table width="100%">
<tr><td>
	<table width='100%'>
	<tr>
	<td width='75%'>
		<table width='100%'>
		<tr>
		<td width="200px">Staff Code</td>
		<td >:&nbsp;&nbsp;<?=$results->staff_code;?></td>
		</tr>
		<tr>
		<td >Joining Date </td>
		<td>:&nbsp;&nbsp;<?=$results->staff_startdate;?></td>
		</tr>
		<tr>
		<td >Staff Name </td>
		<td>:&nbsp;&nbsp;<?=$results->staff_name;?></td>
		</tr>
		</table>
	</td>
	<td>
	<img src='<?= $results->staff_image;?>' width='85px' height='90px'>
	</td>
	</tr>
	</table>
</td>
</tr>
<tr>
<td>
<table width='100%'>
	<tr>
	<td width="200px">Address </td>
	<td>:&nbsp;&nbsp;<?= $results->staff_address;?></td>
	</tr>

	<tr>
	<td >Profession</td>
	<td>:&nbsp;&nbsp;<?= $results->profession_name;?></td>
	</tr>

	<tr>
	<td >Date of Birth</td>
	<td>:&nbsp;&nbsp;<?= $results->staff_dob;?></td>
	</tr>

	<tr>
	<td >Gender </td>
	<td>:&nbsp;&nbsp;<?= $results->staff_gender;?></td>
	</tr>

	<tr>
	<td>Blood Group </td>
	<td>:&nbsp;&nbsp;<?= $results->staff_bloodgroup;?></td>
	</tr>
	
	<tr>
	<td >Mobile</td>
	<td>:&nbsp;&nbsp;<?= $results->staff_mobile;?></td>
	</tr>
	
	<tr>
	<td >Email</td>
	<td>:&nbsp;&nbsp;<?= $results->staff_email;?></td>
	</tr>
	
	<tr>
	<td >Basic Salary</td>
	<td>:&nbsp;&nbsp;<?= $results->staff_basicsalary;?></td>
	</tr>
	
	<tr>
	<td >Aadhar Card No</td>
	<td>:&nbsp;&nbsp;<?= $results->staff_aadharcard;?></td>
	</tr>
		
	<tr>
	<td >Pan Card No</td>
	<td>:&nbsp;&nbsp;<?= $results->staff_pancard;?></td>
	</tr>
	
	<tr>
	<td >Bank Name</td>
	<td>:&nbsp;&nbsp;<?= $results->staff_bankname;?></td>
	</tr>
	
	<tr>
	<td >Bank Account</td>
	<td>:&nbsp;&nbsp;<?= $results->staff_bankaccount;?></td>
	</tr>
	
	<tr>
	<td >Bank IFSC No</td>
	<td>:&nbsp;&nbsp;<?= $results->staff_ifsc;?></td>
	</tr>
	
</table>
</td>
</tr>
</table>
<hr>
</body>                             
                       
</html>