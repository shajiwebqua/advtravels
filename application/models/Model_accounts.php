<?php 
defined("BASEPATH") OR exit("'No direct script access allowed'");
   class Model_accounts extends CI_Model 
   {
      function __construct() { 
         parent::__construct(); 
      } 
	  
      public function insert($table,$data) 
      { 
        $this->db->insert($table, $data);
		return true;
      }
	  
	  public function insert_data($table,$data) 
      { 
        $this->db->insert($table, $data);
		return $this->db->insert_id();
      }
	  	  	  
	  public function update($table,$data,$where) 
      { 
	    $this->db->where($where);
        $this->db->update($table, $data);
		return true;
      }
	  
	  public function del($table,$where) 
      { 
	    $this->db->where($where);
        $this->db->delete($table);
		return $this->db->affected_rows();
      }
	  
	  public function del_investment1($id) 
      { 
	    $rw=$this->db->select('entry_accuid')->from('ac_account_entry')->where('entry_id',$id)->get()->row();
		$rwid=$rw->entry_accuid;
		$this->db->where('accu_id',$rwid);
        $this->db->delete('ac_accounts');
		return $this->db->affected_rows();
      }
	  
	  
	  public function del_investment2($id) 
      { 
	    $rw=$this->db->select('entry_accuid')->from('ac_account_entry')->where('entry_id',$id)->get()->row();
		$rwid=$rw->entry_accuid;
		$this->db->where('entry_accuid',$rwid);
		$this->db->delete('ac_account_entry');
		return $this->db->affected_rows();
      }
	    
	  
	  public function view_capitals() 
      { 
        $this->db->select('*');
		$this->db->from('ac_account_entry');
		$this->db->join('ac_accounts','ac_accounts.accu_id=ac_account_entry.entry_accuid','inner');
		$this->db->where('entry_ledgerid',4);
		$this->db->order_by('entry_id','asc');
		$qry=$this->db->get();
		return $qry->result();
      }
	  
	  
	public function ledger_wise_list($dt1,$dt2,$lid)
	{
	    $this->db->select('*');
		$this->db->from('ac_ledgers');
		$this->db->join('ac_account_entry','ac_account_entry.entry_ledgerid=ac_ledgers.ledg_id','inner');
		$where="(ac_account_entry.entry_date>='".$dt1."' and ac_account_entry.entry_date<='".$dt2."') and ac_account_entry.entry_ledgerid=".$lid;
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();	
	}
	 
	 public function group_list() 
      { 
        $this->db->select('*');
		$this->db->from('acc_groups');
		$this->db->join('acc_group_master','acc_groups.acc_grp_parentid=acc_group_master.acc_mgrp_id','left');
		$this->db->order_by('acc_grp_id','desc');
		$qry=$this->db->get();
		return $qry->result();
      }
	 
	 public function ledger_list() 
      { 
        $this->db->select('*');
		$this->db->from('ac_ledgers');
		$this->db->join('ac_groups','ac_groups.grp_id=ac_ledgers.ledg_group','inner');
		$this->db->order_by('ledg_id','desc');
		$qry=$this->db->get();
		return $qry->result();
      }
	  
//balance sheet-------------------------------------->
public function bsheet_profitloss_amount($sdt1,$edt1)
{
	$this->db->select("ac_ledgers.ledg_id,ac_ledgers.ledg_name,sum(ac_account_entry.entry_debit)as debit,sum(ac_account_entry.entry_credit)as credit")->from('ac_account_entry');
	$this->db->join('ac_ledgers','ac_ledgers.ledg_id=ac_account_entry.entry_ledgerid','inner');
	$where="(ac_ledgers.ledg_group=17 or ac_ledgers.ledg_group=18 or ac_ledgers.ledg_group=19 or ac_ledgers.ledg_group=20) and (ac_account_entry.entry_date>='".$sdt1."' and ac_account_entry.entry_date<='".$edt1."')";
	$this->db->where($where);
	$this->db->group_by('ac_ledgers.ledg_id,ac_ledgers.ledg_name');
	$this->db->order_by('ac_ledgers.ledg_id','asc');
	$query=$this->db->get();
	return $query->result();
}

public function bsheet_asset_values($sdt1,$edt1)
{
	$this->db->select("ac_ledgers.ledg_id,ac_ledgers.ledg_name,sum(ac_account_entry.entry_debit)as debit,sum(ac_account_entry.entry_credit)as credit")->from('ac_account_entry');
	$this->db->join('ac_ledgers','ac_ledgers.ledg_id=ac_account_entry.entry_ledgerid','inner');
	$where="(ac_ledgers.ledg_group=11 or ac_ledgers.ledg_group=15 or ac_ledgers.ledg_group=21 or ac_ledgers.ledg_group=23)  and (ac_account_entry.entry_date>='".$sdt1."' and ac_account_entry.entry_date<='".$edt1."')";
	$this->db->where($where);
	$this->db->group_by('ac_ledgers.ledg_id,ac_ledgers.ledg_name');
	$this->db->order_by('ac_ledgers.ledg_id','asc');
	$query=$this->db->get();
	return $query->result();
}

public function bsheet_liability_values($sdt1,$edt1)
{
	$this->db->select("ac_ledgers.ledg_id,ac_ledgers.ledg_name,sum(ac_account_entry.entry_debit)as debit,sum(ac_account_entry.entry_credit)as credit")->from('ac_account_entry');
	$this->db->join('ac_ledgers','ac_ledgers.ledg_id=ac_account_entry.entry_ledgerid','inner');
	$where="(ac_ledgers.ledg_group=12 or ac_ledgers.ledg_group=16 or ac_ledgers.ledg_group=24) and (ac_account_entry.entry_date>='".$sdt1."' and ac_account_entry.entry_date<='".$edt1."')";
	$this->db->where($where);
	$this->db->group_by('ac_ledgers.ledg_id,ac_ledgers.ledg_name');
	$this->db->order_by('ac_ledgers.ledg_id','asc');
	$query=$this->db->get();
	return $query->result();
}
//------------------------------------------------------------>

//trial Balance ----------------------------------------------->

public function Tbalance_ledger_values($sdt1,$edt1)
{
	$this->db->select("ac_ledgers.ledg_id,ac_ledgers.ledg_name,sum(ac_account_entry.entry_debit)as debit,sum(ac_account_entry.entry_credit)as credit")->from('ac_account_entry');
	$this->db->join('ac_ledgers','ac_ledgers.ledg_id=ac_account_entry.entry_ledgerid','inner');
	$where="(ac_account_entry.entry_date>='".$sdt1."' and  ac_account_entry.entry_date<='".$edt1."') and ac_ledgers.ledg_id<>2";
	$this->db->where($where);
	$this->db->group_by('ac_ledgers.ledg_id,ac_ledgers.ledg_name');
	$this->db->order_by('ac_ledgers.ledg_id','asc');
	$query=$this->db->get();
	return $query->result();
}

//--------------------------------------------------------------

//profit and loss accounts ------------------------------------->

public function profit_loss_incomes($sdt1,$edt1)
{
	$this->db->select("ac_ledgers.ledg_id,ac_ledgers.ledg_name,sum(ac_account_entry.entry_debit)as debit,sum(ac_account_entry.entry_credit)as credit")->from('ac_account_entry');
	$this->db->join('ac_ledgers','ac_ledgers.ledg_id=ac_account_entry.entry_ledgerid','inner');
	$where="( ac_ledgers.ledg_group=17 or ac_ledgers.ledg_group=19) and (ac_account_entry.entry_date>='".$sdt1."' and ac_account_entry.entry_date<='".$edt1."')";
	$this->db->where($where);
	$this->db->group_by('ac_ledgers.ledg_id,ac_ledgers.ledg_name');
	$this->db->order_by('ac_ledgers.ledg_id','asc');
	$query=$this->db->get();
	return $query->result();
}

public function profit_loss_expenses($sdt1,$edt1)
{
	$this->db->select("ac_ledgers.ledg_id,ac_ledgers.ledg_name,sum(ac_account_entry.entry_debit)as debit,sum(ac_account_entry.entry_credit)as credit")->from('ac_account_entry');
	$this->db->join('ac_ledgers','ac_ledgers.ledg_id=ac_account_entry.entry_ledgerid','inner');
	$where="(ac_ledgers.ledg_group=18 and ac_ledgers.ledg_id<>2)  and (ac_account_entry.entry_date>='".$sdt1."' and ac_account_entry.entry_date<='".$edt1."')";
	$this->db->where($where);
	$this->db->group_by('ac_ledgers.ledg_id,ac_ledgers.ledg_name');
	$this->db->order_by('ac_ledgers.ledg_id','asc');
	$query=$this->db->get();
	return $query->result();
}

public function profit_loss_expenses1($sdt1,$edt1) //indirect expenses
{
	$this->db->select("ac_ledgers.ledg_id,ac_ledgers.ledg_name,sum(ac_account_entry.entry_debit)as debit,sum(ac_account_entry.entry_credit)as credit")->from('ac_account_entry');
	$this->db->join('ac_ledgers','ac_ledgers.ledg_id=ac_account_entry.entry_ledgerid','inner');
	$where="(ac_ledgers.ledg_group=20)  and (ac_account_entry.entry_date>='".$sdt1."' and ac_account_entry.entry_date<='".$edt1."')";
	$this->db->where($where);
	$this->db->group_by('ac_ledgers.ledg_id,ac_ledgers.ledg_name');
	$this->db->order_by('ac_ledgers.ledg_id','asc');
	$query=$this->db->get();
	return $query->result();
}
 
public function view_account_details($ledgid) 
  { 
	$this->db->select('*');
	$this->db->from('ac_account_entry');
	$this->db->join('ac_accounts','ac_accounts.accu_id=ac_account_entry.entry_accuid','inner');
	$this->db->join('ac_ledgers','ac_ledgers.ledg_id=entry_ledgerid','inner');
	$this->db->where('entry_ledgerid',$ledgid);
	$this->db->order_by('entry_id','asc');
	$qry=$this->db->get();
	return $qry->result();
  }

  
  
//------------------------------------------------------------------------------
  	/*  
	  public function voucher_type_list() 
      { 
        $this->db->select('*');
		$this->db->from('acc_voucher_types');
		$this->db->join('acc_primary_vtypes','acc_primary_vtypes.acc_pvtypeid=acc_voucher_types.acc_vtype_no','inner');
		$this->db->order_by('acc_vtypeid','desc');
		$qry=$this->db->get();
		return $qry->result();
      }
		  
		  
	  public function payments_list($dt1,$dt2) 
      { 
        $this->db->select('*');
		$this->db->from('acc_trans_head');
		$this->db->join('acc_transactions','acc_transactions.acc_tra_headid=acc_trans_head.acc_trans_id','inner');
		$this->db->join('acc_ledgers','acc_ledgers.acc_led_id=acc_transactions.acc_tra_ledgerid','inner');
		$this->db->join('acc_voucher_types','acc_voucher_types.acc_vtypeid=acc_trans_head.acc_trans_type','inner');
		/*if($dt1!="" and $dt2!="")
		{
			$where="acc_pay_date>='".$dt1."'  and acc_pay_date<='".$dt2."'";
			$this->db->where($where);
		}*/
/*		$this->db->where('acc_trans_head.acc_trans_type=2');
		$this->db->order_by('acc_trans_head.acc_trans_id','desc');
		$qry=$this->db->get();
		return $qry->result();
      }

	  
	  public function all_trans_list($dt1,$dt2,$op,$vt) 
      { 
        $this->db->select('*');
		$this->db->from('acc_trans_head');
		$this->db->join('acc_transactions','acc_transactions.acc_tra_headid=acc_trans_head.acc_trans_id','inner');
		$this->db->join('acc_ledgers','acc_ledgers.acc_led_id=acc_transactions.acc_tra_ledgerid','inner');
		$this->db->join('acc_voucher_types','acc_voucher_types.acc_vtypeid=acc_trans_head.acc_trans_type','inner');
		if($op==1)
		{
			$where="acc_trans_head.acc_trans_date>='".$dt1."'  and acc_trans_head.acc_trans_date<='".$dt2."'";
			$this->db->where($where);
		}
		else if($op==2)
		{
			$this->db->where('acc_trans_head.acc_trans_type',$vt);
		}
		//$this->db->where('acc_trans_head.acc_trans_type=2');
		$this->db->order_by('acc_trans_head.acc_trans_id','desc');
		$qry=$this->db->get();
		return $qry->result();
      }
	  
	  public function all_trans_list2($op,$ldg) 
      { 
        $this->db->select('*');
		$this->db->from('acc_trans_head');
		$this->db->join('acc_transactions','acc_transactions.acc_tra_headid=acc_trans_head.acc_trans_id','inner');
		$this->db->join('acc_ledgers','acc_ledgers.acc_led_id=acc_transactions.acc_tra_ledgerid','inner');
		$this->db->join('acc_voucher_types','acc_voucher_types.acc_vtypeid=acc_trans_head.acc_trans_type','inner');
		if($op==1)
		{
			$this->db->where("acc_transactions.acc_tra_ledgerid",$ldg);
		}
		$this->db->order_by('acc_trans_head.acc_trans_id','desc');
		$qry=$this->db->get();
		return $qry->result();
      }
  
	  
	  public function balance_sheet_data1($dt1,$dt2,$op) 
      { 
        $this->db->select("SUM(acc_transactions.acc_tra_debit) as debit,acc_ledgers.acc_led_id,acc_groups.acc_grp_id,acc_ledgers.acc_led_description");
		$this->db->from('acc_trans_head');
		$this->db->join('acc_transactions','acc_transactions.acc_tra_headid=acc_trans_head.acc_trans_id','inner');
		$this->db->join('acc_ledgers','acc_ledgers.acc_led_id=acc_transactions.acc_tra_ledgerid','inner');
		$this->db->join('acc_groups','acc_ledgers.acc_led_groupid=acc_groups.acc_grp_id','inner');
		$this->db->join('acc_voucher_types','acc_voucher_types.acc_vtypeid=acc_trans_head.acc_trans_type','inner');
		if($op==2)
		{
			$where="acc_trans_head.acc_trans_date>='".$dt1."'  and acc_trans_head.acc_trans_date<='".$dt2."'";
			$this->db->where($where);
		}
		$this->db->where('acc_grp_id=6');
		$this->db->where('acc_trans_head.acc_trans_type=2');
		
		$this->db->group_by('acc_groups.acc_grp_id,acc_ledgers.acc_led_id,acc_ledgers.acc_led_description');
		$qry=$this->db->get();
		return $qry->result();
      }
	  	  
	  public function balance_sheet_data2($dt1,$dt2,$op) 
      { 
        $this->db->select("SUM(acc_transactions.acc_tra_credit) as credit,acc_ledgers.acc_led_id,acc_groups.acc_grp_id,acc_ledgers.acc_led_description");
		$this->db->from('acc_trans_head');
		$this->db->join('acc_transactions','acc_transactions.acc_tra_headid=acc_trans_head.acc_trans_id','inner');
		$this->db->join('acc_ledgers','acc_ledgers.acc_led_id=acc_transactions.acc_tra_ledgerid','inner');
		$this->db->join('acc_groups','acc_ledgers.acc_led_groupid=acc_groups.acc_grp_id','inner');
		$this->db->join('acc_voucher_types','acc_voucher_types.acc_vtypeid=acc_trans_head.acc_trans_type','inner');
		if($op==2)
		{
			$where="acc_trans_head.acc_trans_date>='".$dt1."'  and acc_trans_head.acc_trans_date<='".$dt2."'";
			$this->db->where($where);
		}
		$this->db->where('acc_grp_id!=6');
		$this->db->where('acc_trans_head.acc_trans_type!=2');
		
		$this->db->group_by('acc_groups.acc_grp_id,acc_ledgers.acc_led_id,acc_ledgers.acc_led_description');
		$qry=$this->db->get();
		return $qry->result();
      }
	  	  
	   public function diff_expense($dt1,$dt2,$op) 
       { 
       $this->db->select("SUM(acc_transactions.acc_tra_debit) as debit");
		//$this->db->select("*");
		$this->db->from('acc_transactions');
		$this->db->join('acc_ledgers','acc_ledgers.acc_led_id=acc_transactions.acc_tra_ledgerid','inner');
		$this->db->join('acc_groups','acc_ledgers.acc_led_groupid=acc_groups.acc_grp_id','inner');
		$this->db->join('acc_trans_head','acc_transactions.acc_tra_headid=acc_trans_head.acc_trans_id','inner');
		if($op==2)
		{
			$where="acc_trans_head.acc_trans_date>='".$dt1."'  and acc_trans_head.acc_trans_date<='".$dt2."'";
			$this->db->where($where);
		}
		$this->db->or_where('acc_groups.acc_grp_type','4');
		$qry=$this->db->get();
		return $qry->row()->debit;
      }
	    
	  public function diff_income($dt1,$dt2,$op) 
      { 
       $this->db->select("SUM(acc_transactions.acc_tra_credit) as credit");
		//$this->db->select("*");
		$this->db->from('acc_transactions');
		$this->db->join('acc_ledgers','acc_ledgers.acc_led_id=acc_transactions.acc_tra_ledgerid','inner');
		$this->db->join('acc_groups','acc_ledgers.acc_led_groupid=acc_groups.acc_grp_id','inner');
		$this->db->join('acc_trans_head','acc_transactions.acc_tra_headid=acc_trans_head.acc_trans_id','inner');
		if($op==2)
		{
			$where="(acc_trans_head.acc_trans_date>='".$dt1."'  and acc_trans_head.acc_trans_date<='".$dt2."')";
			$this->db->where($where);
		}
		$this->db->where('acc_groups.acc_grp_type','3');
		$this->db->or_where('acc_groups.acc_grp_type','1');
		
		$qry=$this->db->get();
		return $qry->row()->credit;
      }

	  
	  public function receipts_list($dt1,$dt2) 
      { 
        $this->db->select('*');
		$this->db->from('acc_receipts');
		$this->db->join('acc_ledgers','acc_ledgers.acc_ledgid=acc_receipts.acc_re_ledger','inner');
		if($dt1!="" and $dt2!="")
		{
			$where="acc_re_date>='".$dt1."'  and acc_re_date<='".$dt2."'";
			$this->db->where($where);
		}
		$this->db->order_by('acc_re_id','desc');
		$qry=$this->db->get();
		return $qry->result();
      }
	  	  
	  public function contra_list($dt1,$dt2) 
      { 
        $this->db->select('*');
		$this->db->from('acc_contra');
		//$this->db->join('acc_ledgers','acc_ledgers.acc_ledgid=acc_contra.acc_con_ledger','inner');
		if($dt1!="" and $dt2!="")
		{
			$where="acc_con_date>='".$dt1."'  and acc_con_date<='".$dt2."'";
			$this->db->where($where);
		}
		$this->db->order_by('acc_con_id','desc');
		$qry=$this->db->get();
		return $qry->result();
      }
	  	  
	  
	  public function journal_list($dt1,$dt2) 
      { 
        $this->db->select('*');
		$this->db->from('acc_journal');
		//$this->db->join('acc_ledgers','acc_ledgers.acc_ledgid=acc_journal.acc_jou_by','inner');
		if($dt1!="" and $dt2!="")
		{
			$where="acc_jou_date>='".$dt1."'  and acc_jou_date<='".$dt2."'";
			$this->db->where($where);
		}
		$this->db->order_by('acc_jou_id','desc');
		$qry=$this->db->get();
		return $qry->result();
      }
	  
// Proft and loss accounts

	  public function profit_direct_expense($dt1,$dt2,$op) 
      { 
        $this->db->select("SUM(acc_transactions.acc_tra_debit) as debit,acc_trans_head.acc_trans_id,acc_trans_head.acc_trans_description,acc_ledgers.acc_led_description");
		//$this->db->select("*");
		$this->db->from('acc_transactions');
		$this->db->join('acc_trans_head','acc_transactions.acc_tra_headid=acc_trans_head.acc_trans_id','inner');
		$this->db->join('acc_ledgers','acc_ledgers.acc_led_id=acc_transactions.acc_tra_ledgerid','inner');
		$this->db->join('acc_groups','acc_ledgers.acc_led_groupid=acc_groups.acc_grp_id','inner');
		//$this->db->join('acc_voucher_types','acc_voucher_types.acc_vtypeid=acc_trans_head.acc_trans_type','inner');
		if($op==1)
		{
			$where="acc_trans_head.acc_trans_date>='".$dt1."'  and acc_trans_head.acc_trans_date<='".$dt2."'";
			$this->db->where($where);
		}
		
		$this->db->where('acc_groups.acc_grp_type=4');
		$this->db->group_by('acc_trans_head.acc_trans_id,acc_trans_head.acc_trans_description,acc_ledgers.acc_led_description');
		$qry=$this->db->get();
		return $qry->result();
      }
	  
	  public function profit_direct_income($dt1,$dt2,$op) 
      { 
        $this->db->select("SUM(acc_transactions.acc_tra_credit) as credit,acc_trans_head.acc_trans_id,acc_trans_head.acc_trans_description,acc_ledgers.acc_led_description");
		//$this->db->select("*");
		$this->db->from('acc_transactions');
		$this->db->join('acc_trans_head','acc_transactions.acc_tra_headid=acc_trans_head.acc_trans_id','inner');
		$this->db->join('acc_ledgers','acc_ledgers.acc_led_id=acc_transactions.acc_tra_ledgerid','inner');
		$this->db->join('acc_groups','acc_ledgers.acc_led_groupid=acc_groups.acc_grp_id','inner');
		//$this->db->join('acc_voucher_types','acc_voucher_types.acc_vtypeid=acc_trans_head.acc_trans_type','inner');
		if($op==1)
		{
			$where="acc_trans_head.acc_trans_date>='".$dt1."'  and acc_trans_head.acc_trans_date<='".$dt2."'";
			$this->db->where($where);
		}
		
		$this->db->where('acc_groups.acc_grp_type=3');
		$this->db->group_by('acc_trans_head.acc_trans_id,acc_trans_head.acc_trans_description,acc_ledgers.acc_led_description');
		$qry=$this->db->get();
		return $qry->result();
      }
	  
	  
	  
	  
	  
	  public function cashbook_list($dt1,$dt2,$op) 
      { 
        $this->db->select("*");
		$this->db->from('acc_cashbook');
		$this->db->join('acc_trans_head','acc_cashbook_headid=acc_trans_head.acc_trans_id','inner');
		if($op==1)
		{
			$where="acc_trans_head.acc_trans_date>='".$dt1."'  and acc_trans_head.acc_trans_date<='".$dt2."'";
			$this->db->where($where);
		}
		$qry=$this->db->get();
		return $qry->result();
      }  
	  
	  */

   } 
?> 