<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Staff Logs List</b></h1>
  <ol class="breadcrumb" style="font-size:15px;">
  <li><a id='view-pdf' href="<?php echo site_url('Pdf/pdf_log') ?>" style='color:#4b88ed;' ><i class="fa fa-print" aria-hidden="true"></i>Print</a></li> 
  <li><a id='idsave' href="<?php echo site_url('Pdf/pdf_log') ?>" style='color:#4b88ed;' target="_blank"><i class="fa fa-save" aria-hidden="true"></i>Save</a></li>
  <li><a id='idshare' href="" data-toggle='modal' style='color:#4b88ed;' ><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a></li>
  </ol>
  
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->   
    
  <section class="content"> 
  <!-- <div style="color:#4b88ed;font-size:12px;"> Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , And click on the button (Print,Share,Edit,View,Delete)  </div> -->
    <div style="padding:2px 0px 2px 0px;">
		 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
		 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div></center>
		 </div>
	</div>
	
	<!-- search options -------------------------------------------->
				<div class="row">
                 <div class="col-md-1" style="border-right:1px solid #cecece">
				 <h4  style="color:blue; margin-top:3px">Search </h4> 
				 </div>

				 <div class="col-md-7">
                    <div class="form-group">
						<label class="col-md-2 control-label" style="padding-top:3px;" >Select Date : </label>
						<div class="col-md-3">
								<input type="text" class="form-control date-pick"  id="datepicker1" value="----">
						</div>
						<div class="col-md-3 ">
								<input type="text" class="form-control date-pick"  id="datepicker2" value="----">
						</div>
						
						<div class="col-md-2">
							<input class="form-control btn btn-primary" style="text-align:center;" type="button" value="Get" id="btnget">
						</div>
						
					</div>
				</div> 

				
			<!--<div class="col-md-4" style="border-left:2px solid #e4e4e4;">
					<div class="form-group" >
					<label class="col-md-4  control-label col-md-offset-1" style="padding-top:3px;text-align:right;">Select Month : </label>
					<div class="col-md-6">
					<select class="form-control" id="lmonth" name="lmonth1">
					<option value="0">-----</option>
					<option value="1">January</option>
					<option value="2">February</option>
					<option value="3">March</option>
					<option value="4">April</option>
					<option value="5">May</option>
					<option value="6">June</option>
					<option value="7">July</option>
					<option value="8">August</option>
					<option value="9">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
					</select>
					</div> 
					<!--<div class="col-md-2">
						<input class="form-control" style="text-align:left;" type="button" value="All" id="btnall">
					</div> -->
			<!--	</div>
				    </div> -->
					
                           <label style="background-color:#cecece;width:100%;height:1px;"></label>
				</div>
			
	
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->

<div class="portlet-body">
<div style="background-color:#fff;padding:15px; ">
              
		<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                        
        <!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
        
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
					<div class="table-toolbar">
					<div class="row">
					<div class="col-md-12">
                  
                   </div>
                   </div>
                   </div>
                
            <table class="table table-striped table-hover table-bordered" id="example">
              <thead>
                <tr>
                 <th>ID</th>
                 <th>Staff ID</th> 
                 <th>Staff Name</th>
                 <th>Date</th>
                 <th>Time</th>
                 <th>Entry ID</th>
                 <th>Description</th>
                </tr>
                </thead>
            </table>
        </div>
                       <!-- END BORDERED TABLE PORTLET-->
		</div>
		</div>
                <!-- END CONTENT BODY -->
	</div>
	</div>
            <!-- END QUICK SIDEBAR -->
</div>
  
<div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Add</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
                   </div> 

	   <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Add</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
                   </div> 

                     <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div> 

                    <div class="modal fade draggable-modal" id="myModalsh" tabindex="-1" role="basic" aria- hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-body" >
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Share</h4>
                                </div>
                            </div>
                            </div>
                        <!-- /.modal-dialog -->
                        </div>
                   </div> 
                    
      
  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
<!-- /.content --> 
</section>
  <!-- /.content-wrapper -->
</div>
 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

setInterval(function(){ $('#idh5').html(''); }, 3000);

 $("#idmsg").hide();

//sweet alert ----------------------------
	if($("#idh5").html()!="")
	{
		swal( $("#idh5").html(),"","success")
		$("#idh5").html("");
	}
// sweet alert box -------------------------- 

 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
   
 $(document).ready(function()
  {
		  
    $('#example').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax":{
			url:"../assets/logdata_processing.php",
			data:{date1:"",date2:""},
		}
    });
} );
  
  
$("#btnget").click(function(){
      var dt1=$('#datepicker1').val();
	  var dt2=$('#datepicker2').val();
	  
      if(dt1=="----"||dt1=="" || dt2=="----" || dt2=="")
      {
        alert('Please Select Dates');
      }
	  else
      {
			$('#example').dataTable().fnDestroy();
			$('#example').dataTable({
				"processing": true,
				"serverSide": true,
				"ajax":{
					url:"../assets/logdata_processing.php",
					data:{date1:dt1,date2:dt2},
				}
			});
	  
	  }
		
  });
     
  
  
  
  
 /*
  $('#example').dataTable({
    "ordering":false,
         destroy: true,
        "processing": true,

        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Log/log_ajax/1",// json datasource
               },
  
    "columns": [
      { "data": "id" },
      { "data": "staffid" },
      { "data": "name" },
      { "data": "date" },
      { "data": "time"},
      { "data": "entry"},
      { "data": "description"},
      
        ]
  } );
  */
  
 // var table = $('#example').DataTable();
 //     $('#example tbody').on( 'click', 'tr', function () {
 //        if ( $(this).hasClass('selected') ) {
 //            $(this).removeClass('selected');
 //        }
 //        else {
 //          table.$('tr.selected').removeClass('selected');
 //            $(this).addClass('selected');
 //        }
 //    } );


 $('#view-pdf').on('click',function(){
       //var id=$('#example').find('tr.selected').find('td').eq(2).text();
          var dt1=$('#datepicker1').val();
          var m=$('#lmonth').val();
       var pdf_link = $(this).attr('href')+'/'+m+'/'+dt1;
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        $.createModal({
        title:'Logs Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
      return false; 
    
}); 
  
  
   
  
  $("#lmonth").change(function(){
	   
      var m=$('#lmonth').val();
	  $('#example').dataTable({
		"ordering":false,
         destroy: true,
        "processing": true,

        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Log/log_ajax/3/"+m,// json datasource
               },
  
      "columns": [
      { "data": "id" },
      { "data": "staffid" },
      { "data": "name" },
      { "data": "date" },
      { "data": "time"},
      { "data": "entry"},
      { "data": "description"},
       ]
  } );
  });
  

    $('#idshare').click(function ()
  { 
    var Result=$("#myModalsh .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         //var id =  $(this).attr('id');
     var id=$('#example').find('tr.selected').find('td').eq(0).text();
     var dt1=$('#datepicker1').val();
     var m=$('#lmonth').val();
     //alert(id);
     // if(id=="")
     // {
     //   alert ("please Select customer details..");
     // }
     // else
     // {
	  $('#idshare').attr('data-target','#myModalsh'); 
      jQuery.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "Log/share_log",
      dataType: 'html',
      data:{dt1:dt1,m:m},
      success: function(res) {
      Result.html(res);
       }
      });
     //}
        }); 



    $('#idsave').on('click',function(){
       //var id=$('#example').find('tr.selected').find('td').eq(2).text();
          var dt1=$('#datepicker1').val();
          var m=$('#lmonth').val();
         
       // alert ("please Select staff details..");
       var pdf_link = $(this).attr('href')+'/'+m+'/'+dt1;
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
    //     $.createModal({
    //     title:'Logs Details',
    //     message: iframe,
    //     closeButton:true,
    //     scrollable: true,
    // });
    window.open(pdf_link);
      return false; 
    
}); 



    // $('#idsave').click(function ()
    // {   
    //     //var Result=$("#myModalsa .modal-body");
    //  //   $(this).parent().parent().toggleClass('selected');
    //      //var id =  $(this).attr('id');
    //      var id=$('#example').find('tr.selected').find('td').eq(0).text();
    //  //alert(id);
    //      // if(id=="")
    //      // {
    //      //     alert ("please Select customer details..");
    //      // }
    //      // else
    //      // {
    //         jQuery.ajax({
    //         type: "POST",
    //         url: "<?php echo base_url(); ?>" + "Customer/save_customer",
    //         dataType: 'html',
    //         data:{uid:id},
    //         success: function(res) {
    //         Result.html(res);
    //          }
    //         });
    //      //}
    //     }); 
  
  
    $('#idview').click(function () 
    {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
        //var id =  $(this).attr('id');
        var id=$('#example').find('tr.selected').find('td').eq(0).text();
    alert(id);
    if(id=="")
    {
       alert ("please Select customer details..");
    }
    else
    {
		$('#idview').attr('data-target','#myModal2'); 
		
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Customer/view_customer",
        dataType: 'html',
        data: {uid: id},
        success: function(res) {
        Result.html(res);
            }
          });
    }
        }); 
    

    
    
// -----------------------------------------------------------------------------------------------------------------------------------    
    
     /* $(document).on("click", "#iddelete", function () {
            return confirm('Are you sure you want to delete this entry?');
        });*/




</script>



