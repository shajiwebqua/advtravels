<?php
include('application/views/common/acc_header.php');
?>
  <style>
  .dashbtn
  {
	  width:75px;
	  height:75px;
	  background-color:#f4f4f4;
	  border:1px solid #fff;
	  /*border-radius:10px;*/
	  padding:5px;
	  text-align:center;
  }
  
.sh4
{
 border-bottom:1px solid #e4e4e4;
 width:100%;
 color:#00adee;
 padding-bottom:5px;
 margin-top:0px;"
}

 <link rel="stylesheet" href="<?php echo base_url('assets/chart/css/material-charts.css');?>">
 <script src="<?php echo base_url('assets/chart/js/material-charts.js');?>"></script>

  </style>
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#00adee'> Dashboard 
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
     <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
		
		<div class='row' style="padding:3px 20px 3px 20px;" >
			<div class='col-md-3'>
				  <div class="info-box">
				  <!-- Apply any bg-* class to to the icon to color it -->
				  <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
				  <div class="info-box-content">
					<span class="info-box-text">Happy Customers</span>
					<?php 
					$hcust=$this->db->select('COUNT(*) as hccount')->from('customers')->get()->row();
					?>
					<span class="info-box-number"><?php echo $hcust->hccount;?></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>
			
			
			<div class='col-md-3'>
				  <div class="info-box">
				  <!-- Apply any bg-* class to to the icon to color it -->
				  <span class="info-box-icon bg-aqua"><i class="fa fa-taxi"></i></span>
				  <div class="info-box-content">
					<span class="info-box-text">Running Vehicles</span>
					<?php 
					$rveh=$this->db->select('COUNT(*) as runveh')->from('trip_management')->where('trip_status=1')->where('trip_startdate<=',date('Y-m-d'))->get()->row();
					?>
					<span class="info-box-number"><?php echo $rveh->runveh;?></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>
			<div class='col-md-3'>
				  <div class="info-box">
				  <!-- Apply any bg-* class to to the icon to color it -->
				  <span class="info-box-icon bg-aqua"><i class="fa fa-wrench"></i></span>
				  <div class="info-box-content">
					<span class="info-box-text">Vehicles for Service</span>
					<?php
						//$this->load->model('Model_vehicle');
						//$results=$this->Model_vehicle->view_PSvehicles();
						
						
						$this->db->select('*');
						$this->db->from('services');
						$this->db->join('vehicle_registration','vehicle_registration.vehicle_id=services.ser_vehicle_id','inner');
						$this->db->join('vehicle_types','vehicle_registration.vehicle_type=vehicle_types.veh_type_id','inner');
						$this->db->where("((services.ser_kilometer2-services.ser_kilometer1)>=10000)");
						$this->db->where('services.ser_status=1');
						$this->db->order_by('services.ser_id asc');
						$query = $this->db->get();
						$results=$query->result();
						
						$vs=count($results);
					?>
					
					<span class="info-box-number"><?php echo $vs ;?></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>
			
		<div class='col-md-3'>
				  <div class="info-box">
				  <!-- Apply any bg-* class to to the icon to color it -->
				  <span class="info-box-icon bg-aqua"><i class="fa fa-cab"></i></span>
				  <div class="info-box-content">
					<span class="info-box-text">Upcoming Trips</span>
					<?php 
					//$sta=$this->db->select('COUNT(*) as stano')->from('staffregistration')->where('staff_status=1')->get()->row();
					$upveh=$this->db->select('COUNT(*) as upcveh')->from('trip_management')->where('trip_status=1')->where('trip_startdate>=',date('Y-m-d'))->get()->row();
					?>
					<span class="info-box-number"><?php echo $upveh->upcveh;?></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>
		</div>

        <!-- Main content -->
    <section class="content">
		
		
	<!-- for chart data------------------->
		<input type='hidden' name='tkm' id='tkm' value='<?php echo $this->session->userdata('tkm');?>'>
		<input type='hidden' name='drna' id='drna' value='<?php echo $this->session->userdata('drna');?>'>
		<input type='hidden' name='tkm1' id='tkm1' value='<?php echo $this->session->userdata('tkm1');?>'>
		<input type='hidden' name='dr_id' id='dr_id' value='<?php echo $this->session->userdata('drvid');?>'>
		<input type='hidden' name='dr_id1' id='dr_id1' value='<?php echo $this->session->userdata('drvid1');?>'>
		
	<!-- for chart data------------------->	
			
		
	<!------  BAR CHART ---------------------------------------------------------------------->
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header">
		<div class='col-md-6'>
          <h3 class="box-title"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Driver's Performance</h3>
		  </div>
		  <div class='col-md-6' style='text-align:right'>
		   <form action="#" method="post">
            <div class="input-group">
             <label name="message" class="control-form" ></label>
              <span class="input-group-btn">
                <a href="<?php echo site_url('Driver/performance');?>"><button type="button" class="btn btn-primary btn-flat">More</button></a>
              </span>
            </div>
          </form>
		  </div>
        </div><!-- /.box-header -->
		
        <div class="box-body">
			
		
		 <div class='row' style='padding:15px;'>
		 <div class='col-md-9' style='border-right:1px solid #e4e4e4;'>

            <div class="box-header with-border" style='padding-top:0px;'>
              <h3 class="box-title" style='font-size:14px;'>Performance Chart</h3>
            </div> 

              <div class="chart">
                <canvas id="barChart" style="height:230px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
		  <div class='col-md-3' style='margin-top:10px;'>
		  
		  <table height='100%' width='100%' style='text-align:center;' >
		  <tr><td colspan='3' height='25px;' style='text-align:left;border-bottom:1px solid #e4e4e4;'>Performar of the Month</td></tr>
		   <tr><td colspan='3'height="10px;" ></td></tr>
		  <tr><td width="50px" style='padding:5px;'><div style='width:20px;height:20px;background-color:#00a65a;'></div></td><td colspan='2' style='text-align:left;'><b><?php echo date('F') . " - ". date('Y');?> </b></tr>
		  
		   <?php 
			$did1=$this->session->userdata('drvid1');
			$drrow1=$this->db->select('*')->from('driverregistration')->where('driver_id',$did1)->get()->row(); 
			if($drrow1)
			{
				$drimg1=$drrow1->driver_image;
				$drname1=$drrow1->driver_name;
			}
			else
			{
				$drimg1=base_url('upload/images/placeholder.png');
				$drname1="....................";
			}
		  ?>		  
		  
		  <tr><td></td><td width="50px" style='padding:5px;'><img src='<?php echo $drimg1;?>' width='50px' height='55px'> </div></td><td style='text-align:left;'> <?php  echo strtoupper($drname1); ?></tr>
		  <tr><td colspan='3'height="10px;" ></td></tr>
		  <tr><td width="50px" style='padding:5px;'><div style='width:20px;height:20px;background-color:#d2d6de;'></div></td><td colspan='2' style='text-align:left;'><b><?php echo date('F',strtotime("-1 month"))." - ". date('Y',strtotime("-1 month"));?></b></tr>
		  		  
		 <?php 
			$did=$this->session->userdata('drvid');
			$drrow=$this->db->select('*')->from('driverregistration')->where('driver_id',$did)->get()->row(); 
			if($drrow)
			{
				$drimg2=$drrow->driver_image;
				$drname2=$drrow->driver_name;
			}
			else
			{
				$drimg2=base_url('upload/images/placeholder.png');
				$drname2="....................";
			}
		  ?>	

	 
		  <tr><td></td><td width="50px" style='padding:5px;'><img src='<?php echo $drimg2;?>' width='50px' height='55px'> </div></td><td style='text-align:left;'> <?php  echo strtoupper($drname2); ?></tr>
		    
		  
		  </table>
		  	
		  
		  <!--<table height='100%' width='100%' style='text-align:center;'>
		  <tr><td><img src='<?php echo site_url('/images/shaji.jpg');?>' width='120px' height='130px;'</td></tr>
		  <tr><td>&nbsp;</td></tr>
		  <tr style='height:25px;'><td>CHANEESH K</td></tr>
		  <tr style='height:25px;'><td><i class="fa fa-mobile fa-lg" aria-hidden="true"></i> : 9847556789</td></tr>
		  <tr ><td>White House, West Nadakkavu, Vandipetta, Kozikode</td></tr>
		  </table> -->
		  </div>

			</div>
			</div>
        </div><!-- /.box-body -->
	</div>

	<!--------CHART END ---------------------------------------------------------------------->
	
		
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header">
		<div class='col-md-6'>
          <h3 class="box-title"><i class="fa fa-taxi" aria-hidden="true"></i>&nbsp;Running Vehicle Details</h3>
		  </div>
		  <div class='col-md-6' style='text-align:right'>
		   <form action="#" method="post">
            <div class="input-group">
             <label name="message" class="control-form" ></label>
              <span class="input-group-btn">
                <a href="<?php echo site_url('Trip/view_trip');?>"><button type="button" class="btn btn-primary btn-flat">More</button></a>
              </span>
            </div>
          </form>
		  </div>
		  
		  
        </div><!-- /.box-header -->
		
        <div class="box-body">
          <!-- Conversations are loaded here -->
         <!--<div class="direct-chat-messages">-->
		 <div class='row' style='padding:15px;'>
		 <div class='col-md-12'>
  		 <table class="table table-striped table-hover table-bordered" id="example1" width="100%" >
                    <thead>
						<tr>
						<td>Trip-ID</td>
						<td>Customer</td>
						<td>Driver</td>
						<td>Date(From-To)</td>
						<td>Trip(From-To)</td>
						<td>Status</td>
						</tr>
						</thead>
                    </table>
			</div>
			</div>
          <!--</div> --><!--/.direct-chat-messages-->
          
        </div><!-- /.box-body -->
       
	</div>
	</div>
	
	
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header">
		<div class='col-md-6'>
          <h3 class="box-title"><i class="fa fa-taxi" aria-hidden="true"></i>&nbsp;Upcoming Trip Details</h3>
		</div>
		
		<div class='col-md-6' style='text-align:right'>
           <form action="#" method="post">
            <div class="input-group">
             <label name="message" class="control-form" ></label>
              <span class="input-group-btn">
                <a href="<?php echo site_url('Trip/upcoming_trip');?>"><button type="button" class="btn btn-primary btn-flat">More</button></a>
              </span>
            </div>
          </form>
		</div>
		
        </div><!-- /.box-header -->
		
        <div class="box-body">
          <!-- Conversations are loaded here -->
         <!--<div class="direct-chat-messages">-->
		 <div class='row' style='padding:15px;'>
		 <div class='col-md-12'>
  		 <table class="table table-striped table-hover table-bordered" id="upcoming" width="100%" >
                    <thead>
						<tr>
						<td>Trip-ID</td>
						<td>Customer</td>
						<td>Driver</td>
						<td>Date(From-To)</td>
						<td>Trip(From-To)</td>
						<td>Status</td>
						</tr>
					</thead>
                    </table>
			</div>
			</div>
          <!--</div> --><!--/.direct-chat-messages-->
          
        </div><!-- /.box-body -->
        
	</div>
	</div>
	
	
		<div class='row'>
		<div class='col-md-6'>
		
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header">
		<div class='col-md-6'>
          <h3 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;Service Required Vehicles</h3>
		  </div>
		  <div class='col-md-6' style='text-align:right'>
		  <form action="#" method="post">
            <div class="input-group">
              <span class="input-group-btn">
                  <a href="<?php echo site_url('Vehicle/service_vehicles');?>"><button type="button" class="btn btn-primary btn-flat">More</button></a>
              </span>
            </div>
          </form>
		  </div>
        </div><!-- /.box-header -->
	    <div class="box-body">
        
				<table class="table table-striped table-hover table-bordered" id="example" width="100%" >
                        <thead>
								<tr>
								 <td>ID</td>
								 <td>Register No</td>
								 <td>Vehicle Type</td>
								 <td>Killometers</td>
								 <td>Status</td>
								 </tr>
								</thead>
                </table>
		
        </div><!-- /.box-body -->
      
	</div>
	
	</div>
	<div class='col-md-6'>
	
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header">
		<div class='col-md-6'>
          <h3 class="box-title"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;Servicing Vehicles</h3>
		  </div>
		  <div class='col-md-6' style='text-align:right'>
		  <form action="#" method="post">
            <div class="input-group">
                  <span class="input-group-btn">
                  <a href="<?php echo site_url('Vehicle/servicing_vehicles');?>"><button type="button" class="btn btn-primary btn-flat">More</button></a>
              </span>
            </div>
          </form>
		  </div>
        </div><!-- /.box-header -->
		
        <div class="box-body">
        	<table class="table table-striped table-hover table-bordered" id="example2" width="100%" >
                    <thead>
						<tr>
						 <td>ID</td>
						 <td>Register No</td>
						 <td>Vehicle Type</td>
						 <td>Killometers</td>
						 <td>Status</td>
						 </tr>
					</thead>
                </table>
		
        </div><!-- /.box-body -->

	  </div>
	
	</div>
	</div>
	
	
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header">
          <h3 class="box-title">+</h3>
        </div><!-- /.box-header -->
		
        <div class="box-body">
          <!-- Conversations are loaded here -->
         <!--<div class="direct-chat-messages">-->
		 <div class='row' style='padding:15px;'>
		 <div class='col-md-12'>
  		
		
			</div>
			</div>
          <!--</div> --><!--/.direct-chat-messages-->
          
        </div><!-- /.box-body -->
	</div>
	</div>
	
</section>
    <!-- content wrapper -->
</div>
     <?php
  include('application/views/common/acc_footer.php');
  ?>
 
  
</body>
</html>

  <script type="text/javascript">
  $(document).ready(function()
  {
	  
	  $(".fset").mouseover(function()
	  {
	  $(this).css("background-color","#aeaeae");
	  });
	  	  
	  $(".fset").mouseleave(function()
	  {
	  $(this).css("background-color","#d2d3d4");
	  });
	  
	  
	  
	  $('#example').dataTable( {
		 "ordering":false,
		 "bInfo" : false,
		 "destroy": true,
        "processing": true,
		"pageLength": 5,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Vehicle/periodic_service_ajax/1",// json datasource
                },
			 			   
        "columns": [
            { "data": "sid"},
            { "data": "regno" },
			{ "data": "vtype" },
			{ "data": "kilometer" },
			{ "data": "status" },
      ]
  } );
	  

  $('#example2').dataTable( {
		 "ordering":false,
		 "bInfo" : false,
		 "destroy": true,
        "processing": true,
		"pageLength": 5,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Vehicle/periodic_servicing_ajax",// json datasource
                },
			 			   
        "columns": [
            { "data": "sid"},
            { "data": "regno" },
			{ "data": "vtype" },
			{ "data": "kilometer" },
			{ "data": "status" },
      ]
  } );
  
  
	  
$('#example1').dataTable( {
		"ordering":false,
		"bInfo" : false,
        "destroy": true,
        "processing": true,
		"pageLength": 5,
		  // "ajax": "<?php echo site_url('report/course_fee');?>",
      
        "ajax": {
                url :"<?php echo base_url(); ?>" + "trip/starter_trip_ajax",// json datasource
                },
"columnDefs":[
{"width":"20%","targets":4},
],			 			   
        "columns": [
            { "data": "tid"},
            { "data": "customer" },
			{ "data": "driver" },
			//{ "data": "dmobile" },
			{ "data": "date" },
			{ "data": "fromto" },
			{ "data": "status" },
			
      ]
  } );
	  
	  
	$('#upcoming').dataTable( {
		"ordering":false,
		"bInfo" : false,
        "destroy": true,
        "processing": true,
		"pageLength": 5,
		  // "ajax": "<?php echo site_url('report/course_fee');?>",
      
        "ajax": {
                url :"<?php echo base_url(); ?>" + "trip/upcoming_trip_ajax",// json datasource
                },
"columnDefs":[
{"width":"20%","targets":4},
],			 			   
        "columns": [
            { "data": "tid"},
            { "data": "customer" },
			{ "data": "driver" },
			//{ "data": "dmobile" },
			{ "data": "date" },
			{ "data": "fromto" },
			{ "data": "status" },
			
      ]
  } );
	    
  });
  </script>
 <script>
 $(document).ready(function()
  {


  });
  
</script>
<script type='text/javascript'>



  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- BAR CHART -
    //--------------
 
   var d11=$("#tkm").val();
   var d2=d11.split(",");
   
   var d12=$("#tkm1").val();
   var d21=d12.split(",");
      
   var dr_na=$("#drna").val();
   var lbl=dr_na.split(",");

   var a1=["January", "February", "March", "April", "May", "June", "July"];

    var areaChartData = {
		
      labels: lbl,
	  
      datasets: [
        {
          label: "<?php echo date('F',strtotime("-1 month"));?>",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: d2
		  },
		  
        {
          label: "<?php echo date('F');?>",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          //data: [28, 48, 40, 19, 36, 27, 20]
		  data: d21
        }
      ]
    };
	
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

	
    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
  });
</script>
</body>
</html>

  
  