<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
 <!-- Main content -->
 <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>View Vehicles</b> </h1>
    <ol class="breadcrumb">
	<li><a href="<?php echo base_url('Attached_vehicle/get_add_vehicle');?>" data-target="#myModal2" data-toggle='modal' ><input type='button' class='btn btn-primary' value='Add New Vehicle'></a></li>
<!--<li><a href="<?php echo base_url('Attached_vehicle/get_add_vehicle');?>" data-target="#myModal2" data-toggle='modal' style="color:blue;"><h5><b>Add New Vehicle</b></h5></a></li>-->
	</ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->	  
 
	  
	<section class="content"> 
    <!-- Small boxes (Stat box) -->
	<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
			
    <div class="row">
	<div class="col-md-12">

	<!------------------------------------ Add new user details ----------------------------------->
	
	<div style="background-color:#fff;padding:15px; ">
	<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
                   <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="example">
							<thead>
								<tr>
								 <th>Edit</th>
								 <th>Delete</th>
								 <th>Date</th>
								 <th>Vehicle No</th>
								 <th>Owner Name</th>
								 <th>Type</th>
								 <th>Pay Period</th>
                                 <th>Amount</th>
								 <th>Status</th>
								</tr>
							</thead>
                        </table>
		

						
                        </div>
                        <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
          <!-- END CONTENT BODY -->
          </div>
          </div>
            <!-- END QUICK SIDEBAR -->
        </div>
			<div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body" >
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Edit</h4>
						</div>
					</div>
					</div>
				<!-- /.modal-dialog -->
				</div>
            </div>							
	</div>
	<!------------------------------------ End user details ----------------------------------->
	</div>
	</div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
    <!-- /.content --> 
</div>
</section>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------
 
      $('#example').dataTable( {
        destroy: true,
        "processing": true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Attached_vehicle/vehicle_ajax",// json datasource
               
               },
		"columnDefs":[
		{"width":"8%","targets":0},
		{"width":"8%","targets":1},
		{"width":"8%","targets":2},
		],
			   
        "columns": [
           
            { "data": "edit"},
		        { "data": "delete" },
            {  "data": "date"},
            { "data": "vehicleno"},
            { "data": "ownername" },
			      { "data": "type" },
            { "data": "payperiod" },
            { "data": "amount" },
            { "data": "status" }
       ]
  } );



      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
         //alert(id);
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Attached_vehicle/edit_vehicle",
        dataType: 'html',
        data: {vid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 

		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });



 //});
  


</script>



