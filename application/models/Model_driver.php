<?php
class Model_driver extends CI_Model{
	
    public function form_insert($userdt)
    {
       $this->db->insert('driverregistration', $userdt);  
    }

    public function view_driver(){

    $this->db->select('*');
    $this->db->from('driverregistration');
	$this->db->order_by('driver_id','desc');
    $query = $this->db->get();
    return $query->result();
}

 public function update_driver($id,$userdt)
  {
    $this->db->where('driver_id', $id);
    $this->db->update('driverregistration',$userdt); 
  } 

  public function delete_driver($id)
{
    $this->db->where('driver_id', $id);
    $this->db->delete('driverregistration');
    return $this->db->affected_rows();
}

public function select($table, $where,$column = "*"){
  $this->db->select($column);
  $this->db->from($table);
  $this->db->where($where);
  $query = $this->db->get();
  return $query->result();
} 

public function display_driver_account($table, $where,$column = "*")
{
  $this->db->select($column);
  $this->db->from($table);
  $this->db->where($where);
  $this->db->order_by("driver_acc_id","desc");
  $query = $this->db->get();
  return $query->result();
} 



public function select_cl($table, $column){
  $this->db->select($column);
  $this->db->from($table);
  $query = $this->db->get();
  return $query->result();
}

public function count_charges($where){
  $this->db->select("SUM(debit) AS debit,SUM(credit) AS credit");
  $this->db->from("driver_account");
  $this->db->where($where);
  $query = $this->db->get();
  return $query->result();
}

public function insert($table, $values){
  $this->db->insert($table, $values);
} 

public function update($table,$values,$where)
  {
    $this->db->where($where);
    $this->db->update($table,$values); 
  }

public function drivercharge($value){
  $this->db->select("SUM(driver_trip_charge) as drivertrip_charge");
  $this->db->where("
    trip_id IN (SELECT trip_id 
                FROM trip_management 
                WHERE  trip_status = '2' 
                AND trip_vehicle_id IN (SELECT vehicle_id  
                                        FROM vehicle_registration 
                                        where vehicle_regno = '". $value ."'))");
  $query = $this->db->get("driver_trip_list");
  
  if($query->num_rows() > 0){
     $result = $query->result();
     return  $result[0]->drivertrip_charge;
  }
  else{ return FALSE; }
}

}
?>