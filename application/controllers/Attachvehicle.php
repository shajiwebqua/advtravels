<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attachvehicle extends CI_Controller {

function __construct(){
parent::__construct();
	$user = $this->session->userdata('userid');
	   if (!isset($user))
	   { 
			redirect('/');
	   } 
	   else
	   { 
		   return true;
	   }
	   
$this->load->helper(array('form', 'url'));
$this->load->model('Model_attach');
}

public function index()
{
   $this->load->view('admin/attachvehicle_list');
}


public function new_vehicle()
{
   $this->load->view('admin/attach_vehicle');
}

  
public function addvehicle()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('vtype', 'name', 'required');
$this->form_validation->set_rules('regno', 'regno', 'required');
$this->form_validation->set_rules('nseats', 'email', 'required');
$this->form_validation->set_rules('rcname', 'name', 'required');
$this->form_validation->set_rules('rcmobile', 'email', 'required');
$this->form_validation->set_rules('drname', 'name', 'required');
$this->form_validation->set_rules('drmobile', 'email', 'required');
$this->form_validation->set_rules('startkm', 'name', 'required');

 if($this->form_validation->run())
        {
			$ntype=$this->input->post('vtype');
			$vehicleno=$this->input->post('regno');
			$nseats=$this->input->post('nseats');
			$rcname=$this->input->post('rcname');
			$rcmobile=$this->input->post('rcmobile');
			$drname=$this->input->post('drname');
			$drmobile=$this->input->post('drmobile');
			$startkm=$this->input->post('startkm');
			$endkm=$this->input->post('endkm');
	    
			$ndt = array(
				'attached_vehicletype' => $ntype,
				'attached_vehicleno' => $vehicleno,
				'attached_noofseats'=>$nseats,
				'attached_rcname' => $rcname,
				'attached_rcmobile' =>$rcmobile,
				'attached_drname' =>$drname,
				'attached_drmobile' =>$drmobile,
				'attached_startkm' =>$startkm,
				'attached_endkm' => $endkm,
				'attached_status'=>'0',
				);
				
         $this->load->Model('Model_attach');
         $this->Model_attach->form_insert($ndt);
         $this->session->set_flashdata('message', 'Vehicle details added.');
         $uid = $this->db->insert_id();
		 
		 
		  $date=date("Y/m/d");
			date_default_timezone_set('Asia/Kolkata');
			$time=date('h:i:sa');
	
		  $staffid=$this->session->userdata('userid');
			$staffname=$this->session->userdata('name');
		 
         $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Vendor Details Added'

            );
           $this->load->model('Model_customer');
           $this->Model_customer->form_insertlog($userdt1);
           redirect('Attachvehicle/new_vehicle');  
        }

 else{
         $this->session->set_flashdata('errmsg', 'Vehicle details missing');
        //$data['message']="Data Inserted Successfully";
         redirect('Attachvehicle/new_vehicle');
       }  
}
                
public function vehicle_ajax()
{
        $this->load->model('Model_attach');
        $results=$this->Model_attach->view_vehicles();
		
        $data1 = array();
        foreach($results  as $r) 
               {
					
				$edit='<a href="#myModal2" id="'.$r->attached_vehicleid.'" data-toggle="modal" class="edit"><button class="btn btn-warning btn-xs" style="padding:4px 7px 4px 7px;"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>';
				$del='<a href="#" id="'.$r->attached_vehicleid.'" data-toggle="modal" class="delete"><button class="btn btn-danger btn-xs" style="margin-left:3px;padding:4px 7px 4px 7px;"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a> ';
		
                      array_push($data1, array(
							"edit"=>$edit.$del,
                            "id"=>$r->attached_vehicleid,
                            "vtype"=>$r->attached_vehicletype,
                            "rcname"=>$r->attached_rcname,
							"rcmobile"=>$r->attached_rcmobile,
                            "drname"=>$r->attached_drname,
                            "drmobile"=>$r->attached_drmobile,
                            "startkm"=>$r->attached_startkm
							));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

public function add_owner() 
  {
    $this->load->view("admin/new_vendor");

    return;
    echo "     
     <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>           
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Add owner</font></h4>
    </div>
    <br>

 <form  class='form-horizontal' role='form' method='post' action='". site_url('owner/add_new_owner'). "' enctype='multipart/form-data' onsubmit='return checkdata();' >
                    <!-- text input -->
<div class='row'>
<div class='col-md-8'>
	   
            <div class='form-group'>
            <label class='col-md-4 control-label' style='padding-top:10px;'>Name : </label>
            <div class='col-md-8' style='padding-left:19px;'>
			<input type='text' name='name' class='form-control'  id='mask_date2' required />
            </div>                   
            </div>
                   
           
         <div class='form-group'>
                     <label class='col-md-4 control-label' style='padding-top:10px;'>Email : </label>
                    <div class='col-md-8' style='padding-left:19px;'>
					<input type='email' name='email' class='form-control'  id='mask_date2' required/>
          </div>                   
          </div>

          <div class='form-group'>
                     <label class='col-md-4 control-label' style='padding-top:10px;'>Address : </label>
                    <div class='col-md-8' style='padding-left:19px;'>
					<textarea name='address' row='2' class='form-control' required></textarea>
          </div>                   
          </div>
      
 </div>
    <div class='col-md-4'>  
 
                     <div class='form-group' >
                       <div class='col-md-12'>
                          <div class='fileinput fileinput-new' data-provides='fileinput'>
                             <div class='fileinput-new thumbnail' style='max-width: 145px; max-height: 152px;''>
                              <img src='".base_url('assets/dist/img/user.jpg')."' alt='' /> </div>
                               <div class='fileinput-preview fileinput-exists thumbnail' style='max-width: 135px; max-height: 140px;'> </div>
                             <div>
                              <span class='btn btn-default btn-file'>
                              <span class='fileinput-new'> Select Photo </span>
                              <span class='fileinput-exists'> Change </span>
                              <input type='file' name='image' > </span>
                              <a href='javascript:;' class='btn btn-default fileinput-exists' data-dismiss='fileinput'> Remove </a>
                         </div>
                        </div>
                      </div>
                    </div> 

     </div>
 </div>
 <div class='row'>
 <div class='col-md-11'>
		  <div class='form-group'>
          <label class='col-md-3 control-label'>Mobile : </label>
          <div class='col-md-8'>
		  <input type='number' class='form-control' placeholder='Enter mobile no' name='mobile' id='mobile'>
        <label id='mobmessage' style='color:red;'></label>
          </div>
          </div>
 
          <div class='form-group'>
          <label class='col-md-3  control-label'>Landline : </label>
          <div class='col-md-8'>
		  <input type='number' class='form-control' placeholder='Enter landline' name='landline' value='' required id='landline'>
      <label id='landmessage' style='color:red;'></label>
          </div>
          </div>
     
          
          <div class='form-group'>
          <label class='col-md-3  control-label'>No of Vehicles : </label>
          <div class='col-md-8'>
		  <input type='number' class='form-control' placeholder='Enter vehicleno' name='vehicleno' value='' required/>
          </div>
          </div>

          <div class='form-group'>
          <label class='col-md-3  control-label'>Vehicle Types : </label>
          <div class='col-md-8'>
		  <input type='text' class='form-control' placeholder='Enter Vehicle Type' name='vehicletype' value='' required/>
		  <label style='font-size:10px;color:blue;'>Eg: Innova-1,Tavera-2,..</label>
          </div>
          </div>
     
</div>
</div>
     <label style='height:1px;width:100%;background-color:#cecece;'></label>
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-8'>
                <input type='submit' class='btn btn-primary' value='Update'/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form> 
		   <br>
		   <!--<div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
      </div>-->
	   <script type='text/javascript'>
  
  function checkdata()
  {
    
    var mob=$('#mobile').val();
    var landline=$('#landline').val();
      
  
    if(mob.length<10 || mob.length>10)
    {
      $('#mobmessage').html('Invalid mobile no,10 digits only.');
      return false;
    }
    else if(landline.length<11 || landline.length>11)
    {
      $('#landmessage').html('Invalid mobile no,11 digits only.');
      return false;
    }
    
    
    else
    {
      $('#mobmessage').html('');
      $('#landmessage').html('');
      return true;
    }
  }

</script>  ";
    }

	
 public function edit_vehicle() 
	{
	$m="";
    $f="";
    
        $data=$this->session->all_userdata();
        $id=$this->input->post('uid');
		$query = $this->db->select('*')->from('attached_vehicle')->where('attached_vehicleid',$id)->get();
        $row = $query->row();
     
    
    echo "       
     <style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>   
       
    <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Attached Vehicle</font></h4>
    </div>
    <br>
<div style='margin-left:30px;'>
	<form  onsubmit='return checkdata();'  class='form-horizontal' role='form' method='post' action='". base_url('Attachvehicle/updatevehicle')."' enctype='multipart/form-data'>
                    <!-- text input -->
			<div class='row'>
			<div class='col-md-10'>
	   <input type='hidden' name='vehicleid' value='$row->attached_vehicleid'>
            <div class='form-group'>
            <div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label'>Vehicle Type : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='vtype' class='form-control' value='$row->attached_vehicletype' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >No os seats : </label>
				<div class='col-md-2' style='padding-left:19px;'>
				<input type='text' name='nseats' class='form-control' value='$row->attached_noofseats' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' >R/C Owner Name : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='rcname' class='form-control' value='$row->attached_rcname' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Mobile : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='number' name='rcmobile' class='form-control' value='$row->attached_rcmobile' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' >Driver Name : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='text' name='drname' class='form-control'  value='$row->attached_drname' required />
				</div>           
			</div>
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Mobile No : </label>
				<div class='col-md-8' style='padding-left:19px;'>
				<input type='number' name='drmobile' class='form-control' value='$row->attached_drmobile'  required />
				</div>           
			</div>
			<div class='row' style='margin-top:15px;'>
				<label class='col-md-4 control-label' style='color:blue;'>Kilometers : </label>	
			</div>	
			
			<div class='row' style='margin-top:5px;'>
				<label class='col-md-4 control-label' >Starting : </label>
				<div class='col-md-3' style='padding-left:19px;'>
				<input type='text' name='startkm' class='form-control' value='$row->attached_startkm' required />
				</div>           
				
			</div>
        </div>
			
      
 </div>
 </div>
     <label style='height:1px;width:100%;background-color:#cecece;'></label>
                <div class='form-group'> 
                <div class='col-md-3'>
                </div>
                <div class='col-md-8'>
                <input type='submit' class='btn btn-primary' value='Update Details'/>
                </div>
                </div>                                
           </form> 
		   </div>
		   
       <script type='text/javascript'>
  
  function checkdata()
  {
    
    var mob=$('#mobile').val();
    var landline=$('#landline').val();
      
  
    if(mob.length<10 || mob.length>10)
    {
      $('#mobmessage').html('Invalid mobile no,10 digits only.');
      return false;
    }
    else if(landline.length<11 || landline.length>11)
    {
      $('#landmessage').html('Invalid mobile no,11 digits only.');
      return false;
    }
    
    
    else
    {
      $('#mobmessage').html('');
      $('#landmessage').html('');
      return true;
    }
  }

</script>  ";
    }
	
public function updatevehicle()
{
	
$this->load->library('form_validation');

$this->form_validation->set_rules('vtype', 'name', 'required');
$this->form_validation->set_rules('nseats', 'email', 'required');
$this->form_validation->set_rules('rcname', 'name', 'required');
$this->form_validation->set_rules('rcmobile', 'email', 'required');
$this->form_validation->set_rules('drname', 'name', 'required');
$this->form_validation->set_rules('drmobile', 'email', 'required');
$this->form_validation->set_rules('startkm', 'name', 'required');

 if($this->form_validation->run())
        {
			$vid=$this->input->post('vehicleid');
			$ntype=$this->input->post('vtype');
			$nseats=$this->input->post('nseats');
			$rcname=$this->input->post('rcname');
			$rcmobile=$this->input->post('rcmobile');
			$drname=$this->input->post('drname');
			$drmobile=$this->input->post('drmobile');
			$startkm=$this->input->post('startkm');
	    
			$ndt = array(
				'attached_vehicletype' => $ntype,
				'attached_noofseats'=>$nseats,
				'attached_rcname' => $rcname,
				'attached_rcmobile' =>$rcmobile,
				'attached_drname' =>$drname,
				'attached_drmobile' =>$drmobile,
				'attached_startkm' =>$startkm,
				);
				
         $this->load->Model('Model_attach');
         $this->Model_attach->update_vehicle($vid,$ndt);
         $this->session->set_flashdata('message', 'Vehicle details updated.');
         $uid = $this->db->insert_id();
		 
		  $date=date("Y/m/d");
			date_default_timezone_set('Asia/Kolkata');
			$time=date('h:i:sa');
	
		  $staffid=$this->session->userdata('userid');
			$staffname=$this->session->userdata('name');
		 
		 
         $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Vendor Details Added'

            );
           $this->load->model('Model_customer');
           $this->Model_customer->form_insertlog($userdt1);
           redirect('Attachvehicle/index');  
        }

 else{
         $this->session->set_flashdata('errmsg', 'Vehicle details missing');
        //$data['message']="Data Inserted Successfully";
         redirect('Attachvehicle/index');
       }  
}

  public function del_entry()
  {
    //$id=$this->uri->segment(3, 0);
	
	$id=$this->input->post('id');
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa');
	
      $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
	  
            $this->load->model('Model_attach');
            $result=$this->Model_attach->delete_vehicle($id);
            if($result)
            {
           //$this->session->set_flashdata('message', 'Vendor details has been deleted..!');
            $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$id,
            'log_desc'=>'Vendor Details Deleted'
           );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
          //redirect('Attachvehicle/index');
            }
           else
           {
            $this->session->set_flashdata('message', 'Please try again.');
            //redirect('Attachvehicle/index');
           }
  }
  
  
 
 public function modal_addvehicle()
{
			$ntype=$this->input->post('vtype');
			$vehicleno=$this->input->post('regno');
			$nseats=$this->input->post('nseats');
			$rcname=$this->input->post('rcname');
			$rcmobile=$this->input->post('rcmobile');
			$drname=$this->input->post('drname');
			$drmobile=$this->input->post('drmobile');
			$startkm=$this->input->post('startkm');
			$endkm=$this->input->post('endkm');
	    
			$ndt = array(
				'attached_vehicletype' => $ntype,
				'attached_noofseats'=>$nseats,
				'attached_vehicleno'=>$vehicleno,
				'attached_rcname' => $rcname,
				'attached_rcmobile' =>$rcmobile,
				'attached_drname' =>$drname,
				'attached_drmobile' =>$drmobile,
				'attached_startkm' =>$startkm,
				'attached_endkm' => $endkm,
				'attached_status'=>'0',
				);
				
         $this->load->Model('Model_attach');
         $this->Model_attach->form_insert($ndt);
		 return true;
} 
}