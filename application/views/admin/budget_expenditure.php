<?php
include('application/views/common/header.php');
?>

<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.lr-pad
{
	padding-left:5px;
	padding-right:5px;
}
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

	  <section class="content-header">
        <h1><b>Budget - Expenditure Details</b></h1>
		<ol class="breadcrumb" style='font-size:15px;'>
			<li><a href="<?php echo base_url('Services/request_list');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Service Request List</button></a></li>
	    </ol> 
         <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
  
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
	
  <div class="row">
  <div class="col-md-12">

  <div style="background-color:#fff;border-radius:2px;padding:15px; ">
    
        <div class="portlet-title">
          <div class="caption" >
	 	    <h4 style='margin-top:0px;'><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Details</h4>
        </div>
        </div>
			
<hr style="background-color:#cecece,border:1px;width:100%;top:10px;margin:0px;padding:3px 0px 10px 0px;">
             
<div class="portlet-body form">
<div class='row' style='padding-left:15px;'>
<div class="col-md-7">
<form enctype="multipart/form-data" method="POST" id="addproductivity" name="addproductivity" action="">
		
		<label style='border-bottom:1px solid #e4e4e4;'><b>Select Month & Year</b></label>
		<div class='row'>
		<label class='col-md-4'>Month</label>
		<label class='col-md-2'>Year</label>
		</div>
		
		<div class='row'>
		<div class="col-md-4" style='padding-left:15px;'>
		<select name='month' class="form-control" id='month' required>
		<option value="">----------</option>
		<?php
		for($m=1;$m<=12;$m++)
		{
			$d=date('y')."-".$m."-01";
			echo "<option value='".$m."'>".date('F',strtotime($d))."</option>";
		}
		?>
		</select>
        
        </div>
		<div class="col-md-2 lr-pad" >
		<select name='year' class="form-control" id='year' required>
		<option value="">----------</option>
		<?php
		for($y=date('Y')+1;$y>=date('Y')-1;$y--)
		{
			echo "<option value='".$y."'>".$y."</option>";
		}
		?>
		</select>
	   
        </div>
		</div>
		

		<label style='margin-top:15px;border-bottom:1px solid #e4e4e4;'><b>Add particulers one by one</b></label>
		<div style='padding-left:10px;'>
		<div class='row' >
		<label class=' col-md-5 control-label' style='padding-left:5px;'>Particulars</label>
		<label class=' col-md-3 control-label'>Amount</label>
		</div>
				
		<div class='row'>
		<div class="col-md-7 lr-pad" >
        <input type="text" id="part" placeholder="purticulars" class="form-control">
        </div>
		<div class="col-md-3 lr-pad" >
        <input type="text" id="price" placeholder="Amount" class="form-control">
        </div>
		<div class="col-md-1">
        <input type="button" class="add-row btn btn-default btn-sm" value="Add Row" >
        </div>
		</div>
		<div class='row' style='margin-top:10px;'>
		<div class='col-md-12' style='padding:5px;'>
		<table id="example" class="table table-striped table-bordered display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <td width="10%">ID</td>
                <td >Particulars</td>
                <td width="15%">Amount</td>
                <!--<td width="10%">Action</td>-->
            </tr>
        </thead>
        <tbody>
           
        </tbody>
	</table> 
	</div>
	</div>
	</div>
    <button type="submit" class="btn btn-primary" style='padding:5px 20px 5px 15px;' > <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
  </form>
  </div>
 </div>
 </div> 
  <!--portlet-body-->
 </div>
</div>    
<br>  
  </div>
  </div>
</section>

<?php include('application/views/common/footer.php');?>
  </div>
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script> 

<script type="text/javascript">
/*$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
*/
  
  $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	//endDate:'now'
   });
 
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
	//endDate:'now'
   }); 
    
	
var i=1;
$(".add-row").click(function(){
    var part = $("#part").val();
    var price = $("#price").val();
	
    $("#part").val('');
    $("#price").val('');
	if(part!="" && price!="")
	{
    var markup = "<tr id='"+i+"'><td>"+i+"</td><td>" + part + "</td><td>" + parseFloat(price).toFixed(2)+ "</td></tr>";
    $("table tbody").append(markup);
    i++;
	}
	else
	{
		alert("please select particulars and amount.");
	}
});
  
  
function storeTblValues()
{
    var TableData = new Array();

    $('#example tr').each(function(row, tr){
        TableData[row]={
            "no" : $(tr).find('td:eq(0)').text(),
             "part" :$(tr).find('td:eq(1)').text(),
             "price" : $(tr).find('td:eq(2)').text()
        }    
    }); 
    TableData.shift();  // first row will be empty - so remove
    return TableData;
}


$(document).on("submit", "#addproductivity", function (event) {
          
    event.preventDefault();

	var mon= $("#month").val();
	var yr= $("#year").val();
	
	if(mon!="" && yr!="")
	{
		var TableData;
		TableData = $.toJSON(storeTblValues());
			
		$.ajax({
			url: "<?php echo base_url();?>Budget/add_expenditure",
			type: 'POST',
			data: {pTableData:TableData,month:mon,year:yr},
			success: function (data, status) {
		     if(data="success")
			  {
				  swal({
					  title: "Saved",
					  text: "Expenditure details added.!",
					  type: "success",
					  showCancelButton: false,
					  confirmButtonClass: "btn-success",
					  confirmButtonText: " Ok ",
					  closeOnConfirm: false
					},
					function(){
					  window.location.reload(); 
					});
			  }
			  else
			  {
				swal("Error", "Something went wrong", "error");    
			  }  
			}
		  });
	  
	}
	else
	{
		alert("Month and Year missing, try again.");
	}
});
  
  
/*$("#btnexp1").click(function()
{
	var etype1=$("#exptype1").val();
	var egroup=$("#expgroup").val();
	jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Vehicle/add_exp_type",
        dataType: 'html',
        data: {exptype:etype1,expgroup:egroup},
        success: function(res) {
		$("#etype").append("<option value='"+res+"'>"+etype1+"</option>")
                    }
            });
});
*/

</script>


  </body>
</html>
