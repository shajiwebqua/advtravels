<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Corporate Contacts</b></h1>
  <ol class="breadcrumb" style="font-size:15px;">
  <?php
  //if(checkisAdmin()){
    //	echo "
      //  <li><a id='idedit' href='#' data-target='#myModal3' data-toggle='modal' style='color:#4b88ed;' ><i class='fa fa-pencil' aria-hidden='true'></i>Edit</a></li>
      //  <li><a id='iddelete' href='' style='color:#4b88ed;' ><i class='fa fa-trash' aria-hidden='true'></i>Delete</a></li> 
      //";
   //} 
  ?>
  </ol>
  
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header end -->   
    
  <section class="content"> 
  
      <div style="padding:2px 0px 2px 0px;">
		 <div id='idmsg' style="background-color:#fff;height:25px;margin-bottom:3px;">
		 <center><div id='idh5'><?php echo $this->session->flashdata('message'); ?></div></center>
		 </div>
	</div>
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->

<div class="portlet-body">
<div style="background-color:#fff;padding:15px; ">
              
		<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                        
        <!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
        
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet light portlet-fit bordered">
					<div class="table-toolbar">
					<div class="row">
					<div class="col-md-12">
                  
                   </div>
                   </div>
                   </div>
                
         <table id="TblCorporate" class="table table-striped table-hover table-bordered" cellspacing="0">
						<thead>
						<tr>
							<th>Action</th>
							<th>ID</th>
							<th>Corporate Name</th>
							<th>Key Person</th>
							<th>Contact Person</th>
							<th>Account Person</th>
						</tr>
						</thead>
					 </table>
        </div>
                       <!-- END BORDERED TABLE PORTLET-->
		</div>
		</div>
                <!-- END CONTENT BODY -->
	</div>
	</div>
            <!-- END QUICK SIDEBAR -->
</div>
  
		<div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
        </div> 
 
		
      
  </div>
  <!-- End user details -->
  </div>
  
  </div>
    <!-- /.row --> 
    <!-- /.row (main row) --> 
<!-- /.content --> 
</section>
  <!-- /.content-wrapper -->
</div>
 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 $("#idmsg").hide();

//sweet alert ----------------------------
	if($("#idh5").html()!="")
	{
		var msg=$("#idh5").html().split("#");
		swal(msg[0],msg[1],"success");
		$("#idh5").html("");
	}
// sweet alert box -------------------------- 

 $(document).ready(function()
 {
      

  $('#TblCorporate').dataTable({
    "ordering":true,
         destroy: true,
        "processing": true,
		'scrollX':true,
        // "ajax": "<?php echo site_url('report/course_fee');?>",
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Customer/corporate_ajax",// json datasource
               },
			   "columnDefs":[
		{"width":"25%","targets":2},
		
		/*{ className : "dt-right", "targets":4},*/
		],
  
    "columns": [
	 { "data": "action" },
      { "data": "custid" },
      { "data": "name"},
	  { "data": "kperson"},
      { "data": "cperson"},
      { "data": "aperson"},
       ]
  } );
     
  
  
 /*var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
          table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );*/


//   new code  --------------------------------------------------------------------------------------------------------------
   //   $('#example tbody').on('click', '.edit', function () {
     
  $('#TblCorporate tbody').on('click','.edit',function(e)
  { 
  
    var Result=$("#myModal1 .modal-body");
    var id=$(this).attr('id'); //#example').find('tr.selected').find('td').eq(0).text();

  if(id=="")
     {
       alert ("please Select customer details..");
       e.stopPropagation();
     }
     else
     {
      jQuery.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "Customer/edit_corporate",
      dataType: 'html',
      data:{cid:id},
      success: function(res) {
      Result.html(res);
       }
      });
     }
 }); 
 
// -----------------------------------------------------------------------------------------------------------------------------------    
    
     $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#idh5').html(''); }, 3000);


});
</script>



