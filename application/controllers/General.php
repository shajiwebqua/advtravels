<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General extends CI_Controller 
{
		
function __construct(){
parent::__construct();
$user = $this->session->userdata('userid');
if (!isset($user))
{ 
redirect('/');
} 
else
{ 
return true;
}
$this->load->helper(array('form', 'url'));
}

 public function index()
  {
      $this->load->view('admin/role');
  }   
   
  public function options()
  {
     $this->load->view('admin/general_options');
  }

 
  public function country()
  {
     $this->load->view('admin/country');
  }  


public function add_country()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('country', 'country', 'required');
  if($this->form_validation->run())
        {
            $country= $this->input->post('country');
			$log=array(
                   'country_name'=>$country,
                   'country_status'=>'1'
                );
         $this->load->Model('Model_general');
        /*Calls Model to enter login details  in Login Table of the corresponding user*/
        $this->Model_general->insert_country($log);
        $this->session->set_flashdata('message', 'Country added..!');
        
       redirect('General/country'); 
     
  }
 else{
       $this->session->set_flashdata('message', 'Property type missing..!');
       redirect('General/index');
       }  
}


public function country_ajax()
{
        $this->load->model('Model_general');
        $results=$this->Model_general->view_country();
		
        $data1 = array();
        foreach($results  as $r) 
               {
               	$edit="<a href='#myModal2' id='$r->country_id' data-toggle='modal' class='edit open-AddBookDialog2'>
				<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
				$mm='<center><button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button></center>';
				$del=anchor('General/del_entry/'.$r->country_id, $mm, array('id' =>'del_conf'));
             
		if($r->country_status==1)
		{
			$st="<font color='green'>Active</font>";
		}
		else
		{
			$st="<font color='red'>Inactive</font>";
		}
                      array_push($data1, array(
                            "edit"=>"$edit",
                           	"delete"=>"$del",
                            "country"=>"$r->country_name",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}


public function edit_country() 
  {
  $data=$this->session->all_userdata();
  $id=$this->input->post('id');
  $query = $this->db->select('*')->from('country')->where('country_id',$id)->get();
    $row = $query->row();
     
     echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Country</font></h4>
    </div>
    <br>

    <form  class='form-horizontal' role='form' method='post' action='". site_url('General/update_country'). "' enctype='multipart/form-data'>
                    <!-- text input -->
            <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Country Name : </label>
                    <div class='col-md-9'>  
          <input type='hidden' name='id'  value='$row->country_id'/>
          <input type='text' name='country' class='form-control'  id='mask_date2' value='$row->country_name' required/>
          </div>                   
          </div>
           
                    
            <div class='form-group'>
                      <label class='col-md-3  control-label'>Status: </label>
                     <div class='col-md-9'>
                      <select type='text' name='status' class='form-control'>";
            echo '<option value="0" ';if($row->country_status=='0')echo "selected"; echo'>Inactive</option>
              <option value="1"';if($row->country_status=='1')echo "selected"; echo ">Active</option>
              </select>
                        </div>                     
                  </div>
     

               <label style='background-color:#cecece ;width:100%;height:1px;''></label>
                <div class='form-group'> 
                <div class='row'>
                <div class='col-md-7'>
                </div>
                <div class='col-md-4'>
                <input type='submit' class='btn btn-primary' value='Update'/>
                   <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>  ";
  }

public function update_country()
         {
$this->load->library('form_validation');
$this->form_validation->set_rules('country', 'country', 'required');
           if($this->form_validation->run())
                    {
    $this->load->model('Model_general');
    $country= $this->input->post('country');
    $status= $this->input->post('status');
    $id=$this->input->post('id');
   $usr_newdt = array(
                //'image_url'=>$fname,
                'country_name' => $country,
                'country_status'=>$status
               );
        $this->Model_general->update_country($id,$usr_newdt);
        $this->session->set_flashdata('message',  'Country details Updated..!');
        redirect('General/country');

       }
        else 

        {
           redirect('General/index');
        }                   
}
  



  public function del_entry()
  {
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_country($id);
            if($result)
            {
                    $this->session->set_flashdata('message', 'Country Removed..!');
				redirect('General/country');            }
           else
           {
            $this->session->set_flashdata('message', 'Data missing, try again.');
			redirect('General/country');
           }
  }


  /* Area Entries ---------------------------------------------------------------------------*/

 
 public function area()
       {
           $this->load->model('Model_general');
           $data['result']=$this->Model_general->view_country();
			$this->load->view('admin/area.php',$data);
       }  
 
  
  public function add_area()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('country', 'country', 'required');
$this->form_validation->set_rules('code', 'code', 'required');
 if($this->form_validation->run())
        {
            $country= $this->input->post('country');
			$area= $this->input->post('area');
      $code= $this->input->post('code');
			$log=array(
				   'area_name'=>$area,
				   'area_countryid'=>$country,
           'area_code'=>$code,
           'area_status'=>'1'
                );
         $this->load->Model('Model_general');
        /*Calls Model to enter login details  in Login Table of the corresponding user*/
        $this->Model_general->insert_area($log);
        $this->session->set_flashdata('message', 'Area added..!');
        
       redirect('General/area'); 
     
  }
 else{
       $this->session->set_flashdata('message', 'Property type missing..!');
       redirect('General/area');
       }  
}


public function area_ajax()
{
        $this->load->model('Model_general');
        $results=$this->Model_general->view_area();
		
        $data1 = array();
        foreach($results  as $r) 
               {
               	$edit="<a href='#myModal2' id='$r->area_id' data-toggle='modal' class='edit open-AddBookDialog2'>
				<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
				$mm='<center><button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button></center>';
				$del=anchor('General/del_entry1/'.$r->area_id, $mm, array('id' =>'del_conf'));
             
		if($r->area_status==1)
		{
			$st="<font color='green'>Active</font>";
		}
		else
		{
			$st="<font color='red'>Inactive</font>";
		}
                array_push($data1, array(
                     "edit"=>"$edit",
                    "delete"=>"$del",
                    "code"=>"$r->area_code",
					"area"=>"$r->area_name",
                    "country"=>"$r->country_name",
                    "status"=>$st
                  ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}
  
public function edit_area() 
  {
    $this->load->model('Model_general');
    $results1=$this->Model_general->view_country();
  $data=$this->session->all_userdata();
  $id=$this->input->post('id');
  $query = $this->db->select('*')->from('area')->join('country',"country.country_id=area.area_countryid","inner")->where('area_id',$id)->get();
    $row = $query->row();
     
     echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Area</font></h4>
    </div>
    <br>

    <form  class='form-horizontal' role='form' method='post' action='". site_url('General/update_area'). "' enctype='multipart/form-data'>
                    <!-- text input -->
               
               <div class='form-group'>
         <label class='col-md-3  control-label'>Select Country</label>
                   <div class='col-md-9'>
                    <input type='hidden' name='id'  value='$row->area_id'/>                    
             <select class='form-control' name='country' type='text'>";
                foreach($results1 as $ro){
             echo "<option value='$ro->country_id'";
               if($row->area_countryid==$ro->country_id)echo "selected";
                   echo" > $ro->country_name </option>";
                           }  
                           echo "</select>
                                 </div>
                                    </div>



            <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Area Code : </label>
                    <div class='col-md-9'>  
          <input type='text' name='code' class='form-control'  id='mask_date2' value='$row->area_code' required/>
          </div>                   
          </div>


            <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Area Name : </label>
                    <div class='col-md-9'>  
          <input type='text' name='area' class='form-control'  id='mask_date2' value='$row->area_name' required/>
          </div>                   
          </div>
           
                    
            <div class='form-group'>
                      <label class='col-md-3  control-label'>Status: </label>
                     <div class='col-md-9'>
                      <select type='text' name='status' class='form-control'>";
            echo '<option value="0" ';if($row->area_status=='0')echo "selected"; echo'>Inactive</option>
              <option value="1"';if($row->area_status=='1')echo "selected"; echo ">Active</option>
              </select>
                        </div>                     
                  </div>
     

               <label style='background-color:#cecece ;width:100%;height:1px;''></label>
                <div class='form-group'> 
                <div class='row'>
                <div class='col-md-7'>
                </div>
                <div class='col-md-4'>
                <input type='submit' class='btn btn-primary' value='Update'/>
                   <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>  ";
  }

public function update_area()
         {
$this->load->library('form_validation');
$this->form_validation->set_rules('country', 'country', 'required');
$this->form_validation->set_rules('area', 'area', 'required');
           if($this->form_validation->run())
                    {
    $this->load->model('Model_general');
    $country= $this->input->post('country');
    $area= $this->input->post('area');
     $code= $this->input->post('code');
    $status= $this->input->post('status');
    $id=$this->input->post('id');
   $usr_newdt = array(
                //'image_url'=>$fname,
                'area_countryid'=>$country,
                  'area_code' => $code,
                'area_name' => $area,
                'area_status'=>$status
               );
        $this->Model_general->update_area($id,$usr_newdt);
        $this->session->set_flashdata('message',  'Area details Updated..!');
        redirect('General/area');

       }
        else 

        {
           redirect('General/area');
        }                   
}
  

  
  public function del_entry1()
  {
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_area($id);
            if($result)
            {
                    $this->session->set_flashdata('message', 'Area Removed..!');
				redirect('General/area');            }
           else
           {
            $this->session->set_flashdata('message', '<font color="red"><b>Please try again..</b></font>');
			redirect('General/area');
           }
  }
   
  //-----------------------------------------------------------------------
   
  
    public function category()
  {
      $this->load->view('admin/category.php');
  }  


public function add_category()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('category', 'category', 'required');
  if($this->form_validation->run())
        {
            $category= $this->input->post('category');
			$log=array(
                   'category_name'=>$category,
                   'category_status'=>'1'
                );
         $this->load->Model('Model_general');
        /*Calls Model to enter login details  in Login Table of the corresponding user*/
        $this->Model_general->insert_category($log);
        $this->session->set_flashdata('message', 'Category added..!');
        
       redirect('General/category'); 
     
  }
 else{
       $this->session->set_flashdata('message', '<font color=red><b>Category missing..!</b><font>');
       redirect('General/category');
       }  
}

public function category_ajax()
{
        $this->load->model('Model_general');
        $results=$this->Model_general->view_category();
		
        $data1 = array();
        foreach($results  as $r) 
               {
               	$edit="<a href='#myModal2' id='$r->category_id' data-toggle='modal' class='edit open-AddBookDialog2'>
				<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
				$mm='<center><button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button></center>';
				$del=anchor('General/del_entry2/'.$r->category_id, $mm, array('id' =>'del_conf'));
             
		if($r->category_status==1)
		{
			$st="<font color='green'>Active</font>";
		}
		else
		{
			$st="<font color='red'>Inactive</font>";
		}
                      array_push($data1, array(
                            "edit"=>"$edit",
                           	"delete"=>"$del",
                            "category"=>"$r->category_name",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}


public function edit_category() 
  {
  $data=$this->session->all_userdata();
  $id=$this->input->post('category_id');
  $query = $this->db->select('*')->from('category')->where('category_id',$id)->get();
    $row = $query->row();
     
     echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Category</font></h4>
    </div>
    <br>

    <form  class='form-horizontal' role='form' method='post' action='". site_url('General/update_category'). "' enctype='multipart/form-data'>
                    <!-- text input -->
            <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Category Name : </label>
                    <div class='col-md-9'>  
          <input type='hidden' name='id'  value='$row->category_id'/>
          <input type='text' name='category' class='form-control'  id='mask_date2' value='$row->category_name' required/>
          </div>                   
          </div>
           
                    
            <div class='form-group'>
                      <label class='col-md-3  control-label'>Status: </label>
                     <div class='col-md-9'>
                      <select type='text' name='status' class='form-control'>";
            echo '<option value="0" ';if($row->category_status=='0')echo "selected"; echo'>Inactive</option>
              <option value="1"';if($row->category_status=='1')echo "selected"; echo ">Active</option>
              </select>
                        </div>                     
                  </div>
     

               <label style='background-color:#cecece ;width:100%;height:1px;''></label>
                <div class='form-group'> 
                <div class='row'>
                <div class='col-md-7'>
                </div>
                <div class='col-md-4'>
                <input type='submit' class='btn btn-primary' value='Update'/>
                   <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>  ";
  }

public function update_category()
         {
$this->load->library('form_validation');
$this->form_validation->set_rules('category', 'category', 'required');
           if($this->form_validation->run())
                    {
    $this->load->model('Model_general');
    $category= $this->input->post('category');
    $status= $this->input->post('status');
    $id=$this->input->post('id');
   $usr_newdt = array(
                //'image_url'=>$fname,
                'category_name' => $category,
                'category_status'=>$status
               );
        $this->Model_general->update_category($id,$usr_newdt);
        $this->session->set_flashdata('message',  'Category details Updated..!');
        redirect('General/category');

       }
        else 

        {
           redirect('General/category');
        }                   
}
  

  
 public function del_entry2()
  {
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_category($id);
            if($result)
            {
                    $this->session->set_flashdata('message', 'Category Removed..!');
				redirect('General/category');            }
           else
           {
            $this->session->set_flashdata('message', 'Please try again.');
			redirect('General/category');
           }
  }

  //profession
     public function profession()
  {
      $this->load->view('admin/profession.php');
  }  


public function add_profession()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('profession', 'profesion', 'required');
  if($this->form_validation->run())
        {
            $profession= $this->input->post('profession');
      $log=array(
                   'profession_name'=>$profession,
                   'profession_status'=>'1'
                );
         $this->load->Model('Model_general');
        /*Calls Model to enter login details  in Login Table of the corresponding user*/
        $this->Model_general->insert_profession($log);
        $this->session->set_flashdata('message', 'Profession added..!');
       
        redirect('General/profession'); 
     
  }
 else{
       $this->session->set_flashdata('message', 'Profession missing..!');
       redirect('General/profession');
       }  
}

public function profession_ajax()
{
        $this->load->model('Model_general');
        $results=$this->Model_general->view_profession();
    
        $data1 = array();
        foreach($results  as $r) 
               {
                $edit="<a href='#myModal2' id='$r->profession_id' data-toggle='modal' class='edit open-AddBookDialog2'>
        <center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
        $mm='<center><button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button></center>';
        $del=anchor('General/del_entry3/'.$r->profession_id, $mm, array('id' =>'del_conf'));
             
    if($r->profession_status==1)
    {
      $st="<font color='green'>Active</font>";
    }
    else
    {
      $st="<font color='red'>Inactive</font>";
    }
                      array_push($data1, array(
                           "edit"=>"$edit",
                            "delete"=>"$del",
                            "profession"=>"$r->profession_name",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

public function edit_profession() 
  {
  $data=$this->session->all_userdata();
  $id=$this->input->post('profession_id');
  $query = $this->db->select('*')->from('profession')->where('profession_id',$id)->get();
    $row = $query->row();
     
     echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Profession</font></h4>
    </div>
    <br>

    <form  class='form-horizontal' role='form' method='post' action='". site_url('General/update_profession'). "' enctype='multipart/form-data'>
                    <!-- text input -->
            <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Profession Name : </label>
                    <div class='col-md-9'>  
          <input type='hidden' name='profession_id'  value='$row->profession_id'/>
          <input type='text' name='profession' class='form-control'  id='mask_date2' value='$row->profession_name' required/>
          </div>                   
          </div>
           
                    
            <div class='form-group'>
                      <label class='col-md-3  control-label'>Status: </label>
                     <div class='col-md-9'>
                      <select type='text' name='status' class='form-control'>";
            echo '<option value="0" ';if($row->profession_status=='0')echo "selected"; echo'>Inactive</option>
              <option value="1"';if($row->profession_status=='1')echo "selected"; echo ">Active</option>
              </select>
                        </div>                     
                  </div>
     

               <label style='background-color:#cecece ;width:100%;height:1px;''></label>
                <div class='form-group'> 
                <div class='row'>
                <div class='col-md-7'>
                </div>
                <div class='col-md-4'>
                <input type='submit' class='btn btn-primary' value='Update'/>
                   <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>  ";
  }

public function update_profession()
         {
$this->load->library('form_validation');
$this->form_validation->set_rules('profession', 'profession', 'required');
           if($this->form_validation->run())
                    {
    $this->load->model('Model_general');
    $profession= $this->input->post('profession');
    $status= $this->input->post('status');
    $id=$this->input->post('profession_id');
   $usr_newdt = array(
                //'image_url'=>$fname,
                'profession_name' => $profession,
                'profession_status'=>$status
               );
        $this->Model_general->update_profession($id,$usr_newdt);
        $this->session->set_flashdata('message',  'Profession details Updated..!');
        redirect('General/profession');

       }
        else 

        {
           redirect('General/profession');
        }                   
}
   
 public function del_entry3()
  {
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_profession($id);
            if($result)
            {
                    $this->session->set_flashdata('message', 'Profession Removed..!');
        redirect('General/profession');            }
           else
           {
            $this->session->set_flashdata('message', 'Please try again.');
      redirect('General/profession');
           }
  } 
  
//role
public function role()
  {
      $this->load->view('admin/role.php');
  }  


public function add_role()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('role', 'role', 'required');
  if($this->form_validation->run())
        {
            $role= $this->input->post('role');
      $log=array(
                   'role_name'=>$role,
                   'role_status'=>'1'
                );
         $this->load->Model('Model_general');
        /*Calls Model to enter login details  in Login Table of the corresponding user*/
        $this->Model_general->insert_role($log);
        $this->session->set_flashdata('message', 'Role added..!');
        
       redirect('General/role'); 
     
  }
 else{
       $this->session->set_flashdata('message', 'Category missing..!');
       redirect('General/role');
       }  
}

public function role_ajax()
{
        $this->load->model('Model_general');
        $results=$this->Model_general->view_role();
    
        $data1 = array();
        foreach($results  as $r) 
          {
        $edit="<a href='#myModal2' id='$r->role_id' data-toggle='modal' class='edit open-AddBookDialog2'>
        <center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
        $mm='<center><button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button></center>';
        $del=anchor('General/del_entry4/'.$r->role_id, $mm, array('id' =>'del_conf'));
             
    if($r->role_status==1)
    {
      $st="<font color='green'>Active</font>";
    }
    else
    {
      $st="<font color='red'>Inactive</font>";
    }
                      array_push($data1, array(
                            "edit"=>"$edit",
                            //"delete"=>"$del",
                            "role"=>"$r->role_name",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}


public function edit_role() 
  {
  $data=$this->session->all_userdata();
  $id=$this->input->post('role_id');
  $query = $this->db->select('*')->from('role')->where('role_id',$id)->get();
    $row = $query->row();
     
     echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Role</font></h4>
    </div>
    <br>

    <form  class='form-horizontal' role='form' method='post' action='". site_url('General/update_role'). "' enctype='multipart/form-data'>
                    <!-- text input -->
            <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Role Name : </label>
                    <div class='col-md-9'>  
          <input type='hidden' name='id'  value='$row->role_id'/>
          <input type='text' name='role' class='form-control'  id='mask_date2' value='$row->role_name' required/>
          </div>                   
          </div>
           
                    
            <div class='form-group'>
                      <label class='col-md-3  control-label'>Status: </label>
                     <div class='col-md-9'>
                      <select type='text' name='status' class='form-control'>";
            echo '<option value="0" ';if($row->role_status=='0')echo "selected"; echo'>Inactive</option>
              <option value="1"';if($row->role_status=='1')echo "selected"; echo ">Active</option>
              </select>
                        </div>                     
                  </div>
     

               <label style='background-color:#cecece ;width:100%;height:1px;''></label>
                <div class='form-group'> 
                <div class='row'>
                <div class='col-md-7'>
                </div>
                <div class='col-md-4'>
                <input type='submit' class='btn btn-primary' value='Update'/>
                   <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>  ";
  }
  
  
public function update_role()
    {
$this->load->library('form_validation');
$this->form_validation->set_rules('role', 'role', 'required');
           if($this->form_validation->run())
                    {
    $this->load->model('Model_general');
    $role= $this->input->post('role');
    $status= $this->input->post('status');
    $id=$this->input->post('id');
   $usr_newdt = array(
                //'image_url'=>$fname,
                'role_name' => $role,
                'role_status'=>$status
               );
        $this->Model_general->update_role($id,$usr_newdt);
        $this->session->set_flashdata('message',  'Role details Updated..!');
        redirect('General/role');

       }
        else 

        {
           redirect('General/role');
        }                   
}
  

  
 public function del_entry4()
  {
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_role($id);
            if($result)
            {
                    $this->session->set_flashdata('message', 'Role Removed..!');
        redirect('General/role');            }
           else
           {
            $this->session->set_flashdata('message', 'Please try again.');
      redirect('General/Role');
           }
  }
//Agents details

public function agents()
{
	$this->load->view("admin/agents");
}

public function add_agent()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('agent_name', 'Agent Name', 'required');
$this->form_validation->set_rules('agent_code', 'Agent Name', 'required');
  if($this->form_validation->run())
  {
      $agent = array(
         'agent_code'  =>  $this->input->post("agent_code"),
         'agent_name'  =>  $this->input->post("agent_name"),
         'agent_address'  =>  $this->input->post("agent_address"),
         'agent_mobile'  =>  $this->input->post("agent_mobile"),
         'agent_email'  =>  $this->input->post("agent_email"),
         "agent_status" => "1"
      );
      $this->load->Model('Model_general');
      $this->Model_general->insert_agent($agent);
      $this->session->set_flashdata('message', 'Agent details added.');
        
       redirect('General/agents'); 
     
  }
 else{
       $this->session->set_flashdata('message1', 'Agent details missing.!');
       redirect('General/role');
       }  
}


public function add_agent1()
{
		 $acode=$this->input->post("acode");
         $aname=$this->input->post("aname");
         $aaddress=$this->input->post("aaddress");
         $amobile=$this->input->post("amobile");
         $aemail=$this->input->post("aemail");
 		 
      $agent = array(
         'agent_code'=>$acode,
         'agent_name'=>$aname,
         'agent_address'=>$aaddress,
         'agent_mobile'=>$amobile,
         'agent_email'=>$aemail,
         "agent_status"=> "1"
      );
	  
	  if($acode!="" || $aname!="" || $aaddress!="" || $amobile!="" || $aemail!="")
	  {
      $this->load->Model('Model_general');
      $this->Model_general->insert_agent($agent);
      //$this->session->set_flashdata('message', 'Agent Created Success Fully');
	  echo $this->db->insert_id();
	  }
	  else
	  {
		  echo "0";
	  }
}

public function agent_ajax()
{
        $this->load->model('Model_general');
        $results=$this->Model_general->view_agents();
    
        $data1 = array();
        foreach($results  as $r) 
        {
			$edit="<a href='#myModal2' id='$r->agent_id' data-toggle='modal'   class='edit open-AddBookDialog2'>
			<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
			$mm='<center><button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button></center>';
			$del=anchor('General/del_entry5/'.$r->agent_id, $mm, array('id' =>'del_conf'));
				 
			if($r->agent_status==1)
			{
			  $st="<font color='green'>Active</font>";
			}
			else
			{
			  $st="<font color='red'>Inactive</font>";
			}
                      array_push($data1, array(
                            "edit"=>"$edit",
                            "delete"=>"$del",
							"code"=>"$r->agent_code",
                            "name"=>"$r->agent_name",
							"address"=>"$r->agent_address",
							"mobile"=>"$r->agent_mobile",
							"email"=>"$r->agent_email",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}



public function agentlist_ajax()
{
        $this->load->model('Model_general');
        $results=$this->Model_general->view_agents();
    
        $data1 = array();
        foreach($results  as $r) 
        {
			$select="<a href='#myModalSA' id='$r->agent_id' data-toggle='modal'  class='agselect open-AddBookDialog2'><center><button class='btn btn-warning btn-xs' data-title='select' data-toggle='modal' >Select</button></center></a>";
			 
                      array_push($data1, array(
                            "select"=>"$select",
							"agid"=>"$r->agent_id",
                           	"agname"=>"$r->agent_name",
                            "agmobile"=>"$r->agent_mobile",
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}


public function edit_agent() 
  {
  $data=$this->session->all_userdata();
  $id=$this->input->post('id');
  $query = $this->db->select('*')->from('agents')->where('agent_id',$id)->get();
    $row = $query->row();
     
     echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Agent</font></h4>
    </div>
    <br>

    <form onsubmit='return checkdata1();'  class='form-horizontal' role='form' method='post' action='". site_url('General/update_agent'). "' enctype='multipart/form-data'>
                    <!-- text input -->
            <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Agent Code : </label>
                    <div class='col-md-9'>  
          <input type='hidden' name='id'  value='$row->agent_id'/>
          <input type='text' name='code' class='form-control'  id='mask_date2' value='$row->agent_code' required/>
          </div>                   
          </div>
          <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Name : </label>
                    <div class='col-md-9'>  
         <input type='text' name='name' class='form-control'  id='mask_date2' value='$row->agent_name' required/>
          </div>                   
          </div>

          <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Address : </label>
                    <div class='col-md-9'>  
         <textarea rows=2 cols=30 class='form-control'  name='address' required>$row->agent_address</textarea>
          </div>                   
          </div>

          <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Mobile : </label>
                    <div class='col-md-9'>  
         <input type='text' name='mobile' id='mobile1' pattern='[0-9]*' class='form-control'  id='mask_date2' value='$row->agent_mobile' required/>
          <label id='mobmessage1' style='color:red;'></label>
          </div>                   
          </div>

          <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Email : </label>
                    <div class='col-md-9'>  
         <input type='email' name='email' class='form-control'  id='mask_date2' value='$row->agent_email' required/>
          </div>                   
          </div>
           
                    
            <div class='form-group'>
                      <label class='col-md-3  control-label'>Status: </label>
                     <div class='col-md-9'>
                      <select type='text' name='status' class='form-control'>";
            echo '<option value="0" ';if($row->agent_status=='0')echo "selected"; echo'>Inactive</option>
              <option value="1"';if($row->agent_status=='1')echo "selected"; echo ">Active</option>
              </select>
                        </div>                     
                  </div>
     

               <label style='background-color:#cecece ;width:100%;height:1px;''></label>
                <div class='form-group'> 
                <div class='row'>
                <div class='col-md-7'>
                </div>
                <div class='col-md-4'>
                <input type='submit' class='btn btn-primary' value='Update'/>
                   <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>
            <script type='text/javascript'>
    
     function checkdata1()
  {
   var mob=$('#mobile1').val();
   if(mob.length<10 || mob.length>10)
    {
    $('#mobmessage1').show();
      $('#mobmessage1').html('Invalid mobile no,10 digits only.');
      return false;
    }
   else
    {
    $('#mobmessage1').hide();
    $('#mobmessage1').html('');
     
      return true;
    }
  }

</script> 
    ";  
 
  }

  
  
  
public function update_agent()
         {
$this->load->library('form_validation');
$this->form_validation->set_rules('code', 'code', 'required');
           if($this->form_validation->run())
                    {
    
    $id=$this->input->post('id');
   $agent = array(
         'agent_code'  =>  $this->input->post("code"),
         'agent_name'  =>  $this->input->post("name"),
         'agent_address'  =>  $this->input->post("address"),
         'agent_mobile'  =>  $this->input->post("mobile"),
         'agent_email'  =>  $this->input->post("email"),
         "agent_status" =>  $this->input->post("status")
      );
         $this->load->model('Model_general');
        $this->Model_general->update_agent($id,$agent);
        $this->session->set_flashdata('message',  'Agent details Updated.!');
        redirect('General/agents');

       }
        else 

        {
		 $this->session->set_flashdata('message1',  'Agent details missing.!');
           redirect('General/agents');
        }                   
}
 
 
 public function del_entry5()
  {
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_agent($id);
            if($result)
            {
                    $this->session->set_flashdata('message', 'Agent Removed..!');
        redirect('General/agents');            }
           else
           {
            $this->session->set_flashdata('message1', 'Please try again.');
      redirect('General/agents');
           }
  }

// Branch management


public function branch()
{
	$this->load->view("admin/branches");
}


public function add_new_branch()
{
	echo '
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title"> <font color="blue">Add New Branch</font></h4>
		</div>
		<br>
	      <form onsubmit="return checkdata();" class="form-horizontal" role="form" method="POST" action="'.base_url("General/add_branch").'" enctype="multipart/form-data">
                  
						<div class="form-group">
							  <div class="row" >
							    <label class="col-md-3 control-label" style="padding-top:5px">Branch Code</label>
								 <div class="col-md-8">
								   <input type="text" class="form-control"  name="bcode" required>
								 </div>
							 </div>
                        </div>
 						<div class="form-group" >
						 <div class="row">
							 <label class="col-md-3 control-label" style="padding-top:5px">Name</label>
								 <div class="col-md-8">
								   <input type="text" class="form-control"  name="bname" required>
								 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							<div class="row">
							 <label class="col-md-3 control-label" style="padding-top:5px">Address</label>
								 <div class="col-md-8 ">
								   <textarea rows=2 cols=30 class="form-control"  name="baddress" required></textarea>
								 </div>
							 </div>
                        </div>
 
						 <div class="form-group">
												 
							 <div class="row">
								<label class="col-md-3 control-label" style="padding-top:5px">Mobile</label>
								 <div class="col-md-8 ">
								   <input type="text" class="form-control" pattern="[0-9]*"  name="bmobile" id="mobile" required>
								 <label id="mobmessage" style="color:red;"></label>
                 </div>
							 </div>
                        </div>
						
						 <div class="form-group">
				
							 <div class="row">
								<label class="col-md-3 control-label" style="padding-top:5px">Land phone</label>
								 <div class="col-md-8 ">
								   <input type="text" class="form-control" pattern="[0-9]*"  name="bphone" id="landline" required>
								  <label id="landmessage" style="color:red;"></label>
                 </div>
							 </div>
                        </div>
 
						<div class="form-group">
							 <div class="row">
							    <label class="col-md-3 control-label" style="padding-top:5px">Email</label>
								 <div class="col-md-8 ">
								   <input type="email" class="form-control"  name="bemail" required>
								 </div>
							 </div>
                        </div>
                     <hr>

                     <div class="form-group">
                     <div class="row">
                      <div class="col-md-2">
                      </div>
                               <div class="col-md-9 col-md-offset-3">
                                   <button type="submit" class="btn btn-primary">Submit</button>
								    <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-left:20px;">Close</button>
                               </div>
                      </div>
                    </div>
      </form>
         <script type="text/javascript">
     $("#mobmessage").hide();
     $("#landmessage").hide();  
  function checkdata()
  {
    
    var mob=$("#mobile").val();
    var landline=$("#landline").val();
      
  
    if(mob.length<10 || mob.length>10)
    {
    $("#mobmessage").show();
      $("#mobmessage").html("Invalid mobile no,10 digits only.");
      return false;
    }
    else if(landline.length<11 || landline.length>11)
    {
    $("#mobmessage").hide();
      $("#landmessage").show();
      $("#landmessage").html("Invalid mobile no,11 digits only.");
      return false;
    }
    
    
    else
    {
    $("#mobmessage").hide();
      $("#landmessage").hide();
      $("#mobmessage").html("");
      $("#landmessage").html("");
      return true;
    }
  }

</script> 
	';
}
 

public function add_branch()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('bname', 'bname', 'required');
$this->form_validation->set_rules('baddress', 'baddress', 'required');
$this->form_validation->set_rules('bmobile', 'bmobile', 'required');
$this->form_validation->set_rules('bphone', 'bphone', 'required');
$this->form_validation->set_rules('bemail', 'bemail', 'required');

  if($this->form_validation->run())
        {
      $bcode= $this->input->post('bcode');
	  $bname= $this->input->post('bname');
	  $badd= $this->input->post('baddress');
	  $bmobile= $this->input->post('bmobile');
	  $bphone= $this->input->post('bphone');
	  $bemail= $this->input->post('bemail');
	      
  	 $log=array(
                   'branch_code'=>$bcode,
                   'branch_name'=>$bname,
				   'branch_address'=>$bname,
				   'branch_mobile'=>$bmobile,
				   'branch_phone'=>$bphone,
				   'branch_email'=>$bemail,
				   'branch_status'=>'1',
                );
         $this->load->Model('Model_general');
        /*Calls Model to enter login details  in Login Table of the corresponding user*/
        $this->Model_general->insert_branch($log);
        $this->session->set_flashdata('message', 'Branch added..!');
        
       redirect('General/branch'); 
     
  }
 else{
       $this->session->set_flashdata('message', 'Category missing..!');
       redirect('General/role');
       }  
}

public function branch_ajax()
{
        $this->load->model('Model_general');
        $results=$this->Model_general->view_branches();
    
        $data1 = array();
        foreach($results  as $r) 
        {
			$edit="<a href='#myModal3' id='$r->branch_id' data-toggle='modal' class='edit open-AddBookDialog2'>
			<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
			$mm='<center><button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button></center>';
			$del=anchor('General/del_entry6/'.$r->branch_id, $mm, array('id' =>'del_conf'));
				 
			if($r->branch_status==1)
			{
			  $st="<font color='green'>Active</font>";
			}
			else
			{
			  $st="<font color='red'>Inactive</font>";
			}
                      array_push($data1, array(
                            "edit"=>"$edit",
                            "delete"=>"$del",
							"code"=>"$r->branch_code",
                            "name"=>"$r->branch_name",
							"address"=>"$r->branch_address",
							"mobile"=>"$r->branch_mobile",
							"phone"=>"$r->branch_phone",
							"email"=>"$r->branch_email",
                            "status"=>$st
                      ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}


public function edit_branch() 
  {
  $data=$this->session->all_userdata();
  $id=$this->input->post('id');
  $query = $this->db->select('*')->from('branches')->where('branch_id',$id)->get();
    $row = $query->row();
     
     echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Branches</font></h4>
    </div>
    <br>

    <form onsubmit='return checkdata();'  class='form-horizontal' role='form' method='post' action='". site_url('General/update_branch'). "' enctype='multipart/form-data'>
                    <!-- text input -->
            <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Branch Code : </label>
                    <div class='col-md-9'>  
          <input type='hidden' name='id'  value='$row->branch_id'/>
          <input type='text' name='bcode' class='form-control'  id='mask_date2' value='$row->branch_code' required/>
          </div>                   
          </div>

           <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Name : </label>
                    <div class='col-md-9'>  
        <input type='text' name='bname' class='form-control'  id='mask_date2' value='$row->branch_name' required/>
          </div>                   
          </div>
           <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Address : </label>
                    <div class='col-md-9'>  
      <textarea rows=2 cols=50 class='form-control'  name='baddress'  required>$row->branch_address</textarea>
          </div>                   
          </div>

            <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Mobile : </label>
                    <div class='col-md-9'>  
        <input type='text' name='bmobile' class='form-control' pattern='[0-9]*'  id='mobile' value='$row->branch_mobile' required/>
           <label id='mobmessage' style='color:red;'></label>                            
          </div>                   
          </div>
             <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Landline : </label>
                    <div class='col-md-9'>  
        <input type='text' name='blandline' class='form-control'  pattern='[0-9]*' id='landline' value='$row->branch_phone' required/>
           <label id='landmessage' style='color:red;'></label>                            
          </div>                   
          </div>

             <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Email : </label>
                    <div class='col-md-9'>  
        <input type='email' name='bemail' class='form-control'  id='mask_date2' value='$row->branch_email' required/>
          </div>                   
          </div>
           
                    
            <div class='form-group'>
                      <label class='col-md-3  control-label'>Status: </label>
                     <div class='col-md-9'>
                      <select type='text' name='status' class='form-control'>";
            echo '<option value="0" ';if($row->branch_status=='0')echo "selected"; echo'>Inactive</option>
              <option value="1"';if($row->branch_status=='1')echo "selected"; echo ">Active</option>
              </select>
                        </div>                     
                  </div>
     

               <label style='background-color:#cecece ;width:100%;height:1px;''></label>
                <div class='form-group'> 
                <div class='row'>
                <div class='col-md-7'>
                </div>
                <div class='col-md-4'>
                <input type='submit' class='btn btn-primary' value='Update'/>
                   <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>  

  <script type='text/javascript'>
     $('#mobmessage').hide();
     $('#landmessage').hide();  
  function checkdata()
  {
    
    var mob=$('#mobile').val();
    var landline=$('#landline').val();
      
  
    if(mob.length<10 || mob.length>10)
    {
    $('#mobmessage').show();
      $('#mobmessage').html('Invalid mobile no,10 digits only.');
      return false;
    }
    else if(landline.length<11 || landline.length>11)
    {
    $('#mobmessage').hide();
      $('#landmessage').show();
      $('#landmessage').html('Invalid mobile no,11 digits only.');
      return false;
    }
    
    
    else
    {
    $('#mobmessage').hide();
      $('#landmessage').hide();
      $('#mobmessage').html('');
      $('#landmessage').html('');
      return true;
    }
  }

</script> 

           ";
  }

  
  
public function update_branch()
         {
$this->load->library('form_validation');
$this->form_validation->set_rules('bname', 'bname', 'required');
$this->form_validation->set_rules('baddress', 'baddress', 'required');
$this->form_validation->set_rules('bmobile', 'bmobile', 'required');
$this->form_validation->set_rules('blandline', 'blandline', 'required');
$this->form_validation->set_rules('bemail', 'bemail', 'required');
           if($this->form_validation->run())
                    {
    $this->load->model('Model_general');
   
   $bcode= $this->input->post('bcode');
    $bname= $this->input->post('bname');
    $badd= $this->input->post('baddress');
    $bmobile= $this->input->post('bmobile');
    $bphone= $this->input->post('blandline');
    $bemail= $this->input->post('bemail');
    $status= $this->input->post('status');
    $id= $this->input->post('id');
        
     $log=array(
                   'branch_code'=>$bcode,
                   'branch_name'=>$bname,
                   'branch_address'=>$bname,
           'branch_mobile'=>$bmobile,
           'branch_phone'=>$bphone,
           'branch_email'=>$bemail,
           'branch_status'=>$status
                );
         $this->load->Model('Model_general');
        $this->Model_general->update_branch($id,$log);
        $this->session->set_flashdata('message',  'Branch details Updated..!');
        redirect('General/branch');
       }
        else 

        {
           redirect('General/branch');
        }                   
}
  
 public function del_entry6()
  {
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_branch($id);
            if($result)
            {
                    $this->session->set_flashdata('message', 'Branch Removed..!');
        redirect('General/branch');            }
           else
           {
            $this->session->set_flashdata('message', 'Please try again.');
      redirect('General/branch');
           }
  }
 
   public function get_agentsDT()
{
	$id=$this->input->post('aid');
	$this->load->model('Model_general');
    $result=$this->Model_general->view_agentsDT($id);
	foreach($result as $r)
	{
	     $data1=array(
					"agname"=>$r->agent_name,
                    "agid"=>$r->agent_id,
          );
	}
         //echo json_encode(array('data' => $data1));
		 echo json_encode($data1);
}



public function vehicletype()
{
	$this->load->view("admin/add_vehicletypes");
}



public function add_vehtype()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('vtype', 'vehicle type', 'required');

if($this->form_validation->run())
   {
   $this->load->model('Model_general');
   
  $vtype= $this->input->post('vtype');

			$log=array(
                   'veh_type_name'=>strtoupper($vtype),
                );
       //  $this->load->Model('Model_general');
        $this->db->insert("vehicle_types",$log);
        $this->session->set_flashdata('message',  'Vehicle Type Added.!');
        redirect('General/Vehicletype');
       }
        else 

        {
		   $this->session->set_flashdata('message',  'Vehicle Type Missing.!');
           redirect('General/Vehicletype');
        }                   
}

public function vtype_ajax()
{
        $this->load->model('Model_general');
        $results=$this->Model_general->view_vehicletypes();
		
        $data1 = array();
        foreach($results  as $r) 
               {
               	$edit="<a href='#myModal2' id='$r->veh_type_id' data-toggle='modal' class='edit open-AddBookDialog2'>
				<center><button class='btn btn-default btn-xs' data-title='edit' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
				$mm='<center><button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button></center>';
				$del=anchor('General/del_entry7/'.$r->veh_type_id, $mm, array('id' =>'del_conf'));
             
                array_push($data1, array(
                    "edit"=>"$edit",
                    "delete"=>"$del",
					"vid"=>"$r->veh_type_id",
                    "vtype"=>"$r->veh_type_name",
                  ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

public function del_entry7()
  {
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_vtype($id);
            if($result)
            {
            $this->session->set_flashdata('message', 'Vehicle type Removed..!');
			redirect('General/vehicletype');            }
           else
           {
            $this->session->set_flashdata('message', 'Please try again.');
			redirect('General/vehicletype');
           }
  }

public function edit_vtype() 
  {
  $id=$this->input->post('id');
  $query = $this->db->select('*')->from('vehicle_types')->where('veh_type_id',$id)->get();
    $row = $query->row();
     
     echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Vehicle Type</font></h4>
    </div>
    <br>

    <form  class='form-horizontal' role='form' method='post' action='". site_url('General/update_vtype'). "' enctype='multipart/form-data'>
                    <!-- text input -->
            <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Vehicle Type : </label>
                    <div class='col-md-9'>  
          <input type='hidden' name='id'  value='$row->veh_type_id'/>
          <input type='text' name='vtype' class='form-control'  value='$row->veh_type_name' required/>
          </div>                   
          </div>
           
  

               <label style='background-color:#cecece ;width:100%;height:1px;''></label>
                <div class='form-group'> 
                <div class='row'>
                <div class='col-md-7'>
                </div>
                <div class='col-md-4'>
                <input type='submit' class='btn btn-primary' value='Update'/>
                   <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>  ";
  }
public function update_vtype()
         {
$this->load->library('form_validation');
$this->form_validation->set_rules('vtype', 'vtype', 'required');

  if($this->form_validation->run())
                    {
    $this->load->model('Model_general');
   
    $vtype= $this->input->post('vtype');
    $id= $this->input->post('id');
        
				$log=array(
                   'veh_type_name'=>$vtype,
                );
				
        $this->load->Model('Model_general');
        $this->Model_general->update_vtype($id,$log);
        $this->session->set_flashdata('message',  'Vehicle type updated.!');
        redirect('General/vehicletype');
       }
        else 

        {
		  $this->session->set_flashdata('message',  'please try again.!');
           redirect('General/vehicletype');
        }                   
}

public function add_duty()
{
	$this->load->view('admin/add_dutylist');
}

  public function get_driver_mobile()
   {
	$drid=$this->input->post('drid');   
	$row=$this->db->select('driver_mobile')->from('driverregistration')->where('driver_id',$drid)->get()->row();
	echo $row->driver_mobile;
  }

public function add_duty_entry()
{
	$this->load->library('form_validation');
	$this->form_validation->set_rules('drname', 'name', 'required');
	$this->form_validation->set_rules('ddate', 'duty date', 'required');
	$this->form_validation->set_rules('drmobile', 'mobile', 'required');
	$this->form_validation->set_rules('drshift', 'shift', 'required');
	$this->form_validation->set_rules('drtime', 'time', 'required');
	
	if($this->form_validation->run())
     {
	$ddate= $this->input->post('ddate');
	$drname= $this->input->post('drname');
    $drmobile= $this->input->post('drmobile');
    $drshift= $this->input->post('drshift');
    $drtime= $this->input->post('drtime');
    
	$id= $this->input->post('id');
      $d1=explode("-",$ddate);
	  $dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	  
     $log=array(
		   'drv_date'=>$dt1,
           'drv_name'=>$drname,
           'drv_mobile'=>$drmobile,
           'drv_shift'=>$drshift,
           'drv_time'=>$drtime,
             );
        if($this->db->insert("driver_schedule",$log))
		{
        $this->session->set_flashdata('message',  'Duty details added..!');
        redirect('General/add_duty');
		}
	 }
     else 
	 {
	   $this->session->set_flashdata('message1',  'Details Missing..!');
       redirect('General/add_duty');
     }                   
}

public function drv_duty_ajax()
{
  
  $this->load->Model('Model_general');
  $results=$this->Model_general->view_driver_duty();
				
        $data1 = array();
        foreach($results  as $r) 
               {
               	$edit="<a href='#myModal2' id='$r->drv_id' data-toggle='modal'  class='edit open-AddBookDialog2'>
				<button class='btn btn-default btn-xs' data-title='edit' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></a>";
				$mm='<button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button>';
				$del=anchor('General/del_duty_entry/'.$r->drv_id, $mm, array('id' =>'del_conf'));
             
			 if($r->drv_shift=='1')
				 $st='DAY';
			 else if($r->drv_shift=='2')
				 $st='NIGHT';
			 ELSE
				 $st="<font color=red>OFF</font>";
			 		 
			 if($r->drv_date==date('Y-m-d'))
					$ddt="<b>".date_format(date_create($r->drv_date),"d-m-Y")."</b>";
				else
					$ddt=date_format(date_create($r->drv_date),"d-m-Y");
	 
	 
                array_push($data1, array(
                    "edit"=>$edit."&nbsp;".$del,
                    "drid"=>"$r->drv_id",
					"ddate"=>$ddt,
					"dname"=>"$r->driver_name",
					"dmobile"=>"$r->drv_mobile",
					"dshift"=>$st,
					"dtime"=>"$r->drv_time",
                  ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

public function drv_duty_edit()
{
	$id=$this->input->post('id');
	$row=$this->db->select('*')->from("driver_schedule")->where('drv_id',$id)->get()->row();
	
	echo '
		<form class="form-horizontal" role="form" method="POST" action="'. base_url('General/update_duty_entry') .'" enctype="multipart/form-data">
                 
				 <input type="hidden" class="form-control"  name="drid"  value="'. $row->drv_id .'">
				  
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label left1">Driver Name :</label>
								<div class="col-md-7">
									<input type="date" class="form-control" name="ddate" value="'.$row->drv_date.'" required >
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label left1">Driver Name :</label>
								<div class="col-md-7">
								
								<select class="form-control" placeholder="Choose Purpose" name="drname" id="drname">
								  <option value="">----------</option>';
								  
									
										$vt=$this->db->select('driver_id,driver_name')->from("driverregistration") ->get()->result();
										
										foreach($vt as $v)
										{
										 echo"<option value='". $v->driver_id ."'"; if($v->driver_id==$row->drv_name) echo "selected"; echo ">".strtoupper($v->driver_name)."</option>";
										}
										
						echo '	</select>

								</div>
							</div>
						</div>
								
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label left1">Mobile :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="drmobile" value="'. $row->drv_mobile .'" required>
								</div>
							</div>
						</div>		
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label left1">Shift :</label>
								<div class="col-md-7">
								<select name="drshift" id="drshift" class="form-control" required>
								<option value="">---------</option>
								<option value="1"'; if($row->drv_shift=='1') echo "selected"; echo '>DAY</option>
								<option value="2" '; if($row->drv_shift=='2') echo "selected"; echo '>NIGHT</option>
								</select>
								
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label left1">Shift Time :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="drtime" id="drtime" value="'. $row->drv_time .'" required>
								</div>
							</div>
						</div>
						
                       <hr>

                    <div class="form-group">
                     <div class="row">
                      <div class="col-md-2 left1">
                      </div>
                               <div class="col-md-9" style="text-align:right;">
                                   <button type="submit" id="idsubmit" class="btn btn-primary">Update</button>
								   &nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                               </div>
                        </div>
                       </div>
                        </form>';
						
}

public function update_duty_entry()
{
	$this->load->library('form_validation');
	
	$this->form_validation->set_rules('drname', 'name', 'required');
	$this->form_validation->set_rules('drmobile', 'mobile', 'required');
	$this->form_validation->set_rules('drshift', 'shift', 'required');
	$this->form_validation->set_rules('drtime', 'time', 'required');
	
	if($this->form_validation->run())
     {
	$drname= $this->input->post('drname');
    $drmobile= $this->input->post('drmobile');
    $drshift= $this->input->post('drshift');
    $drtime= $this->input->post('drtime');
    
	$id= $this->input->post('drid');
        
     $log=array(
           'drv_name'=>$drname,
           'drv_mobile'=>$drmobile,
           'drv_shift'=>$drshift,
           'drv_time'=>$drtime,
             );
        if($this->db->where('drv_id',$id)->update("driver_schedule",$log))
		{
        $this->session->set_flashdata('message',  'Duty details updated..!');
        redirect('General/add_duty');
		}
	 }
     else 
	 {
	   $this->session->set_flashdata('message1',  'Details Missing..!');
       redirect('General/add_duty');
     }                   
}

public function del_duty_entry()
	{
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_drv_duty($id);
            if($result)
            {
			$this->session->set_flashdata('message2', 'Driver duty Removed..!');
			redirect('General/add_duty');  
			}
           else
           {
            $this->session->set_flashdata('message1', 'Please try again.');
			redirect('General/add_duty');
           }
  }

public function drivers_duty()
{
	$op=$this->uri->segment(3);
	$md="";
	
	$this->load->Model('Model_general');
	$yr=date('Y');
	$mon=['January','February','March','April','May','June','July','August','September','October','November','December'];
	
	if($op==1)
	{
	$date=$this->input->post('startdate');
	$d1=explode("-",$date);
	$md=$d1[2]."-".$d1[1]."-".$d1[0];
	$data['sop']=$md;
	}
	else if($op==2)
	{
	$md=$this->input->post('month');
	$yr=$this->input->post('syear');
	$data['sop']=$mon[$md-1]." - ".$yr;
	}
	else if($op==null)
	{
	 $md=date('F');	
	 $data['sop']=$md." -" .$yr;
	}
	$data['dresult']=$this->Model_general->view_driver_duty1($op,$md,$yr);
	
	$this->load->view('admin/view_driver_dutylist',$data);
}
  
public function fare_list()
{
	$data['facat']=1;
	$data['ftab']=1;
	$this->load->model('Model_general');
    $data['result'] =$this->Model_general->view_farelist();
	$this->load->view('admin/add_farelist',$data);
}

public function farelist()
{
	$data['ftab']=$this->session->userdata('faretab');
	$data['facat']=$this->session->userdata('farecat');
	$this->load->model('Model_general');
    $data['result'] =$this->Model_general->view_farelist();
	$this->load->view('admin/add_farelist',$data);
}

public function edit_fare()
{
	$this->session->set_userdata('faretab','2');
	$data['facat']=$this->session->userdata('farecat');
	$id=$this->uri->segment(3);
	$data['result']=$this->db->select('*')->from("fare_list")->where('fare_id',$id)->get()->row();
	$this->load->view('admin/edit_farelist',$data);
}

public function add_fare_list()
{
	$this->load->library('form_validation');
	$this->form_validation->set_rules('fcat', 'fare category', 'required');
	$this->form_validation->set_rules('coname', 'name', 'required');
	$this->form_validation->set_rules('vehicle', 'mobile', 'required');
	$this->form_validation->set_rules('farehrs', 'fare hrs', 'required');
	$this->form_validation->set_rules('farekm', 'kilometer', 'required');
	$this->form_validation->set_rules('amount', 'amount', 'required');
	
	if($this->form_validation->run())
    {
	$tripop= $this->input->post('tripop');	
	$facat= $this->input->post('fcat');
	$coname= $this->input->post('coname');
    $vehicle= $this->input->post('vehicle');
	$hrs= $this->input->post('farehrs');
    $farekm= $this->input->post('farekm');
    $amount= $this->input->post('amount');
		
		if($tripop=="")
		{
			$tripop="0";
		}
		    
     $log=array(
		   'fare_trip'=>$tripop,
		   'fare_category'=>$facat,
           'fare_corporate'=>$coname,
           'fare_vehicle'=>$vehicle,
		   'fare_description'=>$hrs,
           'fare_km'=>$farekm,
           'fare_amount'=>$amount,
             );
        if($this->db->insert("fare_list",$log))
		{
		$this->session->set_userdata('farecat',$facat);
		$this->session->set_userdata('faretab','1');
		
        $this->session->set_flashdata('message',  'Fare details added..!');
        redirect('General/farelist');
		}
	 }
     else 
	 {
	   $this->session->set_userdata('farecat',$facat);
	   $this->session->set_userdata('faretab','1');
	   $this->session->set_flashdata('message1',  'Details Missing..!');
       redirect('General/farelist');
     }                   
}


public function update_fare()
{
	$this->load->library('form_validation');
	$this->form_validation->set_rules('fcat', 'fare category', 'required');
	$this->form_validation->set_rules('coname', 'name', 'required');
	$this->form_validation->set_rules('vehicle', 'mobile', 'required');
	$this->form_validation->set_rules('farehrs', 'fare hrs', 'required');
	$this->form_validation->set_rules('farekm', 'kilometer', 'required');
	$this->form_validation->set_rules('amount', 'amount', 'required');
	
	if($this->form_validation->run())
    {
	$fid= $this->input->post('fareid');	
	$tripop= $this->input->post('tripop');	
	$facat= $this->input->post('fcat');
	$coname= $this->input->post('coname');
    $vehicle= $this->input->post('vehicle');
	$hrs= $this->input->post('farehrs');
    $farekm= $this->input->post('farekm');
    $amount= $this->input->post('amount');
		
		if($tripop=="")
		{
			$tripop="0";
		}
		    
     $log=array(
		   'fare_trip'=>$tripop,
		   'fare_category'=>$facat,
           'fare_corporate'=>$coname,
           'fare_vehicle'=>$vehicle,
		   'fare_description'=>$hrs,
           'fare_km'=>$farekm,
           'fare_amount'=>$amount,
             );
			 
	    if($this->db->where('fare_id',$fid)->update("fare_list",$log))
		{
		$this->session->set_userdata('farecat',$facat);
		$this->session->set_userdata('faretab','2');
        $this->session->set_flashdata('message',  'Fare details updated..!');
        redirect('General/farelist');
		}		 
	}
     else 
	 {
		$this->session->set_userdata('farecat',$facat);
		$this->session->set_userdata('faretab','2');
	   $this->session->set_flashdata('message1',  'Details Missing,Try Again.');
       redirect('General/farelist');
     }                   
}


public function del_fare_entry()
{
	$id=$this->uri->segment(3, 0);
	$this->load->model('Model_general');
	$result=$this->Model_general->delete_fare_list($id);
	if($result)
	{
		$this->session->set_userdata('faretab','2');
		$this->session->set_flashdata('message2', 'Fare details Removed..!');
		redirect('General/farelist');  
	}
    else
    {
		$this->session->set_userdata('faretab','2');
		$this->session->set_flashdata('message1', 'Please try again.');
		redirect('General/farelist');
    }
}

// expense type entry

public function Expense_type()
{
	$this->load->view("admin/add_vehicle_EXPtypes");
}

public function add_expense_type()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('extype', 'vehicle expense type', 'required');
$this->form_validation->set_rules('expgroup', 'expense group', 'required');

if($this->form_validation->run())
   {
   $this->load->model('Model_general');
   $extype= $this->input->post('extype');
   $exgroup= $this->input->post('expgroup');
   $exunder= $this->input->post('exunder');

			$log=array(
                   'etype_name'=>strtoupper($extype),
				   'etype_group'=>$exgroup,
                );
       //  $this->load->Model('Model_general');
        $this->db->insert("expense_type",$log);
		$insid=$this->db->insert_id();
		
		$data1=array(
			'ledg_name'=>strtoupper($extype),
			'ledg_group'=>$exunder,
			'ledg_exptype'=>$insid,
			);
			
		$this->db->insert("ac_ledgers",$data1);

        $this->session->set_flashdata('message',  '1#Expense Type Added.!');
        redirect('General/Expense_type');
       }
        else 

        {
		   $this->session->set_flashdata('message',  '4#Expense Type Missing.!');
           redirect('General/Expense_type');
        }                   
}

public function exptype_ajax()
{
    $this->load->model('Model_general');
     $results=$this->Model_general->view_expensetypes();
	
        $data1 = array();
        foreach($results  as $r) 
               {
               	$edit="<a href='#myModal2' id='$r->etype_id' data-toggle='modal' class='edit open-AddBookDialog2'>
				<button class='btn btn-default btn-xs' data-title='edit' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></a>";
				$mm='<button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button>';
				$del=anchor('General/del_entry_exp/'.$r->etype_id, $mm, array('id' =>'del_conf'));
				
				if($r->etype_group==1)	{ $egroup='VEHICLE'; } ELSE{ $egroup='GENERAL'; }
							 
                array_push($data1, array(
                    "action"=>"<center>".$edit." ".$del."<center>",
					"exid"=>"$r->etype_id",
					"exgroup"=>$egroup,
                    "extype"=>"$r->etype_name",
                  ));
             }
          echo json_encode(array('data' => $data1));
		}

public function del_entry_exp()
  {
            $id=$this->uri->segment(3, 0);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_exptype($id);
            if($result)
            {
            $this->session->set_flashdata('message', '3#Expense type Removed..!');
			redirect('General/Expense_type');            }
           else
           {
            $this->session->set_flashdata('message1', '4#Please try again.');
			redirect('General/Expense_type');
           }
  }

public function edit_extype() 
  {
  $id=$this->input->post('id');
  $query = $this->db->select('*')->from('expense_type')->where('etype_id',$id)->get();
    $row = $query->row();
     
     echo "             
     <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
         <h4 class='modal-title' > <font color='blue'>Edit Expense Type</font></h4>
    </div>
    <br>

    <form  class='form-horizontal' role='form' method='post' action='". site_url('General/update_exptype'). "' enctype='multipart/form-data'>
                    <!-- text input -->
            <div class='form-group'>
                     <label class='col-md-3 control-label' style='padding-top:10px;'>Expense Type : </label>
                    <div class='col-md-9'>  
          <input type='hidden' name='id'  value='$row->etype_id'/>
          <input type='text' name='extype' class='form-control'  value='$row->etype_name' required/>
          </div>                   
          </div>
           

               <label style='background-color:#cecece ;width:100%;height:1px;''></label>
                <div class='form-group'> 
                <div class='row'>
                <div class='col-md-7'>
                </div>
                <div class='col-md-4'>
                <input type='submit' class='btn btn-primary' value='Update'/>
                   <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
                </div>                                
           </form>  ";
  }
public function update_exptype()
         {
$this->load->library('form_validation');
$this->form_validation->set_rules('extype', 'extype', 'required');

  if($this->form_validation->run())
                    {
    $this->load->model('Model_general');
   
    $extype= $this->input->post('extype');
    $id= $this->input->post('id');
        
				$log=array(
                   'etype_name'=>strtoupper($extype),
                );
				
        $this->load->Model('Model_general');
        $this->Model_general->update_exptype($id,$log);
        $this->session->set_flashdata('message',  '2#Expense type updated.!');
        redirect('General/Expense_type');
       }
        else 

        {
		  $this->session->set_flashdata('message',  '4#please try again.!');
           redirect('General/Expense_type');
        }                   
}

// update GST % ----------------------------

public function update_GST()
  {	
	$this->load->library('form_validation');
	$this->form_validation->set_rules('gstper', 'GST percentage', 'required');
  
  if($this->form_validation->run())
                    {
    
    $gper= $this->input->post('gstper');
	$id='1';
				$gst=array(
                   'gen_gst'=>$gper,
                );
				
        $this->load->Model('Model_general');
        $this->Model_general->update_GST($id,$gst);
        $this->session->set_flashdata('message',  'GST(%) Updated.!');
        redirect('General/options');
       }
        else 

        {
		  $this->session->set_flashdata('message1',  'Value Missing,try again.!');
           redirect('General/options');
        }                   
}
//------------------------------------
// Registration documents ------------------------------------------------------------------------------------>


public function Document()
{
	$this->session->set_userdata("doctab","1");
	$this->load->Model('Model_general');
    $data['result']=$this->Model_general->get_vehicle();
	$this->load->view('admin/doc_registration',$data);
}

public function Documents()
{
	$this->session->set_userdata("doctab","2");
	$this->load->Model('Model_general');
    $data['result']=$this->Model_general->get_vehicle();
	$this->load->view('admin/doc_registration',$data);
}


public function get_vehicle_type()
{
	$regn=$this->input->post('regno');
	$this->load->Model('Model_general');
    $result=$this->Model_general->get_vehicle();
	foreach($result as $r1)
	{
		if($r1->vehicle_id==$regn)
		{
			$dt=$r1->veh_type_id . ",". $r1->veh_type_name;
		}
	}
	echo $dt;
}

public function doc_ajax()
{
	$vid=$this->uri->segment(3);
	$this->load->model('Model_general');
	$results=$this->Model_general->get_documents($vid);
	
	$data1 = array();
	foreach($results  as $r) 
	   {
		$edit="<a href='#myModal2' id='$r->doc_id' data-toggle='modal' class='edit'>
		<button class='btn btn-info btn-xs' data-title='edit' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";
		$mm='<button class="btn btn-warning btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button>';
		$del=anchor('General/delete_document/'.$r->doc_id, $mm, array('id' =>'del_conf'));
	 
		/*array_push($data1, array(
			"edit"=>$edit." ".$del,
			"vehicle"=>"<b>Reg No</b> : ".$r->vehicle_regno."<br> <b>Type</b> : ".$r->veh_type_name,
			"docna"=>strtoupper($r->doc_document)."<br> <b>No</b> : ".$r->doc_document_no,
			"validity"=>"<b>From</b> : ".$r->doc_valid_from."<br> <b>To</b> : ".$r->doc_valid_to,
			"reminder"=>$r->doc_remainder. " days",
		  ));*/
		  
		array_push($data1, array(
			"edit"=>$edit." ".$del,
			"vehicle"=>$r->vehicle_regno,
			"vtype" =>$r->veh_type_name,
			"docna"=>strtoupper($r->doc_document),
			"docno"=>$r->doc_document_no,
			"vfrom"=>$r->doc_valid_from,
			"vto"=>$r->doc_valid_to,
			"reminder"=>$r->doc_reminder. " days",
		  ));
		    
		  
	  }
	echo json_encode(array('data' => $data1));
}

public function add_documents()
{
	$vfrom=$this->input->post('vfrom');
	$vto=$this->input->post('vto');
	
	
	
	$d1=explode("-",$vfrom);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	
	$d2=explode("-",$vto);
	$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
	
	
	 $data1 = array(
        'doc_vehicle_id' => $this->input->post('regno'),
        'doc_vehicle_type' => $this->input->post('typeid'),
        'doc_document' => $this->input->post('docna'),
        'doc_document_no' => $this->input->post('docno'),
        'doc_valid_from' => $dt1,
        'doc_valid_to' => $dt2,
        'doc_reminder' => $this->input->post('remind'),
        'doc_status' => 1
        );

	    $this->db->insert('vehicle_documents',$data1);
        if($this->db->affected_rows() > 0)
		{
    		echo "success";
		}
		else
		{
			echo "fail";	
		}
}

public function edit_document()
{
	$id=$this->input->post('id');
	$this->load->model('Model_general');
	$row=$this->Model_general->edit_doc($id);
		
	echo
	'<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
                  <h4 class="modal-title">Edit Document</h4>
                </div>
				
				<form class="form-horizontal" role="form" method="POST" action="'. base_url('General/update_doc').'"  enctype="multipart/form-data">
					<div class="form-group" style="margin-top:10px;" >
					<input type="hidden" class="typeid form-control"  name="docid" id="docid" value="'.$row->doc_id .'" required>
                     <div class="row">
                      <label class="col-md-4 control-label left1">Registration No</label>
                          <div class="col-md-6">
						  <select name="regno" class="regno form-control" id="regno" required>
						  <option value="">----------</option>';
						  
						  $result=$this->db->select('vehicle_id,vehicle_regno')->from('vehicle_registration')->get()->result();
						  foreach($result as $r1)
						   {
							   echo "<option value='". $r1->vehicle_id."'"; if($r1->vehicle_id==$row->doc_vehicle_id) echo "selected"; echo ">".$r1->vehicle_regno."</option>";
						   }
					
						   echo '
						   </select>
						   </div>
                        </div>
                     </div>
					 

                     <div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Vehicle</label>
                           <div class="col-md-6">';
						   
						    $row1=$this->db->select("veh_type_id,veh_type_name")->from("vehicle_types")->where("veh_type_id",$row->veh_type_id)->get()->row();
						   echo '
                            <input type="text" class="vehicle form-control"  name="vehiclena" id="vehiclena" value="'.$row1->veh_type_name .'" required>
							 <input type="hidden" class="typeid form-control"  name="typeid" id="typeid" value="'.$row1->veh_type_id .'" required>
                           </div>
                        </div>
                     </div>
					 
									 
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Document</label>
                           <div class="col-md-6">
                            <input type="text" class="form-control"  name="docna" id="docna"  value="'.$row->doc_document .'" required>
                           </div>
                        </div>
                    </div>
					
					 
					 <div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Document No</label>
                           <div class="col-md-6">
                            <input type="text" class="form-control"  name="docno" id="docno" value="'.$row->doc_document_no .'" required>
                           </div>
                        </div>
                     </div>
					 					 
					 <div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Validity From</label>
                           <div class="col-md-4">
								<input type="date" class="form-control" name="vfrom" value="'.$row->doc_valid_from .'" required />
                           </div>
                        </div>
                     </div>
					 
					<div class="form-group">
                    <div class="row">
                      <label class="col-md-4 control-label left1">Validity To</label>
                           <div class="col-md-4">
                            <input type="date" class="form-control" name="vto"  id="datepicker2" value="'.$row->doc_valid_to.'" required />
                           </div>
                    </div>
                    </div>
										 
					<div class="form-group">
                    <div class="row">
                     <label class="col-md-4 control-label left1">Remainder Period</label>
                           <div class="col-md-2">
                            <input type="text" class="form-control"  name="remind" id="remind" value="'.$row->doc_reminder .'" required>
                           </div>
						   <label class="col-md-1 control-label" style="text-align:left;padding:5px 0px;">/Days</label>
                    </div>
                    </div>
					 
                    <hr style="margin:10px 0px 10px 0px;">
 
                    <div class="form-group">
                    <div class="row">
                        <div class="col-md-10" align="right">
                          <button type="submit" class="btn_add btn btn-primary"  style="padding-left:5px;"><span class="glyphicon glyphicon-ok-sign"></span> Update Details</button>

                         &nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  								  
                        </div>
                    </div>
                    </div>

                    </form>
					';
 }
 
public function update_doc()
{
	$vfrom=$this->input->post('vfrom');
	$vto=$this->input->post('vto');
	
	/*$d1=explode("/",$vfrom);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	
	$d2=explode("/",$vto);
	$dt2=$d2[2]."-".$d2[1]."-".$d2[0];*/
	
	$docid=$this->input->post('docid');
	
	 $data1 = array(
        'doc_vehicle_id' => $this->input->post('regno'),
        'doc_vehicle_type' => $this->input->post('typeid'),
        'doc_document' => $this->input->post('docna'),
        'doc_document_no' => $this->input->post('docno'),
        'doc_valid_from' => $this->input->post('vfrom'),
        'doc_valid_to' => $this->input->post('vto'),
        'doc_reminder' => $this->input->post('remind'),
        'doc_status' => 1,
        );
		
	    $this->db->where('doc_id',$docid)->update('vehicle_documents',$data1);
		$this->session->set_flashdata('message',  'Document details Updated..!');
		redirect('General/Documents');
}


public function delete_document()
  {
            $id=$this->uri->segment(3);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_doc($id);
            if($result)
            {
            $this->session->set_flashdata('message', 'Details removed..!');
			redirect('General/Documents');
            }
           else
           {
            $this->session->set_flashdata('message1', 'Please try again.');
			redirect('General/Documents');
           }
  }

public function Renewals()
{
	$this->load->Model('Model_general');
    $data['result']=$this->Model_general->get_vehicle();
	$this->load->view('admin/expired_documents',$data);
}

public function renewal_doc_ajax()
{
	$vid=$this->uri->segment(3);
	$this->load->model('Model_general');
	$results=$this->Model_general->renewal_documents();
	
	$data1 = array();
	$slno=1;
	foreach($results  as $r) 
	   {
 	    $d1=strtotime($r->doc_valid_to);
		$d2=strtotime(date('Y-m-d'));
		$days=floor(($d1-$d2) / (60*60*24));
		if($days<=$r->doc_reminder)
		{
		$update="<a href='#myModal2' id='$r->doc_id' data-toggle='modal' class='edit'>
		<button class='btn btn-warning btn-xs' data-title='edit' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span>&nbsp;&nbsp;Update</button></center></a>";
		//$mm='<button class="btn btn-warning btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button>';
		//$del=anchor('General/delete_document/'.$r->doc_id, $mm, array('id' =>'del_conf'));

		$vto=$r->doc_valid_to;
		$d1=explode("-",$vto);
		$exdate=$d1[2]."-".$d1[1]."-".$d1[0];
		
		if($days<=0)
		{
		$stat="<font color=red>Expired</font>";
		$exdt="<font color=red>".$exdate."</font>";
		}
		else
		{
			$stat="Expired with in <b>".$days. "</b> days";
			$exdt=$exdate;
		}
				
		  array_push($data1, array(
		    "action"=>$update,
			"slno"=>$slno,
			"vehicle"=>$r->vehicle_regno,
			"vtype" =>$r->veh_type_name,
			"docna"=>strtoupper($r->doc_document),
			"docno"=>$r->doc_document_no,
			//"vfrom"=>$r->doc_valid_from,
			"vto"=>$exdt,
			"status"=>$stat,
			
		  ));
 
		 $slno+=1; 
		}
	  }
	echo json_encode(array('data' => $data1));
} 

public function doc_validity_update()
{
	$id=$this->input->post('id');
	$this->load->model('Model_general');
	$row=$this->Model_general->edit_doc($id);
		
	echo
	'<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
                  <h4 class="modal-title">Validity Updation</h4>
                </div>
				
				<form class="form-horizontal" role="form" method="POST" action="'. base_url('General/update_validity').'"  enctype="multipart/form-data">
					<div class="form-group" style="margin-top:10px;" >
					<input type="hidden" class="typeid form-control"  name="docid" id="docid" value="'.$row->doc_id .'" required>
                     <div class="row">
					 
					 <label class="col-md-4 control-label left1">Registration No</label>
                           <div class="col-md-6">

                            <input type="text" class="vehicle form-control"  name="regno" id="regno" value="'.$row->vehicle_regno .'" readonly>
							 <input type="hidden" class="typeid form-control"  name="regid" id="regid" value="'.$row->vehicle_id .'" readonly>
                           </div>
                     
                        </div>
                     </div>
					 

                     <div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Vehicle</label>
                           <div class="col-md-6">';
						   
						    $row1=$this->db->select("veh_type_id,veh_type_name")->from("vehicle_types")->where("veh_type_id",$row->veh_type_id)->get()->row();
						   echo '
                            <input type="text" class="vehicle form-control"  name="vehiclena" id="vehiclena" value="'.$row1->veh_type_name .'" readonly>
							 <input type="hidden" class="typeid form-control"  name="typeid" id="typeid" value="'.$row1->veh_type_id .'" readonly>
                           </div>
                        </div>
                     </div>
					 
									 
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Document</label>
                           <div class="col-md-6">
                            <input type="text" class="form-control"  name="docna" id="docna"  value="'.$row->doc_document .'" readonly>
                           </div>
                        </div>
                    </div>
					
					 
					 <div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Document No</label>
                           <div class="col-md-6">
                            <input type="text" class="form-control"  name="docno" id="docno" value="'.$row->doc_document_no .'" readonly >
                           </div>
                        </div>
                     </div>
					 <div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1" style="color:blue;">Enter new validation period :</label>
                        </div>
                     </div>					 
					
					 <div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Validity From</label>
                           <div class="col-md-4">
								<input type="date" class="form-control" name="vfrom"  required />
                           </div>
                        </div>
                     </div>
					 
					<div class="form-group">
                    <div class="row">
                      <label class="col-md-4 control-label left1">Validity To</label>
                           <div class="col-md-4">
                            <input type="date" class="form-control" name="vto"   required />
                           </div>
                    </div>
                    </div>
										 
                    <hr style="margin:10px 0px 10px 0px;">
 
                    <div class="form-group">
                    <div class="row">
                        <div class="col-md-10" align="right">
                          <button type="submit" class="btn_add btn btn-primary"  style="padding-left:5px;"><span class="glyphicon glyphicon-ok-sign"></span> Update Details</button>

                         &nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  								  
                        </div>
                    </div>
                    </div>

                    </form>	';
 }

public function update_validity()
{
	$vfrom=$this->input->post('vfrom');
	$vto=$this->input->post('vto');
	$docid=$this->input->post('docid');
	
	 $data1 = array(
        'doc_valid_from' => $vfrom,
        'doc_valid_to' => $vto,
        'doc_status' => 1,
        );
		
	    $this->db->where('doc_id',$docid)->update('vehicle_documents',$data1);
		$this->session->set_flashdata('message',  'Document validity Updated..!');
		redirect('General/Renewals');
}
 

  // Monthly targets ----------------------------------------------->
  
  public function Targets()
  {
	  $this->load->view('admin/monthly_target');
  }

public function add_mon_targets()
{
	$mon=$this->input->post('month');
	$yr=$this->input->post('year');
	$veh=$this->input->post('vehicle');
	$amt=$this->input->post('amount');
	
	$this->load->model('Model_general');
	$row=$this->Model_general->check_monthly_entry($mon,$yr,$veh);
	$cnt=$row->cnt;
	if($cnt<=0)
	{	
	 $tdata = array(
        'tar_month' => $mon,
        'tar_year' => $yr,
        'tar_vehicle' => $veh,
        'tar_amount' => $amt,
        'tar_status' => 1
        );

		$this->session->set_userdata('tyear',$yr);
		$this->session->set_userdata('tmonth',$mon);
		
	    $this->db->insert('monthly_targets',$tdata);
		$this->session->set_flashdata('message', '1#Details successfully added.');
		redirect('General/Targets');
	}
	else
	{
		$this->session->set_flashdata('message', '4#Data already Exist.');
		redirect('General/Targets');
	}
}
 
 public function targets_ajax()
{
	$mon=$this->uri->segment(3);
        $this->load->model('Model_general');
        $results=$this->Model_general->get_monthly_targets($mon);
		$month=["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVENMBER","DECEMBER"];
        $data1 = array();
        foreach($results  as $r) 
               {
               	$edit="<a href='#myModal2' id='$r->tar_id' data-toggle='modal' class='edit'>
				<button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></a>";
				$mm='<button class="btn btn-default btn-xs" data-title="Delete" ><span class="glyphicon glyphicon-trash"></span></button>';
				$del=anchor('General/delete_targets/'.$r->tar_id, $mm, array('id' =>'del_conf'));
            
		           array_push($data1, array(
                    "action"=>$edit." ". $del,
                    "month"=>$month[($r->tar_month-1)],
					"year"=>"$r->tar_year",
                    "vehicle"=>"$r->vehicle_regno",
					"amount"=>"<b>&#8377;&nbsp;&nbsp;".number_format($r->tar_amount,"2",".","")."</b>",
                  ));
             }
          echo json_encode(array('data' => $data1));
          //$this->load->view('admin/view_business_category',$data1);
}

public function edit_targets()
{
	$id=$this->input->post('id');
	$this->load->model('Model_general');
	$row=$this->Model_general->edit_mon_targets($id);
	
	echo
	'<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
                  <h4 class="modal-title">Edit Monthly Target</h4>
                </div>
				<form class="form-horizontal" role="form" method="POST" action="'.base_url("General/update_targets").'" enctype="multipart/form-data">
				
				<input type="hidden" class="form-control"  name="tarid" value="'.$row->tar_id.'"required>
				
				<div class="form-group" style="margin-top:15px;">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Select Month</label>
                           <div class="col-md-5">
						   <select name="month" class="form-control" required>
								<option value=""'; if($row->tar_month=="") echo "selected"; echo ' >---month----</option>
								<option value="1"'; if($row->tar_month==1) echo "selected"; echo '>JANUARY</option>
								<option value="2"'; if($row->tar_month==2) echo "selected"; echo '>FERUARY</option>
								<option value="3"';if($row->tar_month==3) echo "selected"; echo '>MARCH</option>
								<option value="4"'; if($row->tar_month==4) echo "selected"; echo '>APRIL</option>
								<option value="5"'; if($row->tar_month==5) echo "selected"; echo '>MAY</option>
								<option value="6"'; if($row->tar_month==6) echo "selected"; echo '>JUNE</option>
								<option value="7"'; if($row->tar_month==7) echo "selected"; echo '>JULY</option>
								<option value="8"'; if($row->tar_month==8) echo "selected"; echo '>AUGUST</option>
								<option value="9"'; if($row->tar_month==9) echo "selected"; echo '>SEPTEMBER</option>
								<option value="10"'; if($row->tar_month==10) echo "selected"; echo '>OCTOBER</option>
								<option value="11"'; if($row->tar_month==11) echo "selected"; echo '>NOVEMBER</option>
								<option value="12"'; if($row->tar_month==12) echo "selected"; echo '>DECEMBER</option>
						   </select>
						   </div>
						   
						   <div class="col-md-2" style="padding-left:0px;padding-right:0px;">
						   <select name="year" class="form-control" required>
								<option value="" <?php if($mon=="") echo "selected";?> >-year-</option>';

								$y=date("Y")+1;
								for($x=$y;$x>=date("Y");$x--)
								{
								echo '<option value="'.$x.'"'; if($row->tar_year==$x) echo "selected"; echo '>'.$x.'</option>';
								}
								
							echo '
								
						   </select>
						   </div>
                        </div>
                     </div>
					 
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Select Vehicle</label>
                           <div class="col-md-7" style="padding-right:0px;">
						   <select name="vehicle" class="form-control" required>
						   <option value="">----Vehicle----</option>';
						   						   
						   $result=$this->db->select("vehicle_id,vehicle_regno")->from("vehicle_registration")->get()->result();
						   foreach($result as $r1)
						   {
							   echo '<option value="'. $r1->vehicle_id.'"'; if($r1->vehicle_id==$row->tar_vehicle) echo "selected"; echo '>'.$r1->vehicle_regno.'</option>';
						   }
						   echo '
						   </select>
						   </div>
                        </div>
                     </div>
				 
                     <div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label left1">Target Amount</label>
                           <div class="col-md-7" style="padding-right:0px;">
                            <input type="number" class="form-control"  name="amount" value="'.$row->tar_amount.'"required>
                           </div>
                        </div>
                     </div>
					 
					 <hr>

                    <div class="form-group">
                     <div class="row">
                          <div class="col-md-11" style="text-align:right;">
                                   <button type="submit" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> Update details</button>
								   &nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                          </div>
                        </div>
                       </div>
                        </form>
				';
 }
  
 public function delete_targets()
  {
            $id=$this->uri->segment(3);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_mon_targets($id);
            if($result)
            {
            $this->session->set_flashdata('message', '3#Details successfully removed.!');
			redirect('General/Targets');
            }
           else
           {
            $this->session->set_flashdata('message', '4#Please try again.');
			redirect('General/Targets');
           }
  }
  
 public function update_targets()
 {
	$tid=$this->input->post('tarid');
	$mon=$this->input->post('month');
	$yr=$this->input->post('year');
	$veh=$this->input->post('vehicle');
	$amt=$this->input->post('amount');

	 $tdata = array(
        'tar_month' => $mon,
        'tar_year' => $yr,
        'tar_vehicle' => $veh,
        'tar_amount' => $amt,
        'tar_status' => 1
        );
	
	    $this->db->where('tar_id',$tid)->update('monthly_targets',$tdata);
		$this->session->set_flashdata('message',  '2#Details successfully Updated..!');
		redirect('General/Targets');
}
 
public function Target_Report()
{
		$mon=$this->input->post('month');
		$yr=$this->input->post('year');
		
		if($mon==null or $yr==null)
		{
			$mon=date('m');
			$yr=date('Y');
		}
	$this->session->set_userdata('mtyear',$yr);
	$this->session->set_userdata('mtmonth',$mon);
	
	$this->load->model('Model_general');
    $data['result']=$this->Model_general->report_doc($mon,$yr);
	$data['result1']=$this->Model_general->get_monthly_collection($mon,$yr);
	$this->load->view('admin/target_report',$data);
} 
 
public function Marketing()
{
	$this->load->view('admin/marketing_business');
} 
 
public function get_staff_designation()
{
	$sid=$this->input->post('sna');
	$this->db->select('staff_profession,profession.profession_id,profession.profession_name')->from('staffregistration');
	$this->db->join('profession','profession.profession_id=staffregistration.staff_profession',"inner");
	$qry1=$this->db->get()->row();
	$desig=$qry1->profession_id.",".$qry1->profession_name;
	echo $desig;
} 
 
 public function add_marketing()
 {
		 $dt1=$this->input->post('datepicker1');
		 $dt2=$this->input->post('datepicker2');
		 
		 $d1=explode("-",$dt1);
		 $dt1=$d1[2]."-".$d1[1]."-".$d1[0];
			
		 $d2=explode("-",$dt2);
		 $dt2=$d2[2]."-".$d2[1]."-".$d2[0];
 
	 
	 
	 $mdata = array(
        'mar_edate' => $dt1,
        'mar_staff' => $this->input->post('staffna'),
        'mar_profession' => $this->input->post('stprofid'),
        'mar_cont_person' => $this->input->post('cperson'),
		'mar_designation' => $this->input->post('cdesig'),
        'mar_cont_number' => $this->input->post('cmobile'),
		'mar_cust_group' => $this->input->post('cgroup'),
        'mar_cust_name' => $this->input->post('custname'),
        'mar_cust_address' => $this->input->post('caddress'),
        'mar_business' => $this->input->post('nbusiness'),
		'mar_start_time' => $this->input->post('timepicker1'),
        'mar_end_time' => $this->input->post('timepicker2'),
		'mar_discussion' => $this->input->post('disc'),
        'mar_res_status' => $this->input->post('rstatus'),
        'mar_business_steps' => $this->input->post('bsteps'),
        'mar_followup_date' => $dt2,
		'mar_status' =>1
		);
	
	    $this->db->insert('marketing',$mdata);
		$this->session->set_flashdata('message',  '1#Details successfully added.!');
		redirect('General/Marketing');
}

public function view_marketing()
 {
	$op=$this->uri->segment(3);
	$md="";	$dt1="";$dt2="";
	
	$this->load->Model('Model_general');
	$mon=date('m')+1;
	$yr=date('Y');
	$month=['January','February','March','April','May','June','July','August','September','October','November','December'];
	
	if($op==1)
	{
	$fdate=$this->input->post('fdate');
	$sdate=$this->input->post('sdate');
	$d1=explode("-",$fdate);
	$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
	
	$d2=explode("-",$sdate);
	$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
	$mbdata['sop']=$dt1." - ".$dt2;
	}
	else if($op==2)
	{
	$mon=$this->input->post('mon');
	$yr=$this->input->post('syear');
	$mbdata['sop']=$month[$mon-1]." - ".$yr;
	}
	else if($op==null or $op=="")
	{
	 $md=date('F');	
	 $mbdata['sop']=$md." -" .$yr;
	 $mon=date('m');
	 $yr=date('Y');
	}

	$this->session->set_userdata("mbyear",$yr);
	$this->session->set_userdata("mbmonth",$mon);
	$this->session->set_userdata("mbdate1",$dt1);
	$this->session->set_userdata("mbdate2",$dt2);
	$this->session->set_userdata("mbop",$op);
	$this->session->set_userdata("se_head",$mbdata['sop']);
			
	//------------------- 
	$this->load->model('Model_general');
    $mbdata['marres']=$this->Model_general->view_marketing($dt1,$dt2,$mon,$yr,$op);
	$this->load->view('admin/view_marketing_business',$mbdata);
 }
 
  
  
  public function del_marketing()
  {
            $id=$this->uri->segment(3);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_marketting($id);
            if($result)
            {
            $this->session->set_flashdata('message', '3#Details successfully removed.!');
			redirect('General/view_marketing');
            }
           else
           {
            $this->session->set_flashdata('message', '4#Please try again.');
			redirect('General/view_marketing');
           }
  }
 
  
 public function Expenses()
 {
	 $this->load->view('admin/general_expense');
 }
  
  
 
 public function add_gen_expense()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('etype', 'etype', 'required');
$this->form_validation->set_rules('edate', 'edate', 'required');
$this->form_validation->set_rules('billno', 'billno', 'required');
$this->form_validation->set_rules('bdate', 'bdate', 'required');
$this->form_validation->set_rules('amount', 'amount', 'required');

 if($this->form_validation->run())
        {
		
	if (!empty($_FILES['files']['name']))
		{
			$filesCount = count($_FILES['files']['name']);
			
			$fname="";	
			for($i = 0; $i < $filesCount; $i++)
			{
                $_FILES['ivf']['name'] = $_FILES['files']['name'][$i];
                $_FILES['ivf']['type'] = $_FILES['files']['type'][$i];
                $_FILES['ivf']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['ivf']['error'] = $_FILES['files']['error'][$i];
                $_FILES['ivf']['size'] = $_FILES['files']['size'][$i];
				
			    $uploadPath = 'upload/expense/';

                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|jpe';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				
                if($this->upload->do_upload('ivf'))
				{
                 $fileData=array('upload_data'=> $this->upload->data());
				 
						if($i==$filesCount-1)
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'];
						 }
						 else
						 {
							$fname.=base_url('upload/customer').'/'.$fileData['upload_data']['file_name'].",";
						 }
				}
				else
				{
					echo $this->upload->display_errors();
					
				}
			}
		}		

	$etype=$this->input->post('etype');
    $edate=$this->input->post('edate');
    $billno=$this->input->post('billno');
    $bdate=$this->input->post('bdate');
	$amount=$this->input->post('amount');
	$narra=$this->input->post('narra');
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
    	
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
    
       $userdt = array(
         'gen_expense_date' => $edate,
         'gen_expense_type' =>$etype,
         'gen_expense_billno' =>$billno,
         'gen_expense_billdate' =>$bdate,
		 'gen_expense_amount' =>$amount,
		 'gen_expense_narration'=>$narra,
         'gen_expense_proof' =>$fname,
          );
		  
        $this->load->Model('Model_general');
		$ins_id="";
        $ins_id=$this->Model_general->insert_general_expense($userdt);
		
		$etype_name=$this->db->select("etype_name")->from("expense_type")->where('etype_id',$etype)->where('etype_group',2)->get()->row()->etype_name;


		//---------------------add data to accounts

		$lg=$this->db->select('ledg_id')->from('ac_ledgers')->where('ledg_exptype',$etype)->get()->row();


		//HEAD ENTRY

		 $this->load->model('Model_accounts');
		 
		 $accdata=array(
		 "accu_date"=>date('Y-m-d'),
		 "accu_description"=>$etype_name,
		 "accu_voucherno"=>0,
		 "accu_narration"=>$narra,
		 );
		  
		 $insid=$this->Model_accounts->insert_data("ac_accounts",$accdata);

		  //ENTRY 1
		 $accdata2=array(
		 "entry_date"=>date('Y-m-d'),
		 "entry_accuid"=>$insid,
		 "entry_ledgerid"=>$lg->ledg_id, 	
		 "entry_debit"=>$amount, 
		 "entry_credit"=>0,
		 );
		 $this->Model_accounts->insert("ac_account_entry",$accdata2);
		 
		 //ENTRY 2
		 $accdata3=array(
		 "entry_date"=>date('Y-m-d'),
		 "entry_accuid"=>$insid,
		 "entry_ledgerid"=>5,  			
		 "entry_debit"=>0, 
		 "entry_credit"=>$amount,
		 );
		 
		 $this->Model_accounts->insert("ac_account_entry",$accdata3);
		 
		 // add data to cash book   
		 $acccash=array(
		 "cashbook_date"=>date('Y-m-d'),
		 "cashbook_headid"=>$insid,
		 "cashbook_debit"=>0,
		 "cashbook_credit"=>$amount, 
		 "cashbook_narration"=>$narra,
		 );
		 $this->Model_accounts->insert("ac_cashbook",$acccash);

		//-----------------------------------

		$this->session->set_flashdata('message', '1#Expense added..!');
        $uid = $this->db->insert_id();
        $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$insid,
            'log_desc'=>'Expense Details Added'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
      	  redirect('General/Expenses');  
    }
	else{
        $this->session->set_flashdata('message', '4#Error: Data Not Inserted..');
        //$data['message']="Data Inserted Successfully";
		redirect('General/Expenses');
       }  
}	

public function View_expense()
{
	    $op=$this->uri->segment(3);
        $this->load->model('Model_general');
		
		if($op=='1')
		{
				$dt1=$this->input->post('fdate');
				$dt2=$this->input->post('sdate');
				
						$dt11=$dt1; //form message
						$dt21=$dt2;
				
				$d1=explode("-",$dt1);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$dt2);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];

			$exdata['genexp']=$this->Model_general->view_general_expense($dt1,$dt2,"","");
			$this->session->set_flashdata('exphead', 'Expense List of : <b>'.$dt11." => ".$dt21.'</b>');
		}
		else if($op=='2')
		{
			$mon=$this->input->post('mon');
			$m1=date('Y')."-".$mon."-"."01";
			$exdata['genexp']=$this->Model_general->view_general_expense("","",$mon,"");
			$this->session->set_flashdata('exphead', 'Expense List of : <b>'. date('F',strtotime($m1)).'</b>');
		}
		else
		{
		$this->session->set_flashdata('exphead', 'Expense List of : <b>'.date('F').'</b>');
		$exdata['genexp']=$this->Model_general->view_general_expense("","","","0");
		}
		$this->load->view('admin/general_expense_list',$exdata);
}
 // Additional incomes
  
 public function Incomes()
 {
	 $this->load->view("admin/additional_incomes");
 }
 
public function add_income_type()
{
	$type=$this->input->post('inctype');
	$incdt = array(
         'inc_type_name' => strtoupper($type),
		 'inc_type_group' => "1",
		);
	$inc1=$this->db->insert('income_types',$incdt);
	echo $this->db->insert_id();
}
 

 
/*public function add_incomes()
{
$this->load->library('form_validation');

$this->form_validation->set_rules('inc_type', 'income type', 'required');
$this->form_validation->set_rules('incdate', 'incdate', 'required');
$this->form_validation->set_rules('afrom', 'from name', 'required');
$this->form_validation->set_rules('amount', 'amount', 'required');

if($this->form_validation->run())
  {
	$inctype=$this->input->post('inc_type');
    $incdate=$this->input->post('incdate');
	$afrom=$this->input->post('afrom');
	$amount=$this->input->post('amount');
	$narra=$this->input->post('narra');
	
    $staffid=$this->session->userdata('userid');
    $staffname=$this->session->userdata('name');
    	
    $date=date("Y/m/d");
    date_default_timezone_set('Asia/Kolkata');
    $time=date('h:i:sa'); 
    
       $incdt = array(
         'addi_inc_date' =>$incdate,
         'addi_inc_type' =>$inctype,
		 "acc_trans_description"=>"", 
         'addi_inc_from' =>$afrom,
         'addi_inc_amount' =>$amount,
		 'addi_inc_narration'=>$narra,
         );
		  		  
        $this->load->Model('Model_general');
		$ins_id="";
        $ins_id=$this->Model_general->insert_additional_incomes($incdt);
		
		$this->session->set_flashdata('message', '1#Income added..!');
        $uid = $this->db->insert_id();
			
	//---------------------add data to accounts

 $accdata=array(
 "acc_trans_date"=>date('Y-m-d'),
 "acc_trans_type"=>1, 
 "acc_trans_voucherNo"=>0,
 "acc_trans_narration"=>$narra,
  );

  
 $this->load->model('Model_accounts');
 $insid=$this->Model_accounts->insert_data("acc_trans_head",$accdata);
 
 // account transactions table data
 $accdata1=array(
 "acc_tra_headid"=>$insid,
 "acc_tra_ledgerid"=>2,
 "acc_tra_debit"=>$amount, 
 "acc_tra_credit"=>0,
 "acc_tra_remarks"=>"Amount from :".$afrom, 
 );
 $this->load->model('Model_accounts');
 $this->Model_accounts->insert("acc_transactions",$accdata1);
 
 $accdata2=array(
 "acc_tra_headid"=>$insid,
 "acc_tra_ledgerid"=>4,
 "acc_tra_debit"=>0, 
 "acc_tra_credit"=>$amount,
 "acc_tra_remarks"=>"CASH", 
 );

 $this->load->model('Model_accounts');
 $this->Model_accounts->insert("acc_transactions",$accdata2);
  
//-----------------------------------
	
        $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Income Details Added'
            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
      	  redirect('General/Incomes');  
    }
 
 else{
        $this->session->set_flashdata('message', '4#Error: Data Not Inserted..');
        //$data['message']="Data Inserted Successfully";
		redirect('General/Incomes');
		  
       }  
}	
 */
 /*
 public function View_Income()
{
	    $op=$this->uri->segment(3);
        $this->load->model('Model_general');

		if($op=='1')
		{
				$dt1=$this->input->post('fdate');
				$dt2=$this->input->post('sdate');
				
						$dt11=$dt1; //form message
						$dt21=$dt2;
				
				$d1=explode("-",$dt1);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$dt2);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];

			$incdata['addiinc']=$this->Model_general->view_additional_incomes($dt1,$dt2,"","");
			$this->session->set_flashdata('inchead', 'Income List of : <b>'.$dt11." => ".$dt21.'</b>');
		}
		else if($op=='2')
		{
			$mon=$this->input->post('mon');
			$m1=date('Y')."-".$mon."-"."01";
			$incdata['addiinc']=$this->Model_general->view_additional_incomes("","",$mon,"");
			$this->session->set_flashdata('inchead', 'Income List of : <b>'. date('F',strtotime($m1)).'</b>');
		}
		else
		{
		$this->session->set_flashdata('inchead', 'Income List of : <b>'.date('F').'</b>');
		$incdata['addiinc']=$this->Model_general->view_additional_incomes("","","","0");
		}
		$this->load->view('admin/additional_income_list',$incdata);
}
*/
/*
 public function del_Income()
  {
            $id=$this->uri->segment(3);
            $this->load->model('Model_general');
            $result=$this->Model_general->delete_income($id);
            if($result)
            {
            $this->session->set_flashdata('message', '3#Income details removed.!');
			redirect('General/View_Income');
            }
           else
           {
            $this->session->set_flashdata('message', '4#Please try again.');
			redirect('General/View_Income');
           }
  }
 */
}
?>