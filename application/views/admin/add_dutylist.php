<?php
 include('application/views/common/header.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  
  <!-- Main content -->
 
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Drivers Duty Entries</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px'>
   </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->
              
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
   <div class="page-content">
   
   <div style="padding:0px 15px 0px 15px;">
				 <div id="idmsg"  style="background-color:#fff;height:25px;margin-bottom:5px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div>
				 <div id='msgerr'><?php echo $this->session->flashdata('message1'); ?></div>
				 <div id='msgdel'><?php echo $this->session->flashdata('message2'); ?></div>
				 </center></div>
	</div>
				 
		
                    <div class="col-md-5" style='padding-left:0px;'>
                    <div style="background-color:#fff;padding:10px; ">
					   
                        <!-- <div class="row"> -->
        <div class="col-md-12">
			   <h4>Driver's Duty</h4>
			   <hr>
          </div>
             <!-- <label style="background-color:#cecece;height:1px;width:100%"></label> -->
                   <!-- </div>/.box-header -->
          <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('General/add_duty_entry')?>" enctype="multipart/form-data">
                  
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label left1">Date :</label>
								<div class="col-md-7">
									 <input type='text' class='form-control' name='ddate' id='datepicker1' placeholder="" value="" required >
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label left1">Driver Name :</label>
								<div class="col-md-7">
								<select class="form-control" placeholder="Choose Purpose" name="drname" id="drname">
								  <option value="">----------</option>
										<?php
										$vt=$this->db->select('driver_id,driver_name')->from("driverregistration") ->get()->result();
										
										foreach($vt as $v)
										{
										 echo"<option value='". $v->driver_id ."'>".strtoupper($v->driver_name)."</option>";
										}
										?>
					
								</select>
								</div>
							</div>
						</div>
								
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label left1">Mobile :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="drmobile" id='drmobile' required>
								</div>
							</div>
						</div>		
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label left1">Shift :</label>
								<div class="col-md-7">
								<select name='drshift' id='drshift' class='form-control' required>
								<option value="">---------</option>
								<option value="1">DAY</option>
								<option value="2">NIGHT</option>
								<option value="3">OFF</option>
								</select>
								
								</div>
							</div>
						</div>
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label left1">Shift Time :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="drtime" id='drtime' required>
								</div>
							</div>
						</div>
						
                       <hr>
					                        
 
                    <div class="form-group">
                     <div class="row">
                           <div class="col-md-12" style="padding-left:25px;">
                                <center><button type="submit" id='idsubmit' class="btn btn-primary">Save Details</button></center>
                           </div>
                        </div>
                       </div>

                        </form>
                       </div>
                    </div>

 
                   <div class="col-md-7" style='padding-left:0px;padding-right:0px;'>
                     <div style="background-color:#fff;padding:10px; ">
       
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                        <div class="col-md-12" >
						<label style='font-size:18px;'>Duty List of : <b><?php echo date('F');?></b></label><br>
						<hr style='margin:0px 0px 20px 0px;'>
						</div>
                </div>
                </div>
				
                <table class="table table-striped table-hover table-bordered" id="example">
					<thead>
					<tr>
					 <th>Edit</th>
					 <th>ID</th>
					 <th>Date</th>
					 <th>Driver Name</th>
					 <th>Mobile</th>
					 <th>Shift</th>
					 <th>Time</th>
					</tr>
					</thead>
                </table>
                </div>
				      <!-- END BORDERED TABLE PORTLET-->
                </div>
                </div>
                </div>
		</div> <!-- end Row --->
                <!-- END CONTENT BODY -->
            <!-- </div> -->
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
			  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times </button>
                  <h4 class="modal-title">Edit driver duty</h4>
                </div>
                <div class="modal-body" >
                			
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
        
 
  <!-- End user details -->
  </div>
  
   <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 

</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">


 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	/*endDate:'now'*/
   });
//sweet alert box ----------------------
$("#idmsg").hide();
if($.trim($("#msg").html())!="")
	{
		swal("Saved",$("#msg").html(),"success")
		$("#msg").html("");
		$("#msgerr").html("");
	}
	
$("#msgerr").hide();
if($.trim($("#msgerr").html())!="")
	{
		swal("Try Again",$("#msgerr").html(),"error")
		$("#msgerr").html("");
		$("#msg").html("");
	}

	$("#msgdel").hide();
if($.trim($("#msgdel").html())!="")
	{
		swal("Deleted",$("#msgdel").html(),"success")
		$("#msgdel").html("");
	}
//-------------------------------------
 
 $("#drname").change(function()
 {
	var did=$("#drname").val();
	
	jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/Get_driver_mobile",
        dataType: 'html',
        data: {drid: did},
        success: function(res)
 		{
			$("#drmobile").val(res);
            }
            });
	
 });
 
 
 $("#drshift").change(function()
 {
	 var st=$("#drshift").val();
	 if(st=='1')
	 {
		 $("#drtime").val("6 AM - 8 PM");
	 }
	 else if(st=='2')
	 {
		 $("#drtime").val("8 PM - 6 AM");
	 }
	 else if(st=='3')
	 {
		 $("#drtime").val("0");
	 }
	 $("idsubmit").focus();
 });
 
 
 
  $('#example').dataTable( {
         destroy: true,
        "processing": true,
		       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/drv_duty_ajax",// json datasource
               },
	"columnDefs":[
	{"width":"12%","targets":0},
	{"width":"7%","targets":1},
	{"width":"9%","targets":4},
	],
        
        "columns": [
            { "data": "edit" },
			{ "data": "drid" },
			{ "data": "ddate" },
            { "data": "dname"},
            { "data": "dmobile" },
			{ "data": "dshift" },
			{ "data": "dtime" },
       ]
  });

   $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/drv_duty_edit",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 
 	
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

setInterval(function(){ $('#msg').html(''); }, 3000);
 //});
  
</script>



