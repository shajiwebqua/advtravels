<?php
 include('application/views/common/header.php');?>
<style>
.top-mr
{
	margin-top:25px;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
 <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <b>Marketting Reports</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px;'>
		<li><a href="<?php echo base_url('General/view_marketing');?>" style='color:#fff;'><button class='btn btn-primary' ><i class="fa fa-list" aria-hidden="true"></i> View Marketing Report</a></li>
			 
    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 

  <div class="row">
  <div class="col-md-12">

              
<div class="page-content-wrapper">

   <div class="page-content">

         <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
<?php
		$dtab=$this->session->userdata('doctab');
?>
	 <div style='padding-left:3px;'> 
                <!-- Tab panes -->
       
	    <div style="background-color:#fff;padding:10px; ">
                        <!-- <div class="row"> -->
        <div class="col-md-12">
			<div class='row'>
				<div class='col-md-6'><h4> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add marketing Reports</h4>  </div>
			   
			</div>
		<hr style='margin:0px 0px 15px 0px;'>
        </div>

        
                  <form class="form-horizontal" method="POST" action="<?php echo base_url('General/add_marketing')?>" enctype="multipart/form-data">
				  
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label">Date</label>
                           <div class="col-md-3">
						  <input type="text" class="form-control"  name="datepicker1" id='datepicker1' required>
						   </div>
                        </div>
                    </div>
				  
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label">Staff Name</label>
                        <div class="col-md-5">
						   <select name='staffna' class='form-control' id='staffna' required>
							   <option value="0">----------</option>
							   <?php
							   $res=$this->db->select('*')->from('staffregistration')->get()->result();
							   foreach($res as $sr1)
							   {
								   echo "<option value='". $sr1->staff_id."'>".$sr1->staff_name."</option>";
							   }
							   ?>
						   </select>
						   						   
						   </div>
                        </div>
                    </div>
					
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label">Designation</label>
                           <div class="col-md-5">
								<input type="text" class="form-control"  name="stdesig" id='stdesig' required>
						  	    <input type="hidden" class="form-control"  name="stprofid" id='stprofid' required>
						   </div>
                        </div>
                    </div>
										
					<div class="form-group top-mr">
                     <div class="row">
                      <label class="col-md-4 control-label">Conatct Person</label>
                           <div class="col-md-5">
						  <input type="text" class="form-control"  name="cperson" id='cperson' required>
						   </div>
                        </div>
                    </div>
					
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label">Designation</label>
                           <div class="col-md-5">
						  <input type="text" class="form-control"  name="cdesig" id='cdesig' required>
					
						   </div>
                        </div>
                    </div>
				  
				    <div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label">Contact Number</label>
                           <div class="col-md-5">
						  <input type="number" class='form-control' name="cmobile" required>
						   </div>
                        </div>
                    </div>
					
					<div class="form-group top-mr">
                     <div class="row">
                      <label class="col-md-4 control-label">Customer Group</label>
                           <div class="col-md-5">
							<select name='cgroup' class='form-control' id='cgroup' required>
							   <option value="0">----------</option>
							   <option value="1">INDIVIDUALS</option>
							   <option value="2">CORPORATES</option>
						   </select>
						   </div>
                        </div>
                    </div>
					
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label">Customer Name</label>
                           <div class="col-md-5">
						  <input type="text" class="form-control"  name="custname" id='custname' required>
						   </div>
                        </div>
                    </div>
					
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label">Customer Address</label>
                           <div class="col-md-5">
						  <textarea type="text" class="form-control" rows=3  name="caddress" id='caddress' required></textarea>
						   </div>
                        </div>
                    </div>

					<div class="form-group top-mr">
                     <div class="row">
                      <label class="col-md-4 control-label">Nature of the Business</label>
                           <div class="col-md-5">
						  <input type="text" class="form-control"  name="nbusiness" id='nbusiness' required>
						   </div>
                        </div>
                    </div>
					
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label">Time Spent with the guest (Start-End)</label>
                           <div class="col-md-2" style='padding-right:5px;'>
							<div class="input-group bootstrap-timepicker timepicker">
							<input name='timepicker1' id="timepicker1" type="text" class="form-control input-small">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							</div>
						   </div>
						  
						   <div class="col-md-2" style='padding-right:5px;'>
							<div class="input-group bootstrap-timepicker timepicker">
							<input name='timepicker2' id="timepicker2" type="text" class="form-control input-small">
							<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
							</div>
						   </div>
						   
                        </div>
                    </div>
					
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label">Discussion Details</label>
                           <div class="col-md-5">
						  <textarea type="text" class="form-control" rows=3  name="disc" id='disc' required></textarea>
						   </div>
                        </div>
                    </div>
				  
				    <div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label">Response Status</label>
                           <div class="col-md-5">
						  <textarea type="text" class="form-control" rows=3  name="rstatus" id='rstatus' required></textarea>
						   </div>
                        </div>
                    </div>
					
					 <div class="form-group top-mr">
                     <div class="row">
                      <label class="col-md-4 control-label">Steps to be taken to achive the business</label>
                           <div class="col-md-5">
						  <textarea type="text" class="form-control" rows=3  name="bsteps" id='bsteps' required></textarea>
						   </div>
                        </div>
                    </div>
					<div class="form-group">
                     <div class="row">
                      <label class="col-md-4 control-label">Followup Date</label>
                           <div class="col-md-3">
						  <input type="text" class="form-control"  name="datepicker2" id='datepicker2' required>
						   </div>
                        </div>
                    </div>
					
                    <hr style='margin:10px 0px 10px 0px;'>
 
                    <div class="form-group">
                    <div class="row">
                        <div class="col-md-9" align='center'>
                          <button type="submit" class="btn btn-primary" style='padding:7px 20px 7px 20px;'><span class='glyphicon glyphicon-plus-sign'></span> Save Details</button>
                        </div>
                    </div>
                    </div>

                    </form>
                    </div>
	
		</div>
</div>
  </div>
</div>
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<!-- modal windows --->

 <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >

              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
		 
<!--- End -------------------------->

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
  
  $('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    autoclose:true,
	
});
   $('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});
 
 // get_dataTable("0");
 // disable();

 $('#timepicker1').timepicker();
 $('#timepicker2').timepicker();

 
$("#staffna").change(function()
{
	var sna=$(".staffna").val();
	 jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/get_staff_designation",
        dataType: 'html',
        data: {sna:sna},
        success: function(res) 
		{
        $pr=res.split(',');
		$("#stprofid").val($pr[0]);
		$("#stdesig").val($pr[1]);
				
        }
	 });
		
});















/*
function get_dataTable(vid)
{ 
      $('#example').dataTable( {
         destroy: true,
        "processing": true,
        
        "ajax": {
                url :"<?php echo base_url(); ?>" + "General/doc_ajax/"+vid,// json datasource
               },
        "columnDefs":[
	{"width":"10%","targets":0},
	],
        "columns": [
            { "data": "edit" },
            { "data": "vehicle"},
			{ "data": "vtype"},
            { "data": "docna"},
			{ "data": "docno"},
			{ "data": "vfrom"},
			{ "data": "vto"},
            { "data": "reminder" },
       ]
  } );
}
  */
 /* 
$(".btn_add").click(function()
  {
	var rgno=$('#regno').val();
	
	var tid=$('#typeid').val();
	var dna=$('#docna').val();
	var dno=$('#docno').val();
	var vf=$('#datepicker1').val();
	var vt=$('#datepicker2').val();
	var re=$('#remind').val();
	
    $.ajax({
            url: "<?php echo base_url();?>General/add_documents",
            type: 'POST',
            //data: new FormData(this),
			dataType: 'html',
			data:{regno:rgno,typeid:tid,docna:dna,docno:dno,vfrom:vf,vto:vt,remind:re},
            success: function (data, status) {
           
              if(data="success")
              {
                swal("Success", "Dcument details successfully added.", "success");  
                //$('.btn_add')[0].reset();
				get_dataTable();
				$('#docna').val("");
				$('#docno').val("");
				$('#datepicker1').val("");
				$('#datepicker2').val("");
				$('#remind').val();
				$('#docna').focus();
              }
              else
              {
                swal("Error", "Something went wrong", "error");    
              }  
            }
          }); 
  });
  */
  

      $('#example tbody').on('click', '.edit', function () {
        var Result=$("#myModal2 .modal-body");
         var id =  $(this).attr('id');
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "General/edit_document",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
        }); 


  // $('#example tbody').on('click', '.change', function () {
  //       var Result=$("#myModal2 .modal-body");
  //    //   $(this).parent().parent().toggleClass('selected');
  //        var id =  $(this).attr('id');
  //        //alert(id);
  //        //alert(id);
  //       jQuery.ajax({
  //       type: "POST",
  //       url: "<?php echo base_url(); ?>" + "userprofile/change_password",
  //       dataType: 'html',
  //       data: {uid: id},
  //       success: function(res) {
  //       Result.html(res);
  //                   }
  //           });
    
  //       });     
    
    
    
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });

	

 //});
  


</script>



