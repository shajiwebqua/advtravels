<?php
 include('application/views/common/header.php');
 ?>
<!-- <link rel="stylesheet" href="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.css');?>" rel="stylesheet" type="text/css" />
 <script src="<?php echo base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
 <script src="<?php echo base_url('assets/dist/js-css-dropdown/bootstrap-select.js');?>"></script> -->

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/bootstrap/bootstrap.min.css');?>">-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/font-awesome/css/font-awesome.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/search_tabs/selectize.css');?>">

 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id='bdy'> 
  
  <!-- Main content -->
    
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><b>Update Completed Trip</b></h1>
    <ol class="breadcrumb">
   <li> <a href="<?php echo base_url('Trip/completed_trip');?>" style="color:#4b88ed;font-size:15px;"> <i class="fa fa-backward" aria-hidden="true"></i>Back to Trip List</a></li>
    </ol> 
    </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
  <!-- Content Header end -->
  <section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id='idmsg'style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id="msg"><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
  
   
  <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">
  
   <div style='background-color:#fff;padding:3px;'> 
 
        <div style='padding:10px 5px;width:100%;background-color:#fff;'> 	  
    
 <!--Add new user details -->
      <!--<div style="background-color:#fff;border-radius:10px;padding:15px; "> -->
    
     <!--<div class="portlet-title">
      <div class="caption">
        <div style="color:blue;" >
            Trip Details                   
        </div> 
        </div>
       </div>  -->
  
 <div class="portlet-body form">
    <form onsubmit="return save_confirm();"  class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Trip/complete_the_trip')?>" enctype="multipart/form-data"  onsubmit='return checkdata();'>
		<?php
		$tripid="";
		$vid="";
		$vregno="";
           foreach($result as $v1){ 
		   $tripid=$v1->trip_id;
		   $vid=$v1->trip_vehicle_id;
		   $vregno=$v1->trip_vehicle_regno;
        ?>        

        <div class="row">
        <div class="col-md-10" > 
		   <input type='hidden' id='trop' name='trop' value='1'>
		   <input type='hidden' id='idtripid' name='tripid' value='<?=$tripid;?>'>
		   <!--<input type='hidden' id='idcustid' name='custid' value='<?=$v1->trip_cust_id;?>'>-->
		   <input type='hidden' id='vehicleid' name='vehicleid' value='<?=$vid;?>'>
		   <input type='hidden' id='driverid' name='driverid' value='<?=$v1->trip_driver_id;?>'>
			<div class='form-group' >
			  <label class='col-md-3 control-label' style='padding-top:5px;' >Trip ID : </label>
			  <div class='col-md-4'>
			  <input type='text' class='form-control' name='trid' id='trid'  value='<?=$tripid;?>' required readonly>
			  </div>
         </div>
		 
		 <div class='form-group' >
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Trip : </label>
			  <div class='col-md-4'>
			  
			  <?php
			  if($v1->trip_option=='ST')
			  {
				  $tripop="SHORT TRIP";
			  }
			  if($v1->trip_option=='LT')
			  {
				  $tripop="LONG TRIP(OUT STATION)";
			  }if($v1->trip_option=='AP')
			  {
				  $tripop="AIRPORT PICKUP";
			  }if($v1->trip_option=='AD')
			  {
				  $tripop="AIRPORT DROP";
			  }if($v1->trip_option=='RP')
			  {
				  $tripop="RAILWAY PICKUP";
			  }if($v1->trip_option=='RD')
			  {
				  $tripop="RAILWAY DROP";
			  }if($v1->trip_option=='FP')
			  {
				  $tripop="FIXED PACKAGE";
			  }if($v1->trip_option=='CM')
			  {
				  $tripop="COMPLIMENTARY TRIP";
			  }
			  if($v1->trip_option=="" or $v1->trip_option=="NIL")
			  {
				  $tripop="NIL";
			  }
			  ?>
			  <input type='text' class='form-control' style='color:#8d01ad;' name='tripmode' id='tripmode'  value='<?=$tripop;?>' required>
			  </div>
         </div>
		 
		<!--<div class='form-group'  id='custna1'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Customer Name : </label>
			  <div class='col-md-8'>
			  <input type='text' class='form-control' name='custname' id='custname'  value='<?=$v1->trip_clientname;?>' required>
			  </div>
         </div> -->
		 
		  <div class='form-group' id='custna1'>
          <label class='col-md-3 control-label' style='padding-top:2px;'>Customer Name :</label>
           <div class='col-md-8'>
			  
				<!-- <select id="id2" class="form-controll_prop" placeholder="Choose Your Name" name="custid">-->
				
				<select id="custid" class="form-control" placeholder="Choose Your Name" name="custid">
				
				  <option value="">----------</option>
				  <?php
					$cquery=$this->db->select("customer_name,customer_id")->from('customers')->get()->result();
					foreach($cquery as $r)
					{
						echo "<option value='".$r->customer_id."'"; if($v1->trip_cust_id==$r->customer_id) echo  "selected"; echo ">".$r->customer_name."</option>";
					}
				  ?>
				</select>

				<div class="select_btn1">                              
				<!-- <a href="<?php echo base_url('Customer/add_tripcustomer/2');?>" data-target="#myModal2" data-toggle='modal'><button id='custadd' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span></button></a> -->
				</div>	
				 <input type='hidden' class='form-control' name='custname' id='custname'  required>
				
				</div>
         </div>
		 
		 
		 
		 
		 
		  <div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Customer Mobile : </label>
			  <div class='col-md-8'>
			  <input type='text' class='form-control' name='cmobile' id='idcmobile' value='<?=$v1->trip_clientmobile;?>' required  >
			  </div>
          </div>
		 
          <div class='form-group'>
			  <label class='col-md-3  control-label' style='padding-top:5px;'>Nature of Jurney : </label>
			  <div class='col-md-8'>
			 <input type='text' class='form-control' name='nojourney' id='nojourney' value='<?=$v1->trip_purpose;?>'required> 
			  </div>
          </div>
		  
		<div class='form-group'>
			  <label class='col-md-3 control-label' style='padding-top:5px;'>Date Start : </label>
			  <div class='col-md-4'>
			  <input type='date' class='form-control' name='startdate' id='startdate' value='<?=$v1->trip_startdate;?>' required>
			  </div>
			  <label class='col-md-1 control-label'  > End : </label>
          <div class='col-md-3'>
          <input type='date' class='form-control' name='enddate' id='enddate'  value='<?=$v1->trip_enddate;?>' required>
          </div>
       </div>
	   
	    <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Time Start : </label>
          <div class='col-md-4'>
          <input type='text' class='form-control' name='starttime' id="starttime" value='<?=$v1->trip_starttime;?>'>
          </div>
		  <label class='col-md-1 control-label' > End : </label>
			<div class='col-md-3'>
          <input type='text' class='form-control' name='endtime' id="endtime" value='<?=$v1->trip_endtime;?>'>
          </div>
        </div>
	      
        <div class='form-group' style='padding-top:20px;'>
          <label class='col-md-3 control-label' style='padding-top:5px;'>Trip From :</label>
          <div class='col-md-8'>
          <input type='text' class='form-control' name='tripfrom' id='tripfrom' value='<?=$v1->trip_from;?>' required>
          </div>
        </div>
		      
        <div class='form-group'>
          <label class='col-md-3 control-label ' style='padding-top:5px;'>Trip To : </label>
          <div class='col-md-8'>
          <input type='text' class='form-control' name='tripto' id="tripto"  value='<?=$v1->trip_to;?>' required>
          </div>
        </div>

		
	<div class='row'>
			 <label style="background-color:#cecece;width:100%;height:1px;"></label>
		</div>
		
		
		<div class='row'>
			<div class='row' style='padding-top:3px;' id='pay_mode'>
				<label class='col-md-5 control-label' style='padding-top:5px;color:blue;'>Payment Mode : </label>
				<div class='col-md-3'>
				<select name="paymode" id='paymode' class='form-control'>
				<option value=''>----------</option>
				<option value='CREDIT' <?php if($v1->trip_paymentmode=='CREDIT') echo "selected";?> >CREDIT</option>
				<option value='CASH'   <?php if($v1->trip_paymentmode=='CASH') echo "selected";?> >CASH</option>
				</select>	
								
				<input type='hidden' class='form-control' name='paymode1' id='paymode1' value='<?=$v1->trip_paymentmode;?>'>
								
				</div>
			</div>

				<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Minimum Kilometers : </label>
				<div class='col-md-3'>
				 <input type='number' class='form-control' name='minckm' id='idminckm' value='<?=$v1->trip_minchargekm;?>'>
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Minimum Kilometer Charge : </label>
				<div class='col-md-3'>
				 <input type='number' class='form-control' name='mincharge' id='mincharge' value='<?=$v1->trip_mincharge;?>'>
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Trip Starting Kilometer (Vehicle) : </label>
				<div class='col-md-3'>
				<input type='number' class='form-control' name='startkm' id='startkm' value='<?=$v1->trip_startkm;?>'>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Trip Ending Kilometer (Vehicle) : </label>
				<div class='col-md-3'>
				<input type='number' class='form-control' name='end_km' id='endkm' value='<?=$v1->trip_endkm;?>' >
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Running KM (Up&Down) : </label>
				<div class='col-md-3'>
				 <input type='text' class='form-control' name='totalkm' id="totalkm" value='<?=$v1->trip_endkm-$v1->trip_startkm;?>' > 

				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'> </label>
				<div class='col-md-3'>
				 <input type='button' class='btn btn-primary' name='btnSet' id='btnSet' value='Get Values'>
				</div>
			</div>
			
					
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Minimum Days : </label>
				<div class='col-md-3'>
				 <input type='text' class='form-control' name='mindays' id='mindays'  value='<?=$v1->trip_mindays;?>' required>
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Minimum Days Total : </label>
				<div class='col-md-3'>
				 <input type='text' class='form-control' style='font-weight:bold' name='mintotal' id='mintotal' value='<?=$v1->trip_mintotal;?>' readonly>
				</div>
			</div>
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Additional Kilometers : </label>
				<div class='col-md-3'>
				 <input type='text' class='form-control' name='addikm' id='addikm'  value='<?=$v1->trip_addikm;?>' required>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Additional Kilometers Charge: </label>
				<div class='col-md-3'>
				 <input type='text' class='form-control' name='addikmcharge' id='addikmcharge' value='<?=$v1->trip_addikmcharge;?>' required>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Additional KM Total Charge : </label>
				<div class='col-md-3'>
				 <input type='text' class='form-control' style='font-weight:bold' name='additotal' id='additotal' value='<?=$v1->trip_additotal;?>' readonly>
				</div>
			</div>
		
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Toll and Parking Charges: </label>
				<div class='col-md-3'>
				 <input type='text' class='form-control' name='tollparking' id='tollparking'  value='<?=$v1->trip_tollparking;?>' required>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Interstate Permit Charge: </label>
				<div class='col-md-3'>
				 <input type='text' class='form-control' name='ispermit' id='ispermit' value='<?=$v1->trip_interstate;?>' required>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Driver Batha: </label>
				<div class='col-md-3'>
				 <input type='text' class='form-control' name='dbtha' id='dbtha' value='<?=$v1->trip_drbatha;?>'>
				</div>
			</div>
			
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Agent Commission: </label>
				<div class='col-md-3'>
				 <input type='text' class='form-control' name='agentcom' id='agentcom' value='<?=$v1->trip_agentcom;?>'>
				</div>
			</div>
			
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Other Charges: </label>
				<div class='col-md-3'>
				 <input type='text' class='form-control' name='ocharge' id='ocharge' value='<?=$v1->trip_othercharge;?>'>
				</div>
			</div>
			
						
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Other Description: </label>
				<div class='col-md-5'>
				<textarea rows=2 class='form-control' name='otherdesc' id='otherdesc' ><?=$v1->trip_otherdesc;?></textarea>
				</div>
			</div>
					
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'> </label>
				<div class='col-md-3'>
				 <input type='button' class='btn btn-primary' name='btncalc' id='idbtncalc' value='Calculate'>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Trip Cost: </label>
				<div class='col-md-3'>
				 <input type='number' class='form-control ttext' style="background-color:#fff;" name='tripcost' id='tripcost' value="<?=$v1->trip_charge;?>" readonly>
				</div>
			</div>
			
			<div class='row' style='padding-top:5px;'>
				<label class='col-md-5 control-label' style='padding-top:5px;'>Discount: </label>
				<div class='col-md-3'>
				 <input type='number' class='form-control ttext' style="background-color:#fff;" name='discount' id='discount' value='<?=$v1->trip_discount;?>'>
				</div>
			</div>
			
			
		
			
		<div class='form-group'>
          <label class="col-md-5 control-label" style="font-size:25px; text-align:right;color:blue;">RS : </label> 
		  <label id="lblgtotal" class="col-md-3 control-label" style="font-size:25px; text-align:left;color:blue;"> </label>
        </div>
			
		<?php
		   }
		?>

		</div>
		</div>
 
	  </div>
 
 

<label style="background-color:#cecece;width:100%;height:1px;"></label>

<div class='form-group'>
<label class='col-md-3 control-label '></label>

<div class='col-md-3'>
	<button type="submit" class="btn btn-success" style="padding:10px 50px 10px 50px;" id="idsavebtn" > <b>Update Completed Trip</b></button>
</div>

</div>

</form>
</div>

<label style="background-color:#cecece;width:100%;height:1px;"></label>
 
</section>
</div>
<!-- /.content-wrapper --> 

<!--- AGENTS END -------------------->

<?php include('application/views/common/footer.php');?>
<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>  -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBx1BJxGlTRJyF2W_AdaCfRf-U0FLRxif4&libraries=places">
</script>
<script type="text/javascript">

//-- driver list
//sweet alert box ----------------------
//$("#custna1").hide();
//$("#custname").hide();
//$("#custmobile").hide();
//$("#viewMap").prop('disabled',true);
//$("#agents").hide();


$("#custid").change(function()
{
	var id=$("#custid").val();
		jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/get_cust_name_mobile",
        dataType: 'html',
        data: {cid: id},
        success: function(data) {
			var obj = jQuery.parseJSON(data);
				$("#idcmobile").val(obj.mobile);
				$("#custname").val(obj.customer);
		       }
            });
});

$("#paymode").change(function()
{
	$("#paymode1").val($(this).val());
});

if($("#paymode1").val()=='NONE')
{
	$("#pay_mode").hide();
	
}
else
{
	$("#pay_mode").show();
}

//focus control ---------------
var minkm=$("#idminckm").val();

if(minkm>=100)
	{
	$("#totalkm").val('0');

	$("#tollparking").focus();
	}
	else if(minkm<100)
	{

	$("#addikm").val("0");
	$("#addikmcharge").val("0");
	$("#additotal").val("0");
	
	
	var skm=$.trim(parseInt($('#startkm').val()));
	var ekm=(parseInt($("#idminckm").val()) + parseInt(skm));
	var tkm=(parseInt(ekm)- parseInt(skm));
	$("#totalkm").val(tkm);	
	//$("#tollparking").focus();
	$("#trid").focus();
	}
//-----------------------------------

$("#endkm").keyup(function()
{
	var skm=$.trim(parseInt($('#startkm').val()));
	var tkm=(parseInt($("#endkm").val()) - parseInt(skm));
	$("#totalkm").val(tkm);		
});

//----------------------------

$("#idminckm").keyup(function()
{
var minkm=$('#idminckm').val();
if(minkm<100)
{
var skm=$.trim(parseInt($('#startkm').val()));
var ekm=(parseInt($.trim(skm))+ parseInt($.trim($('#idminckm').val())));	
$('#endkm').val(ekm);
$("#totalkm").val($('#idminckm').val());
}
else
{
	$("#endkm").val('0');
	$("#totalkm").val('0');
	$("#endkm").focus();
}
});
	

$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------


	$("#idsavebtn").prop('disabled', true);
	$("#idbtnget").prop('disabled', true);
	$("#idbtncalc").prop('disabled', true);
   setTimeout(function(){ $("#msg").html(""); }, 3000);
	
</script>

<script>
/* -------- button desable/enanble ------------------*/

$('#idminckm').focus(function () {
	$("#idbtnget").prop('disabled', false);
});

/* -------- button desable/enanble ------------------*/


$("#discount").keyup(function()
{
	if($("#discount").val()!="")
	{
	var amt=$("#tripcost").val();
	var dis=$("#discount").val();
	
	var tot=(parseInt(amt)-parseInt(dis));
	$("#lblgtotal").html(parseFloat(tot).toFixed(2));
	}
	else
	{
		$("#lblgtotal").html(parseFloat($("#tripcost").val()).toFixed(2));
	}
		
});

 function checkDate() {
            var stDate =new Date($("#startdate").val()) //for javascript
            var enDate =new Date($("#enddate").val()); // For JQuery
		
			var g=stDate-enDate;
            if (g>0) {
                return false;
            }
            else {
               return true;
            }
        }
		
		
function save_confirm()
{
	if ( confirm("Are you sure, Complete this trip."))
	{
		return true;
	}
	else
	{
		return false;
	}
}


/* get Button  ----------------------*/

$('#btnSet').click(function () {
	
	var skm=$.trim(parseInt($('#startkm').val()));
	var ekm=(parseInt($.trim(skm))+ parseInt($.trim($('#totalkm').val())));
	
	var minkm=$("#idminckm").val();
	
	$("#endkm").val(ekm);
	
	var sdate=$('#startdate').val();
	var edate=$('#enddate').val();
	
	var mk1=$.trim($('#idminckm').val());
	
	if(checkDate()==false)
	{
		alert("Please select end date correctly.");
	}
	else if(sdate=="" || edate=="")
	{
		swal("Please choose Trip Start and End Date.");
	}
	
	else if(isNaN(skm) || isNaN(ekm) || skm=="0" || ekm=="0" || (parseInt(skm) > parseInt(ekm)))
	{
		alert("Please enter Starting KM and Ending KM.");
	}
	else if(mk1.length==0)
	{
		alert("please type minimum charge KM..!");
	}
	
	else
	{
	var rkm=(parseInt(ekm)-parseInt(skm));	
	
	var edate1=new Date(edate);
	var sdate1=new Date(sdate);
		
	var days = (parseInt(edate1.getTime() - sdate1.getTime())) / (1000 * 60 * 60 * 24);
	var adkm=parseInt(rkm)-(parseInt(days)*parseInt(mk1))
	
	if(minkm>=100)
	{
	//var days = (edate1.getTime() - sdate1.getTime())
	$("#runningkm").val(rkm);
	$("#mindays").val(days);
	$("#addikm").val(adkm);
	var mtot=(parseInt($("#mindays").val())*parseInt($("#mincharge").val()));
	$("#mintotal").val(mtot);
	
	$("#idbtncalc").prop('disabled', false);
	$("#idsavebtn").prop('disabled', false);
	$("#tollparking").focus();
	}
	else
	{
	$("#idbtncalc").prop('disabled', false);
	$("#runningkm").val(minkm);
	$("#mindays").val("1");
	$("#addikm").val("0");
	var mtot1=$("#mincharge").val();
	$("#mintotal").val(mtot1);
	$("#addikmcharge").val("0");
	$("#additotal").val("0");
	$('#tollparking').focus();	
	}
		
	}
});
/*----------------------------------------*/

/* claulate totals ----------------------*/

$('#idbtncalc').click(function () {

var mdays=$('#mindays').val();	
var mcharge=$('#mincharge').val();	

var akm=$('#addikm').val();	
var akmc=$('#addikmcharge').val();
	
var tpc=$('#tollparking').val();	
var isp=$('#ispermit').val();	
var mtot1=$('#mintotal').val();
var oamt=$('#ocharge').val();
var dbt=$('#dbtha').val();
var acom=$('#agentcom').val();

if(dbt=="") dbt="0";
if(acom=="") acom="0";
if(tpc=="") tpc="0";
if(isp=="") isp="0";
if(mtot1=="") mtot1="0";
if(oamt=="") oamt="0";


//var mtot=(parseFloat(mdays)* parseFloat(mcharge)).toFixed(2);
var adtot=(parseFloat(akm)* parseFloat(akmc)).toFixed(2);
var ttot=(parseFloat(mtot1)+parseFloat(adtot)).toFixed(2);
var gtot=(parseFloat(ttot)+parseFloat(tpc)+parseFloat(isp)+parseFloat(oamt)+parseFloat(dbt)+parseFloat(acom)).toFixed(2);

$("#additotal").val(adtot);
$("#tripcost").val(gtot);
$("#lblgtotal").html(parseFloat(gtot).toFixed(2));

$("#idsavebtn").prop('disabled', false);
$("#discount").focus();
});
/*----------------------------------------*/
   
 $("#tollparking").keyup(function()
 {
	 $("#idsavebtn").prop('disabled', true);
 });
 
 $("#ispermit").keyup(function()
 {
	 $("#idsavebtn").prop('disabled', true);
 });  
   
$("#ocharge").keyup(function()
 {
	 $("#idsavebtn").prop('disabled', true);
 });

$("#dbtha").keyup(function()
 {
	 $("#idsavebtn").prop('disabled', true);
 }); 

 $("#agentcom").keyup(function()
 {
	 $("#idsavebtn").prop('disabled', true);
 });
  
</script>
 