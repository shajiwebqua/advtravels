<?php
include('application/views/common/header.php');
?>
  <style>
  .dashbtn
  {
	  width:75px;
	  height:75px;
	  background-color:#f4f4f4;
	  border:1px solid #fff;
	  /*border-radius:10px;*/
	  padding:5px;
	  text-align:center;
  }
  
.sh4
{
 border-bottom:1px solid #e4e4e4;
 width:100%;
 color:#00adee;
 padding-bottom:5px;
 margin-top:0px;"
}

.chart1
{
	float:left;
	position:relative;
	width:195px;
	text-align:left;
}

.my-btn{
	padding:0px 5px 0px 5px;
	font-size:12px;
}
</style>
 
 <link rel="stylesheet" href="<?php echo base_url('assets/chart/css/material-charts.css');?>">
 <script src="<?php echo base_url('assets/chart/js/material-charts.js');?>"></script>
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#203856'><b>Dashboard </b>
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
     <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>

        <!-- Main content -->
    <section class="content">
		
		
	<!-- for chart data------------------->
		<input type='hidden' name='tkm' id='tkm' value='<?php echo $this->session->userdata('tkm');?>'>
		<input type='hidden' name='drna' id='drna' value='<?php echo $this->session->userdata('drna');?>'>
		<input type='hidden' name='tkm1' id='tkm1' value='<?php echo $this->session->userdata('tkm1');?>'>
		<input type='hidden' name='dr_id' id='dr_id' value='<?php echo $this->session->userdata('drvid');?>'>
		<input type='hidden' name='dr_id1' id='dr_id1' value='<?php echo $this->session->userdata('drvid1');?>'>
		
	<!-- for chart data------------------->	

			<div class='row'>

			<div class='col-md-3'>
			<div class="info-box">
				    <div >
					<center><span class="info-box-text" style='padding:3px;background-color:#864141;color:#fff;'>Document Renewals</span></center>
					<?php 
					$hcust=$this->db->select('COUNT(*) as hccount')->from('customers')->get()->row();
					?>
					<span style='font-size:20px;font-weight:600;padding:3px 10px 3px 25px;'><?php echo $cnt;?></span>
					<span style='float:right;margin-right:20px;padding-top:3px;'> <a href="<?php echo site_url('General/renewals');?>"><button class='btn  btn-danger btn-xs my-btn;' >More</button></a></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>


			<div class='col-md-3'>
			<div class="info-box">
				    <div >
					<center><span class="info-box-text" style='padding:3px;background-color:#494949;color:#fff;'>Running Trips</span></center>
					<?php 
					$rveh=$this->db->select('COUNT(*) as runveh')->from('trip_management')->where('trip_status=1')->where('trip_startdate<=',date('Y-m-d'))->get()->row();
					?>
					<span style='font-size:20px;font-weight:600;padding:3px 10px 3px 25px;'><?php echo $rveh->runveh;?></span>
					<span style='float:right;margin-right:20px;padding-top:3px;'><a href="<?php echo site_url('Trip/view_trip');?>"><button class='btn  btn-info btn-xs my-btn;' >More</button></a></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>


			<div class='col-md-3'>
			<div class="info-box">
				    <div >
					<center><span class="info-box-text" style='padding:3px;background-color:#404e6d;color:#fff;'>Upcoming Trips</span></center>
					<?php 
					//$sta=$this->db->select('COUNT(*) as stano')->from('staffregistration')->where('staff_status=1')->get()->row();
					$upveh=$this->db->select('COUNT(*) as upcveh')->from('trip_management')->where('trip_status=1')->where('trip_startdate>=',date('Y-m-d'))->get()->row();
					?>
					<span style='font-size:20px;font-weight:600;padding:3px 10px 3px 25px;'><?php echo $upveh->upcveh;?></span>
					<span style='float:right;margin-right:20px;padding-top:3px;'><a href="<?php echo site_url('Trip/view_trip');?>"><button class='btn  btn-success btn-xs my-btn;' > More</button></a></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>

			<div class='col-md-3'>
			<div class="info-box">
				    <div >
					<center><span class="info-box-text" style='padding:3px;background-color:#144400;color:#fff;'>Service Required Vehicles</span></center>
					<?php 
					$hcust=$this->db->select('COUNT(*) as hccount')->from('customers')->get()->row();
					?>
					<span style='font-size:20px;font-weight:600;padding:3px 10px 3px 25px;'><?php echo $servehicle;?></span>
					<span style='float:right;margin-right:20px;padding-top:3px;'><a href="<?php echo site_url('Vehicle/service_vehicles');?>"><button class='btn  btn-warning btn-xs my-btn;' >More</button></a></span>
				  </div><!-- /.info-box-content -->
				</div><!-- /.info-box -->
			</div>

			</div>


	<!--------CHART END ---------------------------------------------------------------------->
<style>
.btn-wl
{
	width:210px;
	text-align:left;
	padding-left:5px;
}
</style>
	
	
		<div class='row' style="padding:5px 5px 0px 0px;">
		
		<div class='col-md-6' style='padding-right:2px;'>
		
	    <div class="box box-info1 box-solid " style="border:1px solid #c4c4c4;margin-bottom:12px;">
	    <!--<div class="box-header">
          <h3 class="box-title"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Quick Task</h3>
        </div>-->
		<label style='margin-left:10px;font-size:18px;color:#000;'><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Quick Tasks </label>
		<hr style='margin:0px;'>
        <div class="box-body" style="margin-top:0px;padding:0px 15px 10px 5px;">
		 <div class='row' style='padding:0 5px;'>
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip/index');?>"> <img src="<?php echo base_url('assets/dist/img/01.png');?>" ></a>
			 </div>
			 
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip/view_trip');?>"> <img src="<?php echo base_url('assets/dist/img/02.png');?>" ></a>
			 </div>
			 
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Vehicle/vehicle_expense');?>"> <img src="<?php echo base_url('assets/dist/img/03.png');?>" ></a>
			 </div>
			 
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('General/Expenses');?>"> <img src="<?php echo base_url('assets/dist/img/04.png');?>" ></a>
			 </div>
		 </div>
        </div>
		
	</div>
	<!--  second row --->
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4;margin-bottom:12px;">
	<label style='margin-left:10px;font-size:18px;color:#000;'><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Booking/Lists </label>
	<hr style='margin:0px;'>
        <div class="box-body" style="margin-top:0px;padding:0px 10px 10px 0px;">
		 <div class='row' style='padding:0 10px;'>
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip/booking_trip');?>"> <img src="<?php echo base_url('assets/dist/img/r4.png');?>" ></a>
			 </div>

			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip/booking_list');?>"> <img src="<?php echo base_url('assets/dist/img/r3.png');?>" ></a>
			 </div>
			 
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip_reports/view_vehicleTrip');?>"> <img src="<?php echo base_url('assets/dist/img/r1.png');?>" ></a>
			 </div>
			 
			 <div class='col-md-3'>
			 <a href="<?php echo base_url('Trip_reports/index');?>"> <img src="<?php echo base_url('assets/dist/img/r2.png');?>" ></a>
			 </div>
		 </div>
        </div>
	</div>
	
	<!--  third row --->
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4;margin-bottom:12px;">
	
        <div class="box-body" style='padding-bottom:11px;'>
		 <div class='row'>
			 <div class='col-md-3'>
			  <a href="<?php echo base_url('Driver/performance');?>"><button class='btn btn-default' style='margin:2px 15px 0px 5px;padding-left:5px;'>
			   <img src="<?php echo base_url('assets/dist/img/driver.png');?>" width="35px" height="35px">
			   &nbsp;Driver's Performance
		 </button></a>
			 </div>
		 </div>

        </div>
		
	</div>
	
	</div>
	
	<div class='col-md-3' style='padding-left:15px;padding-right:2px;'>

	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4;">
	   <!-- <div class="box-header">
          <h3 class="box-title"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Accounts</h3>
        </div> -->
		<label style='margin-left:10px;font-size:18px;color:#000;'><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Invoices </label>
		<hr style='margin:0px;'>
				
		<div class="box-body" style='padding-bottom:15px;margin-bottom:5px;height:375px;'>
		 <div class='row' style='padding:0 15px;height:100%'>
		<center>
		 <a href="<?php echo base_url('Invoice/Trips');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/100.png');?>" width="40px" height="40px">
		   &nbsp;&nbsp;Create Invoice
		 </button></a>
		 
		 <a href="<?php echo base_url('Invoice/Payments');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/101.png');?>" width="40px" height="40px">
		   &nbsp;&nbsp;Record Payments
		 </button></a>
		 <a href="<?php echo base_url('Invoice/Paid');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/022.png');?>" width="40px" height="40px">
		   &nbsp;&nbsp;Paid Invoices
		 </button></a>
		 
		 <a href="<?php echo base_url('Invoice/Unpaid');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/102.png');?>" width="40px" height="40px">
		   &nbsp;&nbsp;Unpaid Invoices
		 </button></a>
	 
		</center>
	</div>
	</div>
	</div>
	</div>

	
	
	<div class='col-md-3' style='padding-left:15px;'>
		
	    <div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	   <!-- <div class="box-header">
          <h3 class="box-title"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Accounts</h3>
        </div> -->
		<label style='margin-left:10px;font-size:18px;color:#000;'><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Accounts </label>
		<hr style='margin:0px;'>
				
		<div class="box-body" style='padding-bottom:15px;margin-bottom:5px;height:375px;'>
		 <div class='row' style='padding:0 15px;'>
		<center>
		 <a href="<?php echo base_url('Investment/Add');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/011.png');?>" width="40px" height="40px">
		   &nbsp;&nbsp;Investments
		 </button></a>
		 
		 <a href="<?php echo base_url('Ledger/Add');?>"> <button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/066.png');?>"  width="42px" height="42px">
		 &nbsp;&nbsp;Create Ledger
		 </button></a>
		 
		 <a href="<?php echo base_url('Ledger/View');?>"> <button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/022.png');?>"  width="42px" height="42px">
		 &nbsp;&nbsp;View Ledgers
		 </button></a>
		 		 
		 <a  href="<?php echo base_url('Account/profit_loss');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/033.png');?>"  width="42px" height="42px">
		 &nbsp;&nbsp;Profit & Loass Accounts
		 </button></a>
		 
		 <a href="<?php echo base_url('Account/balance_sheet');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/044.png');?>"  width="42px" height="42px">
		 &nbsp;&nbsp;Balance Sheet
		 </button></a>
		 		 
		 <a href="<?php echo base_url('Account/trial_balance');?>"><button class='btn btn-default btn-wl' style='margin-top:10px;'>
		 <img src="<?php echo base_url('assets/dist/img/055.png');?>"  width="42px" height="42px">
		 &nbsp;&nbsp;Trial Balance
		 </button></a>
		 
       </center>

		</div>
        </div>
	</div>
	</div>
	</div>
	
</section>
    <!-- content wrapper -->
</div>
     <?php
  include('application/views/common/footer.php');
  ?>
 
  
</body>
</html>

  <script type="text/javascript">
  $(document).ready(function()
  {
	  
	  $(".fset").mouseover(function()
	  {
	  $(this).css("background-color","#aeaeae");
	  });
	  	  
	  $(".fset").mouseleave(function()
	  {
	  $(this).css("background-color","#d2d3d4");
	  });
  });
	  
 </script>

</body>
</html>

  
  