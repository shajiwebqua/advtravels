<?php

class Model_services extends CI_Model{

      public function show_regno()
      {
       $this->db->select('*');
       $this->db->from('vehicle_registration');
       $query = $this->db->get();
       return $query->result();
     }
     function form_insert($data){

      $this->db->insert('vehicle_service', $data);  
    }

	 function strip_insert($data){
      $this->db->insert('service_trips', $data);  
    }
	
	 function insert_service_centre($data){
      $this->db->insert('service_centre', $data);  
    }
		
    public function view_services($regno)
    {
      $this->db->select('*');
      $this->db->from('vehicle_service');
      $this->db->join('vehicle_registration ','vehicle_service.service_vehicleid=vehicle_registration.vehicle_id','inner');
      $this->db->where('service_vehicleid', $regno);
      $this->db->where('MONTH(service_date)',date('m'));
      $this->db->order_by("vehicle_service.service_date", "desc");
      $query = $this->db->get();
      return $query->result();
    }
	
	public function view_service_trips()
    {
      $this->db->select('*');
      $this->db->from('service_trips');
      $this->db->join('vehicle_registration ','vehicle_registration.vehicle_id=service_trips.srtrip_vehicleid','inner');
	  $this->db->join('driverregistration ','service_trips.srtrip_driverid=driverregistration.driver_id','inner');
	   $this->db->join('service_centre ','service_centre.srcentre_id=service_trips.srtrip_centre','inner');
	  $this->db->order_by("service_trips.srtrip_id", "desc");
      $query = $this->db->get();
      return $query->result();
    }
	
	 public function update_service_vehicle($id,$data)
    {
      $this->db->where('vehicle_id', $id);
      $this->db->update('vehicle_registration',$data); 
    }
		
	public function update_service_vehicle1($id,$vid,$data1,$data2)
    {
      $this->db->where('srtrip_id', $id);
      $this->db->update('service_trips',$data1); 
	  
	  $this->db->where('vehicle_id', $vid);
      $this->db->update('vehicle_registration',$data2); 
    }

    public function view(){
      $this->db->select('*');
      $this->db->from('vehicle_registration');
      $this->db->join('vehicle_service ','vehicle_service.service_vehicleid=vehicle_registration.vehicle_id','inner');
      $query = $this->db->get();
      return $query->result();
    }
    public function edit_services($id,$data)
    {
      $this->db->where('service_id', $id);
      $this->db->update('vehicle_service',$data); 
    }

    public function regno_ajax($uaid){
              // $user_data=$this->session->all_userdata();
        //$query = $this->db->select('*')->from('table_bus_timing')->get();

     $this->db->select('*');
     $this->db->from('vehicle_registration');
     $this->db->join('vehicle_service ','vehicle_service.service_vehicleid=vehicle_registration.vehicle_id','inner');
     $this->db->where('service_vehicleid', $uaid);
     $query = $this->db->get();
     return $query->result();
    }


    public function del_entry($id)
    {
      $this->db->where('service_id', $id);
      $this->db->delete('vehicle_service'); 
      return $this->db->affected_rows();
    }  

	public function del_entry_strip($id)
    {
      $this->db->where('srtrip_id', $id);
      $this->db->delete('service_trips'); 
      return $this->db->affected_rows();
    }  
	
	
    public function checkregno($value){
        $this->db->where("service_vehicleid IN (SELECT vehicle_id  FROM vehicle_registration where vehicle_regno = '". $value ."')");
        $query = $this->db->get("vehicle_service");
        if($query->num_rows() > 0){ return TRUE; }
        else{ return FALSE; }
    }

    public function getservieces($value){
      $this->db->select("service_invoiceamt");
      $this->db->where("service_vehicleid IN (SELECT vehicle_id  FROM vehicle_registration where vehicle_regno = '". $value ."')");
      $query = $this->db->get("vehicle_service");
      
      if($query->num_rows() > 0){
         $result = $query->result();
         return  $result[0]->service_invoiceamt;
      }
      else{ return FALSE; }
    }


	public function service_evo_list($op,$md)
	{
     $this->db->select('*');
     $this->db->from('service_evaluation');
     $this->db->join('vehicle_registration ','vehicle_registration.vehicle_id=service_evaluation.se_vehicle_no','inner');
	 $this->db->join('driverregistration','driverregistration.driver_id=service_evaluation.se_chauffeur_name','inner');
	 $this->db->order_by("service_evaluation.se_id","desc");
	 if($op==1)
	 {
		 $this->db->where('service_evaluation.se_date',$md);
	 }
	 else if($op==2)
	 {
		 $this->db->where('MONTH(service_evaluation.se_date)',$md);
	 }
	 else
	 {
		  $this->db->where('MONTH(service_evaluation.se_date)',date('m'));
	 }
	 
     $query = $this->db->get();
     return $query->result();
    }
	
	public function service_evo_list1($id)
	{
     $this->db->select('*');
     $this->db->from('service_evaluation');
     $this->db->join('vehicle_registration ','vehicle_registration.vehicle_id=service_evaluation.se_vehicle_no','inner');
	 $this->db->join('driverregistration','driverregistration.driver_id=service_evaluation.se_chauffeur_name','inner');
	 $this->db->where("service_evaluation.se_id",$id);
	 $this->db->order_by("service_evaluation.se_id","desc");
     $query = $this->db->get();
     return $query->row();
    }
		
	public function view_service_request($op,$dt1,$dt2)
    {
      $this->db->select('*');
      $this->db->from('service_request');
      $this->db->join('vehicle_registration ','vehicle_registration.vehicle_id=service_request.srequest_regno','inner');
	  $this->db->join('vehicle_types','vehicle_types.veh_type_id=service_request.srequest_vtype','inner');
	  if($op==1)
	  {
		  $where="(service_request.srequest_date>='".$dt1. "' and service_request.srequest_date<='".$dt2."')";
		  $this->db->where($where);
	  }	
	  else if($op=="")
      { 
		$where="MONTH(service_request.srequest_date)='".date('m')."'"; 
		$this->db->where($where);
      }	
	  else if($op=="R")
	  {
		$this->db->where("service_request.srequest_id",$dt1);  
	  }
	  
	  $this->db->order_by("service_request.srequest_id", "desc");
      $query = $this->db->get();
      return $query->result();
    }
	
	 public function get_service_request($id)
      {
            $result = $this->db->where('srequest_id', $id)->get('service_request')->row();
            return $result;
      }

      public function get_service_requirements($id)
      {
            $result = $this->db->where('srequest_id', $id)->get('service_requirements')->result();
            return $result;  
      }
     
	
	
}  
