<?php
 include('application/views/common/header.php');?>
<style>
.nav-tabs {
    margin: 0;
    padding: 0;
    border: 0;    
}
.nav-tabs > li > a {
    background: #f2f2f2;
    border-radius: 0;
    //box-shadow: inset 0 -8px 7px -9px rgba(0,0,0,.4),-2px -2px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li.active > a:hover {
    background: #fff;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li > a:hover {
    background: #e4e4e4;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}
/* Tab Content */
.tab-pane {
    background: #fff;
    //box-shadow: 0 0 4px rgba(0,0,0,.4);
    border-radius: 0;
    border:1px solid #e4e4e4;
    text-align: left;
    padding: 10px;
}

.cli
{
padding:5px 0px 5px 0px;
margin-top:3px;

}

.selclass > div:hover
{
background-color:#f4f4f4;
}
</style>

<div class="content-wrapper"> 

  <section class="content-header">
    <h1> <b>Delivery Stock Items</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px;'>
			<li><a href="<?php echo base_url('Stock/Add/0');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Stock List</button></a></li> 
			<li><a href="<?php echo base_url('Reports/Delivery');?>" style='color:#4b88ed;' id='view-pdf'><button class='btn btn-primary'><i class="fa fa-print" aria-hidden="true"></i> Print</button></a></li> 
	    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
     
  <section class="content"> 

  <div class="row">
  <div class="col-md-12">
              
<div class="page-content-wrapper">
   <div class="page-content">

    <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
<?php
		$dtab=$stotab;
?>
	 <div style='padding-left:3px;'> 
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item <?php if($dtab=='1' or $dtab=="") echo 'active'; ?>" id='tb1' >
				<a class="nav-link tp1" data-toggle="tab" href="#sadd"  role="tab"><i class="fa fa-plus"></i> &nbsp;Delivery Stock</a>
			</li>
			<li class="nav-item <?php if($dtab=='2') echo 'active'; ?>" id='tb2'>
				<a class="nav-link tp1 " data-toggle="tab" href="#alist"  role="tab"> <i class="fa fa-list"></i> &nbsp;Delivery List</a>
			</li>
			
		 </ul>
                <!-- Tab panes -->
        <div class="tab-content">
		<div class="tab-pane <?php if($dtab=='1') echo 'active'; ?>" id="sadd" role="tabpanel"> 
	    <div style="background-color:#fff; padding:0px 10px; ">

		<div class='row' style='margin-top:0px;'>
		<div class='col-md-12'>
		<h5 style='font-size:16px;'>Enter Details </h5>
		<hr style='margin:0px 0px 10px 0px;'>
		</div>
		</div>
		
		<div class='row'>
		<div class='col-md-8'>
 
    <form  onsubmit="return check_qty();" class="form-horizontal" method="POST" action="<?php echo base_url('Stock/delivery_stock')?>" enctype="multipart/form-data">
             	
			<div class="form-group">
			<label class="col-md-4" style="padding-top:5px;" align="right">Date : </label>
			<div class="col-md-4">
			<input type='text' class='form-control' name="edate"   id="datepicker1" value="<?php echo date('d-m-Y');?>" required>
			</div> 
			</div> 
			<div class="form-group">
			<label class="col-md-4" style="padding-top:5px;" align="right">Staff Name : </label>
			
			<div class="col-md-7">
			    <select class="form-control" name='staffna' id='staffna'>
				<option value="">--------</option>
				<?php
				$staff=$this->db->select('*')->from('staffregistration')->get()->result();
				foreach($staff as $st)
				{				
				?>
				<option value="<?=$st->staff_name;?>"><?=$st->staff_name;?></option>
				<?php
				}
				?>
				</select>
			</div>
			</div>
			
			
			<div class="form-group" >
			<label class="col-md-4" style="padding-top:5px;" align="right">Vehicle/Others : </label>
			
			<div class="col-md-7">
			
                <select class="form-control" name='vregno' id='vregno'>
				<option value="">--------</option>
				<?php
				$vno=$this->db->select('*')->from('vehicle_registration')->get()->result();
				foreach($vno as $vn)
				{				
				?>
				<option value="<?=$vn->vehicle_regno;?>"><?=$vn->vehicle_regno;?></option>
				<?php
				}
				?>
				<option value="OTHERS">OTHERS</option>
				</select>
				
			</div>
			</div>
			
			<div class="form-group" >
			<label class="col-md-4" style="padding-top:5px;" align="right">Particulars : </label>
			
			<div class="col-md-7">
                <select class="form-control" name='particulars' id='particulars'>
				<option value="">--------</option>
				<?php
				$sttype=$this->db->select('*')->from('stock_items')->get()->result();
				foreach($sttype as $st)
				{				
				?>
				<option value="<?=$st->stock_itemname;?>"><?=$st->stock_itemname;?></option>
				<?php
				}
				?>
				</select>
				
			</div>
			</div>
			
			
			
			<div class="form-group">
			<label class="col-md-4" style="padding-top:5px;" align="right">Stock In Hand : </label>
			<div class="col-md-2">
			<input type='text' name="stinhand"  id='stinhand' class='form-control' required>
			</div> 
			<div class="col-md-2">
			<input type='text' name="units"  id='units' class='form-control'>
			</div>
			</div> 
			
		
			<div class="form-group">
			<label class="col-md-4" style="padding-top:5px;" align="right">Quantity : </label>
			<div class="col-md-4">
			<input type='number' name="qty" id='qty' class='form-control'  required>
			</div> 
			</div> 
			
			<div class="form-group">
			<label class="col-md-4" style="padding-top:5px;" align="right">Description : </label>
			<div class="col-md-7">
			<textarea rows=3 name="desc" id='desc' class='form-control' required>Nil</textarea>
			</div> 
			</div> 
			
		
<!--		<label style="background-color:#cecece;width:100%;height:1px;margin:0px padding:0px;"></label> -->
		
			<div class='form-group' style='margin-top:25px;'> 
			<label class='col-md-4 control-label' style='padding-top:3px;'></label>
				<div class='col-md-7'>
					<input type='submit' class='btn btn-primary' value='Save Details'style='padding:7px 20px 7px 20px;' />
					</center>
				</div>
			</div> 

</form>
	 
		 
</div>

<!--- second column------------------------------>
<div class="col-md-4">

</div>

</div>

	</div>
	</div>  <!---- first tab panel ---------->

					
	<div class="tab-pane <?php if($dtab=='2') echo 'active'; ?>" id="alist" role="tabpanel"> 
		<div style="background-color:#fff;padding:3px 10px 3px 10px;">
		
		
		  <div class='row' style='margin:0px;background-color:#e4e4e4;padding:5px;'>
			
			<form role="form" method="POST" action="<?php echo base_url('Stock/Delivery/1')?>" enctype="multipart/form-data">
			<label class="col-md-2 la-right" style="margin-top:5px;">Select category</label>
			
			<div class="col-md-2">
			<input type='text' class='form-control' name="sdate"   id="datepicker2" value="<?php echo date('d-m-Y');?>" required>
			</div> 
			<div class="col-md-2">
			<input type='text' class='form-control' name="edate"   id="datepicker3" value="<?php echo date('d-m-Y');?>" required>
			</div> 
			
			<div class="col-md-1" style="margin-top:2px;">
			<input type="submit" class="form-control btn btn-success" style="text-align:center;"  value="Get" name='btnget' id="btnmonth"> </div>
				</div>
			</form>
		
		
			<div class='row'>
			  <div class='col-md-11'><h4>Delivered Items List</h4> </div>
            </div>
			
			 <div class='row'>
			<hr style='margin:0px 0px 15px 0px;'>
			</div>
			
				<div class='row' style='padding-left:10px; padding-right:10px;' >
			    <table class="table table-striped table-hover table-bordered" id="example" width='100%' style='font-size:14px;'>
							<thead>
								<tr style="color:#5068f8;">
								 <th width='70px'>Del</th>
								 <th width='100px'>Date</th>
								 <th >Staff</th>
								 <th >Vehicle/Others</th>
								 <th >Particulars</th>
								 <th width='60px'>Qty</th>
								 <th width='70px'>Units</th>
								 <th >Description</th>
								 </tr>
							</thead>
							<tbody>
							<?php
							
							if(isset($delidata))
							{
								$gtotal=0;
							foreach($delidata as $r1)
							{
															
							?>
							<tr>
								 <td align='center'><!--<a href="#" id='<?php echo $r1->delivery_id;?>' class='edit' data-toggle='modal' data-target='#myModal4' ><button class='btn btn-primary btn-xs'><i class="fa fa-pencil" aria-hidden="true"></i></button></a>-->
								 <a href="<?php echo base_url('Stock/delete_delivery/'.$r1->delivery_id);?>" id='del_conf'><button class='btn btn-danger btn-xs'><i class="fa fa-trash-o" aria-hidden="true"></i></button></a></td>
								 <td><?=date_format(date_create($r1->delivery_date),"d-m-Y");?></td>
								 <td><?=$r1->delivery_staff;?></td>
								  <td><?=$r1->delivery_vehicle;?></td>
								 <td><?=$r1->delivery_particulars;?></td>
								 <td><?=$r1->delivery_qty;?></td>
								 <td><?=$r1->delivery_units;?></td>
								 <td><?=$r1->delivery_desc;?></td>
								 </tr>
							<?php 
							//$gtotal+=$r1->expense_amount;
							} 
							}?>
							</tbody>
							<thead>
								<!--<tr style="color:#5068f8;font-size:18px;">
								 	 <th colspan='7' style='text-align:right;padding-right:5px;'>Total : &nbsp;&nbsp;&#8377;&nbsp;&nbsp <b> <?=number_format($gtotal,"2",".","");?></b></th>
								 <th></th>
								 </tr>  -->
							</thead>
							
                        </table>
				</div>
                            <!-- END BORDERED TABLE PORTLET-->
		</div>						

       </div><!-- tab pane end --->
	   
		</div><!-- tab content -->
		
		</div>
</div>
  </div>
</div>
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<!-- modal windows --->

		<div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal'>&times;</button>
					<h4 class='modal-title' > <font color='blue'>Add Stock Category</font></h4>
					</div>
					<!--<form method="POST" action="<?php echo base_url('Stock/add_stock_category');?>"> -->
					
					<div class="form-group" style='margin-top:25px;margin-bottom:25px;'>
					<div class='row'>
					<label class="col-md-4" style="padding-top:5px;" align="right">Category Name : </label>
					<div class="col-md-7">
					<input type='text' name="category1" id="category1" class='form-control' required>
					</div> 
					</div> 
					</div>

					<div class='modal-footer'>
					    <button type='button' class='btn btn-primary ' id='catsubmit'> Add category </button>
						<button type='button' class='btn btn-primary ' data-dismiss='modal'>Close</button>
					</div>
					<!--</form> -->
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>     
		 
		 
		<div class="modal fade draggable-modal" id="myModal3" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal'>&times;</button>
					<h4 class='modal-title' > <font color='blue'>Add Stock Items</font></h4>
					</div>
					<!--<form method="POST" action="<?php echo base_url('Stock/add_item_types');?>"> -->
					<div class="form-group" style='margin-top:25px;margin-bottom:25px;'>
					<div class='row'>
					<label class="col-md-4" style="padding-top:5px;" align="right">Item Name : </label>
					<div class="col-md-7">
					<input type='text' name="itemname" id="itemname" class='form-control' required>
					</div> 
					</div> 
					</div>

					<div class='modal-footer'>
					    <button type='button' class='btn btn-primary ' id='itemsubmit'> Add Item </button>
						<button type='button' class='btn btn-primary ' data-dismiss='modal'>Close</button>
					</div>
					<!--</form> -->
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>   
		 
		 

		<div class="modal fade draggable-modal" id="myModal4" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >

              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>    
		 
		 
<!--- End -------------------------->

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

$(document).ready(function()
{
	$('#prn1').attr('href',"<?php echo base_url('Reports/Delivery');?>");
	
});

$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
	
$('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
$('#datepicker3').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });   
   
 
 $("#category").change(function()
 {
	  $("#particulars").prop("disabled",false);
 });
 
 function check_qty()
 {
	 var qty=$("#qty").val();
	 var sqty=$("#stinhand").val();
	 if(parseInt(qty)>parseInt(sqty))
	 {
		 alert("Quantity Insufficient.");
		 return false;
	 }
	 else
	 {
		 return true;
	 }
 }
 
 
 
 $("#particulars").change(function()
 {
	 var ina=$(this).val();
	  jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Stock/delivery_get_item_qty",
        dataType: 'html',
        data: {iname: ina},
        success: function(res)
		{
			if(res!=0)
			{
			var sq=res.split("#");	
			$("#stinhand").val(sq[0]);
			$("#units").val(sq[1]);
			$("#qty").prop("disabled",false);
			$("#qty").focus();
			}
			else
			{
				$("#stinhand").val("0");
				$("#qty").prop("disabled",true);
			}
        }
      });
 });
  

 $('#example tbody').on('click', '.edit', function (){
        var Result=$("#myModal4 .modal-body");
           var id =  $(this).attr('id');
         //alert(id);
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Stock/edit_stock",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
        Result.html(res);
         }
        });
   });

  $("#view-pdf").click(function()
{
		 var pdf_link = $(this).attr('href');
         var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
            $.createModal({
				title:'Delivery Items',
				message: iframe,
				closeButton:true,
				scrollable: true,
			});
			return false; 

});	
  
   	

 $(document).on("click", "#del_conf", function () 
	{
        return confirm('Are you sure you want to delete this entry?');
    });

	
	
	
	

</script>



