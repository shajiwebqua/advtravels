<?php
class Model_stationary extends CI_Model{
	
    public function select($column,$where = null)
    {
        $this->db->select($column);
        $this->db->from('staff_salary');
        if($where != null)
            $this->db->where($where);
        
        $query=$this->db->get();
        return $query->result();
    }
	
    
	public function insert_salary($values){
        $this->db->insert('staff_salary1', $values);  
    }
	
    public function update($values, $where){
        $this->db->where($where);
        $this->db->update('staff_salary', $values); 
    }

	 public function insert_advance($values){
        $this->db->insert('salary_advance', $values); 
    }
		
	public function view_salary_details()
	{
		$this->db->select('*')->from("staff_salary");
		$this->db->join("staffregistration","staffregistration.staff_id=staff_salary.salary_staffid","inner");
		
		$where="(YEAR(salary_date)='".date('Y')."') ";
		$this->db->where($where);
		
		$this->db->order_by("salary_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	
	public function view_salary_details1($dt1,$dt2,$op)
	{
		$this->db->select('*')->from("staff_salary");
		$this->db->join("staffregistration","staffregistration.staff_id=staff_salary.salary_staffid","inner");
		if($op==1)
		{
			$where="(salary_date>='".$dt1."' and salary_date<='".$dt2."') ";
		}
		else if($op==1)
		{
			$where="(MONTH(salary_date)='".$dt1."' and YEAR(salary_date)='".$dt2."') ";
		}
		else
		{
			$where="(YEAR(salary_date)='".date('Y')."') ";
		}
		
		$this->db->where($where);
		$this->db->order_by("salary_id","desc");
		
		$query=$this->db->get();
		return $query->result();
	}
	
	public function delete_salary($id)
	{
    $this->db->where('salary_id', $id);
    $this->db->delete('staff_salary');
    return $this->db->affected_rows();
    }
	
	public function view_advance_salary()
	{
		$this->db->select('*')->from("salary_advance");
		$this->db->join("staffregistration","staffregistration.staff_id=salary_advance.sal_adv_staffid","inner");
		
		$where="(YEAR(sal_adv_date)='".date('Y')."') ";
		$this->db->where($where);
		
		$this->db->order_by("sal_adv_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
	
	public function view_advance_salary1($dt1,$dt2,$op)
	{
		$this->db->select('*')->from("salary_advance");
		$this->db->join("staffregistration","staffregistration.staff_id=salary_advance.sal_adv_staffid","inner");
		if($op==1)
		{
			$where="(sal_adv_date>='".$dt1."' and sal_adv_date<='".$dt2."') ";
		}
		else if($op==1)
		{
			$where="(MONTH(sal_adv_date)='".$dt1."' and YEAR(sal_adv_date)='".$dt2."') ";
		}
		else
		{
			$where="(YEAR(sal_adv_date)='".date('Y')."') ";
		}
		
		$this->db->where($where);
		
		$this->db->order_by("sal_adv_id","desc");
		$query=$this->db->get();
		return $query->result();
	}
		
	public function delete_advance($id)
	{
    $this->db->where('sal_adv_id', $id);
    $this->db->delete('salary_advance');
    return $this->db->affected_rows();
    }
		
	public function get_advance_total()
	{
	    $this->db->select('staff_name,SUM(sal_adv_amount) as total')->from("salary_advance");
		$this->db->join("staffregistration","staffregistration.staff_id=salary_advance.sal_adv_staffid","inner");
		$where="(MONTH(sal_adv_date)='".date('m')."') ";
		$this->db->where($where);
		$this->db->GROUP_by("staff_name");
		$query=$this->db->get();
		return $query->result();
	}
	
}