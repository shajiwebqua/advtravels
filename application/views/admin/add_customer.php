<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//*******************************************************************
//loading the header page
include('application/views/common/header.php');
?>
<style>
	/* hide number  spinner*/
	input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}
	.ttext
	{
		background-color:#fff;
		font-weight:bold;
		color:Green;
	}
	
.entry:not(:first-of-type)
{
    margin-top: 10px;
}

.glyphicon
{
    font-size: 12px;
}

	
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 

	<!-- Main content -->

	<?php 
    //include("application/views/common/top_menu.php");
	?>

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><b>New Customer Details</b> </h1>
		<ol class="breadcrumb">
			<li> <a href="<?php echo base_url("Customer");?>" style="color:#4b88ed;font-size:15px;"> <button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Customers List </button></a></li>
		</ol> 
	</section>
	<label style="background-color:#cecece;width:100%;height:1px;"></label>
	<!-- Content Header end -->
	<section class="content"> 
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12">

				<!--Add new user details -->
				<div style="background-color:#fff;border-radius:2px;padding:15px; ">

					<div class="portlet-title">
 <!--   <div class="caption">
        <div style="color:blue;" >
          Details                   
      </div> -->
   <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:5px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div>
				 <div id='msgerr'><?php echo $this->session->flashdata('message1'); ?></div>
				 </center></div>
      <!--</div> -->
  </div>
  <div class="portlet-body form">
  		<!-- text input -->
  		<div class="box-body">
  			<div class='col-md-12'>        
  			<form  class='form-horizontal' role='form' method='post' action="<?php echo base_url('Customer/add_new_customer1/1') ?>" enctype='multipart/form-data' onsubmit='return checkdata();' >
  				<!-- text input -->
  				<div class='row'> 
  					<div class='col-md-10'>
  						<div class='form-group'>
						
						<div class='row' style='margin-top:10px;'>
  							<label class='col-md-3 control-label' style='padding-top:3px;'>Customer Code : </label>
  							<div class='col-md-4'>  
  								<input type='hidden' name='opt' value='$op'/>
  								<input type='text' name='custcode' class='form-control' value='<?php echo $ccode; ?>' readonly />
  							</div>                   
  						</div>

						<div class='row' style='margin-top:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;'>Customer Category : </label>
							<div class='col-md-4'> 
								<select name='custtype' class='form-control' id='custtype' required>
									<option value=''>----------</option> 
									<option value='1'>INDIVIDUAL</option>
									<option value='2'>CORPORATE</option> 
									<option value='3'>AGENT</option> 
									
								</select>
						</div>                   
						</div>
						
  						<div class='row' style='margin-top:10px;'>
  							<label class='col-md-3 control-label' style='padding-top:3px;'>Date : </label>
  							<div class='col-md-4'>  
  								<input type='date' name='custdate' class='form-control' value='<?php echo date('Y-m-d');?>' required />
  							</div>                   
  						</div>

  						<!--<div class='row' style='margin-top:10px;'>
  							<label class='col-md-3 control-label' style='padding-top:5px;'>Category</label>
  							<div class='col-md-4'>  
  								<select name='category' class='form-control' required>
  									<option value=''>----------</option>
  									<?php
							  $result1=$this->Model_customer->view_category();
							  foreach($result1 as $ro){
								   echo "<option value='".$ro->category_id."'>".$ro->category_name."</option>";
							  }
							   ?>
						   </select>

						</div>                   
						</div>  -->
						
						<div class='row' style='margin-top:10px;'>
						<div id='co_agent'>
							<label class='col-md-3 control-label' style='padding-top:3px;'>C/O Agent : </label>
							<div class='col-md-4'> 
								<select name='coagent' id='coagent' class='form-control' >
									<option value=''>----------</option> 
									<?php
							  $result1=$this->Model_customer->view_agents();
							  foreach($result1 as $ro){
								  echo "<option value='".$ro->agent_id."'>".$ro->agent_name."</option>";
							  }
							  ?>
						  </select>
						  <input type='hidden' name='coagent1' class='form-control' id='coagent1'  value='' /> 
						  </div>
						</div>                   
						
						</div>    
						<div class='row' style='margin-top:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;'>Name : </label>
							<div class='col-md-7'>  
								<input type='text' name='name' class='form-control'  id='mask_date2' required />
							</div>                   
						</div>

						<div class='row' style='margin-top:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;'>Address : </label>
							<div class='col-md-7'>  

								<textarea name='address' rows='3' class='form-control' required></textarea>
							</div>                   
						</div>
						
					<div class='row' style='margin-top:10px;'>
						<label class='col-md-3  control-label'>Mobile : </label>
						<div class='col-md-4'>
							<input type='number' class='form-control' placeholder='Enter mobile no' name='mobile' id='mobile' required>
							<label id='mobmessage' style='color:red;'></label>
						</div>
					</div>

					<div class='row' style='margin-top:10px;'>
						<label class='col-md-3  control-label'>Landline : </label>
						<div class='col-md-4'>
							<input type='number' class='form-control' placeholder='Enter landline' name='landline' value='' required id='landline' required/>
							<label id='landmessage' style='color:red;'></label>
						</div>
					</div>
					
					<div class='row' style='margin-top:10px;'>
						<label class='col-md-3 control-label' style='padding-top:3px;'>Email : </label>
						<div class='col-md-7'>  

							<input type='email' name='email' class='form-control'  id='mask_date2' required/>
						</div>                   
					</div>
					
					
					<div class='row' style='margin-top:20px;'>
						<label class='col-md-3  control-label'>Gst% : </label><label style='margin-top:5px;'>Eg: 18</label>
						<div class='col-md-2'>
							<input type='number' class='form-control' placeholder='enter gst%' name='gst' value=''/>
						</div>
					</div>
					
					<div class='row' style='margin-top:10px;'>
						<label class='col-md-3 control-label' style='padding-top:3px;'>Gst Number : </label>
						<div class='col-md-4'>  
							<input type='text' name='gstno' placeholder='enter gst no' class='form-control' required/>
						</div>                   
					</div>
				

					<div class='row' style='margin-top:20px;'>
						<label class='col-md-3 control-label' style='padding-top:3px;'>Country : </label>
						<div class='col-md-4'> 
							<select name='country' class='form-control' id='country1' required>
								<option value=''>----------</option>
								<?php	
								$result1=$this->Model_customer->view_country();

								foreach($result1 as $ro){
								echo "<option value='".$ro->country_id."'>".$ro->country_name."</option>";
							}
							?>
						</select>
					</div>                   
				</div> 

					<div class='row' style='margin-top:10px;'>
						<label class='col-md-3 control-label' style='padding-top:3px;'>Area : </label>
						<div class='col-md-4'>  
							<select name='area' class='form-control' id='idarea' required>

							</select>
						</div>                   
					</div>

					
					
					<!--  new fields ------------------------------------->
					<div id="KCA" style='padding-left:10px;'>

						<div class='row' style='background-color:#f4f4f4;margin-top:10px;'>
						
						<div class='row' style='margin-top:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;'>Key Person : </label>
							<div class='col-md-5'>  
								<input type='text' name='keyperson' class='form-control'  id='keyperson' />
							</div>                   
						</div>
						
						<div class='row' style='margin-top:10px;margin-bottom:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;'> Mobile: </label>
							<div class='col-md-4'>  
								<input type='text' name='keymobile' class='form-control'  id='keymobile'/>
							</div>                   
						</div>
						</div>
						
						<div class='row' style='background-color:#f4f4f4;margin-top:10px;'>
						<div class='row' style='margin-top:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;'>Contact Person : </label>
							<div class='col-md-5'>  
								<input type='text' name='conperson' class='form-control'  id='conperson' />
							</div>                   
						</div>
						
						<div class='row' style='margin-top:10px;margin-bottom:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;'> Mobile: </label>
							<div class='col-md-4'>  
								<input type='text' name='conmobile' class='form-control'  id='conmobile' />
							</div>                   
						</div>
						</div>
						
						<div class='row' style='background-color:#f4f4f4;margin-top:10px;'>
						<div class='row' style='margin-top:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;'>Account Person : </label>
							<div class='col-md-5'>  
								<input type='text' name='accperson' class='form-control'  id='accperson' />
							</div>                   
						</div>
						
						<div class='row' style='margin-top:10px;margin-bottom:10px;'>
							<label class='col-md-3 control-label' style='padding-top:3px;'> Mobile: </label>
							<div class='col-md-4'>  
								<input type='text' name='accmobile' class='form-control'  id='accmobile' />
							</div>                   
						</div>
					
						</div>
					
					</div>
					<!--  new fields end------------------------------------->
					
					
					
					
					
					<div class='row' style='margin-top:10px;'>
						<label class='col-md-3 control-label' style='padding-top:3px;'>Description : </label>
						<div class='col-md-7'>  
							<textarea name='description' rows='3' class='form-control' required></textarea>
						</div>                   
					</div>

					<!---  attache your proof ----------------------------------->
					
					<div class='form-group' style='margin-top:20px;'> 			
					
					  <div class="row">
					  <label class='col-md-3 control-label' style='padding-top:3px;'>Upload Id Proof: </label>
					  
					  <div class="control-group col-md-6" id="fields" style='padding-left:20px;'>
						  <div class="controls">
						   	  <div class="entry input-group col-xs-3" style='margin-top:5px;'>
								<input class="btn btn-primary" name="files[]" type="file">
								<span class="input-group-btn">
							  <button class="btn btn-success btn-add" type="button">
												<span class="glyphicon glyphicon-plus"></span>
								</button>
								</span>
							  </div>
						 
						</div>
					  </div>
					</div>
					</div>
<!---------------------------------------------------------------->					

					<label style='width:100%;height:1px;'></label>  
					<div class='form-group'> 
					<label class='col-md-3 control-label' style='padding-top:3px;'></label>
						<div class='col-md-7'>
							<input type='submit' class='btn btn-primary' value='Save Details'/>
							</center>
						</div>
					</div> 
					
			</div>  
			</div>
			</div>
			</form>
		</div>

</div>
</div>	
</div>   
</div>
</div>

</section>
</div>

<?php include("application/views/common/footer.php");?>


<script type="text/javascript">
$('#mobmessage').hide();
$('#landmessage').hide();
$("#co_agent").hide();	
$("#KCA").hide();
	
$("#custtype").change(function()
{
	var op=$("#custtype").val();
	if(op==2)
	{
		$("#KCA").show();
	}
	else if(op==3)
	{
		$("#co_agent").show();
		$("#KCA").hide();
	}
	else
	{
		$("#co_agent").hide();
		$("#KCA").hide();
	}
});


$("#coagent").change(function()
{
	if($("#coagent").val()!="")
	{
		var aid=$("#coagent").val();
	$("#coagent1").val(aid);
	}
	
});
	
	///dropdown list  customer name

	   $('#id2').selectize({
	     create: false
	     , sortField: {
	     //field: 'text',
	     direction: 'asc'
	   }
	   , dropdownParent: 'body'
	   });
		  
	  
		  
	  function checkdata()
	  {
	    
	    var mob=$('#mobile').val();
	    var landline=$('#landline').val();
	      
	  
	    if(mob.length<10 || mob.length>10)
	    {
	      $('#mobmessage').show('');
	      $('#mobmessage').html('Invalid mobile no,10 digits only.');
	      return false;
	    }
				/*else if(landline.length<11 || landline.length>11)
				{
				 $('#mobmessage').hide('');
				  $('#landmessage').show('');
				  $('#landmessage').html('Invalid mobile no,11 digits only.');
				  return false;
				}*/
	        
	    else
	    {
	    $('#mobmessage').hide();
	      $('#landmessage').hide();
	      $('#mobmessage').html('');
	      $('#landmessage').html('');
	      $(this).submit();
	    }
	  }

//sweet alert box ----------------------
$("#idmsg").hide();
if($.trim($("#msg").html())!="")
	{
		swal("Saved",$("#msg").html(),"success")
		$("#msg").html("");
	}
	
$("#idmsgerr").hide();
if($.trim($("#msgerr").html())!="")
	{
		swal("Try Again",$("#msgerr").html(),"success")
		$("#msgerr").html("");
	}	
	
//-------------------------------------
	     
	/* get area of selected country  */

	  $('#country1').change(function () {
	        var id=$('#country1').val();
	        jQuery.ajax({
	        type: 'POST',
	        url: "<?php echo base_url('') ?>" + 'Customer/view_areas',
	    	data:{cid:id},
	        success: function(data)
	    	{
	        	$('#idarea').html(data);
	        }
	        });
	       }); 

</script> 

<script>
$(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
      $(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});
</script>

