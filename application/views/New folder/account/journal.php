<?php
include('application/views/common/acc_header.php');
?>
  <style>
  .dashbtn
  {
	  width:75px;
	  height:75px;
	  background-color:#f4f4f4;
	  border:1px solid #fff;
	  /*border-radius:10px;*/
	  padding:5px;
	  text-align:center;
  }
  
.sh4
{
 border-bottom:1px solid #e4e4e4;
 width:100%;
 color:#00adee;
 padding-bottom:5px;
 margin-top:0px;"
}

  </style>
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class='heading' style='color:#00adee'> Voucher Creation
           </h1>
          <ol class="breadcrumb">
            <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
          </ol>
     <label style="background-color:#cecece;width:100%;height:1px;"></label>   
     </section>
	
        <!-- Main content -->
    <section class="content">
	
	<div class='row' style="padding:5px 15px 5px 15px;">
	<div class="box box-info1 box-solid " style="border:1px solid #c4c4c4">
	    <div class="box-header">
          <h3 class="box-title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> JOURNAL</h3>
		  <label id='mes' ><?php echo $this->session->flashdata('message');?></label>
        </div>
		
        <div class="box-body">
        	<div class='row' style='padding:15px;'>
			<div class='col-md-6'>
				 <form onsubmit='return confirm_form();'class="form-horizontal" role="form" method="POST" action="<?php echo base_url('Account/save_voucher')?>" enctype="multipart/form-data">
                  
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Voucher Type :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="vtype" value='JOURNAL'>
								</div>
							</div>
						</div>
												
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">To :</label>
								<div class="col-md-7">
									<select name='vledger' class='form-control' required>
									<option value="">----------</option>
									<?php
										$ledg=$this->db->select('*')->from('acc_ledgers')->get()->result();
										foreach($ledg as $lg)
										{
										echo "<option value=".$lg->acc_ledg_id.">".$lg->acc_ledg_lname."</option>";
										}
									?>
									</select>
									
								</div>
							</div>
						</div>
												
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">By :</label>
								<div class="col-md-7">
									<select name='vledgerto' class='form-control' required>
									<option value="">----------</option>
										<?php
										$ledg=$this->db->select('*')->from('acc_ledgers')->get()->result();
										foreach($ledg as $lg)
										{
										echo "<option value=".$lg->acc_ledg_id.">".$lg->acc_ledg_lname."</option>";
										}
										?>
									</select>
								</div>
							</div>
						</div>		
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Amount :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="vamount" value='' required>
								</div>
							</div>
						</div>	
						
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1">Narration :</label>
								<div class="col-md-7">
									<textarea rows='3' class="form-control"  name="vdesc" required></textarea>
								</div>
							</div>
						</div>	
											
						
						<hr style='margin:15px;'>
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-3 control-label left1"></label>
								<div class="col-md-7">
									<input type="submit" class="btn btn-primary" name="btnsubmit" value='Save Journal Voucher'>
								</div>
							</div>
						</div>
						
			</form>
		
		
			</div>
			</div>
           
        </div>
	</div>
	</div>
	
</section>
    <!-- content wrapper -->
</div>
     <?php
  include('application/views/common/acc_footer.php');
  ?>
   
</body>
</html>
  <script type="text/javascript">
  $("#mes").hide();
  
  if($("#mes").html()!="")
  {
	  swal("Saved.!",$("#mes").html(),"success");
	  $("#mes").html("");
  }
  
  function confirm_form()
  {
	  return confirm("Are you sure, submit this details?");
  }
  
  $("#vcategory").change(function()
  {
	 
	 var vcat=$("#vcategory").val();
	$("#vname").empty();
	 jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Account/category_names",
        dataType: 'html',
        data: {vcatid:vcat},
        success: function(res) 
		{
			$("#vname").append(res);
        }
		});
  });
  
  
  
  </script>
</body>
</html>

  
  