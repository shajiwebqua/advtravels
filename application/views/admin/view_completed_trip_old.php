<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}
</style>

 <!-- Content Wrapper. Contains page content -->
 
<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Completed Trip List</b></h1>
   <ol class="breadcrumb" style='font-size:15px;'>
   <li> <a href="<?php echo base_url('Trip/index');?>" style="color:#4b88ed;font-size:15px;"><button class='btn btn-primary'> <i class="fa fa-cab" aria-hidden="true"></i> New Trip Sheet</button></a></li>
		<li><a id='view-pdf' href="<?php echo site_url('Pdf/pdf_trip') ?>" style='color:#4b88ed;'><button class='btn btn-primary'> <i class="fa fa-print" aria-hidden="true"></i> Print/Save</button></a></li>
		
		<!-- data-target="#myModal1" --><li><a id='idshare' href="" data-target="" data-toggle='modal' style='color:#4b88ed;'><button class='btn btn-primary'> <i class="fa fa-share-alt" aria-hidden="true"></i> Share</button></a></li>
		<!-- data-target="#myModal1" --><li><a id='idview' href="" data-target="" data-toggle='modal'style='color:#4b88ed;'><button class='btn btn-primary'> <i class="fa fa-eye" aria-hidden="true"></i> View</button></a></li>
		<!-- data-target="#myModal1" --> 
		<?php 
		if(checkisAdmin()){
			
			echo "<li><a id='idedit' href='' style='color:#4b88ed;'><button class='btn btn-primary'> <i class='fa fa-pencil-square-o' aria-hidden='true'></i> Edit</button></a></li> 
			<li><a id='iddelete' href='' style='color:#4b88ed;'><button class='btn btn-primary'> <i class='fa fa-trash' aria-hidden='true'></i> Delete</button></a></li>";
		}
		?>
     </ol> 
 </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header  end -->	  
	  
  <section class="content"> 
  <div style="color:blue;font-size:12px;"> Select Trip details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Print,Share,Save,View,Edit,Delete)  </div>
  <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>  
  <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details-->
	
	 <div style="background-color:#fff;border-radius:3px;padding:15px; ">
					
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
<!--- search ---------------------------------------- -->
	<form class="form-horizontal" role="form" method="POST" action="<?php echo base_url('enquiry/view_enquiry')?>" >
				
				<!--<h4>Search </h4>
				 <label style="background-color:#cecece;width:100%;height:1px;"></label>-->


				 <div class="row">
                                 <div class="col-md-1" style="border-right:1px solid #cecece">
				 <h4  style="color:blue; margin-top:3px">Search </h4> 
				 </div>

				 <div class="col-md-6">
                    <div class="form-group">
						<label class="col-md-3 control-label" style="padding-top:3px;" >Select Date : </label>
						<div class="col-md-3">
						<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" readonly id="datepicker1" value="<?php echo date('d-m-Y'); ?>">
						</div> 
						</div>
						<div class="col-md-1" style="width:20px;padding:4px 0px;">=>
						</div>
						<div class="col-md-3">
						<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
						<input type="text" class="form-control date-pick" readonly id="datepicker2" value="<?php echo date('d-m-Y'); ?>">
						</div>
						</div>
						<div class="col-md-2">
							<input class="form-control btn btn-primary" style="text-align:center;" type="button" value="Get" id="btnget">
						</div>
					</div>
				</div> 
				
				<div class="col-md-5" style="border-left:2px solid #e4e4e4;">
					<div class="form-group" >
					<label class="col-md-3  control-label col-md-offset-1" style="padding-top:3px;">Select Month : </label>
					<div class="col-md-7">
					<select class="form-control" id="pmonth" name="pmonth1">
					<option value="0">-----</option>
					<option value="1">January</option>
					<option value="2">February</option>
					<option value="3">March</option>
					<option value="4">April</option>
					<option value="5">May</option>
					<option value="6">June</option>
					<option value="7">July</option>
					<option value="8">August</option>
					<option value="9">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
					</select>
					</div> 
					<!--<div class="col-md-2">
						<input class="form-control" style="text-align:left;" type="button" value="All" id="btnall">
					</div> -->
					</div>
				    </div>
                           <label style="background-color:#cecece;width:100%;height:1px;"></label>
				</div>
			
				</form>
<!-- search end ------------------------------------- -->
				<div class="row">
                <div class="col-md-12"  style='padding-left: 10px;padding-bottom:5px;' >
				<h5 id='ctperiod'><b>To display 3 months data only</b></h5>
				</div>
				</div>
				
				
				
				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                    <div class="row">
                   <div class="col-md-12"  style=' padding-left: 10px;' >
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>

						<table class="table table-striped table-hover table-bordered" width='100%' id="example" style='font-size:14px;'>
                        <thead>
								<tr style='color:#4b88ed'>
								 <th></th>
								 <th>Trip_Id</th>
								 <th>Customer</th>
								 <th>Ref.No</th>
								 <th>Driver</th>
								 <th>Date_Days_KM</th>
								 <th>Charge</th>
								 <th>Total</th>
								 <th>Status</th>
								 </tr>
								</thead>
                        </table>

                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-body" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Edit</h4>
								</div>
							</div>
							</div>
						<!-- /.modal-dialog -->
						</div>
                   </div>			

           <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
			  
		
            <!-- /.modal-dialog -->
            </div>
                   </div>  

            <div class="modal fade draggable-modal" id="myModals" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 id='mtitle' class="modal-title">Share</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
                   </div> 
            </div>       

				
	</div>
	<!-- End user details -->

    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
//sweet alert box ----------------------
$("#idmsg").hide();
if($("#msg").html()!="")
	{
		swal($("#msg").html(),"","success")
		$("#msg").html("");
	}
//-------------------------------------
 $('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
   
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });  
   
  



  
   
 
      $('#example').dataTable({
		 "ordering": false,
         destroy: true,
        "processing": true,
       
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/trip_ajax/2",// json datasource
               },
	"columnDefs":[
	{"width":"15%","targets":5},
	{"width":"12%","targets":6},
	],
			   
        "columns": [
            { "data": "invoice" },
			//{ "data": "tclose" },
			{ "data": "tripid" },
            { "data": "comp_customer"},
			{ "data": "refno"},
			{ "data": "driver"},
            { "data": "comp_date" },
			{ "data": "comp_charge" },
			{ "data": "gtotal" },
			{ "data": "status" },
       ]
  }
  );
  
  
  
  $("#btnget").click(function(){
      var dt1=$('#datepicker1').val();
	  var dt2=$('#datepicker2').val();
	 	  $("#ctperiod").html("");
		  
	  $('#example').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/trip_ajax/3/"+dt1+"/"+dt2,// json datasource
               },
			  
			   
        "columns": [
            { "data": "invoice" },
			//{ "data": "tclose" },
			{ "data": "tripid" },
            { "data": "comp_customer"},
			{ "data": "refno"},
			{ "data": "driver"},
            { "data": "comp_date" },
			{ "data": "comp_charge" },
			{ "data": "gtotal" },
			{ "data": "status" },
       ]
  } );

  });
    
  $("#pmonth").change(function(){
$("#ctperiod").html("");
      var m=$('#pmonth').val();
      $('#example').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/trip_ajax/4/"+m,// json datasource
               },
			   
			   "columns": [
            { "data": "invoice" },
			//{ "data": "tclose" },
			{ "data": "tripid" },
            { "data": "comp_customer"},
			{ "data": "refno"},
			{ "data": "driver"},
            { "data": "comp_date" },
			{ "data": "comp_charge" },
			{ "data": "gtotal" },
			{ "data": "status" },
       ]
  } );
  });
  

var table = $('#example').DataTable();
     $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
	        table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
  });			   
  
  
  $('#view-pdf').on('click',function(){
       var id=$('#example').find('tr.selected').find('td').eq(1).text();
     if(id=="")
     {
       alert ("please Select trip details..");
     }
     else
     {
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
		var pdf_link = $(this).attr('href')+'/'+id;
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><div id="overlay"><div id="loading"></div><iframe id="myDiv" class="animate-bottom" src="'+pdf_link+'"></iframe></div></div>';
//alert(id);
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
       } return false; 

    });    

  
      $('#idedit').click(function () {
        var id=$('#example').find('tr.selected').find('td').eq(1).text();
		
		
		 if(id=="")
		 {
			 alert ("please Select trip details..");
		 }
		 else
		 {
			$("#idedit").attr("href","trip_edit/"+id); 
			 
			 
       /* jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/trip_edit/"+id,
        dataType: 'html',
        data: {tid: id},
        success: function(res) 
		{
			
		}*/
        }
		}); 

		
  $('#idview').click(function ()
  {
        var Result=$("#myModal1 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(id=="")
		 {
			 alert ("please Select trip details..");
		 }
		 else
		 {
			$("#idview").attr('data-target','#myModal1') 
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/trip_details",
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
		 }
        }); 		


       $('#idshare').click(function () {
        var Result=$("#myModals .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
        var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(id=="")
		 {
			 alert ("please Select trip details..");
		 }
		 else
		 {
			$("#idshare").attr('data-target','#myModals') 
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/share_trip_details",
        dataType: 'html',
        data: {id: id},
        success: function(res) {
			 Result.html(res);
                    }
            });
		 }
    
        });  
		

    $('#example tbody').on('click', '.tclose', function () {
        var Result=$("#myModals .modal-body");
         var id =  $(this).attr('id');
			jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "Trip/close_trip",
			dataType: 'html',
			data: {tid: id},
			success: function(res) {
				Result.html(res);
            }
            });
    
        }); 



 $('#idsave').on('click',function(){
       var id=$('#example').find('tr.selected').find('td').eq(1).text();
     if(id=="")
     {
       alert ("please Select trip details..");
     }
     else
     {
		var pdf_link = $(this).attr('href')+'/'+id;
        var iframe = '<div class="iframe-container"><div id="overlay"><div id="loading"></div><iframe id="myDiv" class="animate-bottom" src="'+pdf_link+'"></iframe></div></div>';
		window.open(pdf_link);
       } return false; 

    });    


		
	$('#iddelete').click(function () 
    {
    	 var id=$('#example').find('tr.selected').find('td').eq(1).text();
    	  if(id=="")
         {
             alert ("please Select trip details..");
         }
         else
         {
    var res=confirm("Delete this trip details?");
    if(res)
    {
    var id=$('#example').find('tr.selected').find('td').eq(1).text();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/del_entry",
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
    
    }  
     } 
        });     
	
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });
 //});
  


</script>



