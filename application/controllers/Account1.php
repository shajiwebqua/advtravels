<?php  

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account1 extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		if (!isset($user))
		{ 
			//redirect('/');
		} 
		else
		{ 
			return true;
		}
		$this->load->helper(array('form', 'url','html'));
	}
	
	public function index()
	{
        $this->load->view("accounts/dashboard"); 
	}

	public function blank()
	{
        $this->load->view("accounts/blank"); 
	}
	
// to load account group page
	public function groups()
	{
		//$op=$this->uri->segment(3);
		$this->load->model("Model_accounts");
		$data['result']=$this->Model_accounts->group_list();
        $this->load->view("account/acc_groups",$data); 
	}
		
//to load voucher type adding page
	public function voucher()
	{
		$this->load->model("Model_accounts");
		$data['result']=$this->Model_accounts->voucher_type_list();
        $this->load->view("account/acc_vouchertypes",$data); 
	}
	
//to get nature of groups to adding/editing page	
	public function get_group_nature()
	{
		$gid=$this->input->post('gid');
		$row=$this->db->select('acc_grpnature')->from('acc_groups')->where('acc_grpid',$gid)->get()->row();
		echo $row->acc_grpnature;
	}

//to get ledger details for editing
	public function get_ledger_details()
	{
		$str="";
		$lid=$this->input->post('ledgid');
		$row=$this->db->select('*')->from('acc_ledgers')->where('acc_ledgid',$lid)->get()->row();
		
		$str.=$row->acc_ledgid.",";
		$str.=$row->acc_ledgname.",";
		$str.=$row->acc_ledunder.",";
		$str.=$row->acc_ledsubgroup.",";
		echo $str;
	}
	
// to save account groups to table	
		
	public function save_group()
	{
	$this->load->model("Model_accounts");
	
	
	$grpcode=$this->input->post('grpcode');
	$grpname=$this->input->post('grpname');
	$grpunder=$this->input->post('grpunder');
	$gp=explode("#",$grpunder);
	
//	$row=$this->db->select('acc_grpunder')->from('acc_groups')->where('acc_grpid',$grpunder)->get()->row();
	//$grpunder1=$row->acc_grpunder;
	
	$data1=array(
			'acc_grp_code'=>$grpcode,
			'acc_grp_description'=>strtoupper($grpname),
			'acc_grp_parentid'=>$gp[0],
			'acc_grp_type'=>$gp[1],
			);
		
	$row=$this->db->select('*')->from('acc_groups')->where('acc_grp_description',strtoupper($grpname))->get();
	if($row->num_rows()>0)
	{
	$this->session->set_flashdata('message', '4#Account Group exist.'); 
	redirect('Account/groups');
	}
	else
	{
	$this->Model_accounts->insert("acc_groups",$data1);
	$this->session->set_flashdata('message', '1#Account Group sccessfully added.'); 
	redirect('Account/groups');
	}
}

public function edit_group()
	{
		$grid=$this->input->post('gid');
		
		$grow=$this->db->select("*")->from('acc_groups')->where('acc_grp_id',$grid)->get()->row();

	echo '<div class="modal-header" style="background-color:#cacaca ;">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true" >&times</span>
        </button>
		<h4 class="modal-title" id="exampleModalLabel">Edit Account Group</h4>
      </div>
	  
	  <form class="form-horizontal" role="form" method="POST" action="'. base_url("Account/update_group").'" enctype="multipart/form-data">
	  		
	  <input type="hidden" class="form-control"  name="mgrpid" value="'.$grid.'" required>
				<div class="form-group" style="margin-top:10px;">
				   <div class="row">
				   	  <label class="col-md-4 control-label">Group Name :</label>
						<div class="col-md-7">
							<input type="text" class="form-control"  name="mgrpname" value="'. $grow->acc_grp_description .'"  required>
						</div>
					</div>
					
					<div class="row" style="margin-top:10px;">
					  <label class="col-md-4 control-label">Under :</label>
					<div class="col-md-7">
					  <select  class="form-control" name="mgrpunder" required>
						<option value="">---type----</option>';
	
							$gquery=$this->db->select("*")->from('acc_group_master')->get()->result();
							foreach( $gquery as $g)
							{
								echo "<option value='".$g->acc_mgrp_id."#".$g->acc_mgrp_id."'"; if($g->acc_mgrp_id==$grow->acc_grp_parentid) echo "selected"; echo ">".$g->acc_mgrp_type."</option>";
								
							}
							
						echo '</select>
					  </div>
					</div>
					
				</div>
	
		  <div class="modal-footer" style="margin-right:30px;">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Save changes</button>
		  </div>
	  </form>
	';
}

//to update account group to table
	public function update_group()
	{
	$this->load->model("Model_accounts");
	
	$grpid=$this->input->post('mgrpid');
	$grpname=$this->input->post('mgrpname');
	$grpunder=$this->input->post('mgrpunder');
	$gp=explode("#",$grpunder);
	$data1=array(
			'acc_grp_description'=>strtoupper($grpname),
			'acc_grp_parentid'=>$gp[0],
			'acc_grp_type'=>$gp[1],
			);
 
	$where="acc_grp_id=".$grpid;
	$this->Model_accounts->update("acc_groups",$data1,$where);
	$this->session->set_flashdata('message', '2#Group sccessfully updated.'); 
	redirect('Account/groups');
	}
//delete groups ----------------------
	
	public function delete_group()
	{
		$grpid=$this->uri->segment(3);
		$this->load->model("Model_accounts");
	
	    $rec=$this->db->select('COUNT(*) as cnt')->from('acc_ledgers')->where('acc_led_groupid',$grpid)->get()->row();
		if($rec->cnt<=0)
		{
			$where="acc_groups.acc_grp_id=".$grpid;
			$this->db->where($where)->delete("acc_groups");
			$this->session->set_flashdata('message', '3#Group sccessfully removed.'); 
			redirect('Account/groups');
		}
		else
		{
			$this->session->set_flashdata('message', '4#Could not remove this Group. It is link with another data.'); 
			redirect('Account/groups');
		}
	}

// to load account ledger adding page
	public function ledger()
	{
		$nos=$this->get_ledger_no();
		$this->load->model("Model_accounts");
		$data['result']=$this->Model_accounts->ledger_list();
		$data['lnos']=$nos;
        $this->load->view("account/acc_ledgers",$data); 
	}	
	
	public function get_ledger_no()
	{
		$lno=1001;
		$res=$this->db->select("MAX(acc_led_code) as nos")->from("acc_ledgers")->get()->result();
		foreach($res as $rs)
		{
			if($rs->nos<=0)
			{
				return $lno;
			}
			else
			{
				return $lno=$rs->nos+1;
			}
		}
	}
	
//to save account ledger to table
	public function save_ledger()
	{
	$this->load->model("Model_accounts");
		
	$lcode=$this->input->post('lcode');
	$lname=$this->input->post('lname');
	$lunder=$this->input->post('lunder');
		
	$data1=array(
			'acc_led_code'=>$lcode,
			'acc_led_description'=>strtoupper($lname),
			'acc_led_groupid'=>$lunder,
			);
			
	$row=$this->db->select('*')->from('acc_ledgers')->where('acc_led_code',$lcode)->or_where('acc_led_description',strtoupper($lname))->get();
	if($row->num_rows()>0)
	{
	$this->session->set_flashdata('message', '4#Ledger Code/Name already exist.'); 
	redirect('Account/ledger');
	}
	else
	{
	$this->Model_accounts->insert("acc_ledgers",$data1);
	$this->session->set_flashdata('message', '1#Ledger sccessfully added.'); 
	redirect('Account/ledger');
	}
}

	public function edit_ledger()
	{
	   $id=$this->input->post('lid');
       $query = $this->db->select('*')->from('acc_ledgers')->where('acc_led_id',$id)->get();
       $row = $query->row();
	
	
	echo '<div class="modal-header" style="background-color:#cacaca;margin:0px ;padding:5px 10px 5px 20px;">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding:5px;background-color:#red;">
          <span aria-hidden="true" >&times;</span>
        </button>
		<h4 class="modal-title" id="exampleModalLabel" >Edit Ledger</h4>
      </div>
	  
	  <form class="form-horizontal" role="form" method="POST" action="'. base_url('Account/update_ledger').'" enctype="multipart/form-data">
		<div class="modal-body">
			<input type="hidden" class="form-control"  name="mledgid"  id="mledgid" value="'.$row->acc_led_id.'" required>
				<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Ledger Name :</label>
								<div class="col-md-7">
									<input type="text" class="form-control"  name="mlname"  id="mlname"  value="'. $row->acc_led_description.'"required>
								</div>
							</div>
						</div>
						
									
						<div class="form-group">
						   <div class="row">
							  <label class="col-md-4 control-label">Under :</label>
								<div class="col-md-7">
									<select  class="form-control" name="mlunder"  id="mlunder" required>
									  <option value="">---under----</option>';
									  
										$gquery=$this->db->select("*")->from("acc_groups")->get()->result();
										foreach( $gquery as $g)
										{
											echo "<option value='".$g->acc_grp_id."'"; if($g->acc_grp_id==$row->acc_led_groupid) echo "selected"; echo ">".$g->acc_grp_description."</option>";
										}
									  
									  echo '
														  
									</select>
								</div>
							</div>
						</div>

		  </div>
		  <div class="modal-footer" style="padding-right:50px;">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Save changes</button>
		  </div>
	  </form>';
}

//to edit ledger details and update
	public function update_ledger()
	{
	$mledgid=$this->input->post('mledgid');
	$mlname=$this->input->post('mlname');
	$mlunder=$this->input->post('mlunder');
		
	$data1=array(
			'acc_led_description'=>strtoupper($mlname),
			'acc_led_groupid'=>$mlunder,
			);
			
	$this->load->model("Model_accounts");
	$where="acc_led_id=".$mledgid;
	$this->Model_accounts->update("acc_ledgers",$data1,$where);
	$this->session->set_flashdata('message', '2#Ledger sccessfully updated.'); 
	redirect('Account/ledger');
}

    public function delete_ledger()
	{
		$lid=$this->uri->segment(3);
		$rec=$this->db->select('COUNT(*) as cnt')->from('acc_transactions')->where('acc_tra_ledgerid',$lid)->get()->row();
		if($rec->cnt<=0)
		{
		$where="acc_ledgers.acc_led_id=".$lid;
		$this->db->where($where)->delete("acc_ledgers");
		$this->session->set_flashdata('message', '3#Ledger sccessfully removed.'); 
		redirect('Account/ledger');
		}
		else
		{
		$this->session->set_flashdata('message', '4#Could not remove this ledger. It is link with another data.'); 
		redirect('Account/ledger');
		}
	}

// to save voucher type to table
	public function save_vtype()
	{
	$this->load->model("Model_accounts");
		
	$vname=$this->input->post('vname');
	$vtype=$this->input->post('vtype');
		
	$data1=array(
			'acc_vtypename'=>strtoupper($vname),
			'acc_vtype_no'=>$vtype,
			);
			
	$row=$this->db->select('*')->from('acc_voucher_types')->where('acc_vtypename',strtoupper($vname))->get();
	if($row->num_rows()>0)
	{
	$this->session->set_flashdata('message', '4#Voucher type already exist.'); 
	redirect('Account/voucher');
	}
	else
	{
	$this->Model_accounts->insert("acc_voucher_types",$data1);
	$this->session->set_flashdata('message', '1#Voucher type sccessfully added.'); 
	redirect('Account/voucher');
	}		
	
}
// to update voucher type

	public function edit_vtype()
	{
	$mvtid=$this->input->post('mvtid');
	$mvtype=$this->input->post('mvtype');

	$data1=array
	(
		'acc_vtypename'=>strtoupper($mvtype),
	);
	
	$this->load->model("Model_accounts");
	
	$where="acc_vtypeid=".$mvtid;
	$this->Model_accounts->update("acc_voucher_types",$data1,$where);
	$this->session->set_flashdata('message', '2#Voucher type updated.'); 
	redirect('Account/voucher');
	}

	public function delete_voucher_type()
	{
	$lid=$this->uri->segment(3);
	$where="acc_voucher_types.acc_vtypeid=".$lid;
	$this->db->where($where)->delete("acc_voucher_types");
	$this->session->set_flashdata('message', '3#Voucher type removed.'); 
	redirect('Account/voucher');
	}
	
    public function BSheet()
	{
	   $op=$this->uri->segment(3);
	   $this->load->model("Model_accounts");
	   
	if($op=="" || $op==null)
	{	
		   $bsheetDt['bsdata1']=$this->Model_accounts->balance_sheet_data1("","",$op) ;
		   $bsheetDt['bsdata2']=$this->Model_accounts->balance_sheet_data2("","",$op) ;
		   $bsheetDt['plexp']=$this->Model_accounts->diff_expense("","",$op);
		   $bsheetDt['plinc']=$this->Model_accounts->diff_income("","",$op);
	}
	else if($op==2)
	{
		$sd=$this->input->post('startdate');
		$ed=$this->input->post('enddate');

		$d1=explode("-",$sd);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d2=explode("-",$ed);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
		
		$bsheetDt['bsdata1']=$this->Model_accounts->balance_sheet_data1($dt1,$dt2,$op);
		$bsheetDt['bsdata2']=$this->Model_accounts->balance_sheet_data2($dt1,$dt2,$op);
		$bsheetDt['plexp']=$this->Model_accounts->diff_expense($dt1,$dt2,$op);
		$bsheetDt['plinc']=$this->Model_accounts->diff_income($dt1,$dt2,$op);
	}
		$this->load->view("account/acc_balancesheet",$bsheetDt); 
    }

	
   public function profit_loss()
	{
		$op=$this->uri->segment(3);
		$this->load->model("Model_accounts");
		if($op=="" || $op==null)
		{
			   $prolossDt['plexpense']=$this->Model_accounts->profit_direct_expense("","",$op) ;
			   $prolossDt['plincome']=$this->Model_accounts->profit_direct_income("","",$op) ;
		}
		else if ($op==1)
		{
			$sd=$this->input->post('startdate');
			$ed=$this->input->post('enddate');

			$d1=explode("-",$sd);
			$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
			
			$d2=explode("-",$ed);
			$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			
			$prolossDt['plexpense']=$this->Model_accounts->profit_direct_expense($dt1,$dt2,$op) ;
			$prolossDt['plincome']=$this->Model_accounts->profit_direct_income($dt1,$dt2,$op) ;
		}
			
		$this->load->view("account/acc_profit_loss",$prolossDt); 
	}
		
	public function View_trans()
	{
		$dt1="";
		$dt2="";
		$vt="";
		$op=$this->uri->segment(3);
			if($op==1)
			{
				$sdate=$this->input->post('startdate');
				$edate=$this->input->post('enddate');
			
				$d1=explode("-",$sdate);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$edate);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			}
			else if($op==2)
			{
				$vt=$this->input->post('vtype');
			}
		
		$this->load->model("Model_accounts");
		$tdata['alltrans']=$this->Model_accounts->all_trans_list($dt1,$dt2,$op,$vt);
        $this->load->view("account/acc_alltransactions",$tdata); 
	}
		
	public function ledgerwise_list()
	{
		$ldg="";
		$op=$this->uri->segment(3);
			if($op==1)
			{
				$ldg=$this->input->post('sledger');
			}
		$this->load->model("Model_accounts");
		$ldata['alltrans']=$this->Model_accounts->all_trans_list2($op,$ldg);
        $this->load->view("account/acc_ledgerwise_list",$ldata); 
	}
	
	public function View_cashbook()
	{
		$dt1="";
		$dt2="";
		$vt="";
		$op=$this->uri->segment(3);
			if($op==1)
			{
				$sdate=$this->input->post('startdate');
				$edate=$this->input->post('enddate');
			
				$d1=explode("-",$sdate);
				$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
				
				$d2=explode("-",$edate);
				$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
			}
					
		$this->load->model("Model_accounts");
		$cbdata['cblist']=$this->Model_accounts->cashbook_list($dt1,$dt2,$op);
        $this->load->view("account/acc_cashbook_list",$cbdata); 
	}
	
		
	public function delete_transactions1()  // from all transactions
	{
		$tid=$this->uri->segment(3);
		$this->load->model("Model_accounts");	
		$where="acc_trans_id=".$tid;
		$this->Model_accounts->del("acc_trans_head",$where);
		
		$where="acc_tra_headid=".$tid;
		$this->Model_accounts->del("acc_transactions",$where);
		
		$where="acc_cashbook_id=".$tid;
		$this->Model_accounts->del("acc_cashbook",$where);
		
		$this->session->set_flashdata('message', '3#Voucher sccessfully removed.'); 
		redirect('Account/View_trans');
	}
	
	public function delete_transactions2() // from ledger wise transactions
	{
		$tid=$this->uri->segment(3);
		$this->load->model("Model_accounts");	
		$where="acc_trans_id=".$tid;
		$this->Model_accounts->del("acc_trans_head",$where);
		
		$where="acc_tra_headid=".$tid;
		$this->Model_accounts->del("acc_transactions",$where);
		
		$where="acc_cashbook_id=".$tid;
		$this->Model_accounts->del("acc_cashbook",$where);
		
		$this->session->set_flashdata('message', '3#Voucher sccessfully removed.'); 
		redirect('Account/Ledgerwise_list');
	}

}
?>
