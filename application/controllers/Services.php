    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Services extends CI_Controller 
    {

      function __construct(){
        parent::__construct();
        $user = $this->session->userdata('userid');
        if (!isset($user))
        { 
         redirect('/');
       } 
       else
       { 
         return true;
       }
       $this->load->helper(array('form', 'url'));
     }
    
     
     public function index()
     {
       $this->load->Model('Model_services');
       $data['vehicleid'] = $this->Model_services->show_regno();
       $this->load->view('admin/services',$data);
     }

	 
  public function trip()
  {
     $this->load->view('admin/service_trip_entry');
  }  
 	 
     
     public function add_services()
     {
      $this->load->library('form_validation');
      $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
      $this->form_validation->set_rules('regno', 'regno', 'required');
      $this->form_validation->set_rules('description', 'description', 'required');
      $this->form_validation->set_rules('date', 'date', 'required');
      $this->form_validation->set_rules('invoiceno', 'invoiceno', 'required');
      $this->form_validation->set_rules('invoiceamount', 'invoiceamount', 'required');
    //$this->form_validation->set_rules('status', 'status', 'required');
      if($this->form_validation->run())
      {
        //var_dump($this->input->post());
        
       if(isset($_FILES['image']) && $_FILES['image']['size']!=0){
        
         $config['upload_path'] ='./upload/attachment';
         $config['allowed_types'] = 'gif|jpg|png|jpeg';
            //$image_info = getimagesize($_FILES['userfile']['tmp_name']);
            //$image_width=$image_info[0];
            //$image_height=$image_info[1];
         $this->load->library('upload', $config);
            // echo $image_width;
        if (  !$this->upload->do_upload('image'))
        {
          $error['error'] = array('error' => $this->upload->display_errors());
          $this->load->view("admin/services",$error);
        }
        else
        {
          $date=  date("Y-m-d"); 
          $data = array('upload_data'=> $this->upload->data());
                   //$file_name=$data['upload_data']['file_name'];
          $fname=base_url('/upload/attachment')."/".$data['upload_data']['file_name'];
          $staffid=$this->session->userdata('userid');
          $staffname=$this->session->userdata('name');
          $date=date("Y/m/d");
          date_default_timezone_set('Asia/Kolkata');
          $time=date('h:i:sa'); 
          $data = array(
            'service_vehicleid' => $this->input->post('regno'),
            'service_date'=>$date,
            'service_description' => $this->input->post('description'),
            'service_invoicedate' => $this->input->post('date'),
            'service_invoiceno'=>$this->input->post('invoiceno'),
            'service_invoiceamt'=>$this->input->post('invoiceamount'),
            'service_attachment'=>$fname
            );

          $this->load->Model('Model_services');    
          $this->Model_services->form_insert($data);
          $this->session->set_flashdata('message',  'Service invoice details Added..!');
          
		  $uid = $this->db->insert_id();
		  $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Service trip Added'

            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
          redirect('Services/index');
        }
        
      }
    }
    }


    public function services_ajax()
    {

      $regno= $this->security->xss_clean($this->input->get('regno'));
      $this->load->model('Model_services');
      $results=$this->Model_services->view_services($regno);
      $data1 = array();
      foreach($results  as $r) 
      {
       $date  = $r->service_invoicedate;
       $dt = new DateTime($date);
       $format_date=$dt->format('M-d-Y');
       $date1  = $r->service_date;
       $dt1 = new DateTime($date1);
       $format_date1=$dt1->format('M-d-Y');
       

       $edit="<a href='#myModal2' id='$r->service_id' data-toggle='modal' class='edit open-AddBookDialog2'><center><button class='btn btn-default btn-xs' data-title='Change' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";

       $mm= "<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></button></center>";
       $del=anchor('Services/del_entry/'.$r->service_id, $mm, array('id' =>'del_conf'));
    	//$del=anchor('Services/del_entry/'.$r->service_id, 'Delete', array('id' =>'del_conf'));
    	//$ahr1=anchor('News/del_entry/'.$r->nid, 'Delete', array('id' =>'del_conf')); 
       $img="<img src='$r->service_attachment' class='img-thumbnail' class='img-responsive' alt='Responsive image' style='width:150px; height:100px;' />";   

       array_push($data1, array(

        "edit"=>"$edit",
        "delete"=>"$del",
        "date"=>"$format_date1",
        "regno"=>"$r->vehicle_regno",
        "description"=>"$r->service_description",
        "invoicedate"=>"$format_date",
        "invoiceno"=>"$r->service_invoiceno",
                    "invoiceamt"=>"$r->service_invoiceamt"//,
                   //"files"     =>$img
                    ));
     }
     
     echo json_encode(array('data' => $data1));
    }

		
	
	
    public function view()
    {
     $this->load->model('Model_services');
     $results=$this->Model_services->view();
     $data1 = array();
     foreach($results  as $r) 
     {
       $date  = $r->service_invoicedate;
       $dt = new DateTime($date);
       $format_date=$dt->format('M-d-Y');
       $date1  = $r->service_date;
       $dt1 = new DateTime($date1);
       $format_date1=$dt1->format('M-d-Y');
       

       $edit="<a href='#myModal2' id='$r->service_id' data-toggle='modal' class='edit open-AddBookDialog2'><center><button class='btn btn-default btn-xs' data-title='Change' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";

       $mm= "<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></button></center>";
       $del=anchor('Services/del_entry/'.$r->service_id, $mm, array('id' =>'del_conf'));
    //$del=anchor('Services/del_entry/'.$r->service_id, 'Delete', array('id' =>'del_conf'));
    //$ahr1=anchor('News/del_entry/'.$r->nid, 'Delete', array('id' =>'del_conf')); 
       $img="<img src='$r->service_attachment' class='img-thumbnail' class='img-responsive' alt='Responsive image' style='width:150px; height:100px;' />";   

       array_push($data1, array(

        "edit"=>"$edit",
        "delete"=>"$del",
        "sid"=>"$r->service_id",
        "date"=>$format_date1,
        "regno"=>"$r->vehicle_regno",
        "description"=>"$r->service_description",
        "invoicedate"=>$format_date,
        "invoiceno"=>"$r->service_invoiceno",
                   "invoiceamt"=>"$r->service_invoiceamt"//,
                   //"files"     =>$img
                   ));
     }
     
     echo json_encode(array('data' => $data1));
    }

    public function service_invoicelist()
    {
     $this->load->Model('Model_services');
     $data['regno'] = $this->Model_services->show_regno();
     $this->load->view('admin/view_serviceinvoice',$data);
    }

    public function regno_ajax()
    {
      
     $uaid=$this->uri->segment(3, 0);

     $this->load->model('Model_services');
     
     $results=$this->Model_services->regno_ajax($uaid);
     
     $view="";
     
     $data1 = array();
     foreach($results  as $r) 
     {
      $date  = $r->service_invoicedate;
      $dt = new DateTime($date);
      $format_date=$dt->format('M-d-Y');
      $date1  = $r->service_date;
      $dt1 = new DateTime($date1);
      $format_date1=$dt1->format('M-d-Y');
      

      $edit="<a href='#myModal2' id='$r->service_id' data-toggle='modal' class='edit open-AddBookDialog2'><center><button class='btn btn-default btn-xs' data-title='Change' data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></center></a>";

      $mm= "<center><button class='btn btn-default btn-xs' data-title='Delete' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></button></center>";
      $del=anchor('Services/del_entry/'.$r->service_id, $mm, array('id' =>'del_conf'));
    //$del=anchor('Services/del_entry/'.$r->service_id, 'Delete', array('id' =>'del_conf'));
    //$ahr1=anchor('News/del_entry/'.$r->nid, 'Delete', array('id' =>'del_conf')); 
      $img="<img src='$r->service_attachment' class='img-thumbnail' class='img-responsive' alt='Responsive image' style='width:150px; height:100px;' />";   

      array_push($data1, array(

        "edit"=>"$edit",
        "delete"=>"$del",
        "sid"=>"$r->service_id",
        "date"=>$format_date1,
        "regno"=>"$r->vehicle_regno",
        "description"=>"$r->service_description",
        "invoicedate"=>$format_date,
        "invoiceno"=>"$r->service_invoiceno",
                   "invoiceamt"=>"$r->service_invoiceamt"//,
                   //"files"     =>$img
                   
                   
                   ));
    }


    echo json_encode(array('data' => $data1));
    }



    public function edit_services() {
      
      $data=$this->session->all_userdata();
      $id=$this->input->post('service_id');
              //$id=$this->uri->segment(3);
      $this->load->model('Model_services');
      $results1=$this->Model_services->show_regno();
      
             // $query = $this->db->select('*')->from('table_bus_timing')->where('bus_id',$id)->get();
      $query = $this->db->select('*')->from('vehicle_registration')->join('vehicle_service', 'vehicle_service.service_vehicleid=vehicle_registration.vehicle_id','inner')->where('service_id',$id)->get();
      $row = $query->row();


      echo "   
      <style>
        /* hide number  spinner*/
        input[type=number]::-webkit-inner-spin-button, 
        input[type=number]::-webkit-outer-spin-button { 
          -webkit-appearance: none; 
          margin: 0; 
        }

      </style>          
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal'>&times;</button>
        <h4 class='modal-title' > <font color='blue'>Edit Vehicle Services</font></h4>
      </div>
      <br>

      <form  class='form-horizontal' role='form' method='post' action='". site_url('Services/edit_services1'). "' enctype='multipart/form-data'>
        <!-- text input -->
        
        <div class='form-group'>
          <div class='col-md-9'>  
            <input type='hidden' name='service_id'  value='$row->service_id'/>
          </div>                   
        </div>



        <div class='form-group'>
          <label class='col-md-3 control-label' style='padding-top:10px;'>Vehicle Registration No </label>
          <div class='col-md-9'>                           
            <select class='form-control' name='regno' type='text'>";
              foreach($results1 as $ro){
                echo "<option value='.$ro->vehicle_id.'";
                if($row->service_vehicleid==$ro->vehicle_id)echo "selected";
                echo" >
                $ro->vehicle_regno
              </option>";
            }  
            echo "</select></br>
          </div>
        </div>

        

        <div class='form-group'>
          <label class='col-md-3  control-label'>Service Description : </label>
          <div class='col-md-6'>
           <textarea rows=5 cols=50 class='form-control'  name='description'  required> $row->service_description</textarea>
         </div>
       </div>


       <div class='form-group'>
        <label class='col-md-3 control-label' style='padding-top:10px;'>Invoice Date : </label>
        <div class='col-md-9'>  
         <input type='text' name='date' class='form-control'  id='mask_date2' value='$row->service_invoicedate'/>
       </div>                   
     </div>
     

     <div class='form-group'>
      <label class='col-md-3  control-label'>Invoice NO : </label>
      <div class='col-md-9'>
        <input type='number' class='form-control' placeholder='Enter mobile no' name='invoiceno' value='$row->service_invoiceno' required/>
      </div>
    </div>

    <div class='form-group'>
      <label class='col-md-3  control-label'>Invoice Amount : </label>
      <div class='col-md-9'>
        <input type='number' class='form-control' placeholder='Enter mobile no' name='invoiceamount' value='$row->service_invoiceamt' required/>
      </div>
    </div>


    <div class='form-group' >
     <label class='col-md-3 control-label'>Select File : </label>
     <div class='col-md-9'>
      <div class='fileinput fileinput-new' data-provides='fileinput'>
       
        <span class='btn btn-primary btn-file'>
          <span class='fileinput-new'> Select file </span>
          <span class='fileinput-exists'> Change </span>
          <input type='file' name='image' multiple='true' id='f'> </span>
          <span class='fileinput-filename'> $row->service_attachment</span> &nbsp;
          <a href='javascript:;'' class='close fileinput-exists' data-dismiss='fileinput'> </a>
        </div>
      </div>
      <input type='hidden' name='tmpimage' value='$row->service_attachment'>
    </div>




    </div>



    <div class='form-group'> 
      <div class='col-md-3'>
      </div>
      <div class='col-md-9'>
        <input type='submit' class='btn btn-primary' value='Update'/>
      </div>
    </div>                                
    </form> <div class='modal-footer'>
    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
    </div> ";

    }   


    public function edit_services1()
    {
      $this->load->library('form_validation');
      $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
      $this->form_validation->set_rules('regno', 'regno', 'required');
      $this->form_validation->set_rules('description', 'description', 'required');
      $this->form_validation->set_rules('date', 'date', 'required');
      $this->form_validation->set_rules('invoiceno', 'invoiceno', 'required');
      $this->form_validation->set_rules('invoiceamount', 'invoiceamount', 'required');
      
      if($this->form_validation->run())
      {
        $upload1 = $_FILES['image']['name'];
        $fname=$this->input->post('tmpimage');
        
        if(isset($_FILES['image']) && $_FILES['image']['size']!=0)
        {

          $config['upload_path'] = 'upload/attachment';                        
          $config['allowed_types'] = 'jpg|png|jpeg|gif';
          $this->load->library('upload', $config);
          $this->upload->do_upload('image');
          $upload_data = $this->upload->data();
          $file_name = $upload_data['file_name'];
          $fname=base_url('/upload/attachment')."/".$_FILES['image']['name'];
        }
        
        $this->load->model('Model_services');
        
        $regno=$this->input->post('regno');
        $description=$this->input->post('description');
        $date=$this->input->post('date');
        $invoiceno=$this->input->post('invoiceno');
        $invoiceamount=$this->input->post('invoiceamount');
        $id=$this->input->post('service_id');
        $staffid=$this->session->userdata('userid');
        $staffname=$this->session->userdata('name');
        $date=date("Y/m/d");
        date_default_timezone_set('Asia/Kolkata');
        $time=date('h:i:sa'); 
        
        $data = array(
          
          'service_vehicleid' => $regno,
          'service_description' => $description,
          'service_invoicedate' =>$date,
          'service_invoiceno' => $invoiceno,
          'service_invoiceamt' =>$invoiceamount,
          'service_attachment'=>$fname
          
          );
        
        $this->Model_services->edit_services($id,$data);
        $this->session->set_flashdata('message',  'Service details Updated..!');
        $userdt1 = array(
          'log_staffid' => $staffid,
          'log_staffname'=>$staffname,
          'log_time'=>$time,
          'log_date'=>$date,
          'log_entryid'=>$id,
          'log_desc'=>'Service Invoice Details Updated'

          );
        $this->load->model('Model_customer');
        $this->Model_customer->form_insertlog($userdt1);
        redirect('Services/index');

      }

      else 

      {
       redirect('Services/service_invoicelist');

     }                   
    }

    public function del_entry()
    {
      
      $id=$this->uri->segment(3, 0);
      $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
      $date=date("Y/m/d");
      date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa'); 
      $this->load->model('Model_services');
      $result=$this->Model_services->del_entry($id);
      if($result=1)
      {
        $this->session->set_flashdata('message', ' Deleted Successfully');
        $userdt1 = array(
          'log_staffid' => $staffid,
          'log_staffname'=>$staffname,
          'log_time'=>$time,
          'log_date'=>$date,
          'log_entryid'=>$id,
          'log_desc'=>'Service Invoice Details Deleted'

          );
        $this->load->model('Model_customer');
        $this->Model_customer->form_insertlog($userdt1);
        redirect('Services/index');
      }
      $this->session->set_flashdata('message', 'Please try again');
      redirect('Services/service_invoicelist');
    }


    public function share_servicehistory()
    {
     $data=$this->session->all_userdata();
     $id=$this->input->post('id');
     echo " <style> .tx{text-align:right;} .fnt{text-align:right;font-size:18px;}</style>            
     <div class='modal-header'>
      <button type='button' class='close' data-dismiss='modal'>&times;</button>
      <h4 class='modal-title' > <font color='blue'>Share</font></h4>
    </div>
    <br>

    <form  class='form-horizontal' role='form' method='post' action='". site_url('Pdf/share_servicehistory'). "' enctype='multipart/form-data'>

      <div class='row'>
        <div class='col-md-12'>
         
         <div class='form-group'>
          <label class='col-md-2 control-label' style='padding-top:10px;'>Email to </label>
          <div class='col-md-8'>
            <input type='hidden' name='id'  value='$id'/>
            <input type='email' class='form-control' name='email' value='' placeholder='Enter email'> 
          </div>
        </div>
      </div>
    </div> 
    <div class='modal-footer'>
      <input type='submit' class='btn btn-primary' value='Share'/>
      <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
    </div>     

    </form>   
    ";


    }

    public function save_servicehistory()
    {


      $this->load->model('Model_pdf');
      $this->load->library('pdf1');
      
      
      $pdf=$this->pdf1->load();
      $id=$this->input->post('uid');
      $this->load->model('Model_pdf');
      $data['value']=$this->Model_pdf->service_invoice($id);
      $data['results']=$this->Model_pdf->service_invoice1($id);
      ini_set('memory_limit','256M');
      $html=  $this->load->view('pdf/service_invoice',$data,true);
      $pdf->WriteHtml($html);
      $output='vehicleid_'.$id.'_'.date('Y_m_d_H_i_s').'_.pdf';
    // $pdf->Output("$output",'D');
      $pdf->Output('upload/savedpdf/'.$output,'F');
      $this->session->set_flashdata('message',  'Service Details Saved Successfully..!');
      redirect('Services/service_invoicelist');
      exit();
      
    }


	
	// SERVICE TRIP ------------------------------------------------
	
	public function add_service_trip()
     {
      $this->load->library('form_validation');

      $this->form_validation->set_rules('vehicleid', 'vehicle id', 'required');
      $this->form_validation->set_rules('spsdate', 'start date', 'required');
      $this->form_validation->set_rules('spstime', 'start time', 'required');
	  $this->form_validation->set_rules('vfrom', 'vehicle from', 'required');
	  $this->form_validation->set_rules('srcentreid', 'service centre', 'required');
	  $this->form_validation->set_rules('rservice', 'required services', 'required');
	  $this->form_validation->set_rules('startkm', 'startkm', 'required');
    
      if($this->form_validation->run())
      {

          $staffid=$this->session->userdata('userid');
          $staffname=$this->session->userdata('name');
          $date=date("Y/m/d");
          date_default_timezone_set('Asia/Kolkata');
          $time=date('h:i:sa'); 
	  		  
			  $vehid=$this->input->post('vehicleid');
			  
          $data = array(
            'srtrip_vehicleid' => $vehid,
            'srtrip_sdate'=>$this->input->post('spsdate'),
            //'srtrip_edate'=>$this->input->post('spedate'),
            'srtrip_stime'=>$this->input->post('spstime'),
            //'srtrip_etime'=>$this->input->post('spetime'),
			'srtrip_driverid'=>$this->input->post('driverid'),
            'srtrip_vfrom'=>$this->input->post('vfrom'),
			'srtrip_centre'=>$this->input->post('srcentreid'),
			'srtrip_rservice'=>$this->input->post('rservice'),
            'srtrip_startkm'=>$this->input->post('startkm'),
			'srtrip_endkm'=>"0",
			'srtrip_status'=>"1",
            );

		
        $this->load->Model('Model_services');    
          $this->Model_services->strip_insert($data);
		  
		  $dat1 = array(
            'vehicle_available' =>"1",
            );
		  $this->Model_services->update_service_vehicle($vehid,$dat1);  
		  
		  		  
          $uid = $this->db->insert_id();
          $userdt1 = array(
            'log_staffid' => $staffid,
            'log_staffname'=>$staffname,
            'log_time'=>$time,
            'log_date'=>$date,
            'log_entryid'=>$uid,
            'log_desc'=>'Service trip inserted',
            );
          $this->load->model('Model_customer');
          $this->Model_customer->form_insertlog($userdt1);
		  
		  $this->session->set_flashdata('message',  'Service Trip added..!');
          redirect('Services/trip');
        }
		else
		{
			
			$this->session->set_flashdata('message1',  'Service trip details missing..!');
			 redirect('Services/trip');
		}
        
      }

	public function service_trip_ajax()
    {

      $this->load->model('Model_services');
      $results=$this->Model_services->view_service_trips();
      $data1 = array();
      foreach($results  as $r) 
      {
       /*$date  = $r->service_invoicedate;
       $dt = new DateTime($date);
       $spsdate=$dt->format('M-d-Y');
       $date1  = $r->service_date;
       $dt1 = new DateTime($date1);
       $format_date1=$dt1->format('M-d-Y');*/
       
       $edit="<a href='#myModal2' id='$r->srtrip_id' data-toggle='modal' class='edit'><button class='btn btn-default btn-xs'  data-toggle='modal' ><span class='glyphicon glyphicon-pencil'></span></button></a>";
	   $close="<a href='#' id='$r->srtrip_id' data-toggle='modal'  class='stclose'><button class='btn btn-warning btn-xs'  data-toggle='modal' >Close</button></a>";
       $mm= "<button style='margin-left:3px;' class='btn btn-default btn-xs'><span class='glyphicon glyphicon-trash'></span></button>";
       $del=anchor('Services/del_entry1/'.$r->srtrip_id, $mm, array('id' =>'del_conf'));
    
	$stc="None";
	if($r->srtrip_status=='1')
	{
		$st='<font color=red>Servicing</font>';
		$stc=$close.$del;
	}
	else if($r->srtrip_status=='2')
	{
		$st='<font color=green>Complete</font>';
		$stc=$del;
	}
	
	if($r->srtrip_endkm!='0'){
	$km=($r->srtrip_endkm-$r->srtrip_startkm);
	$km1=$r->srtrip_startkm." - ". $r->srtrip_endkm."($km)";
	}
	else
	{
		$km1=$r->srtrip_startkm." - (0)";
	}
	
       array_push($data1, array(
        "edit"=>$del,
		"stclose"=>$stc,
        "id"=>"$r->srtrip_id",
        "vehicle"=>"$r->vehicle_regno",
		"driver"=>"$r->driver_name",
		"sdate"=>"$r->srtrip_sdate",
		"stime"=>"$r->srtrip_stime",
		"vfrom"=>"$r->srtrip_vfrom",
		"scentre"=>"$r->srcentre_name",
		"rservice"=>"$r->srtrip_rservice",
		"startkm"=>$km1,
		"status"=>"$st",
	
        ));
     }
     
     echo json_encode(array('data' => $data1));
    }

	
	 public function del_entry1()
    {
      
      $id=$this->uri->segment(3, 0);
      $staffid=$this->session->userdata('userid');
      $staffname=$this->session->userdata('name');
	  
      $date=date("Y/m/d");
      date_default_timezone_set('Asia/Kolkata');
      $time=date('h:i:sa'); 
	  
	  $vid=$this->db->select('srtrip_vehicleid')->from('service_trips')->where('srtrip_id',$id)->get()->row();
	  $vid1=$vid->srtrip_vehicleid;
		
	  $this->load->model('Model_services');
      $result=$this->Model_services->del_entry_strip($id);
	  
	  $dat1=array("vehicle_available"=>"0");
	  $this->Model_services->update_service_vehicle($vid1,$dat1);
	  	  
      if($result=1)
      {
        $this->session->set_flashdata('message', ' Deleted Successfully');
        $userdt1 = array(
          'log_staffid' => $staffid,
          'log_staffname'=>$staffname,
          'log_time'=>$time,
          'log_date'=>$date,
          'log_entryid'=>$id,
          'log_desc'=>'Service Invoice Details Deleted'

          );
        $this->load->model('Model_customer');
        $this->Model_customer->form_insertlog($userdt1);
        redirect('Services/trip');
      }
	  else
	  {
      $this->session->set_flashdata('message', 'Please try again');
      redirect('Services/trip');
	  }
    }
	
	
	//SERVICE CENTRE --------------------------------------------------
	
	public function add_service_centre()
	{
		$scna=$this->input->post('srcentre');
		$scadd=$this->input->post('sraddress');
		$scmob=$this->input->post('srmobile');
		
		$dat1=array("srcentre_name"=>strtoupper($scna),
		"srcentre_address"=>strtoupper($scadd),
		"srcentre_phone"=>$scmob,
		);
		
		$this->load->model('Model_services');
		$result=$this->Model_services->insert_service_centre($dat1);
		
		redirect("Services/trip");
	}
	
	//--------------------------------------------------------------
	
	public function trip_list()
	{
		$this->load->view("admin/service_trip_list");
	}
	
	public function close_service_entry()
	{
		$stid=$this->input->post('vehid');
		$sta=$this->input->post('srstatus');
		$skm=$this->input->post('currkm');
		$sdt=$this->input->post('srdate');
		
		$d=explode("-",$sdt);
		$edt=$d[2]."-".$d[1]."-".$d[0];
				
		$vid=$this->db->select('*')->from('service_trips')->where('srtrip_id',$stid)->get()->row();
	    $vid1=$vid->srtrip_vehicleid;
		$pskm=$vid->srtrip_startkm;
		
		if($pskm>$skm)
		{
		$this->session->set_flashdata('message1', 'Currect KM is invalid. Start Km is($pskm)');
		redirect("Services/trip_list");
		}
		else
		{
		$dat1=array("srtrip_status"=>"2",
		"srtrip_edate" => $edt,
		"srtrip_endkm" => $skm,
		);
		$dat2=array("vehicle_available"=>"0",);
				
		$this->load->model('Model_services');
		$this->Model_services->update_service_vehicle1($stid,$vid1,$dat1,$dat2);
		$this->session->set_flashdata('message', 'Service trip closed..');
		redirect("Services/trip_list");
		}
		
	}
	
	//Service evaluation ------------------------------------
	
	public function Evaluation()
	{
		$this->load->view('admin/service_evaluation');
	}
	
	public function add_ser_evaluation()
	{
		$d= $this->input->post('date');
		$d1=explode("-",$d);
		$md=$d1[2]."-".$d1[1]."-".$d1[0];
				
    	$sedata = array(
      		'se_service_type' => $this->input->post('service_type'),
      		'se_guest_name' => $this->input->post('guest_name'),
      		'se_contact_no' => $this->input->post('contact_no'),
      		'se_date' => $md,
      		'se_vehicle_no' => $this->input->post('vehicle_no'),
      		'se_chauffeur_name' => $this->input->post('chauffeur_name'),
      		'se_feedback' => $this->input->post('feedback'),
			'se_actiontaken' => $this->input->post('actiontaken'),
			'se_status' => "1",
    	);

    	$this->db->insert('service_evaluation',$sedata);
    	if($this->db->affected_rows() > 0)
    	{
        	$this->session->set_flashdata('message', '1#Service evaluation details added..');
			redirect("Services/Evaluation");
    	}
    	else
    	{
      		$this->session->set_flashdata('message', '#Details missing.');
			redirect("Services/Evaluation");  
    	}
	}

	public function Evaluation_list()
	{
		$op="";
		$md="";
		$this->load->model('Model_services');
		$sedata['sehead']=date('F');
		$sedata['eva_result'] = $this->Model_services->service_evo_list($op,$md);
		$this->load->view("admin/view_evaluation_list",$sedata);
	}
	
	public function view_evaluation()
	{
		$op=$this->uri->segment(3);
		$this->session->set_userdata('se_opt',$op);
		
		if($op==1)
		{
		$d=$this->input->post('sdate');
		$d1=explode("-",$d);
		$md=$d1[2]."-".$d1[1]."-".$d1[0];
		$sedata['sehead']=$md;
		}
		else
		{
		$md=$this->input->post('smonth');
		$time="2018-".$md."-01";
		$sedata['sehead']=date('F',strtotime($time))." - ".date('Y');
		}
		//for report
		$this->session->set_userdata('se_op',$op);
		$this->session->set_userdata('se_opt',$md);
		$this->session->set_userdata('se_head',$sedata['sehead']);
		//-----------
		$this->load->model('Model_services');
		$sedata['eva_result'] = $this->Model_services->service_evo_list($op,$md);
		
		$this->load->view("admin/view_evaluation_list",$sedata);
	}
	
	
	public function edit_evaluation()
	{
    	$id = $this->input->get('id');
		$this->load->model('Model_services');
    	$data['seva'] = $this->Model_services->service_evo_list1($id);
		//var_dump($data['service']);
    	$this->load->view('admin/edit_evaluation',$data);
  	}
	
	public function update_evaluation()
	{
  
      $sedata = array(
      		'se_service_type' => $this->input->post('service_type'),
      		'se_guest_name' => $this->input->post('guest_name'),
      		'se_contact_no' => $this->input->post('contact_no'),
      		'se_date' => $this->input->post('date'),
      		'se_vehicle_no' => $this->input->post('vehicle_no'),
      		'se_chauffeur_name' => $this->input->post('chauffeur_name'),
      		'se_feedback' => $this->input->post('feedback'),
			'se_actiontaken' => $this->input->post('actiontaken'),
      );
      $id = $this->input->post('edit_service_id');
      $this->db->where('se_id',$id)->update('service_evaluation',$sedata);
      if($this->db->affected_rows() > 0)
      {
        $this->session->set_flashdata('message', '2#Details successfully updated.');
			redirect("Services/Evaluation_list");
      }
      else
      {
       $this->session->set_flashdata('message', '4#Details missing.');
			redirect("Services/Evaluation_list");
     }
    }
	
	
	 public function delete_evaluation()
	 {
    	$id = $this->uri->segment(3);
    	$this->db->where('se_id', $id)->delete('service_evaluation');
    	if($this->db->affected_rows() > 0)
    	{
        	$this->session->set_flashdata('message', '3#Service evaluation removed..');
			redirect("Services/Evaluation_list");
    	}
    	else
    	{
      	    $this->session->set_flashdata('message', '4#Details missing.');
			redirect("Services/Evaluation_list");
    	}
     }
	
	/// service part request entry
	
	
	public function service_parts()
	{
		$this->load->view("admin/service_part_request");
	}
	
	public function request_list()
	{
		$op=$this->uri->segment(3);
		if($op=="1")
		{
		$sdate=$this->input->post('sdate');
		$edate=$this->input->post('edate');
	
		$d1=explode("-",$sdate);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d1=explode("-",$edate);
		$dt2=$d1[2]."-".$d1[1]."-".$d1[0];
		$srdata['dcap']="Requested List of : [ ".$sdate."&nbsp;&nbsp;=>&nbsp;&nbsp;".$edate." ]";
		}
		else
		{
			$dt1="";$dt2="";$op=="";
			$srdata['dcap']="Requested List of : [ ".date('F')." - ".date('Y')." ]";
		}
		
		$this->load->model('Model_services');
    	$srdata['srresult'] = $this->Model_services->view_service_request($op,$dt1,$dt2);
		
		$this->load->view("admin/service_request_list",$srdata);
	}
	
	
	public function add_service_request()
	{
		$d=$this->input->post('edate');
		$d1=explode("-",$d);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$d=$this->input->post('date_time');
		$d1=explode("-",$d);
		$dt2=$d1[2]."-".$d1[1]."-".$d1[0];
		
    	$data = array(
      		'srequest_date' => $dt1,
      		'srequest_regno' => $this->input->post('reg_no'),
      		'srequest_vtype' => $this->input->post('vehicle_type'),
      		'srequest_category' => $this->input->post('service_category'),
      		'srequest_datetime' => $dt2,
      		'srequest_duration' => $this->input->post('duration'),
      		'srequest_workshop' => $this->input->post('centerworkshop'),
    	);
		
    	$this->db->insert('service_request',$data);
        $id = $this->db->insert_id(); 
      // Unescape the string values in the JSON array
        $tableData = stripcslashes($_POST['pTableData']);

        // Decode the JSON array
        $tableData = json_decode($tableData,TRUE);

       for($i=0;$i<count($tableData);$i++)
       {
            $data = array('srequest_id'=>$id,'parts'=>$tableData[$i]['Parts'],'price'=>$tableData[$i]['Price']);
            $this->db->insert('service_requirements',$data);
       }
       echo "success";
	}
		
	public function edit_request()
	{
  		$this->load->model('Model_services');
    	$id = $this->input->get('id');
    	$data['result'] = $this->Model_services->get_service_request($id);
        $data['veh_types'] = $this->db->get('vehicle_types')->result();
        $data['requirements'] = $this->Model_services->get_service_requirements($id);
    	$this->load->view('admin/edit_parts_request',$data);
  
	}
	
	public function update_service_request()
	{
    
  	 $id = $this->input->post('service_id');
     // Unescape the string values in the JSON array
      $tableData = stripcslashes($this->input->post('pTableData'));
      // Decode the JSON array
      $tableData = json_decode($tableData,TRUE);

      for($i=0;$i<count($tableData);$i++)
      {
          $data = array('srequest_id'=>$id,'parts'=>$tableData[$i]['Parts'],'price'=>$tableData[$i]['Price']);
          $this->db->where('pk_int_id',$tableData[$i]['No'])->update('service_requirements',$data);
      }
	  
	    $edate=$this->input->post('edate');
		$d1=explode("-",$edate);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		$dtime=$this->input->post('date_time');
		$d2=explode("-",$dtime);
		$dt2=$d2[2]."-".$d2[1]."-".$d2[0];
	  	  
     $data = array(
          'srequest_date' => $dt1,
          'srequest_regno' => $this->input->post('reg_no'),
          'srequest_vtype' => $this->input->post('vehicle_type'),
          'srequest_category' => $this->input->post('service_category'),
          'srequest_datetime' => $dt2,
          'srequest_duration' => $this->input->post('duration'),
          'srequest_workshop' => $this->input->post('centerworkshop'),
      );
      
      $this->db->where('srequest_id',$id)->update('service_request',$data);
     
      if($this->db->affected_rows() > 0)
      {
        echo "success";
      }
      else
      {
       echo "fail";  
      }
	  

  }

    public function delete_service_request()
	{
  
    	$id = $this->input->get('id');
    	$this->db->where('srequest_id', $id)->delete('service_request');
      $this->db->where('srequest_id', $id)->delete('service_requirements');
    	if($this->db->affected_rows() > 0)
    	{
        	echo "success";
    	}
    	else
    	{
      	echo "fail";  
    	}
  }
	
	
	
   }