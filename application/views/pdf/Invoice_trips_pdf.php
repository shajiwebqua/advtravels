<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
<title>Travels</title>

 <style type="text/css">
 td{padding:2px 5px 3px 5px; }
 table{font-size:14px;}

	.table-main{
		  margin-top:15px;
          border-collapse: collapse;
		  font-size:15px;
     }

    .table-main td{
    border: 1px solid black;
     }
	 .table-main th{
       padding-top:3px;
	   padding-bottom:3px;
     }
	
	.table-inner td{
		border: none !important;
	}
 </style>
  
</head>

<body>
<!-- heading ----------------------------------------------------->
<br>
<table style="border:none;width:100%;margin-top:20px;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
<td width="150px" style="padding: 0px" align='right'>
<?php $date = date('d/m/Y');?>
Date: <?=$date;?>
  </td>
</tr>

<tr>
<td width="150px" style="padding: 0px">
<Strong>Advance World Holidays</strong><br>
PK & Sons Complex, East Moozhikkal<br>
NH 212, Calicut, Kerala, India-673571<br>
Tel: 0091 495 2732 500, 0091 495 3100 700<br>
Fax: 0091 495 2732 400
</td>
<td width="150px" style="padding: 0px" align='right'>

</td>
</tr>
</table>
<!-- heading end----------------------------------------------------->
<hr>

<div width='1024px'>
<table width='100%'><tr><td ><h3>Trip details for Invoice No: INV-<?php echo $invoiceno;?></h3></td></tr></table>

<table width='100%'  class='table-main' cellpadding=0 cellspacing=0 border=1 style='border:none !important;'>

<tr>
<th>Trip_id</th>
<th>Date</th>
<th>Customer</th>
<th>Invoice_no</th>
<th>Amount</th>
<th>Gst%</th>
<th>Gst</th>
<th>Total</th>
</tr>

<?php
$gtot=0;

$cust=$this->db->select('customer_name,customer_address')->from('customers')->where('customer_id',$custid)->get()->row();

foreach($invotrips as $tr)
{
	$tot=$tr->up_amount+$tr->up_gst;
	$gtot+=$tot;
?>
<tr>
<td><?=$tr->up_tripid;?></td>
<td><?=date_format(date_create($tr->up_date),'d-m-Y');?></td>
<td width='35%'><?=$cust->customer_name;?><br><?=nl2br($cust->customer_address);?></td>
<td>INV-<?=$tr->up_invoiceno;?></td>
<td align='right'><?=$tr->up_amount;?></td>
<td align='right'><?=$tr->up_gstper;?></td>
<td align='right'><?=$tr->up_gst;?></td>
<td align='right'><b><?=number_format($tot,'2','.','');?></b></td>
</tr>
<?php
}
?>
</table>
<table width='100%'>
<tr><td width='75%' align='right'><h2>Grand Total:</h2></td><td width='25%' align='right'><h2><?=number_format($gtot,'2','.','');?></h2></td> </tr>
<tr><td width='75%' align='right'>&nbsp;</td><td width='25%' align='right'>===============</h2></td> </tr>
</table>
	
</div>
</body>
</html>