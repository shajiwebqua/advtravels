<?php
include('application/views/common/header.php');
?>
<style>
/* hide number  spinner*/
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
p
{
padding-top:0px;
padding-bottom:0px;	
maring:0px;
}
.tbl tr
{
	height:30px;
}

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1><b>Service Parts Request List</b></h1>
		<ol class="breadcrumb" style='font-size:15px;'>
			<li><a href="<?php echo base_url('');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Service Request List</button></a></li>
			
	    </ol> 
         <label style="background-color:#cecece;width:100%;height:1px;"></label>   
        </section>
  
	<section class="content"> 
			<div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
			</div>
    <!-- Small boxes (Stat box) -->
<div class="row">
<div class="col-md-12">

<div style="background-color:#fff;border-radius:2px;padding:15px; ">
    

 <div class="portlet-body form">
 <div class='row' style='margin-bottom:15px;'>
 <div class='col-md-12'>
 <label style='font-size:16px;font-weight:bold;'></label>
 </div>
 </div>
 
 <table style="border:none;width:100%;">
		<tr>
			<td colspan=2 style="padding: 0px">
				<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" height='35px'>
			</td>
		</tr>
		<tr>
			<td style="padding:0px">
				<span style="font-size:15px;">Advance World Holidays</span><br>
				<span>PK & Sons Complex, East Moozhikkal</span><br>
				<span>NH 212, Calicut, Kerala, India - 673571</span><br>
				<span>Tel : 0091 495 2732 500, 0091 495 3100 700</span><br>
				<span>Fax: 0091 495 2732 400</span>
			</td>
			<td width='150px' valign='top' align='right'>Date: <?php echo date('d-m-Y');?></td>
		</tr>
	</table>
	<hr style='margin-bottom:2px;'>
	 <table width="100%"><tr><td width='220px;'><h4>Vehicle Service Request Report</h4></td></tr>
 </table>
 <hr style='margin-top:2px;'>
 

 <table width="100%" class='tbl'>
       
        <tbody>
        <?php $i=1;
		foreach($srresult1 as $row){
			$d=$row->srequest_date;
			$d1=explode("-",$d);
			$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
			
			$d=$row->srequest_datetime;
			$d1=explode("-",$d);
			$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
			
			?>
            <tr ><td width='25%'>Request ID</td><td>:&nbsp;&nbsp;<?=$row->srequest_id;?></td></tr>
			<tr ><td>Request Date</td><td>:&nbsp;&nbsp;<?=$dt1;?></td></tr>
			<tr ><td>Vehicle</td><td>:&nbsp;&nbsp;<?=$row->vehicle_regno?> (<?=$row->veh_type_name?>)</td></tr>
			<tr ><td>Servie </td><td>:&nbsp;&nbsp;<?php if($row->srequest_category==1)echo "Toyota Servicing"; else echo "General";?></td></tr>
			<tr ><td>Expected Date</td><td>:&nbsp;&nbsp;<?=$dt1?></td></tr>
			<tr><td>Expected Duration</td><td>:&nbsp;&nbsp;<?= $row->srequest_duration?></td></tr>
			<tr><td>Service Centre</td><td>:&nbsp;&nbsp;<?= $row->srequest_workshop?></td></tr>
			
			<tr ><td colspan="2"  style='padding:15px 5px;'><b>Service Parts details</b></td></tr>
			<tr><td colspan="2">
			
				<table width='70%' border=1>
				<tr><th width='10%' style='padding:3px 3px 3px 5px;'>Slno</th>
				<th style='padding:3px 3px 3px 5px;'>Particulers</th>
				<th width='20%' style='padding:3px 3px 3px 5px;'>Price (Approximate)</th></tr>
				<?php $res = $this->db->where('srequest_id',$row->srequest_id)->get('service_requirements')->result();
				$slno=1;
				$tot=0;
				 foreach($res as $row1)
				{
					
                  echo "<tr ><td style='padding:3px 3px 3px 5px;'>".$slno."</td>";
				  echo"<td style='padding:3px 3px 3px 5px;'>".$row1->parts."</td>";
				  echo "<td style='padding:3px 5px 3px 5px;text-align:right;'> &#8377;&nbsp;".number_format($row1->price,"2",".","")."</td></tr>";
				  $slno++;
				  $tot+=$row1->price;
                }
				?>
				<tr height='50px'><td colspan="3" style='padding:3px 5px 3px 5px;text-align:right;font-size:18px;font-weight:600;'>Total :&nbsp;&nbsp;&nbsp;&#8377;&nbsp;<?=number_format($tot,"2",".","");?></td>
				</table>
			
			</td></tr>
				
			</table>			
         <?php $i++;} ?>   
		 
			<table width='70%'>
				<tr height="60px;"><td style='padding:3px 3px 3px 5px;'> Approved By :</td></tr>
				<tr height="50px;"><td style='padding:3px 15px 3px 5px;text-align:right;'> For Advanced Travels</td></tr>
				<tr height="50px;"><td style='padding:3px 15px 3px 5px;text-align:right;'> (Signature)</td></tr>
			</table>
        </tbody>
    </table> 
 
 </div> <!--portlet-body-->
 
 </div>
</div>    
<br>  
  </div>
  </div>
</section>

<?php include('application/views/common/footer.php');?>
  </div>

<script type="text/javascript">
$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }

 $('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});

$('#datepicker2').datepicker({
    format: 'dd-mm-yyyy',
    //startDate: '-3d'
	autoclose:true,
});

  
 var table=$('#example').DataTable({
	 "ordering":false,
 });
  $('#example tbody').on( 'click', 'tr', function () {
         if ( $(this).hasClass('selected') ) {
             $(this).removeClass('selected');
         }
         else {
 	        table.$('tr.selected').removeClass('selected');
             $(this).addClass('selected');
         }
   });			   
	
function delete_service_request(id)
{
  swal({
        title: "Are you sure?",
        text: "You will not be able to recover",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel pls!",
        closeOnConfirm: true,
        closeOnCancel: true 
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
        type: "post",
        url: "<?php echo base_url();?>Vehicle_service/delete_service_request?id="+id,
        success: function(data){ 

        if(data="success")
        {
          swal("Success", "Service request deleted successfully", "success");
          window.location.reload();
        }
        else
        {
          swal("Error", "Something went wrong", "error");    
        }  
        }
        });
      }
      else
      {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    }
    );
}
	
$("#view-pdf").click(function()
{
	  var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(id=="")
		 {
			 alert ("Please select service request entry.");
		 }
		 else
		 {
		 var pdf_link = $(this).attr('href');
         var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
            $.createModal({
				title:'Trip Details',
				message: iframe,
				closeButton:true,
				scrollable: true,
			});
			return false; 
		 }
});	
 
/*$("#btnexp1").click(function()
{
	var etype1=$("#exptype1").val();
	var egroup=$("#expgroup").val();
	jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Vehicle/add_exp_type",
        dataType: 'html',
        data: {exptype:etype1,expgroup:egroup},
        success: function(res) {
		$("#etype").append("<option value='"+res+"'>"+etype1+"</option>")
                    }
            });
});
*/

</script>


  </body>
</html>
