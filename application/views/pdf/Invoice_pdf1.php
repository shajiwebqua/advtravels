<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Travels</title>

  <style type="text/css">
	  td{padding:2px 5px 3px 5px; }
     .tb2{ border:none;}
	.tr1{height:25px;}
	.tr3{height:40px;}
	.tr2{height:200px;}
	.add{padding:15px;}
	
	.row1 td{border:1px solid #999;height:25px;}
     table{font-size:14px;}
	 
	 .row2{height:35px;}
	 
		 
		 
	.table-inner{
		border: none !important;
	} 
	 
   </style>
  
  </head>
<body>
<table style="border:none;width:100%;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
<td width="150px" style="padding: 0px" align='right'>
<?php $date = date('Y/m/d');?>
Date: <?=$date;?>
  </td>
</tr>
</table>
<hr>


<div style='width:1024px;'>
<table width='100%'><tr><td align='center'><h2>Tax Invoice</h2></td></tr></table>

<table border=1 width='100%' cellpadding=0 cellspacing=0 style='border:none;'>
<tr height='25px'>
<td style='width:40%;border:1px solid #000;' rowspan=3 valign='top'>
		<address>
            <strong>Advance World Holidays</strong><br>
            PK & Sons Complex, East Moozhikkal<br>
            NH 212, Calicut, Kerala, India-673571<br>
			Tel: 0091 495 2732 500, 0091 495 3100 700<br>
            Fax: 0091 495 2732 400
          </address>

</td>
	<td > <table class='table-inner' width='100%'><tr><td>Invoice No.</td></tr><tr><td style='font-weight:600;'> INV-000102</td></tr></table> </td>
	<td > <table class='table-inner' width='100%'><tr><td>Dated.</td></tr><tr><td style='font-weight:600;'>26-JUN-2018</td></tr></table>
	</td>
</tr>


<tr>
    <td style='width:30%;border:1px solid #000;'> <table width='100%'><tr><td>Delivery Note.</td></tr><tr><td style='font-weight:600;'>&nbsp;</td></tr></table> </td>
	<td style='width:30%;border:1px solid #000;'> <table width='100%'><tr><td>Mode/Terms of Payment</td></tr><tr><td style='font-weight:600;'>15 days</td></tr></table>
	</td>
</tr>

<tr>
    <td style='width:30%;border:1px solid #000;'> <table width='100%'><tr><td>Supplier's Ref.</td></tr><tr><td style='font-weight:600;'>Car rental 01-15 February 18</td></tr></table> </td>
	<td style='width:30%;border:1px solid #000;'> <table width='100%'><tr><td>Dated.</td></tr><tr><td style='font-weight:600;'>26-6-2018</td></tr></table>
	</td>
</tr>

<tr><td style='width:40%;border:1px solid #000;' rowspan=4 valign='top' >
<span>Buyer</span><br>
		<address>
            <strong>Advance World Holidays</strong><br>
            PK & Sons Complex, East Moozhikkal<br>
            NH 212, Calicut, Kerala, India-673571<br>
			Tel: 0091 495 2732 500, 0091 495 3100 700<br>
            Fax: 0091 495 2732 400
          </address>


</td>
<td style='width:30%;border:1px solid #000;'> <table width='100%'><tr><td>Supplier's Ref.</td></tr><tr><td style='font-weight:600;'>&nbsp;</td></tr></table> </td>
	<td style='width:30%;border:1px solid #000;'> <table width='100%'><tr><td>Other Reference(s)</td></tr><tr><td style='font-weight:600;'>&nbsp;</td></tr></table>
	</td>
</tr>
	
<tr>
    <td style='width:30%;border:1px solid #000;'><table width='100%'><tr><td>Despatch Document No.</td></tr><tr><td style='font-weight:600;'>&nbsp;</td></tr></table> </td>
	<td style='width:30%;border:1px solid #000;'> <table width='100%'><tr><td>Delivery Note Date</td></tr><tr><td style='font-weight:600;'>&nbsp;</td></tr></table>
	</td>
</tr>

<tr><td style='width:30%;border:1px solid #000;'><table width='100%'><tr><td>Despatched through</td></tr><tr><td style='font-weight:600;'>&nbsp;</td></tr></table> </td>
	<td style='width:30%;border:1px solid #000;'> <table width='100%'><tr><td>Destination</td></tr><tr><td style='font-weight:600;'>&nbsp;</td></tr></table>
	</td>
</tr>

<tr><td style='width:30%;border:1px solid #000;height:120px;' colspan=2  valign='top' >Terms of Delivery</td></tr>
</table>

<table  border=1 width='100%' cellpadding=0 cellspacing=0 >
	<tr class='tr1' style='font-weight:600;'>
		<td width='5%'>&nbsp;</td>
		<td width='45%'>&nbsp;</td>
		<td width='12%'>HSN/HAC</td>
		<td width='10%'>Quantity</td>
		<td width='10%'>Rate</td>
		<td width='6%'>Per</td>
		<td width='12%'>Amount</td>
	</tr>
	
	<tr valign='top'>
		<td width='5%' style='height:200px;' valign='top'>&nbsp;</td>
		<td width='45%' valign='top'>&nbsp;</td>
		<td width='12%' valign='top'>HSN/HAC</td>
		<td width='10%' valign='top'>Quantity</td>
		<td width='10%' valign='top'>Rate</td>
		<td width='6%' valign='top'>Per</td>
		<td width='12%' valign='top'>Amount</td>
	</tr>
	
	<tr class='tr1'>
		<td width='5%'>&nbsp;</td>
		<td width='45%'>&nbsp;</td>
		<td width='12%'>&nbsp;</td>
		<td width='10%'>&nbsp;</td>
		<td width='10%'>&nbsp;</td>
		<td width='6%'>&nbsp;</td>
		<td width='12%'>&nbsp;</td>
	</tr>
	
	<tr valign='top'>
		<td colspan=7>
		<table width='100%'>
			<tr><td>Amount chargable (in words): </td><td align='right'>E & OE</td></tr>
		<tr><td style='font-weight:600;padding-left:50px;'>ewqe we wqe qwe qwe wqe qwe qweqwe</td></tr></table>
		
		</td>
	</tr>

</table>


<table  border=1 width='100%' cellpadding=0 cellspacing=0 >
	<tr class='tr1' style='font-weight:600;'>
		<td width='25%' rowspan=2 valign='top' align='center' >HSN/SAC</td>
		<td width='20%' rowspan=2 valign='top' align='center'>Taxable<br> Value</td>
		<td width='20%' align='center' colspan=2>Central Tax</td>
		<td width='20%' align='center' colspan=2>State Tax</td>
		<td width='15%' rowspan=2 valign='middle' align='center'>Total <br>Tax Amount</td>
	</tr>
	<tr class='tr1' style='font-weight:600;'>
		<td >Rate</td>
		<td >Amount</td>
		<td >Rate</td>
		<td >Amount</td>
	</tr>
	<tr class='tr3'>
		<td >&nbsp;</td>
		<td align='right'>&nbsp;</td>
		<td align='right'>&nbsp;</td>
		<td align='right'>&nbsp;</td>
		<td align='right'>10<br>2.59</td>
		<td align='right'>&nbsp;</td>
		<td align='right'>&nbsp;</td>
	</tr>
	
	<tr class='tr1'>
		<td >&nbsp;</td>
		<td align='right'>&nbsp;</td>
		<td align='right'>&nbsp;</td>
		<td align='right'>&nbsp;</td>
		<td align='right'>&nbsp;</td>
		<td align='right'>&nbsp;</td>
		<td align='right'>&nbsp;</td>
	</tr>

<tr>
<td colspan=7>
<table class='tb2 ' width='100%' cellpadding=0 cellspacing=0 >
	<tr style='border:none;'><td>Taxable Amount (in words): </td></tr>
	<tr style='border:none;'><td style='padding-left:50px;'>ewqe we wqe qwe qwe wqe qwe qweqwe</td></tr>
</table>
</td>
</tr>
</table>
	
<table border=1 width='100%' cellpadding=0 cellspacing=0 >
	<tr height='80px'>
		<td width='65%' valign='top'>Declaration:<br>
		We declare that this invoice shows the actual price of the goods described and  that all particulars  are true and correct.
		</td>
		<td  valign='top' align='right'>
		<b>For Advance World</b><br><br><br>
		Authorized Signatory
		
		</td>
	</tr>
	
</table>	
<table width='100%'><tr><td align='center'><p>This is a computer generated invoice</p></td></tr></table>
</div>

</html>