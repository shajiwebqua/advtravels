<?php
 include('application/views/common/header.php');?>
<style>
.nav-tabs {
    margin: 0;
    padding: 0;
    border: 0;    
}
.nav-tabs > li > a {
    background: #f2f2f2;
    border-radius: 0;
    //box-shadow: inset 0 -8px 7px -9px rgba(0,0,0,.4),-2px -2px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li.active > a:hover {
    background: #fff;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}

.nav-tabs > li > a:hover {
    background: #e4e4e4;
	
   // box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
}
/* Tab Content */
.tab-pane {
    background: #fff;
    //box-shadow: 0 0 4px rgba(0,0,0,.4);
    border-radius: 0;
    border:1px solid #e4e4e4;
    text-align: left;
    padding: 10px;
}

.cli
{
padding:5px 0px 5px 0px;
margin-top:3px;

}

.selclass > div:hover
{
background-color:#f4f4f4;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Main content -->
 <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <b>Salary Payments</b> </h1>
	 <ol class="breadcrumb" style='font-size:15px;'>
			<!--<li><a href="<?php echo base_url('StaffSalary/view_salary');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-list" aria-hidden="true"></i> Payment List</button></a></li> -->
	    </ol> 
  </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>

<!-- Content Header end -->   
     
  <section class="content"> 
    <!-- Small boxes (Stat box) -->
  <div class="row">
  <div class="col-md-12">

  <!-- Add new user details -->
              
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
   <div class="page-content">

         <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
<?php
		$dtab=$this->session->userdata('saltab');
?>
	 <div style='padding-left:3px;'> 
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item <?php if($dtab=='1' or $dtab=="") echo 'active'; ?>" id='tb1' >
						<a class="nav-link tp1" data-toggle="tab" href="#sadd"  role="tab"><i class="fa fa-user"></i> Salary Payment</a>
					</li>
					<li class="nav-item <?php if($dtab=='2') echo 'active'; ?>" id='tb2'>
						<a class="nav-link tp1 " data-toggle="tab" href="#alist"  role="tab"> <i class="fa fa-cab"></i> Payment List</a>
					</li>
					
                 </ul>
                <!-- Tab panes -->
        <div class="tab-content">
		<div class="tab-pane <?php if($dtab=='1') echo 'active'; ?>" id="sadd" role="tabpanel"> 
	    <div style="background-color:#fff; padding:0px 10px; ">

		<div class='row' style='margin-top:0px;'>
		<div class='col-md-7'>
		<h5 style='font-size:16px;'>Enter Details </h5>
		<hr style='margin:0px 0px 10px 0px;'>
		</div>
		
		<div class='col-md-5'>
		<h5 style='font-size:16px;'> Salary Advance Details</h5>
		<hr style='margin:0px 0px 10px 0px; '>
		</div>
		</div>
		
		<div class='row'>
		<div class='col-md-7' style='border-right:1px solid #c2c2c2;'>
 
     <form onsubmit='return check_payment();' class="form-horizontal" method="POST" action="<?php echo base_url('StaffSalary/add_salary')?>" enctype="multipart/form-data">
			<div class="form-group">
			<label class="col-md-4" style="padding-top:5px;" align="right">Staff Name : </label>
			<div class="col-md-8">
			<div id="regno1">
				<select name="staffna" id="staffna" class="form-control" required>
				 <option value=''>------------</option>
				 <?php
				  $staff=$this->db->select('*')->from('staffregistration')->get()->result();	
					foreach($staff as $st)
					{
					?>
						<option value='<?=$st->staff_id;?>'> <?=$st->staff_name;?> </option>
					<?php 
					} 
					?>
				</select>
            </div>
				
			</div> 
			</div>
		<div class="form-group">
		<label class="col-md-4" style="padding-top:5px;" align="right">Date : </label>
        <div class="col-md-4">
		<input type='date' name="edate" class='form-control' value="<?php echo date('Y-m-d');?>" required>
		</div> 
		</div> 
		
		<div class="form-group">
		<label class="col-md-4" style="padding-top:5px;" align="right">Voucher No : </label>
        <div class="col-md-4">
		<input type='number' name="voucherno" id='vno' class='form-control'  required>
		</div> 
		</div> 
		
		<div class="form-group">
		<label class="col-md-4" style="padding-top:5px;" align="right">Voucher Date : </label>
        <div class="col-md-4">
		<input type='date' name="vdate" id='vdate' class='form-control' value="<?php echo date('Y-m-d');?>" required>
		</div> 
		</div> 
		
		<div class="form-group">
		<label class="col-md-4" style="padding-top:5px;" align="right">Advance Salary : </label>
        <div class="col-md-4">
		<input type='text' style='background-color:#fbfbfb;' name="advance" id='advance' class='form-control'  readonly>
		</div> 
		</div> 
	
		
		<div class="form-group">
		<label class="col-md-4" style="padding-top:5px; " align="right">Balance : </label>
        <div class="col-md-4">
		<input type='text' style='background-color:#fbfbfb;' name="balance" id='balance' class='form-control'  readonly>
		</div> 
		</div> 
				
		<div class="form-group">
		<label class="col-md-4" style="padding-top:5px;" align="right">Amount : </label>
		<div class="col-md-4">
		<input type='text' name="amount" id='amount' class='form-control'  required>
		</div> 
		</div> 
		
	   <div class="form-group">
		<label class="col-md-4" style="padding-top:5px;" align="right">Narration : </label>
        <div class="col-md-8">
		<textarea rows=4 name="narra" class='form-control'  required>Nil</textarea>
		</div> 
		</div> 
	
	
		
		<!---  attache your proof ----------------------------------->
					
					<div class='form-group' style='margin-top:20px;'> 			
					
					  <div class="row">
					  <label class='col-md-4 control-label' style='padding-top:3px;'>Upload Voucher Copy: </label>
					  
					  <div class="control-group col-md-8" id="fields" style='padding-left:20px;'>
						  <div class="controls">
						   	  <div class="entry input-group col-xs-3" style='margin-top:5px;'>
								<input class="btn btn-primary" name="files[]" type="file">
								<span class="input-group-btn">
							  <button class="btn btn-success btn-add" type="button">
												<span class="glyphicon glyphicon-plus"></span>
								</button>
								</span>
							  </div>
						 
						</div>
					  </div>
					</div>
					</div>
		
		<!--------------------------------------------------------------------->
		
				<label style="background-color:#cecece;width:100%;height:1px;margin:0px padding:0px;"></label> 
					<div class='form-group'> 
					<label class='col-md-4 control-label' style='padding-top:3px;'></label>
						<div class='col-md-7'>
							<input type='submit' class='btn btn-primary' id='bsave'  value='Save Details' style='padding:7px 20px 7px 20px;' />
							</center>
						</div>
					</div> 
</form>
</div>
	<div class='col-md-5'>
	<style>
	td,th
	{
		padding-left:10px;
		padding-right:10px;
	}
	.tda
	{
		text-align:right;
	}
	</style>
	<div class='row'>
	<label class="col-md-4" style="padding-top:5px;" align="right">Salary : </label>
			<div class="col-md-6">
			<input type='text' style='background-color:#fbfbfb;font-size:16px;' name="bsalary"  id='bsalary' class='form-control'  readonly >
			</div> 
	</div>
	<div class='row' style='margin:5px 15px 0px 10px'>
	<label style='font-weight:700;'>Salary Advance</label>
	</div>
	<div class='row' style='margin:0px 15px 5px 10px;'>
	<table border=1 width='100%'>
	<tr><th>Date</th> <th>Voucher_No</th><th class='tda'>Amount</th></tr>
	<tbody class='stbody'>

	</tbody>
	<tr><td colspan='2' class='tda'>Total Advance</td><td class='tda' id='advamt' style='font-size:16px;font-weight:600;'>0.00</td></tr>
	</table>
	</div>
	</div>

</div>

	</div>
	</div>  <!---- first tab panel ---------->

					
	<div class="tab-pane <?php if($dtab=='2') echo 'active'; ?>" id="alist" role="tabpanel"> 
		<div style="background-color:#fff;padding:3px 10px 3px 10px;">
		
		
		  <div class='row' style='margin:0px;background-color:#e4e4e4;padding:5px;'>

			<form role="form" method="POST" action="<?php echo base_url('StaffSalary/Salary/1')?>" enctype="multipart/form-data">
		
			<div class='col-md-2 ' style='padding-top:5px;'>	<label class='control-label'>Select Date [from-to] : </label>	</div>
			<div class='col-md-2 ' style='padding-top:2px;'><input type='text' class='form-control' name='startdate' id='datepicker1' placeholder="Start date" value="" required >	</div>
			<div class='col-md-2 ' style='padding-top:2px;'><input type='text' class='form-control' name='enddate' id='datepicker2' placeholder="End date" value="" required>	</div>
			<div class='col-md-1 ' style='padding-top:2px;'>
			<input type="submit" class="form-control btn btn-success" style="text-align:center;"  value="Get" name='btnget' id="btnget"> 
			</div>
			</form>
			
			<form role="form" method="POST" action="<?php echo base_url('StaffSalary/Salary/2')?>" enctype="multipart/form-data">
			<div class='col-md-2 col-md-offset-1'  style="margin-top:2px;border-left:2px solid red;">
				<select class="form-control" id="smonth" name="smonth">
					<option value="">-----------</option>
					<option value="1">JANUARY</option>
					<option value="2">FEBRUARY</option>
					<option value="3">MARCH</option>
					<option value="4">APRIL</option>
					<option value="5">MAY</option>
					<option value="6">JUNE</option>
					<option value="7">JULY</option>
					<option value="8">AUGUST</option>
					<option value="9">SEPTEMBER</option>
					<option value="10">OCTOBER</option>
					<option value="11">NOVEMBER</option>
					<option value="12">DECEMBER</option>
					
				</select>
				</div>
				<div class='col-md-1'  style="margin-top:2px;padding:0px;">
					<select class="form-control" id="syear" name="syear">
						<option value="">-----------</option>
						<?php
						for($x=date('Y');$x>=2017;$x--)
						{
						echo "<option value='".$x."'"; if($x==date('Y')) echo " selected"; echo ">".$x."</option>";
						}
						?>
						
					</select>
				</div>
			<div class="col-md-1" style="margin-top:2px;">
			<input type="submit" class="form-control btn btn-success" style="text-align:center;"  value="Get" name='btnget' id="btnmonth"> </div>
				</div>
		</form>
		
			<div class='row'>
			  <div class='col-md-11'><h4>Salary Payment Details</h4> </div>
            </div>
			
			 <div class='row'>
			<hr style='margin:0px 0px 15px 0px;'>
			</div>
			
				<div class='row' style='padding-left:10px; padding-right:10px;' >
			<table class="table table-striped table-hover table-bordered" id="example" width='100%' style='font-size:14px;'>
							<thead>
								<tr style="color:#5068f8;">
								 <th>Del</th>
								 <th>Date</th>
								 <th>Staff</th>
								 <th>Voucher_No</th>
								  <th>Voucher_date</th>
								 <th>Advance</th>
								 <th>Balance</th>
								 <th>Paid_Amount</th>
								 <th>Narration</th>
								 </tr>
							</thead>
							<tbody>
							<?php
							
							if(isset($vsalary))
							{
								$gtotal=0;
							foreach($vsalary as $r1)
							{
							?>
							<tr>
								 <td width='50px' align='center'><a href="<?php echo base_url('StaffSalary/del_salary/'.$r1->salary_id);?>" id='del_conf'><button class='btn btn-danger btn-xs'><i class="fa fa-trash-o" aria-hidden="true"></i></button></a></td>
								 <td><?=$r1->salary_date;?></td>
								 <td><?=$r1->staff_name;?></td>
								 <td width='10%'><?=$r1->salary_voucherno;?></td>
								 <td width='10%'><?=$r1->salary_voucherdate;?></td>
								 <td align='right' width='80px'><?=number_format($r1->salary_advance,"2",".","");?></td>
								 <td align='right' width='80px'><?=number_format($r1->salary_balance,"2",".","");?></td>
								 <td align='right' width='80px'><?=number_format($r1->salary_paidamount,"2",".","");?></td>
								 <td><?=$r1->salary_narration;?></td>
								 </tr>
							<?php 
							$gtotal+=$r1->salary_paidamount;
							} 
							}?>
							</tbody>
							<thead>
								<tr style="color:#5068f8;font-size:18px;">
								 	 <th colspan='8' style='text-align:right;padding-right:5px;'><span style='font-size:14px;'>Total Paid Amount</span> : &nbsp;&nbsp;&#8377;&nbsp;&nbsp <b> <?=number_format($gtotal,"2",".","");?></b></th>
								 <th></th>
								 </tr> 
							</thead>
							
                        </table>
				</div>
                            <!-- END BORDERED TABLE PORTLET-->
		</div>						

       </div><!-- tab pane end --->
	   
		</div><!-- tab content -->
		
		</div>
</div>
  </div>
</div>
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<!-- modal windows --->

 <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >

              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
         </div>             
		 
<!--- End -------------------------->

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">
 
$("#idmsg").hide();
if($("#msg").html()!="")
  {
	  var msg=$("#msg").html();
	var mg=msg.split('#');
    if(mg[0]==1)
		swal("Success",mg[1],"")
	if(mg[0]==2)
		swal("Updated",mg[1],"")
	if(mg[0]==3)
		swal("Deleted",mg[1],"")
	if(mg[0]==4)
		swal("Try Again",mg[1],"")
    $("#msg").html("");
  }
	
$("#example").DataTable();
	
	
$('#datepicker1').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   });
 
 $('#datepicker2').datepicker({
	format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
	endDate:'now'
   }); 
   	
function check_payment()
{
	var bal=parseInt($("#balance").val());
	var pamt=parseInt($("#amount").val());
	if(pamt>bal)
	{
		alert("Please check paid amount.");
		$("#amount").focus();
		return false;
	}
	else
	{
		return true;
	}
}

$("#staffna").change(function()
{
	var sid=$("#staffna").val();
		
	jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "StaffSalary/get_details",
        dataType: 'html',
        data: {staffid:sid},
        success: function(res) {
	
		var dt=res.split("#");
		$(".stbody").html(dt[0]);
		$("#bsalary").val(dt[1]);
		$("#advamt").html(dt[2]);
		$("#advance").val(dt[2]);
		$("#balance").val(dt[3]);
		$("#vno").focus();
            }
        });

});

 $(document).on("click", "#del_conf", function () 
	{
        return confirm('Are you sure you want to delete this entry?');
    });


</script>



