<?php
class Pdf extends CI_Controller {
public function Pdf() {
parent::__construct();

$this->load->model('Model_pdf');//load the model

}

public function pdf_trip()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id = $this->uri->segment(3);
    $this->load->model('Model_pdf');

    $data['results']=$this->Model_pdf->trip($id);

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
//echo realpath($path);   
$html=  $this->load->view('pdf/trip_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}


public function pdf_trip1()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id=$this->input->post('tid');
    $this->load->model('Model_pdf');

    $data['results']=$this->Model_pdf->trip($id);

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/trip_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function pdf_UPCtrip()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id = $this->uri->segment(3);
    $this->load->model('Model_pdf');

    $data['results']=$this->Model_pdf->UPCtrip();
ini_set('memory_limit','256M');
$path  = base_url("upload/pdf/");  
echo realpath($path);   
$html=  $this->load->view('pdf/UPCtrip_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}



public function share_utrip()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('email', 'email', 'required');

$this->load->model('Model_pdf');

$this->load->library('pdf1');
 
 if($this->form_validation->run())
  {  

     $email=$this->input->post('email');
      $no=explode(',',$email);
        foreach ($no as  $values) {
            echo $values;
   
        $pdf=$this->pdf1->load();
         $id=$this->input->post('id');
        //$this->load->model('buku_model4');
       $data['results']=$this->Model_pdf->UPCtrip();
        
        
        ini_set('memory_limit','256M');


$html=  $this->load->view('pdf/UPCtrip_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_').'_.pdf';
// $pdf->Output("$output",'D');
$pdf->Output('upload/pdf/'.$output,'F');


/*Send mail to the registered user with their username nd password*/
        $this->load->library('my_phpmailer');
   
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "webquaforum@gmail.com";  // user email address
        $mail->Password   = "webquaforum@14";            // password in GMail
        $mail->SetFrom('webquaforum@gmail.com', 'Adv Travels');  //Who is sending the email
        $mail->AddReplyTo("webquaforum@gmail.com","Adv Travels");  //email address that receives the response
        $mail->Subject    = "Trip Details";

        $mail->IsHTML(true);
        $mail->Body = "Trip detail send as an attachement"

        ;

        // $mail->msgHTML($body, dirname(__FILE__), true);
        // $mail->AltBody    = "Username is".$user." <br/> Password is".$pass;
        $destino = $values; // Who is addressed the email to
        $mail->AddAddress($destino,'Trip');

        $mail->AddAttachment("upload/pdf/".$output);      // some attached files
        // $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want
        if(!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }
    }
         // header('location: http://localhost/advtravels/admin/Trip/view_trip');
    $this->session->set_flashdata('message',  'Trip Details Shared Successfully..!');
           redirect('Trip/upcoming_trip');
    exit();
        
        }
}


public function share_rtrip()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('email', 'email', 'required');

$this->load->model('Model_trip');

$this->load->library('pdf1');
 
 if($this->form_validation->run())
  {  

     $email=$this->input->post('email');
      $no=explode(',',$email);
        foreach ($no as  $values) {
            echo $values;
   
        $pdf=$this->pdf1->load();
         $id=$this->input->post('id');
        //$this->load->model('buku_model4');
    $data['results']=$this->Model_trip->view_trip();
        
        
        ini_set('memory_limit','256M');


$html=  $this->load->view('pdf/rtrip_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_').'_.pdf';
// $pdf->Output("$output",'D');
$pdf->Output('upload/pdf/'.$output,'F');


/*Send mail to the registered user with their username nd password*/
        $this->load->library('my_phpmailer');
   
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "webquaforum@gmail.com";  // user email address
        $mail->Password   = "webquaforum@14";            // password in GMail
        $mail->SetFrom('webquaforum@gmail.com', 'Adv Travels');  //Who is sending the email
        $mail->AddReplyTo("webquaforum@gmail.com","Adv Travels");  //email address that receives the response
        $mail->Subject    = "Trip Details";

        $mail->IsHTML(true);
        $mail->Body = "Trip detail send as an attachement"

        ;

        // $mail->msgHTML($body, dirname(__FILE__), true);
        // $mail->AltBody    = "Username is".$user." <br/> Password is".$pass;
        $destino = $values; // Who is addressed the email to
        $mail->AddAddress($destino,'Trip');

        $mail->AddAttachment("upload/pdf/".$output);      // some attached files
        // $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want
        if(!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }
    }
         // header('location: http://localhost/advtravels/admin/Trip/view_trip');
    $this->session->set_flashdata('message',  'Trip Details Shared Successfully..!');
           redirect('Trip/view_trip');
    exit();
        
        }
}



public function pdf_rtrip()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id = $this->uri->segment(3);
    $this->load->model('Model_trip');

    $data['results']=$this->Model_trip->view_trip();

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/rtrip_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}


public function pdf_vehicle()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id = $this->uri->segment(3);
    $this->load->model('Model_vehicle');

    $data['results']=$this->Model_vehicle->view_vehicle();

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/vehicle_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}


public function pdf_driver()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    //$id = $this->uri->segment(3);
    $this->load->model('Model_driver');

    $data['results']=$this->Model_driver->view_driver();

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/driver_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}


public function pdf_attach()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    //$id = $this->uri->segment(3);
    $this->load->model('Model_attach');

    $data['results']=$this->Model_attach->view_vehicles();

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/attach_pdf',$data,true);

$pdf->WriteHtml($html);
$output='attached_vehicles'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}


public function pdf_log()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $m = $this->uri->segment(3);
     $dt = $this->uri->segment(4);
    $this->load->model('Model_log');

    // $data['results']=$this->Model_log->view_log1();
    if($dt!="----" && $m!="0") {
         $d2=explode("-",$dt);
        $dte=$d2[2]."/".$d2[1]."/".$d2[0];
    $data['results']=$this->Model_log->view_log4($dte,$m); 
    }  
    else if($m!=0) {
$data['results']=$this->Model_log->view_log1($m);
}
else if($m=='0' && $dt=='----')
{
$data['results']=$this->Model_log->view_log2();    
}
else if($dt!='----')
{
        $d2=explode("-",$dt);
        $dte=$d2[2]."/".$d2[1]."/".$d2[0];
$data['results']=$this->Model_log->view_log3($dte);
}


ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/log_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report_log'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}



public function pdf_loan()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id = $this->uri->segment(3);
    $this->load->model('Model_loan');

    $data['results']=$this->Model_loan->view_loan();

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/loan_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report_loan'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}



function service_invoice()
{
   $this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id = $this->uri->segment(3);
    $this->load->model('Model_pdf');

    $data['value']=$this->Model_pdf->service_invoice($id);
    $data['results']=$this->Model_pdf->service_invoice1($id);

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/service_invoice',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function customer()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id = $this->uri->segment(3);
    $this->load->model('Model_customer');
    $data['results1']=$this->Model_customer->view_customer1($id);
     //$data['re']=$id;   
 // $ids=$this->model_staff->projectdet($sess);
        
 //$i=array(
                // 'id'=>$ids->user_id,
                // 'name'=>$ids->name,
                // );
        
 //$data['resul']=$this->buku_model4->projectdet1($sess);
ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/admin/upload/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/customer_pdf',$data,true);

$pdf->WriteHtml($html);
$output='customerid_'.$id.'_'.date('Y_m_d').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}





function staff()
{

   $id = $this->uri->segment(3);
   if($id=="")
   {
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id = $this->uri->segment(3);
    $this->load->model('Model_staff');
    $data['results']=$this->Model_staff->view_stafflistPDF();
     //$data['re']=$id;   
 // $ids=$this->model_staff->projectdet($sess);
        
 //$i=array(
                // 'id'=>$ids->user_id,
                // 'name'=>$ids->name,
                // );
        
 //$data['resul']=$this->buku_model4->projectdet1($sess);
ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/admin/upload/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/stafflist_pdf',$data,true);

$pdf->WriteHtml($html);
$output='staffid_'.$id.'_'.date('Y_m_d').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
   }
   else
   {   


$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id = $this->uri->segment(3);
    $this->load->model('Model_staff');
    $data['results']=$this->Model_staff->view_staffPDF($id);
     //$data['re']=$id;   
 // $ids=$this->model_staff->projectdet($sess);
        
 //$i=array(
                // 'id'=>$ids->user_id,
                // 'name'=>$ids->name,
                // );
        
 //$data['resul']=$this->buku_model4->projectdet1($sess);
ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/admin/upload/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/staff_pdf',$data,true);

$pdf->WriteHtml($html);
$output='staffid_'.$id.'_'.date('Y_m_d').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
} }




function user()
{


$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    //$id = $this->uri->segment(3);
    //$this->load->model('buku_model4');
    $data['results']=$this->Model_pdf->user();
        
 // $ids=$this->model_staff->projectdet($sess);
        
 //$i=array(
                // 'id'=>$ids->user_id,
                // 'name'=>$ids->name,
                // );
        
 //$data['resul']=$this->buku_model4->projectdet1($sess);
ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/user_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H_i_s').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function share_customer()
{

$this->load->library('form_validation');
$this->form_validation->set_rules('email', 'email', 'required');

$this->load->model('Model_trip');

$this->load->library('pdf1');
 
 if($this->form_validation->run())
  {  

     $email=$this->input->post('email');
      $no=explode(',',$email);
        foreach ($no as  $values) {
            echo $values;
   
        $pdf=$this->pdf1->load();
         $id=$this->input->post('id');
    $this->load->model('Model_customer');
        $data['results1']=$this->Model_customer->view_customer1($id);
        
        
        ini_set('memory_limit','256M');


$html=  $this->load->view('pdf/customer_pdf',$data,true);

$pdf->WriteHtml($html);
$output='customerid_'.$id.'_'.date('Y_m_d').'_.pdf';
// $pdf->Output("$output",'D');
$pdf->Output('upload/pdf/'.$output,'F');


/*Send mail to the registered user with their username nd password*/
        $this->load->library('my_phpmailer');
   
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "tls";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 587;                   // SMTP port to connect to GMail
        $mail->Username   = "webquaforum@gmail.com";  // user email address
        $mail->Password   = "webquaforum@14";            // password in GMail
        $mail->SetFrom('webquaforum@gmail.com', 'Adv Travels');  //Who is sending the email
        $mail->AddReplyTo("webquaforum@gmail.com","Adv Travels");  //email address that receives the response
        $mail->Subject    = "Customer Details";

        $mail->IsHTML(true);
        $mail->Body = "Customer detail send as an attachement"

        ;

        // $mail->msgHTML($body, dirname(__FILE__), true);
        // $mail->AltBody    = "Username is".$user." <br/> Password is".$pass;
        $destino = $values; // Who is addressed the email to
        $mail->AddAddress($destino,'Customer');

        $mail->AddAttachment("upload/pdf/".$output);      // some attached files
        // $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want
        if(!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }
    }
         // header('location: http://localhost/advtravels/admin/Trip/view_trip');
    $this->session->set_flashdata('message',  'Customer Details Shared Successfully..!');
           redirect('Customer/index');
    exit();
        
        }
    }

public function share_trip()
{
$this->load->library('form_validation');
$this->form_validation->set_rules('email', 'email', 'required');

$this->load->model('Model_pdf');

$this->load->library('pdf1');
 
 if($this->form_validation->run())
  {  

     $email=$this->input->post('email');
      $no=explode(',',$email);
        foreach ($no as  $values) {
            echo $values;
   
        $pdf=$this->pdf1->load();
         $id=$this->input->post('id');
        //$this->load->model('buku_model4');
        $data['results']=$this->Model_pdf->trip($id);
        
        
        ini_set('memory_limit','256M');


$html=  $this->load->view('pdf/trip_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_').'_.pdf';
// $pdf->Output("$output",'D');
$pdf->Output('upload/pdf/'.$output,'F');


/*Send mail to the registered user with their username nd password*/
        $this->load->library('my_phpmailer');
   
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "webquaforum@gmail.com";  // user email address
        $mail->Password   = "webquaforum@14";            // password in GMail
        $mail->SetFrom('webquaforum@gmail.com', 'Adv Travels');  //Who is sending the email
        $mail->AddReplyTo("webquaforum@gmail.com","Adv Travels");  //email address that receives the response
        $mail->Subject    = "Trip Details";

        $mail->IsHTML(true);
        $mail->Body = "Trip detail send as an attachement"

        ;

        // $mail->msgHTML($body, dirname(__FILE__), true);
        // $mail->AltBody    = "Username is".$user." <br/> Password is".$pass;
        $destino = $values; // Who is addressed the email to
        $mail->AddAddress($destino,'Trip');

        $mail->AddAttachment("upload/pdf/".$output);      // some attached files
        // $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want
        if(!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }
    }
         // header('location: http://localhost/advtravels/admin/Trip/view_trip');
    $this->session->set_flashdata('message',  'Trip Details Shared Successfully..!</font>');
           redirect('Trip/completed_trip');
    exit();
        
        }
}

public function share_staff()
{

$this->load->library('form_validation');
$this->form_validation->set_rules('email', 'email', 'required');

//$this->load->model('Model_trip');

$this->load->library('pdf1');
 
 if($this->form_validation->run())
  {  

     $email=$this->input->post('email');
      $no=explode(',',$email);
        foreach ($no as  $values) {
            echo $values;
   
        $pdf=$this->pdf1->load();
         //$id=$this->input->post('id');
    $this->load->model('Model_staff');
         $data['results']=$this->Model_staff->view_stafflistPDF();
        
        
        ini_set('memory_limit','256M');


$html=  $this->load->view('pdf/stafflist_pdf',$data,true);

$pdf->WriteHtml($html);
$output='staffid_'.$id.'_'.date('Y_m_d').'_.pdf';
// $pdf->Output("$output",'D');
$pdf->Output('upload/pdf/'.$output,'F');


/*Send mail to the registered user with their username nd password*/
        $this->load->library('my_phpmailer');
   
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "tls";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 587;                   // SMTP port to connect to GMail
        $mail->Username   = "webquaforum@gmail.com";  // user email address
        $mail->Password   = "webquaforum@14";            // password in GMail
        $mail->SetFrom('webquaforum@gmail.com', 'Adv Travels');  //Who is sending the email
        $mail->AddReplyTo("webquaforum@gmail.com","Adv Travels");  //email address that receives the response
        $mail->Subject    = "Staff Details";

        $mail->IsHTML(true);
        $mail->Body = "Staff detail send as an attachement"

        ;

        // $mail->msgHTML($body, dirname(__FILE__), true);
        // $mail->AltBody    = "Username is".$user." <br/> Password is".$pass;
        $destino = $values; // Who is addressed the email to
        $mail->AddAddress($destino,'Customer');

        $mail->AddAttachment("upload/pdf/".$output);      // some attached files
        // $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want
        if(!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }
    }
         // header('location: http://localhost/advtravels/admin/Trip/view_trip');
    $this->session->set_flashdata('message',  '<font color="green">Staff Details Shared Successfully..!</font>');
           redirect('Staff/view_staff');
    exit();
        
        }
    }




    public function share_servicehistory()
{

$this->load->library('form_validation');
$this->form_validation->set_rules('email', 'email', 'required');

$this->load->model('Model_trip');

$this->load->library('pdf1');
 
 if($this->form_validation->run())
  {  

     $email=$this->input->post('email');
      $no=explode(',',$email);
        foreach ($no as  $values) {
            echo $values;
   
        $pdf=$this->pdf1->load();
         $id=$this->input->post('id');
    $this->load->model('Model_pdf');
       $data['value']=$this->Model_pdf->service_invoice($id);
    $data['results']=$this->Model_pdf->service_invoice1($id);
        
        
        ini_set('memory_limit','256M');


$html=  $this->load->view('pdf/service_invoice',$data,true);

$pdf->WriteHtml($html);
$output='vehicleid_'.$id.'_'.date('Y_m_d').'_.pdf';
// $pdf->Output("$output",'D');
$pdf->Output('upload/pdf/'.$output,'F');


/*Send mail to the registered user with their username nd password*/
        $this->load->library('my_phpmailer');
   
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "tls";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 587;                   // SMTP port to connect to GMail
        $mail->Username   = "webquaforum@gmail.com";  // user email address
        $mail->Password   = "webquaforum@14";            // password in GMail
        $mail->SetFrom('webquaforum@gmail.com', 'Adv Travels');  //Who is sending the email
        $mail->AddReplyTo("webquaforum@gmail.com","Adv Travels");  //email address that receives the response
        $mail->Subject    = "Service Details";

        $mail->IsHTML(true);
        $mail->Body = "Service detail send as an attachement"

        ;

        // $mail->msgHTML($body, dirname(__FILE__), true);
        // $mail->AltBody    = "Username is".$user." <br/> Password is".$pass;
        $destino = $values; // Who is addressed the email to
        $mail->AddAddress($destino,'Customer');

        $mail->AddAttachment("upload/pdf/".$output);      // some attached files
        // $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want
        if(!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }
    }
         // header('location: http://localhost/advtravels/admin/Trip/view_trip');
    $this->session->set_flashdata('message',  'Service Details Shared Successfully..!');
           redirect('Services/service_invoicelist');
    exit();
        
        }
    }

    public function enquery(){
        $this->load->library('pdf1');
        $pdf=$this->pdf1->load();
        $slno = $this->uri->segment(3);
        $this->load->model('Model_order');
        $data['results']=$this->Model_order->view_enqueryPDF();
        ini_set('memory_limit','256M');
        
        $html=  $this->load->view('pdf/enquery_pdf',$data, TRUE);
        $pdf->WriteHtml($html);
        $output='enquiry_'.$id.'_'.date('Y_m_d').'_.pdf';
        $pdf->Output("$output",'I');

    }



    public function share_log()
{

$this->load->library('form_validation');
$this->form_validation->set_rules('email', 'email', 'required');

$this->load->model('Model_log');

$this->load->library('pdf1');
 
 if($this->form_validation->run())
  {  

     $email=$this->input->post('email');
      $no=explode(',',$email);
        foreach ($no as  $values) {
            echo $values;
   
        $pdf=$this->pdf1->load();
$dt=$this->input->post('dt1');
$m=$this->input->post('m');
    $this->load->model('Model_log');
        //$data['results']=$this->Model_log->view_log1();
         if($dt!="----" && $m!="0") {
         $d2=explode("-",$dt);
        $dte=$d2[2]."/".$d2[1]."/".$d2[0];
    $data['results']=$this->Model_log->view_log4($dte,$m); 
    }  
    else if($m!=0) {
$data['results']=$this->Model_log->view_log1($m);
}
else if($m=='0' && $dt=='----')
{
$data['results']=$this->Model_log->view_log2();    
}
else if($dt!='----')
{
        $d2=explode("-",$dt);
        $dte=$d2[2]."/".$d2[1]."/".$d2[0];
$data['results']=$this->Model_log->view_log3($dte);
}
        
        
        ini_set('memory_limit','256M');


$html=  $this->load->view('pdf/log_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report_log_'.date('Y_m_d').'_.pdf';
// $pdf->Output("$output",'D');
$pdf->Output('upload/pdf/'.$output,'F');


/*Send mail to the registered user with their username nd password*/
        $this->load->library('my_phpmailer');
   
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "tls";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 587;                   // SMTP port to connect to GMail
        $mail->Username   = "webquaforum@gmail.com";  // user email address
        $mail->Password   = "webquaforum@14";            // password in GMail
        $mail->SetFrom('webquaforum@gmail.com', 'Adv Travels');  //Who is sending the email
        $mail->AddReplyTo("webquaforum@gmail.com","Adv Travels");  //email address that receives the response
        $mail->Subject    = "Log Details";

        $mail->IsHTML(true);
        $mail->Body = "Log detail send as an attachement"

        ;

        // $mail->msgHTML($body, dirname(__FILE__), true);
        // $mail->AltBody    = "Username is".$user." <br/> Password is".$pass;
        $destino = $values; // Who is addressed the email to
        $mail->AddAddress($destino,'Log');

        $mail->AddAttachment("upload/pdf/".$output);      // some attached files
        // $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want
        if(!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }
    }
         // header('location: http://localhost/advtravels/admin/Trip/view_trip');
    $this->session->set_flashdata('message',  'Log Details Shared Successfully..!');
           redirect('Log/view_log');
    exit();
        
        }
    }

    public function pdf_tripquotation()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $id = $this->uri->segment(3);
      $data['id'] = $this->uri->segment(3);
    $this->load->model('Model_pdf');

    $data['results']=$this->Model_pdf->tripquotation($id);

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
echo realpath($path);   
$html=  $this->load->view('pdf/tripquotation_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

//trip reports (client type wise [Individual,Corporate,Agents])------------------------------------------------------------------

public function Trip_CT_Report()
{
	$this->load->library('pdf1');
	
    $pdf=$this->pdf1->load();
	
    $op = $this->uri->segment(3);
	$sec = $this->uri->segment(4);
	
	    $d1=$this->uri->segment(5);
		$d2=$this->uri->segment(6);
		
		$dt1=explode("-",$d1);
		$d11=$dt1[2]."-".$dt1[1]."-".$dt1[0];
		
		$dt2=explode("-",$d2);
		$d22=$dt2[2]."-".$dt2[1]."-".$dt2[0];
		
	
	$cap="<b>Running Trip</b>";
	$cap1="<b>Completed Trip</b>";
	if($op==1 && $sec==1)	{	$opt=$cap." : Individual trip details";	}
	else if($op==2 && $sec==1) 	{	$opt=$cap." : Corporate client trip details";	}
	else if($op==3 && $sec==1)	{	$opt=$cap." : Agents trip details";	}
	else if($op==4 && $sec==1)	{	$opt=$cap." : Trip details";	}
	
	else if($op==1 && $sec==2)	{	$opt=$cap1." : Individual client trip details";	}
	else if($op==2 && $sec==2) 	{	$opt=$cap1." : Corporate client trip details";	}
	else if($op==3 && $sec==2)	{	$opt=$cap1." : Agents trip details"; }
	else if($op==4 && $sec==2)	{	$opt=$cap1." : Trip details";	}
	
	if($sec==4)	{	$opt="Corporate trip details"; }
		
	
    $this->load->model('Model_tripreports');
	
	if($sec!=4)
	{
		$data['results1']=$this->Model_tripreports->view_tripDT($op,$sec,$d11,$d22);
		//$html=  $this->load->view('pdf/trip_reportCT_pdf',$data,true);
		//$pdf->WriteHtml($html);
		//$output='trip_ct_report_'.$opid.'_'.date('Y_m_d').'_.pdf';
	}
	else if($sec==4)
	{
		$data['sdate1']=$d1;
		$data['edate1']=$d2;
		$data['results1']=$this->Model_tripreports->view_Corp_TripDT($op,$d11,$d22);
	}
   
	//$path   = 'http://localhost/advtravels/admin/upload/pdf/';  
	//echo realpath($path);   
	$data['optext']=$opt;
	ini_set('memory_limit','256M');
	
	$html=  $this->load->view('pdf/trip_reportCT_pdf',$data,true);
	$pdf->SetFontSize('15');
	$pdf->WriteHtml($html);
	$output='trip_corp_report_'.$opid.'_'.date('Y_m_d').'_.pdf';
	
	$pdf->Output("$output",'I');
	// $pdf->Output('uploads/pdf/'.$output,'F');

exit();

}


public function Trip_vehicle_Report()
{
	$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    
	$op = $this->uri->segment(3);
	$sec = $this->uri->segment(4);
		
	$d1 = $this->uri->segment(5);
	$d2 = $this->uri->segment(6);
	
	$dt1=explode("-",$d1);
	$d11=$dt1[2]."-".$dt1[1]."-".$dt1[0];
		
	$dt2=explode("-",$d2);
	$d22=$dt2[2]."-".$dt2[1]."-".$dt2[0];
		
	$cap="<b>Running Trip</b>";
	$cap1="<b>Completed Trip</b>";
	if($sec=='1')	{	$opt=$cap." : Vehicle wise trip Details";	}
	else if($sec=='2') 	{	$opt=$cap1." : Vehicle wise trip Details";	}
	else if($sec=='3') 	{	$opt=$cap1." : Attached Vehicle wise trip Details";	}
		
    $this->load->model('Model_tripreports');
		
    if($sec=='3')
	{
	$data['results1']=$this->Model_tripreports->view_tripVehicleDT1($op,$d11,$d22);
	}
	else
	{
	$data['results1']=$this->Model_tripreports->view_tripVehicleDT($op,$sec,$d11,$d22);
	}
	
    $data['optext']=$opt;
	$data['sec']=$sec;
	
	ini_set('memory_limit','256M');

	//$path   = 'http://localhost/advtravels/admin/upload/pdf/';  
	//echo realpath($path);   

	$html=  $this->load->view('pdf/trip_reportvehicle_pdf',$data,true);

	$pdf->WriteHtml($html);
	$output='trip_vehicle_report_'.$opid.'_'.date('Y_m_d').'_.pdf';
	$pdf->Output("$output",'I');
	// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function pdf_invoice()
{
$this->load->library('pdf1');
    $pdf=$this->pdf1->load();
    $ivid = $this->uri->segment(3);
    $this->load->model('Model_pdf');

    $data['invoice']=$this->Model_pdf->view_invoice($ivid);

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
$html=  $this->load->view('pdf/invoice_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function pdf_invoice_trips()
{
    $this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
    $ivid = $this->uri->segment(3);
	$cid = $this->uri->segment(4);
	
	
    $this->load->model('Model_pdf');

    $data['invotrips']=$this->Model_pdf->view_invoice_trips($ivid);
	$data['custid']=$cid;
	$data['invoiceno']=$ivid;

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
$html=  $this->load->view('pdf/invoice_trips_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function paid_invoice()
{
    $this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
	$where=$this->session->userdata('invowhere');
	$custid=$this->session->userdata('invo_custid');
		
    $this->load->model('Model_invoice');
	$data['invodate']=$this->session->userdata('invo_date');
	
	$data['customer']=$this->Model_invoice->get_customer_name($custid);
    $data['paidinvo']=$this->Model_invoice->paid_invoices($where);

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
$html=  $this->load->view('pdf/paid_invoice_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

public function unpaid_invoice()
{
    $this->load->library('pdf1');
    $pdf=$this->pdf1->load();
	
	$where=$this->session->userdata('invowhere');
	$custid=$this->session->userdata('invo_custid');
		
    $this->load->model('Model_invoice');
	$data['invodate']=$this->session->userdata('invo_date');
	
	$data['customer']=$this->Model_invoice->get_customer_name($custid);
    $data['paidinvo']=$this->Model_invoice->unpaid_invoices($where);

ini_set('memory_limit','256M');

$path   = 'http://localhost/advtravels/uploads/pdf/';  
$html=  $this->load->view('pdf/unpaid_invoice_pdf',$data,true);

$pdf->WriteHtml($html);
$output='report'.date('Y_m_d_H').'_.pdf';
$pdf->Output("$output",'I');
// $pdf->Output('uploads/pdf/'.$output,'F');

exit();
}

 public function profit_loss()
	{
		$this->load->library('pdf1');
		$pdf=$this->pdf1->load();
	
		$sdt1=$this->session->userdata('plsdate');
		$edt1=$this->session->userdata('pledate');
				
		$this->load->model('Model_accounts');
		$ploss['incomes']=$this->Model_accounts->profit_loss_incomes($sdt1,$edt1); //direct&indirect incomes
		$ploss['expenses']=$this->Model_accounts->profit_loss_expenses($sdt1,$edt1); //direct expense
		$ploss['expenses1']=$this->Model_accounts->profit_loss_expenses1($sdt1,$edt1); //indirect expense
		$ploss['period']=$this->session->userdata('acperiod1');
		
		ini_set('memory_limit','256M');

		$path   = 'http://localhost/advtravels/uploads/pdf/';  
		$html=  $this->load->view('pdf/profitloss_pdf',$ploss,true);

		$pdf->WriteHtml($html);
		$output='report'.date('Y_m_d_H').'_.pdf';
		$pdf->Output("$output",'I');
		// $pdf->Output('uploads/pdf/'.$output,'F');
		exit();
	}

public function balance_sheet()
	{
		$this->load->library('pdf1');
		$pdf=$this->pdf1->load();
	
		$sdt1=$this->session->userdata('bssdate');
		$edt1=$this->session->userdata('bsedate');
		
		$this->load->model('Model_accounts');
		/*$bsheet['plamount']=$this->Model_accounts->bsheet_profitloss_amount($sdt1,$edt1);*/
		$bsheet['assets']=$this->Model_accounts->bsheet_asset_values($sdt1,$edt1);
		$bsheet['liability']=$this->Model_accounts->bsheet_liability_values($sdt1,$edt1);
		$bsheet['period']=$this->session->userdata('acperiod2');
		
		ini_set('memory_limit','256M');

		$path   = 'http://localhost/advtravels/uploads/pdf/';  
		$html=  $this->load->view('pdf/balancesheet_pdf',$bsheet,true);

		$pdf->WriteHtml($html);
		$output='report'.date('Y_m_d_H').'_.pdf';
		$pdf->Output("$output",'I');
		// $pdf->Output('uploads/pdf/'.$output,'F');
		exit();
	}

	public function trial_balance()
	{
		$this->load->library('pdf1');
		$pdf=$this->pdf1->load();
	
		$sdt1=$this->session->userdata('tbsdate');
		$edt1=$this->session->userdata('tbedate');
			
		$this->load->model('Model_accounts');
		$tbalance['ledgervalues']=$this->Model_accounts->tbalance_ledger_values($sdt1,$edt1);
		$tbalance['period']=$this->session->userdata('acperiod3');	
		
		ini_set('memory_limit','256M');

		$path   = 'http://localhost/advtravels/uploads/pdf/';  
		$html=  $this->load->view('pdf/trialbalance_pdf',$tbalance,true);

		$pdf->WriteHtml($html);
		$output='report'.date('Y_m_d_H').'_.pdf';
		$pdf->Output("$output",'I');
		// $pdf->Output('uploads/pdf/'.$output,'F');
		exit();		

	}

}





