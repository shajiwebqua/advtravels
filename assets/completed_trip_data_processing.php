<?php
/*
 * DataTables example server-side processing script.
 *
 * Easy set variables
 */
 
// DB table to use
$table = 'trip_data';
 
// Table's primary key
$primaryKey = 'trip_id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes

$columns = array(
	array( 'db' => 'trip_id',  	   'dt' => 0 ),
    array( 'db' => 'customer_name',  'dt' => 1 ),
	array( 'db' => 'trip_refno','dt' => 2 ),
    array( 'db' => 'trip_drivername','dt' => 3 ),

    array(
        'db'        => 'trip_startdate',
        'dt'        => 4,
        'formatter' => function($d, $row ) {
            return date( 'd-m-Y', strtotime($d));
        }
    ),
	array(
        'db'        => 'trip_enddate',
        'dt'        => 5,
        'formatter' => function($d, $row ) {
            return date( 'd-m-Y', strtotime($d));
        }
    ),

	array(
        'db'        => 'trip_gtotal',
        'dt'        => 6,
        'formatter' => function( $d, $row ) {
            return number_format($d,"2",".","");
        }
		),

	array(
        'db'        => 'trip_status',
        'dt'        => 7,
        'formatter' => function($d, $row ) {
			if($d==2 or $d ==3)
			{
            return "<font color='red'>Closed</font>";
			}
        }
    ),
		
   /* array(
        'db'        => 'salary',
        'dt'        => 5,
        'formatter' => function( $d, $row ) {
            return '$'.number_format($d);
        }
    )*/
);
 
// SQL server connection information
$sql_details = array(
    'user' => 'root',
    'pass' => '',
    'db'   => 'advtravels',
    'host' => 'localhost'
);
  
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require('ssp.class.php');


		//custom conditions ----------------------------------
		$dt1=$_REQUEST['date1'];
		$dt2=$_REQUEST['date2'];
		if(!empty($dt1) and !empty($dt2))
		{
		$d1=explode("-",$dt1);
		$da1=$d1[2]."-".$d1[1]."-".$d1[0];

		$d2=explode("-",$dt2);
		$da2=$d2[2]."-".$d2[1]."-".$d2[0];

		$where="trip_enddate>='".$da1 ."' and trip_enddate<='".$da2."'";
		}
		else
		{
		$where=null;		
		}

echo json_encode(
    /*SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )*/
	SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns,$whereResult=null,$whereAll=$where)
);
