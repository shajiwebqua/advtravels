<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
<title>Travels</title>

 <style type="text/css">
 td{padding:2px 5px 3px 5px; }
 table{font-size:14px;}

	.table-main{
		  margin-top:15px;
          border-collapse: collapse;
		  font-size:15px;
     }

    .table-main td{
    border: 1px solid black;
     }
	 .table-main th{
       padding-top:3px;
	   padding-bottom:3px;
     }
	
	.table-inner td{
		border: none !important;
	}
 </style>
  
</head>

<body>
<!-- heading ----------------------------------------------------->
<br>
<table style="border:none;width:100%;margin-top:20px;">
<tr>
<td width="150px" style="padding: 0px">
<img src="<?php echo base_url('upload/images/atravels_logo.png');?>" width='150px' height='30px'>
</td>
<td width="150px" style="padding: 0px" align='right'>
<?php $date = date('d/m/Y');?>
Date: <?=$date;?>
  </td>
</tr>

<tr>
<td width="150px" style="padding: 0px">
<Strong>Advance World Holidays</strong><br>
PK & Sons Complex, East Moozhikkal,&nbsp;
NH 212, Calicut, Kerala, India-673571<br>
Tel: 0091 495 2732 500, 0091 495 3100 700,&nbsp;
Fax: 0091 495 2732 400
</td>
<td width="150px" style="padding: 0px" align='right'>

</td>
</tr>
</table>
<!-- heading end----------------------------------------------------->
<hr>
<?php
if(isset($customer))
{
	$htext="Client: &nbsp;&nbsp;&nbsp;".$customer;
	$dt="Period : &nbsp;".$invodate;
}
else
{
	$htext="Client:All";
	$dt="";
}
?>

<div width='1024px'>
<table width='100%'>
<tr><td align='center'><h3>PAID INVOCE DETAILS</h3></td></tr>
<tr><td><h3><?=$htext;?></h3></td></tr>
<tr><td ><h4><?php echo $dt;?></h4></td></tr>
</table>
<table width='100%'><tr><td><hr></td></tr></table>



<table class="table table-striped table-hover table-bordered" id="example" width='100%'>
              <thead>
                <tr>
				 <th>Id</th> 
 				 <th>Date</th> 
                 <th>Invoice_No</th>
				 <th width='25%'>Customer</th>
				 <th>Amount</th>
				 <th>GST%</th>
				 <th>GST</th>
				 <th>Total</th>
				 </tr>
                </thead>
				<tbody>
				
				<?php
				$tot=0;
				$gtot=0;
				foreach($paidinvo as $pd)
				{
					if($pd->invo_status==1){$st="Paid";}else{$st="unpaid";}
					$tot+=($pd->invo_amount+$pd->invo_gst);
					$gtot+=$tot;
				?>
				
				 <tr>
				 <td><?=$pd->invo_id;?></td> 
 				 <td><?=date_format(date_create($pd->invo_date),'d-m-Y');?></td> 
				 <td>INV-<?=$pd->invo_invoiceid;?></td>
                 <td width='25%'><?=$pd->customer_name;?><br><?=nl2br($pd->customer_address);?></td>
				 <td align='right'><?=number_format($pd->invo_amount,'2','.','');?></td>
				 <td align='right'><?=$pd->invo_gstper;?>%</td>
				 <td align='right'><?=number_format($pd->invo_gst,'2','.','');?></td>
				 <td align='right'><b><?=number_format($tot,'2','.','');?></b></td>
				 </tr>
				
				<?php
				}
				?>

				</tbody>
				<tfoot>
					 <tr style='font-size:17px !important;text-align:right;'>
					 <td colspan=8><hr></td>
					 </tr>
				</tfoot>
            </table>
			<table width='100%'>
				<tr> <td width='75%' align='right'><h3>Total Paid Amount:</h3></td>
					 <td width='25%' align='right'><h3><?=number_format($gtot,'2','.','');?><h3></td>
					 </tr>
				<tr> <td width='75%' align='right'>&nbsp;</td>
					 <td width='25%' align='right'>===============</td>
					 </tr>	 
				
			</table>

</div>
</body>
</html>