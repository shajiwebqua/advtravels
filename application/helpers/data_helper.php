<?php

function if_data_exist($data, $model,$function){
	$CI =& get_instance();
	$CI->load->model($model);
	$status = $CI->$model->$function($data);
	if($status == FALSE){
		return FALSE;
	}
	return TRUE;
}

function get_data($data, $model,$function){
	$CI =& get_instance();
	$CI->load->model($model);
	return $CI->$model->$function($data);
}

function dateFormat($date, $format = 'd-m-Y'){
  $date = new DateTime($date);
  return $date->format($format);
}