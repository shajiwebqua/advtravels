<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asset extends CI_Controller 
{
    
    function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url','html'));
		$this->load->model('Model_assets');
	}
	
	public function index()
	{
		$data['result'] = $this->db->get('assets')->result();
		$this->load->view("admin/add_assets",$data);
	}

	public function add_assets()
	{
		$dt=$this->input->post('date');
		$d1=explode("-",$dt);
		$dt1=$d1[2]."-".$d1[1]."-".$d1[0];
		
		
    	$data = array(
      		'asset_date' => $dt1,
      		'asset_name' => $this->input->post('asset_name'),
      		'asset_cost' => $this->input->post('actual_cost'),
      		'asset_qty' => $this->input->post('quantity'),
      		'asset_description' => $this->input->post('description'),
    	);
		
    	$this->db->insert('assets',$data);
        echo "success";
	}
	
	public function edit_assets()
	{
      $id = $this->input->get('id');
      $data['assets'] = $this->Model_assets->get_asset($id);  
      $this->load->view('admin/edit_assets',$data);
	}

	public function update_assets()
	{
     $id = $this->input->post('edit_assets_id');
     $data = array(
          'asset_date' => $this->input->post('edit_date'),
          'asset_name' => $this->input->post('edit_asset_name'),
          'asset_cost' => $this->input->post('edit_actual_cost'),
          'asset_qty' => $this->input->post('edit_quantity'),
          'asset_description' => $this->input->post('edit_description'),
      );
      
      $this->db->where('asset_id',$id)->update('assets',$data);
     
        $this->session->set_flashdata('message',  '2#Details successfiully updated.!');
		redirect('Asset/index');
      
  }

  public function delete_assets()
	{
    	$id = $this->input->get('id');
    	$this->db->where('asset_id', $id)->delete('assets');
    	if($this->db->affected_rows() > 0)
    	{
        	echo "success";
    	}
    	else
    	{
      	echo "fail";  
    	}
  }

}
?>