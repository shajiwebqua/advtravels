<?php
 include('application/views/common/header.php');?>
 <style>
.innerBtn
{
height:40px;	
}
</style>
 
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper"> 
  
<!-- Content Header (Page header) -->	  
  <section class="content-header">
  <!--<h1> View Trip Details </h1>-->
  <h1><b>Trip Details</b></h1>
   <ol class="breadcrumb" style='font-size:15px;'>
   
		<li><a href="<?php echo base_url('Trip/index');?>" style='color:#4b88ed;'><button class='btn btn-primary'><i class="fa fa-cab" aria-hidden="true"></i> New Trip Sheet</button></a></li>
		<!--<li><a href="<?php echo base_url('Trip/completed_trip');?>"style='color:#4b88ed;'><i class="fa fa-bus" aria-hidden="true"></i>Completed</a></li> -->
		
		<li><a id='view-pdf' href="<?php echo site_url('Pdf/pdf_rtrip') ?>" style='color:#4b88ed;'><button class='btn btn-warning'><i class="fa fa-print" aria-hidden="true"></i> Print</button></a></li>
		<!-- data-target="#myModal1" --> <li><a id='idsave' href="<?php echo site_url('Pdf/pdf_rtrip') ?>" data-target="" data-toggle='modal' style='color:#4b88ed;' target="_blank"><button class='btn btn-success'><i class="fa fa-save" aria-hidden="true"></i> Save</button></a></li>
		<!-- data-target="#myModal1" --><li><a id='idshare' href="" data-target="" data-toggle='modal' style='color:#4b88ed;'><button class='btn btn-info'><i class="fa fa-share-alt" aria-hidden="true"></i> Share</button></a></li>
    <?php 
    if(checkisAdmin()){
	  // echo "<li><a id='iddelete' href='' style='color:#4b88ed;'><i class='fa fa-trash' aria-hidden='true'></i>Delete</a></li>";
    }
    ?>
     </ol> 
 </section>
  <label style="background-color:#cecece;width:100%;height:1px;"></label>
<!-- Content Header  end -->	  
	  
  <section class="content"> 
  <div style="color:blue;font-size:12px;"> <!-- Select Customer details first (<b>CLICK ON THE TABLE ROW</b>) , Then Choose the option (Print,Share,Save,View,Edit,Delete)  --> </div>
        <div style="padding:2px 0px 2px 0px;">
				 <div id="idmsg" style="background-color:#fff;height:25px;margin-bottom:3px;">
				 <center><div id='msg'><?php echo $this->session->flashdata('message'); ?></div></center>
				 </div>
		</div>  
  <!-- Small boxes (Stat box) -->
    <div class="row">
	<div class="col-md-12">

	<!-- Add new user details-->
	
	 <div style="background-color:#fff;border-radius:3px;padding:10px; ">
					
		<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">

				<!-- <hr style="color:#cecece;width:100%;height:1px;"> -->
				
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                    <div class="col-md-12">
								</div>
								</div>
								</div>
						<table class="table table-striped table-hover table-bordered" id="example">
                        <thead>
								<tr style='color:#4b88ed'>
								 <th></th>
								 <th></th>
								 <th>Trip_Id</th>
								 <th>Customer</th>
								 <th>Mobile/Phone</th>
								 <th>Driver</th>
								 <th>Date</th>
								 <th>Days</th>
								 <th>KM</th>
								 <th>Charge</th>
								 <th>Others</th>
								 <th>Total</th>
								 <th>Status</th>
								 </tr>
								</thead>
                        </table>
                        </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
	
  
        <div class="modal fade draggable-modal" id="myModal2" tabindex="-1" role="basic" aria- hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-body" >
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Edit</h4>
								</div>
							</div>
							</div>
						<!-- /.modal-dialog -->
						</div>
                   </div>			

           <div class="modal fade draggable-modal" id="myModal1" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Edit</h4>
                </div>
              </div>
              </div>
			  
		
            <!-- /.modal-dialog -->
            </div>
                   </div>  

           <div class="modal fade draggable-modal" id="myModals" tabindex="-1" role="basic" aria- hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" >
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title">Share</h4>
                </div>
              </div>
              </div>
            <!-- /.modal-dialog -->
            </div>
          </div>     
        </div>       

				
	</div>
	<!-- End user details -->

    <!-- /.row --> 
    <!-- /.row (main row) --> 
  
  <!-- /.content --> 
</div>
</section>
</div>
<!-- /.content-wrapper --> 

<?php include('application/views/common/footer.php');?>

<script type="text/javascript">

//sweet alert box ----------------------
$("#idmsg").hide();

if($("#msg").html()!="")
	{
		var msg1=$("#msg").html();
		swal("",msg1,"success")
		$("#msg").html("");
	}
//-------------------------------------

 setTimeout(function(){ $("#idh5").html(""); }, 3000);
 
      $('#example').dataTable( {
		 "ordering": false,
         destroy: true,
        "processing": true,
       // 'scrollX':true,
	   
        "ajax": {
                url :"<?php echo base_url(); ?>" + "Trip/trip_ajax/1",// json datasource
               },
					   
        "columns": [
			{ "data": "edit" },
			{ "data": "complete" },
			{ "data": "tripid" },
            { "data": "customer"},
			{ "data": "custphone"},
			{ "data": "driver"},
            { "data": "date" },
			{ "data": "days" },
			{ "data": "totalkm" },
			{ "data": "charge" },
			{ "data": "ocharge" },
			{ "data": "gtotal" },
			{ "data": "status" },
       ]
  } );

 var table = $('#example').DataTable();
      $('#example tbody').on( 'click', 'tr', function () {
         if ( $(this).hasClass('selected') ) {
             $(this).removeClass('selected');
         }
         else {
 	        table.$('tr.selected').removeClass('selected');
             $(this).addClass('selected');
         }
   });			   

    
  $('#view-pdf').on('click',function(){
	  	  
     //   var id=$('#example').find('tr.selected').find('td').eq(1).text();
     // if(id=="")
     // {
     //   alert ("please Select customer details..");
     // }
     // else
     // {
        //var pdf_link = $(this).parent('a').attr('href')+'/'+id;
		var pdf_link = $(this).attr('href');
        //alert(pdf_link);
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>';
        //alert(iframe);
        $.createModal({
        title:'Trip Details',
        message: iframe,
        closeButton:true,
        scrollable: true,
    });
       
     return false; 

    });    

  
      $('#idedit').click(function () {
	  
	if(id=="")
		 {
			 alert ("please Select staff details..");
		 }
		 else
		 {
	
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/edit_trip/"+id,
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
			
                   }
            });
		 }
		 
        }); 

		
  $('#idview').click(function ()
  {
        //var Result=$("#myModal1 .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
         var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 if(id=="")
		 {
			 alert ("please Select staff details..");
		 }
		 else
		 {
			//$("#idview").attr('data-target','#myModal1') 
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/update_details/1",
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
        }
          });
		 }
        }); 		


       $('#idshare').click(function () {
        var Result=$("#myModals .modal-body");
     //   $(this).parent().parent().toggleClass('selected');
   //      var id=$('#example').find('tr.selected').find('td').eq(1).text();
		 // if(id=="")
		 // {
			//  alert ("please Select staff details..");
		 // }
		 // else
		 // {
			$("#idshare").attr('data-target','#myModals') 
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/share_rtrip_details",
        dataType: 'html',
        //data: {id: id},
        success: function(res) {
			 Result.html(res);
                    }
            });
		 //}
    
        });  

  $('#idsave').click(function ()
    {   
         // var id=$('#example').find('tr.selected').find('td').eq(1).text();
         // if(id=="")
         // {
         //     alert ("please Select trip details..");
         // }
         // else
         // {
            jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "Trip/save_rtrip",
            dataType: 'html',
           // data:{tid:id},
            success: function(res) {
				//alert("Trip sheet saved...!");
             }
            });
         //}
        }); 
	
		
	$('#iddelete').click(function () 
    {
    var res=confirm("Delete this coustomer details?");
    if(res)
    {
    var id=$('#example').find('tr.selected').find('td').eq(1).text();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Trip/del_entry",
        dataType: 'html',
        data: {tid: id},
        success: function(res) {
        Result.html(res);
                    }
            });
		}
        });     
		
      $(document).on("click", "#del_conf", function () {
            return confirm('Are you sure you want to delete this entry?');
        });
 //});
  


</script>



